//
//  ErrorShowVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 29/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class ErrorShowVC: UIViewController {

    @IBOutlet weak var btnCancel: AttributedButton!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    var message = ""
     var Flow = ""
      
    override func viewDidLoad() {
        super.viewDidLoad()
    self.tabBarController?.tabBar.isHidden = true
    setFont ()
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func setFont ()  {
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 21, color: Color.red, title: "ERROR", placeHolder: "")
        Fonts().set(object: lblMsg, fontType: 0, fontSize: 16, color: Color.c52_58_64, title: message, placeHolder: "")
        Fonts().set(object: btnCancel, fontType: 1, fontSize: 14, color: Color.c0_137_255, title: "CANCEL", placeHolder: "")
        btnCancel.backgroundColor = Color.white
 
    }
    
    @IBAction func btnCancel(_ sender: AttributedButton) {
//        if CurrentFlow.flowType == FlowType.PesaLinkToPhone{
//        self.navigationController?.popViewController(animated: true)
//            self.setUpSlideMenu()
//        }
//        else
            if Flow == "1"{
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        }
        else  if Flow == "2"{
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 2], animated: true)
            }
        else if Flow == "salary" || CurrentFlow.flowType == FlowType.ForexRate {
            global.cupApi = "NO"
             self.setUpSlideMenu()
        }
        else if CurrentFlow.flowType == FlowType.SendMoney
        || CurrentFlow.flowType == FlowType.BuyAirtime || CurrentFlow.flowType == FlowType.InternalFundTransfer || CurrentFlow.flowType == FlowType.Rtgs || CurrentFlow.flowType == FlowType.PesaLinkToAc || CurrentFlow.flowType == FlowType.PesaLinkToCard || CurrentFlow.flowType == FlowType.PesaLinkToPhone || CurrentFlow.flowType == FlowType.MpesaToAccount || CurrentFlow.flowType == FlowType.PayBillTransaction || CurrentFlow.flowType == FlowType.BuyGoods
         {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        }
                
                 else if CurrentFlow.flowType == FlowType.PayBill
            {
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 5], animated: true)
            }
        else{
        //    self.setUpSlideMenu()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        leftVC.mainViewController = homeVC
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
}
