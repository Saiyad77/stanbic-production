//
//  ViewController.swift
//  DemoProgramaticallyControls
//
//  Created by 5Exceptions6 on 10/05/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var lblTitle: UILabel?
    var btn1: UIButton?
    var btn2: UIButton?
    var btn3: UIButton?
    var btn4: UIButton?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setLbl()
        setBtn1()
        setBtn2()
        setBtn3()
        let width = (self.view.frame.width-80)/3
        var x = 20
        var y = 300
        var height = 30
        for i in 1...6
        {
            let frame = CGRect(x: x, y: y, width: Int(width), height: height)
            btn4 = UIButton(frame: frame)
            btn4?.backgroundColor = .red
            self.view.addSubview(btn4!)
            
            if i >= 3
            {
             y = y + (height+20)
            }
            else
            {
            x = x + Int(width) + 20
            }
        }
        
    }
   
func setLbl()
{
    let frame = CGRect(x: 185, y: 100, width: 40, height: 20)
    lblTitle = UILabel(frame: frame)
    lblTitle?.text = "Title"
    lblTitle?.backgroundColor = .green
    self.view.addSubview(lblTitle!)
}

    func setBtn3()
    {
        let width = (self.view.frame.width-80)/3
        let frame = CGRect(x: 20, y: 200, width: width, height: 40)
        btn3 = UIButton(frame: frame)
        btn3?.setTitle("3", for: .normal)
        btn3?.backgroundColor = .red
        self.view.addSubview(btn3!)
    }
    func setBtn2()
    {
        let width = (self.view.frame.width-80)/3
        let x = width + 40
        let frame = CGRect(x: x, y: 200, width: width, height: 40)
        btn2 = UIButton(frame: frame)
        btn2?.setTitle("2", for: .normal)
        btn2?.backgroundColor = .green
        self.view.addSubview(btn2!)
    }
    func setBtn1()
    {
        let width = (self.view.frame.width-80)/3
        let x = (width*2) + 60
        let frame = CGRect(x: x, y: 200, width: width, height: 40)
        btn1 = UIButton(frame: frame)
        btn1?.setTitle("1", for: .normal)
        btn1?.backgroundColor = .red
        self.view.addSubview(btn1!)
    }
    
    
}

