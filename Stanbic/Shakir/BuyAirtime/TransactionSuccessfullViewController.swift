//
//  TransactionSuccessfullViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 20/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class TransactionSuccessfullViewController: UIViewController, UINavigationControllerDelegate {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    // titles
    @IBOutlet weak var lbldatetitle: UILabel!
    @IBOutlet weak var lbltimetitle: UILabel!
    @IBOutlet weak var lblsendtotitle: UILabel!
    @IBOutlet weak var lblsentfromtitle: UILabel!
    @IBOutlet weak var lblamounttitle: UILabel!
    @IBOutlet weak var lblrecieptnotitle: UILabel!
    // values
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var lblsendto: UILabel!
    @IBOutlet weak var lblsentfrom: UILabel!
    @IBOutlet weak var lblamount: UILabel!
    @IBOutlet weak var lblrecieptno: UILabel!
    @IBOutlet weak var imgright: UIImageView!
    @IBOutlet weak var viewdata: UIView!
    @IBOutlet weak var btndone: UIButton!
    
    var message : String?
    var dataModels : BuyaTimeVM?
    var recieptNo = ""
 
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        setData()
        
        lblsendto.text =  dataModels?.mobilenumber!
        
        imgright.layer.borderWidth = 2
        imgright.layer.borderColor = Color.white.cgColor
        imgright.clipsToBounds = true
        imgright.layer.cornerRadius = imgright.frame.height/2
        self.tabBarController?.tabBar.isHidden = true
    //    addShadowImage(Image : imgright as! UIImageView)
        addShadow1(button: btndone)
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        if CurrentFlow.flowType == FlowType.SendMoney{
            Fonts().set(object: self.lblsubtitle, fontType: 0, fontSize: 16, color: Color.c52_58_64, title: message!, placeHolder: "")
        }
        else if CurrentFlow.flowType == FlowType.BuyAirtime{
            Fonts().set(object: self.lblsubtitle, fontType: 0, fontSize: 16, color: Color.c52_58_64, title: message!, placeHolder: "")
        }
        else if CurrentFlow.flowType == FlowType.PayBill{
            Fonts().set(object: self.lblsubtitle, fontType: 0, fontSize: 16, color: Color.c52_58_64, title: message!, placeHolder: "")
        }
        else if CurrentFlow.flowType == FlowType.InternalFundTransfer{
            Fonts().set(object: self.lblsubtitle, fontType: 0, fontSize: 16, color: Color.c52_58_64, title: message!, placeHolder: "")
        }
        else {
              Fonts().set(object: self.lblsubtitle, fontType: 0, fontSize: 16, color: Color.c52_58_64, title: message!, placeHolder: "")
        }
       
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 21, color: Color.c0_173_108, title: "THANKYOU", placeHolder: "")
       
        
        Fonts().set(object: self.lbldatetitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "DATE", placeHolder: "")
        Fonts().set(object: self.lbltimetitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "TIME", placeHolder: "")
        Fonts().set(object: self.lblsendtotitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "SENTTO", placeHolder: "")
        Fonts().set(object: self.lblsentfromtitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "SENTFROM", placeHolder: "")
        Fonts().set(object: self.lblamounttitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "Amount", placeHolder: "")
        Fonts().set(object: self.lblrecieptnotitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "RECIEPTNO", placeHolder: "")
        
        Fonts().set(object: self.lbldate, fontType: 1, fontSize: 15, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: self.lbltime, fontType: 1, fontSize: 15, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: self.lblsendto, fontType: 1, fontSize: 15, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: self.lblsentfrom, fontType: 1, fontSize: 15, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: self.lblamount, fontType: 1, fontSize: 15, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: self.lblrecieptno, fontType: 1, fontSize: 15, color: Color.black, title: recieptNo, placeHolder: "")
       
        Fonts().set(object: btndone, fontType: 1, fontSize: 14, color: Color.white, title: "DONE", placeHolder: "")
    
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        //self.navigationController?.popViewController(animated: true)
        global.cupApi = "NO"
         setUpSlideMenu()
    }
    
    @IBAction func Done(_ sender: AttributedButton) {
//        let v = self.navigationController?.viewControllers
//        for i in (v)!
//        {
//            if i is HomeViewController
//            {
//                self.navigationController?.popToViewController(i, animated: true)
//                break
//            }
//        }
       global.cupApi = "NO"
        setUpSlideMenu()
    }
    
    @IBAction func share(_ sender: AttributedButton) {
        
    }
}


extension TransactionSuccessfullViewController {
    
    func setData() {
        let dateStr = DateConverter().convertDate(date_str: "\(Date())", formatTo: "dd/MM/yyyy", formatFrom: "yyyy-MM-dd HH:mm:ss +z")
        let timeStr = DateConverter().convertDate(date_str: "\(Date())", formatTo: "hh:mm a", formatFrom: "yyyy-MM-dd HH:mm:ss +z")
        lbldate.text = dateStr
        lbltime.text = timeStr
        lblsendto.text = dataModels?.mobilenumber
        lblsentfrom.text = dataModels?.acalias
        lblamount.text = dataModels?.amount ?? ""
        lblrecieptno.text = recieptNo
        //lblsubtitle.text = recieptNo
        
    }
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
}


