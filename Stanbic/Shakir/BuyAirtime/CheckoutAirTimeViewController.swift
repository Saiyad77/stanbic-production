//
//  CheckoutAirTimeViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 20/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication

class CheckoutAirTimeViewController: UIViewController, UINavigationControllerDelegate {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblbuyairtimefor: UILabel!
    @IBOutlet weak var lblmyself: UILabel!
    @IBOutlet weak var lblmobile: UILabel!
    @IBOutlet weak var lblamounttitle: UILabel!
    @IBOutlet weak var lblamount: UILabel!
    @IBOutlet weak var lblbuyfrom: UILabel!
    @IBOutlet weak var lblactype: UILabel!
    @IBOutlet weak var lbltransactioncosttitle: UILabel!
    @IBOutlet weak var lbltransactioncost: UILabel!
    @IBOutlet weak var lblcostmsg: UILabel!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var switchfav: UISwitch!
    @IBOutlet weak var lblAddFav: UILabel!
    @IBOutlet weak var viewcenter: UIView!
    @IBOutlet weak var viewcheckbox: UIView!
    
    // Benificially btm
    @IBOutlet weak var txtbenificially: UITextField!
    @IBOutlet weak var viewbeneficialy: UIView!
    @IBOutlet weak var consHeight: NSLayoutConstraint!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    
    var dataModels : BuyaTimeVM?
    var paybilldataModels1 : PayBillVM1?
    var nominate = ""
    var reasonForPayment = ""
    var internalfundModel: IntenalFundVM?
    var mySelf = ""
    var sendMoneyName = ""
    var recipientAddress = ""
    var enterpin = ""
    var accountno = ""
    var TotelAmount = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // chek for hide and show buttom views
        if mySelf == "Myself"{
            viewcheckbox.isHidden =  true
            viewbeneficialy.isHidden = true
            
        }
        
        txtbenificially.autocorrectionType = .no
        btmview(hide: true)
        setFonts()
        setdata()
        self.tabBarController?.tabBar.isHidden = true
        viewcenter.addshaow(offset : CGSize(width: 2, height: 2))
        viewcheckbox.addshaow(offset : CGSize(width: -1, height: -1))
        txtbenificially.addUI(placeholder: "ENTERBENEFICIARY")
        txtbenificially.delegate = self
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        view.endEditing(true)
    }
    
    // SetData:-
    func setdata() {
        if CurrentFlow.flowType == FlowType.BuyAirtime {
            lblactype.text = dataModels?.acalias!
            lblmobile.text = dataModels?.mobilenumber! ?? ""
            lblmyself.text = dataModels?.mobilename!
            lblamount.text = dataModels?.amount!
            lblbuyfrom.text = "BuyFrom".localized(Session.sharedInstance.appLanguage)
            viewbeneficialy.isHidden = true
            viewcheckbox.isHidden = true
            
          // Transaction Cost :-
            let cost = getTransectionCostFor(service_code: "BAO", amount: dataModels?.amount! ?? "0")
            lbltransactioncost.text = cost
            if dataModels?.mobilenumber! == OnboardUser.mobileNo{
                viewcheckbox.isHidden = true
            }else{
                viewcheckbox.isHidden = true
                
            }
            lblcostmsg.isHidden = true
            
            if switchfav.isOn == true{
                nominate = "Yes"
            }
            else {
                nominate = "No"
            }
        }
        else if CurrentFlow.flowType == FlowType.SendMoney{
            lblactype.text = dataModels?.acalias!
            lblmobile.text = dataModels?.mobilenumber!
            lblmyself.text = sendMoneyName
            lblamount.text = dataModels?.amount!
            if dataModels?.nominateflag == "Yes"{
                switchfav.isOn = false
                viewbeneficialy.isHidden = true
                viewcheckbox.isHidden = true
            }
            else{
                switchfav.isOn = false
                viewcheckbox.isHidden = true
                viewbeneficialy.isHidden = true
            }
            nominate = dataModels?.nominateflag! ?? ""
            txtbenificially.text =  dataModels?.mobilename ?? reasonForPayment
            txtbenificially.isUserInteractionEnabled = false
            //btmview(hide: true)
            
            // Transaction Cost :-
            let cost = getTransectionCostFor(service_code: "SM", amount: (dataModels?.amount! ?? "0"))
            lbltransactioncost.text = cost
            
            if dataModels?.mobilenumber! == OnboardUser.mobileNo{
                viewcheckbox.isHidden = true
            }else{
                viewcheckbox.isHidden = false
                
            }
            
            if switchfav.isOn == true{
                nominate = "Yes"
            }
            else {
                nominate = "No"
            }
            
            viewbeneficialy.isHidden = true
            viewcheckbox.isHidden = true
        }
        else if CurrentFlow.flowType == FlowType.MpesaToAccount{
            lblactype.text = dataModels?.acalias!  
            lblmobile.text = dataModels?.mobilenumber!
            lblmyself.text = "M_PESA".localized(Session.sharedInstance.appLanguage)
            
//            if dataModels?.mobilenumber == OnboardUser.mobileNo {
//             lblmyself.text = "M_PESA".localized(Session.sharedInstance.appLanguage)
//            }
//            else {
//              lblmyself.text =  dataModels?.mobilename!
//            }
            
            lblamount.text = dataModels?.amount!
            lblbuyfrom.text = "Transefer to"
            if dataModels?.nominateflag == "Yes"{
                switchfav.isOn = false
                viewbeneficialy.isHidden = true
                viewcheckbox.isHidden = true
            }
            else{
                viewbeneficialy.isHidden = true
                viewcheckbox.isHidden = true
            }
            nominate = dataModels?.nominateflag! ?? ""
            btmview(hide: true)
            
            // Transaction Cost :-
            let cost = getTransectionCostFor(service_code: "STKTOPUP", amount: dataModels?.amount! ?? "0")
            lbltransactioncost.text = cost
//            if dataModels?.mobilenumber! == OnboardUser.mobileNo{
//                viewcheckbox.isHidden = true
//            }else{
//                viewcheckbox.isHidden = false
//            }
            
            if switchfav.isOn == true{
                nominate = "Yes"
            }
            else {
                nominate = "No"
            }
        }
        else if CurrentFlow.flowType == FlowType.PayBill {
            lblactype.text = paybilldataModels1?.acalias!
            lblmobile.text = paybilldataModels1?.cardnumber!
            lblmyself.text = paybilldataModels1?.biller!
            lblamount.text = paybilldataModels1?.amount!
            lblbuyfrom.text = "PayFrom".localized(Session.sharedInstance.appLanguage)
            lblbuyairtimefor.text = "MAKEPAYMENTTO".localized(Session.sharedInstance.appLanguage)
            
            // Transaction Cost :-
            if mySelf == "CREDITCARD"{
                let cost = getCreditcardCostFor(amount: paybilldataModels1?.amount! ?? "0")
                lbltransactioncost.text = cost
            }
            else {
                let cost = getTransectionCostFor(service_code: "PB", amount: paybilldataModels1?.amount! ?? "0")
                lbltransactioncost.text = cost
            }
            
            
                if paybilldataModels1?.nominateFlag == "Yes"{
                    switchfav.isOn = false
                    viewbeneficialy.isHidden = true
                    
                }
                else{
                    viewbeneficialy.isHidden = true
                    switchfav.isOn = false
                    viewcheckbox.isHidden = true
                    
                }
                nominate = paybilldataModels1?.nominateFlag! ?? ""
            
            if switchfav.isOn == true{
                nominate = "Yes"
            }
            else {
                nominate = "No"
            }
            
            if switchfav.isOn == true{
                nominate = "Yes"
            }
            else {
                nominate = "No"
            }
            
        }
        else if CurrentFlow.flowType == FlowType.InternalFundTransfer{
            
            lblmyself.text = internalfundModel?.anominatealias
            lblmobile.text =  internalfundModel?.anominatedaccount
            lblamount.text = internalfundModel?.amount!
            lblbuyfrom.text = "Transfer money from"
            lblactype.text = internalfundModel?.acalias!
            viewbeneficialy.isHidden = true
            
            if internalfundModel?.nominateflag == "Yes"{
                nominate = internalfundModel?.nominateflag ?? "No"
                switchfav.isOn = true
                viewbeneficialy.isHidden = true
                viewcheckbox.isHidden = true
                txtbenificially.text = internalfundModel?.anominatealias
            }
            else{
                viewbeneficialy.isHidden = true
                switchfav.isOn = false
                 nominate = internalfundModel?.nominateflag ?? "No"
            }
        
            // txtbenificially.text = = internalfundModel
            // Transaction Cost :-
            let cost = getTransectionCostFor(service_code: "TF", amount: internalfundModel?.amount! ?? "0")
            lbltransactioncost.text = cost
            
//            if internalfundModel?.anominatedaccount == OnboardUser.mobileNo{
//                viewcheckbox.isHidden = true
//            }else{
//                viewcheckbox.isHidden = false
//
//            }
            if switchfav.isOn == true{
                nominate = "Yes"
            }
            else {
                nominate = "No"
            }
        }
        else if CurrentFlow.flowType == FlowType.Rtgs{
            
            lblmyself.text = internalfundModel?.anominatealias
            lblmobile.text =  internalfundModel?.anominatedaccount
            lblamount.text = internalfundModel?.amount!
            lblbuyfrom.text = "TRANSFERMONEYTO".localized(Session.sharedInstance.appLanguage)
            lblactype.text = internalfundModel?.acalias!
            
            if internalfundModel?.nominateflag == "Yes"{
                switchfav.isOn = false
                viewbeneficialy.isHidden = true
                viewcheckbox.isHidden = true
                txtbenificially.text = internalfundModel?.anominatealias
            }
            else{
                viewbeneficialy.isHidden = true
                switchfav.isOn = false
                viewcheckbox.isHidden = false
            }
            nominate = (internalfundModel?.nominateflag!)!
            // Transaction Cost :-
            let cost = getTransectionCostFor(service_code: "RTGS", amount: internalfundModel?.amount! ?? "0")
            lbltransactioncost.text = cost
            if switchfav.isOn == true{
                nominate = "Yes"
            }
            else {
                nominate = "No"
            }
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToAc{
            
            lblmyself.text = internalfundModel?.anominatealias
            lblmobile.text =  internalfundModel?.anominatedaccount
            lblamount.text = internalfundModel?.amount!
            lblbuyfrom.text = "TRANSFERMONEYFROM1".localized(Session.sharedInstance.appLanguage)
            lblactype.text = internalfundModel?.acalias!
            
            if internalfundModel?.nominateflag == "Yes"{
                switchfav.isOn = true
                viewbeneficialy.isHidden = true
                viewcheckbox.isHidden = true
            }
            else{
                viewbeneficialy.isHidden = true
                switchfav.isOn = false
                viewcheckbox.isHidden = false
            }
            nominate = (internalfundModel?.nominateflag!)!
            txtbenificially.text = nominate
            // Transaction Cost :-
            let cost = getTransectionCostFor(service_code: "PAYAC", amount: internalfundModel?.amount! ?? "0")
            lbltransactioncost.text = cost
            
            if switchfav.isOn == true{
                nominate = "Yes"
            }
            else {
                nominate = "No"
            }
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToCard{
            
            lblmyself.text = internalfundModel?.anominatealias
            lblmobile.text =  internalfundModel?.anominatedaccount
            lblamount.text = internalfundModel?.amount!
            lblbuyfrom.text = "TRANSFERMONEYFROM1".localized(Session.sharedInstance.appLanguage)
            lblactype.text = internalfundModel?.acalias!
            
            if internalfundModel?.nominateflag == "Yes"{
                switchfav.isOn = true
                viewcheckbox.isHidden = true
                viewbeneficialy.isHidden = true
            }
            else{
                switchfav.isOn = false
                viewcheckbox.isHidden = false
            }
            nominate = (internalfundModel?.nominateflag!)!
            // Transaction Cost :-
            let cost = getTransectionCostFor(service_code: "PAYC", amount: internalfundModel?.amount! ?? "0")
            lbltransactioncost.text = cost
            if internalfundModel?.anominatedaccount == OnboardUser.mobileNo{
                viewcheckbox.isHidden = true
            }else{
                viewcheckbox.isHidden = false
            }
            if switchfav.isOn == true{
                nominate = "Yes"
            }
            else {
                nominate = "No"
            }
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToPhone{
            
            lblmyself.text = internalfundModel?.anominatealias
            lblmobile.text =  internalfundModel?.anominatedaccount
            lblamount.text = internalfundModel?.amount!
            lblactype.text = internalfundModel?.acalias!
            
            if internalfundModel?.nominateflag == "Yes"{
                switchfav.isOn = false
                viewbeneficialy.isHidden = true
                viewcheckbox.isHidden = true
                txtbenificially.text = internalfundModel?.anominatealias
            }
            else{
                viewbeneficialy.isHidden = true
                switchfav.isOn = false
                viewcheckbox.isHidden = false
            }
            
            nominate = (internalfundModel?.nominateflag!)!
            // Transaction Cost :-
            let cost = getTransectionCostFor(service_code: "PAYM", amount: (internalfundModel?.amount! ?? "0"))
            lbltransactioncost.text = cost
            
            
            if switchfav.isOn == true{
                nominate = "Yes"
            }
            else {
                nominate = "No"
            }
        }
        else if CurrentFlow.flowType == FlowType.PayBillTransaction{
            lblactype.text = dataModels?.acalias!
            lblmobile.text = dataModels?.mobilenumber! ?? ""
            lblmyself.text = "BussinessNumber".localized(Session.sharedInstance.appLanguage)
            lblamount.text = dataModels?.amount!
            lblbuyfrom.text = "PayFrom".localized(Session.sharedInstance.appLanguage)
            // Transaction Cost :-
            let cost = getTransectionCostFor(service_code: "ACCBILL", amount: dataModels?.amount! ?? "0")
            lbltransactioncost.text = cost
            if dataModels?.mobilenumber! == OnboardUser.mobileNo{
                viewcheckbox.isHidden = true
            }else{
                viewcheckbox.isHidden = true
            }
            
            if switchfav.isOn == true{
                nominate = "Yes"
            }
            else {
                nominate = "No"
            }
        }
        else if CurrentFlow.flowType == FlowType.BuyGoods{
            lblactype.text = dataModels?.acalias!
            lblmobile.text = dataModels?.mobilenumber! ?? ""
            lblmyself.text = "TillNumber".localized(Session.sharedInstance.appLanguage)
            lblamount.text = dataModels?.amount!
            lblbuyfrom.text = "PayFrom".localized(Session.sharedInstance.appLanguage)
            // Transaction Cost :-
            let cost = getTransectionCostFor(service_code: "ACCTILL", amount: dataModels?.amount! ?? "0")
            lbltransactioncost.text = cost
            if dataModels?.mobilenumber! == OnboardUser.mobileNo{
                viewcheckbox.isHidden = true
            }else{
                viewcheckbox.isHidden = true
            }
            
            if switchfav.isOn == true{
                nominate = "Yes"
            }
            else {
                nominate = "No"
            }
            
        } else {
            
        }
        
        
       
        
        TotelAmount =  TotelCharges(percent: global.percent, charge:removeKES(price: lbltransactioncost.text ?? "0"))
        
        lbltransactioncost.text = "KES \(TotelAmount)"
        
        if lbltransactioncost.text == "KES 0" || lbltransactioncost.text == "KES 0 " || lbltransactioncost.text == "KES 0.0" || lbltransactioncost.text == "KES 0.0 " {
            lblcostmsg.isHidden = true
        }
        else {
            if CurrentFlow.flowType != FlowType.BuyAirtime{
                lblcostmsg.isHidden = false
            }
            else {
                lblcostmsg.isHidden = true
            }
        }
      }
    
    func btmview (hide:Bool)  {
        
        viewbeneficialy.isHidden = hide
        txtbenificially.isHidden = hide
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        let (header, reviewTitle, buttonTitle) = (CurrentFlow.flowType?.getReviewData())!
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 17, color: Color.white, title: header, placeHolder: "")
        Fonts().set(object: self.lblbuyairtimefor, fontType: 1, fontSize: 9, color: Color.c0_123_255, title: reviewTitle, placeHolder: "")
        Fonts().set(object: self.lblmyself, fontType: 1, fontSize: 13, color: Color.black, title: "MPESA", placeHolder: "")
        Fonts().set(object: self.lblmobile, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: self.lblamounttitle, fontType: 1, fontSize: 13, color: Color.black, title: "Amount", placeHolder: "")
        Fonts().set(object: self.lblamount, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: self.lblAddFav, fontType: 1, fontSize: 13, color: Color.black, title: "ADDFAV", placeHolder: "")
        Fonts().set(object: self.lblactype, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: self.lbltransactioncosttitle, fontType: 1, fontSize: 13, color: Color.black, title: "Transactionalcost", placeHolder: "")
        Fonts().set(object: self.lblcostmsg, fontType: 0, fontSize: 11, color: Color.c160_166_173, title: "TRANSACTIONMSG", placeHolder: "")
        Fonts().set(object: self.lbltransactioncost, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: self.lblbuyfrom, fontType: 1, fontSize: 13, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: txtbenificially, fontType: 0, fontSize: 15, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: buttonTitle, placeHolder: "")
        
        if CurrentFlow.flowType == FlowType.SendMoney{
            Fonts().set(object: self.lblbuyfrom, fontType: 1, fontSize: 13, color: Color.black, title: "TRANSFERMONEYFROM1", placeHolder: "")
        }
        else if  CurrentFlow.flowType == FlowType.MpesaToAccount{
            Fonts().set(object: self.lblAddFav, fontType: 1, fontSize: 13, color: Color.black, title: "FAVSAVE", placeHolder: "")
            Fonts().set(object: self.lblbuyfrom, fontType: 1, fontSize: 13, color: Color.black, title: "TransferTo", placeHolder: "")
            Fonts().set(object: self.lblcostmsg, fontType: 0, fontSize: 11, color: Color.c160_166_173, title: "TRANSACTIONMPESA", placeHolder: "")
        }
        else if  CurrentFlow.flowType == FlowType.PesaLinkToAc{
            Fonts().set(object: self.lblAddFav, fontType: 1, fontSize: 13, color: Color.black, title: "ADDFAV", placeHolder: "")
            Fonts().set(object: self.lblbuyfrom, fontType: 1, fontSize: 13, color: Color.black, title: "TRANSFERMONEYFROM", placeHolder: "")
        }
        else if  CurrentFlow.flowType == FlowType.PesaLinkToPhone{
            Fonts().set(object: self.lblAddFav, fontType: 1, fontSize: 13, color: Color.black, title: "ADDFAV", placeHolder: "")
            Fonts().set(object: self.lblbuyfrom, fontType: 1, fontSize: 13, color: Color.black, title: "TRANSFERMONEYFROM1", placeHolder: "")
            
        }
        else if  CurrentFlow.flowType == FlowType.PesaLinkToCard{
            Fonts().set(object: self.lblAddFav, fontType: 1, fontSize: 13, color: Color.black, title: "ADDFAV", placeHolder: "")
            Fonts().set(object: self.lblbuyfrom, fontType: 1, fontSize: 13, color: Color.black, title: "TRANSFERMONEYFROM1", placeHolder: "")
        }
        else if  CurrentFlow.flowType == FlowType.InternalFundTransfer{
            Fonts().set(object: self.lblbuyfrom, fontType: 1, fontSize: 13, color: Color.black, title: "BuyFrom", placeHolder: "")
        }
        else if  CurrentFlow.flowType == FlowType.BuyAirtime{
            viewbeneficialy.isHidden = true
            viewcheckbox.isHidden = true
            
        }
        else if  CurrentFlow.flowType == FlowType.PayBillTransaction{
            
            viewcheckbox.isHidden = true
            viewbeneficialy.isHidden = true
        }
        else if  CurrentFlow.flowType == FlowType.BuyGoods{
            viewcheckbox.isHidden = true
            viewbeneficialy.isHidden = true
        }
        else {
            Fonts().set(object: self.lblbuyfrom, fontType: 1, fontSize: 13, color: Color.black, title: "", placeHolder: "")
        }
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // Switch Action Value Changed
    @IBAction func switchValueChanged(sender: UISwitch) {
        if switchfav.isOn {
            txtbenificially.isUserInteractionEnabled = true
            nominate = "Yes"
            btmview(hide: false)
            txtbenificially.becomeFirstResponder()
        } else {
            txtbenificially.isUserInteractionEnabled = false
            self.view.endEditing(true)
            btmview(hide: true)
            nominate = "No"
            txtbenificially.text = ""
        }
    }
    
    
    @IBAction func countinue(_ sender: AttributedButton) {
        if CurrentFlow.flowType == FlowType.BuyAirtime || CurrentFlow.flowType == FlowType.SendMoney || CurrentFlow.flowType == FlowType.PesaLinkToAc || CurrentFlow.flowType == FlowType.PesaLinkToCard || CurrentFlow.flowType == FlowType.PesaLinkToPhone  {
            if !checkNominate(){
                if lblmyself.text == self.mySelf{
                    self.txtbenificially.text = lblmyself.text
                }
               validate()
               // validAuth()
            }
        }else{
           validate()
              //validAuth()
        }
    }
    
    func checkNominate() -> Bool{
        
        
        if nominate == "Yes"{
            if txtbenificially.text == "" {
              //  self.showMessage(title: "", message: "Please add benificially text.")
                return false
            }else if nominate == "No"{
                self.txtbenificially.text = ""
                return true
            }
        }
        return false
    }
    
}

// BY A Time FLow API Data
extension CheckoutAirTimeViewController {
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        //let resultArray: Result?
//        let accountalias: Int?
//        let amount: String?
//        let biller: Int?
//        let billerref: Int?
//        let refrenceid: String?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
//            case accountalias = "ACCOUNT_ALIAS"
//            case amount = "AMOUNT"
//            case biller = "BILLER"
//            case billerref = "BILLER_REFERENCE"
//            case refrenceid = "REFERENCE_ID"
           }
    }
    
    func buyaTimeTransaction(){
        
    //    let amount = lblamount.text!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        let amount = removeKES(price: lblamount.text!)
        
        RestAPIManager.buyAirtime(title: "PLEASEWAIT", subTitle: "PLEASEWAITTRANSACTIONS", pin: OnboardUser.pin, type: Response.self, mobileNo:OnboardUser.mobileNo, accountAlias: lblactype.text!, amount: amount, recipetno: "\(lblmobile.text!)", mnoWalletId:(dataModels?.aMNO_WALLET_ID!)!, nominate: nominate, reciptAlias: txtbenificially.text!, completion: { (response) in
            DispatchQueue.main.async {
                if response.statusCode == 200 {
                    
                    let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                    nextvc.dataModels = self.dataModels
                    nextvc.message = response.statusMessage ?? ""
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    //  self.showMessage(title: "", message: response.statusMessage ?? "")
                    let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                    nextvc.message = response.statusMessage ?? ""
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                
            }
        }) { (error) in
         
        }
    }
}

//Text field delegate:-
extension CheckoutAirTimeViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
}

// SEND A MONEY API DATA
extension CheckoutAirTimeViewController {
    
    // Model : -
    struct SendMoneyResponse: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let refrenceId: String?
        let acalias: String?
        let mnowalletid: String?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case refrenceId = "REFERENCE_ID"
            case acalias = "ACCOUNT_ALIAS"
            case mnowalletid = "MNO_WALLET_ID"
        }
    }
    
    // Model : -
    struct MpesaResponse: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let refrenceId: String?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case refrenceId = "REFRENCE_ID"
            
        }
    }
    // ------------------------------------------------------------------------------------
    // Send to Mobile Money
    func sendMoneytransactions(){
     //   let amount = lblamount.text!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range: nil)
        
         let amount =  removeKES(price: lblamount.text!)
        
        RestAPIManager.sendtomobilemoney(title: "PLEASEWAIT", subTitle: "PLEASEWAITTRANSACTIONS", pin: global.enterpin, type: SendMoneyResponse.self, mobileNo: OnboardUser.mobileNo, accountAlias: lblactype.text!, amount: amount, mnowalletid: dataModels?.aMNO_WALLET_ID ?? "", recipientno: "\(lblmobile.text!)", recipientalias: txtbenificially.text!, nominateflag: nominate,completion: { (response) in
            DispatchQueue.main.async {
             
                if response.statusCode == 200 {
                    let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                    nextvc.message = response.statusMessage ?? ""
                    nextvc.dataModels = self.dataModels
                    nextvc.recieptNo = response.refrenceId ?? ""
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    // self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                    let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                    nextvc.message = response.statusMessage ?? ""
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
            }
        }) { (error) in
            
        }
    }
    
    // -------------------------------------------------------------------------------------------
    // Mpesa STK Push  mpesa to account
    func mPesaToAccount(){
        let amount = removeKES(price: lblamount.text!)
        
        let acno =  global.acnumber
        
        RestAPIManager.mPesaToaccount(title: "PLEASEWAIT", subTitle: "PLEASEWAITTRANSACTIONS", pin:  global.enterpin, type: MpesaResponse.self, mobileNo: OnboardUser.mobileNo, accountAlias: lblactype.text!, amount: amount, accountnumber: acno ,completion: { (response) in
            DispatchQueue.main.async {
                
                if response.statusCode == 200{
                    let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                    nextvc.message = response.statusMessage ?? ""
                    nextvc.dataModels = self.dataModels
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    // self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                    //SuccessViewController
                    let nextvc = SuccessViewController.getVCInstance() as! SuccessViewController
                    nextvc.msg = response.statusMessage ?? ""
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
            }
        }) { (error) in
            
        }
        
    }
    
    // -------------------------------------
    // Pay BILL
    func paybill(){
        
          let loader = RestAPIManager.addProgress(title: "PLEASEWAIT", description: "WEAREPROCESSINGYOURREQUEST")
        
        let amounts = removeKES(price: (paybilldataModels1?.amount!)!)
        RestAPIManager.paybill(title: "PLEASEWAIT", subTitle: "PLEASEWAITTRANSACTIONS", billerrefrence: (paybilldataModels1?.cardnumber)!, type: Response.self, mobileNo: OnboardUser.mobileNo,accountalias: (paybilldataModels1?.acalias!)!, amount: amounts, pin:  global.enterpin,biller:(paybilldataModels1?.biller!)!, enrollbillrefrence: nominate, enrollmenalias: txtbenificially.text ?? "",completion: { (response) in
            DispatchQueue.main.async {
                  loader.remove()
              
                if response.statusCode == 200
                {
                    let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                    nextvc.message = response.statusMessage ?? ""
                    
                    let dataModel = BuyaTimeVM.init(mobilenumber: self.paybilldataModels1?.cardnumber ?? "", mobilename: self.paybilldataModels1?.acalias ?? "", acalias: self.paybilldataModels1?.acalias ?? "", aMNO_WALLET_ID: "", amount: self.paybilldataModels1?.amount ?? "", acnumber: self.paybilldataModels1?.acnumber ?? "", nominateflag: self.paybilldataModels1?.nominateFlag ?? "")
                    nextvc.dataModels = dataModel
                   // nextvc.recieptNo = response.billerref ?? ""
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    // self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                    let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                    nextvc.message = response.statusMessage ?? ""
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
            }
        }) { (error) in
           
        }
    }
    
    
    // ------------------------------------------
    // Internal Fund Transfer:-
    func rtgs_internalFundTransfer(command : String, reciptBank : String){
        var reciepnt_alias = ""
        
        if nominate == "Yes" {
          reciepnt_alias = txtbenificially.text ?? ""
        }
        else {
          reciepnt_alias = ((internalfundModel?.anominatedaccount!)!)
        }
        
        let amount = removeKES(price: (internalfundModel?.amount!)!)
        
        RestAPIManager.Internalfuntranfer(title: "PLEASEWAIT",
                                          subTitle: "PLEASEWAITTRANSACTIONS", scommand : command,
                                          acalias:(internalfundModel?.acalias!)!,
                                          type: IntenalFundVM.Response.self,
                                          mobileNo: OnboardUser.mobileNo,
                                          pin:  global.enterpin,
                                          amount: amount,
                                          recieptacc: internalfundModel?.anominatedaccount ?? "",
                                          recieptBankbranch: (internalfundModel?.dictbank["aBRANCH_CODE"] ?? ""), // Only for ift
            nominate: nominate,
            recieptalias: txtbenificially.text ?? "", // If fav
            reciptBank: reciptBank,
            recieptacname:((internalfundModel?.anominatealias!)!),
            narration : internalfundModel?.narration ?? "", address: recipientAddress,
            completion: { (response) in
                DispatchQueue.main.async {
                    if response.statusCode == 200 {
                        let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                        
                        let dataModel = BuyaTimeVM.init(mobilenumber: ((self.internalfundModel?.anominatedaccount!)!), mobilename: (self.internalfundModel?.acalias!)!, acalias: (self.internalfundModel?.acalias!)!, aMNO_WALLET_ID: "", amount: self.internalfundModel?.amount!, acnumber: "", nominateflag: self.internalfundModel!.nominateflag!)
                        nextvc.message = response.statusMessage ?? ""
                        nextvc.dataModels = dataModel
                        nextvc.recieptNo = response.reffrenceid ?? ""
                        
                        self.navigationController?.pushViewController(nextvc, animated: true)
                    }
                    else{
                        //    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                        let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                        nextvc.message = response.statusMessage ?? ""
                        self.navigationController?.pushViewController(nextvc, animated: true)
                    }
                }
        }) { (error) in
           
        }
    }
    
    // --------------------------------
    // Pesalink - Send to Account
    func SendToAccount(){
      //  let amount = internalfundModel?.amount!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range:nil)
        
        
        let amount = removeKES(price: (internalfundModel?.amount!)!)
        
        var bankname = ""
        if (internalfundModel?.dictbank["aBANK_NAME"]) != nil {
         bankname = internalfundModel?.dictbank["aBANK_NAME"] ?? ""
        }
        else {
            bankname = internalfundModel?.dictbank["aBANK"] ?? ""
        }
        RestAPIManager.sendToAccount(title: "PLEASEWAIT", subTitle: "PLEASEWAITTRANSACTIONS",
                                     acalias: ((internalfundModel?.acalias!)!),
                                     type: PesaLinkVM.Response.self,
                                     mobileNo: OnboardUser.mobileNo,
                                     pin: global.enterpin,
                                     amount: amount,
                                     recieptacnumber: internalfundModel!.anominatedaccount!,
                                     nominatealias: txtbenificially.text ?? "",
                                     bank: bankname,
                                     narration: reasonForPayment,
                                     nominate: nominate,
                                     completion: { (response) in
                                        DispatchQueue.main.async {
                                            
                                            if response.statusCode == 200 {
                                                let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                                                
                                                let model = BuyaTimeVM.init(mobilenumber: OnboardUser.mobileNo, mobilename:"", acalias: (self.internalfundModel?.acalias!)!, aMNO_WALLET_ID: "", amount: amount, acnumber: ((self.internalfundModel?.anominatedaccount!)!), nominateflag: "")
                                                nextvc.dataModels = model
                                                nextvc.message = response.statusMessage
                                                nextvc.recieptNo = response.refrenceId ?? ""
                                                self.navigationController?.pushViewController(nextvc, animated: true)
                                            }
                                            else{
                                                //   self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                                                let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                                                nextvc.message = response.statusMessage ?? ""
                                                self.navigationController?.pushViewController(nextvc, animated: true)
                                            }
                                        }
        }) { (error) in
            
        }
    }
    
    // --------------------------------------------------
    // Pesalink - Send to CARD
    func SendToCard(){   // TODO CARDNUMBER
       // let amount = internalfundModel?.amount!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range:nil)
        
            let amount = removeKES(price: (internalfundModel?.amount!)!)
        
        RestAPIManager.sendToCard(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOUR", acalias: ((internalfundModel?.acalias!)!), type: PesaLinkVM.Response.self, mobileNo: OnboardUser.mobileNo, pin: global.enterpin, amount: amount,nominate: nominate,cardnumber : internalfundModel!.anominatedaccount!,narration:reasonForPayment,nominatealias:txtbenificially.text ?? "", bank: (internalfundModel?.dictbank["aBANK"])!,completion: { (response) in
            DispatchQueue.main.async {
                
                if response.statusCode == 200 {
                    let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                    let model = BuyaTimeVM.init(mobilenumber: OnboardUser.mobileNo, mobilename:"", acalias: (self.internalfundModel?.acalias!)!, aMNO_WALLET_ID: "", amount: amount, acnumber: ((self.internalfundModel?.anominatedaccount!)!), nominateflag: "")
                    nextvc.dataModels = model
                    nextvc.message = response.statusMessage
                    nextvc.recieptNo = response.refrenceId ?? ""
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    // self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                    let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                    nextvc.message = response.statusMessage ?? ""
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
            }
        }) { (error) in
           
        }
    }
    
    // ----------------------------------------------------------
    // Pesalink - Send to CARD
    func SentToPhone(){
       // let amount = internalfundModel?.amount!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range:nil)
        // TODO receipentmobnumber , banksortcode ,
        
        let amount = removeKES(price: (internalfundModel?.amount!)!)
        
        RestAPIManager.sendToPhone(title : "PLEASEWAIT", subTitle : "We are validate your information", acalias : (internalfundModel?.acalias!)!,type: PesaLinkVM.Response.self, mobileNo : OnboardUser.mobileNo,pin :  global.enterpin,amount : amount,recieptbank : (internalfundModel?.dictbank["aBANK"])!,nominate : nominate,banksortcode : (internalfundModel?.dictbank["banksortcode"])!,narration:reasonForPayment,nominatealias:txtbenificially.text ?? "",receipentmobnumber:internalfundModel!.anominatedaccount!,
                                   completion: { (response) in
                                    DispatchQueue.main.async {
                                      
                                        if response.statusCode == 200 {
                                            let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                                            
                                            let model = BuyaTimeVM.init(mobilenumber: OnboardUser.mobileNo, mobilename:"", acalias: (self.internalfundModel?.acalias!)!, aMNO_WALLET_ID: "", amount: amount, acnumber: ((self.internalfundModel?.anominatedaccount!)!), nominateflag: "")
                                            nextvc.dataModels = model
                                            nextvc.recieptNo = response.refrenceId ?? ""
                                            nextvc.message = response.statusMessage
                                            
                                            self.navigationController?.pushViewController(nextvc, animated: true)
                                        }
                                        else{
                                            //    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                                            let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                                            nextvc.message = response.statusMessage ?? ""
                                            self.navigationController?.pushViewController(nextvc, animated: true)
                                        }
                                    }
        }) { (error) in
         
        }
    }
    
    // TO DO : Add 2 parameters value
    func buyGoods()
    {
        let amount = removeKES(price: dataModels?.amount! ?? "")
        
        RestAPIManager.buyGoods(title: "PLEASEWAIT", subTitle: "PLEASEWAITTRANSACTIONS", type: BuyGoodsVM.Response.self, mobileNo: OnboardUser.mobileNo , accountalias: dataModels?.acalias! ?? "", pin:  global.enterpin, amount: amount, tillNumber: dataModels?.mobilenumber! ?? "", enrollAccount: "", enrollAlias: "", completion: { (response) in
            DispatchQueue.main.async {
                
                
                if response.statusCode == 200 {
                    let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                    
                    let model = BuyaTimeVM.init(mobilenumber: OnboardUser.mobileNo, mobilename:"", acalias: (self.dataModels!.acalias!), aMNO_WALLET_ID: "", amount: amount, acnumber: ((self.dataModels!.acnumber!)), nominateflag: "")
                    nextvc.dataModels = model
                    nextvc.message = response.statusMessage
                    nextvc.recieptNo = response.refrenceid ?? ""
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    // self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                    let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                    nextvc.message = response.statusMessage ?? ""
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                
            }
        }) { (error) in
           
        }
    }
    
    
    //MARK: Pay bill Transaction
    func PayBillTransaction()  {
      //  let amount = dataModels?.amount!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range:nil)
        let amount = removeKES(price: dataModels?.amount! ?? "")
        
        RestAPIManager.payBillTransact(title: "PLEASEWAIT", subTitle: "PLEASEWAITTRANSACTIONS", type: BuyGoodsVM.Response.self, mobileNo:OnboardUser.mobileNo , accountalias: (dataModels?.acalias!)!, pin:  global.enterpin, amount: amount, paybillNumber:dataModels?.mobilenumber! ?? "", paybillAccount: (dataModels?.mobilename)!, enrollAccount: "", enrollAlias: "",completion: { (response) in
            DispatchQueue.main.async {
                
                if response.statusCode == 200 {
                    let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                    
                    let model = BuyaTimeVM.init(mobilenumber: OnboardUser.mobileNo, mobilename:"", acalias: (self.dataModels!.acalias!), aMNO_WALLET_ID: "", amount: amount, acnumber: ((self.dataModels!.acnumber!)), nominateflag: "")
                    nextvc.dataModels = model
                    nextvc.message = response.statusMessage
                    nextvc.recieptNo = response.refrenceid ?? ""
                    
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    //  self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                    let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                    nextvc.message = response.statusMessage ?? ""
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                
            }
        }) { (error) in
       
        }
    }
}

// VALIDATE FACE AND PIN :_
extension CheckoutAirTimeViewController:AuthDelegate {
    
    func validate() {
        //Check if user enable biometric access
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            let context = LAContext()
            Authntication(context).authnticateUser { (success, message) in
                DispatchQueue.main.async {
                    if success == true {
                        global.enterpin = OnboardUser.pin
                        self.validAuth()
                    }
                    else {
                        if message != "" {
                             self.showMessage(title: "", message: message)
                        }
                    }
                    
                }
            }
        }
        else {
            global.enterpin = ""
            let vc = AuthoriseViewController.getVCInstance() as! AuthoriseViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func validAuth() {
        
        if !isInternetAvailable(){
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        if CurrentFlow.flowType == FlowType.SendMoney{
            sendMoneytransactions()
        }
        else if CurrentFlow.flowType == FlowType.BuyAirtime{
            buyaTimeTransaction()
        }
        else if CurrentFlow.flowType == FlowType.PayBill{
            paybill()
        }
        else if CurrentFlow.flowType == FlowType.InternalFundTransfer{
            let recptBranch = internalfundModel?.dictbank["aBANK_BRANCH"] ?? ""
            rtgs_internalFundTransfer(command: sCommand.TF, reciptBank: recptBranch)
        }
        else if CurrentFlow.flowType == FlowType.Rtgs{
            let recptBank = internalfundModel?.dictbank["aBANK_CODE"] ?? ""
            rtgs_internalFundTransfer(command: sCommand.RTGS, reciptBank: recptBank)
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToAc{
            SendToAccount()
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToCard{
            SendToCard()
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToPhone{
            SentToPhone()
        }
        else if CurrentFlow.flowType == FlowType.MpesaToAccount{
            mPesaToAccount()
        }
        else if CurrentFlow.flowType == FlowType.PayBillTransaction{
            PayBillTransaction()
        }
        else if CurrentFlow.flowType == FlowType.BuyGoods{
            buyGoods()
        }
        else{
            
        }
    }
}


extension CheckoutAirTimeViewController {
    
    func getTransectionCostFor(service_code : String,amount:String) -> String {
        
        let array = CoreDataHelper().getDataForEntity(entity: Entity.SERVICES_INFO)
        for i in 0 ..< array.count {
            
            if array[i]["aSERVICE_CODE"] == service_code {
                if let dict = convertToDictionary(text: array[i]["aCHARGE"]!){
                    
                    let keys = Array(dict.keys)
                    for i in 0 ..< keys.count {
                        let array = keys[i]
                        
                        if array.contains(":"){
                            var arr = array.components(separatedBy: ":")
                            
                            let value1 =  (arr[0] as NSString).integerValue
                            let value2 =  (arr[1] as NSString).integerValue
                            let prices = removeKES(price: amount)
                            let price =  (prices as NSString).integerValue
                            
                            
                            if price >= value1 && price <= value2 {
                                let tranCost = dict[array]
                                return tranCost ?? "0"
                            } else {
                                if i == keys.count - 1 {
                                    return "0"
                                }
                            }
                            
                        }
                        else {
                            let keyindex = keys[0]
                            let tranCost = dict[keyindex]
                            return tranCost ?? "0"
                        }
                    }
                }
                else
                {
                    let transactionCost = array[i]["aCHARGE"] ?? "0"
                    return transactionCost
                }
            }
            
        }
        return "0"
    }
    
    // MARK:_ Credit Card COST:_
    
    func getCreditcardCostFor(amount:String) -> String {
        
        for i in 0 ..< global.cardInfo.count {
            let dict = global.cardInfo[i]
            if global.creditCardID == dict["ID"] as! String {
                let newdict = dict["Charge"] as! NSDictionary
                let keys = Array(newdict.allKeys)
                for i in 0 ..< keys.count {
                    let array = keys[i]
                    
                    if (array as AnyObject).contains(":"){
                        var arr = (array as AnyObject).components(separatedBy: ":")
                        
                        let value1 = (arr[0] as NSString).integerValue
                        let value2 = (arr[1] as NSString).integerValue
                        let prices = removeKES(price: amount)
                        let price = (prices as NSString).integerValue
                        
                        
                        if price >= value1 && price <= value2 {
                            let tranCost = newdict[array]
                            return "\(tranCost ?? "0")"
                        }
                        else {
                            return "0"
                        }
                    }
                    else {
                        let keyindex = keys[0]
                        let tranCost = newdict[keyindex]
                        return "\(tranCost ?? "0")"
                    }
                }
            }
            
        }
        return ""
    }
    
//    func getTransectionCostFor(service_code : String,amount:String,acno:String) -> String {
//
//        let array = CoreDataHelper().getDataForEntity(entity: Entity.SERVICES_INFO)
//        for i in 0 ..< array.count {
//
//            if array[i]["aSERVICE_CODE"] == service_code {
//             //   if let dict = convertToDictionary(text: array[i]["aCHARGE"]!){
//
//                let dict = array[i]["aCHARGES"]!
//                let data = convertToDictionary1(text:dict)
//
//                if let dictcharges = data?[acno] as? [String:String]{
//
//                    let keys = Array(dictcharges.keys)
//                    for i in 0 ..< keys.count {
//                        let array = keys[i]
//
//                        if array.contains(":"){
//                            var arr = array.components(separatedBy: ":")
//
//                            let value1 =  (arr[0] as NSString).integerValue
//                            let value2 =  (arr[1] as NSString).integerValue
//                            let prices = removeKES(price: amount)
//                            let price =  (prices as NSString).integerValue
//
//
//                            if price >= value1 && price <= value2 {
//                                let tranCost = dictcharges[array]
//                                return tranCost ?? "0"
//                            } else {
//                                if i == keys.count - 1 {
//                                     return "0"
//                                }
//                            }
//
//                        }
//                        else {
//                            let keyindex = keys[0]
//                            let tranCost = dictcharges[keyindex]
//                            return tranCost ?? "0"
//                        }
//                    }
//                }
//                else
//                {
//                    let transactionCost = array[i]["aCHARGE"] ?? "0"
//                    return transactionCost
//                }
//            }
//
//        }
//        return "0"
//    }
//
//    // MARK:_ Credit Card COST:_
//
//    func getCreditcardCostFor(amount:String) -> String {
//
//        for i in 0 ..< global.cardInfo.count {
//            let dict = global.cardInfo[i]
//            if global.creditCardID == dict["ID"] as! String {
//                let newdict = dict["Charge"] as! NSDictionary
//                let keys = Array(newdict.allKeys)
//                for i in 0 ..< keys.count {
//                    let array = keys[i]
//
//                    if (array as AnyObject).contains(":"){
//                        var arr = (array as AnyObject).components(separatedBy: ":")
//
//                        let value1 = (arr[0] as NSString).integerValue
//                        let value2 = (arr[1] as NSString).integerValue
//                        let prices = removeKES(price: amount)
//                        let price = (prices as NSString).integerValue
//
//
//                        if price >= value1 && price <= value2 {
//                            let tranCost = newdict[array]
//                            return "\(tranCost ?? "0")"
//                        }
//                        else {
//                            return "0"
//                        }
//                    }
//                    else {
//                        let keyindex = keys[0]
//                        let tranCost = newdict[keyindex]
//                        return "\(tranCost ?? "0")"
//                    }
//                }
//            }
//
//        }
//        return ""
//    }
    
 
}

// COnvert String to Int : -
extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}
