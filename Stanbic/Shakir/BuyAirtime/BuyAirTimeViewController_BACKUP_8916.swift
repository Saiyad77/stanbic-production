//
//  BuyAirTimeViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 20/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit


class PricebuttonCell: UICollectionViewCell {
    
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var viewbg: UIView!
    
   
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = 4.0
        layer.shadowRadius = 4
        layer.shadowOpacity = 0.16
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.masksToBounds = false
        viewbg.layer.borderColor = Color.c134_142_150.cgColor
        viewbg.layer.borderWidth = 0.5
    }
}

class BuyAirTimeViewController: UIViewController,UIGestureRecognizerDelegate {
   
    //Mark:- IBOUTLETS
    @IBOutlet weak var txtmobile: UITextField!
    @IBOutlet weak var txtamount: UITextField!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblselectnetwork: UILabel!
    @IBOutlet weak var Clsprice: UICollectionView!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lblbuyfrom: UILabel!
    @IBOutlet weak var lblHowMuchTopup: UILabel!
    @IBOutlet weak var btnNetwork: UIButton!
    @IBOutlet weak var viewNetwork: UIView!
    @IBOutlet weak var txtNetworkProvider: UITextField!
    @IBOutlet weak var constlblactitle: NSLayoutConstraint!
    //Mark:- Varibales
   
    private var lastContentOffset: CGFloat = 0
    let Pricearr = ["100", "250","500","1000"]
    var aMNO_WALLET_ID = ""
    var isMobileEditing : Bool = true
    var bottomSelectedIndx = -1

    var indexcolletion = IndexPath()
    
    var contactno = ""
    var contactname = ""
    var selectnetworok = true
    var nominateflag = ""
    var mySelf = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        Clsprice?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
        setFonts()
        
        txtamount.delegate = self
        txtmobile.text = contactno
        txtamount.text = "KES "
               txtamount.keyboardType = .numberPad
        
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            lblaccount.text = acdata[0]["aACCOUNT_ALIAS"]!
            lblaccountnumber.text = acdata[0]["aACCOUNT_NUMBER"]!.hashingOfTheAccount(noStr: acdata[0]["aACCOUNT_NUMBER"]!)
        }
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isMobileEditing == true {
            txtmobile.isEnabled = true
            txtamount.didBegin()
            txtmobile.didBegin()
            txtmobile.becomeFirstResponder()
        }
        else {
            txtmobile.isEnabled = false
            txtamount.becomeFirstResponder()
            txtmobile.didBegin()
        }
        
        self.tabBarController?.tabBar.isHidden = true
     }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
   
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lblHowMuchTopup, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "HOWMUCHTOPUP", placeHolder: "")
        //Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: "BuyAIRTIME", placeHolder: "")
        Fonts().set(object: self.txtmobile, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtamount, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        
        txtamount.addUI(placeholder: "ENTERAMOUNT")
        txtmobile.addUI(placeholder: "ENTERMOBPLACEHOLDER1")

        Fonts().set(object: self.lblselectnetwork, fontType: 0, fontSize: 13, color: Color.c134_142_150, title: "SELECTNETWORKPROVIDER", placeHolder: "")
        
        Fonts().set(object: self.lblbuyfrom, fontType: 1, fontSize: 9, color: Color.c0_123_255, title: "BUYFROM", placeHolder: "")
        
        Fonts().set(object: self.lblaccount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "", placeHolder: "")
        
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        
        initialSetUpNavigationBar()
    }
    
    // MARK: - Required Method's
    func initialSetUpNavigationBar(){
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = "BuyAIRTIME".localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func networkprovider(_ sender: AttributedButton) {
        view.endEditing(true)
        selectnetworok = true
        self.showBottomList()
     }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        
        if txtmobile.text == ""{
            txtmobile.showError(errStr: "ENTERMOBPLACEHOLDER")
        }
        else if txtamount.text == "KES " || txtamount.text == "KES"{
            txtamount.showError(errStr: "ENTERAMOUNT")
        }
        else if lblselectnetwork.text == "Select Network Provider"{
            showMessage(title: "", message: "SELECTNETWORKPROVIDER")
          //  txtNetworkProvider.showError(errStr: "SELECTNETWORKPROVIDER")
        }
        else if lblaccountnumber.text == ""{
            showMessage(title: "", message: "SELECTACCOUNT")
        }
        else if getNetworkMobileValidate(mnoName: lblselectnetwork.text!, mob: txtmobile.text!) == false{
          //  showMessage(title: "", message: "NOTVALIDNUMBER")
            txtmobile.showError(errStr: "NOTVALIDNUMBER")
        }
        else{
            
            let dataModels = BuyaTimeVM.init(mobilenumber: txtmobile.text!, mobilename:  contactname, acalias: lblaccount.text!, aMNO_WALLET_ID: aMNO_WALLET_ID, amount:txtamount.text!, acnumber: lblaccountnumber.text!,nominateflag: nominateflag)
            let nextvc = CheckoutAirTimeViewController.getVCInstance() as! CheckoutAirTimeViewController
            nextvc.dataModels = dataModels
            nextvc.mySelf = mySelf
            self.navigationController?.pushViewController(nextvc, animated: true)
         }
     }
    
    @IBAction func accountselect(_ sender: AttributedButton) {
        view.endEditing(true)
        selectnetworok = false
       // self.showBottomSheetVC(xibtype: "ACCOUNT", Selectname: lblaccount.text!)
    }
    
    func checkValidMobileNo() -> Bool {
        
        if self.txtmobile.text?.count == 10 {
            
            let prefix = String((self.txtmobile.text?.prefix(2))!)
            if prefix != "07" {
                self.showMessage(title: "", message: "INVALIDMOB")
                return false
            }
            return true
            
        }
        else if self.txtmobile.text?.count == 9 {
            let prefix = String((self.txtmobile.text?.prefix(1))!)
            if prefix == "0" {
                return false
            }
            else if prefix != "7" {
                //self.showMessage(title: "", message: "INVALIDMOB2")
                txtmobile.showError(errStr: "INVALIDMOB2")
                return false
            }
            
            return true
        }
        else {
            return false
        }
    }

<<<<<<< HEAD
=======
    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
       // bottomSheetVC!.view.frame.roundCorners(corners: [.topLeft, .topRight], radius: 3.0)
    }
    
   
    func showBottomSheetVC(xibtype: String,Selectname: String) {
        
        var height = 300  // 340
        let coreData = CoreDataHelper()
        if xibtype == "ACCOUNT" {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
                height = acdata.count * 130
                if acdata.count == 1 {
                    height = 130
                }
            }
        }
        else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acnetwork  = coreData.getDataForEntity(entity: Entity.MNO_INFO)
                height = acnetwork.count * 130
            }
        }
        
        // bottomSheetVC?.partialView = UIScreen.main.bounds.height - CGFloat(height)
        bottomSheetVC!.partialView = self.view.frame.size.height - CGFloat(height)
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imgView.backgroundColor = Color.black
        imgView.alpha = 0.2
        imgView.isUserInteractionEnabled = true
        self.view.addSubview(imgView)
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "", SelectName: "")
        imgView.removeFromSuperview()
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
>>>>>>> 3ed599046bfe4950d9cf59591a00a64478de6630
}

//Text field delegate
extension BuyAirTimeViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtamount{
            txtamount.text = "KES "
            indexcolletion = IndexPath()
            Clsprice.reloadData()
         }
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtamount {
        
        textField.typingAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
        let protectedRange = NSMakeRange(0, 4)
        let intersection = NSIntersectionRange(protectedRange, range)
        if intersection.length > 0 {
            
            return false
        }
        return true
        }
        else{
            return true
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

//Text field delegate
extension BuyAirTimeViewController : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Pricearr.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PricebuttonCell", for: indexPath as IndexPath) as! PricebuttonCell
        
        if indexcolletion == indexPath {
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.white, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = Color.c0_123_255
        }else{
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = .clear
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        indexcolletion = indexPath
        txtamount.text = "KES \(self.Pricearr[indexPath.row])"
        txtamount.removeError()
        txtamount.didBegin()
        Clsprice.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.Clsprice.frame.width/4) - 8
        return CGSize(width: width, height: collectionView.frame.size.height - 5)
    }
}

extension BuyAirTimeViewController : SelectNetworkDelegate{
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
     lblaccountnumber.text =  acnumber
     lblaccount.text = acname
    }
    
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
<<<<<<< HEAD
=======
        hideBottomSheetVC()
        txtNetworkProvider.removeError()
>>>>>>> 3ed599046bfe4950d9cf59591a00a64478de6630
        if selectnetworok == true {
        Fonts().set(object: self.lblselectnetwork, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: newtorkname, placeHolder: "")
            self.aMNO_WALLET_ID = networkid
        }
        else{
          lblaccount.text = newtorkname
          lblaccountnumber.text = networkid.hashingOfTheAccount(noStr: networkid)
        }
    }
}

extension BuyAirTimeViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}

extension BuyAirTimeViewController: BottomListDelegate{
    func showBottomList(){
        let vc = BottomListVC.getVCInstance() as! BottomListVC
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        vc.selectedIndx = self.bottomSelectedIndx
        vc.titleString = "Select network provider"
        vc.cellIdentifier = .titleOnlyCell
        let acnetwork  = CoreDataHelper().getDataForEntity(entity: Entity.MNO_INFO)
        let networks = acnetwork.map{$0["aMNO_NAME"] ?? ""}
        vc.titleList = networks
        self.present(vc, animated: true, completion: nil)
    }
    
    func getIndx(indx: Int) {
        self.bottomSelectedIndx = indx
    }
}

