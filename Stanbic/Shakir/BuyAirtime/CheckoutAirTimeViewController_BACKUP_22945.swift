//
//  CheckoutAirTimeViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 20/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication

class CheckoutAirTimeViewController: UIViewController {
   
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblbuyairtimefor: UILabel!
    @IBOutlet weak var lblmyself: UILabel!
    @IBOutlet weak var lblmobile: UILabel!
    @IBOutlet weak var lblamounttitle: UILabel!
    @IBOutlet weak var lblamount: UILabel!
    @IBOutlet weak var lblbuyfrom: UILabel!
    @IBOutlet weak var lblactype: UILabel!
    @IBOutlet weak var lbltransactioncosttitle: UILabel!
    @IBOutlet weak var lbltransactioncost: UILabel!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var switchfav: UISwitch!
    @IBOutlet weak var lblAddFav: UILabel!
    @IBOutlet weak var viewcenter: UIView!
    @IBOutlet weak var viewcheckbox: UIView!
    // Benificially btm
    @IBOutlet weak var txtbenificially: UITextField!
    @IBOutlet weak var viewbeneficialy: UIView!
    @IBOutlet weak var consHeight: NSLayoutConstraint!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    
    var dataModels : BuyaTimeVM?
    var paybilldataModels1 : PayBillVM1?
    var nominateflag = ""
    var internalfundModel: IntenalFundVM?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtbenificially.autocorrectionType = .no
        
        btmview(hide: false)
        setFonts()
        setdata()
        self.tabBarController?.tabBar.isHidden = true
        viewcenter.addshaow(offset : CGSize(width: 2, height: 2))
        viewcheckbox.addshaow(offset : CGSize(width: -1, height: -1))
        txtbenificially.addUI(placeholder: "ENTERBENEFICIARY")
        txtbenificially.delegate = self
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        view.endEditing(true)
    }
    
    // SetData:-
    func setdata() {
        if CurrentFlow.flowType == FlowType.BuyAirtime {
            lblactype.text = dataModels?.acalias!
            lblmobile.text = dataModels?.mobilenumber! ?? ""
            lblmyself.text = dataModels?.mobilename!
            lblamount.text = dataModels?.amount!
            
            if dataModels?.nominateflag == "YES"{
                switchfav.isOn = true
            }
            else{
                switchfav.isOn = false
            }
            nominateflag = dataModels?.nominateflag! ?? ""
        }
        else if CurrentFlow.flowType == FlowType.SendMoney{
            lblactype.text = dataModels?.acalias!
            lblmobile.text = dataModels?.mobilenumber!
            lblmyself.text = "MPESA".localized(Session.sharedInstance.appLanguage)
            lblamount.text = dataModels?.amount!
            
            if dataModels?.nominateflag == "YES"{
                switchfav.isOn = true
            }
            else{
                switchfav.isOn = false
            }
            nominateflag = dataModels?.nominateflag! ?? ""
            btmview(hide: true)
        }
        else if CurrentFlow.flowType == FlowType.MpesaToAccount{
            lblactype.text = dataModels?.acalias!
            lblmobile.text = dataModels?.mobilenumber!
            lblmyself.text = "MPESA".localized(Session.sharedInstance.appLanguage)
            lblamount.text = dataModels?.amount!
            
            if dataModels?.nominateflag == "YES"{
                switchfav.isOn = true
            }
            else{
                switchfav.isOn = false
            }
            nominateflag = dataModels?.nominateflag! ?? ""
             btmview(hide: true)
        }
        else if CurrentFlow.flowType == FlowType.PayBill {
            lblactype.text = paybilldataModels1?.acalias!
            lblmobile.text = paybilldataModels1?.cardnumber!
            lblmyself.text = paybilldataModels1?.recptAccAlise!
            lblamount.text = paybilldataModels1?.amount!
            lblbuyfrom.text = "PayFrom".localized(Session.sharedInstance.appLanguage)
            
            if paybilldataModels1?.nominateFlag == "YES"{
                switchfav.isOn = true
            }
            else{
                switchfav.isOn = false
            }
            nominateflag = paybilldataModels1?.nominateFlag! ?? ""
        }
        else if CurrentFlow.flowType == FlowType.InternalFundTransfer{
            
            lblmyself.text = internalfundModel?.anominatealias
            lblmobile.text =  internalfundModel?.anominatedaccount
            lblamount.text = internalfundModel?.amount!
            lblbuyfrom.text = "Transfer money from"
            lblactype.text = internalfundModel?.acalias!
            
            if internalfundModel?.nominateflag == "YES"{
                switchfav.isOn = true
            }
            else{
                switchfav.isOn = false
            }
            nominateflag = (internalfundModel?.nominateflag!)!
         }
        else if CurrentFlow.flowType == FlowType.Rtgs{
            
            lblmyself.text = internalfundModel?.anominatealias
            lblmobile.text =  internalfundModel?.anominatedaccount
            lblamount.text = internalfundModel?.amount!
            
            lblbuyfrom.text = "TRANSFERMONEYTO".localized(Session.sharedInstance.appLanguage)
            lblactype.text = internalfundModel?.acalias!
            
            if internalfundModel?.nominateflag == "YES"{
                switchfav.isOn = true
            }
            else{
                switchfav.isOn = false
            }
            nominateflag = (internalfundModel?.nominateflag!)!
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToAc{
            
            lblmyself.text = internalfundModel?.anominatealias
            lblmobile.text =  internalfundModel?.anominatedaccount
            lblamount.text = internalfundModel?.amount!
            
            lblbuyfrom.text = "TRANSFERMONEYTO".localized(Session.sharedInstance.appLanguage)
            lblactype.text = internalfundModel?.acalias!
            
            if internalfundModel?.nominateflag == "YES"{
                switchfav.isOn = true
            }
            else{
                switchfav.isOn = false
            }
            nominateflag = (internalfundModel?.nominateflag!)!
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToCard{
            
            lblmyself.text = internalfundModel?.anominatealias
            lblmobile.text =  internalfundModel?.anominatedaccount
            lblamount.text = internalfundModel?.amount!
            
            lblbuyfrom.text = "TRANSFERMONEYTO".localized(Session.sharedInstance.appLanguage)
            lblactype.text = internalfundModel?.acalias!
            
            if internalfundModel?.nominateflag == "YES"{
                switchfav.isOn = true
            }
            else{
                switchfav.isOn = false
            }
            nominateflag = (internalfundModel?.nominateflag!)!
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToPhone{
            
            lblmyself.text = internalfundModel?.anominatealias
            lblmobile.text =  internalfundModel?.anominatedaccount
            lblamount.text = internalfundModel?.amount!
            
            lblbuyfrom.text = "TRANSFERMONEYTO".localized(Session.sharedInstance.appLanguage)
            lblactype.text = internalfundModel?.acalias!
            
            if internalfundModel?.nominateflag == "YES"{
                switchfav.isOn = true
            }
            else{
                switchfav.isOn = false
            }
            nominateflag = (internalfundModel?.nominateflag!)!
        }
        else{
            
        }
    }
    
    func btmview (hide:Bool)  {
        if hide == true {
            consHeight.constant = 0
        }
        else{
            consHeight.constant = 50
        }
        viewbeneficialy.isHidden = hide
        txtbenificially.isHidden = hide
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
   
    // MARK:_SET FONT
    func setFonts() {
        
        let (header, reviewTitle, buttonTitle) = (CurrentFlow.flowType?.getReviewData())!
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 17, color: Color.white, title: header, placeHolder: "")
        Fonts().set(object: self.lblbuyairtimefor, fontType: 1, fontSize: 9, color: Color.c0_123_255, title: reviewTitle, placeHolder: "")
        Fonts().set(object: self.lblmyself, fontType: 1, fontSize: 13, color: Color.black, title: "MPESA", placeHolder: "")
        Fonts().set(object: self.lblmobile, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: self.lblamounttitle, fontType: 1, fontSize: 13, color: Color.black, title: "Amount", placeHolder: "")
        Fonts().set(object: self.lblamount, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: self.lblbuyfrom, fontType: 1, fontSize: 13, color: Color.black, title: "BuyFrom", placeHolder: "")
        Fonts().set(object: self.lblactype, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: self.lbltransactioncosttitle, fontType: 1, fontSize: 13, color: Color.black, title: "Transactionalcost", placeHolder: "")
        Fonts().set(object: self.lbltransactioncost, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: self.lblAddFav, fontType: 1, fontSize: 13, color: Color.black, title: "ADDFAV", placeHolder: "")
        Fonts().set(object: txtbenificially, fontType: 0, fontSize: 13, color: Color.c134_142_150, title: "", placeHolder: "")
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: buttonTitle, placeHolder: "")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // Switch Action Value Changed
    @IBAction func switchValueChanged(sender: UISwitch) {
        if switchfav.isOn {
            nominateflag = "YES"
        } else {
            nominateflag = "NO"
        }
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        validate()
    }
}

// BY A Time FLow API Data
extension CheckoutAirTimeViewController {
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        //let resultArray: Result?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            //  case resultArray = "RESULT_ARRAY"
            
        }
    }
    
    func buyaTimeTransaction(){
        
        let amount = lblamount.text!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        RestAPIManager.buyAirtime(title: "PLEASEWAIT", subTitle: "BUYAIRTIMETransaction", pin: OnboardUser.pin, type: Response.self, mobileNo:OnboardUser.mobileNo, accountAlias: lblactype.text!, amount: amount, recipetno: "\(lblmobile.text!)", mnoWalletId:(dataModels?.aMNO_WALLET_ID!)!, nominateFlag: nominateflag, reciptAlias: lblmyself.text!, completion: { (response) in
            DispatchQueue.main.async {
                if response.statusCode == 200 {
                    
                    let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                    nextvc.dataModels = self.dataModels
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    self.showMessage(title: "", message: response.statusMessage ?? "")
                }
                
            }
        }) { (error) in
            print(error)
        }
    }
}

//Text field delegate
extension CheckoutAirTimeViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
     func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}

// SEND A MONEY API DATA
extension CheckoutAirTimeViewController {
    
    // Model : -
    struct SendMoneyResponse: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        //let resultArray: Result?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            //  case resultArray = "RESULT_ARRAY"
        }
    }
        // ------------------------------------------------------------------------------------
   // Send to Mobile Money
    func sendMoneytransactions(){
        let amount = lblamount.text!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        RestAPIManager.sendtomobilemoney(title: "PLEASEWAIT", subTitle: "Send to Mobile Money", pin: OnboardUser.pin, type: SendMoneyResponse.self, mobileNo: OnboardUser.mobileNo, accountAlias: lblactype.text!, amount: amount, mnowalletid: "", recipientno: "\(lblmobile.text!)", recipientalias: "Account to M-Pesa",completion: { (response) in
            DispatchQueue.main.async {
                print(response)
                if response.statusCode == 200 {
                    let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                    nextvc.message = response.statusMessage ?? ""
                    nextvc.dataModels = self.dataModels
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                }
            }
        }) { (error) in
            print(error)
        }
    }
        // -------------------------------------------------------------------------------------------
    // Mpesa STK Push  mpesa to account
    func mPesaToAccount(){
          let amount = lblamount.text!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        RestAPIManager.mPesaToaccount(title: "PLEASEWAIT", subTitle: "Send to Mobile Money", pin: OnboardUser.pin, type: SendMoneyResponse.self, mobileNo: OnboardUser.mobileNo, accountAlias: lblactype.text!, amount: amount, accountnumber: dataModels!.mobilenumber! ,completion: { (response) in
            DispatchQueue.main.async {
                print(response)
                if response.statusCode == 200 {
                    let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                    nextvc.message = response.statusMessage ?? ""
                    nextvc.dataModels = self.dataModels
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                }
            }
        }) { (error) in
            print(error)
        }
        
    }
    
        // -----------------------------------------------------------------------------------------
    // Pay BILL
    func paybill(){
        
        RestAPIManager.paybill(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOUR", billerrefrence: "", type: Response.self, mobileNo: OnboardUser.mobileNo, biller: (paybilldataModels1?.biller)!, accountalias: (paybilldataModels1?.acalias!)!, amount: (paybilldataModels1?.amount!)!, enrollbillrefrence: paybilldataModels1?.nominateFlag ?? "", enrollmenalias: "",completion: { (response) in
            DispatchQueue.main.async {
                if response.statusCode == 200 {
                    let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                    nextvc.message = response.statusMessage ?? ""
                    
                    let dataModel = BuyaTimeVM.init(mobilenumber: self.paybilldataModels1?.cardnumber ?? "", mobilename: self.paybilldataModels1?.acalias ?? "", acalias: self.paybilldataModels1?.acalias ?? "", aMNO_WALLET_ID: "", amount: self.paybilldataModels1?.amount ?? "", acnumber: self.paybilldataModels1?.acnumber ?? "", nominateflag: self.paybilldataModels1?.nominateFlag ?? "")
                    nextvc.dataModels = dataModel
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                }
            }
        }) { (error) in
            print(error)
        }
    }
    

     // ---------------------------------------------------------------------------------------
    // Internal Fund Transfer:-
    func rtgs_internalFundTransfer(command : String){
        
<<<<<<< HEAD
        let amount = internalfundModel?.amount!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range:nil)
        RestAPIManager.Internalfuntranfer(title: "PLEASEWAIT",
                                          subTitle: "WEAREPROCESSINGYOUR", scommand : command,
                                          acalias:(internalfundModel?.acalias!)!,
                                          type: IntenalFundVM.Response.self,
                                          mobileNo: OnboardUser.mobileNo,
                                          pin: OnboardUser.pin,
                                          amount: amount!,
                                          recieptacc: ((internalfundModel?.anominatedaccount!)!),
                                          recieptBankbranch: (internalfundModel?.dictbank["aBRANCH_CODE"] ?? ""),
                                          nominateflag: (internalfundModel!.nominateflag!),
                                          recieptalias: ((internalfundModel?.anominatealias!)!),
                                          reciptBank: (internalfundModel?.dictbank["aBANK_BRANCH"])!,
                                          recieptacname:"", // TODO
            completion: { (response) in
                DispatchQueue.main.async {
                    if response.statusCode == 200 {
                        let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                        
                        let dataModel = BuyaTimeVM.init(mobilenumber: ((self.internalfundModel?.anominatedaccount!)!), mobilename: (self.internalfundModel?.acalias!)!, acalias: (self.internalfundModel?.acalias!)!, aMNO_WALLET_ID: "", amount: self.internalfundModel?.amount!, acnumber: "", nominateflag: self.internalfundModel!.nominateflag!)
                        nextvc.message = response.statusMessage ?? ""
                        nextvc.dataModels = dataModel
                        
                        self.navigationController?.pushViewController(nextvc, animated: true)
                    }
                    else{
                        self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                    }
                }
        }) { (error) in
            print(error)
        }
    }
        // --------------------------------
        // --------------------------------
=======
   
//        let amount = internalfundModel?.amount!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range:nil)
//        RestAPIManager.Internalfuntranfer(title: "PLEASEWAIT",
//                                          subTitle: "",
//                                          acalias:(internalfundModel?.acalias!)!,
//                                          type: IntenalFundVM.Response.self,
//                                          mobileNo: OnboardUser.mobileNo,
//                                          pin: OnboardUser.pin,
//                                          amount: amount!,
//                                          recieptacc: ((internalfundModel?.anominatedaccount!)!),
//                                          recieptBankbranch: ((internalfundModel?.anominatealias!)!),
//                                          nominateflag: (internalfundModel?.dictbank["aBRANCH_CODE"])!,
//                                          recieptalias: (internalfundModel!.nominateflag!),
//                                          reciptBank: (internalfundModel?.dictbank["aBANK_BRANCH"])!,
//                                          recieptacname:"", // TODO
//            completion: { (response) in
//                DispatchQueue.main.async {
//                    print(response)
//                    if response.statusCode == 200 {
//                        let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
//                        self.navigationController?.pushViewController(nextvc, animated: true)
//                    }
//                    else{
//                        self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
//                    }
//                }
//        }) { (error) in
//            print(error)
//        }
    }
    

        // ------------------------------------------------------------------------------------------
    // RTGS Fund Transfer:-
    func RtgsFundTransfer(){
        
        let amount = internalfundModel?.amount!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range:nil)
        RestAPIManager.rtgs(title: "PLEASEWAIT",
                            subTitle: "",
                            acalias:(internalfundModel?.acalias!)!,
                            type: IntenalFundVM.Response.self,
                            mobileNo: OnboardUser.mobileNo,
                            pin: OnboardUser.pin,
                            amount: amount!,
                            recieptacc: ((internalfundModel?.anominatedaccount!)!),
                            recieptBankbranch: ((internalfundModel?.anominatealias!)!),
                            nominateflag: (internalfundModel?.dictbank["aBRANCH_CODE"])!,
                            recieptalias: (internalfundModel!.nominateflag!),
                            reciptBank: (internalfundModel?.dictbank["aBANK_BRANCH"])!,
                            recieptacname:"", // TODO
            narration:"",// TODO
            completion: { (response) in
                DispatchQueue.main.async {
                    print(response)
                    if response.statusCode == 200 {
                        let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                        self.navigationController?.pushViewController(nextvc, animated: true)
                    }
                    else{
                        self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                    }
                }
        }) { (error) in
            print(error)
        }
    }
    
    
    

   
        // --------------------------------------------------------------------------------------------
>>>>>>> 353cd217832b374ace3dba6f87351ccc96119769
     // Pesalink - Send to Account
    func SendToAccount(){
         let amount = internalfundModel?.amount!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range:nil)
        
        
        
        RestAPIManager.sendToAccount(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOUR",
                                     acalias: ((internalfundModel?.acalias!)!),
                                     type: PesaLinkVM.Response.self,
                                     mobileNo: OnboardUser.mobileNo,
                                     pin: OnboardUser.pin,
                                     amount: amount!,
                                     recieptacnumber: internalfundModel!.anominatedaccount!,
                                     nominatealias: nominateflag,
                                     bank: (internalfundModel?.dictbank["aBANK"])!,
                                     narration: "",
                                     nominate: txtbenificially.text ?? "",
                                     completion: { (response) in
            DispatchQueue.main.async {
                print(response)
                if response.statusCode == 200 {
                    let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                    
                    let model = BuyaTimeVM.init(mobilenumber: OnboardUser.mobileNo, mobilename:"", acalias: (self.internalfundModel?.acalias!)!, aMNO_WALLET_ID: "", amount: amount, acnumber: ((self.internalfundModel?.anominatedaccount!)!), nominateflag: "")
                    nextvc.dataModels = model
                    nextvc.message = response.statusMessage
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                }
            }
        }) { (error) in
            print(error)
        }
    }
    
//    (lldb) po print(internalfundModel?.acalias)
//    Optional("Current Acct-KES-0100006123443")
//    (lldb) po print(internalfundModel?.anominatedaccount)
//    Optional("0100003636474")
//    (lldb) po print(internalfundModel?.anominatedaccount)
//    Optional("0100003636474")
//    (lldb) po print(internalfundModel?.anominatealias)
//    Optional("Edward Savings")
//    (lldb) po print(internalfundModel?.dictbank)
//    Optional(["aBANK_CODE": "IDB", "aBANK_ID": "38", "aBANK": "Industrial Development Bank", "aDATE": "2019-04-30 09:26:34"])
    
    
    // --------------------------------------------------------------------------------------------------
   // Pesalink - Send to CARD
    func SendToCard(){   // TODO CARDNUMBER
        let amount = internalfundModel?.amount!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range:nil)
        
        RestAPIManager.sendToCard(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOUR", acalias: ((internalfundModel?.acalias!)!), type: PesaLinkVM.Response.self, mobileNo: OnboardUser.mobileNo, pin: OnboardUser.pin, amount: amount!,recieptacnumber : internalfundModel!.anominatedaccount!,nominateflag : "",cardnumber : "",narration:"",nominate:txtbenificially.text ?? "",completion: { (response) in
                        DispatchQueue.main.async {
                            print(response)
                            if response.statusCode == 200 {
                                let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                                self.navigationController?.pushViewController(nextvc, animated: true)
                            }
                            else{
                                self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                            }
                        }
                    }) { (error) in
                        print(error)
                    }
      }
    
    // --------------------------------------------------------------------------------------------------------
    // Pesalink - Send to CARD
    func SentToPhone(){
        let amount = internalfundModel?.amount!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range:nil)
        // TODO receipentmobnumber , banksortcode ,
        RestAPIManager.sendToPhone(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOUR", acalias: (internalfundModel?.acalias!)!, type: PesaLinkVM.Response.self, mobileNo: OnboardUser.mobileNo, pin: OnboardUser.pin, amount: amount!, recieptacnumber: internalfundModel!.anominatedaccount!, nominateflag: "", banksortcode: "", narration: txtbenificially.text ?? "", nominate: "", receipentmobnumber: "", completion: { (response) in
                        DispatchQueue.main.async {
                            print(response)
                            if response.statusCode == 200 {
                                let nextvc = TransactionSuccessfullViewController.getVCInstance() as! TransactionSuccessfullViewController
                                
                                let model = BuyaTimeVM.init(mobilenumber: OnboardUser.mobileNo, mobilename:"", acalias: (self.internalfundModel?.acalias!)!, aMNO_WALLET_ID: "", amount: amount, acnumber: ((self.internalfundModel?.anominatedaccount!)!), nominateflag: "")
                                nextvc.dataModels = model
                                nextvc.message = response.statusMessage
                                
                                self.navigationController?.pushViewController(nextvc, animated: true)
                            }
                            else{
                                self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                            }
                        }
                    }) { (error) in
                        print(error)
                    }
    }
}


// VALIDATE FACE AND PIN :_
extension CheckoutAirTimeViewController:AuthDelegate {
    func validate() {
        //Check if user enable biometric access
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            let context = LAContext()
            Authntication(context).authnticateUser { (success, message) in
                DispatchQueue.main.async {
                    if success == true {
                        self.validAuth()
                    }
                    else {
                        if message != "" {
                            self.showMessage(title: "", message: message)
                        }
                    }
                }
            }
        }
        else {
            let vc = AuthoriseViewController.getVCInstance() as! AuthoriseViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func validAuth() {
        
        if CurrentFlow.flowType == FlowType.SendMoney{
            sendMoneytransactions()
        }
        else if CurrentFlow.flowType == FlowType.BuyAirtime{
            buyaTimeTransaction()
        }
        else if CurrentFlow.flowType == FlowType.PayBill{
            paybill()
        }
        else if CurrentFlow.flowType == FlowType.InternalFundTransfer{
            rtgs_internalFundTransfer(command: sCommand.TF)
        }
        else if CurrentFlow.flowType == FlowType.Rtgs{
            rtgs_internalFundTransfer(command: sCommand.RTGS)
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToAc{
            SendToAccount()
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToCard{
            SendToCard()
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToPhone{
           SentToPhone()
        }
        else if CurrentFlow.flowType == FlowType.MpesaToAccount{
          mPesaToAccount()
        }
        else{
         
        }
    }
}
