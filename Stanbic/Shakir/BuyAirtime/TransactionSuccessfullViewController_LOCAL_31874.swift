//
//  TransactionSuccessfullViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 20/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class TransactionSuccessfullViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    // titles
    @IBOutlet weak var lbldatetitle: UILabel!
    @IBOutlet weak var lbltimetitle: UILabel!
    @IBOutlet weak var lblsendtotitle: UILabel!
    @IBOutlet weak var lblsentfromtitle: UILabel!
    @IBOutlet weak var lblamounttitle: UILabel!
    @IBOutlet weak var lblrecieptnotitle: UILabel!
    // values
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var lblsendto: UILabel!
    @IBOutlet weak var lblsentfrom: UILabel!
    @IBOutlet weak var lblamount: UILabel!
    @IBOutlet weak var lblrecieptno: UILabel!
    @IBOutlet weak var imgright: UIView!
    @IBOutlet weak var viewdata: UIView!
    @IBOutlet weak var btndone: UIButton!
    
    var message : String?
    
    var dataModels : BuyaTimeVM?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        setData()
        
        lblsendto.text =  dataModels?.mobilenumber!
        
        imgright.layer.borderWidth = 2
        imgright.layer.borderColor = UIColor.cyan.cgColor
        imgright.clipsToBounds = true
        imgright.layer.cornerRadius = imgright.frame.height/2
        imgright.layer.shadowColor = UIColor.lightGray.cgColor
        imgright.layer.shadowOpacity = 1
        imgright.layer.shadowOffset = CGSize.zero
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 22, color: Color.c0_173_108, title: "THANKYOU", placeHolder: "")
        Fonts().set(object: self.lblsubtitle, fontType: 0, fontSize: 17, color: Color.c52_58_64, title: "", placeHolder: "")
        
        Fonts().set(object: self.lbldatetitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "DATE", placeHolder: "")
        Fonts().set(object: self.lbltimetitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "TIME", placeHolder: "")
        Fonts().set(object: self.lblsendtotitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "SENTTO", placeHolder: "")
        Fonts().set(object: self.lblsentfromtitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "SENTFROM", placeHolder: "")
        Fonts().set(object: self.lblamounttitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "Amount", placeHolder: "")
        Fonts().set(object: self.lblrecieptnotitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "RECIEPTNO", placeHolder: "")
        
        Fonts().set(object: self.lbldate, fontType: 1, fontSize: 15, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: self.lbltime, fontType: 1, fontSize: 15, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: self.lblsendto, fontType: 1, fontSize: 15, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: self.lblsentfrom, fontType: 1, fontSize: 15, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: self.lblamount, fontType: 1, fontSize: 15, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: self.lblrecieptno, fontType: 1, fontSize: 15, color: Color.black, title: "", placeHolder: "")
       
        Fonts().set(object: btndone, fontType: 1, fontSize: 14, color: Color.white, title: "DONE", placeHolder: "")
    
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Done(_ sender: AttributedButton) {
        
    }
    
    @IBAction func share(_ sender: AttributedButton) {
        
    }
}


extension TransactionSuccessfullViewController {
    func setData() {
        let dateStr = DateConverter().convertDate(date_str: "\(Date())", formatTo: "dd/MM/yyyy", formatFrom: "yyyy-MM-dd HH:mm:ss +z")
        let timeStr = DateConverter().convertDate(date_str: "\(Date())", formatTo: "hh:mm a", formatFrom: "yyyy-MM-dd HH:mm:ss +z")
        lbldate.text = dateStr
        lbltime.text = timeStr
        lblsendto.text = dataModels?.mobilenumber
        lblsentfrom.text = dataModels?.acalias
        lblamount.text = dataModels?.amount
        lblrecieptno.text = "NQR2345H" //TODO
        lblsubtitle.text = message
        
    }
}
