//
//  CheckoutAirTimeViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 20/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication

class CheckoutAirTimeViewController: UIViewController,paybillfetchdataVMDelegate {
   
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblbuyairtimefor: UILabel!
    @IBOutlet weak var lblmyself: UILabel!
    @IBOutlet weak var lblmobile: UILabel!
    @IBOutlet weak var lblamounttitle: UILabel!
    @IBOutlet weak var lblamount: UILabel!
    @IBOutlet weak var lblbuyfrom: UILabel!
    @IBOutlet weak var lblactype: UILabel!
    @IBOutlet weak var lbltransactioncosttitle: UILabel!
    @IBOutlet weak var lbltransactioncost: UILabel!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var switchfav: UISwitch!
    
      @IBOutlet weak var viewcenter: UIView!
      @IBOutlet weak var viewcheckbox: UIView!
    
    
    var dataModels : BuyaTimeVM?
    var paybilldataModels : PayBillVM?
    var paybilldataModels1 : PayBillVM1?
    var nominateflag = ""
    
     var paybillmodel = paybillfetchdata()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        setdata()
        paybillmodel.delegate = self
          self.tabBarController?.tabBar.isHidden = true
          //viewcenter.addshaow()
          viewcheckbox.addshaow()
    }
    
    func setdata(){
        if CurrentFlow.flowType == FlowType.BuyAirtime{
            lblactype.text = dataModels?.acalias!
            lblmobile.text = dataModels?.mobilenumber!
            lblmyself.text = dataModels?.mobilename!
            lblamount.text = dataModels?.amount!
            
            if dataModels?.nominateflag == "YES"{
                switchfav.isOn = true
            }
            else{
                switchfav.isOn = false
            }
            nominateflag = dataModels?.nominateflag! ?? ""
        }
        
        if CurrentFlow.flowType == FlowType.SendMoney{
            lblactype.text = dataModels?.acalias!
            lblmobile.text = dataModels?.mobilenumber!
            lblmyself.text = dataModels?.mobilename!
            lblamount.text = dataModels?.amount!
            
            if dataModels?.nominateflag == "YES"{
                switchfav.isOn = true
            }
            else{
                switchfav.isOn = false
            }
            nominateflag = dataModels?.nominateflag! ?? ""
        }
        
        if CurrentFlow.flowType == FlowType.PayBill{
            lblactype.text = paybilldataModels1?.acalias!
            lblmobile.text = paybilldataModels?.mobilenumber!
            lblmyself.text = paybilldataModels?.mobilename!
            lblamount.text = paybilldataModels1?.amount!
            
            if paybilldataModels?.nominateflag == "YES"{
                switchfav.isOn = true
            }
            else{
                switchfav.isOn = false
            }
            nominateflag = paybilldataModels?.nominateflag! ?? ""
        }
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        if CurrentFlow.flowType == FlowType.SendMoney{
            Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "DONE", placeHolder: "")
            Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 17, color: Color.white, title: "CONFIRMANDTRANFER", placeHolder: "")
            Fonts().set(object: self.lblbuyairtimefor, fontType: 0, fontSize: 9, color: Color.c0_123_255, title: "TRANSFERMONEYTO", placeHolder: "")
        }
        if CurrentFlow.flowType == FlowType.BuyAirtime{
            Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "BUYAIRTIME", placeHolder: "")
            Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 17, color: Color.white, title: "Confirmandbuy", placeHolder: "")
            Fonts().set(object: self.lblbuyairtimefor, fontType: 0, fontSize: 9, color: Color.c0_123_255, title: "BUYAIRTIMEFOR", placeHolder: "")
        }
        if CurrentFlow.flowType == FlowType.PayBill {
            Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "PAYBILL", placeHolder: "")
            Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 17, color: Color.white, title: "CONFIRMANDPAY", placeHolder: "")
            Fonts().set(object: self.lblbuyairtimefor, fontType: 0, fontSize: 9, color: Color.c0_123_255, title: "MAKEPAYMENTTO", placeHolder: "")
        }
        
    
        Fonts().set(object: self.lblmyself, fontType: 1, fontSize: 13, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: self.lblamounttitle, fontType: 1, fontSize: 13, color: Color.black, title: "Amount", placeHolder: "")
        Fonts().set(object: self.lblbuyfrom, fontType: 1, fontSize: 13, color: Color.black, title: "BuyFrom", placeHolder: "")
        Fonts().set(object: self.lbltransactioncosttitle, fontType: 1, fontSize: 13, color: Color.black, title: "Transactionalcost", placeHolder: "")
        Fonts().set(object: self.lblmobile, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: self.lblamount, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: self.lblactype, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: self.lbltransactioncost, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // Switch Action Value Changed
    @IBAction func switchValueChanged(sender: UISwitch) {
        if switchfav.isOn {
            nominateflag = "YES"
        } else {
            nominateflag = "NO"
        }
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        
        validate()
    }
    
    // validate()
    
    func authnticateUser() {
        //CHECK USER'S FINGERPRINT OR FACE ID
        let context = LAContext()
        Authntication(context).authnticateUser { (success, message) in
            if success == true {
                DispatchQueue.main.async {
                    self.buyaTimeTransaction()
                }
            }
            else {
                DispatchQueue.main.async {
                    if message != "" {
                        self.showMessage(title: "", message: message)
                    }
                }
            }
        }
    }
    
    func fetchbilldata() {
        
    }
    
}

// BY A Time FLow API Data
extension CheckoutAirTimeViewController {
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        //let resultArray: Result?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            //  case resultArray = "RESULT_ARRAY"
            
        }
    }
    
    func buyaTimeTransaction(){
        
        
        let amount = lblamount.text!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        RestAPIManager.buyAirtime(title: "PLEASEWAIT", subTitle: "BUYAIRTIMETransaction", pin: "Cx+1e0vEBoZlPztxxZG5fQ==", type: Response.self, mobileNo:OnboardUser.mobileNo, accountAlias: lblactype.text!, amount: amount, recipetno: "254\(lblmobile.text!)", mnoWalletId:(dataModels?.aMNO_WALLET_ID!)!, nominateFlag: nominateflag, reciptAlias: lblmyself.text!, completion: { (response) in
            DispatchQueue.main.async {
                print(response)
                if response.statusCode == 200 {
                    
                    let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionSuccessfullViewController") as! TransactionSuccessfullViewController
                    nextvc.dataModels = self.dataModels
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                }
                
            }
        }) { (error) in
            print(error)
        }
    }
}

// SEND A MONEY API DATA
extension CheckoutAirTimeViewController {
    
    // Model : -
    struct SendMoneyResponse: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        //let resultArray: Result?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            //  case resultArray = "RESULT_ARRAY"
        }
    }
    
    func sendMoneytransactions(){
        let amount = lblamount.text!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        RestAPIManager.sendtomobilemoney(title: "PLEASEWAIT", subTitle: "Send to Mobile Money", pin: "Cx+1e0vEBoZlPztxxZG5fQ==", type: SendMoneyResponse.self, mobileNo: OnboardUser.mobileNo, accountAlias: lblactype.text!, amount: amount, mnowalletid: "", recipientno: "254\(lblmobile.text!)", recipientalias: "Account to M-Pesa",completion: { (response) in
            DispatchQueue.main.async {
                print(response)
                if response.statusCode == 200 {
                    let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionSuccessfullViewController") as! TransactionSuccessfullViewController
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                }
            }
        }) { (error) in
            print(error)
        }
    }
    
    // Pay BILL
    func paybill(){
        
        let dict = paybillmodel.responseData?.fetchbills[0]
        
        RestAPIManager.paybill(title: "String", subTitle: "PAYBILLPROCESS", billerrefrence: "", type: Response.self, mobileNo: OnboardUser.mobileNo, biller: (dict?.biller)!, accountalias: (paybilldataModels1?.acalias!)!, amount: (paybilldataModels1?.amount!)!, enrollbillrefrence: "", enrollmenalias: " ",completion: { (response) in
            DispatchQueue.main.async {
                print(response)
                if response.statusCode == 200 {
                    let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionSuccessfullViewController") as! TransactionSuccessfullViewController
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                }
            }
        }) { (error) in
            print(error)
        }
    }
    
}







extension CheckoutAirTimeViewController:AuthDelegate {
    func validate() {
        //Check if user enable biometric access
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            let context = LAContext()
            Authntication(context).authnticateUser { (success, message) in
                DispatchQueue.main.async {
                    if success == true {
                        self.validAuth()
                    }
                    else {
                        if message != "" {
                            self.showMessage(title: "", message: message)
                        }
                    }
                }
            }
        }
        else {
            let vc = AuthoriseViewController.getVCInstance() as! AuthoriseViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func validAuth() {
        print("valid")
        if CurrentFlow.flowType == FlowType.SendMoney{
            sendMoneytransactions()
        }
        else if CurrentFlow.flowType == FlowType.BuyAirtime{
            buyaTimeTransaction()
        }
         else if CurrentFlow.flowType == FlowType.PayBill{
            paybill()
        }
        else{
            
        }
    }
}
