//
//  RequestBankStatementViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 22/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication

class RequestBankStatementViewController: UIViewController, UIGestureRecognizerDelegate, UITextFieldDelegate, UINavigationControllerDelegate {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var txtstartDate: UITextField!
    @IBOutlet weak var txtendDate: UITextField!
    @IBOutlet weak var lblstart: UILabel!
    @IBOutlet weak var lblend: UILabel!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lblrequestfor: UILabel!
     @IBOutlet weak var lblcharge: UILabel!
    
    // Variables for Bottom sheet
    var bottomSheetVC: BottomSheetViewController?
    var imgView = UIImageView()
    let datePicker = UIDatePicker()
    var txtfield = ""
    var todayDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add Bottom sheet VC
        addBottomSheetView()
        bottomSheetVC?.delegate = self
        
        setFonts()
        
        self.tabBarController?.tabBar.isHidden = true
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            lblaccount.text = acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!
            lblaccountnumber.text = acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!.hashingOfTheAccount(noStr: acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!)
        }
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        initialSetUpNavigationBar()
     
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: UIGestureRecognizer
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
   static func getVCInstance() -> UIViewController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Checkbook.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK: - Required Method's
    func initialSetUpNavigationBar(){
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 24)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = "FULLSTATEMENT".localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
       // Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: "FULLSTATEMENT", placeHolder: "")
        Fonts().set(object: self.lblsubtitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "REQUESTBANKSTATEMENT", placeHolder: "")
        
        Fonts().set(object: self.lblstart, fontType: 0, fontSize: 12, color: Color.c0_123_255, title: "SELECTSTARTDATE", placeHolder: "")
        Fonts().set(object: self.lblend, fontType: 0, fontSize: 12, color: Color.c0_123_255, title: "SELECTENDDATE", placeHolder: "")
        Fonts().set(object: self.lblrequestfor, fontType: 0, fontSize: 12, color: Color.c0_123_255, title: "REQUESTFOR", placeHolder: "")
        
        Fonts().set(object: self.txtendDate, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "Select end date")
        Fonts().set(object: self.txtstartDate, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "Select start date")
        
        Fonts().set(object: self.lblaccount, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        
        Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "", placeHolder: "")
        
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "SUBMIT", placeHolder: "")
            Fonts().set(object: lblcharge, fontType: 1, fontSize: 10, color: Color.c0_123_255, title: "*"+global.ftmessage, placeHolder: "")
        
  //      Fonts().set(object: lblcharge, fontType: 0, fontSize: 16, color: Color.c10_34_64, title: global.ftmessage, placeHolder: "")
        
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func accountselect(_ sender: AttributedButton) {
        view.endEditing(true)
        self.showBottomSheetVC(xibtype: "ACCOUNT", Selectname: lblaccount.text!)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
                view.endEditing(true)
         if txtstartDate.text == "" {
            txtstartDate.showError(errStr: "SELECTSTARTDATE")
        }
        else if txtendDate.text == "" {
            txtendDate.showError(errStr: "SELECTENDDATE")
        }
        else if lblaccountnumber.text == "" {
            showMessage(title: "", message: "SELECTACCOUNT")
        }
        else{
           validate()
         
        }
    }
    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
  
    func showBottomSheetVC(xibtype: String,Selectname: String) {
        
        var height = 300  // 340
        let coreData = CoreDataHelper()
        if xibtype == "ACCOUNT" {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
                height = acdata.count * 100
                if acdata.count == 1 {
                    height = 200
                }
            }
        }
        else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acnetwork  = coreData.getDataForEntity(entity: Entity.MNO_INFO)
                height = acnetwork.count * 200
            }
        }
        
        // bottomSheetVC?.partialView = UIScreen.main.bounds.height - CGFloat(height)
        bottomSheetVC!.partialView = self.view.frame.size.height - CGFloat(height)
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imgView.backgroundColor = Color.black
        imgView.alpha = 0.2
        imgView.isUserInteractionEnabled = true
        self.view.addSubview(imgView)
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "", SelectName: "")
        imgView.removeFromSuperview()
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        // textField.didBegin()
        if textField == txtstartDate {
            txtfield = "1"
            initiateDatePicker(textfield: txtstartDate)
        }
        else {
            if textField == txtendDate{
                txtendDate.removeError()
                if txtstartDate.text == "" {
                   // self.showMessage(title: "", message: "SELECTSTARTDATE")
                    txtstartDate.showError(errStr: "SELECTSTARTDATE")
                    self.view.endEditing(true)
                 }
                else {
                    txtfield = "2"
                    initiateDatePicker(textfield: txtendDate)

                }
             }
         }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // textField.didEnd()
    }
    
    // Date Picker Logic =======
    func initiateDatePicker(textfield : UITextField){
        //Registrater date picker value changed event
        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        datePicker.maximumDate = Date()
        //Formate Date
        datePicker.datePickerMode = .date
        textfield.inputView = datePicker
        datePicker.isHidden = false
        
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(RequestBankStatementViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(RequestBankStatementViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        if txtfield == "1" {
            self.txtstartDate.inputAccessoryView = toolBar

        }
        else{
            self.txtendDate.inputAccessoryView = toolBar
        }
      
    }
    
    @objc func doneClick() {
        let formatter = DateFormatter()
      //  formatter.dateFormat = "yyyy-MM-dd"
         formatter.dateFormat = "dd-MM-yyyy"
         if txtfield == "1" {
        self.txtstartDate.text = (formatter.string(from: datePicker.date))
              txtstartDate.removeError()
        }
         else{
            self.txtendDate.text = (formatter.string(from: datePicker.date))
            txtendDate.removeError()
        }
         self.view.endEditing(true)
    }
    
    @objc func cancelClick() {
        self.view.endEditing(true)
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        let formatter = DateFormatter()
        
       // formatter.dateFormat = "yyyy-MM-dd"
         formatter.dateFormat = "dd-MM-yyyy"
        if txtfield == "1" {
            txtstartDate.text = (formatter.string(from: sender.date))
            txtendDate.resignFirstResponder()
            todayDate = sender.date
            txtstartDate.removeError()
        }
        if txtfield == "2" {
            if sender.date < todayDate {
            txtendDate.text = ""
            //    self.showMessage(title: "", message: "INVALIDENDDATE")
                txtendDate.showError(errStr: "INVALIDENDDATE")
            }
            else {
            txtendDate.text = (formatter.string(from: sender.date))
            txtendDate.resignFirstResponder()
                 txtendDate.removeError()
            }
        }
    }
    
    func touchesBegan(_ touches: Set<AnyHashable>, withEvent event: UIEvent) {
        
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

extension RequestBankStatementViewController : SelectNetworkDelegate{
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
        lblaccountnumber.text =  acnumber
        lblaccount.text = acname
    }
    
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
        hideBottomSheetVC()
        lblaccount.text = newtorkname
        lblaccountnumber.text = networkid.hashingOfTheAccount(noStr: networkid)
    }
}

extension RequestBankStatementViewController:AuthDelegate {
    // VALIDATE FACE AND PIN :_
    
    func validate() {
        //Check if user enable biometric access
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            let context = LAContext()
            Authntication(context).authnticateUser { (success, message) in
                DispatchQueue.main.async {
                    if success == true {
                        global.enterpin = OnboardUser.pin
                        self.validAuth()
                    }
                    else {
                        if message != "" {
                           self.showMessage(title: "", message: message)
                        }
                    }
                 
                }
            }
        }
        else {
               global.enterpin = ""
            let vc = AuthoriseViewController.getVCInstance() as! AuthoriseViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    // FULL STATEMENT :_DaF
    func validAuth() {
        
        RestAPIManager.fullStatement(title : "PLEASEWAIT", subTitle : "FULLSTATEMENT", pin : global.enterpin,type: Response.self, mobileNo : OnboardUser.mobileNo,accountAlias : lblaccount.text!,startDate :  txtstartDate.text!,endDate :  txtendDate.text!,deliveryMode : "EMAIL",email : "",completion: { (response) in
            DispatchQueue.main.async {
                if response.statusCode == 200 {
                    let nextvc = ChequeBookSuccessViewController.getVCInstance() as! ChequeBookSuccessViewController
                    nextvc.msg = response.statusMessage!
                    nextvc.flow = "FullStatement"
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else {
                    //self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                    
                    let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                    nextvc.message = response.statusMessage ?? ""
                    
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
            }
            
        }){ (error) in
          
        }
     }
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let refrenceId: String?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case refrenceId = "REFERENCE_ID"
         }
    }
}



extension Date {
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}
