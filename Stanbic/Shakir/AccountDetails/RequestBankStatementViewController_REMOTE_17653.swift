//
//  RequestBankStatementViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 22/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication

class RequestBankStatementViewController: UIViewController,UIGestureRecognizerDelegate,UITextFieldDelegate{
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var txtstartDate: UITextField!
    @IBOutlet weak var txtendDate: UITextField!
    @IBOutlet weak var lblstart: UILabel!
    @IBOutlet weak var lblend: UILabel!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lblrequestfor: UILabel!
    
    // Variables for Bottom sheet
    var bottomSheetVC: BottomSheetViewController?
        var imgView = UIImageView()
    
    let datePicker = UIDatePicker()
    var txtfield = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add Bottom sheet VC
        addBottomSheetView()
        bottomSheetVC?.delegate = self
        
        
        setFonts()
        self.tabBarController?.tabBar.isHidden = true
        
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            lblaccount.text = acdata[0]["aACCOUNT_ALIAS"]!
            lblaccountnumber.text = acdata[0]["aACCOUNT_NUMBER"]!.hashingOfTheAccount(noStr: acdata[0]["aACCOUNT_NUMBER"]!)
        }
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
   static func getVCInstance() -> UIViewController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Checkbook.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: "FULLSTATEMENT", placeHolder: "")
        Fonts().set(object: self.lblsubtitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "REQUESTBANKSTATEMENT", placeHolder: "")
        
        Fonts().set(object: self.lblstart, fontType: 1, fontSize: 9, color: Color.c0_123_255, title: "SELECTSTARTDATE", placeHolder: "")
        Fonts().set(object: self.lblend, fontType: 1, fontSize: 9, color: Color.c0_123_255, title: "SELECTENDDATE", placeHolder: "")
        Fonts().set(object: self.lblrequestfor, fontType: 1, fontSize: 9, color: Color.c0_123_255, title: "REQUESTFOR", placeHolder: "")
        
        Fonts().set(object: self.txtendDate, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "Start Date")
        Fonts().set(object: self.txtstartDate, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "End Date")
            Fonts().set(object: self.lblaccountnumber, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "CURRENTACCOUNT", placeHolder: "")
        Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "XX-XXX-XXX-X23", placeHolder: "")
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "SUBMIT", placeHolder: "")
        
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func accountselect(_ sender: AttributedButton) {
        view.endEditing(true)
        self.showBottomSheetVC(xibtype: "ACCOUNT", Selectname: lblaccount.text!)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        
        if txtstartDate.text == "" {
            txtstartDate.showError(errStr: "SELECTSTARTDATE")
        }
        else if txtendDate.text == "" {
            txtendDate.showError(errStr: "SELECTENDDATE")
        }
        else if lblaccountnumber.text == "" {
            showMessage(title: "", message: "SELECTACCOUNT")
        }
        else{
            validate()
        }
    }
    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
    func showBottomSheetVC(xibtype: String,Selectname: String) {
        
        var height = 300  // 340
        let coreData = CoreDataHelper()
        if xibtype == "ACCOUNT" {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
                height = acdata.count * 200
                if acdata.count == 1 {
                    height = 310
                }
            }
        }
        else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acnetwork  = coreData.getDataForEntity(entity: Entity.MNO_INFO)
                height = acnetwork.count * 200
            }
        }
        
        bottomSheetVC?.partialView = UIScreen.main.bounds.height - CGFloat(height)
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imgView.backgroundColor = Color.black
        imgView.alpha = 0.2
        imgView.isUserInteractionEnabled = true
        self.view.addSubview(imgView)
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "", SelectName: "")
        imgView.removeFromSuperview()
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        // textField.didBegin()
        if textField == txtstartDate {
            txtfield = "1"
            initiateDatePicker(textfield: txtstartDate)
        }
        else {
            txtfield = "2"
            initiateDatePicker(textfield: txtendDate)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // textField.didEnd()
    }
    
    // Date Picker Logic =======
    func initiateDatePicker(textfield : UITextField){
        //Registrater date picker value changed event
        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        datePicker.maximumDate = Date()
        //Formate Date
        datePicker.datePickerMode = .date
        textfield.inputView = datePicker
        datePicker.isHidden = false
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if txtfield == "1" {
            txtstartDate.text = (formatter.string(from: sender.date))
             txtendDate.resignFirstResponder()
        }
        if txtfield == "2" {
            txtendDate.text = (formatter.string(from: sender.date))
            txtendDate.resignFirstResponder()
        }
    }
    
    func touchesBegan(_ touches: Set<AnyHashable>, withEvent event: UIEvent) {
        
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }}

extension RequestBankStatementViewController : SelectNetworkDelegate{
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
        lblaccountnumber.text =  acnumber
        lblaccount.text = acname
    }
    
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
        hideBottomSheetVC()
    }
}

extension RequestBankStatementViewController:AuthDelegate {
    // VALIDATE FACE AND PIN :_
    
    func validate() {
        //Check if user enable biometric access
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            let context = LAContext()
            Authntication(context).authnticateUser { (success, message) in
                DispatchQueue.main.async {
                    if success == true {
                        self.validAuth()
                    }
                    else {
                        if message != "" {
                            self.showMessage(title: "", message: message)
                        }
                    }
                }
            }
        }
        else {
            let vc = AuthoriseViewController.getVCInstance() as! AuthoriseViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    // FULL STATEMENT :_
    func validAuth() {
        
        RestAPIManager.fullStatement(title : "PLEASEWAIT", subTitle : "FULLSTATEMENT", pin : OnboardUser.pin,type: Response.self, mobileNo : OnboardUser.mobileNo,accountAlias : lblaccount.text!,startDate :  txtstartDate.text!,endDate :  txtendDate.text!,deliveryMode : "EMAIL",email : "",completion: { (response) in
            DispatchQueue.main.async {
                print(response)
                if response.statusCode == 200 {
                    let nextvc = ChequeBookSuccessViewController.getVCInstance() as! ChequeBookSuccessViewController
                    nextvc.msg = response.statusMessage!
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else {
                    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                }
            }
            
        }){ (error) in
            print(error)
        }
     }
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let refrenceId: String?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case refrenceId = "REFERENCE_ID"
         }
    }
}
