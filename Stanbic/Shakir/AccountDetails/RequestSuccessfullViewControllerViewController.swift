//
//  RequestSuccessfullViewControllerViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 22/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class RequestSuccessfullViewControllerViewController: UIViewController, UINavigationControllerDelegate {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var lblfor: UILabel!
    @IBOutlet weak var lbldatetitle: UILabel!
    @IBOutlet weak var lblacnumber: UILabel!
    @IBOutlet weak var lblactype: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var btndone: UIButton!
    
    var dictdata = NSMutableDictionary()
    
    let str =  "Your Bank Statement for account\n****5623 has been sent to your email\n jamesmacharia@gmail.com"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
          self.tabBarController?.tabBar.isHidden = true
    }
    
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.c0_173_108, title: "REQUESTSUCCUESS", placeHolder: "")
        Fonts().set(object: self.lblsubtitle, fontType: 3, fontSize: 16, color: Color.c52_58_64, title: str, placeHolder: "")
        
        
        Fonts().set(object: self.lblfor, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "FOR", placeHolder: "")
        Fonts().set(object: self.lbldatetitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "DATE", placeHolder: "")
        
        Fonts().set(object: self.lblactype, fontType: 1, fontSize: 13, color: Color.black, title: "Current account", placeHolder: "")
        Fonts().set(object: self.lblacnumber, fontType: 1, fontSize: 13, color: Color.c134_142_150, title: "XX-XXX-XXX-X23", placeHolder: "")
        
        Fonts().set(object: self.lbldate, fontType: 1, fontSize: 13, color: Color.black, title: "24/03/2019", placeHolder: "")
        
         Fonts().set(object: self.btndone, fontType: 1, fontSize: 14, color: Color.white, title: "DONE", placeHolder: "")
       
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Done(_ sender: AttributedButton) {
        
    }
}
