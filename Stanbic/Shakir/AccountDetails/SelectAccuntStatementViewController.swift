//
//  SelectAccuntStatementViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 22/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class  SelectAccuntStatementCell: UITableViewCell {
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var imgstatement: UIImageView!
    
    override func awakeFromNib() {
        Fonts().set(object: lbltitle, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: "Fullstatement", placeHolder: "")
        Fonts().set(object: lblsubtitle, fontType: 0, fontSize: 11, color: Color.c160_166_173, title: "Getafullaccountstatement", placeHolder: "")
    }
}

class SelectAccuntStatementViewController: UIViewController, UINavigationControllerDelegate{
   
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var tblselectstatement: UITableView!
    
    var iscontroller = "sendMoney"

    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        self.tabBarController?.tabBar.isHidden = true
     }
    
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: self.lbltitle, fontType: 0, fontSize: 24, color: Color.white, title: "NEWREQUEST", placeHolder: "")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SelectAccuntStatementViewController : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    let cell = tableView.dequeueReusableCell(withIdentifier: "SelectAccuntStatementCell", for: indexPath as IndexPath) as! SelectAccuntStatementCell
        
    return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           self.view.endEditing(true)
        if iscontroller == "sendMoney"{
            let nextvc = ContactListViewController.getVCInstance() as! ContactListViewController
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
        else{
         let nextvc = RequestBankStatementViewController.getVCInstance() as! RequestBankStatementViewController
         self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let animation = AnimationFactory.makeFade(duration: 0.5, delayFactor: 0.0)
        let animator = Animator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: tableView)
    }
 }
