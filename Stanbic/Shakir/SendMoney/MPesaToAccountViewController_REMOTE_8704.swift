//
//  MPestToAccountViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 23/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class PricebuttonCell1: UICollectionViewCell {
    
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var viewbg: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 4.0
        layer.shadowRadius = 4
        layer.shadowOpacity = 0.16
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.masksToBounds = false
        viewbg.layer.borderColor = Color.c134_142_150.cgColor
        viewbg.layer.borderWidth = 0.5
    }
}

class MPesaToAccountViewController: UIViewController,UIGestureRecognizerDelegate {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var txtmobile: UITextField!
    @IBOutlet weak var txtamount: UITextField!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var Clsprice: UICollectionView!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lbltranserfrom: UILabel!
    @IBOutlet weak var lblselectnetwork: UILabel!

    //Mark:- Varibales
    
    // Variables for Bottom sheet
    var editableTextfield : UITextField?
    private var lastContentOffset: CGFloat = 0
    let Pricearr = ["100", "250","500","1000"]
    var isMobileEditing : Bool = true
    var headerTitle = ""
    
    // Variables for Bottom sheet
    var bottomSheetVC: BottomSheetViewController?
      var imgView = UIImageView()
    
    var indexcolletion = IndexPath()
    var dictdata = NSMutableDictionary()
    var contactno = ""
    var contactname = ""
    var nominateflag = ""
    var selectnetworok = true
        var aMNO_WALLET_ID = ""
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add Bottom sheet VC
        addBottomSheetView()
        bottomSheetVC?.delegate = self
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        Clsprice?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
        setFonts()
        Clsprice.dataSource = self
        Clsprice.delegate = self
        txtmobile.delegate = self
        txtamount.delegate = self
//        if contactno == ""{
//            contactno = OnboardUser.mobileNo
//        }
        
        txtmobile.text = contactno
        txtamount.text = "KES"
       
               txtamount.keyboardType = .numberPad
        self.tabBarController?.tabBar.isHidden = true
        
     }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isMobileEditing == true {
           txtmobile.isEnabled = true
        }
        else {
          txtmobile.didBegin()
          txtmobile.isEnabled = false
        }
        txtamount.didBegin()
        txtamount.becomeFirstResponder()
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        if CurrentFlow.flowType == FlowType.SendMoney {
            headerTitle = "ACCOUNTTOPHONE"
            Fonts().set(object: self.lblsubtitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "HOWMUCHWOULDYOULIKETOTSEND", placeHolder: "")
            Fonts().set(object: self.lbltranserfrom, fontType: 1, fontSize: 9, color: Color.c0_123_255, title: "TRANSFERFROM", placeHolder: "")
                txtmobile.addUI(placeholder: "MPESAMOBILENUMBER")
            
        }
        else {
            headerTitle = "MPESATOACCOUNT"
            Fonts().set(object: self.lblsubtitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "HOWMUCHWOULDYOULIKETOTRANSFER", placeHolder: "")
             Fonts().set(object: self.lbltranserfrom, fontType: 1, fontSize: 9, color: Color.c0_123_255, title: "TRANSFERTO", placeHolder: "")
             txtmobile.addUI(placeholder: "MOBILENUMBER")
         }
        
        let accountArray = CoreDataHelper().getDataForEntity(entity: Entity.ACCOUNTS_INFO)
        if accountArray.count > 0 {
            let accountInfo = accountArray[0]
            let acNo = (accountInfo["aACCOUNT_NUMBER"] ?? "").hashingOfTheAccount(noStr: (accountInfo["aACCOUNT_NUMBER"] ?? ""))
            Fonts().set(object: self.lblaccount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: accountInfo["aACCOUNT_ALIAS"] ?? "", placeHolder: "")
            Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: acNo, placeHolder: "")
        }
        
        Fonts().set(object: self.lblselectnetwork, fontType: 0, fontSize: 13, color: Color.c10_34_64, title: "SELECTNETWORKPROVIDER", placeHolder: "")
        
        Fonts().set(object: self.txtamount, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtmobile, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        
        txtamount.addUI(placeholder: "ENTERAMOUNT")
    
        
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        
        initialSetUpNavigationBar()
    }
    
    // MARK: - Required Method's
    func initialSetUpNavigationBar(){
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headerTitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage:(UIImage(named: "GradientBar-2")?.resizableImage(withCapInsets: UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0), resizingMode: .stretch))!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
           self.view.endEditing(true)
        if txtamount.text == "KES "{
            txtamount.showError(errStr: "ENTERAMOUNT")
        }
        else if txtmobile.text == ""{
            txtmobile.showError(errStr: "ENTERMOBPLACEHOLDER")
        }
        else if lblselectnetwork.text == "Select Network Provider"{
            showMessage(title: "", message: "SELECTNETWORKPROVIDER")
        }
        else if lblaccountnumber.text == ""{
            showMessage(title: "", message: "SELECTACCOUNT")
        }
        else {
            if CurrentFlow.flowType == FlowType.MpesaToAccount{
              contactname = lblselectnetwork.text!
            }
            
            let dataModels = BuyaTimeVM.init(mobilenumber: txtmobile.text!, mobilename:  contactname, acalias: lblaccount.text!, aMNO_WALLET_ID: aMNO_WALLET_ID, amount:txtamount.text!, acnumber: lblaccountnumber.text!,nominateflag: "")
            let nextvc = CheckoutAirTimeViewController.getVCInstance() as! CheckoutAirTimeViewController
            nextvc.dataModels = dataModels
            nextvc.reasonForPayment = contactname
            self.navigationController?.pushViewController(nextvc, animated: true)
      }
    }
    
    @IBAction func networkprovider(_ sender: AttributedButton) {
        view.endEditing(true)
        selectnetworok = true
        self.showBottomSheetVC(xibtype: "NETWORK", Selectname: lblselectnetwork.text!)
    }
    
    @IBAction func accountselect(_ sender: AttributedButton) {
          view.endEditing(true)
            selectnetworok = false
            self.showBottomSheetVC(xibtype: "ACCOUNT", Selectname: lblaccount.text!)
      }
    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
   
    func showBottomSheetVC(xibtype: String,Selectname: String) {
        
        var height = 300  // 340
        let coreData = CoreDataHelper()
        if xibtype == "ACCOUNT" {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
                height = acdata.count * 200
                if acdata.count == 1 {
                    height = 200
                }
            }
        }
        else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acnetwork  = coreData.getDataForEntity(entity: Entity.MNO_INFO)
                height = acnetwork.count * 200
            }
        }
        
        // bottomSheetVC?.partialView = UIScreen.main.bounds.height - CGFloat(height)
        bottomSheetVC!.partialView = self.view.frame.size.height - CGFloat(height)
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imgView.backgroundColor = Color.black
        imgView.alpha = 0.2
        imgView.isUserInteractionEnabled = true
        self.view.addSubview(imgView)
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "", SelectName: "")
        imgView.removeFromSuperview()
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
}

//Text field delegate
extension MPesaToAccountViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtamount {
            txtamount.text = "KES "
            indexcolletion = IndexPath()
            Clsprice.reloadData()
        }
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtamount {
            
            textField.typingAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            let protectedRange = NSMakeRange(0, 4)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                
                return false
            }
            return true
        }
        else{
            return true
        }
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

//Text field delegate
extension MPesaToAccountViewController : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Pricearr.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PricebuttonCell1", for: indexPath as IndexPath) as! PricebuttonCell1
        if indexcolletion == indexPath {
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.white, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = Color.c0_123_255
        }else{
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = .clear
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        indexcolletion = indexPath
        txtamount.text = "KES \(self.Pricearr[indexPath.row])"
        txtamount.removeError()
        Clsprice.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.Clsprice.frame.width/4) - 8
        return CGSize(width: width, height: collectionView.frame.size.height - 5)
    }

}

extension MPesaToAccountViewController : SelectNetworkDelegate{
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
        lblaccountnumber.text =  acnumber
        lblaccount.text = acname
    }
    
    // Select network/ account in bottomshit
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
        hideBottomSheetVC()
        
        if selectnetworok == true {
            Fonts().set(object: self.lblselectnetwork, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: newtorkname, placeHolder: "")
            self.aMNO_WALLET_ID = networkid
        }
        else{
            lblaccount.text = newtorkname
            lblaccountnumber.text = networkid.hashingOfTheAccount(noStr: networkid)
        }
    }
}

extension MPesaToAccountViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}

