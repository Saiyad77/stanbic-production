//
//  AccountToPhoneViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 03/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class AccountToPhoneViewController: UIViewController, UIGestureRecognizerDelegate, UINavigationControllerDelegate {

    //Mark:- IBOUTLETS
    @IBOutlet weak var txtmobile: UITextField!
    @IBOutlet weak var txtamount: UITextField!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var Clsprice: UICollectionView!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lbltranserfrom: UILabel!
    @IBOutlet weak var txtuseracnumber: UITextField!
    @IBOutlet weak var txtenterAcnumber: UITextField!
    @IBOutlet weak var btnmyacount: UIButton!
    @IBOutlet weak var btnother: UIButton!
    @IBOutlet weak var imgmyacount: UIImageView!
    @IBOutlet weak var imgother: UIImageView!
    @IBOutlet weak var view_otherac: UIView!
    
    //Mark:- Varibales
    
    // Variables for Bottom sheet
    var editableTextfield : UITextField?
    private var lastContentOffset: CGFloat = 0
    let Pricearr = ["100", "250","500","1000"]
    var isMobileEditing : Bool = true
    
    // Variables for Bottom sheet
    var bottomSheetVC: BottomSheetViewController?
     var imgView = UIImageView()
    
    var indexcolletion = IndexPath()
    var dictdata = NSMutableDictionary()
    var contactno = ""
    var contactname = ""
    var nominateflag = ""
    var selectnetworok = true
    var aMNO_WALLET_ID = ""
var accountno = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add Bottom sheet VC
        addBottomSheetView()
        bottomSheetVC?.delegate = self
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        Clsprice?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
        setFonts()
        Clsprice.dataSource = self
        Clsprice.delegate = self
        txtmobile.delegate = self
        txtamount.delegate = self
        txtuseracnumber.delegate = self
        txtenterAcnumber.delegate = self
        //        if contactno == ""{
        //            contactno = OnboardUser.mobileNo
        //        }
        
        txtmobile.text = contactno
        txtamount.text = "KES"
        
        txtamount.keyboardType = .numberPad
        self.tabBarController?.tabBar.isHidden = true
        
        imgmyacount.image = UIImage.init(named: "Select_bluedot")
        
        btnmyacount.backgroundColor = Color.c239_247_255
        btnother.backgroundColor = Color.white
        
        btnmyacount.layer.masksToBounds = false
        btnmyacount.layer.cornerRadius = 5
        btnmyacount.clipsToBounds = true
        
        btnother.layer.masksToBounds = false
        btnother.layer.cornerRadius = 5
        btnother.clipsToBounds = true
        
        btnmyacount.layer.borderWidth = 1
        btnother.layer.borderWidth = 1
        
        btnmyacount.layer.borderColor = Color.c0_123_255.cgColor
        btnother.layer.borderColor = Color.c242_242_242.cgColor
        view_otherac.isHidden = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
  //     txtmobile.text = removeSpecialCharsFromString(text: txtmobile.text!)
        if txtamount.text!.count > 5 {
            
            txtmobile.removeError()
        } else {
            txtamount.didBegin()
            txtamount.becomeFirstResponder()
        }
        
        if isMobileEditing == true {
            txtmobile.isEnabled = true
        } else {
            txtmobile.didBegin()
            txtmobile.isEnabled = false
        }
        
        
        if CurrentFlow.flowType == FlowType.MpesaToAccount
        {
            txtmobile.text = "+\(OnboardUser.mobileNo)"
            txtmobile.didBegin()
        }
        if txtuseracnumber.text != ""{
        txtuseracnumber.didBegin()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        if CurrentFlow.flowType == FlowType.SendMoney{
            //Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: "ACCOUNTTOPHONE", placeHolder: "")
            Fonts().set(object: self.lblsubtitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "HOWMUCHWOULDYOULIKETOTSEND", placeHolder: "")
            Fonts().set(object: self.lbltranserfrom, fontType: 0, fontSize: 12, color: Color.c0_123_255, title: "TransferTo", placeHolder: "")
            txtmobile.addUI(placeholder: "MPESAMOBILENUMBER")
            initialSetUpNavigationBar(headertitle:"ACCOUNTTOPHONE")
            
        }
        else {
            Fonts().set(object: self.lblsubtitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "HOWMUCHWOULDYOULIKETOTRANSFER", placeHolder: "")
            Fonts().set(object: self.lbltranserfrom, fontType: 0, fontSize: 12, color: Color.c0_123_255, title: "TransferTo", placeHolder: "")
            txtmobile.addUI(placeholder: "MOBILENUMBER1")
             initialSetUpNavigationBar(headertitle:"MPESATOACCOUNT")
            
            Fonts().set(object: btnmyacount, fontType: 0, fontSize: 12, color: Color.c102_102_102, title: "MYACCOUNTS", placeHolder: "")
             Fonts().set(object: btnother, fontType: 0, fontSize: 12, color: Color.black, title: "OTHERACCOUND", placeHolder: "")
            Fonts().set(object: self.txtuseracnumber, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
            
            txtuseracnumber.addUI(placeholder: "ACCOUNTNUMBER")
            txtuseracnumber.isUserInteractionEnabled = false
            
             Fonts().set(object: self.txtenterAcnumber, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
             txtenterAcnumber.addUI(placeholder: "RECIPIENTACCOUNT")
        }
        
        let accountArray = CoreDataHelper().getDataForEntity(entity: Entity.ACCOUNTS_INFO)
        if accountArray.count > 0 {
            let accountInfo = accountArray[0]
            txtuseracnumber.text = accountInfo["aACCOUNT_NUMBER"] ?? ""
            let acNo = (accountInfo["aACCOUNT_NUMBER"] ?? "").hashingOfTheAccount(noStr: (accountInfo["aACCOUNT_NUMBER"] ?? ""))
            accountno = accountInfo["aACCOUNT_NUMBER"] ?? ""
            Fonts().set(object: self.lblaccount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: accountInfo["aACCOUNT_ALIAS"] ?? "", placeHolder: "")
            Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: acNo, placeHolder: "")
            accountno = accountInfo["aACCOUNT_NUMBER"] ?? ""
        }
        
        Fonts().set(object: self.txtamount, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtmobile, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        txtamount.addUI(placeholder: "ENTERAMOUNT")
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        
       
    }
    // MARK: - Required Method's
    func initialSetUpNavigationBar(headertitle:String){
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headertitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    //Mark:-  other - my acount selection
    @IBAction func Select(_ sender: UIButton) {
        // my account
        imgmyacount.image = UIImage.init(named: "unselect_white") //
        imgother.image = UIImage.init(named: "unselect_white")
        Fonts().set(object: btnmyacount, fontType: 0, fontSize: 12, color: Color.black, title: "MYACCOUNTS", placeHolder: "")
        Fonts().set(object: btnother, fontType: 0, fontSize: 12, color: Color.black, title: "OTHERACCOUND", placeHolder: "")
        
        if sender.tag == 101 {
        imgmyacount.image = UIImage.init(named: "Select_bluedot")
         Fonts().set(object: btnmyacount, fontType: 0, fontSize: 12, color: Color.c102_102_102, title: "MYACCOUNTS", placeHolder: "")
              self.view.endEditing(true)
            btnmyacount.backgroundColor = Color.c239_247_255
            btnother.backgroundColor = Color.white
            
            btnmyacount.layer.borderColor = Color.c0_123_255.cgColor
            btnother.layer.borderColor = Color.c242_242_242.cgColor
            setView(view: view_otherac, hidden: true)
        }
        // other account
        else{
        Fonts().set(object: btnother, fontType: 0, fontSize: 12, color: Color.c102_102_102, title: "OTHERACCOUND", placeHolder: "")
            imgother.image = UIImage.init(named: "Select_bluedot")
            btnother.backgroundColor = Color.c239_247_255
            btnmyacount.backgroundColor = Color.white
            
            btnother.layer.borderColor = Color.c0_123_255.cgColor
            btnmyacount.layer.borderColor = Color.c242_242_242.cgColor
            setView(view: view_otherac, hidden: false)
            txtenterAcnumber.becomeFirstResponder()
        }
     }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
                view.endEditing(true)
        if lblaccountnumber.text == ""{
            showMessage(title: "", message: "SELECTACCOUNT")
            return
        }
        let amounts = getAmountValdiation1(service_code : "STKTOPUP")
        
        let min = amounts.0
        let max = amounts.1
        let amount = removeKES(price : txtamount.text ?? "")
        let msg = "\("AMOUNTBEETWEEN".localized(Session.sharedInstance.appLanguage)) \(min) \("-".localized(Session.sharedInstance.appLanguage)) \(max) "
        self.view.endEditing(true)
        
        if txtamount.text == "KES "{
            txtamount.showError(errStr: "ENTERAMOUNT")
        }
        else if checkAmount(min:min,max:max,amount:amount) == false {
            txtamount.showError(errStr: msg)
        }
        else {
            if view_otherac.isHidden == true{
         if txtmobile.text == ""{
            txtmobile.showError(errStr: "ENTERMOBPLACEHOLDER")
         }
         else if lblaccountnumber.text == ""{
            showMessage(title: "", message: "SELECTACCOUNT")
            
        }
        else {
            Submit(ac: lblaccountnumber.text!)
        }
            }
            else {
                if txtenterAcnumber.text == ""{
                    txtenterAcnumber.showError(errStr: "RECIPIENTACCOUNT")
                    
                }
                else if txtenterAcnumber.text == "RECIPIENTACCOUNT".localized(Session.sharedInstance.appLanguage) {
                    txtenterAcnumber.showError(errStr: "RECIPIENTACCOUNT")
                    
                }
                else if getgenralRegax(value: txtenterAcnumber.text!, type: "ac") == false {
                    txtenterAcnumber.showError(errStr: "ACCOUNTREGAX")

                }
                else {
                    Submit(ac: txtenterAcnumber.text!)
                }
            }
      }
     
    }
        
    func Submit(ac:String){
         if view_otherac.isHidden == false {
          lblaccount.text! = txtenterAcnumber.text!
        }
        
            let dataModels = BuyaTimeVM.init(mobilenumber: txtmobile.text!, mobilename:  contactname, acalias: lblaccount.text!, aMNO_WALLET_ID: aMNO_WALLET_ID, amount:txtamount.text!, acnumber: lblaccountnumber.text!,nominateflag: "")
            let nextvc = CheckoutAirTimeViewController.getVCInstance() as! CheckoutAirTimeViewController
            nextvc.dataModels = dataModels
            nextvc.reasonForPayment = contactname
             nextvc.accountno = accountno
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    
    @IBAction func accountselect(_ sender: AttributedButton) {
        view.endEditing(true)
        selectnetworok = false
        self.showBottomSheetVC(xibtype: "ACCOUNT", Selectname: lblaccount.text!)
    }
    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
   
    func showBottomSheetVC(xibtype: String,Selectname: String) {
        
        var height = 300  // 340
        let coreData = CoreDataHelper()
        if xibtype == "ACCOUNT" {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
                height = acdata.count * 100
                if acdata.count == 1 {
                    height = 200
                }
            }
        }
        else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acnetwork  = coreData.getDataForEntity(entity: Entity.MNO_INFO)
                height = acnetwork.count * 200
            }
        }
        
        // bottomSheetVC?.partialView = UIScreen.main.bounds.height - CGFloat(height)
        bottomSheetVC!.partialView = self.view.frame.size.height - CGFloat(height)
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imgView.backgroundColor = Color.black
        imgView.alpha = 0.2
        imgView.isUserInteractionEnabled = true
        self.view.addSubview(imgView)
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "", SelectName: "")
        imgView.removeFromSuperview()
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
}

//Text field delegate
extension AccountToPhoneViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtamount {
            txtamount.text = "KES "
            indexcolletion = IndexPath()
            Clsprice.reloadData()
        }
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtamount {
            
            textField.typingAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            let protectedRange = NSMakeRange(0, 4)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                
                return false
            }
            return true
        }
        else if txtmobile == textField{
                let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
                let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return string == numberFiltered
        }else{
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
    }

    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

//Text field delegate
extension AccountToPhoneViewController : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Pricearr.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PricebuttonCell1", for: indexPath as IndexPath) as! PricebuttonCell1
        if indexcolletion == indexPath {
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.white, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = Color.c0_123_255
        }else{
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = .clear
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        indexcolletion = indexPath
        txtamount.text = "KES \(self.Pricearr[indexPath.row])"
        txtamount.removeError()
        Clsprice.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.Clsprice.frame.width/4) - 8
        return CGSize(width: width, height: collectionView.frame.size.height - 5)
    }
    
}

extension AccountToPhoneViewController : SelectNetworkDelegate{
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
        lblaccountnumber.text =  acnumber
        lblaccount.text = acname
        
        
    }
    
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
        hideBottomSheetVC()
        lblaccount.text = newtorkname
        lblaccountnumber.text = networkid.hashingOfTheAccount(noStr: networkid)
        txtuseracnumber.text = networkid
        accountno = networkid
    }
}

extension AccountToPhoneViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
           //  self.view.endEditing(true)
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.2, options: .curveEaseIn, animations: {
            view.isHidden = hidden
        })
    }
}


