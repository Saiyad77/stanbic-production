//
//  LoginSecurityViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 04/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication

class LoginSecurityCell: UITableViewCell {
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imgbranch: UIImageView!
    @IBOutlet weak var imgcheckmark: UIImageView!
    
    override func awakeFromNib() {
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 17, color: Color.black, title: "", placeHolder: "")
//        imgbranch.layer.cornerRadius = imgbranch.frame.size.height / 2
//        imgbranch.layer.borderWidth = 0.5
//        imgbranch.layer.borderColor = UIColor.lightGray.cgColor
//        imgbranch.layer.masksToBounds = true
    }
    
    func setData(title:String,img:String){
        lbltitle.text = title
        imgbranch.image = UIImage.init(named: img)
    }
}

class LoginSecurityViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var tableview: UITableView!
    
    var arrtitle = [String]();
    var  arrimages = [String]();
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
        checkUserDeviceSupport()
       
    }
    // MARK: - Required Method's
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp4.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 17, color: Color.black, title: "LOGINANDSECURITY", placeHolder: "")
     }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//UITableViewDataSource ,delegates
extension LoginSecurityViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return arrtitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoginSecurityCell", for: indexPath) as! LoginSecurityCell
        
        cell.setData(title: arrtitle[indexPath.row], img: arrimages[indexPath.row])
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let nextvc = Thumb_FaceIDViewController.getVCInstance() as! Thumb_FaceIDViewController
            self.navigationController?.pushViewController(nextvc, animated: true)
        default:
            return
        }
    }
}

extension LoginSecurityViewController  {

    func checkUserDeviceSupport()
    {
        //IF USER DEVICE SUPPORT FACE ID, CHANGE LABEL ON SCREEN
        let context = LAContext()
        if Authntication(context).checkUserDeviceFaceIDSupport() == true
        {
           arrtitle = ["Face ID"]
            arrimages = ["faceid"]
        }
        
        else {
             
             arrtitle = ["Touch ID",]
             arrimages = ["fingerprintblue"]
          }
        
        tableview.reloadData()
        }
}
