//
//  SettingViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 08/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class SettingViewCell: UITableViewCell {
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    
    override func awakeFromNib() {
        Fonts().set(object: lbltitle, fontType: 1, fontSize: 17, color: Color.c0_123_255, title: "", placeHolder: "")
        Fonts().set(object: lblsubtitle, fontType: 0, fontSize: 13, color: Color.c134_142_150, title: "", placeHolder: "")
    }
    
    func setdata(title:String,subtitle:String){
        lbltitle.text = title
        lblsubtitle.text = subtitle
    }
    
}

class SettingViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    
    //  let arrdict = ["Manage beneficiaries", "Change your PIN", "Login and Security","Notifications"]
    // ["Manage your saved beneficiaries", "Change your mobile banking PIN", "Keep your account safe and secure","Allow push notifications"]
    
    let arrdata = [["title":"Manage beneficiaries","subtitle":"Manage your saved beneficiaries"],["title":"Change your PIN","subtitle":"Change your mobile banking PIN"],["title":"Login and Security","subtitle":"Keep your account safe and secure"]]
    
    // ["title":"Notifications","subtitle":"Allow push notifications"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        initialSetUpNavigationBar(headerTitle:"SETTING")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: - Required Method's
    func initialSetUpNavigationBar(headerTitle:String){
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headerTitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    static func getVCInstance() -> UIViewController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Checkbook.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
       global.cupApi = "NO"
     setUpSlideMenu()
    }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
    
}

extension SettingViewController:UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return arrdata.count
  
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingViewCell", for: indexPath) as! SettingViewCell
        let dict =  arrdata[indexPath.row]
        cell.setdata(title: dict["title"]!, subtitle: dict["subtitle"]!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let nextvc = ManagebenificiallyViewController.getVCInstance() as! ManagebenificiallyViewController
            self.navigationController?.pushViewController(nextvc, animated: true)
        case 1:
            let nextvc = ChangePinCurrentVC.getVCInstance() as! ChangePinCurrentVC
            self.navigationController?.pushViewController(nextvc, animated: true)
        case 2:
            let nextvc = LoginSecurityViewController.getVCInstance() as! LoginSecurityViewController
            self.navigationController?.pushViewController(nextvc, animated: true)
        default:
            break;
        }
    }
 }
