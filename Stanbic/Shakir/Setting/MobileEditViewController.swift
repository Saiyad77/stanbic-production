//
//  MobileEditViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 17/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class MobileEditViewController: UIViewController {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnPickImg: UIButton!
    @IBOutlet weak var txtBeneficiaryBank: UITextField!
    @IBOutlet weak var txtBankBranch: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var btnEditDetail: AttributedButton!
    @IBOutlet weak var btnDeleteBeneficiary: AttributedButton!
    @IBOutlet weak var consBottombtn: NSLayoutConstraint!
    //MARK: Variables
    private var lastContentOffset: CGFloat = 0
    var name: String = ""
    var arrBankInfo = [String: String]()
    //MARK: View's Life Cycle
    var ProfileID = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        initialFontSetup()
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // txtBankBranch.text = arrBankInfo["aENROLLMENT_ALIAS"]
     
        setData()
        if CoreDataHelper().getDataForEntity(entity: Entity.ACCOUNTS_INFO).count > 0{
        let acdata  = CoreDataHelper().getDataForEntity(entity: Entity.ACCOUNTS_INFO)
        ProfileID = acdata[acdata.count - 1]["aPROFILE_ID"] ?? ""
        }
        
     }
    
    // MARK: Required methods
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp4.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func initialFontSetup()
    {
        txtPhoneNumber.delegate = self
        txtBeneficiaryBank.delegate = self
        txtBankBranch.delegate = self
        txtEmailAddress.delegate = self
        
        txtPhoneNumber.isUserInteractionEnabled = false
        txtBeneficiaryBank.isUserInteractionEnabled = false
        txtBankBranch.isUserInteractionEnabled = false
        txtEmailAddress.isUserInteractionEnabled = false
  
        Fonts().set(object: txtBeneficiaryBank, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
     
        Fonts().set(object: txtBankBranch, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
       
        Fonts().set(object: txtEmailAddress, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
     
        Fonts().set(object: txtPhoneNumber, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
     
        Fonts().set(object: btnEditDetail, fontType: 1, fontSize: 14, color: Color.white, title: "EDITDETAILS", placeHolder: "")
        Fonts().set(object: btnDeleteBeneficiary, fontType: 1, fontSize: 14, color: Color.c0_123_255, title: "DELETEBENEFICIARY", placeHolder: "")
        initialSetUpNavigationBar(headerTitle:"MOBILEACCOUNT")
    }
    
    func initialSetUpNavigationBar(headerTitle:String)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headerTitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
     
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txtPhoneNumber.didBegin()
        txtBeneficiaryBank.didBegin()
        txtBankBranch.didBegin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    // set data to UI
    func setData() {
        txtBeneficiaryBank.addUI(placeholder: "ENROLLMEN ALIAS")
        txtBankBranch.addUI(placeholder: "BILLER")
        txtPhoneNumber.addUI(placeholder: "BILLER REFERENCE")
        txtEmailAddress.addUI(placeholder: "EMAILADDRESS")
        
            txtBankBranch.text = arrBankInfo["aBILLER"]!
            txtBeneficiaryBank.text = arrBankInfo["aENROLLMENT_ALIAS"]
            txtPhoneNumber.text = arrBankInfo["aBILLER_REFERENCE"]
        
       }
    
    //MARK:Actions
    @IBAction func btnTappedEditProfile(_ sender: AttributedButton) {
        let str = "EDITDETAILS"
        if btnEditDetail.currentTitle == str.localized(Session.sharedInstance.appLanguage){
         Fonts().set(object: btnEditDetail, fontType: 1, fontSize: 14, color: Color.white, title: "UPDATEDETAIL", placeHolder: "")

            txtPhoneNumber.isUserInteractionEnabled = true
            txtBeneficiaryBank.isUserInteractionEnabled = true
            txtEmailAddress.isUserInteractionEnabled = true
        }
        else{

            if  txtBeneficiaryBank.text == "" {
                txtBeneficiaryBank.showError(errStr: arrBankInfo["ENROLLMENT ALIAS"] ?? "")
            }
            else if txtPhoneNumber.text == "" {
               txtPhoneNumber.showError(errStr: arrBankInfo["BILLER REFERENCE"] ?? "")
            }
            else {
            updateMobile(action: "UPDATE", benificillyactype: arrBankInfo["aBENEFICIARY_TYPE"]!.description, reciepentalias: arrBankInfo["aENROLLMENT_ALIAS"]!.description, acnumber: arrBankInfo["aBILLER_REFERENCE"]!.description, profileid: ProfileID, newRecipetAalias:  txtBeneficiaryBank.text!, newaccountno:  txtPhoneNumber.text!)
            }
      }
    }
    
    @IBAction func btnTappedDeleteBeneficiary(_ sender: AttributedButton) {

        updateMobile(action: "DELETE", benificillyactype: arrBankInfo["aBENEFICIARY_TYPE"]!.description, reciepentalias: arrBankInfo["aENROLLMENT_ALIAS"]!.description, acnumber: arrBankInfo["aBILLER_REFERENCE"]!.description, profileid: ProfileID, newRecipetAalias:  arrBankInfo["aENROLLMENT_ALIAS"]!.description, newaccountno:  arrBankInfo["aBILLER_REFERENCE"]!.description)
    }
    
    @IBAction func barBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//Text field delegate
extension MobileEditViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
}

extension MobileEditViewController
{
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottombtn.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottombtn.constant = 100
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

extension MobileEditViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //        self.view.endEditing(true)
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        Global.invalidTimer()
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}

extension MobileEditViewController {

   //   Beneficiary Management
    func updateMobile (action:String,benificillyactype:String,reciepentalias:String,acnumber:String,profileid:String,newRecipetAalias:String,newaccountno:String) {
        
        RestAPIManager.beneficiaryManagement(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOURREQUEST", type: Response.self, mobileNo: OnboardUser.mobileNo,pin: OnboardUser.pin, action: action, benificillyactype: benificillyactype, reciepentalias: reciepentalias, acnumber: acnumber, profileid: profileid, newRecipetAalias: newRecipetAalias, newaccountno: newaccountno, completion: { (response) in
            DispatchQueue.main.async {
                if response.statusCode == 200 {
                
                    let nextvc = SuccessViewController.getVCInstance() as! SuccessViewController
                    nextvc.link = "Manage beneficiary"
                    nextvc.msg = response.statusMessage ?? ""
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
               let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                    nextvc.message = response.statusMessage ?? ""
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
             }
        }) { (error) in
            
        }
    }
    
    // Model : - for FETCH
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
      //  let resultArray: Result?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
           // case resultArray = "RESULTS_ARRAY"
        }
    }
}
