//
//  ManagebenificiallyViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 08/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class ManagebenificiallyViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var btnbankac: UIButton!
    @IBOutlet weak var paybill: UIButton!
    @IBOutlet weak var slideView: UIView!
    @IBOutlet weak var tableViewcontacts: UITableView!
    @IBOutlet weak var consViewSlide: NSLayoutConstraint!
    
    // Varriables -
    var arrMobileInfo = [[String:String]]()
    var arrBankInfo = [[String:String]]()
    var arrPayBillInfo = [[String:String]]()
    let coreData = CoreDataHelper()
    var contact = "Bank"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getData()
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Checkbook.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: lbltitle, fontType: 1, fontSize: 17, color: Color.white, title:"MANAGEBENIFICIARY", placeHolder: "")
        
        Fonts().set(object: btnbankac, fontType: 1, fontSize: 13, color: Color.c134_142_150, title:"BANKACCOUNT", placeHolder: "")
       // Fonts().set(object: btnmobileac, fontType: 1, fontSize: 13, color: Color.c134_142_150, title:"MOBILEACCOUNT", placeHolder: "")
        Fonts().set(object: paybill, fontType: 1, fontSize: 13, color: Color.c134_142_150, title:"PAYBILL", placeHolder: "")
        
        //    Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "ADDANEWBENEFICIARY", placeHolder: "")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func bankAccount(_ sender: UIButton) {
        btnbankac.setTitleColor(Color.c10_34_64, for: .normal)
      //  btnmobileac.setTitleColor(Color.c134_142_150, for: .normal)
        paybill.setTitleColor(Color.c134_142_150, for: .normal)
        
        let xPosition = btnbankac.frame.origin.x + 25
        //  consViewSlide.constant = 70
        //View will slide 20px up
        let yPosition = slideView.frame.origin.y
        
        let height = slideView.frame.size.height
         let width = btnbankac.frame.width - 10
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.slideView.frame = CGRect(x: xPosition, y: yPosition, width: width, height: height)
            
        })
        contact = "Bank"
        tableViewcontacts.reloadData()
    }
    
    @IBAction func mobileAccount(_ sender: UIButton) {
//        btnmobileac.setTitleColor(Color.c10_34_64, for: .normal)
//        btnbankac.setTitleColor(Color.c134_142_150, for: .normal)
//        paybill.setTitleColor(Color.c134_142_150, for: .normal)
//
//        let xPosition = btnmobileac.frame.origin.x + 25
//        // consViewSlide.constant = 70
//
//        //View will slide 20px up
//        let yPosition = slideView.frame.origin.y
//
//        let height = slideView.frame.size.height
//        let width = CGFloat(70)
//
//        UIView.animate(withDuration: 0.5, animations: {
//            self.slideView.frame = CGRect(x: xPosition, y: yPosition, width: width, height: height)
//
//        })
//        contact = "Mobile"
//        tableViewcontacts.reloadData()
    }
    
    @IBAction func payBill(_ sender: UIButton) {
        btnbankac.setTitleColor(Color.c134_142_150, for: .normal)
       // btnmobileac.setTitleColor(Color.c134_142_150, for: .normal)
        paybill.setTitleColor(Color.c10_34_64, for: .normal)
        // consViewSlide.constant = 50
        //  let xPosition = slideView.frame.origin.x - 250
        let xPosition = paybill.frame.origin.x + 25
        
        //View will slide 20px up
        let yPosition = slideView.frame.origin.y
        
        let height = slideView.frame.size.height
        let width = paybill.frame.width - 10
        
        UIView.animate(withDuration: 0.5, animations: {
            self.slideView.frame = CGRect(x: xPosition, y: yPosition, width: width, height: height)
        })
        contact = "Paybill"
        tableViewcontacts.reloadData()
    }
    
    @IBAction func newadd(_ sender: UIButton) {
        
    }
}


extension ManagebenificiallyViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if contact == "Bank"{
            return arrBankInfo.count
        }
//        else if contact == "Mobile"{
//            return arrMobileInfo.count
//        }
        else {
            return arrPayBillInfo.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "managebenificially", for: indexPath) as! ContactTableViewCell
        cell.roundImage()
        cell.roundLabel()
        
        if contact == "Bank"{
            
            arrBankInfo.sort(by: {($0["aNOMINATION_ALIAS"] as! String) < $1["aNOMINATION_ALIAS"] as! String})
            
            cell.viewcard.isHidden = true
            let contacts = arrBankInfo[indexPath.row]
            
            
            if contacts["aBENEFICIARY_TYPE"]! == "15" || contacts["aBENEFICIARY_TYPE"]! == "16"{
                
                let accountno = "\(contacts["aBANK_NAME"]!) A/C \(contacts["aNOMINATED_ACCOUNT"]!.hashingOfTheAccount(noStr:contacts["aNOMINATED_ACCOUNT"]!)) "

                   cell.setdata(name: contacts["aNOMINATION_ALIAS"]!, number: accountno,flag: "")
            }
            else {
                if contacts["aBENEFICIARY_TYPE"]! == "17"{
                    let accountno = "\(contacts["aBANK_NAME"]!) A/C \(contacts["aNOMINATED_ACCOUNT"]!.hashingOfTheAccount(noStr:contacts["aNOMINATED_ACCOUNT"]!)) "
                    
                    cell.setdata(name: contacts["aNOMINATION_ALIAS"]!, number: accountno,flag: "")
                }
                
                if contacts["aBENEFICIARY_TYPE"]! == "1"{
                    let accountno = "\(contacts["aBANK_NAME"]!) A/C \(contacts["aNOMINATED_ACCOUNT"]!.hashingOfTheAccount(noStr:contacts["aNOMINATED_ACCOUNT"]!)) "
                    
                    cell.setdata(name: contacts["aNOMINATION_ALIAS"]!, number: accountno,flag: "")
                }
                if contacts["aBENEFICIARY_TYPE"]! == "2"{
                    let accountno = "\(contacts["aBANK_NAME"]!) A/C \(contacts["aNOMINATED_ACCOUNT"]!.hashingOfTheAccount(noStr:contacts["aNOMINATED_ACCOUNT"]!)) "
                    
                    cell.setdata(name: contacts["aNOMINATION_ALIAS"]!, number: accountno,flag: "")
                }
            }
            cell.imgContact.isHidden = true
            cell.nameLetters.isHidden = false
            
        }
//        else if contact == "Mobile"{
//             arrMobileInfo.sort(by: {($0["aENROLLMENT_ALIAS"] as! String) < $1["aENROLLMENT_ALIAS"] as! String})
//            cell.viewcard.isHidden = true // 5,7
//            let contacts = arrMobileInfo[indexPath.row]
//            cell.setdata(name: contacts["aENROLLMENT_ALIAS"]!, number: contacts["aBILLER_REFERENCE"]!, flag: "")
//        }
        else {
             Fonts().set(object: cell.cardnameLabel, fontType: 1, fontSize: 13, color: Color.black, title: "", placeHolder: "")
            cell.viewcard.isHidden = false
                arrPayBillInfo.sort(by: {($0["aENROLLMENT_ALIAS"] as! String) < $1["aENROLLMENT_ALIAS"] as! String})
            let contacts = arrPayBillInfo[indexPath.row]
            
             cell.cardnameLabel.text = contacts["aENROLLMENT_ALIAS"] ?? ""
           // let name = contacts["aENROLLMENT_ALIAS"] ?? ""
           // cell.nameLabel.text = name
           // cell.nameLetters.text = (name.characters.first?.description ?? "");
            cell.nameLetters.isHidden = true
               cell.imgContact.sd_setImage(with: URL(string: contacts["aBILLER_LOGO"] ?? ""), placeholderImage: UIImage(named: "placeholder"))
            cell.imgContact.isHidden = false
            
               cell.imgContact.contentMode = .scaleAspectFit
          
            if contacts["aBILLER_LOGO"] ?? "" == ""{
             let name = contacts["aENROLLMENT_ALIAS"] ?? ""
             cell.nameLabel.text = name
             cell.nameLetters.text = (name.characters.first?.description ?? "").capitalizingFirstLetter();
             cell.nameLetters.isHidden = false
                     cell.imgContact.isHidden = true
            }
            else {
                cell.nameLetters.isHidden = true
                 cell.imgContact.isHidden = false
            }
            
            cell.imgContact.layer.cornerRadius = cell.imgContact.frame.size.height / 2
            cell.imgContact.layer.borderColor = Color.c233_233_235.cgColor
            cell.imgContact.layer.borderWidth = 1.0
           
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if contact == "Bank"
        {
            let contacts = arrBankInfo[indexPath.row]
//            let banker = ["1","2","3","4","6"]
//            if banker.contains(contacts["aBENEFICIARY_TYPE"]!.description)
//            {
                let nextVC = BankEditViewController.getVCInstance() as! BankEditViewController
                nextVC.arrBankInfo = contacts
                nextVC.name = "Bank"
                self.navigationController?.pushViewController(nextVC, animated: true)
           // }
        }
//        else if contact == "Mobile"{
//            let nextVC = MobileEditViewController.getVCInstance() as! MobileEditViewController
//            nextVC.arrBankInfo = arrMobileInfo[indexPath.row]
//            nextVC.name = "Mobile"
//            self.navigationController?.pushViewController(nextVC, animated: true)
//        }
        else {
            let nextVC = BillEditViewController.getVCInstance() as! BillEditViewController
            nextVC.arrBankInfo = arrPayBillInfo[indexPath.row]
            nextVC.name = "PayBill"
            self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
}

extension ManagebenificiallyViewController {
    
    func getData(){
        
         var arrdummy = [[String:String]]()
        
        self.arrBankInfo.removeAll()
        self.arrPayBillInfo.removeAll()
        self.arrMobileInfo.removeAll()
        
        let enrollmentInfo =  CoreDataHelper().getDataForEntity(entity: Entity.ENROLLMENTS_INFO)
        let beneficiariesInfo =  CoreDataHelper().getDataForEntity(entity: Entity.BENEFICIARIES_INFO)
        
        // ENROLLMENTS_INFO
        for i in 0..<enrollmentInfo.count {
            let contacts = enrollmentInfo[i]
            
            // mobile   // || contacts["aBENEFICIARY_TYPE"] == "7"
//            if contacts["aBENEFICIARY_TYPE"] == "5" {
//                arrMobileInfo.append(contacts)
//            }
            
            // Bank
            let bankar = ["1","2","11","12","13","15","16","17"]
            if bankar.contains(contacts["aBENEFICIARY_TYPE"]!.description){
                arrBankInfo.append(contacts)
            }
            
            // PayBill
            if contacts["aBENEFICIARY_TYPE"] == "4" || contacts["aBENEFICIARY_TYPE"] == "6"{
                arrdummy.append(contacts)
            }
        }
        
        // BENEFICIARIES_INFO
        for i in 0..<beneficiariesInfo.count {
            let contacts = beneficiariesInfo[i]
            
            // mobile   // || contacts["aBENEFICIARY_TYPE"] == "7"
//            if contacts["aBENEFICIARY_TYPE"] == "5" {
//                arrMobileInfo.append(contacts)
//            }
            
            // Bank
            let bankar = ["1","2","11","12","13","15","16","17"]
            if bankar.contains(contacts["aBENEFICIARY_TYPE"]!.description){
                arrBankInfo.append(contacts)
            }
            
            // PayBill
            if contacts["aBENEFICIARY_TYPE"] == "4" || contacts["aBENEFICIARY_TYPE"] == "6"{
                arrdummy.append(contacts)
            }
        }
        
         let arrBilldata =  CoreDataHelper().getDataForEntity(entity: Entity.BILLERS_INFO)
        
        for info in arrBilldata{
            for beneInfo in arrdummy{
                if info["aBILLER"] == beneInfo["aBILLER"] {
                    var temp = info
                    temp["aBILLER_REFERENCE"] = beneInfo["aBILLER_REFERENCE"];
                    temp["aENROLLMENT_ALIAS"] = beneInfo["aENROLLMENT_ALIAS"]
                    temp["aBENEFICIARY_TYPE"] = beneInfo["aBENEFICIARY_TYPE"]
                    arrPayBillInfo.append(temp)
                }
                else if beneInfo["aBILLER"] == "CARD" {
                    var temp = beneInfo
                    temp["aBILLER_REFERENCE"] = beneInfo["aBILLER_REFERENCE"]
                    temp["aENROLLMENT_ALIAS"] = beneInfo["aENROLLMENT_ALIAS"]
                    temp["aBILLER_LOGO"] = ""
                    arrPayBillInfo.append(temp)
                }
                else{
                    
                }
            }
        }
        
        arrPayBillInfo = noDuplicates(arrPayBillInfo)
        tableViewcontacts.reloadData()
    }
    
    func noDuplicates(_ arrayOfDicts: [[String: String]]) -> [[String: String]] {
        var noDuplicates = [[String: String]]()
        var usedNames = [String]()
        for dict in arrayOfDicts {
            if let name = dict["aENROLLMENT_ALIAS"], !usedNames.contains(name) {
                noDuplicates.append(dict)
                usedNames.append(name)
            }
        }
        return noDuplicates
    }
}
