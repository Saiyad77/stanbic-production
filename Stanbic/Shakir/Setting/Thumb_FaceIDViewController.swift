//
//  Thumb_FaceIDViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 04/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication

class Thumb_FaceIDViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblid: UILabel!
    @IBOutlet weak var switchID: UISwitch!
    
    var iD = false
    var isFaceIDSupport : Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
        checkUserDeviceSupport()
        
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            switchID.setOn(true, animated: true)
        }
          else {
              switchID.setOn(false, animated: true)
        }
     }
    
    // MARK: - Required Method's
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp4.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // Switch Action Value Changed
    @IBAction func switchValueChanged(sender: UISwitch) {
        
        var supportid = ""
        let context = LAContext()
        context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        
        if Authntication(context).checkUserDeviceFaceIDSupport() == true
        {
            supportid = "Login with Face ID"
        }
        else
        {
            supportid = "Login with Touch ID"
        }
        if context.evaluatedPolicyDomainState != nil {
            if switchID.isOn {
                iD = true
                validAuth()
            }
            else {
                iD = false
                validAuth()
            }
        }
        else {
            showMessage(title: "", message: "\(supportid) not enable")
        }
       
     }
}

extension Thumb_FaceIDViewController  {
    
    func checkUserDeviceSupport()
    {
        //IF USER DEVICE SUPPORT FACE ID, CHANGE LABEL ON SCREEN
        let context = LAContext()
        if Authntication(context).checkUserDeviceFaceIDSupport() == true
        {
              isFaceIDSupport = true
          
            Fonts().set(object: self.lblid, fontType: 1, fontSize: 17, color: Color.black, title: "Allow transactions with Face ID", placeHolder: "")
            Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 17, color: Color.black
                , title: "FACEIDSETTING", placeHolder: "")
        }
        else {
             isFaceIDSupport = false
        
            Fonts().set(object: self.lblid, fontType: 1, fontSize: 17, color: Color.black, title: "Allow transactions with Touch ID", placeHolder: "")
            Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 17, color: Color.black
                , title: "TOUCHIDSETTING", placeHolder: "")
        }
    }
}

extension Thumb_FaceIDViewController :AuthDelegate   {
    
    func validate() {
        //Check if user enable biometric access
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            let context = LAContext()
            Authntication(context).authnticateUser { (success, message) in
                DispatchQueue.main.async {
                    if success == true {
                        self.validAuth()
                    }
                    else {
                        if message != "" {
                            self.showMessage(title: "", message: message)
                        }
                    }
                }
            }
        }
        else {
            let vc = AuthoriseViewController.getVCInstance() as! AuthoriseViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func validAuth() {
        if iD == true{
        
        authnticateUser()
        }
        else {
          switchID.setOn(false, animated: true)
          UserDefaults.standard.set(false, forKey: UserDefault.biometric)
        }
    }
    
   
    func authnticateUser() {
        //CHECK USER'S FINGERPRINT OR FACE ID
        let context = LAContext()
        Authntication(context).authnticateUser { (success, message) in
            if success == true {
                DispatchQueue.main.async {
                    //Set flag in user defaults for biometric access
                    UserDefaults.standard.set(true, forKey: UserDefault.biometric)
                    let vc = BiometricSuccessViewController.getVCInstance() as! BiometricSuccessViewController
                    vc.isFaceSupport = self.isFaceIDSupport
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else {
                DispatchQueue.main.async {
                    if message != "" {
                        self.switchID.setOn(false, animated: true)
                        //Set flag in user defaults for biometric access
                        UserDefaults.standard.set(false, forKey: UserDefault.biometric)
                        self.showMessage(title: "", message: message)
                    }
                }
            }
        }
    }
}


