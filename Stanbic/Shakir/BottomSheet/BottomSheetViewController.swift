//
//  BottomSheetViewController.swift
//  Ecobank
//
//  Created by Vijay Patidar on 22/03/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

protocol SelectNetworkDelegate{
    func selectnewtwork(newtorkname:String,networkid:String)
    func acDetails(acname:String,acnumber:String)
}

@available(iOS 11.0, *)
class BottomSheetViewController: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    let fullView: CGFloat = 500
    var partialView: CGFloat = UIScreen.main.bounds.height
    
    var rowsAreChecked = [IndexPath]()
    var delegate : SelectNetworkDelegate?
    
    var arrayAccounts = [[String:String]]()
    var arrayNetwork = [[String:String]]()
    
    var xibtype = ""
    
    var SelectName =  ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblHeaderTitle.text = ""
        
        tableView.register(UINib(nibName: "BottomSheetTableViewCell", bundle: nil), forCellReuseIdentifier: "CELL")
        tableView.register(UINib(nibName: "BottomSheetAccountTableViewCell", bundle: nil), forCellReuseIdentifier: "BottomSheetAccountTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(BottomSheetViewController.panGesture))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
        
        let coreData = CoreDataHelper()
        arrayAccounts = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
        arrayNetwork = coreData.getDataForEntity(entity: Entity.MNO_INFO)
        
        if arrayAccounts.count > 0 {
            delegate?.acDetails(acname: arrayAccounts[0]["aACCOUNT_ALIAS"]!, acnumber: arrayAccounts[0]["aACCOUNT_NUMBER"]!)
        }
       // tableView.roundCorners(corners: [.topLeft, .topRight], radius: 5)
        tableView.reloadData()
        
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
    
        //Here I'm masking the textView's layer with rectShape layer
      
    }
    
    override func viewDidLayoutSubviews() {
        //   headerView.roundCorners(corners: [.topLeft, .topRight], radius: 10)
    }
    
    func setFonts(xibtype:String)
    {
        //btnEdit.isHidden = true
        if xibtype == "NETWORK"{
            Fonts().set(object: lblHeaderTitle, fontType: 1, fontSize: 13, color: Color.c134_142_150, title: "SELECTNETWORKPROVIDER", placeHolder: "")
        }
        else{
            Fonts().set(object: lblHeaderTitle, fontType: 1, fontSize: 13, color: Color.c134_142_150, title: "SELECTACCOUNTTOSENDMONEYFROM", placeHolder: "")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //prepareBackgroundView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        moveBottomSheet(xibtype: "", SelectName: "")
    }
    
    func moveBottomSheet(xibtype:String,SelectName:String) {
        UIView.animate(withDuration: 0.4, animations: { [weak self] in
            let frame = self?.view.frame
            let yComponent = self?.partialView
            self?.view.frame = CGRect(x: 0, y: yComponent!, width: frame!.width, height: frame!.height)
        })
        setFonts(xibtype: xibtype)
        self.xibtype = xibtype
        self.SelectName = SelectName
        tableView.reloadData()
    }
    
    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
        
        let translation = recognizer.translation(in: self.view)
        let velocity = recognizer.velocity(in: self.view)
        
        let y = self.view.frame.minY
        if (y + translation.y >= fullView) && (y + translation.y <= partialView) {
            self.view.frame = CGRect(x: 0, y: y + translation.y, width: view.frame.width, height: view.frame.height)
            recognizer.setTranslation(CGPoint.zero, in: self.view)
        }
        
        if recognizer.state == .ended {
            var duration =  velocity.y < 0 ? Double((y - fullView) / -velocity.y) : Double((partialView - y) / velocity.y )
            
            duration = duration > 1.3 ? 1 : duration
            
            UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: {
                if  velocity.y >= 0 {
                    self.view.frame = CGRect(x: 0, y: self.partialView, width: self.view.frame.width, height: self.view.frame.height)
                } else {
                    self.view.frame = CGRect(x: 0, y: self.fullView, width: self.view.frame.width, height: self.view.frame.height)
                }
                
            }, completion: { [weak self] _ in
                if ( velocity.y < 0 ) {
                    self?.tableView.isScrollEnabled = true
                }
            })
        }
    }
    
    
    func prepareBackgroundView(){
        let blurEffect = UIBlurEffect.init(style: .regular)
        let visualEffect = UIVisualEffectView.init(effect: blurEffect)
        let bluredView = UIVisualEffectView.init(effect: blurEffect)
        bluredView.contentView.addSubview(visualEffect)
        visualEffect.frame = UIScreen.main.bounds
        bluredView.frame = UIScreen.main.bounds
        view.insertSubview(bluredView, at: 0)
    }
}


extension BottomSheetViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if xibtype == "NETWORK" {
            return arrayNetwork.count
        }
        else {
            return arrayAccounts.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if xibtype == "NETWORK"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as! BottomSheetTableViewCell
            let dictdata = arrayNetwork[indexPath.row]
             var name = ""
              if CurrentFlow.flowType == FlowType.SendMoney{
              name = dictdata["aMNO_WALLET"]!
             }
             else {
                name  = dictdata["aMNO_NAME"]!
             }
            
            cell.setdata(acname: name)
            
            if SelectName == name {
                cell.imgCheckmark.image = UIImage.init(named: "check")
            }
            else{
                cell.imgCheckmark.image = UIImage.init(named: "uncheck")
            }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BottomSheetAccountTableViewCell") as! BottomSheetAccountTableViewCell
            let dictdata = arrayAccounts[indexPath.row]
            cell.setdata(acname: dictdata["aACCOUNT_ALIAS"]!, acnumber: dictdata["aACCOUNT_NUMBER"]!)
            
            if xibtype == "ACCOUNT" {
                
                if SelectName == dictdata["aACCOUNT_ALIAS"]! {
                    cell.imgCheckmark.image = UIImage.init(named: "check")
                }
                else {
//                    if indexPath.row == 0 || ((arrayAccounts[0])["aACCOUNT_ALIAS"]!)  == dictdata["aACCOUNT_ALIAS"]!  {
//
//                        cell.imgCheckmark.image = UIImage.init(named: "check")
//                    }
//                    else {
//                        cell.imgCheckmark.image = UIImage.init(named: "uncheck")
//                    }
                    cell.imgCheckmark.image = UIImage.init(named: "uncheck")
                }
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if xibtype == "NETWORK"{
            xibtype = "NETWORK"
            if delegate != nil {
                let dictdata = arrayNetwork[indexPath.row]
                 if CurrentFlow.flowType == FlowType.SendMoney{
                self.SelectName = dictdata["aMNO_WALLET"]!
                    delegate?.selectnewtwork(newtorkname:dictdata["aMNO_WALLET"]!,networkid:dictdata["aMNO_WALLET_ID"]!)
                }
                 else {
                        self.SelectName  = dictdata["aMNO_NAME"]!
                   delegate?.selectnewtwork(newtorkname:dictdata["aMNO_NAME"]!,networkid:dictdata["aMNO_WALLET_ID"]!)
                }
            }
        }
        else{
            let dictdata = arrayAccounts[indexPath.row]
            xibtype = "ACCOUNT"
            if delegate != nil{
                global.acnumber = dictdata["aACCOUNT_NUMBER"]!;
                self.SelectName = dictdata["aACCOUNT_NUMBER"]!; delegate?.selectnewtwork(newtorkname:dictdata["aACCOUNT_ALIAS"]!,networkid:dictdata["aACCOUNT_NUMBER"]!)
            }
        }
        tableView.reloadData()
    }
}

extension BottomSheetViewController: UIGestureRecognizerDelegate {
    
//    // Solution
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        let gesture = (gestureRecognizer as! UIPanGestureRecognizer)
//        let direction = gesture.velocity(in: view).y
//
//        let y = view.frame.minY
//        if (y == fullView && tableView.contentOffset.y == 0 && direction > 0) || (y == partialView) {
//            tableView.isScrollEnabled = false
//        } else {
//            tableView.isScrollEnabled = true
//        }
//
//        return false
//    }
}
