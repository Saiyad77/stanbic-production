//
//  BottomSheetTableViewCell.swift
//  Ecobank
//
//  Created by Vijay Patidar on 23/03/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class BottomSheetTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imgCheckmark: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 15, color: Color.c52_58_64, title: "", placeHolder: "")
        imgCheckmark.layer.cornerRadius = imgCheckmark.frame.size.width / 2.0
        imgIcon.layer.cornerRadius = imgIcon.frame.size.width / 3.0
    }
    
    func setdata(acname:String) {
        if acname ==  "AIRTELMONEY" || acname == "Airtel" {
         imgIcon.image = UIImage.init(named: "airtel")
        }
        if acname ==  "MPESA" || acname == "Safaricom" {
           imgIcon.image = UIImage.init(named: "safaricom")
        }
        if acname ==  "telkom" {
            imgIcon.image = UIImage.init(named: "telkom")
        }
        
        lblTitle.text = acname
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
