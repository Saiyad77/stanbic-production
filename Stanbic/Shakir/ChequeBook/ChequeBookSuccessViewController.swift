//
//  ChequeBookSuccessViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 06/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class ChequeBookSuccessViewController: UIViewController, UINavigationControllerDelegate {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblmessagebtm: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var btndone: UIButton!
    
    var msg = ""
    var flow = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setFonts()
        self.tabBarController?.tabBar.isHidden = true
        
        if flow == "FullStatement"{
            lblsubtitle.text = "FULLSTATEMENTTITLE".localized(Session.sharedInstance.appLanguage)
        }
        else if flow == "FullStatement"{
            lblsubtitle.text = "FULLSTATEMENTTITLE".localized(Session.sharedInstance.appLanguage)
        }
        else {
            
        }
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Checkbook.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 21, color: Color.c0_173_108, title: "THANKYOU", placeHolder: "")
        Fonts().set(object: self.lblsubtitle, fontType: 0, fontSize: 16, color: Color.c52_58_64, title: "YOURLOANREQUESTWASSUBMITTED", placeHolder: "")
        Fonts().set(object: self.lblmessagebtm, fontType: 0, fontSize: 16, color: Color.black, title: msg, placeHolder: "")
        
        Fonts().set(object: btndone, fontType: 1, fontSize: 14, color: Color.white, title: "DONE", placeHolder: "")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        global.cupApi = "NO"
        setUpSlideMenu()
    }
    
    @IBAction func share(_ sender: AttributedButton) {
        
    }
    
    @IBAction func Done(_ sender: AttributedButton) {
         global.cupApi = "NO"
        setUpSlideMenu()
    }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
}
