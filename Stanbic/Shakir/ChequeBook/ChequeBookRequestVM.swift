//
//  ChequeBookRequestVM.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 09/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation

protocol CheckBookRequestVMDelegate: class {
    func checkBookRequst()
}

// ViewModel for PAY BILL
class CheckBookRequestVM {
    
    let leaves: String?
    let acalias: String?
    let acnumber: String?
    let Requestcost: String?
    let amount: String?
    
    init(leaves:String, acalias:String, acnumber:String, Requestcost:String, amount:String) {
        self.leaves = leaves
        self.acalias = acalias
        self.acnumber = acnumber
        self.Requestcost = Requestcost
        self.amount = amount
    }
    
    // MARK: CHECKBOOK REQUEST
    func chekbookrequest( acalias:String, leaves:String ) {
        RestAPIManager.chequeBookRequest(title : "PLEASEWAIT", subTitle : "WEAREPROCESSINGYOURREQUEST",type: Response.self, mobileNo : OnboardUser.mobileNo,accountalias : acalias,pin : global.enterpin,leaves : leaves,completion: { (response) in
            DispatchQueue.main.async {
                self.responseData = response
                self.delegate.checkBookRequst()
            }
            
        }){ (error) in
        
        }
    }
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let refrenceId: String?
        let leave: String?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case refrenceId = "REFERENCE_ID"
            case leave = "LEAVES"
        }
    }
    
    var responseData : Response?
    weak var delegate : CheckBookRequestVMDelegate!
}
