//
//  ForexRatesViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 02/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import SDWebImage
import LocalAuthentication

class ForexRatesHeaderCell: UITableViewCell {
    @IBOutlet weak var lbltitle:UILabel!
    @IBOutlet weak var lblsell: UILabel!
    @IBOutlet weak var lblbuy: UILabel!
    
    override func awakeFromNib() {
        Fonts().set(object: lbltitle, fontType: 2, fontSize: 13, color: Color.c160_166_173, title: "Currency", placeHolder: "")
        Fonts().set(object: lblsell, fontType: 2, fontSize: 13, color: Color.c160_166_173, title: "Sell", placeHolder: "")
        Fonts().set(object: lblbuy, fontType: 2, fontSize: 13, color: Color.c160_166_173, title: "Buy", placeHolder: "")
    }
}

class ForexRatesCell: UITableViewCell {
    
    @IBOutlet weak var lbltitle:UILabel!
    @IBOutlet weak var lblsubtitle:UILabel!
    @IBOutlet weak var lblsell: UILabel!
    @IBOutlet weak var lblbuy: UILabel!
    @IBOutlet weak var imglogo: UIImageView!
    
    
    func setdata(title:String,subtitle:String,sell:String,buy:String,logo:String){
        lbltitle.text = title
        lblsubtitle.text = subtitle
        lblsell.text = sell
        lblbuy.text = buy
        imglogo.sd_setImage(with: URL(string: logo), placeholderImage: UIImage(named: "placeholder"))
    }
    
    override func awakeFromNib() {
        Fonts().set(object: lbltitle, fontType: 1, fontSize: 14, color: Color.black, title: "", placeHolder: "CELL")
        Fonts().set(object: lblsubtitle, fontType: 0, fontSize: 13, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: lblsell, fontType: 1, fontSize: 13, color: Color.c52_58_64, title: "", placeHolder: "")
        Fonts().set(object: lblbuy, fontType: 1, fontSize: 13, color: Color.c52_58_64, title: "", placeHolder: "")
    }
}

class ForexRatesViewController: UIViewController, UINavigationControllerDelegate {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var tableviewForex: UITableView!
    @IBOutlet weak var lblMsg: UILabel!
    
    let coreData = CoreDataHelper()
    var resultArray = [JSON]()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblMsg.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        setFonts()
        
        
        self.tabBarController?.tabBar.isHidden = true
        
        Fonts().set(object: lblMsg, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "DATANOTFOUND", placeHolder: "")
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
           let acnos = acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!
            
            var acno = ""
            if acnos.contains("KES-"){
                let arrac = acnos.components(separatedBy: "KES-")[1]
                acno = arrac
            }
            else {
                acno = acnos
            }

                    self.validate()
            
        }
        else {
            global.cupApi = "NO"
            setUpSlideMenu()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK:_SET FONT
    func setFonts() {
        initialSetUpNavigationBar()
    }
    
    // MARK: - Required Method's
    func initialSetUpNavigationBar(){
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = "FOREXRATES".localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Checkbook.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        global.cupApi = "NO"
        setUpSlideMenu()
    }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
}

extension ForexRatesViewController :UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForexRatesHeaderCell") as! ForexRatesHeaderCell
        cell.contentView.backgroundColor = Color.white
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if (resultArray.count) == 0 {
            return 0
        }
        else {
            
            return 52
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if resultArray.count > 0 {
            lblMsg.isHidden = true
        }
        
        return resultArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForexRatesCell", for: indexPath) as! ForexRatesCell
        let dict = resultArray[indexPath.row]
        cell.setdata(title: (dict["CURRENCY_CODE"].stringValue), subtitle: (dict["CURRENCY"].stringValue), sell: (dict["SELL"].stringValue), buy: (dict["BUY"].stringValue), logo: (dict["LOGO"].stringValue))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
}

extension ForexRatesViewController {
    
    // FULL STATEMENT :_
    func getForexRate(acalias:String) {
        if !isInternetAvailable(){
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        let loader = RestAPIManager.addProgress(title: "PLEASEWAIT", description: "WEAREPROCESSINGYOURREQUEST")
        
        var acno = ""
        if acalias.contains("KES-"){
            let arrac = acalias.components(separatedBy: "KES-")[1]
            acno = arrac
        }
        else {
            acno = acalias
        }
        let charge = getChargebyCost(service_code: "FR", acno: acno)
        
        let TotelAmount = TotelCharges(percent: global.percent, charge:charge)
        
        RestAPIManager.forexRates(title : "PLEASEWAIT", subTitle : "WEAREPROCESSINGYOURREQUEST",mobileNo : OnboardUser.mobileNo,accountalias : acalias,pin : global.enterpin){ (json) in
            DispatchQueue.main.async {
                loader.remove()
            }
            if json["STATUS_CODE"].intValue == 200 {
                DispatchQueue.main.async {
                    self.lblMsg.isHidden = true
                    self.resultArray = json["RESULT_ARRAY"].arrayValue
                    self.tableviewForex.reloadData()
                }
            }
            else {
                DispatchQueue.main.async {
                    self.lblMsg.isHidden = false
               
                //self.showMessage(title: "", message: json["STATUS_MESSAGE"].stringValue)
                let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                nextvc.message = json["STATUS_MESSAGE"].stringValue
                self.navigationController?.pushViewController(nextvc, animated: true)
                    
                }
            }
        }
    }
}


extension ForexRatesViewController:AuthDelegate {
    
    func validate() {
        //Check if user enable biometric access
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            let context = LAContext()
            Authntication(context).authnticateUser { (success, message) in
                DispatchQueue.main.async {
                    if success == true {
                        global.enterpin = OnboardUser.pin
                        self.validAuth()
                    }
                    else {
                        if message != "" {
                             self.showMessage(title: "", message: message)
                        }
                    }
                    
                }
            }
        }
        else {
             global.enterpin = ""
            let vc = AuthoriseViewController.getVCInstance() as! AuthoriseViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    func validAuth() {
        
        if !isInternetAvailable(){
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
                getForexRate(acalias:  acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!)
            }
        }
    }
}




