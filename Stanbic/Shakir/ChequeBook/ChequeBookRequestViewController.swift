//
//  ChequeBookRequestViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 02/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication

class ChequeBookRequestViewController: UIViewController,UIGestureRecognizerDelegate, UINavigationControllerDelegate {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var txtleaves: UITextField!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lblbuyfrom: UILabel!
    @IBOutlet weak var lblbtmtitle: UILabel!
    @IBOutlet weak var lblbtmSubtitle: UILabel!
    
    // Variables for Bottom sheet
    var bottomSheetVC: BottomSheetViewController?
    var imgView = UIImageView()
    
    // View Leaves
    @IBOutlet var viewLeaves: UIView!
    @IBOutlet weak var lblSelectLeavesTitle: UILabel!
    @IBOutlet weak var tblViewLeaves: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btn_leaves: UIButton!
    var imgBlack : UIImageView?
    var arrNumberOfLeaves = [String]()
    var accountno = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add Bottom sheet VC
        addBottomSheetView()
        bottomSheetVC?.delegate = self
        
        txtleaves.addUI(placeholder: "NUMBERLEAVES")
        setFonts()
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            lblaccount.text = acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!
            lblaccountnumber.text = acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!.hashingOfTheAccount(noStr: acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!)
          accountno = acdata[acdata.count - 1]["aACCOUNT_NUMBER"] ?? ""
        }
        self.tabBarController?.tabBar.isHidden = true
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        lblbtmtitle.isHidden = true
        txtleaves.isUserInteractionEnabled  = false
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getNoOfLeavesFor(service_code: "RCB")
     }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Checkbook.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: lbltitle, fontType: 1, fontSize: 24, color: Color.white, title:"CHEQUEREQUEST", placeHolder: "")
        
        Fonts().set(object: lblsubtitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title:"REQUESTFORCHEQUEBOOK", placeHolder: "")
        
        
        Fonts().set(object: txtleaves, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblaccount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "", placeHolder: "")
        
        Fonts().set(object: lblbuyfrom, fontType: 0, fontSize: 12, color: Color.c0_123_255, title: "REQUESTFOR", placeHolder: "")
        Fonts().set(object: lblbtmtitle, fontType: 1, fontSize: 10, color: Color.c0_123_255, title: "", placeHolder: "")
        Fonts().set(object: lblbtmSubtitle, fontType: 1, fontSize: 10, color: Color.c0_123_255, title: "", placeHolder: "")
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "SUBMIT", placeHolder: "")
        let appLanguage = Session.sharedInstance.appLanguage
        lblbtmSubtitle.text = "CHEQUEBOOKREQUESTCHARGES".localized(appLanguage as String);
        lblbtmSubtitle.isHidden = false
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func leaves(_ sender: UIButton) {
        txtleaves.didBegin()
        self.showPopup(popupView: viewLeaves, height: (arrNumberOfLeaves.count * 50) + 130)
    }
    
    @IBAction func accountselect(_ sender: AttributedButton) {
        view.endEditing(true)
        self.showBottomSheetVC(xibtype: "ACCOUNT", Selectname: lblaccount.text!)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        view.endEditing(true)
        if txtleaves.text == ""{
            txtleaves.showError(errStr: "NUMBERLEAVES")
        }
        else if lblaccountnumber.text == ""{
            showMessage(title: "", message: "SELECTACCOUNT")
        }
        else{
            // validate()
            
            let datamodel = CheckBookRequestVM.init(leaves: txtleaves.text!, acalias: lblaccount.text!, acnumber: lblaccountnumber.text!, Requestcost: "", amount: "")
            
            let nextvc = CheckbookRequestReviewVC.getVCInstance() as! CheckbookRequestReviewVC
            nextvc.model = datamodel
            nextvc.accountno = accountno
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
    
    @IBAction func btnCancelLeaves(_ sender: UIButton) {
        self.dismissPopup(popupView: viewLeaves)
    }
    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
    func showBottomSheetVC(xibtype: String,Selectname: String) {
        
        var height = 300  // 340
        let coreData = CoreDataHelper()
        if xibtype == "ACCOUNT" {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
                height = acdata.count * 100
                if acdata.count == 1 {
                    height = 200
                }
            }
        }
        else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acnetwork  = coreData.getDataForEntity(entity: Entity.MNO_INFO)
                height = acnetwork.count * 200
            }
        }
        
        // bottomSheetVC?.partialView = UIScreen.main.bounds.height - CGFloat(height)
        bottomSheetVC!.partialView = self.view.frame.size.height - CGFloat(height)
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imgView.backgroundColor = Color.black
        imgView.alpha = 0.2
        imgView.isUserInteractionEnabled = true
        self.view.addSubview(imgView)
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "", SelectName: "")
        imgView.removeFromSuperview()
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
}

//Text field delegate
extension ChequeBookRequestViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

extension ChequeBookRequestViewController : SelectNetworkDelegate{
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
        lblaccountnumber.text =  acnumber
        lblaccount.text = acname
    }
    
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
        hideBottomSheetVC()
        lblaccount.text = newtorkname
        lblaccountnumber.text = networkid.hashingOfTheAccount(noStr: networkid)
        accountno = networkid
        
        
    }
}

extension ChequeBookRequestViewController : UITableViewDelegate, UITableViewDataSource {
    
    func showPopup(popupView : UIView, height : Int) {
        
        imgBlack = UIImageView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        imgBlack?.alpha = 0.5
        //imgBlack?.backgroundColor = colorConstant.kBlackColor
        imgBlack?.isUserInteractionEnabled = true
        self.view.addSubview(imgBlack!)
        
        let frm = CGRect(x: ((self.view.frame.size.width / 2) - ((self.view.frame.size.width * 0.9) / 2)), y: -(popupView.frame.size.height), width: (self.view.frame.size.width * 0.9), height: CGFloat(height))
        
        popupView.frame = frm
        
        self.view.addSubview(popupView)
        
        popupView.layer.cornerRadius = 10
        popupView.clipsToBounds = true
        
        self.arrNumberOfLeaves.reverse()
        
        //  Fonts().set(object: lblSelectLeavesTitle, fontType: 1, fontSize: 20, color: Color.black, title: "NUMBEROFLEAVES", placeHolder: "")
        Fonts().set(object: btnCancel, fontType: 1, fontSize: 17, color: Color.black, title: "CANCEL", placeHolder: "")
        
        
        UIView.animate(withDuration: 0.5, animations: {
            
            let frm = CGRect(x: ((self.view.frame.size.width / 2) - ((self.view.frame.size.width * 0.9) / 2)), y: (self.view.frame.size.height / 2 - popupView.frame.size.height / 2), width: (self.view.frame.size.width * 0.9), height: CGFloat(height))
            
            popupView.frame = frm
            
        }){ (Bool) in
            
        }
    }
    
    
    // Dismiss popup
    func dismissPopup(popupView : UIView) {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            var frm =  popupView.frame
            
            frm.origin.y = -(popupView.frame.size.height)
            
            popupView.frame = frm
            
        }) { (Bool) in
            
            popupView.removeFromSuperview()
            
            self.imgBlack?.removeFromSuperview()
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrNumberOfLeaves.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LEAVES", for: indexPath) as! LeavesTableViewCell
        cell.lblNumberLeaves.text = arrNumberOfLeaves[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        txtleaves.text = arrNumberOfLeaves[indexPath.row]
        self.changeChargeAmount(index:indexPath.row)
        self.dismissPopup(popupView: viewLeaves)
    }
    
    func changeChargeAmount(index:Int)
    {
        lblbtmtitle.isHidden = false
        let cost = getTransectionCostFor(service_code : "RCB",costKey:arrNumberOfLeaves[index])
        let TotelAmount =  TotelCharges(percent: global.percent, charge:removeKES(price: cost))
        lblbtmtitle.text = "*A Charge of KSH \(TotelAmount) will be applied"
    }
    
    func getNoOfLeavesFor(service_code : String) {
        var firstkey = ""
        arrNumberOfLeaves.removeAll()
        let array = CoreDataHelper().getDataForEntity(entity: Entity.SERVICES_INFO)
        
        for i in 0 ..< array.count {
            
            if array[i]["aSERVICE_CODE"] == service_code {
                
                if let dict = convertToDictionary(text: array[i]["aCHARGE"]!){
                    
                    var keys = Array(dict.keys)
                    
                    let keyindex = keys[0]
                    //let tranCost = dict[keyindex]
                    arrNumberOfLeaves.append(keyindex)
                    firstkey = keyindex
                }
                else
                {
                    //let transactionCost = array[i]["aCHARGE"] ?? "0"
                    
                }
            }
            
            if arrNumberOfLeaves.count == 1 {
                let cost = getTransectionCostFor(service_code : "RCB",costKey:firstkey)
                lblbtmtitle.text = "*A Charge of KSH \(cost)will be applied"
                btn_leaves.isUserInteractionEnabled = false
                txtleaves.text = arrNumberOfLeaves[0]
                txtleaves.didBegin()
                lblbtmtitle.isHidden = false
            }
            else {
                btn_leaves.isUserInteractionEnabled = true
            }
        }
        // return "0"
    }
    
    
    
    // GET COSt Accouding to leaves:-
    func getTransectionCostFor(service_code : String,costKey:String) -> String {
        
        let array = CoreDataHelper().getDataForEntity(entity: Entity.SERVICES_INFO)
        
        for i in 0 ..< array.count {
            
            if array[i]["aSERVICE_CODE"] == service_code {
                if let dict = convertToDictionary(text: array[i]["aCHARGE"]!){
                    
                    if dict[costKey] != nil{
                        return dict[costKey]!
                    }
                    else {
                        return "0"
                    }
                    
                }
                else {
                    return "0"
                }
            }
        }
        return "0"
    }
    
//    func getNoOfLeavesFor(service_code : String,acno:String) {
//        var firstkey = ""
//        arrNumberOfLeaves.removeAll()
//        let array = CoreDataHelper().getDataForEntity(entity: Entity.SERVICES_INFO)
//
//        for i in 0 ..< array.count {
//
//            if array[i]["aSERVICE_CODE"] == service_code {
//
//            //  if let dict = convertToDictionary(text: array[i]["aCHARGE"]!){
//                let dictcharges = array[i]["aCHARGES"]!
//                let data = convertToDictionary1(text:dictcharges)
//
//                if let dict = data?[acno] as? [String:String]{
//
//                    var keys = Array(dict.keys)
//                    let keyindex = keys[0]
//                    //let tranCost = dict[keyindex]
//                    arrNumberOfLeaves.append(keyindex)
//                    firstkey = keyindex
//                }
//                else
//                {
//                    //let transactionCost = array[i]["aCHARGE"] ?? "0"
//
//                }
//            }
//
//            if arrNumberOfLeaves.count == 1 {
//                let cost = getTransectionCostFor(service_code : "RCB",costKey:firstkey, acno: accountno)
//              //lblbtmtitle.text = "*A Charge of KSH \(cost)will be applied"
//                let TotelAmount =  TotelCharges(percent: global.percent, charge:removeKES(price: cost))
//                lblbtmtitle.text = "*A Charge of KSH \(TotelAmount) will be applied"
//                 btn_leaves.isUserInteractionEnabled = false
//                 txtleaves.text = arrNumberOfLeaves[0]
//                 txtleaves.didBegin()
//                 lblbtmtitle.isHidden = false
//            }
//            else {
//                 btn_leaves.isUserInteractionEnabled = true
//            }
//        }
//       // return "0"
//    }
//
//
//    // GET COSt Accouding to leaves:-
//    func getTransectionCostFor(service_code : String,costKey:String,acno:String) -> String {
//
//        let array = CoreDataHelper().getDataForEntity(entity: Entity.SERVICES_INFO)
//        for i in 0 ..< array.count {
//
//            if array[i]["aSERVICE_CODE"] == service_code {
//               // if let dict = convertToDictionary(text: array[i]["aCHARGE"]!){
//                let dictcharges = array[i]["aCHARGES"]!
//                let data = convertToDictionary1(text:dictcharges)
//
//                if let dict = data?[acno] as? [String:String]{
//
//                    if dict[costKey] != nil{
//                        return dict[costKey]!
//                    }
//                    else {
//                        return "0"
//                    }
//                }
//                else {
//                    return "0"
//                }
//            }
//        }
//        return "0"
//    }
}

class LeavesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblNumberLeaves: UILabel!
    @IBOutlet weak var imglocations: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Fonts().set(object: lblNumberLeaves, fontType: 0, fontSize: 16, color: Color.black, title: "", placeHolder: "")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}


