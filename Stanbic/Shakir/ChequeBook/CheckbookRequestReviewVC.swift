//
//  CheckbookRequestSuccessVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 09/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication

class CheckbookRequestReviewVC: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblReqChequeBook: UILabel!
    @IBOutlet weak var lblCurrentAc: UILabel!
    @IBOutlet weak var lblCurrentAcName: UILabel!
    @IBOutlet weak var lblNumLeaves: UILabel!
    @IBOutlet weak var lblNumLeavesName: UILabel!
    @IBOutlet weak var lblRequestCost: UILabel!
    @IBOutlet weak var lblRequestCostName: UILabel!
    @IBOutlet weak var btnContinue: AttributedButton!
    @IBOutlet weak var viewObj: UIView!
    
    var model : CheckBookRequestVM?
    var accountno = ""
    
    //MARK: View's life Cycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        initialFontSetup()
        model?.delegate = self
        lblRequestCostName.text = getTransectionCostFor(service_code : "RCB",costKey:model?.leaves ?? "")
        viewObj.addshaow(offset : CGSize(width: 2, height: 2))
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Checkbook.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func initialFontSetup()
    {
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 17, color: Color.white, title: "CONFIRMANDREQUEST", placeHolder: "")
        Fonts().set(object: lblReqChequeBook, fontType: 1, fontSize: 9, color: Color.c0_123_255, title: "REQUESTCHEQUEBOOKFOR", placeHolder: "")
        Fonts().set(object: lblCurrentAc, fontType: 1, fontSize: 13, color: Color.black, title: (model?.acalias)!, placeHolder: "")
        Fonts().set(object: lblCurrentAcName, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: (model?.acnumber)!, placeHolder: "")
        Fonts().set(object: lblNumLeaves, fontType: 1, fontSize: 13, color: Color.black, title: "NUMBERLEAVES1", placeHolder: "")
        Fonts().set(object: lblNumLeavesName, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: (model?.leaves)!, placeHolder: "")
        Fonts().set(object: lblRequestCost, fontType: 1, fontSize: 13, color: Color.black, title: "REQUESTCOST", placeHolder: "")
        Fonts().set(object: lblRequestCostName, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: btnContinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBuyAirtimeTapped(_ sender: UIButton) {
     validate()
        
    }
}

extension CheckbookRequestReviewVC : CheckBookRequestVMDelegate,AuthDelegate {
    
    func validate() {
        //Check if user enable biometric access
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            let context = LAContext()
            Authntication(context).authnticateUser { (success, message) in
                DispatchQueue.main.async {
                    if success == true {
                        global.enterpin = OnboardUser.pin
                        self.validAuth()
                    }
                    else {
                        if message != "" {
                              self.showMessage(title: "", message: message)
                        }
                    }
                    
                }
            }
        }
        else {
            global.enterpin = ""
            let vc = AuthoriseViewController.getVCInstance() as! AuthoriseViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func validAuth() {
        model?.chekbookrequest(acalias: lblCurrentAc.text!, leaves: lblNumLeavesName.text!)
    }
    
    func checkBookRequst() {

        if model?.responseData?.statusCode == 200 {
            let nextvc = ChequeBookSuccessViewController.getVCInstance() as! ChequeBookSuccessViewController
            nextvc.msg = (model?.responseData?.statusMessage!)!
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
        else {
          //  self.showMessage(title: "", message: (model?.responseData?.statusMessage!)!)
            
            let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
            nextvc.message = model?.responseData?.statusMessage ?? ""
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
    
    // GET COSt Accouding to leaves:-
    func getTransectionCostFor(service_code : String,costKey:String) -> String {
        
        let array = CoreDataHelper().getDataForEntity(entity: Entity.SERVICES_INFO)
        
        for i in 0 ..< array.count {
            
            if array[i]["aSERVICE_CODE"] == service_code {
                if let dict = convertToDictionary(text: array[i]["aCHARGE"]!){
                    
                    if dict[costKey] != nil{
                        return dict[costKey]!
                    }
                    else {
                        return "0"
                    }
                    
                }
                else {
                    return "0"
                }
            }
        }
        return "0"
    }
    
//    // GET COSt Accouding to leaves:-
//    func getTransectionCostFor(service_code : String,costKey:String,acno:String) -> String {
//
//        let array = CoreDataHelper().getDataForEntity(entity: Entity.SERVICES_INFO)
//
//        for i in 0 ..< array.count {
//
//            if array[i]["aSERVICE_CODE"] == service_code {
//               // if let dict = convertToDictionary(text: array[i]["aCHARGE"]!){
//                let dictcharges = array[i]["aCHARGES"]!
//                let data = convertToDictionary1(text:dictcharges)
//
//                if let dict = data?[acno] as? [String:String]{
//
//                    if dict[costKey] != nil{
//                        return dict[costKey]!
//                    }
//                    else {
//                        return "0"
//                    }
//
//                }
//                else {
//                    return "0"
//                }
//            }
//        }
//        return "0"
//    }
}
