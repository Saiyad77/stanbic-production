//
//  ForexNavController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 11/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

//Forex
class ForexNavController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    static func getVCInstance() -> UINavigationController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Checkbook.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))") as! UINavigationController
    }
}

//Contact us
class ContactUsNavController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    static func getVCInstance() -> UINavigationController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))") as! UINavigationController
    }
}

//About us
class AboutUsNavController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    static func getVCInstance() -> UINavigationController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))") as! UINavigationController
    }
}
    //Settings
    class SettingNavController: UINavigationController {
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            // Do any additional setup after loading the view.
        }
        
        static func getVCInstance() -> UINavigationController {
            // This method returns the instance on it self to push or present in Navigation.
            return UIStoryboard(name: Storyboards.Checkbook.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))") as! UINavigationController
        }
    }
    //Locate us
    class LocateUsNavController: UINavigationController {
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            // Do any additional setup after loading the view.
        }
        
        static func getVCInstance() -> UINavigationController {
            // This method returns the instance on it self to push or present in Navigation.
            return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))") as! UINavigationController
    }
}
//----------Change pin
class ChangeNewPinNavController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    static func getVCInstance() -> UINavigationController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))") as! UINavigationController
    }
}


//----------loan Calculater
class SelectLoanCalculater: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    static func getVCInstance() -> UINavigationController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp3.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))") as! UINavigationController
    }
}


//NOTIFICATIONS :_
class NotificationNavController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     }
    
    static func getVCInstance() -> UINavigationController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp3.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))") as! UINavigationController
    }
}

//----------loan
class NavSelectLoan: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
}
    // This method returns the instance on it self to push or present in Navigation.
    static func getVCInstance() -> UINavigationController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp4.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))") as! UINavigationController
    }
}
