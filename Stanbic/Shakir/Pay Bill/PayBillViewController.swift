
//
//  PayBillViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 23/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import SDWebImage
 
class PayBillHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imglogo: UIImageView!
    @IBOutlet weak var lbllogotitle: UILabel!
    
    override func awakeFromNib() {
        Fonts().set(object: lbltitle, fontType: 1, fontSize: 13, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: lbllogotitle, fontType: 2, fontSize: 8, color: Color.c134_142_150, title: "", placeHolder: "")
    }
}

class PayBillTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imgcards: UIImageView!
    
    func setfont(number:Int,title:String, url : String) {
        imgcards.layer.cornerRadius = imgcards.frame.height/2
        imgcards.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeholderurl"))
        Fonts().set(object: lbltitle, fontType: 1, fontSize: number, color: Color.black, title: title, placeHolder: "")
    }
    
    func setfontSection(number:Int,title:String, url : String) {
        imgcards.layer.cornerRadius = 0.0
        Fonts().set(object: lbltitle, fontType: 1, fontSize: number, color: Color.black, title: title, placeHolder: "")
        imgcards.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeholderurl"))
    }
    override func awakeFromNib() {
        imgcards.layer.cornerRadius = imgcards.frame.size.height / 2
        imgcards.layer.borderColor = Color.c233_233_235.cgColor
        imgcards.layer.borderWidth = 1.0
    }
}


class PayBillViewController: UIViewController, UINavigationControllerDelegate {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var tblpaybill: UITableView!
    @IBOutlet weak var txtserachfield: UITextField!
    @IBOutlet weak var viewSearch: AttributedView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblHeaderTitle: UIView!
    private var lastContentOffset: CGFloat = 0
    
    var arrBilldata = [[String:String]]()
    var arrSerachdata = [[String:String]]()
    var arrCards = [[String:String]]()
    var contactno = ""
    var contactname = ""
    var nominateflag = ""
    var search = false
    var imgHeader =  UIImageView()
    var imgHederHeight : CGFloat = 0
     var noDataLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setFonts()
        setupHeader()
        arrCards =  CoreDataHelper().getDataForEntity(entity: Entity.CARD_INFO)
        arrBilldata =  CoreDataHelper().getDataForEntity(entity: Entity.BILLERS_INFO)
        tblpaybill.reloadData()
        self.txtserachfield.addTarget(self, action: #selector(searchdatasAsPerText(_ :)), for: .editingChanged)
        self.tabBarController?.tabBar.isHidden = true
           arrBilldata.sort(by: {($0["aBILLER_NAME"] as! String) < $1["aBILLER_NAME"] as! String})
        
        noDataLabel = UILabel(frame: CGRect(x: 0, y: 10, width: 200, height: 18))
        noDataLabel.center.x = self.view.center.x
        noDataLabel.frame.origin.y = self.tblpaybill.frame.minY + 270
        noDataLabel.numberOfLines = 0
        noDataLabel.textAlignment = .center
        noDataLabel.text = "No results found!"
        noDataLabel.font = UIFont.init(name: Fonts.kFontMedium, size: CGFloat(14))
        noDataLabel.textColor = .black
        self.noDataLabel.isHidden = true
        self.view.addSubview(noDataLabel)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblpaybill.reloadData()
             //animateTable()
    }
    
    @objc func searchdatasAsPerText(_ textfield:UITextField) {
        self.arrSerachdata.removeAll()
        if textfield.text?.count != 0 {
            for i in 0..<self.arrBilldata.count {
                let dictdata = self.arrBilldata[i]
                if (dictdata["aBILLER_NAME"]!.lowercased()).contains((textfield.text!).lowercased()){
                    arrSerachdata.append(arrBilldata[i])
                }
            }
        }
//        if arrSerachdata.count > 0 {
//            search = true
//        }
//        else{
//            search = false
//        }
        
        search = true
        
        if arrSerachdata.count > 0 {
            //self.noDataLabel.isHidden = true
        }
        else{
         //   self.noDataLabel.isHidden = false
            if textfield.text?.count == 0{
                search = false
               // self.noDataLabel.isHidden = true
            }
            
        }
        self.tblpaybill.reloadData()
 
    }
    
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: "PB", placeHolder: "")
        Fonts().set(object: self.lblHeaderTitle, fontType: 1, fontSize: 17, color: Color.white, title: "PB", placeHolder: "")
        Fonts().set(object: self.txtserachfield, fontType: 0, fontSize: 13, color: Color.c10_34_64, title: "", placeHolder: "SEARCHFORBILL")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        self.view.endEditing(true)
    }
}

//Text field delegate
extension PayBillViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " " {
            return false
        }
        return true
    }
}

extension PayBillViewController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if arrCards.count == 0{
            return 2
        }
        else{
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }
        else if arrCards.count == 0{
            if search == false{
                return arrBilldata.count
            }
            else{
                return arrSerachdata.count
            }
        }
        else{
            if section == 0 {
                return 1
            }
            else if section == 1 {
                return arrCards.count
            }
            else {
                if search == false{
                    return arrBilldata.count
                }
                else{
                    return arrSerachdata.count
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
          tblHeight.constant = tableView.contentSize.height
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "merchants") as! PayBillTableViewCell
            cell.setfont(number: 16,title:"Saved Accounts", url: "")
            cell.imgcards.image = UIImage.init(named: "fav")
           //  viewAnimate(views: cell.contentView)
            return cell
        }
        else {
        
        if arrCards.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "merchants") as! PayBillTableViewCell
            if search == false {
                let dictdata = arrBilldata[indexPath.row]
                cell.setfont(number: 16,title:dictdata["aBILLER_NAME"]!, url: dictdata["aBILLER_LOGO"]!)
            }
            else {
                let dictdata = arrSerachdata[indexPath.row]
                cell.setfont(number: 16,title:dictdata["aBILLER_NAME"]!, url: dictdata["aBILLER_LOGO"]!)
            }
         //    viewAnimate(views: cell.contentView)
            return cell
        }
        else{
            if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "mycards") as! PayBillTableViewCell
                cell.setfontSection(number: 13,title: arrCards[indexPath.row]["aCARD_NAME"] ?? "", url: arrCards[indexPath.row]["aCARD_ICON"] ?? "")
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "merchants") as! PayBillTableViewCell
                if search == false{
                    
                    let dictdata = arrBilldata[indexPath.row]
                    cell.setfont(number: 16,title:dictdata["aBILLER_NAME"]!, url: dictdata["aBILLER_LOGO"]!)
                }
                else{
                    let dictdata = arrSerachdata[indexPath.row]
                    cell.setfont(number: 16,title:dictdata["aBILLER_NAME"]!, url: dictdata["aBILLER_LOGO"]!)
                }
                
//                UIView.animate(withDuration: 1, animations: {
//                    cell.contentView.frame.size.width += 20
//                    cell.contentView.frame.size.height += 20
//                }) { _ in
//                    UIView.animate(withDuration: 1, delay: 0.25, options: [.autoreverse, .repeat], animations: {
//                        cell.contentView.frame.origin.y -= 50
//                    })
//                }
              // viewAnimate(views: cell.contentView)
                
                return cell
            }
         }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PayBillHeaderTableViewCell") as! PayBillHeaderTableViewCell
        if arrCards.count == 0{
            cell.lbltitle.text = "All merchants"
            
            cell.lbllogotitle.isHidden = true
            cell.imglogo.isHidden = true
            cell.lbllogotitle.text = "POWERED BY"
        }
        else {
             if section == 1 {
                cell.lbltitle.text = "Pay to my Cards"
                cell.lbllogotitle.isHidden = true
                cell.imglogo.isHidden = true
            }
            else {
                cell.lbltitle.text = "All merchants"
                cell.lbllogotitle.isHidden = true
                cell.imglogo.isHidden = true
                cell.lbllogotitle.text = "POWERED BY"
            }
        }
         let headerView = UITableViewHeaderFooterView()
        viewAnimate(views: headerView)
         return cell
    }
    // animate section header
    private func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
//        let numberOfSections = tableView.numberOfSections
//        let tableHeight: CGFloat = tableView.bounds.size.height
//        var index = 0
//
//        for i in 0...numberOfSections - 1 {
//            if (section == i) {
//                view.transform = CGAffineTransform(translationX: 0, y: tableHeight)
//                UIView.animate(withDuration: 1.0, delay: 0.05 * Double(index),usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: { () -> Void in
//                    view.transform = CGAffineTransform(translationX: 0, y: 0)
//                }, completion: nil)
//            }
//            index += 1
//        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.section == 0 {
        let nextvc = PayBillSavedAccountVC.getVCInstance() as! PayBillSavedAccountVC
             nextvc.billerInfo = arrBilldata
            nextvc.arrCards = arrCards
            global.creditCardID = ""
              self.navigationController?.pushViewController(nextvc, animated: true)
        }
        else if indexPath.section == 1 {
            let cardInfo = arrCards[indexPath.row]
            
            let nextvc = PayBillSelectAccountViewController.getVCInstance() as! PayBillSelectAccountViewController
            nextvc.cardInfo = cardInfo
           
            nextvc.flow = "CREDITCARD"
            global.creditCardID = cardInfo["aCARD_ID"] ?? ""
            global.imgPaybill = cardInfo["aCARD_ICON"] ?? ""
             let dataModels = PayBillVM.init(dictPaybill:cardInfo, mobilenumber: contactno,mobilename: contactname, nominateflag: "Yes")
             nextvc.dataModels  = dataModels
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
        else {
            let dataModels = PayBillVM.init(dictPaybill:arrBilldata[indexPath.row], mobilenumber: contactno,mobilename: contactname, nominateflag: "Yes")
            let nextvc = PayBillNumberEnterViewController.getVCInstance() as! PayBillNumberEnterViewController
            nextvc.dataModels  = dataModels
            global.imgPaybill = arrBilldata[indexPath.row]["aBILLER_LOGO"] ?? ""
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0 {
            return 0
        }else {
        return 60
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
          return 80
        }
        else if indexPath.section == 1 {
            return 72
        }
        else {
            return 80
        }
    }
    
   
}

extension PayBillViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = imgHederHeight - (scrollView.contentOffset.y + 10)
        let height = min(max(y, 150), 400)
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
        
        if scrollView.contentOffset.y > 50 {
            self.lblHeaderTitle.isHidden = false
        }
        else {
            self.lblHeaderTitle.isHidden = true
        }
        
        
        Global.invalidTimer()
        
     //    This is the offset at the bottom of the scroll view.
//        let totalScroll: CGFloat = scrollView.contentSize.height - scrollView.bounds.size.height
//
//        // This is the current offset.
//        let offset: CGFloat = scrollView.contentOffset.y
//
//        // This is the percentage of the current offset / bottom offset.
//        let percentage: CGFloat = offset / totalScroll
//
//        // When percentage = 0, the alpha should be 1 so we should flip the percentage.
//        lblHeaderTitle.alpha = (percentage * 10)
    }
    
    func setupHeader() {
        
        imgHederHeight = 210
        scrollView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        scrollView.delegate = self
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: imgHederHeight)
        imgHeader.contentMode = .scaleAspectFill
        imgHeader.image = UIImage.init(named: "GradientBar-3")
        imgHeader.clipsToBounds = true
        
        view.addSubview(imgHeader)
        self.view.bringSubviewToFront(scrollView)
        self.view.bringSubviewToFront(btnBack)
        self.view.bringSubviewToFront(lblHeaderTitle)
    }
    
    func animateTable() {
        self.tblpaybill.reloadData()
        
        let cells = tblpaybill.visibleCells
        let tableHeight: CGFloat = tblpaybill.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            
            index += 1
        }
    }
  
    
    
    func viewAnimate(views:UIView){
        UIView.animate(withDuration: 1, animations: {
            views.frame.origin.x -= 50
            views.frame.origin.y -= 50
        } , completion : nil )
    }
    
}
