//
//  PayUpcomingBillViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 29/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class PayUpcomingBillViewController: UIViewController, UINavigationControllerDelegate{
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var viewBillLogo: UIView!
    @IBOutlet weak var imgBillLogo: UIImageView!
    @IBOutlet weak var txtBillNumer: UITextField!
    @IBOutlet weak var txtAcountName: UITextField!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblBillName: UILabel!
    @IBOutlet weak var Clsprice: UICollectionView!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lblPayfrom: UILabel!
    
     @IBOutlet weak var view_top: UIView!

    var bottomSheetVC: BottomSheetViewController?
    var imgView = UIImageView()
    
    private var lastContentOffset: CGFloat = 0
    let Pricearr = ["100", "250","500","1000"]
    var indexcolletion = IndexPath()
    var billInfo : FetchBill.Result?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setFonts()
        Clsprice?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
        
        //Add Bottom sheet VC
        addBottomSheetView()
        bottomSheetVC?.delegate = self
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        //Show selected zero index account
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0 {
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            lblaccount.text = acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!
            lblaccountnumber.text = acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!.hashingOfTheAccount(noStr: acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!)
        }
        
        let arrBilldata =  CoreDataHelper().getDataForEntity(entity: Entity.BILLERS_INFO)
        
        for i in 0..<arrBilldata.count{
           
            if billInfo?.biller == arrBilldata[i]["aBILLER"]{
                txtBillNumer.addUI(placeholder: arrBilldata[i]["aREFERENCE_LABEL"] ?? "METERNUMBER")
            }
            
        }
        setData()
        
        
     }
    
    override func viewDidAppear(_ animated: Bool) {
        txtBillNumer.didBegin()
        txtAcountName.didBegin()
        txtAmount.didBegin()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func setFonts() {
        
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 17, color: Color.white, title: "PAYBILL", placeHolder: "")
        Fonts().set(object: self.lblBillName, fontType: 1, fontSize: 20, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtAmount, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtAcountName, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtBillNumer, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblPayfrom, fontType: 0, fontSize: 12, color: Color.c0_123_255, title: "PAYFROM", placeHolder: "")
        Fonts().set(object: self.lblaccount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "", placeHolder: "")
        
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        
        txtAmount.text = "KES "
        txtAmount.addUI(placeholder: "ENTERAMOUNT")
        txtAcountName.addUI(placeholder: "ACCOUNTNAME")
        //txtBillNumer.addUI(placeholder: "METERNUMBER")
        txtAmount.keyboardType = .numberPad
        
        //imgBillLogo.layer.cornerRadius = imgBillLogo.frame.size.height / 2
        //imgBillLogo.clipsToBounds = true
       // viewBillLogo.layer.cornerRadius = viewBillLogo.frame.size.height / 2
        //viewBillLogo.setShadowWithCornerRadius(view: viewBillLogo, cornerRadius: viewBillLogo.frame.size.height / 2, shodowOffset: CGSize(width: 1.0, height: 1.0))
        
    }
    
    func setData() {
        if let _ = billInfo {
            imgBillLogo.sd_setImage(with: URL(string: billInfo?.billerLogo ?? ""), placeholderImage: UIImage(named: "placeholderurl"))
            lblBillName.text = billInfo?.billerName ?? ""
            txtBillNumer.text =  "\(billInfo?.billRef ?? "0")"
            txtAcountName.text = billInfo?.customerName ?? ""
            txtAmount.text = "KES \(billInfo?.amount ?? "")"
            
            txtBillNumer.isEnabled = false
            txtAcountName.isEnabled = false
            
//        let images = self.resizeImage(image: imgBillLogo.image!, targetSize: imgBillLogo.frame.height,imgBillLogo.frame.width)
//             imgBillLogo.image = images
            
        }
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        self.view.endEditing(true)
        
        if txtAmount.text == "KES " || txtAmount.text == "KES"{
            txtAmount.showError(errStr: "ENTERAMOUNT")
        }
        else if txtAcountName.text == "" {
            txtAcountName.showError(errStr: "ACCOUNTNAME")
        }
        else if txtBillNumer.text == ""{
            txtBillNumer.showError(errStr: "")
        }
        else if lblaccountnumber.text == ""{
            self.showMessage(title: "", message: "SELECTNETWORKPROVIDER")
        }
        else{
            let datamodel =  PayBillVM1.init(cardnumber: txtBillNumer.text!, acalias: lblaccount.text!, acnumber: lblaccountnumber.text!,amount: txtAmount.text!, recptAccAlise : billInfo?.billerName ?? "", biller : billInfo?.biller ?? "", nominateFlag : "No")
            
            let nextvc = CheckoutAirTimeViewController.getVCInstance() as! CheckoutAirTimeViewController
            nextvc.paybilldataModels1 = datamodel
            self.navigationController?.pushViewController(nextvc, animated: true)
            
        }
    }
    
    @IBAction func accountselect(_ sender: AttributedButton) {
        view.endEditing(true)
        self.showBottomSheetVC(xibtype: "ACCOUNT", Selectname: lblaccount.text!)
    }
}

extension PayUpcomingBillViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtAmount{
            txtAmount.text = "KES "
            indexcolletion = IndexPath()
            Clsprice.reloadData()
        }
        textField.removeError()
        textField.didBegin()
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtAmount {
            
            textField.typingAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            let protectedRange = NSMakeRange(0, 4)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                
                return false
            }
            return true
        }
        else{
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

extension PayUpcomingBillViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Pricearr.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PricebuttonCell", for: indexPath as IndexPath) as! PricebuttonCell
        
        if indexcolletion == indexPath {
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.white, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = Color.c0_123_255
        }
        else{
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = .clear
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        indexcolletion = indexPath
        txtAmount.text = "KES \(self.Pricearr[indexPath.row])"
        txtAmount.removeError()
        txtAmount.didBegin()
        Clsprice.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.Clsprice.frame.width/4) - 8
        return CGSize(width: width, height: collectionView.frame.size.height - 5)
    }
}


extension PayUpcomingBillViewController : UIGestureRecognizerDelegate {
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
        // bottomSheetVC!.view.frame.roundCorners(corners: [.topLeft, .topRight], radius: 3.0)
    }
    
    func showBottomSheetVC(xibtype: String,Selectname: String) {
        
//        bottomSheetVC?.partialView = self.view.frame.size.height - 340
//        //bottomSheetVC?.partialView = UIScreen.main.bounds.height - 200
//        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
//        addBGImage()
//        self.view.bringSubviewToFront(bottomSheetVC!.view)
        var height = 200  // 340
        let coreData = CoreDataHelper()
        if xibtype == "ACCOUNT" {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
               height = acdata.count * 100
                if acdata.count == 1 {
                    height = 200
                }
            }
        }
        else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acnetwork  = coreData.getDataForEntity(entity: Entity.MNO_INFO)
                height = acnetwork.count * 200
                if acnetwork.count == 1 {
                    height = 200
                }
            }
        }
        
        // bottomSheetVC?.partialView = UIScreen.main.bounds.height - CGFloat(height)
        bottomSheetVC!.partialView = self.view.frame.size.height - CGFloat(height)
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imgView.backgroundColor = Color.black
        imgView.alpha = 0.2
        imgView.isUserInteractionEnabled = true
        self.view.addSubview(imgView)
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "", SelectName: "")
        imgView.removeFromSuperview()
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
}

extension PayUpcomingBillViewController : SelectNetworkDelegate{
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
        lblaccountnumber.text =  acnumber
        lblaccount.text = acname
    }
    
    
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
        hideBottomSheetVC()
        
        lblaccount.text = newtorkname
        lblaccountnumber.text = networkid.hashingOfTheAccount(noStr: networkid)
        
    }
}


extension PayUpcomingBillViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
           //  self.view.endEditing(true)
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        Global.invalidTimer()
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

