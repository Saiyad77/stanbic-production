//
//  PayBillNumberEnterViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 23/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class PayBillNumberEnterViewController: UIViewController, UINavigationControllerDelegate {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var txtcardnumber: UITextField!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var tblBill: UITableView!
    
    var model = paybillfetchdata()
    
    var dataModels : PayBillVM?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        setFonts()
        txtcardnumber.delegate = self
        model.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
          txtcardnumber.becomeFirstResponder()
    }
    
 
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        let billerName = dataModels?.dictPaybill["aBILLER_NAME"] ?? "Bill's"
        var titleStr = "PLEASEENTERYOURBILLNUMBER".localized(Session.sharedInstance.appLanguage)
        if let newTitle = dataModels?.dictPaybill["aREFERENCE_LABEL"] {
            titleStr = titleStr.replacingOccurrences(of: "BILLNNAME", with: billerName)
            titleStr = titleStr.replacingOccurrences(of: "REFERENCE", with: newTitle)
            txtcardnumber.addUI(placeholder: newTitle.uppercased())
            txtcardnumber.didBegin()
        }
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: titleStr, placeHolder: "")
        Fonts().set(object: self.txtcardnumber, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func countinue(_ sender: AttributedButton) {
        
 
        self.view.endEditing(true)
        if txtcardnumber.text == "" {
            txtcardnumber.showError(errStr: "VLIDNUMBER_")
        }
//        else if getgenralRegax(value: txtcardnumber.text!, type: "card") == false  {
//             txtcardnumber.showError(errStr: "VLIDNUMBER")
//        }
        else {
            let dict = dataModels?.dictPaybill
            model.querybill(mobileNo: OnboardUser.mobileNo, biller: dict!["aBILLER"]!, hubserviceid: dict!["aHUB_SERVICEID"]!, billerrefrence: txtcardnumber.text!)
        }
    }
}

//Text field delegate
extension PayBillNumberEnterViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
         textField.removeError()
         textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}


// QueryBill API -
extension PayBillNumberEnterViewController:paybillfetchdataVMDelegate {
    
    func fetchbilldata() {
        
        if model.responseData?.statusCode == 200 {
            let nextvc = PayBillSelectAccountViewController.getVCInstance() as! PayBillSelectAccountViewController
            nextvc.cardnumber = self.txtcardnumber.text!
            nextvc.self.model = model
            nextvc.dataModels = dataModels
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
        else {
           // self.showMessage(title: "", message: model.responseData?.statusMessage ?? "")
            let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
             nextvc.Flow = "2"
            nextvc.message = model.responseData?.statusMessage ?? ""
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
   
}

