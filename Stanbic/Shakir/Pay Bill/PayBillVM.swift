//
//  PayBillVM.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 25/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation


// ViewModel for PAY BILL
class PayBillVM {
    
    var mobilenumber: String?
    var mobilename: String?
    var nominateflag: String?
    var dictPaybill: [String:String]
    
    init(dictPaybill:[String:String],mobilenumber:String,mobilename:String,nominateflag:String) {
        self.dictPaybill = dictPaybill
        self.mobilenumber = mobilenumber
        self.mobilename = mobilename
        self.nominateflag = nominateflag
    }
}

// nextdata paybill
class PayBillVM1 {
    
    var cardnumber: String?
    var acalias: String?
    var acnumber: String?
    var amount: String?
    var recptAccAlise: String?
    var biller: String?
    var nominateFlag: String?
   
    init(cardnumber:String,acalias:String,acnumber:String,amount:String, recptAccAlise : String, biller : String, nominateFlag : String) {
        self.cardnumber = cardnumber
        self.acnumber = acnumber
        self.acalias = acalias
        self.amount = amount
        self.recptAccAlise = recptAccAlise
        self.biller = biller
        self.nominateFlag = nominateFlag
      }
}


protocol paybillfetchdataVMDelegate: class {
    func fetchbilldata()
}

class paybillfetchdata{
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let fetchbills: [Result]?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case fetchbills = "FETCHED_BILLS"
            
        }
        
        
        struct Result: Codable{
            let billdescription    : String?
            let billrefrence    : String?
            let biller        : String?
            let amount   : Float?
            let custmername   : String?
            let deudate   : String?
            
            private enum CodingKeys: String, CodingKey {
                case billdescription = "BILL_DESCRIPTION"
                case billrefrence = "BILL_REFERENCE"
                case biller = "BILLER"
                case amount = "AMOUNT"
                case custmername = "CUSTOMER_NAME"
                case deudate = "DUE_DATE"
            }
        }
    }
    
    var responseData : Response?
    weak var delegate : paybillfetchdataVMDelegate!
    
    
    func querybill(mobileNo:String,biller:String,hubserviceid:String,billerrefrence:String){
        
        RestAPIManager.querybill(title : "PLEASEWAIT", subTitle : "FETCHINGBILLINFO", billerrefrence : billerrefrence,type: Response.self, mobileNo : mobileNo,biller : biller,hubserviceid : hubserviceid,completion: { (response) in
            DispatchQueue.main.async {
                self.responseData = response
                self.delegate.fetchbilldata()
            }
        }) { (error) in
         
        }
    }
}


