//
//  PayBillSelectAccountViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 26/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class PayBillSelectAccountViewController: UIViewController,UIGestureRecognizerDelegate, UINavigationControllerDelegate {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var Clsprice: UICollectionView!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lblbuyfrom: UILabel!
    
    @IBOutlet weak var consVerticalBuyFrom: NSLayoutConstraint!
    @IBOutlet weak var viewAttriAoountName: AttributedView!
    
    @IBOutlet weak var viewBillLogo: UIView!
    @IBOutlet weak var imgBillLogo: UIImageView!
    @IBOutlet weak var txtBillNumer: UITextField!
    @IBOutlet weak var txtAcountName: UITextField!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var lblBillName: UILabel!
    @IBOutlet weak var lblPayfrom: UILabel!
    
    
    // Variables for Bottom sheet
    //Mark:- Varibales
    private var lastContentOffset: CGFloat = 0
    let Pricearr = ["100", "250","500","1000"]
    var cardnumber = ""
    var flow = ""
    var placecard = "Card Number"
    var savedcardno = ""
    
    // Variables for Bottom sheet
    var bottomSheetVC: BottomSheetViewController?
    var imgView = UIImageView()
    var accountno = ""
    var indexcolletion = IndexPath()
    var dataModels : PayBillVM?
    var cardInfo : [String : String]?
    var cardPlaceholder : String = ""
    var model : paybillfetchdata?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        imgBillLogo.image = nil
        setFonts()
        setData()
        Clsprice?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
        //Add Bottom sheet VC
        addBottomSheetView()
        bottomSheetVC?.delegate = self
        txtAmount.delegate = self
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        //Show selected zero index account
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0 {
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            lblaccount.text = acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!
            lblaccountnumber.text = acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!.hashingOfTheAccount(noStr: acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!)
            
            accountno = acdata[acdata.count - 1]["aACCOUNT_NUMBER"] ?? ""
        }
        txtAmount.keyboardType = .numberPad
        
      imgBillLogo.contentMode = .scaleAspectFit
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let _ = model?.responseData?.fetchbills![0] {
            txtBillNumer.didBegin()
            txtAcountName.didBegin()
            txtAmount.didBegin()
        }
        else if let _ = cardInfo {
            txtAmount.didBegin()
            txtAmount.becomeFirstResponder()
        }
       imgBillLogo.sd_setImage(with: URL(string: global.imgPaybill), placeholderImage: UIImage(named: "placeholderurl"))
        self.tabBarController?.tabBar.isHidden = true
        
       
        if savedcardno != ""{
            txtBillNumer.isEnabled = true
            txtBillNumer.text = savedcardno
            txtBillNumer.didBegin()
        }
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 17, color: Color.white, title: "PAYBILL", placeHolder: "")
        Fonts().set(object: self.lblBillName, fontType: 1, fontSize: 20, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtAmount, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtAcountName, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtBillNumer, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblPayfrom, fontType: 0, fontSize: 12, color: Color.c0_123_255, title: "PAYFROM", placeHolder: "")
        Fonts().set(object: self.lblaccount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "", placeHolder: "")
        
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        
        txtAmount.text = "KES "
        txtAmount.addUI(placeholder: "ENTERAMOUNT")
        txtAcountName.addUI(placeholder: "ACCOUNTNAME")
        txtBillNumer.addUI(placeholder: "METERNUMBER")
        txtAmount.keyboardType = .numberPad
        
        imgBillLogo.layer.cornerRadius = imgBillLogo.frame.size.height / 2
        imgBillLogo.clipsToBounds = true
        
        viewBillLogo.layer.cornerRadius = viewBillLogo.frame.size.height / 2
        viewBillLogo.setShadowWithCornerRadius(view: viewBillLogo, cornerRadius: viewBillLogo.frame.size.height / 2, shodowOffset: CGSize(width: 1.0, height: 1.0))
        
          viewBillLogo.contentMode = .scaleAspectFit
        
    }
    
    func setData() {
         if let dict = model?.responseData?.fetchbills![0] {
            
            Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: "PAYBILL", placeHolder: "")
            cardPlaceholder = "\(dataModels?.dictPaybill["aREFERENCE_LABEL"] ?? placecard)"  ;
            txtBillNumer.addUI(placeholder: cardPlaceholder)
            consVerticalBuyFrom.constant = 21
            viewAttriAoountName.isHidden = false
            txtAcountName.isHidden = false
            txtBillNumer.text = cardnumber
          //  let fetchamount1 = ((dict.amount?.description ?? "0") as NSString).integerValue
             let fetchamount1 = (dict.amount?.description ?? "0") as NSString
            txtAmount.text = "KES \(fetchamount1)"
            txtAcountName.text = dict.custmername
            txtBillNumer.text = cardnumber
            txtBillNumer.isEnabled = false
            txtAcountName.isEnabled = false
        }
        else if let dictCardInfo = cardInfo {
          //  cardPlaceholder = dictCardInfo["aCARD_TYPE"] ?? "ENTERCARNUMBER"
            cardPlaceholder = "CREDITCARDNUMBER"
            Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: "PAYCREDITCARD", placeHolder: "")
            txtBillNumer.addUI(placeholder: cardPlaceholder)
            txtAcountName.text = dictCardInfo["aCARD_NAME"]
           consVerticalBuyFrom.constant = -50
           viewAttriAoountName.isHidden = true
            txtAcountName.isHidden = true
            txtBillNumer.keyboardType = .numberPad
        }
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        self.view.endEditing(true)

        let datacheck = getamount()
        let val1 = datacheck.0
         let valbool = datacheck.1


        if txtAmount.text == "KES " || txtAmount.text == "KES"{
            txtAmount.showError(errStr: "ENTERAMOUNT")
        }
        else if txtAmount.text == "KES 0" || txtAmount.text == "KES 00" || txtAmount.text == "KES 0000"  || txtAmount.text == "KES 000" || txtAmount.text == "KES 00000"  {
        txtAmount.showError(errStr: "PLEASEENTERVALIDAMOUNT")
        }
        else if valbool == false {
            txtAmount.showError(errStr: val1)
        }
        else if txtAcountName.text == "" && model?.responseData?.fetchbills![0] != nil {
            txtAcountName.showError(errStr: "ACCOUNTNAME")
        } else if txtBillNumer.text!.isEmpty {
            txtBillNumer.showError(errStr: cardPlaceholder)

        }
        else if flow == "CREDITCARD" {

           if getgenralRegax(value: txtBillNumer.text!, type: "card") == false {
            txtBillNumer.showError(errStr: "CREDITCARDNUMBER_")
          }
          else {
            if  lblaccountnumber.text == ""{
                self.showMessage(title: "", message: "SELECTNETWORKPROVIDER")
            }
            else{

                if flow == "CREDITCARD" {
                    let datamodel =  PayBillVM1.init(cardnumber: txtBillNumer.text!, acalias: lblaccount.text!, acnumber: lblaccountnumber.text!,amount: txtAmount.text!, recptAccAlise : txtAcountName.text!, biller : cardInfo!["aCARD_TYPE"] ?? "", nominateFlag : dataModels?.nominateflag! ?? "")

                    let nextvc = CheckoutAirTimeViewController.getVCInstance() as! CheckoutAirTimeViewController
                    nextvc.paybilldataModels1 = datamodel
                    nextvc.mySelf = flow
                    nextvc.accountno = accountno
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else {
                    let datamodel =  PayBillVM1.init(cardnumber: txtBillNumer.text!, acalias: lblaccount.text!, acnumber: lblaccountnumber.text!,amount: txtAmount.text!, recptAccAlise : txtAcountName.text!, biller : model?.responseData?.fetchbills?[0].biller ?? "", nominateFlag : dataModels?.nominateflag! ?? "")

                    let nextvc = CheckoutAirTimeViewController.getVCInstance() as! CheckoutAirTimeViewController
                    nextvc.paybilldataModels1 = datamodel
                    nextvc.mySelf = flow
                       nextvc.accountno = accountno
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }


            }
            }
        }
        else if  lblaccountnumber.text == ""{
            self.showMessage(title: "", message: "SELECTNETWORKPROVIDER")
        }
        else{
            let datamodel =  PayBillVM1.init(cardnumber: txtBillNumer.text!, acalias: lblaccount.text!, acnumber: lblaccountnumber.text!,amount: txtAmount.text!, recptAccAlise : txtAcountName.text!, biller : model?.responseData?.fetchbills?[0].biller ?? "", nominateFlag : dataModels?.nominateflag ?? "No")

            let nextvc = CheckoutAirTimeViewController.getVCInstance() as! CheckoutAirTimeViewController
            nextvc.paybilldataModels1 = datamodel
            nextvc.accountno = accountno
            self.navigationController?.pushViewController(nextvc, animated: true)

        }
   }
    
    func getamount() -> (String,Bool) {
        if flow == "CREDITCARD"{
            let amounts = getCreditcardAmountValdiation()
            let min = amounts.0
            let max = amounts.1
            let amount = removeKES(price : txtAmount.text ?? "")
            let msg = "\("AMOUNTBEETWEEN".localized(Session.sharedInstance.appLanguage)) \(min) \("-".localized(Session.sharedInstance.appLanguage)) \(max) "
            return (msg,checkAmount(min:min,max:max,amount:amount))

        }
        else {
            let amounts = getAmountValdiation1(service_code : "PB")
            let min = amounts.0
            let max = amounts.1
            let amount = removeKES(price : txtAmount.text ?? "")
            let msg = "\("AMOUNTBEETWEEN".localized(Session.sharedInstance.appLanguage)) \(min) \("-".localized(Session.sharedInstance.appLanguage)) \(max) "
           return (msg,checkAmount(min:min,max:max,amount:amount))
            
        }
    }
    
    @IBAction func accountselect(_ sender: AttributedButton) {
        view.endEditing(true)
        self.showBottomSheetVC(xibtype: "ACCOUNT", Selectname: lblaccount.text!)
    }
    
    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
        // bottomSheetVC!.view.frame.roundCorners(corners: [.topLeft, .topRight], radius: 3.0)
    }
    
    
    func showBottomSheetVC(xibtype: String,Selectname: String) {
        
        var height = 300  // 340
        let coreData = CoreDataHelper()
        if xibtype == "ACCOUNT" {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
                height = acdata.count * 100
                if acdata.count == 1 {
                    height = 200
                }
            }
        }
        else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acnetwork  = coreData.getDataForEntity(entity: Entity.MNO_INFO)
                height = acnetwork.count * 200
            }
        }
        
        // bottomSheetVC?.partialView = UIScreen.main.bounds.height - CGFloat(height)
        bottomSheetVC!.partialView = self.view.frame.size.height - CGFloat(height)
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imgView.backgroundColor = Color.black
        imgView.alpha = 0.2
        imgView.isUserInteractionEnabled = true
        self.view.addSubview(imgView)
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "", SelectName: "")
        imgView.removeFromSuperview()
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
}

//Text field delegate
extension PayBillSelectAccountViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtAmount{
            txtAmount.text = "KES "
            indexcolletion = IndexPath()
            Clsprice.reloadData()
        }
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == txtAmount {
            
            textField.typingAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            let protectedRange = NSMakeRange(0, 4)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                
                return false
            }
            return true
        }
//        if textField == txtAmount {
//
//            if textField.text == " "{
//                return false
//            } else {
//                return true
//
//            }
//            let protectedRange = NSMakeRange(0, 4)
//            let intersection = NSIntersectionRange(protectedRange, range)
//            if intersection.length > 0 {
//
//                return false
//            }
//            return true
//        }
//        else if textField == txtcardnumber {
//                 if (textField.text?.count)! > 15 &&  range.length == 0 {
//                    return false
//                } else {
//                    if string.count > 15 {
//                        let index = string.index((string.startIndex), offsetBy: 16)
//                        textField.text = String(string[index...])
//                        return false
//                    } else {
//                        return true
//                    }
//                }
//        }
        else {
        
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        
    }
    
   
    
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

//Text field delegate
extension PayBillSelectAccountViewController : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Pricearr.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PricebuttonCell", for: indexPath as IndexPath) as! PricebuttonCell
        
        if indexcolletion == indexPath {
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.white, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = Color.c0_123_255
        }
        else{
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = .clear
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        indexcolletion = indexPath
        txtAmount.text = "KES \(self.Pricearr[indexPath.row])"
        txtAmount.removeError()
        txtAmount.didBegin()
        Clsprice.reloadData()
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.Clsprice.frame.width/4) - 8
        return CGSize(width: width, height: collectionView.frame.size.height - 5)
    }
}

extension PayBillSelectAccountViewController : SelectNetworkDelegate{
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
        lblaccountnumber.text =  acnumber
        lblaccount.text = acname
    }
    
    
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
        hideBottomSheetVC()
        
        lblaccount.text = newtorkname
        lblaccountnumber.text = networkid.hashingOfTheAccount(noStr: networkid)
        accountno = networkid
    }
}


extension PayBillSelectAccountViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //  self.view.endEditing(true)
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
        Global.invalidTimer()
    }
}


/*
 func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
 
 let mobileNumber: String = txtMobileNumber.text!
 
 if textField.text?.count == 0 && string == " " {
 return false
 }
 
 if mobileNumber.isInt {
 if textField == self.txtMobileNumber {
 if (textField.text?.count)! > 11 &&  range.length == 0 {
 return false
 } else {
 if string.count > 8 {
 let index = string.index((string.startIndex), offsetBy: 9)
 textField.text = String(string[index...])
 return false
 } else {
 return true
 }
 }
 } else {
 return true
 }
 } else {
 return true
 }
 }
 
 
 */
