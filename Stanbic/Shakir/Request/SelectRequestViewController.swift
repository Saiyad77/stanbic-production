//
//  SelectRequestViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 02/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class SelectRequestViewController: UIViewController, UINavigationControllerDelegate {
    
    //Mark:- IBOUTLETS
    // @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var colletionview: UICollectionView!
    
    
    var arrtitle = ["Full Statement\nRequest","Cheque Book\nRequest"]//,"Standing Order\nRequest",
    // "Debit & Credit\nCard Request","Open current or\nSavings Account","Insurance\nRequest"]
    
    var arrImages = ["fullStatement","checkRequest"]//,
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // self.tabBarController?.tabBar.isHidden = false
        if #available(iOS 10.0, *) {
            self.colletionview?.isPrefetchingEnabled = false
        }
        
        if let layout = colletionview.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionHeadersPinToVisibleBounds = true
        }
       
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Home.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 0
    }
}

extension SelectRequestViewController : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SelectTransactionHeaderCell", for: indexPath as IndexPath) as! SelectTransactionHeaderCell
            //   headerView.backgroundColor = Color.c10_34_64
            if indexPath.section == 0{
                headerView.lbltitle.text = ""
                headerView.imgBg.image = UIImage(named:"GradienTop2")
            }
            else {
                Fonts().set(object: headerView.lbltitle, fontType: 0, fontSize: 24, color: Color.white, title: "NEWREQUEST", placeHolder: "")
                headerView.imgBg.image = UIImage(named:"GradienTop1")
            }
            return headerView
            
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0 {
            return 0
        }
        else {
            return arrtitle.count
            //  return 30
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectTransactionCell", for: indexPath as IndexPath) as! SelectTransactionCell
        cell.setdata(title: arrtitle[indexPath.row], imageName: arrImages[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
            
        case 0 :
            let vc = RequestBankStatementViewController.getVCInstance() as! RequestBankStatementViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = ChequeBookRequestViewController.getVCInstance() as! ChequeBookRequestViewController
            self.navigationController?.pushViewController(vc, animated: true)
        case 2 :
            let vc = ForexRatesViewController.getVCInstance() as! ForexRatesViewController
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            return
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if DeviceType.IS_IPHONE_5 {
            let collectionViewSize = ((self.colletionview.frame.size.width - 60)/2)
            return CGSize(width : collectionViewSize,height: collectionViewSize - 10)
        }
        else{
        let collectionViewSize = ((self.colletionview.frame.size.width - 60)/2)
        return CGSize(width : collectionViewSize,height: collectionViewSize - 30)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0{
            return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
        else {
            return UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 20)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0{
            let statusBarHieght = UIApplication.shared.statusBarFrame.size.height
            return CGSize(width: collectionView.frame.width, height: statusBarHieght+25)
        }
        else{
            return CGSize(width: collectionView.frame.width, height: 100)
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
}
