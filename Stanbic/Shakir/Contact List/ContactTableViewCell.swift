//
//  ContactTableViewCell.swift
//  dtb
//
//  Created by Sumit5Exceptions on 07/09/18.
//  Copyright © 2018 5Exceptions. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameLetters:UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    @IBOutlet weak var imgContact: UIImageView!
    @IBOutlet weak var viewcard: UIView!
      @IBOutlet weak var cardnameLabel: UILabel!
    
    func roundLabel()
    {
        nameLetters.layer.cornerRadius = nameLetters.frame.width/2
        nameLetters.layer.masksToBounds = true
    
    }
    
    func roundImage()
    {
        imgContact.layer.cornerRadius = imgContact.frame.width/2
        imgContact.layer.masksToBounds = true
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       nameLetters.backgroundColor = Color.c244_245_249
        Fonts().set(object: nameLabel, fontType: 1, fontSize: 13, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: nameLetters, fontType: 0, fontSize: 13, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: phoneNumberLabel, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
//        
//        Fonts().set(object: cell.nameLabel, fontType: 1, fontSize: 13, color: UIColor.black, title: "", placeHolder: "")
//        Fonts().set(object: cell.nameLetters, fontType: 0, fontSize: 13, color: UIColor.black, title: "", placeHolder: "")
//        Fonts().set(object: cell.phoneNumberLabel, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setdata(name:String,number:String,flag:String){
        if flag == "BANK"{
            nameLabel.text = name
            phoneNumberLabel.text = number.hashingOfTheAccount(noStr:number)
            nameLetters.text = (name.characters.first?.description ?? "");
            imgContact.isHidden = true
        }
        else {
        
       nameLabel.text = name
       phoneNumberLabel.text = number
       nameLetters.text = (name.characters.first?.description ?? "");
       imgContact.isHidden = false
            
            imgContact.layer.cornerRadius = imgContact.frame.size.height / 2
            imgContact.layer.borderColor = Color.c233_233_235.cgColor
            imgContact.layer.borderWidth = 1.0
        }
     }

}
