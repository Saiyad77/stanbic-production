//
//  ContactVM.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 30/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

protocol ContactVMDelegate: class {
    func reloadData(status:Int)
}

class ContactVm {
    
    let contactStore = CNContactStore()
    var contacts = [CNContact]()
    
    var responseData = [CNContact]()
    weak var delegate : ContactVMDelegate!
    
    
    
    func getContacts(){
        let keys = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,
            CNContactEmailAddressesKey,
            CNContactImageDataKey
            ] as [Any]
        
        
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        
        do {
            try self.contactStore.enumerateContacts(with: request){
                (contact, stop) in
                if contact.phoneNumbers.count != 0 && contact.givenName != ""
                {
                    for i in 0 ..< contact.phoneNumbers.count {
                        
                        let contactNew = CNMutableContact()
                        
                        let contactNumber = (contact.phoneNumbers[i].value ).value(forKey: "digits") as! String
                        // Phone number
                        let phoneNumber = CNLabeledValue(label: CNLabelWork, value:CNPhoneNumber(stringValue: contactNumber))
                        contactNew.phoneNumbers = [phoneNumber]
                        // Given name
                        contactNew.givenName = contact.givenName
                        //Family name
                        contactNew.familyName = contact.familyName
                        self.contacts.append(contactNew)
                    }
                }
            }
            DispatchQueue.main.async {
                self.responseData = self.contacts
                self.delegate.reloadData(status: 200)
            }
            
        } catch {
            DispatchQueue.main.async {
                self.delegate.reloadData(status: 500)
                
            }
        }
    }

}
