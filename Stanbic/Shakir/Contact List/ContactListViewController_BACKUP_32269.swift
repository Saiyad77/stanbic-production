//
//  ContactListViewController.swift
//  Stanbic
//
//  Created by 5Exceptions 4 on 04/20/19.
//  Copyright © 2018 5Exceptions. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class ContactListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate
{
    
    // @IBOutlet weak var searchBarHeight: NSLayoutConstraint!
    @IBOutlet weak var contactsTableView: UITableView!
    @IBOutlet weak var lbltitle: UILabel!
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtFieldSearch: UITextField!
    @IBOutlet weak var viewtxtsearch: UIView!
    
    var sortedArr = [CNContact]()
    var sort = [Character]()
    var sortedHeaders = [Character:[CNContact]]()
    let contactStore = CNContactStore()
    var contacts = [CNContact]()
    var sectionsArray = [String]()
    var arrdick = [[Character:CNContact]]()
    var oneArray = [Character]()
    var newDick = [Character:[CNContact]]()
    var noDataLabel = UILabel()
    var arrEnrolmentInfo = [[String:String]]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //searchBarHeight.constant = 0
        contactsTableView.delegate = self
        contactsTableView.dataSource = self
        
        viewtxtsearch.layer.shadowColor = Color.c10_34_64.cgColor
        viewtxtsearch.layer.shadowOpacity = 0.5
        viewtxtsearch.layer.shadowOffset = CGSize.zero
        viewtxtsearch.layer.shadowRadius = 0.5
        viewtxtsearch.layer.cornerRadius = 4
        
        // Sort data by type
        
        if CurrentFlow.flowType == FlowType.SendMoney {
            
            arrEnrolmentInfo = EnrollmentInfo().getEnrollmentinforfor(enrollmentinfo: CoreDataHelper().getDataForEntity(entity: Entity.ENROLLMENTS_INFO), type: (CurrentFlow.flowType?.getType())!)
        }
        else if CurrentFlow.flowType == FlowType.PayBill {
            
            arrEnrolmentInfo = EnrollmentInfo().getEnrollmentinforfor(enrollmentinfo: CoreDataHelper().getDataForEntity(entity: Entity.ENROLLMENTS_INFO), type: (CurrentFlow.flowType?.getType())!)
        }
            
        else if CurrentFlow.flowType == FlowType.BuyAirtime {
            
            arrEnrolmentInfo = EnrollmentInfo().getEnrollmentinforfor(enrollmentinfo: CoreDataHelper().getDataForEntity(entity: Entity.ENROLLMENTS_INFO), type: (CurrentFlow.flowType?.getType())!)
        }
            
        else{
            
        }
        
        fetchContact()
        setFonts()
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        //no data label
        noDataLabel = UILabel(frame: CGRect(x: 0, y: 10, width: 200, height: 18))
        noDataLabel.center.x = self.view.center.x
        noDataLabel.frame.origin.y = self.contactsTableView.frame.minY + 100
        noDataLabel.numberOfLines = 0
        noDataLabel.text = "No results found!"
        noDataLabel.textAlignment = .center
        noDataLabel.font = UIFont.init(name: Fonts.kFontMedium, size: CGFloat(14))
        noDataLabel.textColor = .black
        self.noDataLabel.isHidden = true
        self.view.addSubview(noDataLabel)
        
        
        txtFieldSearch.addTarget(self, action: #selector(sortContacts(_:)), for: UIControl.Event.editingChanged)
        self.txtFieldSearch.delegate = self
        self.txtFieldSearch.autocorrectionType = .no
        txtFieldSearch.tintColor = UIColor.blue
        
        // CurrentFlow.flowType = FlowType.BuyAirtime
        
        
        contactsTableView.reloadData()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func fetchContact()
    {
        self.requestAccess { (granted) in
            if granted {
                
                let keys = [
                    CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                    CNContactPhoneNumbersKey,
                    CNContactEmailAddressesKey,
                    CNContactImageDataKey
                    ] as [Any]
                
                
                let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
                
                do {
                    try self.contactStore.enumerateContacts(with: request){
                        (contact, stop) in
                        if contact.phoneNumbers.count != 0 && contact.givenName != ""
                        {
                            for i in 0 ..< contact.phoneNumbers.count {
                                
                                let contactNew = CNMutableContact()
                                
                                let contactNumber = (contact.phoneNumbers[i].value ).value(forKey: "digits") as! String
                                // Phone number
                                let phoneNumber = CNLabeledValue(label: CNLabelWork, value:CNPhoneNumber(stringValue: contactNumber))
                                contactNew.phoneNumbers = [phoneNumber]
                                // Given name
                                contactNew.givenName = contact.givenName
                                //Family name
                                contactNew.familyName = contact.familyName
                                self.contacts.append(contactNew)
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        self.sortContact()
                    }
                    
                } catch {
                    DispatchQueue.main.async {
                        self.showMessage(title: "Error", message: "unable to fetch contacts")
                        
                    }
                }
            }
        }
    }
    
    func sortContact() {
        contacts = contacts.sorted { $0.givenName.localizedCaseInsensitiveCompare($1.givenName) == ComparisonResult.orderedAscending }
        
        for i in 0..<contacts.count
        {
            if let firstLetter = contacts[i].givenName.first
            {
                if(!oneArray.contains((firstLetter)))
                {
                    oneArray.append(firstLetter)
                }
            }
        }
        
        for i in 0..<oneArray.count
        {
            var contactarray = [CNContact]()
            for j in 0..<contacts.count
            {
                if oneArray[i] == contacts[j].givenName.first
                {
                    contactarray.append(contacts[j])
                    
                    //                    let contactNo = contacts[j].phoneNumbers[0].value.stringValue
                    //                    let validCharacters = CharacterSet(charactersIn: "0123456789")
                    //                    let newString = contactNo.components(separatedBy: validCharacters.inverted).joined()
                    //                    arrContactNumber.append(newString)
                }
                
            }
            newDick[oneArray[i]] = contactarray
        }
        contactsTableView.reloadData()
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: CurrentFlow.flowType?.getContactListHeader() ?? "", placeHolder: "")
        Fonts().set(object: txtFieldSearch, fontType: 0, fontSize: 13, color: Color.c10_34_64, title: "", placeHolder: "SEACHCONTACTS")
    }
    
    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func newContact(_ sender: Any)
    {
        if CurrentFlow.flowType == FlowType.SendMoney{
            let nextvc = MPesaToAccountViewController.getVCInstance() as! MPesaToAccountViewController
            nextvc.isMobileEditing = true
            nextvc.contactno = ""
            nextvc.nominateflag = "No"
            nextvc.contactname = ""
            self.navigationController?.pushViewController(nextvc, animated: true)
            
        }
        
        else if CurrentFlow.flowType == FlowType.BuyAirtime{
            let nextvc = BuyAirTimeViewController.getVCInstance() as! BuyAirTimeViewController
            nextvc.contactno = ""
            nextvc.nominateflag = "No"
            nextvc.contactname = ""
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
        
<<<<<<< HEAD
=======
        if CurrentFlow.flowType == FlowType.SendMoney{
            
        }
        
>>>>>>> af14f42590bfe9e6718fc31820b801f5842c90fe
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if(txtFieldSearch.text == "")
        {
            return oneArray.count + 1
            
        }
        else
        {
            return sort.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if(txtFieldSearch.text == "")
        {
            if(section == 0)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HEADERCELL") as! ContactHeaderTableViewCell
                Fonts().set(object: cell.lblLetter, fontType: 1, fontSize: 13, color: Color.c10_34_64, title: "FAVORITES", placeHolder: "")
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HEADERCELL") as! ContactHeaderTableViewCell
                Fonts().set(object: cell.lblLetter, fontType: 1, fontSize: 13, color: Color.c10_34_64, title: "CONTACTS", placeHolder: "")
                return cell
            }
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HEADERCELL") as! ContactHeaderTableViewCell
            Fonts().set(object: cell.lblLetter, fontType: 1, fontSize: 13, color: Color.c10_34_64, title: "CONTACTS", placeHolder: "")
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if(txtFieldSearch.text == "")
        {
            if(section == 0)
            {
                return 60
            }
            else if(section == 1)
            {
                return 60
            }
            else
            {
                return 0
            }
        }
        else
        {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(txtFieldSearch.text == "")
        {
            self.noDataLabel.isHidden = true
            if(section == 0)
            {
                if CurrentFlow.flowType == FlowType.BuyAirtime{
                    return arrEnrolmentInfo.count
                }
                else if CurrentFlow.flowType == FlowType.SendMoney{
                    return arrEnrolmentInfo.count
                }
                else if CurrentFlow.flowType == FlowType.PayBill{
                    return arrEnrolmentInfo.count
                }
                else{
                    return 0
                }
                
            }
            else
            {
                return (newDick[oneArray[section-1]]?.count)!
            }
        }
        else
        {
            //            if sortedHeaders[sort[section]]?.count == 0{  //============ show popup
            //                let validCharacters = CharacterSet(charactersIn: "0123456789")
            //                let newString = txtFieldSearch.text!.components(separatedBy: validCharacters.inverted).joined()
            //                if newString != ""{
            //                    popupView.isHidden = false
            //                }
            //            }
            
            return (sortedHeaders[sort[section]]?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(txtFieldSearch.text == "")
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as! ContactTableViewCell
            
            if(indexPath.section == 0)
            {
                
                let contacts = arrEnrolmentInfo[indexPath.row]
                cell.nameLabel.text = contacts["aBILLER_NAME"]
                cell.phoneNumberLabel.text =  contacts["aBILLER_REFERENCE"]!.hashingOfTheAccount(noStr: contacts["aBILLER_REFERENCE"]!)
                cell.imgContact.isHidden = true
                cell.nameLetters.text = String(contacts["aBILLER_NAME"]?.first ?? " ")
                cell.roundLabel()
            }
            else
            {
                if let contact = self.newDick[oneArray[indexPath.section-1]]?[indexPath.row] {
                    
                    let firstName = contact.givenName
                    let lastName = contact.familyName
                    cell.nameLabel.text = firstName + " " + lastName
                    let contactNo = contact.phoneNumbers[0].value.stringValue
                    let validCharacters = CharacterSet(charactersIn: "0123456789")
                    let newString = contactNo.components(separatedBy: validCharacters.inverted).joined()
                    cell.phoneNumberLabel.text = newString
                    
                    if contact.imageData != nil {
                        cell.imgContact.isHidden = false
                        cell.nameLetters.isHidden = true
                        cell.roundImage()
                        cell.imgContact.image = UIImage(data: contact.imageData!)
                    }
                    else {
                        cell.imgContact.isHidden = true
                        cell.nameLetters.isHidden = false
                        cell.roundLabel()
                        cell.nameLetters.text = String(contact.givenName.first ?? " ") + String(contact.familyName.first ?? " ")
                    }
                }
            }
            return cell
        }
        else
        {
            //sort = one Array
            //sortedArr  = contacts
            //Sorted  = newDic
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as! ContactTableViewCell
            
            Fonts().set(object: cell.nameLabel, fontType: 1, fontSize: 13, color: UIColor.black, title: "", placeHolder: "")
            Fonts().set(object: cell.nameLetters, fontType: 0, fontSize: 13, color: UIColor.black, title: "", placeHolder: "")
            
            Fonts().set(object: cell.phoneNumberLabel, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
            
            if let contact = self.sortedHeaders[sort[indexPath.section]]?[indexPath.row] {
                
                let firstName = contact.givenName
                let lastName = contact.familyName
                cell.nameLabel.text = firstName + " " + lastName
                let contactNo = contact.phoneNumbers[0].value.stringValue
                let validCharacters = CharacterSet(charactersIn: "0123456789")
                let newString = contactNo.components(separatedBy: validCharacters.inverted).joined()
                cell.phoneNumberLabel.text = newString
                
                if contact.imageData != nil {
                    cell.imgContact.isHidden = false
                    cell.nameLetters.isHidden = true
                    cell.roundImage()
                    cell.imgContact.image = UIImage(data: contact.imageData!)
                }
                else {
                    cell.imgContact.isHidden = true
                    cell.nameLetters.isHidden = false
                    cell.roundLabel()
                    cell.nameLetters.text = String(contact.givenName.first ?? " ") + String(contact.familyName.first ?? " ")
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.view.endEditing(true)
        
        var contactname = ""
        var contactnumber = ""
        var nominateflag = ""
        
        if(txtFieldSearch.text == "")
        {
            if(indexPath.section == 0)
            {
<<<<<<< HEAD
                if(indexPath.section == 0)
                {
                    
                    let contacts = arrEnrolmentInfo[indexPath.row]
                    let contactNo = contacts["aBILLER_REFERENCE"]
                    //  let contactNo = User.username
                    let validCharacters = CharacterSet(charactersIn: "0123456789")
                    let newString = contactNo!.components(separatedBy: validCharacters.inverted).joined()
                    contactnumber = newString
                    contactname = contacts["aBILLER_NAME"] ?? ""
                    nominateflag = "YES"
                 }
                else{
                    let contactArray = self.newDick[oneArray[indexPath.section-1]]
                    contactname = (contactArray?[indexPath.row].givenName ?? "") + " " + (contactArray?[indexPath.row].familyName ?? "")
                    if self.newDick[oneArray[indexPath.section-1]] != nil {
                        let contactNo = self.newDick[oneArray[indexPath.section-1]]![indexPath.row].phoneNumbers[0].value.stringValue
                        let validCharacters = CharacterSet(charactersIn: "0123456789")
                        let newString = contactNo.components(separatedBy: validCharacters.inverted).joined()
                        contactnumber = newString
                        nominateflag = "NO"
                        
                    }
                }
=======
                
                let contacts = arrEnrolmentInfo[indexPath.row]
                let contactNo = contacts["aBILLER_REFERENCE"]
                //  let contactNo = User.username
                let validCharacters = CharacterSet(charactersIn: "0123456789")
                let newString = contactNo!.components(separatedBy: validCharacters.inverted).joined()
                contactnumber = newString
                nominateflag = "YES"
>>>>>>> af14f42590bfe9e6718fc31820b801f5842c90fe
            }
            else{
                let contactArray = self.newDick[oneArray[indexPath.section-1]]
                contactname = (contactArray?[indexPath.row].givenName ?? "") + " " + (contactArray?[indexPath.row].familyName ?? "")
                if self.newDick[oneArray[indexPath.section-1]] != nil {
                    let contactNo = self.newDick[oneArray[indexPath.section-1]]![indexPath.row].phoneNumbers[0].value.stringValue
                    let validCharacters = CharacterSet(charactersIn: "0123456789")
                    let newString = contactNo.components(separatedBy: validCharacters.inverted).joined()
                    contactnumber = newString
                    nominateflag = "NO"
                    
                }
            }
        }
        else{
            let contactArray = self.sortedHeaders[sort[indexPath.section]]
            contactname = (contactArray?[indexPath.row].givenName ?? "") + " " + (contactArray?[indexPath.row].familyName ?? "")
            if self.sortedHeaders[sort[indexPath.section]] != nil {
                let contactNo = self.sortedHeaders[sort[indexPath.section]]![indexPath.row].phoneNumbers[0].value.stringValue
                let validCharacters = CharacterSet(charactersIn: "0123456789")
                let newString = contactNo.components(separatedBy: validCharacters.inverted).joined()
                contactnumber = newString
                nominateflag = "NO"
            }
        }
        
        if CurrentFlow.flowType == FlowType.SendMoney{
            let nextvc = MPesaToAccountViewController.getVCInstance() as! MPesaToAccountViewController
            nextvc.isMobileEditing = false
            nextvc.contactno = contactnumber
            nextvc.nominateflag = nominateflag
            nextvc.contactname = contactname
            self.navigationController?.pushViewController(nextvc, animated: true)
            
        }
        
        if CurrentFlow.flowType == FlowType.BuyAirtime{
            let nextvc = BuyAirTimeViewController.getVCInstance() as! BuyAirTimeViewController
            nextvc.contactno = contactnumber
            nextvc.nominateflag = nominateflag
            nextvc.contactname = contactname
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
        
        //          if CurrentFlow.flowType == FlowType.PayBill{
        //            let nextvc = PayBillViewController.getVCInstance() as! PayBillViewController
        //            nextvc.contactno = contactnumber
        //            nextvc.nominateflag = nominateflag
        //            nextvc.contactname = contactname
        //            self.navigationController?.pushViewController(nextvc, animated: true)
        //        }
        
    }
    
    @objc func sortContacts(_ textField:UITextField)
    {
        sortedArr.removeAll()
        sort.removeAll()
        sortedHeaders.removeAll()
        
        for i in contacts
        {
            let name = i.givenName.contains(textField.text?.capitalized ?? "")
            let gName = i.familyName.contains(textField.text?.capitalized ?? "")
            
            let contactNo = i.phoneNumbers[0].value.stringValue
            let validCharacters = CharacterSet(charactersIn: "0123456789")
            let newString = contactNo.components(separatedBy: validCharacters.inverted).joined()
            let mobileNo = newString.contains(textField.text ?? "")
            
            if name != false || gName != false || mobileNo != false
            {
                sortedArr.append(i)
            }
        }
        
        for i in 0..<sortedArr.count
        {
            if let firstLetter = sortedArr[i].givenName.first
            {
                if(!sort.contains((firstLetter)))
                {
                    sort.append(firstLetter)
                }
            }
            
        }
        for i in 0..<sort.count
        {
            var contactarray = [CNContact]()
            for j in 0..<sortedArr.count
            {
                
                if sort[i] == sortedArr[j].givenName.first
                {
                    contactarray.append(sortedArr[j])
                }
                
            }
            sortedHeaders[sort[i]] = contactarray
        }
        
        if sortedHeaders.count == 0{  //============ show popup
            let validCharacters = CharacterSet(charactersIn: "0123456789")
            let newString = txtFieldSearch.text!.components(separatedBy: validCharacters.inverted).joined()
            if newString != ""{
                //popupView.isHidden = false
            }
        }
        
        if sortedArr.count == 0{
            self.noDataLabel.isHidden = false
        }else{
            self.noDataLabel.isHidden = true
        }
        
        self.contactsTableView.reloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(txtFieldSearch.isFirstResponder)
        {
            txtFieldSearch.resignFirstResponder()
        }
        return true
    }
    
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        var contentInset:UIEdgeInsets = self.contactsTableView.contentInset
        contentInset.bottom = (kb?.cgRectValue.height ?? 270)
        contactsTableView.contentInset = contentInset
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        contactsTableView.contentInset = contentInset
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            showSettingsAlert(completionHandler)
        case .restricted, .notDetermined:
            contactStore.requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                } else {
                    DispatchQueue.main.async {
                        self.showSettingsAlert(completionHandler)
                    }
                }
            }
        }
    }
    
    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Would you like to open settings and grant permission to contacts?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
            completionHandler(false)
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            completionHandler(false)
        })
        present(alert, animated: true)
    }
}

extension ContactListViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}

// MARK:_ SORT ARRAY BY aBENEFICIARY_TYPE
struct EnrollmentInfo {
    func getEnrollmentinforfor(enrollmentinfo:[[String:String]],type:String) -> [[String:String]] {
        var arrEnrolmentInfo = [[String:String]]()
        if type != "2" {
            let dict =  ["aBENEFICIARY_TYPE": "7", "aMNO_WALLET_ID": "0", "aDATE": "2018-09-29 00:09:59", "aENROLLMENT_ALIAS": "MY Self", "aBILLER_NAME": "Myself", "aBILLER": "SAFTOPUP", "aBILLER_REFERENCE": "+\("254700707455")", "aMNO": "Safaricom"]
            
            arrEnrolmentInfo.append(dict)
        }
        
        for i in 0..<enrollmentinfo.count{
            if type == enrollmentinfo[i]["aBENEFICIARY_TYPE"] {
                arrEnrolmentInfo.append(enrollmentinfo[i])
            }
        }
        return arrEnrolmentInfo
    }
}
