//
//  ContactHeaderTableViewCell.swift
//  DTB
//
//  Created by Five Exceptions on 15/09/18.
//  Copyright © 2018 Five Exceptions. All rights reserved.
//

import UIKit

class ContactHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var lblLetter: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
