//
//  SalaryAdvanceLoanViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 08/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication

class SalaryAdvanceLoanViewController: UIViewController, UIGestureRecognizerDelegate {
    
    // MARK: - IBOUTLETS
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var txtamount: UITextField!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lblSentToMoney: UILabel!
    @IBOutlet weak var lblaccounttitle: UILabel!
    @IBOutlet weak var lblpercentcharge: UILabel!
    @IBOutlet weak var lblHowMuch: UILabel!
    @IBOutlet weak var lblcharge: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblHeaderTitle: UIView!
    private var lastContentOffset: CGFloat = 0
    @IBOutlet weak var lblexcisedutycost: UILabel!
    
    // MARK: Variables for Bottom sheet
    var bottomSheetVC: BottomSheetViewController?
    var imgView = UIImageView()
    var model = SalaryAdvanceVM()
    var salary = "FETCH"
    var imgHeader =  UIImageView()
    var imgHederHeight : CGFloat = 0
    var checkAmt = Int()
    var selectnetworok = true
    var aMNO_WALLET_ID = ""
    var keyY = CGFloat()
    var accountno = ""
    var TotelCost = ""
    
    // MARK: View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        model.delegate = self
        txtamount.addUI(placeholder: "ADVANCE AMOUNT")
        addBottomSheetView()
        bottomSheetVC?.delegate = self
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        txtamount.keyboardType = .numberPad
        FetchSalary()
        setupHeader()
        txtamount.delegate = self
        txtamount.text = "KES"
        view.bringSubviewToFront(btnCountinue)
        self.hideKeyboardOnTap(#selector(self.dismissKeyboard))
        keyY =  scrollView.contentOffset.y
        
        checkAmt = model.responseData!.resultArray!.amount!
        txtamount.text = "KES \(model.responseData!.resultArray!.amount!)"
        setFonts()
        lblcharge.text = "*"+"\(model.responseData!.resultArray!.chargemessage ?? "")"
        
        let charges = model.responseData?.resultArray?.charges ?? 0
        let str  = "\(charges)"
        TotelCost =  TotelCharges(percent: global.percent, charge:removeKES(price: str))
        getpercentCharges(amount: txtamount.text ?? "0")
        
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            accountno = acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!
            
            if accountno.contains("KES-"){
                let arrac = accountno.components(separatedBy: "KES-")[1]
                accountno = arrac
            }
            
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txtamount.didBegin()
    }
    
    static func getVCInstance() -> UIViewController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Checkbook.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: lblcharge, fontType: 1, fontSize: 10, color: Color.c0_123_255, title: "", placeHolder: "")
        Fonts().set(object: lblexcisedutycost, fontType: 1, fontSize: 10, color: Color.c0_123_255, title: "TRANSACTIONMSG", placeHolder: "")
        Fonts().set(object: lblpercentcharge, fontType: 1, fontSize: 10, color: Color.c0_123_255, title: "", placeHolder: "")
        Fonts().set(object: lbltitle, fontType: 1, fontSize: 24, color: Color.white, title:"CASHADVANCE", placeHolder: "")
        Fonts().set(object: lblHeaderTitle, fontType: 1, fontSize: 17, color: Color.white, title:"CASHADVANCE", placeHolder: "")
        Fonts().set(object: lblHowMuch, fontType: 1, fontSize: 17, color: Color.black, title:"HOWMUCHWOULDYOULIKETOBORROW", placeHolder: "")
        let str = "YOUAREELIGIBLEFORASALARYFORSALARYADVANCE".localized(Session.sharedInstance.appLanguage)
        Fonts().set(object: lblsubtitle, fontType: 1, fontSize: 17, color: Color.white, title:"\(str) \(txtamount.text!)", placeHolder: "")
        //Fonts().set(object: lblSentToMoney, fontType: 0, fontSize: 12, color: Color.c0_123_255, title:"CASHACCOUNT", placeHolder: "")
        Fonts().set(object: self.lblaccount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "", placeHolder: "")
        Fonts().set(object: self.txtamount, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "APPLY", placeHolder: "")
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0 {
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            lblaccount.text = acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!
            lblaccountnumber.text = acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!.hashingOfTheAccount(noStr: acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!)
        }
    }
    
    @IBAction func accountselect(_ sender: AttributedButton) {
        view.endEditing(true)
        self.showBottomSheetVC(xibtype: "ACCOUNT", Selectname: lblaccount.text!)
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func Apply(_ sender: UIButton) {
          if DeviceType.IS_IPHONE_5 {
        DispatchQueue.main.async {
            self.scrollView.contentOffset.y =  self.scrollView.contentOffset.y - 100
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
        
        self.view.endEditing(true)
        if lblaccountnumber.text == "" {
            showMessage(title: "", message: "SELECTACCOUNT")
            return
        }
        
        let amounts = getAmountValdiation1(service_code : "SA")
        let min = amounts.0
        let max = amounts.1
        let amount = removeKES(price : txtamount.text ?? "")
        let msg = "\("AMOUNTBEETWEEN".localized(Session.sharedInstance.appLanguage)) \(min) \("-".localized(Session.sharedInstance.appLanguage)) \(max) "
        
        if txtamount.text == "KES " || txtamount.text == "" {
            txtamount.showError(errStr: "ENTERAMOUNT")
        } else if removeKES(price: txtamount.text ?? "KES ") == "0"{
            txtamount.showError(errStr: "Enter valid amount")
        } else if Int(removeKES(price: txtamount.text!))! > checkAmt {
            txtamount.showError(errStr: "PLEASEENTERVALIDAMOUNT")
        } else if checkAmount(min:min,max:max,amount:amount) == false {
            txtamount.showError(errStr: msg)
        }
//        else if lblaccount.text == "" {
//            showMessage(title: "", message: "SELECTACCOUNT")
//        }
        else {
            salary = "SUBMIT"
            validate()
        }
    }
    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
    func showBottomSheetVC(xibtype: String, Selectname: String) {
        
        var height = 300
        let coreData = CoreDataHelper()
        if xibtype == "ACCOUNT" {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
                height = acdata.count * 100
                if acdata.count == 1 {
                    height = 200
                }
            }
        } else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acnetwork  = coreData.getDataForEntity(entity: Entity.MNO_INFO)
                height = acnetwork.count * 200
            }
        }
        bottomSheetVC!.partialView = self.view.frame.size.height - CGFloat(height)
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imgView.backgroundColor = Color.black
        imgView.alpha = 0.2
        imgView.isUserInteractionEnabled = true
        self.view.addSubview(imgView)
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "", SelectName: "")
        imgView.removeFromSuperview()
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
}

extension SalaryAdvanceLoanViewController: UITextFieldDelegate {
    
    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtamount {
            txtamount.text = "KES "
            
        }
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidChange(textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
//        if textField == txtamount {
//            getpercentCharges(amount: txtamount.text ?? "0")
//        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtamount {
            textField.typingAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            let protectedRange = NSMakeRange(0, 4)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                return false
            }
            let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
            getpercentCharges(amount: newString)
            return true
        } else {
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        }
    }
    
    func textField(
        textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField.text == " "{
            return false
        }
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification) {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 5)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded()
              if DeviceType.IS_IPHONE_5 {
            self.scrollView.contentOffset.y = 80
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            }
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded()
            
              if DeviceType.IS_IPHONE_5 {
              self.scrollView.contentOffset.y = self.keyY
             UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
              }
            }
        }
    }
}

extension SalaryAdvanceLoanViewController:SalaryVMDelegate {
    func salayData() {
        if salary == "FETCH" {
            if model.responseData?.statusCode == 200 {
                checkAmt = model.responseData!.resultArray!.amount!
                txtamount.text = "KES \(model.responseData!.resultArray!.amount!)"
                setFonts()
                lblcharge.text = "*"+"\(model.responseData!.resultArray!.chargemessage ?? "")"
                getpercentCharges(amount: txtamount.text ?? "0")
                let charges = model.responseData?.resultArray?.charges ?? 0
                let str  = "\(charges)"
                  TotelCost =  TotelCharges(percent: global.percent, charge:removeKES(price: str))
            } else {
                let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                nextvc.message = model.responseData?.statusMessage ?? ""
                nextvc.Flow = "1"
                self.navigationController?.pushViewController(nextvc, animated: true)
            }
        } else {
            if model.responseData?.statusCode == 200 {
                let nextvc = ChequeBookSuccessViewController.getVCInstance() as! ChequeBookSuccessViewController
                nextvc.msg = (model.responseData?.statusMessage!)!
                self.navigationController?.pushViewController(nextvc, animated: true)
            } else {
                let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                nextvc.message = model.responseData?.statusMessage ?? ""
                self.navigationController?.pushViewController(nextvc, animated: true)
            }
        }
    }
    
    func submitSalary() {
        if !isInternetAvailable(){
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        let amount = txtamount.text!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range:nil)
        
        model.SalaryAdvanceSubmit(mob: OnboardUser.mobileNo, accountalias: lblaccount.text!, action: "SUBMIT", amount:amount, fname: model.responseData?.resultArray?.firstname ?? "", lname: model.responseData?.resultArray?.lastname ?? "", IDtype: (model.responseData?.resultArray?.idtype ?? "")!, idnumber: (model.responseData?.resultArray?.idnumber ?? "")!, totelcharge: TotelCost)
    }
    
    func FetchSalary() {
        if !isInternetAvailable() {
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        model.SalaryAdvance(mob: OnboardUser.mobileNo, accountalias:  lblaccount.text!, action: "FETCH", amount: "")
        }
}

extension SalaryAdvanceLoanViewController: SelectNetworkDelegate {
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
        lblaccountnumber.text =  acnumber
        lblaccount.text = acname
    }
    
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
        hideBottomSheetVC()
        lblaccount.text = newtorkname
        lblaccountnumber.text = networkid.hashingOfTheAccount(noStr: networkid)
        
    }
}

extension SalaryAdvanceLoanViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = imgHederHeight - (scrollView.contentOffset.y + 10)
        let height = min(max(y, 60), 400)
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
        if scrollView.contentOffset.y > 70 {
            self.lblHeaderTitle.isHidden = false
        } else {
            self.lblHeaderTitle.isHidden = true
        }
        Global.invalidTimer()
    }
    
    func setupHeader() {
        imgHederHeight = 180
        scrollView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        scrollView.delegate = self
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: imgHederHeight)
        imgHeader.contentMode = .scaleAspectFill
        imgHeader.image = UIImage.init(named: "GradientBar-3")
        imgHeader.clipsToBounds = true
        viewContainer.frame.size.height = viewContainer.frame.size.height + lblHeaderTitle.frame.size.height
        view.addSubview(imgHeader)
        self.view.bringSubviewToFront(scrollView)
        self.view.bringSubviewToFront(btnBack)
        self.view.bringSubviewToFront(lblHeaderTitle)
    }
}

extension SalaryAdvanceLoanViewController:AuthDelegate {
    
    func validAuth() {
        submitSalary()
    }
    
    func validate() {
        //Check if user enable biometric access
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            let context = LAContext()
            Authntication(context).authnticateUser { (success, message) in
                DispatchQueue.main.async {
                    if success == true {
                        self.validAuth()
                    } else {
                        if message != "" {
                            self.showMessage(title: "", message: message)
                        }
                    }
                }
            }
        } else {
            let vc = AuthoriseViewController.getVCInstance() as! AuthoriseViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func getpercentCharges(amount:String) {
        if model.responseData?.resultArray?.chargetype == "FIXED"{
             let newprice = removeKES(price: amount)
              let price = Double(newprice)
            if newprice != "" {
         let charge = Double(model.responseData?.resultArray?.charges ?? 0)
            let percent =  TotelCost.toDouble()! + price!
                //let totelamount = percent + TotelCost.toDouble()!
            lblpercentcharge.text = "\("YOURTOTELREPAYMENTAMOUNTIS".localized(Session.sharedInstance.appLanguage)) \(percent)"
            }
            else{
                 lblpercentcharge.text = "\("YOURTOTELREPAYMENTAMOUNTIS".localized(Session.sharedInstance.appLanguage)) \(0)"
            }
        }
        else {
            
            let newprice = removeKES(price: amount)
            if newprice != "" {
                let price = Double(newprice)
              //  let charge = Double(model.responseData?.resultArray?.charges ?? 0)
                //let percent = (price!*charge/100) + price!
                  let percent =  TotelCost.toDouble()! + price!
                 // let totelamount = percent + TotelCost.toDouble()!
                lblpercentcharge.text = "\("YOURTOTELREPAYMENTAMOUNTIS".localized(Session.sharedInstance.appLanguage)) \(percent)"
            } else {
                lblpercentcharge.text = "\("YOURTOTELREPAYMENTAMOUNTIS".localized(Session.sharedInstance.appLanguage)) \(0)"
            }
        }
    }
    
 
}
