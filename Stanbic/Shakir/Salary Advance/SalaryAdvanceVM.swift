//
//  SalaryAdvanceVM.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 08/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation

protocol SalaryVMDelegate: class {
    func salayData()
}

class SalaryAdvanceVM{
    
    // Model : - for FETCH
    struct ResponseFetch: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let resultArray: Result?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case resultArray = "RESULTS_ARRAY"
        }
    }
    
    struct Result: Codable{
        let statusCode: Int?
        let eligibility: Int?
        let amount: Int?
        let responce: String?
        let firstname: String?
        let lastname: String?
        let idtype: String?
        let idnumber: String?
        let chargemessage: String?
        let charges: Double?
        let chargetype: String?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case eligibility = "ELIGIBILITY"
            case amount = "AMOUNT"
            case responce = "RESPONSE"
            case firstname = "FIRST_NAME"
            case lastname = "LAST_NAME"
            case idtype = "ID_TYPE"
            case idnumber = "ID_NUMBER"
            case chargemessage = "CHARGE_MESSAGE"
            case charges = "CHARGE"
            case chargetype = "CHARGE_TYPE"
        }
    }
    
    var responseData : ResponseFetch?
    weak var delegate : SalaryVMDelegate!
    
    // SALARY ADVANCE :-
    func SalaryAdvance(mob:String,accountalias:String,action:String,amount:String) {
        
        RestAPIManager.salaryAdvance(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOURREQUEST", type: ResponseFetch.self, mobileNo: OnboardUser.mobileNo, accountalias: accountalias, pin: OnboardUser.pin, action: action, amount: "",completion: { (response) in
            DispatchQueue.main.async {
                self.responseData = response
                self.delegate.salayData()
             }
        }){ (error) in
           
        }
    }
    
    // SALARY ADVANCE :-
    func SalaryAdvanceSubmit(mob:String,accountalias:String,action:String,amount:String,fname:String,lname:String,IDtype:String,idnumber:String,totelcharge:String) {
    
        RestAPIManager.salaryAdvanceSubmit(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOURREQUEST", type: ResponseFetch.self, mobileNo: OnboardUser.mobileNo, accountalias: accountalias, pin: OnboardUser.pin, action: action, amount: amount, fname: fname, lname: lname, IDtype: IDtype, idnumber: idnumber,completion: { (response) in
    DispatchQueue.main.async {
    self.responseData = response
    self.delegate.salayData()
    
    }
    }){ (error) in
    
    }
    }
}
