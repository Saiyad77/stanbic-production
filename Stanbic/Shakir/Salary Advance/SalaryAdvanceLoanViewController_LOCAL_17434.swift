//
//  SalaryAdvanceLoanViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 08/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication


class SalaryAdvanceLoanViewController: UIViewController,UIGestureRecognizerDelegate {
    
    //Mark:- IBOUTLETS
 //   @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var txtamount: UITextField!
   
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lblaccounttitle: UILabel!
    
    // Variables for Bottom sheet
    var bottomSheetVC: BottomSheetViewController?
    var imgView = UIImageView()
    
    var model = SalaryAdvanceVM()
    var salary = "FETCH"

    override func viewDidLoad() {
        super.viewDidLoad()
        model.delegate = self
        
        txtamount.addUI(placeholder: "ENTER AMOUNT")
        addBottomSheetView()
        bottomSheetVC?.delegate = self
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
            self.tabBarController?.tabBar.isHidden = true
        
        
        self.tabBarController?.tabBar.isHidden = true
        txtamount.keyboardType = .numberPad
        FetchSalary()
          initialSetUpNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txtamount.didBegin()
        
    }
    
    static func getVCInstance() -> UIViewController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Checkbook.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        //Fonts().set(object: lbltitle, fontType: 1, fontSize: 24, color: Color.white, title:"SALARYADVANCELOANS", placeHolder: "")
        
        let str = "YOUAREELIGIBLEFORASALARYFORSALARYADVANCE".localized(Session.sharedInstance.appLanguage)
        Fonts().set(object: lblsubtitle, fontType: 1, fontSize: 17, color: Color.white, title:"\(str) \(txtamount.text!)", placeHolder: "")
        
        Fonts().set(object: lblaccounttitle, fontType: 1, fontSize: 9, color: Color.c0_123_255, title:"SENDMONEYTO", placeHolder: "")
        Fonts().set(object: self.lblaccount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "", placeHolder: "")
        
         Fonts().set(object: self.txtamount, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        
         Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "APPLY", placeHolder: "")
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            lblaccount.text = acdata[0]["aACCOUNT_ALIAS"]!
            lblaccountnumber.text = acdata[0]["aACCOUNT_NUMBER"]!.hashingOfTheAccount(noStr: acdata[0]["aACCOUNT_NUMBER"]!)
        }
      
        
    }
    
    // MARK: - Required Method's
    func initialSetUpNavigationBar() {
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = "SALARYADVANCELOANS".localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage:(UIImage(named: "GradienTop2")?.resizableImage(withCapInsets: UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0), resizingMode: .stretch))!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    

    
    @IBAction func accountselect(_ sender: AttributedButton) {
        view.endEditing(true)
        self.showBottomSheetVC(xibtype: "ACCOUNT", Selectname: lblaccount.text!)
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Apply(_ sender: UIButton) {
        if txtamount.text == "KES " || txtamount.text == "KES"{
            txtamount.showError(errStr: "ENTERAMOUNT")
        }
        else if lblaccount.text == "" {
            showMessage(title: "", message: "SELECTACCOUNT")
        }else {
             salary = "SUBMIT"
            submitSalary() // APPLY
        }
    }
    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
  
    func showBottomSheetVC(xibtype: String,Selectname: String) {
        
        var height = 300  // 340
        let coreData = CoreDataHelper()
        if xibtype == "ACCOUNT" {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
                height = acdata.count * 200
                if acdata.count == 1 {
                    height = 200
                }
            }
        }
        else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acnetwork  = coreData.getDataForEntity(entity: Entity.MNO_INFO)
                height = acnetwork.count * 200
            }
        }
        
        // bottomSheetVC?.partialView = UIScreen.main.bounds.height - CGFloat(height)
        bottomSheetVC!.partialView = self.view.frame.size.height - CGFloat(height)
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imgView.backgroundColor = Color.black
        imgView.alpha = 0.2
        imgView.isUserInteractionEnabled = true
        self.view.addSubview(imgView)
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "", SelectName: "")
        imgView.removeFromSuperview()
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
}

//Text field delegate
extension SalaryAdvanceLoanViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtamount {
            txtamount.text = "KES "
            
        }
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtamount {
            
            textField.typingAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            let protectedRange = NSMakeRange(0, 4)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                return false
            }
            return true
        }
        else{
            return true
        }
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

extension SalaryAdvanceLoanViewController:SalaryVMDelegate{
    
    func salayData() {
        if salary == "FETCH" {
            if model.responseData?.statusCode == 200 {
            txtamount.text = "KES \(model.responseData!.resultArray!.amount!)"
               setFonts()
            }
            else {
                showMessageAndBack(title: "",message: (model.responseData?.statusMessage)!)
            }
        }
        // SUBMIT
        else {
        if model.responseData?.statusCode == 200 {
            let nextvc = ChequeBookSuccessViewController.getVCInstance() as! ChequeBookSuccessViewController
            nextvc.msg = (model.responseData?.statusMessage!)!
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
        else {
            self.showMessage(title: "", message: (model.responseData?.statusMessage)!)
        }
      }
    }
    
    func submitSalary(){
           let amount = txtamount.text!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range:nil)
        
        model.SalaryAdvance(mob: OnboardUser.mobileNo, accountalias: lblaccount.text!, action: "SUBMIT", amount:amount)
    }
    
    func FetchSalary(){
        model.SalaryAdvance(mob: OnboardUser.mobileNo, accountalias:  lblaccount.text!, action: "FETCH", amount: "")
    }
}


extension SalaryAdvanceLoanViewController : SelectNetworkDelegate{
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
        lblaccountnumber.text =  acnumber
        lblaccount.text = acname
    }
    
    
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
        hideBottomSheetVC()
    }
}
