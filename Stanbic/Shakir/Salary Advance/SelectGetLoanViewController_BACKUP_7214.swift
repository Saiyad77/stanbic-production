//
//  SelectGetLoanViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 08/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class SelectGetHeaderCell: UITableViewCell {
    @IBOutlet weak var lbltitle:UILabel!
    @IBOutlet weak var imgBg: UIImageView!
 }

class SelectGetLoanCell: UITableViewCell {
    @IBOutlet weak var lbltitle:UILabel!
    @IBOutlet weak var imglogo: UIImageView!
    
    override func awakeFromNib() {
        Fonts().set(object: lbltitle, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: "", placeHolder: "")
    }
 }

class SelectGetLoanViewController: UIViewController {
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var tabelviewloan: UITableView!
    
    let arrTitle = ["Car loans","Mortgage loans","Salary advance loans","Personal Unsecured loans","Equity loans","Land loans"]
    
    let arrImage = ["car loan","Mortgage loans","salary advance loan","personal loan","salary advance loan","salary advance loan"]
    
    let model = SalaryAdvanceVM()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setFonts()
        model.delegate = self
        initialSetUpNavigationBar()
    }
    
    
    // MARK: - Required Method's
    func initialSetUpNavigationBar(){
        
<<<<<<< HEAD
        self.navigationController?.setNavigationBarHidden(false, animated: true)
=======
        //        let button = UIButton(type: UIButton.ButtonType.custom)
        //        button.setImage(UIImage(named: "back.png"), for: UIControl.State.normal)
        //        button.addTarget(self, action:Selector(("callMethod")), for: UIControl.Event.touchDragInside)
        //        button.frame = CGRect(x:0, y:0, height:0, width:0)
        //        let barButton = UIBarButtonItem(customView: button)
        //
        //        self.navigationItem.leftBarButtonItems = [newBackButton,UIButton]
        //
      
       self.navigationController?.setNavigationBarHidden(false, animated: true)
>>>>>>> 34a74e0847c67017d54278339a1116a21c35b08a
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 24)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = "GETLOAN".localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage:(UIImage(named: "GradientBar-2")?.resizableImage(withCapInsets: UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0), resizingMode: .stretch))!)
        
        self.tabelviewloan.reloadData()
    }
    
    // MARK:_SET FONT
    func setFonts() {
     //   Fonts().set(object: lbltitle, fontType: 1, fontSize: 24, color: Color.white, title:"GETLOAN", placeHolder: "")
    }
    
    static func getVCInstance() -> UIViewController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Checkbook.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SelectGetLoanViewController :UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectGetLoanCell") as! SelectGetLoanCell
        
        cell.imglogo.image = UIImage.init(named: arrImage[indexPath.row])
        cell.lbltitle.text = arrTitle[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch indexPath.row {
        case 0:
            print(indexPath)
            
        case 1:
            print(indexPath)
            
        case 2:

           FetchSalary()
            
        case 3:
            print(indexPath)
            
        default:
            print(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
}


extension SelectGetLoanViewController:SalaryVMDelegate{
    func salayData() {
        print(model.responseData)
        
        if model.responseData?.statusCode == 200 {
                        let nextvc = SalaryAdvanceLoanViewController.getVCInstance() as! SalaryAdvanceLoanViewController
                          nextvc.model = model
                        self.navigationController?.pushViewController(nextvc, animated: true)
        }
        else {
            self.showMessage(title: "", message: (model.responseData?.statusMessage)!)
        }
    }
    
    
    func FetchSalary(){
     model.SalaryAdvance(mob: OnboardUser.mobileNo, accountalias: "Current Acct-KES-0100006123443", action: "FETCH", amount: "")
    }
}
