//
//  SelectTransactionViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 26/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

// HEADER TITLE CELL COLLETON VIEW
class  SelectTransactionHeaderCell: UICollectionReusableView {
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imgBg: UIImageView!
    
    override func awakeFromNib() {
        
    }
}

class  SelectTransactionCell: UICollectionViewCell {
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imglogo: UIImageView!
    @IBOutlet weak var viewbg: UIView!
    @IBOutlet weak var viewImgbg: UIView!
    @IBOutlet weak var imglayerview: UIView!
    
    override func awakeFromNib() {
        Fonts().set(object: lbltitle, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: "Fullstatement", placeHolder: "")
        Global.createCardView(view: viewbg)
        imglayerview.isHidden = true
    }
    
    // Setdata
    func setdata(title:String, imageName : String) {
        lbltitle.text = title
        imglogo.image = UIImage.init(named: imageName)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
}

class SelectTransactionViewController: UIViewController, UINavigationControllerDelegate{
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var colletionview: UICollectionView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    
    var arrImages = ["mobile_money","mpesatoac", "banktobank","internaltransfer", "pesalinktransfer","paybills","buy_Goods"]
     var arrtitle = ["Mobile Money\nTransfer","Mpesa to Account\nTransfer", "Bank to Bank\nTransfer","Internal\nTransfer", "PesaLink \nTransfer","Mpesa Paybill","Buy Goods"]
    let serviceTitle = ["Mobile_money", "STKTOPUP", "RTGS", "TF","PESALINKSTATIC","ACCBILL","ACCTILL"]
    
    // MARK: - Properties
    let headerViewMaxHeight: CGFloat = 165
    let headerViewMinHeight: CGFloat = 44 + UIApplication.shared.statusBarFrame.height
    var serviceTitleFinal = [String]()
    var serviceIconFinal = [String]()
    var activeServices = [[String : String]]()
    let modelcontacts = ContactVm()
    var contacts = [CNContact]()
    var oneArray = [Character]()
    var newDick = [Character:[CNContact]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        modelcontacts.delegate = self
        modelcontacts.getContacts()
        
        // self.tabBarController?.tabBar.isHidden = false
        
        if #available(iOS 10.0, *) {
            self.colletionview?.isPrefetchingEnabled = false
        }
        
        if let layout = colletionview.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionHeadersPinToVisibleBounds = true
        }
        checkActiveService()
        
                
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Home.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 0
    }
    
}

extension SelectTransactionViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SelectTransactionHeaderCell", for: indexPath as IndexPath) as! SelectTransactionHeaderCell
            //   headerView.backgroundColor = Color.c10_34_64
            if indexPath.section == 0{
                headerView.lbltitle.text = ""
                headerView.imgBg.image = UIImage(named:"GradienTop2")
            }
            else {
                Fonts().set(object: headerView.lbltitle, fontType: 0, fontSize: 24, color: Color.white, title: "NEWTRANSACTION", placeHolder: "")
                headerView.imgBg.image = UIImage(named:"GradienTop1")
            }
            return headerView
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0 {
            return 0
        }
        else {
            //return arrtitle.count
            return serviceTitleFinal.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectTransactionCell", for: indexPath as IndexPath) as! SelectTransactionCell
        cell.setdata(title: serviceTitleFinal[indexPath.row], imageName: serviceIconFinal[indexPath.row])
        
        let activeService = activeServices[indexPath.row]
        if activeService["aACTIVE"] == "2" {
        }
        else {
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
     let activeService = activeServices[indexPath.row]
        if activeService["aACTIVE"] == "2" {
            self.showMessage(title: "INACTIVE", message: activeService["aINACTIVE_MESSAGE"] ?? "")
        }

        else {
            switch serviceTitleFinal[indexPath.row]{

            case "Mobile Money\nTransfer" :
                CurrentFlow.flowType = FlowType.SendMoney
                let vc = ContactListViewController.getVCInstance() as! ContactListViewController
                vc.contacts = contacts
                vc.oneArray1 = oneArray
                vc.newDick1 = newDick
                self.navigationController?.pushViewController(vc, animated: true)

            case "Mpesa to Account\nTransfer":
                CurrentFlow.flowType = FlowType.MpesaToAccount
                let vc = AccountToPhoneViewController.getVCInstance() as! AccountToPhoneViewController
                self.navigationController?.pushViewController(vc, animated: true)

            case "Bank to Bank\nTransfer" :
                //Check RTGS Time
                 self.checkRTGSTransferTime()

               // GonextVC()

            case "Internal\nTransfer" :
                CurrentFlow.flowType = FlowType.InternalFundTransfer

                let arrEnrolmentInfo = EnrollmentInfo().getEnrollmentinforfor(enrollmentinfo: CoreDataHelper().getDataForEntity(entity: Entity.BENEFICIARIES_INFO),  type: (CurrentFlow.flowType?.getType())!)

                if arrEnrolmentInfo.count > 0 {
                    let nextvc = ContactListFundTransferViewController.getVCInstance() as! ContactListFundTransferViewController
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else {
                    let nextvc = InternalFundTransferViewController.getVCInstance() as! InternalFundTransferViewController
                    nextvc.nominateflag = "No"
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
            case "PesaLink \nTransfer" :
                let nextvc = PesaLinkSelectViewController.getVCInstance() as! PesaLinkSelectViewController
                nextvc.contacts = contacts
                nextvc.oneArray1 = oneArray
                nextvc.newDick1 = newDick
                self.navigationController?.pushViewController(nextvc, animated: true)
            case "Mpesa Paybill" :
                CurrentFlow.flowType = FlowType.PayBillTransaction
                let nextvc = PayBillTransactionVC.getVCInstance() as! PayBillTransactionVC
                self.navigationController?.pushViewController(nextvc, animated: true)
            case "Buy Goods" :
                CurrentFlow.flowType = FlowType.BuyGoods
                let nextvc = BuyGoodsViewController.getVCInstance() as! BuyGoodsViewController
                self.navigationController?.pushViewController(nextvc, animated: true)
            default:
               return
            }
       }
     }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if DeviceType.IS_IPHONE_5 {
            
            let collectionViewSize = ((self.colletionview.frame.size.width - 60)/2)
            return CGSize(width : collectionViewSize,height: collectionViewSize - 10)
            
        }
        else{
        
        let collectionViewSize = ((self.colletionview.frame.size.width - 60)/2)
        return CGSize(width : collectionViewSize,height: collectionViewSize - 30)
        }
       
    }
      func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5) {
            if let cell = collectionView.cellForItem(at: indexPath) as? SelectTransactionCell {
                cell.transform = .init(scaleX: 0.95, y: 0.95)
                cell.contentView.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
            }
        }
    }
    
      func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.5) {
            if let cell = collectionView.cellForItem(at: indexPath) as? SelectTransactionCell {
                cell.transform = .identity
                cell.contentView.backgroundColor = .clear
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0{
            return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
        else {
            return UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 20)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0{
            let statusBarHieght = UIApplication.shared.statusBarFrame.size.height
            return CGSize(width: collectionView.frame.width, height: statusBarHieght+25)
        }
        else{
            return CGSize(width: collectionView.frame.width, height: 100)
        }
    }
     func collectionView(_ collectionView: UICollectionView,
                                 willDisplay cell: UICollectionViewCell,
                                 forItemAt indexPath: IndexPath) {
        
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
}

extension SelectTransactionViewController {
    
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
        }
    }
    
    func checkRTGSTransferTime() {
        RestAPIManager.checkRTGSTransfer(title: "PLEASEWAIT", subTitle: "RTGSTIMESLOT", type: Response.self, completion: { (response) in
            DispatchQueue.main.async {
                if response.statusCode == 200 {
                    self.GonextVC()
                 }
                else if response.statusCode == 452 {
                    let alertCon = UIAlertController(title: "Stanbic", message: response.statusMessage ?? "", preferredStyle: UIAlertController.Style.alert)
                    let actionCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil)
                    let actionConfirm = UIAlertAction(title: "Proceed", style: .default) { action in
                        DispatchQueue.main.async {
                           self.GonextVC()
                        }
                    }
                    alertCon.addAction(actionConfirm)
                    alertCon.addAction(actionCancel)
                    self.present(alertCon,animated: true)
                  //self.showMessage(title: "", message: response.statusMessage ?? "")
                }
                else {
                    let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                    nextvc.message = response.statusMessage ?? ""
                    nextvc.Flow = "salary"
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
            }
        }) { (error) in
     //       print(error.localizedDescription)
        }
    }
    
    func GonextVC() {
        CurrentFlow.flowType = .Rtgs
        
        let   arrEnrolmentInfo = EnrollmentInfo().getEnrollmentinforfor(enrollmentinfo: CoreDataHelper().getDataForEntity(entity: Entity.BENEFICIARIES_INFO),  type: (CurrentFlow.flowType?.getType())!)
        if arrEnrolmentInfo.count > 0 {
            let nextvc = ContactListFundTransferViewController.getVCInstance() as! ContactListFundTransferViewController
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
        else{
            let nextvc = RTGSViewController.getVCInstance() as! RTGSViewController
            nextvc.nominateflag = "No"
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
}

// Fetch COntact Fastly
extension SelectTransactionViewController : ContactVMDelegate{
    
    func reloadData(status: Int) {
        if status == 200 {
            contacts = modelcontacts.responseData
            sortContact()
        }
    }
    
    func sortContact() {
        contacts = contacts.sorted { $0.givenName.localizedCaseInsensitiveCompare($1.givenName) == ComparisonResult.orderedAscending }
        
        for i in 0..<contacts.count
        {
            if let firstLetter = contacts[i].givenName.first
            {
                if(!oneArray.contains((firstLetter)))
                {
                    oneArray.append(firstLetter)
                }
            }
        }
        
        for i in 0..<oneArray.count
        {
            var contactarray = [CNContact]()
            for j in 0..<contacts.count
            {
                if oneArray[i] == contacts[j].givenName.first
                {
                    contactarray.append(contacts[j])
                }
                
            }
            newDick[oneArray[i]] = contactarray
        }
    }
}

// check Active Service
extension SelectTransactionViewController {
    
    func checkActiveService(){
        for i in 0 ..< serviceTitle.count{
            if let activeService = self.checkActiveService(service : serviceTitle[i]) {
                
                if activeService["aACTIVE"] != "3" {
                 // Only add data whice have service id 1 or 2. 3 is disable service
                    serviceTitleFinal.append(arrtitle[i])
                    serviceIconFinal.append(arrImages[i])
                    activeServices.append(activeService)
                }
            }
            else {
                if serviceTitle[i] == "PESALINKSTATIC"{
                serviceTitleFinal.append(arrtitle[i])
                serviceIconFinal.append(arrImages[i])
                activeServices.append(["aACTIVE":"1"])
                }
            }
        }
        colletionview.reloadData()
     }
}

// ADD SHADOW
extension UIView {
    func addshaow() {
        self.layer.cornerRadius = 4.0
        layer.shadowRadius = 4
        layer.shadowOpacity = 0.16
        layer.shadowOffset = CGSize(width: -0.5, height: -0.5)
        self.clipsToBounds = false
    }
    
    func addshaow(offset : CGSize) {
        self.layer.cornerRadius = 4.0
        layer.shadowRadius = 4
        layer.shadowOpacity = 0.16
        layer.shadowOffset = offset
        self.clipsToBounds = false
    }
}

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.characters.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
