//
//  Extension+UITextfield.swift
//  Stanbic
//
//  Created by Vijay Patidar on 18/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    //shake
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.4
        animation.values = [-15.0, 15.0, -15.0, 15.0, -9.0, 9.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    // add error label
    func showError(errStr : String){
        let appLanguage = Session.sharedInstance.appLanguage
        self.shake()
        self.resignFirstResponder()
        for subview in self.subviews{ //remove error label
            if let lineView = subview as? UIImageView{
                lineView.frame.size.height = 2
                lineView.backgroundColor = Color.red
            }
        }
        let x = 0 //Int(-1 * (self.frame.minX - 20))
        let y = Int(self.frame.height + CGFloat(2))
        let wid = self.frame.maxX - 20
        let label = UILabel(frame: CGRect(x: x, y: y, width: Int(wid), height: 14))
        label.numberOfLines = 0
        label.text = errStr.localized(appLanguage as String)
        label.font = UIFont.init(name: Fonts.kFontMedium, size: CGFloat(10))
        label.textColor = Color.red
        self.addSubview(label)
    }
    
    
    //remove error from textfield when not editing
    func removeError(){
        for subview in self.subviews{ //remove error label
            if let label = (subview as? UILabel){
                if label.center.y > self.bounds.midY{
                    label.removeFromSuperview()
                }
            }else if let lineView = subview as? UIImageView{
                lineView.frame.size.height = 1
                lineView.backgroundColor = Color.c217_219_222
            }
        }
        //self.resignFirstResponder()
    }
    
    
    func addUI(placeholder:String){
        DispatchQueue.main.async{
            self.addLine()
            self.addLabll(placeholder: placeholder)
        }
    }
    
    func addUItwo(placeholder:String){
        DispatchQueue.main.async{
            self.addLine()
            self.addLablltwo(placeholder: placeholder)
        }
    }
    
    func didBegin(){
        for subview in self.subviews{  //remove error label
            
            if let lineView = subview as? UIImageView{
                lineView.frame.size.height = 2
                lineView.backgroundColor = Color.c0_123_255  // Blue
                
            }else if let label = (subview as? UILabel){
                
                if label.transform == .identity{
                    label.alpha = 0
                    label.textColor = Color.c134_142_150 // Grey
                    label.text = label.text?.uppercased()
                    label.sizeToFit()
                    UIView.animate(withDuration: 0.2, delay: 0.15, options: .transitionCrossDissolve, animations: {
                        label.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
                        label.frame = CGRect(x: 0, y: Int(self.bounds.minY - 12), width: Int(label.frame.width), height: 18)
                        label.adjustsFontSizeToFitWidth = true
                        
                        label.textColor = Color.c0_123_255 // Blue
                      //  label.textAlignment = .center
                        label.alpha = 1
                    }, completion: nil)
                }
            }
        }
        
    }
    
    func didEnd(){
        if self.text == ""{
            for subview in self.subviews{
                if let lineView = subview as? UIImageView{
                    lineView.frame.size.height = 0
                    lineView.backgroundColor = Color.c217_219_222  // Line Grey
                    
                }else if let label = subview as? UILabel{
                   
                    UIView.animate(withDuration: 0.2, animations: {
                        
                        label.transform = CGAffineTransform.identity
                        label.frame = CGRect(x: 0, y: 0, width: Int(label.frame.width), height: 18)
                        label.center.y = self.bounds.midY
                        label.textColor = Color.c134_142_150 // Grey
                        label.text = label.text?.capitalized
                        

                    }) { (true) in
                        
                    }
                }
            }
        }else{
            for subview in self.subviews{  //remove error label
                if let lineView = subview as? UIImageView{
                    lineView.frame.size.height = 0
                    lineView.backgroundColor = Color.c217_219_222  // Line Grey
                }
            }
        }
    }
    
    private func addLabll(placeholder:String){
        
        
        for lbl in self.subviews {
            if let label = lbl as? UILabel {
                label.removeFromSuperview()
            }
        }
        // Add placeholder
        var wid = self.frame.width
        let placeLbl = UILabel()
        placeLbl.textColor = Color.c134_142_150 // Grey
        placeLbl.font = UIFont.init(name: Fonts.kFontRegular, size: CGFloat(15))
        let appLanguage = Session.sharedInstance.appLanguage
        placeLbl.text = placeholder.localized(appLanguage as String).capitalized
        
        let contentWidth = placeLbl.intrinsicContentSize.width
        wid = contentWidth < wid ? contentWidth : wid
        placeLbl.frame = CGRect(x: 0, y: 0, width: Int(wid) + 4, height: 18)
        placeLbl.center.y = self.bounds.midY
        placeLbl.backgroundColor = UIColor.white
        
        self.addSubview(placeLbl)
    }
    
    
    private func addLablltwo(placeholder:String) {
        
        
        for lbl in self.subviews {
            if let label = lbl as? UILabel {
                label.removeFromSuperview()
            }
        }
        // Add placeholder
        var wid = self.frame.width
        let placeLbl = UILabel()
        placeLbl.textColor = Color.c134_142_150 // Grey
        placeLbl.font = UIFont.init(name: Fonts.kFontRegular, size: CGFloat(15))
        let appLanguage = Session.sharedInstance.appLanguage
        placeLbl.text = placeholder.localized(appLanguage as String).capitalized
        
        let contentWidth = placeLbl.intrinsicContentSize.width
        wid = contentWidth < wid ? contentWidth : wid
        placeLbl.frame = CGRect(x: 0, y: 0, width: Int(wid) + 4, height: 18)
        placeLbl.center.y = self.bounds.midY
        placeLbl.backgroundColor = UIColor.clear
        
        self.addSubview(placeLbl)
    }
    
    
    
    private func addLine(){
        
        for obj in self.subviews {
            if let line = obj as? UIImageView {
                line.removeFromSuperview()
            }
        }
        
        // Add line
        let x = -13
        let y = Int(self.frame.height)
        let widt = self.frame.width + 26
        let line = UIImageView(frame: CGRect(x: x, y: y, width: Int(widt), height: 0))
        self.addSubview(line)
    }
    func textWidth(text: String, font: UIFont?) -> CGFloat {
        let attributes = font != nil ? [NSAttributedString.Key.font: font] : [:]
        return text.size(withAttributes: attributes as [NSAttributedString.Key : Any]).width
    }
    
    func firstCharacterUppercaseString(string: String) -> String {
        let str = string as NSString
        let firstUppercaseCharacter = str.substring(to: 1).uppercased()
        let firstUppercaseCharacterString = str.replacingCharacters(in: NSMakeRange(0, 1), with: firstUppercaseCharacter)
        return firstUppercaseCharacterString
    }
}//END

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
    func fixedOrientation() -> UIImage {
        
        if imageOrientation == UIImage.Orientation.up {
            return self
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
            break
        case .up, .upMirrored:
            break
        }
        
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        }
        
        let ctx: CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0, space: self.cgImage!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        return UIImage(cgImage: ctx.makeImage()!)
    }
    
    

}

extension String {
    func firstCharacterUpperCase() -> String {
        let lowerCasedString = self.lowercased()
        return lowerCasedString.replacingCharacters(in: lowerCasedString.startIndex...lowerCasedString.startIndex, with: String(lowerCasedString[lowerCasedString.startIndex]).uppercased())
    }
    
       
   
}
