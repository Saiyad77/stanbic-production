//
//  Extension+UIView.swift
//  Stanbic
//
//  Created by Vijay Patidar on 30/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit



extension UIView {
    func setShadowWithCornerRadius(view : UIView, cornerRadius : CGFloat, shadowRadius : CGFloat = 3, shodowOffset : CGSize) {
        
        let shadowLayer = CAShapeLayer()
        
        shadowLayer.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: cornerRadius).cgPath
        
        shadowLayer.shadowColor = Color.black.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = shodowOffset
        shadowLayer.shadowOpacity = 0.3
        shadowLayer.shadowRadius = shadowRadius
        view.layer.insertSublayer(shadowLayer, at: 0)
    }
    
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        
        DispatchQueue.main.async {
            let path = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
            let maskLayer = CAShapeLayer()
            maskLayer.frame = self.bounds
            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
        }
    }
    //Shadow Left And Right
    func leftRightShadow(){
//        self.layer.shadowOpacity = 0.8
//        self.layer.shadowOffset = CGSize(width: 0, height: 3)
//        self.layer.shadowRadius = 4.0

//        let shadowRect: CGRect = self.bounds.insetBy(dx: 0, dy: 4)
//        self.layer.shadowPath = UIBezierPath(rect: shadowRect).cgPath

    }
    func shadow(_ height: Int = 0) {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 1
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 1 , height: height)
    }


}
