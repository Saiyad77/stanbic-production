//
//  Extensions+ViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 17/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController {
    
    func showMessage(title : String, message : String) {
        //FOR LOCALIZATION
        let appLanguage = Session.sharedInstance.appLanguage
        let localTitle = title.localized(appLanguage as String)
        let localMessage = message.localized(appLanguage as String)
        let alert = UIAlertController(title: localTitle, message: localMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK".localized(appLanguage as String), style: .cancel) { action in
            
        })
        present(alert, animated: true)
    }
    
    func showMessageAndBack(title : String, message : String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            self.navigationController?.popViewController(animated: true)
        }))
        present(alert, animated: true)
    }
    
    func addProgress(title: String, description : String) -> ProcessingBar {
        
        let customPopup = ProcessingBar(frame: self.view.frame)
        customPopup.showCustomPopup(title: title, description: description, parent_view: self.view)
        self.view.addSubview(customPopup)
        return customPopup
    }
    
    func imageWithGradient(startColor:UIColor, endColor:UIColor, size:CGSize, horizontally:Bool = true) -> UIImage? {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
        if horizontally {
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        } else {
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        }
        
        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func addShadow(button : UIButton) {
        button.layer.shadowRadius = 7
        button.layer.shadowOpacity = 0.16
        button.layer.shadowOffset = CGSize(width: 5, height: 7)
        button.clipsToBounds = false
    }
    
    func addShadowImage(Image : UIImageView) {
        Image.layer.shadowRadius = 5
        Image.layer.shadowOpacity = 0.1
        Image.layer.shadowOffset = CGSize(width: 5, height: 7)
        Image.clipsToBounds = false
    }
    
    func addShadowview(view : UIView) {
        view.layer.shadowRadius = 7
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: 5, height: 7)
        view.clipsToBounds = false
    }
    
    func addShadow1(button : UIButton) {
    button.layer.shadowColor = UIColor.lightGray.cgColor
    button.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
    button.layer.shadowRadius = 8
    button.layer.shadowOpacity = 0.0
    }
    
    // Mark:- Custom Methods
    // Safaricom,Airtel, Airtelmoney, Telkom, M-Pesa
    func getActiveServiceCode(service : String) -> String {
        
        let appLanguage = Session.sharedInstance.appLanguage
        switch service {
            
        case  ("Mobile_money".localized(appLanguage as String)) :
            return "SM"
            
        case  ("BA".localized(appLanguage as String)) :
            return "BAO"
            
        case  ("PB".localized(appLanguage as String))  :
            return "PB"
            
        case  ("Cash_advance".localized(appLanguage as String))  :
            return "SA"
            
        case  ("STKTOPUP".localized(appLanguage as String))  :
            return "STKTOPUP"
            
        case  ("PESALINKSTATIC".localized(appLanguage as String))  :
            return "PESALINKSTATIC"
            
        case  ("TF".localized(appLanguage as String))  :
            return "TF"
            
        case  ("RTGS".localized(appLanguage as String))  :
            return "RTGS"
            
        case  ("ACCTILL".localized(appLanguage as String))  :
            return "ACCTILL"
            
        case  ("ACCBILL".localized(appLanguage as String))  :
            return "ACCBILL"
            
        case  ("card".localized(appLanguage as String))  :
            return "TFC"
      
        case  ("SENDTOPHONE".localized(appLanguage as String))  :
            return "PAYM"
            
        case  ("SENDTOACCOUNT".localized(appLanguage as String))  :
            return "PAYAC"
            
        case  ("SENDTOCARD".localized(appLanguage as String))  :
            return "PAYC"
            
        case  ("Link your Account".localized(appLanguage as String))  :
            return "KREG"
            
        case  ("Unlink your Account".localized(appLanguage as String))  :
            return "KREG"
            
        case  ("Primary Account".localized(appLanguage as String))  :
            return "KREG"
            
        case  ("Export".localized(appLanguage as String))  :
            return "FSR"
            
        case  ("LOADACCOUNTMPESA".localized(appLanguage as String))  :
            return "STKTOPUP"
            
        default:
            return ""
        }
    }
    
    //Active : 1 -> Active service
    //Active : 2 -> Deactive service clicking on it show inactive message.
    //Active : 3 -> Disbale service Don't show this service
    
    func checkActiveService(service : String) -> [String : String]? {
        
        //Get service code
        let appLanguage = Session.sharedInstance.appLanguage
        let service1 = (service.localized(appLanguage as String))
        let serviceCode = self.getActiveServiceCode(service: service1)
        let activeService = CoreDataHelper().getDataForEntity(entity : Entity.SERVICES_INFO)
        
        if let index = activeService.index(where: { ($0["aSERVICE_CODE"]) == serviceCode}) {
            return activeService[index]
        }
        else {
            //If service code not found return a dictionry with enable service.
            let activeService = ["aACTIVE": "1", "aSERVICE_NAME": "Temp", "aINACTIVE_MESSAGE": "Temp", "aSERVICE_CODE": "Temp"]
            return activeService
        }

    }
    
    func getInActiveMessage(service : String) -> String {
        
        let serviceCode = self.getActiveServiceCode(service: service)
        let activeService = CoreDataHelper().getDataForEntity(entity : Entity.SERVICES_INFO)
        
        if let index = activeService.index(where: { ($0["aSERVICE_CODE"]) == serviceCode}) {
            if activeService[index]["aACTIVE"] == "2" || activeService[index]["aACTIVE"] == "3" { // checking for three is only for send money
                return activeService[index]["aINACTIVE_MESSAGE"] ?? ""
            }
            else {
                return ""
            }
        }
           return ""
    }
    
    func showMessageBack(title : String, message : String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            
            self.navigationController?.popViewController(animated: true)
        }))
        present(alert, animated: true)
    }
    
    // MARK:_ GEt Totel Charges
    func TotelCharges(percent:String,charge:String) -> String {
        
        let chargesper = charge.toDouble()! / 100
        let chargesinto = chargesper * percent.toDouble()!
        let addcharge = chargesinto + charge.toDouble()!
        return "\(addcharge)"
     }
    
    func convertToDictionary1(text: String) -> [String: AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            } catch {
            }
        }
        return nil
    }
    
    func convertToDictionary(text: String) -> [String: String]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: String]
            } catch {
            }
        }
        return nil
    }
    
    // MARK: REMOVE KES IN STRING
    func removeKES(price : String) -> String {
        let amount = price.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range: nil)
        let amount1 = amount.replacingOccurrences(of: ",", with: "", options: NSString.CompareOptions.literal, range: nil)
        //let amount2 = amount1.replacingOccurrences(of: ".", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        let okayChars : Set<Character> =
            Set("1234567890")
        let datastr =  String(amount1.filter {okayChars.contains($0) })
        
        if let index = datastr.range(of: ".")?.lowerBound {
            return (datastr.substring(to: index))
        }
        else {
            return datastr
        }
    }
    
    // MARK: REMOVE Plus + IN STRING
    func removePlus(price : String) -> String {
        let mob = price.replacingOccurrences(of: "+", with: "", options: NSString.CompareOptions.literal, range: nil)
        return mob
    }
    
    
    // Validate Regax
    func getNetworkMobileValidate(mnoName:String,mob:String) -> Bool
    {
        var entityname = Entity(rawValue: "")
        
        if CurrentFlow.flowType == FlowType.SendMoney{
            entityname = Entity.MOBILE_MONEY_INFO
        }
        else {
            entityname =  Entity.MNO_INFO
        }
        
        if (CoreDataHelper().getDataForEntity(entity: entityname!)).count > 0{
            let acdata  = CoreDataHelper().getDataForEntity(entity: entityname!)
            
            for i in 0 ..< acdata.count {
                let dict = acdata[i]
                
                if CurrentFlow.flowType == FlowType.SendMoney{
                    if dict["aMNO_WALLET"]! == mnoName{
                        
                        let emailTest = NSPredicate(format:"SELF MATCHES %@", dict["aREGEX"] ?? "")
                        return emailTest.evaluate(with: mob)
                    }
                }else {
                    if dict["aMNO_NAME"]! == mnoName {
                        
                        let emailTest = NSPredicate(format:"SELF MATCHES %@", dict["aREGEX"]!)
                        return emailTest.evaluate(with: mob)
                    }
                }
            }
            return false
        }
        return false
    }
    
    // CUP Genral Regax :-
    func getgenralRegax(value:String,type:String) ->Bool{
        
        switch type{
            
        case "ac":
            let regax = global.dictRegax["STANBIC_ACCOUNT_NUMBER_REGEX"]?.stringValue
            let acTest = NSPredicate(format:"SELF MATCHES %@", regax!)
            return acTest.evaluate(with: value)
            
        case "card":
            let regax = global.dictRegax["STANBIC_CREDIT_CARD_REGEX"]?.stringValue
            let cardTest = NSPredicate(format:"SELF MATCHES %@", regax!)
            return cardTest.evaluate(with: value)
            
        case "genral":
            let regax = global.dictRegax["GENERAL_INPUT_REGEX"]?.stringValue
            let genralTest = NSPredicate(format:"SELF MATCHES %@", regax!)
            return genralTest.evaluate(with: value)
            
        case "pin":
            let regax = global.dictRegax["PIN_REGEX"]?.stringValue
            let pinTest = NSPredicate(format:"SELF MATCHES %@", regax!)
            return pinTest.evaluate(with: value)
            
        case "dob":
            let regax = global.dictRegax["DOB_REGEX"]?.stringValue
            let dobTest = NSPredicate(format:"SELF MATCHES %@", regax!)
            return dobTest.evaluate(with: value)
            
        case "idno":
            let regax = global.dictRegax["ID_NUMBER_REGEX"]?.stringValue
            let idnoTest = NSPredicate(format:"SELF MATCHES %@", regax!)
            return idnoTest.evaluate(with: value)
            
        case "passport":
            let regax = global.dictRegax["PASSPORT_REGEX"]?.stringValue
            let passportTest = NSPredicate(format:"SELF MATCHES %@", regax!)
            return passportTest.evaluate(with: value)
            
        case "other":
            let regax = global.dictRegax["OTHER_ACCOUNT_NUMBERS_REGEX"]?.stringValue
            let passportTest = NSPredicate(format:"SELF MATCHES %@", regax ?? "")
            return passportTest.evaluate(with: value)
            
        default:
            return false
            
        }
    }
    
    // MY SELF Validate Regax
    func getmySelfRegax(mob:String) -> (String,String)
    {
        
        var entityname = Entity(rawValue: "")
        
        if CurrentFlow.flowType == FlowType.SendMoney{
            entityname = Entity.MOBILE_MONEY_INFO
        }
        else {
            entityname =  Entity.MNO_INFO
        }
        
        if (CoreDataHelper().getDataForEntity(entity: entityname!)).count > 0{
            let acdata  = CoreDataHelper().getDataForEntity(entity: entityname!)
            for i in 0 ..< acdata.count {
                let dict = acdata[i]
                let emailTest = NSPredicate(format:"SELF MATCHES %@", dict["aREGEX"] ?? "")
                if (emailTest.evaluate(with: mob)) == true {
                    if CurrentFlow.flowType == FlowType.SendMoney{
                        return (dict["aMNO_WALLET"]!,dict["aMNO_WALLET_ID"]!)
                        
                    }
                    else {
                        return (dict["aMNO_NAME"]!,dict["aMNO_WALLET_ID"]!)
                    }
                }
            }
        }
        return ("SELECTNETWORKPROVIDER".localized(Session.sharedInstance.appLanguage),"")
    }
    
    func hideKeyboardOnTap(_ selector: Selector) {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: selector)
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    //  GET COST  ( only o index data)
    func getChargebyCost(service_code : String,acno:String) -> String {
        
        let array = CoreDataHelper().getDataForEntity(entity: Entity.SERVICES_INFO)
        for i in 0 ..< array.count {
            
            if array[i]["aSERVICE_CODE"] == service_code {
               
               // let dict = array[i]["aCHARGES"]!
               // let data = convertToDictionary1(text:dict)
                
                let dict = convertToDictionary(text: array[i]["aCHARGE"]!)
                let keys = Array(dict!.keys)
                    if keys.count > 0 {
                        return dict?[keys[0]] ?? "0"
                    }
                    else {
                     return "0"
                    }
                
            
               }
        }
        return "0"
    }
    
    
    //  GET PRICE RANGE
    func getAmountValdiation(service_code : String,acno:String) -> (Double,Double) {
        
        let array = CoreDataHelper().getDataForEntity(entity: Entity.SERVICES_INFO)
        var arr1 = [String]()
        var arr2 = [String]()
        for i in 0 ..< array.count {
            if array[i]["aSERVICE_CODE"] == service_code {
                let dict = array[i]["aCHARGES"]!
                let data = convertToDictionary1(text:dict)
                if let dictcharges = data?[acno] as? [String:String]{
                    let keys = Array(dictcharges.keys)
                    
                    for i in 0 ..< keys.count {
                        let array = keys[i]
                        if array.contains(":"){
                            var arr = array.components(separatedBy: ":")
                            arr1.append(arr[0])
                            arr2.append(arr[1])
                            
                        } else {
                            return (0,0)
                        }
                    }
                    if arr1.count > 0 {
                        
                        
                        let newarr1 = arr1.compactMap(Double.init)
                        let newarr2 = arr2.compactMap(Double.init)
                        
                        let min = Double(newarr1.min()!)
                        let max = Double(newarr2.max()!)
                        return (min,max)
                    }
                    
                }
                // Account no are not contain in Dict
                return (0,0)
            }
            
            
        }
        return (0,0)
    }
       //  GET PRICE RANGE
    func getAmountValdiation1(service_code : String) -> (Double,Double) {
        
        let array = CoreDataHelper().getDataForEntity(entity: Entity.SERVICES_INFO)
        var arr1 = [String]()
        var arr2 = [String]()
        for i in 0 ..< array.count {
            if array[i]["aSERVICE_CODE"] == service_code {
                if let dict = convertToDictionary(text: array[i]["aCHARGE"]!){
                    let keys = Array(dict.keys)
                    
                    for i in 0 ..< keys.count {
                        let array = keys[i]
                        if array.contains(":"){
                            var arr = array.components(separatedBy: ":")
                            arr1.append(arr[0])
                            arr2.append(arr[1])
                            
                        } else {
                            return (0,0)
                        }
                    }
                    if arr1.count > 0 {
                        
                        let newarr1 = arr1.compactMap(Double.init)
                        let newarr2 = arr2.compactMap(Double.init)
                        
                        let min = Double(newarr1.min()!)
                        let max = Double(newarr2.max()!)
                        return (min,max)
                    }
                }
            }
        }
        return (0,0)
    }
    
    
    // MARK:_ Credit CARD VALIDATIONS
    func getCreditcardAmountValdiation() -> (Double,Double) {
        var arr1 = [String]()
        var arr2 = [String]()
        for i in 0 ..< global.cardInfo.count {
            let dict1 = global.cardInfo[i]
            
            if global.creditCardID == dict1["ID"] as! String {
                let newdict = dict1["Charge"] as! NSDictionary
                
                let keys = Array(newdict.allKeys)
                
                for i in 0 ..< keys.count {
                    let array = keys[i]
                    if (array as AnyObject).contains(":"){
                        var arr = (array as AnyObject).components(separatedBy: ":")
                        arr1.append(arr[0])
                        arr2.append(arr[1])
                        
                    } else {
                        return (0,0)
                    }
                }
                if arr1.count > 0 {
                    let newarr1 = arr1.compactMap(Double.init)
                    let newarr2 = arr2.compactMap(Double.init)
                    
                    let min = Double(newarr1.min()!)
                    let max = Double(newarr2.max()!)
                    return (min,max)
                }
                
            }
        }
        return (0,0)
        
    }
    
    func checkAmount(min:Double,max:Double,amount:String)->Bool{
        if max != 0 {
            if (amount as NSString).doubleValue >= min && (amount as NSString).doubleValue <= max {
                return true
            }
            else{
                return false
            }
        }
        else {
            return false
        }
    }
    
//
//    func checkAmount(min:Int,max:Int,amount:String)->Bool{
//        if max != 0 {
//            if (amount as NSString).integerValue >= min && (amount as NSString).integerValue <= max {
//                return true
//            }
//            else{
//                return false
//            }
//        }
//        else {
//            return false
//        }
//    }
}

