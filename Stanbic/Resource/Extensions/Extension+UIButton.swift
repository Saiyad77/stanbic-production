//
//  Extension+UIButton.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 29/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit

extension UIButton{
    
    //Rounded button with border
    func roundedButtonBorder() {
        self.backgroundColor = .clear
        self.layer.cornerRadius = 25
        self.layer.borderWidth = 1
    }
    
    //Rounded button without border
    func roundedButton() {
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
    
}

