//
//  Extension+String.swift
//  Stanbic
//
//  Created by Vijay Patidar on 23/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
extension String {
    
    var pairs: [String] {
        var result: [String] = []
        let characters = Array(self)
        stride(from: 0, to: count, by: 2).forEach {
            result.append(String(characters[$0..<min($0+2, count)]))
        }
        return result
    }
    mutating func insert(separator: String, every n: Int) {
        self = inserting(separator: separator, every: n)
    }
    func inserting(separator: String, every n: Int) -> String {
        var result: String = ""
        let characters = Array(self)
        stride(from: 0, to: count, by: n).forEach {
            result += String(characters[$0..<min($0+n, count)])
            if $0+n < count {
                result += separator
            }
        }
        return result
    }
    
    func hashingOfTheAccount(noStr : String) -> String {
        
        var arrC = [String]()
        for character in noStr {
            arrC.append("\(character)")
        }
        
        var hashStr = ""
        for i in 0 ..< arrC.count {
            
            if i <= 3 {
                hashStr = hashStr + "\(arrC[i])"
            }
            else {
                hashStr = hashStr + "X"
            }
        }
        return hashStr.inserting(separator: " ", every: 4)
    }
    
    
    func reverseHashingOfTheAccount(noStr : String) -> String {
        
        var arrC = [String]()
        for character in noStr {
            arrC.append("\(character)")
        }
        
        var hashStr = ""
        for i in 0 ..< arrC.count {
            
            if i <= (arrC.count - 5) {
                hashStr = hashStr + "X"
            }
            else {
                hashStr = hashStr + "\(arrC[i])"
            }
        }
        return hashStr
    }
    
}
