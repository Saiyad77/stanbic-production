//
//  Authntication.swift
//  Stanbic
//
//  Created by Vijay Patidar on 16/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import  UIKit
import LocalAuthentication


class  Authntication : NSObject {

    var context : LAContext?
    var keychain:KeychainPasswordItem!
    
    init(_ context:LAContext)
    {
        self.context = context
        context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
    }
    override init()
    {
       super.init()
    }

    func checkUserDeviceFaceIDSupport() -> Bool
    {
        if #available(iOS 11.0, *)
        {
            if(context?.biometryType == .faceID)
            {
               return true
            }
            else {
                return false
            }
        }
        else {
            return false
        }
       
    }
    
    func authnticateUser(completion: ((_ success: Bool, _ error: String) -> Void)!) {
        
        //context?.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil)
        //context = LAContext()
        
        //context.localizedCancelTitle = "Enter passcode"
        
        context?.localizedFallbackTitle = ""
        
        // First check if we have the needed hardware support.
        var error: NSError?
        if (context?.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error))! {
            
            let reason = "Unlock to proceed"
            context?.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason ) { success, error in
                
                if success {
                    
                    if( completion != nil ) {
                        Session.sharedInstance.isAuthnicated = true
                        completion?(true, "Sucess")
                    }
                    
                } else {
                    if( completion != nil ) {
                        Session.sharedInstance.isAuthnicated = false
                        completion?(false, self.displayErrorMessage(error: error as! LAError))
                    }
                }
            }
        } else {
            if( completion != nil ) {
                Session.sharedInstance.isAuthnicated = false
                completion?(false, self.displayErrorMessage(error: error as! LAError))
            }
            
        }
    }
    
    func displayErrorMessage(error:LAError) -> String {
        switch error.code {
        case LAError.authenticationFailed:
            return ""
        case LAError.authenticationFailed:
            return ""
        case LAError.userCancel:
            return ""
        case LAError.userFallback:
            return ""
        case LAError.touchIDNotEnrolled:
           return ""
        case LAError.passcodeNotSet:
            return ""
        case LAError.systemCancel:
            return ""
        default:
            return "You have exceeded maximum number of unsuccessful attempts. Please lock your device once to re-enable biometric ID and access this feature."
        }
    }
    
    func saveUserCredientiacial(username : String, password : String, isAuth : Bool) -> Bool {
        
        //guard account.isEmpty, password.isEmpty else { return }
        
        let maniUsername = self.convertNalue(text: username)
        UserDefaults.standard.set(maniUsername, forKey: KeychainConfiguration.kLastAccessedUsername)
        let passwordItem = KeychainPasswordItem(service: KeychainConfiguration.kLastAccessedPassword, account: maniUsername, accessGroup: KeychainConfiguration.accessGroup)
        do {
            
            try passwordItem.saveValue(password)
    
            if isAuth == true {
                UserDefaults.standard.set(true, forKey: KeychainConfiguration.kIsAuthEnable)
            }
            
            return true
            
        } catch {
            return false
        }
    }
    
    func loadPasswordFromKeychainAndAuthenticateUser(_ account: String) -> String
    {
        
        guard !account.isEmpty else { return ""}
        let passwordItem = KeychainPasswordItem(service:   KeychainConfiguration.kLastAccessedPassword, account: account, accessGroup: KeychainConfiguration.accessGroup)
        do {
            let storedPassword = try passwordItem.readPassword()
            return storedPassword
        } catch KeychainPasswordItem.KeychainError.noPassword {
            
        } catch {
            
        }
        return ""
    }
    
    func loadPasswordFromKeychainAndAuthenticateUser2(_ account: String, service : String) -> String
    {
        
        guard !account.isEmpty else { return ""}
        let passwordItem = KeychainPasswordItem(service: service, account: account, accessGroup: KeychainConfiguration.accessGroup)
        do {
            let storedPassword = try passwordItem.readPassword()
            return storedPassword
        } catch KeychainPasswordItem.KeychainError.noPassword {
            
        } catch {
            
        }
        return ""
    }
    
    func removePassword(_ account: String) -> Bool {
        
        guard !account.isEmpty else { return false}
        let passwordItem = KeychainPasswordItem(service:   KeychainConfiguration.kLastAccessedPassword, account: account, accessGroup: KeychainConfiguration.accessGroup)
        do {
            try passwordItem.deleteItem()

        } catch KeychainPasswordItem.KeychainError.noPassword {
            return false
        } catch {
            return false
        }
        return true
    }
    
    func convertNalue(text : String) -> String {
        
        let input:String = text
        let cipher:String = CryptoHelper.encrypt(input:input)!;
        //cipher = "kb1TyFRUcMaY6Z1vCRravA==";
        //let output:String = CryptoHelper.decrypt(input:cipher)!;
        return cipher
    }
}


struct KeychainConfiguration {
    
    static let kLastAccessedUsername  = "lastAccessedUsername"
    static let kLastAccessedPassword  = "lastAccessedPassword"
    static let kIsAuthEnable          = "isAuthEnable"
    static let accessGroup: String? = nil
    static let encptKey = "ENCPTIONSKEY"
    static let encptAccount = "ENCPTIONSACCOUNT"
    static let encptValue = "ENCPTIONSACCOUNT"
    static let notificationID = "applicationDidBecomeActive"
    static let miniStatement = "MINISTATEMENT"
}
