//
//  Font.swift
//  Stanbic
//
//  Created by Vijay Patidar on 16/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import  UIKit

class Fonts {
    static let kFontRegular =  "BentonSans-Regular"
    static let kFontMedium = "BentonSans-Medium"
    static let kFontBold =  "BentonSans-Bold"
    static let kFontBook = "BentonSans-Book"
    
    func set(object : NSObject, fontType : Int, fontSize : Int, color : UIColor, title : String,placeHolder:String) {
        
        //FOR LOCALIZATION
        let appLanguage = Session.sharedInstance.appLanguage
        let localTitle = title.localized(appLanguage as String)
        let placeholder = placeHolder.localized(appLanguage as String)
        
        switch object
        {
        case (object as UILabel) :
            (object as! UILabel).font = UIFont.init(name: getFontString(type: fontType), size: CGFloat(fontSize))
            (object as! UILabel).textColor = color
            (object as! UILabel).text = localTitle
            break
            
        case (object as UIButton) :
            (object as! UIButton).titleLabel?.font = UIFont.init(name: getFontString(type: fontType), size: CGFloat(fontSize))
            (object as! UIButton).setTitleColor(color, for: .normal)
            (object as! UIButton).setTitle(localTitle, for: .normal)
            break
    
        case (object as UITextField) :
            (object as! UITextField).font = UIFont.init(name: getFontString(type: fontType), size: CGFloat(fontSize))
            (object as! UITextField).textColor = color
            (object as! UITextField).placeholder = placeholder
            break
        default:
            break
        }
        
    }
    
    
    func getFontString(type : Int) -> String
    {
        
        switch type {
            
        case 0:
            return Fonts.kFontRegular
            
        case 1:
             return Fonts.kFontMedium
            
        case 2:
            return Fonts.kFontBold
            
        case 3:
            return Fonts.kFontBook
            
        default:
            return Fonts.kFontRegular
        }
        
 }
}


extension String
{
    
    func localized(_ lang:String) -> String
    {
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        
    }
    
}
