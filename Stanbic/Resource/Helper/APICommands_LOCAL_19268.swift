//
//  APICommands.swift
//  Stanbic
//
//  Created by Vijay Patidar on 17/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
// Color Constant
class sCommand {
    
    static let oRGID = AppMode.appMode?.getOriginID() ?? 13
    static let oRIGIN       =  "5EXCEPTIONS"
    static let GAK          = "PBXUDGP"   //Get validation Key
    static let VAK          = "Z0U7021"   //Validate Activation Key
    static let VPIN         = "SIU6GM9"   //Validate Pin Request
    static let SR           = "YXZI4EU"  //Self Registration
    static let AO           = "9XTY5RE"  //Account Opening // Validate id, validate personal details
    static let BAO           = "R64ZN7G"  //Buy Airtime
    static let MSR           = "689L9GE"  //Mini Statement
    static let CUP           = "5Z3Q32L"  //Check Updates/Fetch System Settings
    static let BE           = "OTFUFMD"  //Balance Enquiry
    static let FB           = "IGHIK41"  //Fetch Bills
}
