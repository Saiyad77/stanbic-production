//
//  AmountFormatter.swift
//  Stanbic
//
//  Created by Vijay Patidar on 22/03/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit


class AmountField: UITextField {
    
    private var isFirstDecimal : Bool = true
    override func willMove(toSuperview newSuperview: UIView?) {
        
        addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        keyboardType = .decimalPad
        textAlignment = .left
        placeholder = "0.0"
        //editingChanged()
    }
    override func deleteBackward() {
        var currentText = self.text ?? ""
        currentText = String(currentText.dropLast())
        self.text = currentText
        editingChanged(self)
    }
    
    @objc func editingChanged(_ textField: UITextField? = nil) {
        var doubleStr = textField?.text ?? "00"
        var currencyStr = ""
        if doubleStr.contains("KES") || doubleStr.contains("KSH") || doubleStr.contains("-") {
            let sepratorStr = doubleStr.components(separatedBy: " ")
            currencyStr = sepratorStr[0]
        }
        
        let validCharacters = CharacterSet(charactersIn: "0123456789,.")
        doubleStr = doubleStr.components(separatedBy: validCharacters.inverted).joined()
        
        let decimalCount = doubleStr.components(separatedBy: ".")
        if decimalCount.count > 2 {
            //var currentText = self.text ?? ""
            var currentText = doubleStr
            currentText = String(currentText.dropLast())
            if currencyStr != "" {
                 currentText = "\(currencyStr) \(currentText)"
            }
            self.text = currentText
            return
        }
        
        if doubleStr.contains(".") && isFirstDecimal == true {
            if currencyStr != "" {
                self.text = "\(currencyStr) \(doubleStr)"
            }
            else {
                self.text = doubleStr
            }
            
            isFirstDecimal = false
            return
        }
        else if !(doubleStr.contains(".")) {
            isFirstDecimal = true
        }
        
        let doubleStrTemp = doubleStr.replacingOccurrences(of: ",", with: "")
        
        if doubleStrTemp != "" {
            if let n = Decimal(string: doubleStrTemp )?.significantFractionalDecimalDigits {
                if n > 2 {
                    //var currentText = self.text ?? ""
                    var currentText = doubleStr
                    currentText = String(currentText.dropLast())
                    if currencyStr != "" {
                        self.text = "\(currencyStr) \(currentText)"
                    }
                    else {
                        self.text = currentText
                    }
                    
                    
                    return
                }
            }
        }
        doubleStr = doubleStr.replacingOccurrences(of: ",", with: "")
        
        let doube = Double(doubleStr)
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        if doube != nil {
            let formattedNumber = numberFormatter.string(from: NSNumber(value:doube!))
            if currencyStr != "" {
                self.text = "\(currencyStr) \(formattedNumber ?? "0.0")"
            }
            else {
                self.text = formattedNumber
            }
         }
    }
}

extension Decimal {
    var significantFractionalDecimalDigits: Int {
        return max(-exponent, 0)
    }
}
