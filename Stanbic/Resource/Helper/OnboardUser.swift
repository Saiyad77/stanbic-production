//
//  OnboardUser.swift
//  Stanbic
//
//  Created by Vijay Patidar on 16/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit

struct OnboardUser {
    static var mobileNo = ""
    static var pin = ""
    static var sectrust : SecTrust?
    
    static var userEmail = ""
    static var userKraPIN = ""
    static var userplaceresidence = ""
    static var userpostalcode = ""
    static var userPostaladdress = ""
    static var userbank = ""
    static var userbankID = ""
    
    static var occupationalData = [[String : Any]]()
    static var kinData = [[String : Any]]()
    static var beArray = [JSON]()
    static var currency = "KSH"
    static var branchData =  [[String : String]]()
    
    static var empStatusData =  [[String : AnyObject]]()
    
}



