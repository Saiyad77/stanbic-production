//
//  AppFlows.swift
//  Stanbic
//
//  Created by Vijay Patidar on 22/03/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit

enum FlowType : Int {
    
    case SendMoney = 0, BuyAirtime, PayBill ,InternalFundTransfer,Rtgs,PesaLinkToAc,PesaLinkToCard,PesaLinkToPhone,MpesaToAccount,LinkAccount,ChequeBookRequest,PayBillTransaction,BuyGoods,ForexRate
    
    func title() -> String {
        switch self {
        case .SendMoney:
            return "Send money"
            
        case .BuyAirtime:
            return "Buy airtime"
            
        case .PayBill :
            return "Pay Bill"
            
        case .InternalFundTransfer :
            return "Internal func Transfer"
            
        case .Rtgs :
            return "Rtgs"
            
        case .PesaLinkToAc :
            return "PesaLinkToAc"
            
        case .PesaLinkToCard :
            return "PesaLinkToCard"
            
        case .PesaLinkToPhone :
            return "PesaLinkToPhone"
            
        case .MpesaToAccount :
            return "MpesaToAccount"
            
        case .LinkAccount :
            return "Link account"
            
        case .ChequeBookRequest :
            return "Cheque book request"
            
        case .PayBillTransaction:
            return "Pay bill"
            
        case .BuyGoods:
            return "Buy goods"
            
        case .ForexRate:
            return "Buy goods"
        
    }
        
    }
    
    func icon() -> UIImage {
        switch self {
            
        case .SendMoney :
            return UIImage.init(named: "XpressCash")!
            
        case .BuyAirtime:
            return UIImage.init(named: "Buyairtime")!
            
        case .PayBill :
            return UIImage.init(named: "Paybill")!
            
        case .InternalFundTransfer:
            return UIImage.init(named: "Paybill")!
            
        case .Rtgs:
            return UIImage.init(named: "Paybill")!
            
        case .PesaLinkToAc:
            return UIImage.init(named: "PesaLinkToAc")!
            
        case .PesaLinkToCard:
            return UIImage.init(named: "PesaLinkToCard")!
            
        case .PesaLinkToPhone:
            return UIImage.init(named: "PesaLinkToPhone")!
            
        case .MpesaToAccount:
            return UIImage.init(named: "PesaLinkToPhone")!
            
        case .LinkAccount:
            return UIImage.init(named: "Register")!
            
        case .ChequeBookRequest:
            return UIImage.init(named: "LinkAccount")!
            
        case .PayBillTransaction:
            return UIImage.init(named: "")!
        case .BuyGoods:
            return UIImage.init(named: "")!
        case .ForexRate:
             return UIImage.init(named: "")!
        }
    }
    
    func getType() -> String {
        switch self {
        case .SendMoney :
            return "-5"
            
        case .BuyAirtime:
            return "7"
            
        case .PayBill :
            return "5"
            
        case .InternalFundTransfer:
            return "2"
            
        case .Rtgs:
            return "1"
            
        case .PesaLinkToAc:
            return "15"
            
        case .PesaLinkToCard:
            return "16"
            
        case .PesaLinkToPhone:
            return "17"
            
        case .MpesaToAccount:
        return "0"
            
        case .LinkAccount:
              return "0"
            
        case .ChequeBookRequest:
        return "0"
            
        case .PayBillTransaction:
            return "6"
            
        case .BuyGoods:
            return "4"
            
        case .ForexRate:
            return "0"
        }
    }
    
 
    
    func getContactListHeader() -> String {
        switch self {
            
        case .SendMoney :
            return "SENDMONEYTITLE"
            
        case .BuyAirtime:
            return "WHOWOULDYOULIKE"
            
        case .PayBill :
            return ""
            
        case .InternalFundTransfer :
            return "WHOWOULDYOULIKETOSENDMONEYTO"
            
        case .Rtgs :
            return "WHOWOULDYOULIKETOSENDMONEYTO"
            
        case .PesaLinkToAc :
            return "WHOWOULDYOULIKETOSENDMONEYTO"
            
        case .PesaLinkToCard :
            return "WHOWOULDYOULIKETOSENDMONEYTO"
            
        case .PesaLinkToPhone :
            return "WHOWOULDYOULIKETOSENDMONEYTO"
            
        case .MpesaToAccount :
            return ""
            
        case .LinkAccount:
            return ""
            
        case .ChequeBookRequest:
            return ""
        case .PayBillTransaction:
            return ""
        case .BuyGoods:
            return ""
        case .ForexRate:
            return ""
        }
    }
    
    func getReviewData() -> (String, String, String) { // (Header, Trnsaction title, button)
        switch self {
            
        case .SendMoney :
            return ("CONFIRMANDTRANFER", "TRANSFERMONEYTO", "CONTINUE")
            
        case .BuyAirtime:
            return ("Confirmandbuy", "BUYAIRTIMEFOR", "BUYAIRTIME")
            
        case .PayBill :
            return ("CONFIRMANDPAY", "TRANSFERMONEYTO", "PAYBILLCAP")
            
        case .InternalFundTransfer :
           return ("CONFIRMANDTRANFER", "TRANSFERMONEYTO", "CONTINUE")
            
        case .Rtgs :
            return ("CONFIRMANDTRANFER", "TRANSFERMONEYTO", "CONTINUE")
            
        case .PesaLinkToAc :
            return ("CONFIRMANDTRANFER", "TRANSFERMONEYTO", "CONTINUE")
            
        case .PesaLinkToCard :
           return ("CONFIRMANDTRANFER", "TRANSFERMONEYTO", "CONTINUE")
            
        case .PesaLinkToPhone :
          return ("CONFIRMANDTRANFER", "TRANSFERMONEYTO", "CONTINUE")
            
        case .MpesaToAccount :
            return ("CONFIRMANDTRANFER", "TRANSFERMONEYFROM", "CONTINUE")
            
        case .LinkAccount:
           return ("CONFIRMANDTRANFER", "TRANSFERFROM", "CONTINUE")
            
        case .ChequeBookRequest:
            return ("CONFIRMANDREQUEST", "REQUESTCHEQUEBOOKFOR", "CONTINUE")
            
        case .PayBillTransaction:
            return ("Confirmandpay","MAKEPAYMENTTO","PAYBILLCAP")
        case .BuyGoods:
            return ("Confirmandpay","MAKEPAYMENTTO","PAYBILLCAP")
        case .ForexRate:
            return ("","","")
        }
    }
    
    func pesaLinkData() -> (String, String, String) { // (Header, Trnsaction title, button)
        switch self {
            
        case .SendMoney :
            return ("", "", "")
            
        case .BuyAirtime:
             return ("", "", "")
            
        case .PayBill :
             return ("", "", "")
            
        case .InternalFundTransfer :
            return ("", "", "")
            
        case .Rtgs :
             return ("BANKTOBANK", "HOWMUCHWOULDYOULIKETOTRANSFER", "CONTINUE")
            
        case .PesaLinkToAc :
            return ("PESALINKTOACCOUNT", "HOWMUCHWOULDYOULIKETOTRANSFER", "CONTINUE")
            
        case .PesaLinkToCard :
            return ("PESALINKTOCARD", "HOWMUCHWOULDYOULIKETOTRANSFER", "CONTINUE")
            
        case .PesaLinkToPhone :
            return ("PESALINKTOPHONE", "HOWMUCHWOULDYOULIKETOTSEND", "CONTINUE")
            
        case .MpesaToAccount :
            return ("", "", "")
            
        case .LinkAccount :
            return ("", "", "")
            
        case .ChequeBookRequest :
            return ("", "", "")
            
        case .PayBillTransaction:
            return ("","","")
        case .BuyGoods:
            return ("","","")
        case .ForexRate:
             return ("","","")
        }
    }
}

struct CurrentFlow {
    
    static var flowType : FlowType?
}
