

//
//  Validation.swift
//  Stanbic
//
//  Created by Vijay Patidar on 19/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation

func isValidEmail(emailStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: emailStr)
}
