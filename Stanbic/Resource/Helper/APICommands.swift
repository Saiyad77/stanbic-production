//
//  APICommands.swift
//  Stanbic
//
//  Created by Vijay Patidar on 17/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation

// Color Constant

class sCommand {
    
    static let oRGID = AppMode.appMode?.getOriginID() ?? 13
    static let oRIGIN       = "5EXCEPTIONS"
    static let GAK          = "PBXUDGP"     // Get validation Key
    static let VAK          = "Z0U7021"     // Validate Activation Key
    static let VPIN         = "SIU6GM9"     // Validate Pin Request
    static let SR           = "YXZI4EU"     // Self Registration
    static let AO           = "9XTY5RE"     // Account Opening // Validate id, validate personal details
    static let BAO          = "R64ZN7G"     // Buy Airtime
    static let MSR          = "689L9GE"     // Mini Statement
    static let CUP          = "5Z3Q32L"     // Check Updates/Fetch System Settings
    static let BE           = "OTFUFMD"     // Balance Enquiry
    static let FB           = "IGHIK41"     // Fetch Bills
    static let SM           = "8C0G7J8"     // Send Money To Mobile
    static let QBILL        = "FR4GX9T"     // Query Bill
    static let PB           = "KYTJT5F"     // Pay Bill
    static let TF           = "DSZFQUU"     // FUND  Transfer
    static let RTGS         = "4MK2LKD"     // RTGS
    static let TFC          = "VN7CW1R"     // Card Transfers
    static let CSR          = "9OTOU88"     // Pesalink - Check Registration
    static let FKITSB       = "ET5353GE"    // Pesalink - Phone lookup
    static let PAYM         = "L172R9Z"     // Pesalink - Send to Phone
    static let PAYAC        = "4YLDLII"     // Pesalink - Send to Account
    static let PAYC         = "RUWQC5H"     // Pesalink - Send to Card
    static let KREG         = "RNLYW14"     // Pesalink - Link/Unlink/Primary
    static let PMQUERY      = "FR8HJOP"     // Pesalink - Paybill Validate Reference
    static let RCB          = "3869V69"     // Cheque Book Request
    static let SC           = "KXEMBDH"     // Stop Cheque
    static let CST          = "3FQ9ROE"     // Cheque Status
    static let BAL          = "SVZDQ0O"     // Branch ATM Locator
    static let CP           = "UX7UF7I"     // Change Pin
    static let COTP         = "JHSWF2Y"     // One Time Pin
    static let FSR          = "7AKFXH0"     // Full Statement
    static let STKTOPUP     = "GQU2W3A"     // Mpesa STK Push
    static let FST          = "YR1D7VT"     // Fetch RTGS Time
    static let FR           = "W9S0ASO"     // Forex Rates
    static let SA           = "8U4F8OZ"     // Salary Advance
    static let LCT          = "SJPEKMOH"    // Loan Calculator
    static let MBEF         = "DLZ1MYO"     // Beneficiary Management
    static let ACCBILL      = "QXQQ3MX"     // Safaricom Mpesa Paybill
    static let ACCTILL      = "CQJ26ST"     // Mpesa Business Buy Goods and Services
    static let REG          = "J5T93YM"     // Check Registration Status
    static let LEQ          = "JTZ76YB"     // Loan Enquiries
    static let FTC          = "ED626MD"     // Fetch terms & conditions and privacy policy
}
