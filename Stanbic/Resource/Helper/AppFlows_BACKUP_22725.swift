//
//  AppFlows.swift
//  Stanbic
//
//  Created by Vijay Patidar on 22/03/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit

enum FlowType : Int {
    
    case SendMoney = 0, BuyAirtime, PayBill ,InternalFundTransfer,Rtgs,PesaLinkToAc,PesaLinkToCard,PesaLinkToPhone,MpesaToAccount
    
    func title() -> String {
        switch self {
        case .SendMoney:
            return "Send money"
            
        case .BuyAirtime:
            return "Buy airtime"
            
        case .PayBill :
            return "Pay Bill"
            
        case .InternalFundTransfer :
            return "Internal func Transfer"
            
        case .Rtgs :
            return "Rtgs"
            
        case .PesaLinkToAc :
            return "PesaLinkToAc"
            
        case .PesaLinkToCard :
            return "PesaLinkToCard"
            
        case .PesaLinkToPhone :
            return "PesaLinkToPhone"
            
        case .MpesaToAccount :
            return "MpesaToAccount"
            
        }
    }
    
    func icon() -> UIImage {
        switch self {
            
        case .SendMoney :
            return UIImage.init(named: "XpressCash")!
            
        case .BuyAirtime:
            return UIImage.init(named: "Buyairtime")!
            
        case .PayBill :
            return UIImage.init(named: "Paybill")!
            
        case .InternalFundTransfer:
            return UIImage.init(named: "Paybill")!
            
        case .Rtgs:
            return UIImage.init(named: "Paybill")!
            
        case .PesaLinkToAc:
            return UIImage.init(named: "PesaLinkToAc")!
            
        case .PesaLinkToCard:
            return UIImage.init(named: "PesaLinkToCard")!
            
        case .PesaLinkToPhone:
            return UIImage.init(named: "PesaLinkToPhone")!
            
        case .MpesaToAccount:
            return UIImage.init(named: "PesaLinkToPhone")!
            
        }
    }
    
    
    func getType() -> String {
        switch self {
        case .SendMoney :
            return "5"
            
        case .BuyAirtime:
            return "7"
            
        case .PayBill :
            return "5"
            
        case .InternalFundTransfer:
            return "2"
            
        case .Rtgs:
            return "1"
            
        case .PesaLinkToAc:
            return "12"
            
        case .PesaLinkToCard:
            return "13"
            
        case .PesaLinkToPhone:
            return "11"
            
        case .MpesaToAccount:
        return "0"
        }
    }
    
    func getContactListHeader() -> String {
        switch self {
            
        case .SendMoney :
            return "SENDMONEYTITLE"
            
        case .BuyAirtime:
            return "WHOWOULDYOULIKE"
            
        case .PayBill :
            return ""
            
        case .InternalFundTransfer :
            return ""
            
        case .Rtgs :
            return ""
            
        case .PesaLinkToAc :
            return ""
            
        case .PesaLinkToCard :
            return ""
            
        case .PesaLinkToPhone :
            return ""
            
        case .MpesaToAccount :
            return ""
        }
    }
    
    func getReviewData() -> (String, String, String) { // (Header, Trnsaction title, button)
        switch self {
            
        case .SendMoney :
            return ("CONFIRMANDTRANFER", "TRANSFERMONEYTO", "CONTINUE")
            
        case .BuyAirtime:
            return ("Confirmandbuy", "BUYAIRTIMEFOR", "BUYAIRTIME")
            
        case .PayBill :
            return ("CONFIRMANDPAY", "MAKEPAYMENTTO", "PAYBILLCAP")
            
<<<<<<< HEAD
        case .InternalFundTransfer, .Rtgs :
            return ("CONFIRMANDTRANFER", "TRANSFERMONEYTO", "CONTINUE")
=======
        case .InternalFundTransfer :
           return ("CONFIRMANDPAY", "MAKEPAYMENTTO", "PAYBILLCAP")
            
        case .Rtgs :
            return ("CONFIRMANDPAY", "MAKEPAYMENTTO", "PAYBILLCAP")
>>>>>>> 353cd217832b374ace3dba6f87351ccc96119769
            
        case .PesaLinkToAc :
            return ("CONFIRMANDTRANFER", "MAKEPAYMENTTO", "CONTINUE")
            
        case .PesaLinkToCard :
           return ("CONFIRMANDTRANFER", "MAKEPAYMENTTO", "CONTINUE")
            
        case .PesaLinkToPhone :
          return ("CONFIRMANDTRANFER", "MAKEPAYMENTTO", "CONTINUE")
            
        case .MpesaToAccount :
            return ("CONFIRMANDTRANFER", "TRANSFERFROM", "CONTINUE")
            
        }
    }
    
    func pesaLinkData() -> (String, String, String) { // (Header, Trnsaction title, button)
        switch self {
            
        case .SendMoney :
            return ("", "", "")
            
        case .BuyAirtime:
             return ("", "", "")
            
        case .PayBill :
             return ("", "", "")
            
        case .InternalFundTransfer :
            return ("", "", "")
            
        case .Rtgs :
             return ("", "", "")
            
        case .PesaLinkToAc :
            return ("PESALINKTOACCOUNT", "HOWMUCHWOULDYOULIKETOTRANSFER", "CONTINUE")
            
        case .PesaLinkToCard :
            return ("PESALINKTOCARD", "HOWMUCHWOULDYOULIKETOTRANSFER", "CONTINUE")
            
        case .PesaLinkToPhone :
            return ("PESALINKTOPHONE", "HOWMUCHWOULDYOULIKETOTSEND", "CONTINUE")
            
        case .MpesaToAccount :
            return ("", "", "")
            
        }
    }
}

struct CurrentFlow {
    
    static var flowType : FlowType?
}
