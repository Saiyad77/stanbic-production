//
//  Variables.swift
//  Stanbic
//
//  Created by Vijay Patidar on 16/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation

class Variables {
    // MARK: - Variables

    private var slt: String
    
    // MARK: - Initialization
    
    init() {
        self.slt = "\(String(describing: AppDelegate.self))\(String(describing: NSObject.self))"
    }
    
    init(with salt: String) {
        self.slt = salt
    }
    
    func reveal(key: [UInt8]) -> String {
        let sifer = [UInt8](self.slt.utf8)
        let length = sifer.count
        
        var dcpt = [UInt8]()
        
        for k in key.enumerated() {
            dcpt.append(k.element ^ sifer[k.offset % length])
        }
        
        return String(bytes: dcpt, encoding: .utf8)!
    }
    
    func bytesByObfuscatingString(string: String) -> [UInt8] {
        let text = [UInt8](string.utf8)
        let cipher = [UInt8](self.slt.utf8)
        let length = cipher.count
        
        var encrypted = [UInt8]()
        
        for t in text.enumerated() {
            encrypted.append(t.element ^ cipher[t.offset % length])
        }
        
        return encrypted
    }
}

enum VariableConstants {
    static let VarialbeString: [UInt8] = [52, 27, 24, 116, 7, 6, 1, 80, 2, 7, 31, 38, 42, 127, 12, 11]
    static let VarialbeString1: [UInt8] = [24, 63, 37, 5, 55, 41, 34, 34, 47, 49, 44, 1, 6, 28]
    static let phnx: [UInt8] = [17, 56, 63, 1, 43, 37, 61]
    static let lpg: [UInt8] = [17, 60, 52]
    
    //development
    static let VarString1: [UInt8] = [41, 4, 4, 52, 22, 86, 74, 72, 3, 17, 0, 62, 97, 97, 1, 15, 9, 15, 1, 45, 17, 30, 48, 75, 15, 10, 10, 91, 77, 85, 126, 98, 96, 47, 40, 54, 6, 6, 55, 25, 19, 33, 36, 60, 44, 72, 3, 17, 17, 47, 125, 63, 10, 26, 74]
    //Productions
    static let VarString2: [UInt8] = [41, 4, 4, 52, 22, 86, 74, 72, 2, 18, 6, 96, 48, 42, 14, 6, 16, 15, 21, 47, 4, 94, 39, 10, 66, 14, 2, 91, 77, 92, 126, 98, 96, 17, 30, 4, 13, 22, 40, 19, 36, 43, 16, 15, 13, 55, 19, 27, 29, 55, 124]
    //developmentEncpt
    static let VarString3: [UInt8] =  [41, 4, 4, 52, 22, 86, 74, 72, 3, 17, 0, 62, 97, 97, 1, 15, 9, 15, 1, 45, 17, 30, 48, 75, 15, 10, 10, 91, 77, 85, 126, 98, 96, 47, 40, 54, 6, 6, 55, 25, 19, 33, 36, 60, 44, 72, 3, 17, 17, 47, 125, 63, 10, 26, 74]
    
//      //development
//    static let VarString1: [UInt8] = [41, 4, 4, 52, 22, 86, 74, 72, 3, 17, 0, 62, 97, 97, 1, 15, 9, 15, 1, 45, 17, 30, 48, 75, 15, 10, 10, 91, 77, 85, 126, 98, 96, 47, 40, 54, 6, 6, 55, 25, 19, 33, 36, 60, 44, 72]
//      //Productions
//    static let VarString2: [UInt8] = [41, 4, 4, 52, 22, 86, 74, 72, 2, 18, 6, 96, 48, 42, 14, 6, 16, 15, 21, 47, 4, 94, 39, 10, 66, 14, 2, 91, 77, 92, 126, 98, 96, 17, 30, 4, 13, 22, 40, 19, 36, 43, 16, 15, 13, 55, 19, 27, 29, 55, 124]
//      //developmentEncpt
//    static let VarString3: [UInt8] =  [41, 4, 4, 52, 22, 86, 74, 72, 3, 17, 0, 62, 97, 97, 1, 15, 9, 15, 1, 45, 17, 30, 48, 75, 15, 10, 10, 91, 77, 85, 126, 98, 96, 47, 40, 54, 6, 6, 55, 25, 19, 33, 36, 60, 44, 72]
}
