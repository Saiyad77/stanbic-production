//
//  DateConverter.swift
//  Stanbic
//
//  Created by Vijay Patidar on 22/03/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation


class DateConverter : DateFormatter {
    
    var dateFormatter : DateFormatter? = nil
    
    override init() {
        super.init()
        dateFormatter = DateFormatter()
        //dateFormatter?.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        dateFormatter?.timeZone = TimeZone.current
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    func convertDate(date_str : String, formatTo : String, formatFrom : String) -> String {
    
        if date_str == "" {
            return ""
        }
        
        dateFormatter?.dateFormat = formatFrom
        dateFormatter?.locale = Locale(identifier: "en_US")
       // dateFormatter?.timeZone = NSTimeZone.local
        let date = dateFormatter?.date(from: date_str)
        dateFormatter?.dateFormat = formatTo
        if date != nil {
            
            let timeStamp = dateFormatter?.string(from: date!)
            return timeStamp!
        }
        else {
            return ""
        }
        
        
    }
    
    func stringToDate(strDate :String) throws -> Date {
        
        //     let date = Date().toLocalTime()
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        
        dateFormatter.calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)! as Calendar
        
        let timeZone = NSTimeZone.local
        let seconds : TimeInterval = Double(timeZone.secondsFromGMT(for:Date()))
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: Int(seconds)) as TimeZone?
        
        let strTemp = "\(strDate) 00:00:00 +0000"
        
        guard dateFormatter.date(from: strTemp) != nil else {
            throw VendingMachineError.errorFound
        }
        
        var dateTemp = dateFormatter.date(from: strTemp)
        
        dateTemp = try? self.DateToLocalDate(date: dateTemp!)
        
        return dateTemp!
    }
    
    func DateToLocalDate(date : Date)  throws -> Date {
        
        var dateToChange = date
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone?
        dateFormatter.dateFormat = "yyyy-MM-dd"//this your string date format
        dateFormatter.locale = Locale(identifier: "en_US")
        let dateStr = dateFormatter.string(from: date)
        
        guard dateFormatter.date(from: dateStr) != nil else {
            throw VendingMachineError.errorFound
        }
        
        dateToChange = dateFormatter.date(from: dateStr)!
        return dateToChange
        
    }
    
    func dateToDateWithZeroTimezone(date : Date) throws -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateStr = dateFormatter.string(from: date)
        let dateLocal = try self.stringToDate(strDate: dateStr)
        
        return dateLocal
    }
    
    func getDateInterval(toDate : Date, date_str : String, formatFrom : String) -> Int {
        
        if date_str == "" {
            return 0
        }
        let dateTemp = date_str.components(separatedBy: " ")
        if dateTemp.count > 1 {
            let date = try? stringToDate(strDate: dateTemp[0])
            let date2 = try? dateToDateWithZeroTimezone(date: toDate)
            
            //CALCULATE DAY FROM TODAY'S DATE
            if date != nil {
                let diffInDays = Calendar.current.dateComponents([.day], from: date2 ?? Date(), to: date!).day
                return diffInDays!
            }
        }
        
        return 0
    }
    
    func changeDate(toDate : Date, format : String) -> String {
        
        dateFormatter?.dateFormat = format
        let str = dateFormatter?.string(from: toDate)
        return str!
    }
}

enum VendingMachineError: Error {
    case errorFound
    
}
