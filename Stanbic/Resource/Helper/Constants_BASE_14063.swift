//
//  Constants.swift
//  Stanbic
//
//  Created by Vijay Patidar on 16/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit

//MARK:- Color
struct Color {
    static let white = UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
    static let red = UIColor.red
    static let c124_134_141 = UIColor.init(red: 124/255.0, green: 134/255.0, blue: 141/255.0, alpha: 1)
    static let c0_123_255 = UIColor.init(red: 0/255.0, green: 123/255.0, blue: 255/255.0, alpha: 1)
    static let c0_51_161 = UIColor.init(red: 0/255.0, green: 51/255.0, blue: 161/255.0, alpha: 1)
    static let c3_150_253 = UIColor(red: 3.0 / 255.0, green: 150.0 / 255.0, blue: 253.0 / 255.0, alpha: 1.0)
    static let c31_89_216 = UIColor(red: 31.0 / 255.0, green: 89.0 / 255.0, blue: 216.0 / 255.0, alpha: 1.0)
    static let black = UIColor.init(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1)
    static let c10_34_64 = UIColor.init(red: 10/255.0, green: 34/255.0, blue: 64/255.0, alpha: 1)
    static let c52_58_64 = UIColor.init(red: 52/255.0, green: 58/255.0, blue: 64/255.0, alpha: 1)
    static let c0_173_108 = UIColor.init(red: 0/255.0, green: 173/255.0, blue: 108/255.0, alpha: 1)
    static let c134_142_150 = UIColor.init(red: 134/255.0, green: 142/255.0, blue: 150/255.0, alpha: 1)
    static let c217_219_222 = UIColor.init(red: 217/255.0, green: 219/255.0, blue: 222/255.0, alpha: 1)
    static let c207_211_214 = UIColor.init(red: 207/255.0, green: 211/255.0, blue: 214/255.0, alpha: 1)
    static let c160_166_173 = UIColor.init(red: 160/255.0, green: 166/255.0, blue: 173/255.0, alpha: 1)
    static let c28_80_194 = UIColor.init(red: 28/255.0, green: 80/255.0, blue: 194/255.0, alpha: 1)
    static let c169_169_169 = UIColor.init(red: 169/255.0, green: 169/255.0, blue: 169/255.0, alpha: 1)
    static let c232_0_51 = UIColor.init(red: 232/255.0, green: 0/255.0, blue: 51/255.0, alpha: 1)
    static let c0_179_255 = UIColor.init(red: 0/255.0, green: 179/255.0, blue: 255/255.0, alpha: 1)
    static let c250_164_49 = UIColor(red: 250.0/255.0, green: 164/255.0, blue: 49/255.0, alpha: 1)
    static let c244_245_249 = UIColor(red: 244.0/255.0, green: 245/255.0, blue: 249/255.0, alpha: 1)
    static let c233_233_235 = UIColor(red: 233.0/255.0, green: 233/255.0, blue: 235/255.0, alpha: 1)
    static let c237_237_237 = UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1)
    static let c74_74_74 = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1)
    static let c0_137_255 = UIColor(red: 0/255.0, green: 137/255.0, blue: 255/255.0, alpha: 1)
    static let c201_211_214 =  UIColor(red: 207/255.0, green: 211/255.0, blue: 214/255.0, alpha: 1)
    static let c11_15_42 =  UIColor(red: 11/255.0, green: 15/255.0, blue: 42/255.0, alpha: 1)
}

struct NotificationID {
    let document = "DOCUMENT"
}

struct UserDefault {
    static let biometric = "BIOMETRIC"
    static let registered = "REGISTERED"
}

enum Storyboards : String {
    case Login_Registrtation, Temp, Home,Rtgs,Checkbook,Temp2,Temp4,Temp3
    
    func identifier() -> String {
        return self.rawValue
    }
    
}

struct User {
    
    static var username  = ""
    static var password = ""
}

struct AppMode {
    static var appMode : ApplicationMode?
}

enum ApplicationMode : Int {
    case development = 0, production, developmentEncpt
    
    func getBaseUrl() -> String {
        switch self {
            
        case .development:  //Base Url development
            let encryptedURL = "JsO1GV/nVos64DImGEU0NVeSdq/jewOC8uE0aWpPNNjle53F04pIcQ5EWWFa8pbv"
            let output:String = CryptoHelper.decrypt(input:encryptedURL) ?? "";
            return output
            
            
        case .production:  //Base Url Production
            let encryptedURL = "djP5n7QfVW//my+zul2+d5gS9g9w01cgBZ7YSzFL1ZAr13BT6Xf0NjiHAf29QY4qw51bDwzKmx5MxVIV/6iGEw=="
            let output:String = CryptoHelper.decrypt(input:encryptedURL) ?? "";
            return output
            
            
        case .developmentEncpt : //Base Url Encryption
            let encryptedURL = "JsO1GV/nVos64DImGEU0NVeSdq/jewOC8uE0aWpPNNjMAtxdjv1qNAVJtQruZfPWB9uPTBPHhAP0AGL30rEpSg=="
            let output:String = CryptoHelper.decrypt(input:encryptedURL) ?? "";
            return output
            
        }
    }
    
    func getOriginID() -> Int {
        // FOR DEVELOPMENT MODE 3 // FOR PRODUCTION MODE 7
        switch self {
        case .development, .developmentEncpt:
            return 3
            
        case .production:  //Base Url Production
            return 7
        }
    }
    func dcptPIN(text : String) -> String {
        
        let input:String = text
        //let cipher:String = CryptoHelper.encrypt(input:input)!;
        //cipher = "kb1TyFRUcMaY6Z1vCRravA==";
        let output:String = CryptoHelper.decrypt(input:input) ?? "";
        return output
    }
}

struct Secure {
    //ENCRYPT PIN
    func encryptPIN(text : String) -> String {
        let input:String = text
        let cipher:String = CryptoHelper.encrypt(input:input) ?? ""
        return cipher
    }
    
    func decryptPIN(text : String) -> String {
        let input:String = text
        let output:String = CryptoHelper.decrypt(input:input) ?? "";
        return output
    }
}


