//
//  ProcessingBar.swift
//  DTB
//
//  Created by Five Exceptions on 20/09/18.
//  Copyright © 2018 Five Exceptions. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

class ProcessingBar: UIView, CAAnimationDelegate {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var viewProgress: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    var isRotate = true
    
    func showCustomPopup(title: String, description : String, parent_view : UIView) {
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 20, color: Color.white, title: title, placeHolder: "")
        Fonts().set(object: lblDescription, fontType: 3, fontSize: 13, color: Color.white, title: description, placeHolder: "")
        self.rotateView(targetView: viewProgress)
    }
    
    // Rotate <targetView> indefinitely
    //heating
    private func rotateView(targetView: UIView, duration: Double = 0.4) {
        
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
            targetView.transform = targetView.transform.rotated(by: CGFloat(Double.pi))
        }) { finished in
            if self.isRotate == true {
                self.rotateView(targetView: targetView, duration: duration)
            }
        }
    }
    
    func animateView() {

        let fullRotation = CABasicAnimation(keyPath: "transform.rotation")
        fullRotation.delegate = self
        fullRotation.fromValue = NSNumber(floatLiteral: 0)
        fullRotation.toValue = NSNumber(floatLiteral: Double(CGFloat.pi * 2))
        fullRotation.duration = 0.8
        fullRotation.repeatCount = 100
        viewProgress.layer.add(fullRotation, forKey: "360")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("ProcessingBar", owner: self, options: nil)
        view.frame = frame
        //view.backgroundColor = UIColor(white: 1, alpha: 0.95)
        addSubview(self.view)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("ProcessingBar", owner: self, options: nil)
        addSubview(self.view)
    }
    
    func remove() {
        self.isRotate = false
        self.removeFromSuperview()
    }
}


