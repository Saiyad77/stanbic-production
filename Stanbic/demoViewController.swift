
//
//  demoViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 22/08/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class demoViewController: UIViewController {

    @IBOutlet weak var imgFrontPic: UIImageView!
    var img = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgFrontPic.image = img
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Home.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    //MARK :- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
