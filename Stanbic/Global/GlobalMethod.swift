//
//  GlobalMethod.swift
//  Stanbic
//
//  Created by 5exceptions-mac3 on 17/07/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit


class GlobalMethod {
    
    func willCallBE() -> Bool {
        
        let elapsed = Int(Date().timeIntervalSince(Session.sharedInstance.beAPICallTime ?? Date()))
 
         var unlockCardTime = 60
        
        if global.balance_expire_time.count > 0 {
            unlockCardTime = (Int(global.balance_expire_time) ?? 0)
        }
        
        if global.balance_expire_time.count > 0 {
            if Int(elapsed) > unlockCardTime {
                Session.sharedInstance.isUpdatedBEInfo = false
                return true
            }
            else {
                Session.sharedInstance.isUpdatedBEInfo = true
                return false
            }
        }
        else {
            Session.sharedInstance.isUpdatedBEInfo = false
            return true
        }
    }
    
    func willCallBENew(account_alise : String) -> Bool {
        
        let cardInfoArr = Session.sharedInstance.cardInfo ?? [[String : Any]]()
        for i in 0 ..< cardInfoArr.count {
            var cardInfoDict = cardInfoArr[i]
            if account_alise == cardInfoDict["ACCOUNT_ALIAS"] as? String {
                
                let elapsed = Int(Date().timeIntervalSince(cardInfoDict["APITIME"] as? Date ?? Date()))
                
                let beAPIIntervalTime = global.balance_expire_time
                
                var unlockCardTime = 60
                
                if beAPIIntervalTime.count > 0 {
                    unlockCardTime = (Int(global.balance_expire_time) ?? 0)
                }
                if Int(elapsed) > unlockCardTime {
                    Session.sharedInstance.isUpdatedBEInfo = false
                    return true
                }
                else {
                    Session.sharedInstance.isUpdatedBEInfo = true
                    return false
                }
            }
        }
        Session.sharedInstance.isUpdatedBEInfo = false
        return false
    }
    
    func willLockCard() -> Bool {
        
        if Session.sharedInstance.lockCardtime == nil {
            return false
        }
        else {
            let elapsed = Int(Date().timeIntervalSince(Session.sharedInstance.lockCardtime!))
            
            let unlockCardTime = 20
            
            if Int(elapsed) > unlockCardTime {
                Session.sharedInstance.lockCardtime = nil
                return true
            }
            else {
                return false
            }
        }
        
    }
    
    func willLockCardNew(account_alise : String) -> Bool {
        
        let cardInfoArr = Session.sharedInstance.cardInfo ?? [[String : Any]]()
        for i in 0 ..< cardInfoArr.count {
            var cardInfoDict = cardInfoArr[i]
            if account_alise == cardInfoDict["ACCOUNT_ALIAS"] as? String {
                
                let elapsed = Int(Date().timeIntervalSince(cardInfoDict["UNLOCKTIME"] as? Date ?? Date()))
                
                let unlockCardTime = 20
                if Int(elapsed) > unlockCardTime {
                    cardInfoDict["ACCOUNT_ALIAS"] = Date()
                    return true
                }
                else {
                    return false
                }
            }
        }
        return false
    }
}
