//
//  Timer.swift
//  DTB
//
//  Created by Five Exceptions on 09/10/18.
//  Copyright © 2018 Five Exceptions. All rights reserved.
//

import Foundation
import UIKit


class LockTimer {
    
    private var timer:Timer = Timer()
    private var state:Bool = true
    
    func initLockTimer() {
           if self.state {
            self.timer.invalidate()
            self.timer = Timer.scheduledTimer(timeInterval: 25, target: self, selector: #selector(self.onTimeEnd), userInfo: nil, repeats: false)
        }else{
            self.timer.invalidate()
        }
        self.state = !self.state
    }
    
    @objc func onTimeEnd() {
        
    }
    
    func initLockTimerNew(accont_alise : String) {
        
        let cardInfoArr = Session.sharedInstance.cardInfo ?? [[String : Any]]()
        for i in 0 ..< cardInfoArr.count {
            var cardInfoDict = cardInfoArr[i]
            if accont_alise == cardInfoDict["ACCOUNT_ALIAS"] as? String {
                cardInfoDict["UNLOCKTIME"] = Date()
                Session.sharedInstance.cardInfo?[i] = cardInfoDict
            }
        }
        
       // let date = Date().addingTimeInterval(10)
        
        //self.timer.fireDate = date
       // self.timer.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 25, target: self, selector: #selector(self.onTimeEndNew), userInfo: accont_alise, repeats: false)
        
    }
    
    @objc func onTimeEndNew(sender: Timer) {
        
       }
}



class TimerManager {
    
    var _timerTable = [String: Timer]()
    
    /*! Schedule a timer and return an integer that represents id of the timer
     */
    func startTimer(target: AnyObject, selector: Selector, alise: String) {
        let timer = Timer.scheduledTimer(timeInterval: 25, target: target, selector: selector, userInfo: alise, repeats: false)
        //_id += 1
        _timerTable[alise] = timer
    }
    
    /*! Stop a timer of an id
     */
    func stopTimer(alise: String) {
        if let timer = _timerTable[alise] {
            if timer.isValid {
                timer.invalidate()
            }
        }
    }
    
    /*! Returns timer instance of an id
     */
    func getTimer(alise: String) -> Timer? {
        return _timerTable[alise]
    }
    
}
