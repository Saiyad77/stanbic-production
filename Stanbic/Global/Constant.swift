//
//  Constant.swift
//  My ID
//

import UIKit

struct GlobalConstant {
    // MARK: APP Name and Url
    
    static var timer = Timer()
    
    
    static let APPNAME = ""
    static let DefaultURL = ""
    static let APIKey = ""
    static let GoogleAPIKey = ""
    static let FACEBOOKSCHEME = ""
    static let KKIsLogin = ""
    // MARK: - Screen Size
    static let SCREENWIDTH         = UIScreen.main.bounds.size.width
    static let SCREENHEIGHT        = UIScreen.main.bounds.size.height
    static let SCREENMAXLENGTH    = max(GlobalConstant.SCREENWIDTH, GlobalConstant.SCREENHEIGHT)
    static let SCREENMINLENGTH    = min(GlobalConstant.SCREENWIDTH, GlobalConstant.SCREENHEIGHT)
    // MARK: - Device IPHONE
    static let ISIPHONE4ORLESS  = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH < 568.0
    static let ISIPHONE5 = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 568.0
    static let ISIPHONE6 = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 667.0
    static let ISIPHONE6P = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 736.0
    static let ISIPHONEXAndXS = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 812.0
    static let ISIPHONEXSMax = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 2688.0
    static let ISIPHONEXR = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 1792.0
    
    static let ISIPAD = UIDevice.current.userInterfaceIdiom == .pad && GlobalConstant.SCREENMAXLENGTH == 1024.0
    static let ISIPADPRO = UIDevice.current.userInterfaceIdiom == .pad && GlobalConstant.SCREENMAXLENGTH == 1366.0
    // MARK: - Device Version
    static let SYSVERSIONFLOAT = (UIDevice.current.systemVersion as NSString).floatValue
    static let iOS7 = (GlobalConstant.SYSVERSIONFLOAT < 8.0 && GlobalConstant.SYSVERSIONFLOAT >= 7.0)
    static let iOS8 = (GlobalConstant.SYSVERSIONFLOAT >= 8.0 && GlobalConstant.SYSVERSIONFLOAT < 9.0)
    static let iOS9 = (GlobalConstant.SYSVERSIONFLOAT >= 9.0 && GlobalConstant.SYSVERSIONFLOAT < 10.0)
    // MARK: Navigation bar color
    static var navigationBarColor = ""
    
    static var font = UIFont(name:"Roboto-Regular", size: 13.0)
    static var fontMedium = UIFont(name:"Roboto-Medium", size: 13.0)
    
    
    // MARK: Caller Name
    static var kkCallerName = ""
    static var kkCallerID = ""
    static var kkCheckOutDate = ""
    static var selectedIndexpath: Int = 0
    static var kkBooking_id = ""
    static var kkBookingSuccessfull = ""
    
    static var KKbooking_status = ""
    
    static var controllerCheck = ""
    
    static var isAlreadyLaunchedOnce = false
    static var notificationDict:[AnyHashable : Any]?

     static var isStatusChange: Bool = Bool()
    
    
    static var cardlockUnlock: NSArray = NSArray()
    
    
   static var balanceDict: NSDictionary = NSDictionary()
   static var balanceArray: NSMutableArray = NSMutableArray()
   static var accountsInfo : [[String : String]]?

    
    }
    // MARK: All Segue
    struct Segue {
    static let kkSegueIntroductionToHome = "introductionToHome"
    }
    // MARK: API Names
    struct APIName {
    static let kkUserLogin = "introductionToHome"
    }

struct DeviceType {
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 736.0
    static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 812
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && GlobalConstant.SCREENMAXLENGTH  == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && GlobalConstant.SCREENMAXLENGTH   == 1366.0
    static let IS_IPHONE_XSMAX      = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 896
    static let IS_IPHONE_XR         = UIDevice.current.userInterfaceIdiom == .phone && GlobalConstant.SCREENMAXLENGTH == 1792
  }

 
