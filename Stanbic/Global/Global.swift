//
//  Global.swift
//  Stanbic
//
//  Created by 5exceptions on 14/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration


var viewControllerInstance: AnyObject!
var badgeCartViewController: AnyObject!
//var indicator: MaterialActivityIndicatorView!

class Global: NSObject {
    
    class var sharedInstance: Global {
        struct Static {
            static let instance: Global = Global()
        }
        return Static.instance
    }
    
    class func removeSpecialCharacter(str: String) -> String {
        let charsToRemove: Set<Character> = Set(" ()+-".characters)
        let newNumberCharacters = String(str.characters.filter { !charsToRemove.contains($0) })
  
        return newNumberCharacters
    }
    
    class func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    static func whenViewDidLoad(view: UIView) {
        let times = Double(global.timeout)
        GlobalConstant.timer = Timer.scheduledTimer(timeInterval: times ?? 120, target: self, selector: #selector(Global.doStuff), userInfo: nil, repeats: true)
        
        let resetTimer = UITapGestureRecognizer(target: self, action: #selector(Global.resetTimer));
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(resetTimer)
        
    }
    
    @objc static func doStuff() {
        GlobalConstant.timer.invalidate()
        NotificationCenter.default.post(name: Notification.Name("Idle"), object: nil)
     }
    
    @objc static func resetTimer() {
         let times = Double(global.timeout)
        GlobalConstant.timer.invalidate()
        GlobalConstant.timer = Timer.scheduledTimer(timeInterval: times ?? 120, target: self, selector: #selector(Global.doStuff), userInfo: nil, repeats: true)
    }
    
    class func invalidTimer() {
        GlobalConstant.timer.invalidate()
    }
    
    
    class func createCardView(view: UIView) {
        view.layer.cornerRadius = 5.0
        view.layer.borderWidth = 1.0
        view.layer.borderColor = Global.hexStringToUIColor("#868E96").cgColor
    }
    
    
    // MARK: - Global alert Methods
    static func showAlertMessageWithOkButtonAndTitle(_ strTitle: String, andMessage strMessage: String ) {
        if objc_getClass("UIAlertController") == nil {
            let alert = UIAlertController(title: strTitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(action)
        } else {
            let alertController: UIAlertController = UIAlertController(title: strTitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
            
            let okay: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okay)
            alertController.view.frame = CGRect(x: GlobalConstant.SCREENWIDTH-30, y: GlobalConstant.SCREENHEIGHT+20, width: 200, height: 200)
            alertController.view.layer.shadowColor = UIColor.black.cgColor
            alertController.view.layer.shadowOpacity = 0.8
            alertController.view.layer.shadowRadius = 5
            alertController.view.layer.shadowOffset = CGSize(width: 0, height: 0)
            alertController.view.layer.masksToBounds = false
            let topWindow: UIWindow = UIWindow(frame: UIScreen.main.bounds)
            topWindow.rootViewController = UIViewController()
            topWindow.windowLevel = UIWindow.Level.alert + 1
            topWindow.makeKeyAndVisible()
            alertController.view.frame = CGRect(x: GlobalConstant.SCREENWIDTH-30, y: GlobalConstant.SCREENHEIGHT+20, width: 200, height: 200)
            topWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK : Call Number
    class func callNumber(phoneNumber: String) {
        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)
                }
            }
        }
    }
    
    static func stringExists(_ str: String) -> Bool {
        var strString: String? = str
        if strString == nil {
            return false
        }
        if strString == String(describing: NSNull()) {
            return false
        }
        if strString == "<null>" {
            return false
        }
        if strString == "(null)" {
            return false
        }
        strString = Global.trim(str)
        if str == "" {
            return false
        }
        if strString?.count == 0 {
            return false
        }
        return true
    }
    // returns string value after removing null and unwanted characters
    static func getStringValue(_ str: AnyObject) -> String {
        if str is NSNull {
            return ""
        } else if str is String {
            return (str as? String)!
        } else if str is Double || str is Float || str is NSNumber || str is Int {
            return "\(str)"
        } else {
            var strString: String? = str as? String
            if Global.stringExists(strString!) {
                strString = strString!.replacingOccurrences(of: "\t", with: " ")
                strString = Global.trim(strString!)
                if strString == "{}" {
                    strString = ""
                }
                if strString == "()" {
                    strString = ""
                }
                if strString == "null" {
                    strString = ""
                }
                if strString == "<null>" {
                    strString = ""
                }
                return strString!
            }
            return ""
        }
    }
    
    // MARK: - String Methods
    /// Trim for String
    static func trim(_ value: String) -> String {
        let value = value.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return value
    }
    
    
    class func backButton(_ sender: UIViewController) {
        viewControllerInstance = sender
        let backBtn: UIButton = UIButton(type: .custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let backBtnImage: UIImage = UIImage(named: "back")!
        backBtn.setImage(backBtnImage, for: UIControl.State())
        backBtn.addTarget(self, action: #selector(Global.goback), for: UIControl.Event.touchUpInside)
        let backButton: UIBarButtonItem = UIBarButtonItem(customView: backBtn)
        ((viewControllerInstance as? UIViewController))!.navigationItem.leftBarButtonItem = backButton
    }
    @objc class func goback() {
        ((viewControllerInstance as? UIViewController))!.navigationController!.popViewController(animated: true)
    }
    
    
    class func showGlobalProgressHUD(withTitle title: String) {
        
      //  KRProgressHUD.set(maskType: .custom(color: UIColor.black.withAlphaComponent(0.5)))
       // KRProgressHUD.set(activityIndicatorViewColors: [.darkGray])
        //KRProgressHUD.show(withMessage: "Loading...")
        
    }
    
    class func dismissGlobalHUD() {
      //  KRProgressHUD.dismiss {
         
       // }
    }
    
    // MARK: - Creates a UIColor from a Hex string.
    static func hexStringToUIColor (_ hex: String) -> UIColor {
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }
        if (cString.count) != 6 {
            return UIColor.gray
        }
        var rgbValue: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
 
    
    //MARK: Convert Array To JSON Object
    class func convertArrayToJson(from object: Any) -> String? {
        if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString
        }
        return nil
    }
    
    
    // MARK: - Diffrence Between Two Dates
    class func dateDiff(_ dateStr: String, DateFormat dateFormate: String) -> String {
        let formate: DateFormatter = DateFormatter()
        formate.timeZone = NSTimeZone.local
        formate.dateFormat = dateFormate
        let now = formate.string(from: NSDate() as Date)
        let startDate = formate.date(from: dateStr)
        let endDate = formate.date(from: now)
        let dateComponents = Calendar.current.dateComponents([.month, .day, .hour, .minute, .second, .year], from: startDate!, to: endDate!)
        let weeks = dateComponents.weekOfMonth ?? 0
        let days = dateComponents.day ?? 0
        let hours = dateComponents.hour ?? 0
        let min = dateComponents.minute ?? 0
        let sec = dateComponents.second ?? 0
        let month = dateComponents.month ?? 0
        let year = dateComponents.year ?? 0

        var timeAgo = ""
        if  sec > 0 {
            if sec > 1 {
                timeAgo = "\(sec)"
            } else {
                timeAgo = "\(sec)"
            }
        }
        if  min > 0 {
            if min > 1 {
                timeAgo = "\(min)"
            } else {
                timeAgo = "\(min)"
            }
        }
        if hours > 0 {
            if hours > 1 {
                timeAgo = "\(hours)"
            } else {
                timeAgo = "\(hours)"
            }
        }
        if days > 0 {
            if days > 1 {
                timeAgo = "\(days)"
            } else {
                timeAgo = "\(days)"
            }
        }
        if weeks > 0 {
            if weeks > 1 {
                timeAgo = "\(weeks)"
            } else {
                timeAgo = "\(weeks)"
            }
        }
        if month > 0 {
            if month > 1 {
                timeAgo = "\(month)"
            } else {
                timeAgo = "\(month)"
            }
        }
        
        if year > 0 {
            if year > 1 {
                timeAgo = "\(year)"
            } else {
                timeAgo = "\(year)"
            }
        }
        return timeAgo
    }
    
    
 }
