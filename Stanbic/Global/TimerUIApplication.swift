//
//  TimeOut.swift
//  Stanbic
//
//  Created by 5exceptions-mac3 on 18/07/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit

extension NSNotification.Name {
    public static let TimeOutUserInteraction: NSNotification.Name = NSNotification.Name(rawValue: "TimeOutUserInteraction")
}


class InterractionUIApplication: UIApplication {
    
    static let ApplicationDidTimoutNotification = "AppTimout"
    // The timeout in seconds for when to fire the idle timer.
    let timeoutInSeconds: TimeInterval = Double(global.timeout) ?? 120 //30//15 * 60
    var idleTimer: Timer?
    
    // Listen for any touch. If the screen receives a touch, the timer is reset.
    override func sendEvent(_ event: UIEvent) {
        super.sendEvent(event)
        
        if UserDefaults.standard.object(forKey: "logout") as? Bool == true {
            if idleTimer != nil {
                
                self.resetIdleTimer()
                
            }
            
            if let touches = event.allTouches {
                for touch in touches {
                    if touch.phase == UITouch.Phase.began {
                        self.resetIdleTimer()
                    }
                }
            }
        }
    }
    
    // Resent the timer because there was user interaction.
    func resetIdleTimer() {
        if let idleTimer = idleTimer {
        
            idleTimer.invalidate()
        }
        
        idleTimer = Timer.scheduledTimer(timeInterval: timeoutInSeconds, target: self, selector: #selector(self.idleTimerExceeded), userInfo: ["timeout":"yes"], repeats: false)
    }
    
    // If the timer reaches the limit as defined in timeoutInSeconds, post this notification.
    @objc func idleTimerExceeded() {
        
        NotificationCenter.default.post(name:Notification.Name.TimeOutUserInteraction, object: nil)
        //Go Main page after 15 second
    }
}
