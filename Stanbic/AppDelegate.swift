//
//  AppDelegate.swift
//  Stanbic
//
//  Created by Vijay Patidar on 16/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import GooglePlaces
import GooglePlacePicker
import UserNotifications
import Firebase
import Crashlytics
import Fabric
import LocalAuthentication

class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var firebaseToken = ""
    var badgeCount = 0
    var appBackground = false
    var view = UIView()
    var str_biometric = Data()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
      // AppMode.appMode = ApplicationMode.development
       AppMode.appMode = ApplicationMode.production
        
        GMSServices.provideAPIKey("AIzaSyCvFxAOvA246L6Syk7Cl426254C-sMJGxk")
        GMSPlacesClient.provideAPIKey("AIzaSyBsztzLlpNfNvFgzeOTU3o79VWUjoZNuco")
        
        checkForRegisteredUser()
        UITextField.appearance().keyboardAppearance = .dark
//        let filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
//        guard let fileopts = FirebaseOptions(contentsOfFile: filePath!)
//            else { return false }
//          FirebaseApp.configure(options: fileopts)
        FirebaseApp.configure()
        
        Fabric.with([Crashlytics.self])
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        if UserDefaults.standard.object(forKey: "badgeCount") != nil{
            let badgeCount = UserDefaults.standard.object(forKey: "badgeCount") as! Int
            if badgeCount > 0 {
                UIApplication.shared.applicationIconBadgeNumber = badgeCount
                NotificationCenter.default.post(name: Notification.Name(rawValue: "notificationshow"), object: nil)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "badgecount"), object: nil)
            }
            else {
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
        }
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        NotificationCenter.default.addObserver(self, selector: #selector(Loginwhenidel), name: Notification.Name.TimeOutUserInteraction, object: nil)
        
//        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
//
//            if let userInfo = launchOptions?[.remoteNotification] as?  [AnyHashable : Any] {
//            badgeCount = 0
//
//            let coreData = CoreDataHelper()
//            var arrNotification = [[String: String]]()
//
//            if CoreDataHelper().FetchNotifications(entity: Entity.NOTIFICATIONS_DATA).count > 0 {
//                arrNotification = CoreDataHelper().FetchNotifications(entity: Entity.NOTIFICATIONS_DATA)
//            }
//
//            let imageUrl = userInfo[AnyHashable("gcm.notification.imageUrl")] as? String
//            let time = userInfo["gcm.notification.time"] as? String
//            let body = userInfo["body"] as? String
//            let title = userInfo["title"] as? String
//            let link = userInfo["link"] as? String
//            let notificationType = userInfo["notificationType"] as? String
//
//            if body?.count ?? 0  > 0 &&  title?.count ?? 0 > 0 {
//
//                let dict:[String:String]? = ["abody":body ?? "","atitle":title ?? "","aimageUrl":imageUrl ?? "","atime":time ?? "","alink":link ?? "","anotificationType":notificationType ?? "","aviewstatus":"true"]
//                if let getdict = dict{
//                    arrNotification.append(getdict)
//                    coreData.saveArrayForNotifiation(json: arrNotification, entityObject: Entity.NOTIFICATIONS_DATA, jsonKey: Entity.NOTIFICATIONS_DATA.rawValue)
//                }else{
//
//                }
//           }
//            else{
//
//            }
//
//            if UserDefaults.standard.object(forKey: "badgeCount") != nil{
//                badgeCount = UserDefaults.standard.object(forKey: "badgeCount") as! Int
//            }
//            if badgeCount == 0 {
//                UIApplication.shared.applicationIconBadgeNumber = 1
//                badgeCount = 1
//            }
//            else {
//                UIApplication.shared.applicationIconBadgeNumber = badgeCount + 1
//            }
//
//            badgeCount = UIApplication.shared.applicationIconBadgeNumber
//            UserDefaults.standard.setValue(badgeCount, forKey: "badgeCount")
//            UserDefaults.standard.synchronize()
//            NotificationCenter.default.post(name: Notification.Name(rawValue: "notificationshow"), object: nil)
//            NotificationCenter.default.post(name: Notification.Name(rawValue: "badgecount"), object: nil)
//            // UIApplication.shared.applicationIconBadgeNumber = 10
//            }
//            else {
//            // UIApplication.shared.applicationIconBadgeNumber = 20
//            }
//        }
//        else {
//           // UIApplication.shared.applicationIconBadgeNumber = 25
//        }
        return true
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
      print("application: UIApplication, didReceiveRemoteNotification userInfo: - 140")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("fetchCompletionHandler completionHandler: - 146")
        
        let coreData = CoreDataHelper()
        var arrNotification = [[String: String]]()
        if CoreDataHelper().FetchNotifications(entity: Entity.NOTIFICATIONS_DATA).count > 0 {
            arrNotification = CoreDataHelper().FetchNotifications(entity: Entity.NOTIFICATIONS_DATA)
        }
        
        let imageUrl = userInfo[AnyHashable("imageUrl")] as? String
        let time = userInfo["time"] as? String
        let body = userInfo["body"] as? String
        let title = userInfo["title"] as? String
        let link = userInfo["link"] as? String
        let notificationType = userInfo["notificationType"] as? String
        
        if body?.count ?? 0  > 0 &&  title?.count ?? 0 > 0 {
            
            let dict:[String:String]? = ["abody":body ?? "","atitle":title ?? "","aimageUrl":imageUrl ?? "","atime":time ?? "","alink":link ?? "","anotificationType":notificationType ?? "","aviewstatus":"true"]
            if let getdict = dict{
                arrNotification.append(getdict)
                coreData.saveArrayForNotifiation(json: arrNotification, entityObject: Entity.NOTIFICATIONS_DATA, jsonKey: Entity.NOTIFICATIONS_DATA.rawValue)
            }else{
              
            }
         }else{
            
        }
        
        if UserDefaults.standard.object(forKey: "badgeCount") != nil{
            badgeCount = UserDefaults.standard.object(forKey: "badgeCount") as! Int
        }
        
        if badgeCount == 0 {
            UIApplication.shared.applicationIconBadgeNumber = 1
            badgeCount = 1
        }
        else {
            UIApplication.shared.applicationIconBadgeNumber = badgeCount + 1
        }
        
        badgeCount = UIApplication.shared.applicationIconBadgeNumber
        UserDefaults.standard.setValue(badgeCount, forKey: "badgeCount")
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: Notification.Name(rawValue: "notificationshow"), object: nil)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "badgecount"), object: nil)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        global.showbiometric = true
        let isRegistered = UserDefaults.standard.bool(forKey:UserDefault.registered)
        if isRegistered {
            // Add biometic mode
            addBiometricMode()
            
            if !ChangeBiometic() {
                Loginwhenidel()
             } else {
                self.setUpEnterpin()
             }
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        let isRegistered = UserDefaults.standard.bool(forKey:UserDefault.registered)
        if isRegistered {
            
            // Add biometic mode
            addBiometricMode()
            
            if !ChangeBiometic() {
                
                Loginwhenidel()
                
            } else {
                self.setUpEnterpin()
                
                let alert = UIAlertController(title: "Stanbic", message: "We have detected changes in your biometrics. Please log in using your pin and enroll your biometrics again", preferredStyle: .alert)
                
                let actionYes = UIAlertAction(title: "Ok", style: .default, handler: { action in
                })
                
                alert.addAction(actionYes)
                //DispatchQueue.main.async {
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                //}
            }
        }
    }
    
    func setUpEnterpin() {
        // global.initialview = true
        let nextvc = EnterPinChangeBiometricVC.getVCInstance() as! EnterPinChangeBiometricVC
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        nextvc.flow = "BIO"
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        let isRegistered = UserDefaults.standard.bool(forKey:UserDefault.registered)
        if isRegistered {
            // Add biometic mode
            addBiometricMode()
            
            if !ChangeBiometic() {
                Loginwhenidel()
            } else {
                self.setUpEnterpin()
            }
        }
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Stanbic")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    func checkIsiPhoneIsJailbroken() -> Bool {
        
        if TARGET_IPHONE_SIMULATOR != 1 {
            // Check 1 : existence of files that are common for jailbroken devices
            if FileManager.default.fileExists(atPath: "/Applications/Cydia.app")
                || FileManager.default.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib")
                || FileManager.default.fileExists(atPath: "/bin/bash")
                || FileManager.default.fileExists(atPath: "/usr/sbin/sshd")
                || FileManager.default.fileExists(atPath: "/etc/apt")
                || FileManager.default.fileExists(atPath: "/private/var/lib/apt/")
                || UIApplication.shared.canOpenURL(URL(string: "cydia://package/com.example.package")!)
            {
                return true
            }
            // Check 2 : Reading and writing in system directories (sandbox violation)
            let stringToWrite = "Jailbreak Test"
            do
            {
                try stringToWrite.write(toFile: "/private/JailbreakTest.txt", atomically:true, encoding:String.Encoding.utf8)
                //Device is jailbroken
                return true
            }catch
            {
                return false
            }
        } else {
            return false
        }
    }
}

extension AppDelegate {
    
    func checkForRegisteredUser() {
        let isRegistered = UserDefaults.standard.bool(forKey:UserDefault.registered)
        if isRegistered {
            
            setUpSlideMenu()
            
        } else {
            //SAVE DATA IN KEYCHAIN
            let encpt = KeychainPasswordItem(service: KeychainConfiguration.encptKey, account: KeychainConfiguration.encptValue, accessGroup: KeychainConfiguration.accessGroup)
            try? encpt.saveValue(Variables().reveal(key: VariableConstants.VarialbeString))
        }
    }
    
    func setUpSlideMenu() {
        
        global.initialview = true
        let nextvc = StartingViewController.getVCInstance() as! StartingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    // Go enter pin when changes detect biometric
    //    func setUpEnterpin() {
    //
    //       // global.initialview = true
    //        let nextvc = EnterPinChangeBiometricVC.getVCInstance() as! EnterPinChangeBiometricVC
    //        let appDelegate = UIApplication.shared.delegate as! AppDelegate
    //        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
    //        appDelegate.window?.rootViewController = nextvc
    //        appDelegate.window?.makeKeyAndVisible()
    //
    //    }
    
    func gohome(){
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        leftVC.mainViewController = homeVC
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate  {
    
    // open app
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void){
        
        print("_ center: UNUserNotificationCenter,withCompletionHandler completionHandler")
        let userInfo = notification.request.content.userInfo
        let coreData = CoreDataHelper()
        var arrNotification = [[String: String]]()
        
        if CoreDataHelper().FetchNotifications(entity: Entity.NOTIFICATIONS_DATA).count > 0 {
            arrNotification = CoreDataHelper().FetchNotifications(entity: Entity.NOTIFICATIONS_DATA)
        }
        
        let imageUrl = userInfo[AnyHashable("gcm.notification.imageUrl")] as? String
        let time = userInfo["gcm.notification.time"] as? String
        let body = userInfo["body"] as? String
        let title = userInfo["title"] as? String
        let link = userInfo["link"] as? String
        let notificationType = userInfo["notificationType"] as? String
        
        if body?.count ?? 0  > 0 &&  title?.count ?? 0 > 0 {
            
            let dict:[String:String]? = ["abody":body ?? "","atitle":title ?? "","aimageUrl":imageUrl ?? "","atime":time ?? "","alink":link ?? "","anotificationType":notificationType ?? "","aviewstatus":"true"]
            if let getdict = dict{
                arrNotification.append(getdict)
                coreData.saveArrayForNotifiation(json: arrNotification, entityObject: Entity.NOTIFICATIONS_DATA, jsonKey: Entity.NOTIFICATIONS_DATA.rawValue)
                
            }else{
                
            }
            
        }
        if UserDefaults.standard.object(forKey: "badgeCount") != nil{
            badgeCount = UserDefaults.standard.object(forKey: "badgeCount") as! Int
        }
        
        if badgeCount == 0 {
            UIApplication.shared.applicationIconBadgeNumber = 1
            badgeCount = 1
        }
        else {
            UIApplication.shared.applicationIconBadgeNumber = badgeCount + 1
        }
        
        badgeCount = UIApplication.shared.applicationIconBadgeNumber
        UserDefaults.standard.setValue(badgeCount, forKey: "badgeCount")
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: Notification.Name(rawValue: "notificationshow"), object: nil)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "badgecount"), object: nil)
        completionHandler([.alert,.sound, .badge])
        
   
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("userNotificationCenter(_ center: UNUserNotificationCenter - 441")
        
        if UIApplication.shared.applicationState == .inactive {
            
         let userInfo = response.notification.request.content.userInfo
//
//        if let messageID = userInfo[gcmMessageIDKey] {
//
//        }
//        completionHandler()
        
        let coreData = CoreDataHelper()
        var arrNotification = [[String: String]]()
        if CoreDataHelper().FetchNotifications(entity: Entity.NOTIFICATIONS_DATA).count > 0 {
            arrNotification = CoreDataHelper().FetchNotifications(entity: Entity.NOTIFICATIONS_DATA)
        }
        
        let imageUrl = userInfo[AnyHashable("imageUrl")] as? String
        let time = userInfo["time"] as? String
        let body = userInfo["body"] as? String
        let title = userInfo["title"] as? String
        let link = userInfo["link"] as? String
        let notificationType = userInfo["notificationType"] as? String
        
        if body?.count ?? 0  > 0 &&  title?.count ?? 0 > 0 {
            
            let dict:[String:String]? = ["abody":body ?? "","atitle":title ?? "","aimageUrl":imageUrl ?? "","atime":time ?? "","alink":link ?? "","anotificationType":notificationType ?? "","aviewstatus":"true"]
            if let getdict = dict{
                arrNotification.append(getdict)
                coreData.saveArrayForNotifiation(json: arrNotification, entityObject: Entity.NOTIFICATIONS_DATA, jsonKey: Entity.NOTIFICATIONS_DATA.rawValue)
            }else{
                
            }
        }else{
            
        }
        
        if UserDefaults.standard.object(forKey: "badgeCount") != nil{
            badgeCount = UserDefaults.standard.object(forKey: "badgeCount") as! Int
        }
        
        if badgeCount == 0 {
            UIApplication.shared.applicationIconBadgeNumber = 1
            badgeCount = 1
        }
        else {
            UIApplication.shared.applicationIconBadgeNumber = badgeCount + 1
        }
        
        badgeCount = UIApplication.shared.applicationIconBadgeNumber
        UserDefaults.standard.setValue(badgeCount, forKey: "badgeCount")
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: Notification.Name(rawValue: "notificationshow"), object: nil)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "badgecount"), object: nil)
        }
        completionHandler()
     }
}

// [END ios_10_message_handling]
extension AppDelegate : MessagingDelegate {
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        firebaseToken = fcmToken
        print("firebaseToken  firebaseToken \n \(firebaseToken) firebaseToken  firebaseToken \n")
         let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")

    }
    
    @objc func Loginwhenidel() {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func notification(message msg : String, timeInterval : TimeInterval){
        let localNotification = UILocalNotification()
        localNotification.alertBody = msg
        localNotification.fireDate = Date(timeIntervalSinceNow: timeInterval)
        localNotification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.shared.scheduleLocalNotification(localNotification)
    }
    
    // Add Biometic mode
    func addBiometricMode(){
        
        let arr = CoreDataHelper().getDataForEntity1(entity : Entity.BIOMETRIC)
        if arr.count > 0
        {
            str_biometric = arr[0]["aNEW"] ?? Data()
        }
        
        var datamode = Data()
        let context = LAContext()
        context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        if context.evaluatedPolicyDomainState != nil {
            datamode = (context.evaluatedPolicyDomainState ?? nil)!
//            let str = String(decoding: datamode, as: UTF8.self)
            CoreDataHelper().removeAllRecords(Entity.BIOMETRIC.rawValue)
            CoreDataHelper().saveUUID1(value : datamode, entityName : Entity.BIOMETRIC.rawValue, key : "NEW", removeRecords : true)
          }
        else{
            // let str = String(decoding: datamode, as: UTF8.self)
            
            let data = Data("1212".utf8)
            
            CoreDataHelper().removeAllRecords(Entity.BIOMETRIC.rawValue)
            CoreDataHelper().saveUUID1(value : data, entityName : Entity.BIOMETRIC.rawValue, key : "NEW", removeRecords : true)
            
           // CoreDataHelper().saveUUID(value : "datanil", entityName : Entity.BIOMETRIC.rawValue, key : "NEW", removeRecords : true)
          }
     }
    
    // Detect Change biometric ThumbID / Face ID -
    func ChangeBiometic() -> Bool {
        
        let context = LAContext()
        context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        
        let arr = CoreDataHelper().getDataForEntity1(entity : Entity.BIOMETRIC)
        if arr.count > 0
        {
            if str_biometric == arr[0]["aNEW"] ?? Data() || str_biometric == Data()
            {
                return false
            }
            else {
                UserDefaults.standard.set(false, forKey: UserDefault.biometric)
                UserDefaults.standard.synchronize()
                return true
            }
        }
        else{
            return false
        }
    }
}

extension UserDefaults {
    func contains(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
}

extension AppDelegate {
    
    func registerAPNS (_ application: UIApplication) {
        if #available(iOS 10.0, *)
        {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            UNUserNotificationCenter.current().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }
    
    func connectToFcm() {
        if Messaging.messaging().isDirectChannelEstablished {
            print("Connected to FCM.")
        } else {
            print("Disconnected from FCM.")
        }
    }
   
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("Couldn't register: \(error)")
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
         print("Notif register: \(userInfo)")
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject],fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        Messaging.messaging().appDidReceiveMessage(userInfo)
    }
  }
