//
//  RestAPIManager.swift
//  RestAPIJSONProject
//
//  Created by 5Exception-Mac2 on 22/07/16.
//  Copyright © 2016 5Exception-Mac2. All rights reserved.
//

import UIKit

    typealias ServiceResponse = (JSON, NSError?) -> Void
    
    class RestAPIManager: NSObject {
        
        static let sharedInstance = RestAPIManager()
        static var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier(rawValue: 0)

        /////////**** BASE URL *****//////////
      //  let baseURL = AppMode.appMode?.getBaseUrl() ?? ""
        static let baseURL = "https://beep2.cellulant.com:9001/MBServiceAPI/"
        static let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        static let clientHash = "$P$B7lwP0IV3fdK4H3oAuttUZiKh3sc7c1"
        static let UDEVID = "0ed100cf-ad33-4953-b1ae-447ae4cc08d1"
        
        class func getuuidFromDb() -> String
        {
            var udidArray = CoreDataHelper().getDataForEntity(entity : Entity.DEVICEID)
            if udidArray.count > 0
            {
                let udid = udidArray[0]["aDEVICEID"] ?? ""
                return udid
            }
            else
            {
                CoreDataHelper().saveUUID(value: UIDevice.current.identifierForVendor!.uuidString, entityName: Entity.DEVICEID.rawValue, key: Entity.DEVICEID.rawValue, removeRecords: true)
                return UIDevice.current.identifierForVendor!.uuidString
            }
        }

        
        //Get Validation Key
        class func get_validation_key<T>(title : String, subTitle : String, type: T.Type, mobileNo : String, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"              :   sCommand.GAK,
                "UPHONENUM"             :   mobileNo,
                "UDEVID"                :   getuuidFromDb(),
                "ORGID"                 :   sCommand.oRGID,
                "CUSTOMER_HASH"         :   getuuidFromDb(),
                "CLIENT_HASH"           :   clientHash,
                "osVersion"             :   UIDevice.current.systemVersion,
                "appVersion"            :   appVersion,
                "ORIGIN"                :   sCommand.oRIGIN,
                "parseInstallationID"   :   getuuidFromDb(),
                "deviceName"            :   getuuidFromDb(),
                "HASH_KEY"              :   getuuidFromDb()
                ] as [String : Any]
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        //Get Validation Key
        class func verify_validation_key<T>(title : String, subTitle : String, activationKey : String,type: T.Type, mobileNo : String, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"              :   sCommand.VAK,
                "UPHONENUM"             :   mobileNo,
                "UDEVID"                :   getuuidFromDb(),
                "ORGID"                 :   sCommand.oRGID,
                "CUSTOMER_HASH"         :   getuuidFromDb(),
                "CLIENT_HASH"           :   clientHash,
                "osVersion"             :   UIDevice.current.systemVersion,
                "appVersion"            :   appVersion,
                "ORIGIN"                :   sCommand.oRIGIN,
                "ACTIVATION_KEY"        :   activationKey
                ] as [String : Any]
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        //Get details
        class func get_setQuestion(title : String, subTitle : String, action : String, answer : [[String : String]],onCompletion: @escaping (JSON) -> Void) {
            
            let paramDict = [
                "SCOMMAND"      :   sCommand.SR,
                "UPHONENUM"     :   OnboardUser.mobileNo,
                "UDEVID"        :   getuuidFromDb(),
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :   getuuidFromDb(),
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "ACTION"        :   action,
                "QUESTIONS"     :   answer
                
                ] as [String : Any]
            
            makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Get details
        class func getAccountOpeningData(title : String, subTitle : String, action : String, answer : [[String : String]],onCompletion: @escaping (JSON) -> Void) {
            
            let paramDict = [
                "SCOMMAND"      :   sCommand.AO,
                "UPHONENUM"     :   OnboardUser.mobileNo,
                "UDEVID"        :   getuuidFromDb(),
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :   getuuidFromDb(),
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "ACTION"        :   action,
                ] as [String : Any]
            
            makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Get details
        class func validateData(title : String, subTitle : String, action : String, idNo : String, idType : String, imgFront : UIImage, imgBack : UIImage, onCompletion: @escaping (JSON) -> Void) {
            
            let paramDict = [
                "SCOMMAND"      :   sCommand.AO,
                "UPHONENUM"     :   OnboardUser.mobileNo,
                "UDEVID"        :   getuuidFromDb(),
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :   getuuidFromDb(),
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "ACTION"        :   action,
                "ID_NUMBER"     :   idNo,
                "ID_TYPE"       :   idType,
                "ID_FRONT"      :   "imgFront",
                "ID_BACK"       :   "imgBack"
                ] as [String : Any]
            
            makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Personal detail validation
        class func validatePersonalDetail(title : String, subTitle : String, action : String, imgSelfie : UIImage, imgSignature : UIImage, onCompletion: @escaping (JSON) -> Void) {
            
            let paramDict = [
                "SCOMMAND"      :   sCommand.AO,
                "UPHONENUM"     :   OnboardUser.mobileNo,
                "UDEVID"        :   getuuidFromDb(),
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :   getuuidFromDb(),
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "ACTION"        :   action,
                "SELFIE"        :   "imgSelfie",
                "SIGNATURE"     :   "imgSignature",
                "KRA_PIN"       :   OnboardUser.userKraPIN,
                "EMAIL"         :   OnboardUser.userEmail
                ] as [String : Any]
    
            
            makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Occupational & next kra pin submit data
        class func submitOccupationl_KraDetail<T>(title : String, subTitle : String,type: T.Type, action : String, dataDict : [String : Any], completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            var paramDict = dataDict
            paramDict["SCOMMAND"]       = sCommand.AO
            paramDict["UPHONENUM"]      = OnboardUser.mobileNo
            paramDict["UDEVID"]         = getuuidFromDb()
            paramDict["ORGID"]          = sCommand.oRGID
            paramDict["CUSTOMER_HASH"]  = getuuidFromDb()
            paramDict["CLIENT_HASH"]    = clientHash
            paramDict["osVersion"]      = UIDevice.current.systemVersion
            paramDict["appVersion"]     = appVersion
            paramDict["ORIGIN"]         = sCommand.oRIGIN
            paramDict["ACTION"]         = action
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        //Get Validation Key
        class func validatePIN<T>(title : String, subTitle : String, pin : String,type: T.Type, mobileNo : String, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"      :   sCommand.VPIN,
                "UPHONENUM"     :   mobileNo,
                "UDEVID"        :   getuuidFromDb(),
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :   getuuidFromDb(),
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "UAUTHPIN"      :   pin
                
                ] as [String : Any]
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        //Check Updates/Fetch System Settings
        class func checkUpdatesFetchSystemSettings(title : String, subTitle : String, onCompletion: @escaping (JSON) -> Void) {
            
            let paramDict = [
                "SCOMMAND"      :   sCommand.CUP,
                "UPHONENUM"     :   OnboardUser.mobileNo,
                "UDEVID"        :   getuuidFromDb(),
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :   getuuidFromDb(),
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "ALL_FLAG"      :   "1"
                ] as [String : Any]
            
            makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Check Updates/Fetch System Settings
        class func balanceEnquiryFor(title : String, subTitle : String, accountAlise : String, onCompletion: @escaping (JSON) -> Void) {
            
            let paramDict = [
                "SCOMMAND"      :   sCommand.BE,
                "UPHONENUM"     :   OnboardUser.mobileNo,
                "UDEVID"        :   getuuidFromDb(),
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :   getuuidFromDb(),
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "UAUTHPIN"      :   Secure().encryptPIN(text: "1234"),
                "ALLFLAG"       :   "0",
                "ACCOUNT_ALIAS" :   accountAlise
                ] as [String : Any]
            
            makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Fetch bill
        class func fetchBill<T>(title : String, subTitle : String,type: T.Type, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"       :   sCommand.FB,
                "UPHONENUM"      :   OnboardUser.mobileNo,
                "UDEVID"         :   getuuidFromDb(),
                "ORGID"          :   sCommand.oRGID,
                "CUSTOMER_HASH"  :   getuuidFromDb(),
                "CLIENT_HASH"    :   clientHash,
                "osVersion"      :   UIDevice.current.systemVersion,
                "appVersion"     :   appVersion,
                "ORIGIN"         :   sCommand.oRIGIN,
                
                ] as [String : Any]
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        
        //Buy Airtime
        class func buyAirtime<T>(title : String, subTitle : String, pin : String,type: T.Type, mobileNo : String,accountAlias : String,amount : String,recipetno : String,mnoWalletId : String,nominateFlag : String,reciptAlias : String, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"       :   sCommand.BAO,
                "UPHONENUM"      :   mobileNo,
                "UDEVID"         :   getuuidFromDb(),
                "ORGID"          :   sCommand.oRGID,
                "CUSTOMER_HASH"  :   getuuidFromDb(),
                "CLIENT_HASH"    :   clientHash,
                "osVersion"      :   UIDevice.current.systemVersion,
                "appVersion"     :   appVersion,
                "ORIGIN"         :   sCommand.oRIGIN,
                "UAUTHPIN"       :   pin,
                "ACCOUNT_ALIAS"  :   accountAlias,
                "AMOUNT"         :   amount,
                "RECIPIENT_NO"   :   recipetno,
                "MNO_WALLET_ID"  :   mnoWalletId,
                "NOMINATE_FLAG"  :   nominateFlag,
                "RECIPIENT_ALIAS":   reciptAlias,
               
                ] as [String : Any]
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        // Mini Statement
        
        class func miniStatement<T>(title : String, subTitle : String, pin : String,type: T.Type, mobileNo : String,accountAlias : String, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"       :   sCommand.MSR,
                "UPHONENUM"      :   mobileNo,
                "UDEVID"         :   UDEVID,
                "ORGID"          :   sCommand.oRGID,
                "CUSTOMER_HASH"  :   getuuidFromDb(),
                "CLIENT_HASH"    :   clientHash,
                "osVersion"      :   UIDevice.current.systemVersion,
                "appVersion"     :   appVersion,
                "ORIGIN"         :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"  :   accountAlias,
                "UAUTHPIN"       :  pin] as [String : Any]
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
       
        
        // Send to Mobile Money
        class func sendtomobilemoney<T>(title : String, subTitle : String, pin : String,type: T.Type, mobileNo : String,accountAlias : String,amount : String,mnowalletid : String,recipientno :String,recipientalias :String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"       :   sCommand.SM,
                "UPHONENUM"      :   mobileNo,
                "UDEVID"         :   UDEVID,
                "ORGID"          :   sCommand.oRGID,
                "CUSTOMER_HASH"  :   getuuidFromDb(),
                "CLIENT_HASH"    :   clientHash,
                "osVersion"      :   UIDevice.current.systemVersion,
                "appVersion"     :   appVersion,
                "ORIGIN"         :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"  :   accountAlias,
                "UAUTHPIN"       :   pin,
                "AMOUNT"         :   amount,
                "MNO_WALLET_ID"  :   mnowalletid,
                "RECIPIENT_ALIAS":   recipientalias,
                "RECIPIENT_NO"   :   recipientno] as [String : Any]
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        //  Mpesa STK Push
        class func mPesaToaccount<T>(title : String, subTitle : String, pin : String,type: T.Type, mobileNo : String,accountAlias : String,amount : String,accountnumber:String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"       :   sCommand.STKTOPUP,
                "UPHONENUM"      :   mobileNo,
                "UDEVID"         :   UDEVID,
                "ORGID"          :   sCommand.oRGID,
                "CUSTOMER_HASH"  :   getuuidFromDb(),
                "CLIENT_HASH"    :   clientHash,
                "osVersion"      :   UIDevice.current.systemVersion,
                "appVersion"     :   appVersion,
                "ORIGIN"         :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"  :   accountAlias,
                "UAUTHPIN"       :   pin,
                "AMOUNT"         :   amount,
                "ACCOUNT_NUMBER" :   accountnumber] as [String : Any]
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        // QueryBill
        class func querybill<T>(title : String, subTitle : String, billerrefrence : String,type: T.Type, mobileNo : String,biller : String,hubserviceid : String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"       :   sCommand.QBILL,
                "UPHONENUM"      :   mobileNo,
                "UDEVID"         :   UDEVID,
                "ORGID"          :   sCommand.oRGID,
                "CUSTOMER_HASH"  :   getuuidFromDb(),
                "CLIENT_HASH"    :   clientHash,
                "osVersion"      :   UIDevice.current.systemVersion,
                "appVersion"     :   appVersion,
                "ORIGIN"         :   sCommand.oRIGIN,
                "BILLER_REFERENCE":  billerrefrence,
                "BILLER"         :   biller,
                "HUB_SERVICEID"  :   hubserviceid,
                ] as [String : Any]
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        
        
        
        
        
        // temp1
        class func paybill<T>(title : String, subTitle : String, billerrefrence : String,type: T.Type, mobileNo : String,biller : String,accountalias : String,amount : String,enrollbillrefrence : String,enrollmenalias : String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"        :   sCommand.PB,
                "UPHONENUM"       :   mobileNo,
                "UDEVID"          :   UDEVID,
                "ORGID"           :   sCommand.oRGID,
                "CUSTOMER_HASH"   :   getuuidFromDb(),
                "CLIENT_HASH"     :   clientHash,
                "osVersion"       :   UIDevice.current.systemVersion,
                "appVersion"      :   appVersion,
                "ORIGIN"          :   sCommand.oRIGIN,
                "BILLER_REFERENCE":  billerrefrence,
                "BILLER"          :   biller,
                "ACCOUNT_ALIAS"   :   accountalias,
                "AMOUNT"          :   amount,
                "ENROLL_BILL_REFERENCE" :   enrollbillrefrence,
                "ENROLLMENT_ALIAS"  :   enrollmenalias
                ] as [String : Any]
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        
        // Internal Fund Transfer
        class func Internalfuntranfer<T>(title : String, subTitle : String, scommand : String, acalias : String,type: T.Type, mobileNo : String,pin : String,amount : String,recieptacc : String,recieptBankbranch : String,nominateflag : String,recieptalias : String,reciptBank : String,recieptacname : String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"       :   scommand,
                "UPHONENUM"      :   mobileNo,
                "UDEVID"         :   UDEVID,
                "ORGID"          :   sCommand.oRGID,
                "CUSTOMER_HASH"  :   getuuidFromDb(),
                "CLIENT_HASH"    :   clientHash,
                "osVersion"      :   UIDevice.current.systemVersion,
                "appVersion"     :   appVersion,
                "ORIGIN"         :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"  :  acalias,
                "UAUTHPIN"       :   pin,
                "AMOUNT"         :   amount,
                "RECIPIENT_ACC"  :   recieptacc,
                "RECIPIENT_BANK_BRANCH":  recieptBankbranch,
                "NOMINATE_FLAG"     :   nominateflag,
                "RECIPIENT_ALIAS"   :   recieptalias,
                "RECIPIENT_BANK"    :  reciptBank,
                "RECIPIENT_ACC_NAME":recieptacname
                ] as [String : Any]
      
             RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        // RTGS Fund Transfer
        class func rtgs<T>(title : String, subTitle : String, acalias : String,type: T.Type, mobileNo : String,pin : String,amount : String,recieptacc : String,recieptBankbranch : String,nominateflag : String,recieptalias : String,reciptBank : String,recieptacname : String,narration:String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"       :   sCommand.RTGS,
                "UPHONENUM"      :   mobileNo,
                "UDEVID"         :   UDEVID,
                "ORGID"          :   sCommand.oRGID,
                "CUSTOMER_HASH"  :   getuuidFromDb(),
                "CLIENT_HASH"    :   clientHash,
                "osVersion"      :   UIDevice.current.systemVersion,
                "appVersion"     :   appVersion,
                "ORIGIN"         :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"  :  acalias,
                "UAUTHPIN"       :   pin,
                "AMOUNT"         :   amount,
                "RECIPIENT_ACC"  :   recieptacc,
                "RECIPIENT_BANK_BRANCH":  recieptBankbranch,
                "NOMINATE_FLAG"     :   nominateflag,
                "RECIPIENT_ALIAS"   :   recieptalias,
                "RECIPIENT_BANK"    :  reciptBank,
                "RECIPIENT_ACC_NAME":recieptacname,
                "NARRATION":narration
                ] as [String : Any]
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        
        // Pesalink - Send to Account
        class func sendToAccount<T>(title : String, subTitle : String, acalias : String,type: T.Type, mobileNo : String,pin : String,amount : String,recieptacnumber : String,nominateflag : String,bank : String,narration:String,nominate:String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"             :   sCommand.PAYAC,
                "UPHONENUM"            :   mobileNo,
                "UDEVID"               :   UDEVID,
                "ORGID"                :   sCommand.oRGID,
                "CUSTOMER_HASH"        :   getuuidFromDb(),
                "CLIENT_HASH"          :   clientHash,
                "osVersion"            :   UIDevice.current.systemVersion,
                "appVersion"           :   appVersion,
                "ORIGIN"               :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"        :   acalias,
                "UAUTHPIN"             :   pin,
                "AMOUNT"               :   amount,
                "NOMINATION_ALIAS"     :   nominateflag,
                "BANK"                 :   bank,
                "NOMINATE"             :   nominate,
                "CURRENCY"             :   "KES",
                "RECIPIENT_ACCOUNT_NUMBER"  :   recieptacnumber,
                "NARRATION"            :   narration,
                
                ] as [String : Any]
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        
        // Pesalink - Send to CARD
        class func sendToCard<T>(title : String, subTitle : String, acalias : String,type: T.Type, mobileNo : String,pin : String,amount : String,recieptacnumber : String,nominateflag : String,cardnumber : String,narration:String,nominate:String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"             :   sCommand.PAYC,
                "UPHONENUM"            :   mobileNo,
                "UDEVID"               :   UDEVID,
                "ORGID"                :   sCommand.oRGID,
                "CUSTOMER_HASH"        :   getuuidFromDb(),
                "CLIENT_HASH"          :   clientHash,
                "osVersion"            :   UIDevice.current.systemVersion,
                "appVersion"           :   appVersion,
                "ORIGIN"               :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"        :   acalias,
                "UAUTHPIN"             :   pin,
                "AMOUNT"               :   amount,
                "NOMINATION_ALIAS"     :   nominateflag,
                "CARD_NUMBER"          :   cardnumber,
                "NOMINATE"             :   nominate,
                "CURRENCY"             :   "KES",
                "NARRATION"            :   narration,
                
                ] as [String : Any]
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        
        // Pesalink - Send to PHONE
        class func sendToPhone<T>(title : String, subTitle : String, acalias : String,type: T.Type, mobileNo : String,pin : String,amount : String,recieptacnumber : String,nominateflag : String,banksortcode : String,narration:String,nominate:String,receipentmobnumber:String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let paramDict = [
                "SCOMMAND"             :   sCommand.PAYM,
                "UPHONENUM"            :   mobileNo,
                "UDEVID"               :   UDEVID,
                "ORGID"                :   sCommand.oRGID,
                "CUSTOMER_HASH"        :   getuuidFromDb(),
                "CLIENT_HASH"          :   clientHash,
                "osVersion"            :   UIDevice.current.systemVersion,
                "appVersion"           :   appVersion,
                "ORIGIN"               :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"        :   acalias,
                "UAUTHPIN"             :   pin,
                "AMOUNT"               :   amount,
                "NOMINATION_ALIAS"     :   nominateflag,
                "BANK_SORT_CODE"       :   banksortcode,
                "NOMINATE"             :   nominate,
                "CURRENCY"             :   "KES",
                "RECIPIENT_BANK"       :   recieptacnumber,
                "NARRATION"            :   narration,
                "RECIPIENT_MOBILE_NUMBER" : receipentmobnumber
                
                ] as [String : Any]
            
            RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
                completion(response)
            }) { (err) in
                error(err)
            }
        }
        
        
        //MARK: Perform POST Request
        class func makeHTTPPOSTRequest(_ path: String, body: [String: AnyObject], title : String, subtitle : String, onCompletion: @escaping ServiceResponse) {
            var encriptedInfo = [String : AnyObject]()
            
            if AppMode.appMode == ApplicationMode.developmentEncpt {  //FOR NORMAL REQUEST
                encriptedInfo = body
            }
            else if AppMode.appMode == ApplicationMode.production { // //FOR ENCRIPTED REQUEST
                let jsonData = try! JSONSerialization.data(withJSONObject: body, options: [])
                var decoded = String(data: jsonData, encoding: .utf8) ?? ""
                decoded = Secure().encryptPIN(text: decoded)
                let phnx = Variables().reveal(key: VariableConstants.phnx)
                let lpg = Variables().reveal(key: VariableConstants.lpg)
                encriptedInfo[phnx] = "1" as AnyObject
                encriptedInfo[lpg] = decoded as AnyObject
            }
        
            let headers = ["content-type": "application/json"]
            let request = NSMutableURLRequest(url: URL(string: path)!)
            request.allHTTPHeaderFields = headers
            request.httpMethod = "POST"
            request.timeoutInterval = 120 // In second
            let loader = RestAPIManager.addProgress(title: title, description: subtitle)
            do {
                // Set the POST body for the request
                let jsonBody = try JSONSerialization.data(withJSONObject: encriptedInfo, options: .prettyPrinted)
                
                request.httpBody = jsonBody
                
                let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                    
                    self.backgroundTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
                         endBackgroundTask()
                    })
//                    self.backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
//                          endBackgroundTask()
//                    }
                    assert(self.backgroundTask.rawValue != 0)
                    
                    
                    if let jsonData = data {
                        
                        let json:JSON = JSON(data: jsonData)
                        
                        let phnx = Variables().reveal(key: VariableConstants.phnx)
                        let lpg = Variables().reveal(key: VariableConstants.lpg)
                        
                        if json[phnx].intValue == 1 {
                            
                            let encriptedStr = json[lpg].stringValue
                            let decrptedStr = Secure().decryptPIN(text: encriptedStr)
                            let decriptedData = Data(decrptedStr.utf8)
                            let json:JSON = JSON(data: decriptedData)
                            
                            if self.isDisableApp(json: json) {
                               return
                            }
                            else {
                                if json == JSON.null {
                                    var jsonTemp = JSON()
                                    jsonTemp["STATUS_CODE"] = 1003
                                    jsonTemp["STATUS_MESSAGE"] = "Can't perform the requested action due to a temporary server error.\nPlease try again later"
                                    onCompletion(jsonTemp, error as NSError?)
                                }
                                else {
                                    onCompletion(json, nil)
                                }
                                
                            }
                        }
                        else {
                            
                            if self.isDisableApp(json: json) {
                                return
                            }
                            else {
                                onCompletion(json, nil)
                            }
                        }
                        
                    } else {
                        
                        var jsonTemp = JSON()
                        jsonTemp["STATUS_CODE"] = 1003
                        jsonTemp["STATUS_MESSAGE"] = "Can't perform the requested action due to a temporary server error.\nPlease try again later"
                        onCompletion(jsonTemp, error as NSError?)
                    }
                    
                    DispatchQueue.main.async {
                        loader.remove()
                    }
                })
                
                task.resume()
                
            } catch {
                
                // Create your personal error
                var jsonTemp = JSON()
                jsonTemp["STATUS_CODE"] = 1003
                jsonTemp["STATUS_MESSAGE"] = "Can't perform the requested action due to a temporary server error.\nPlease try again later"
                onCompletion(jsonTemp, error as NSError?)
                
            }
        }
        
        class func callAPI<T>(title: String, subTitle : String, param: [String : Any], type: T.Type, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
            
            let headers = ["content-type": "application/json"]
            let request = NSMutableURLRequest(url: URL(string: baseURL)!)
            request.allHTTPHeaderFields = headers
            request.httpMethod = "POST"
            let jsonBody = try! JSONSerialization.data(withJSONObject: param)
            request.httpBody = jsonBody
            
            //Add Loader
            let loader = RestAPIManager.addProgress(title: title, description: subTitle)
            URLSession.shared.dataTask(with: request as URLRequest) { (data, response, err) in
                DispatchQueue.main.async {
                    loader.remove()
                }
                
                if let data = data{
                    do {
                        let decoder = JSONDecoder()
                        let gitData = try decoder.decode(type, from: data)
                        completion(gitData)
                        
                    } catch let err {
                        print("Err", err)
                    }
                }else if let erro = err{
                    error(erro)
                    print(err?.localizedDescription ?? "Error")
                }
                
                }.resume()
        }
        
        
        class func endBackgroundTask() {
            print("Background task ended.")
            UIApplication.shared.endBackgroundTask(backgroundTask)
            backgroundTask = UIBackgroundTaskIdentifier(rawValue: 0)
        }
        
        ///////////////////////////////Alamofire///////////////////////////////
        
        class func isDisableApp(json : JSON) -> Bool {

            if json["STATUS_CODE"].intValue == 211 {
                
                DispatchQueue.main.async {
                    let visibleViewController = self.getVisibleViewController(nil)
                    visibleViewController?.showMessage(title: "Alert!", message: json["STATUS_MESSAGE"].stringValue)
                }
                return true
            }
            return false
        }
        
        class func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {
            
            var rootVC = rootViewController
            if rootVC == nil {
                rootVC = UIApplication.shared.keyWindow?.rootViewController
            }
            
            if rootVC?.presentedViewController == nil {
                return rootVC
            }
            
            if let presented = rootVC?.presentedViewController {
                if presented.isKind(of: UINavigationController.self) {
                    let navigationController = presented as! UINavigationController
                    return navigationController.viewControllers.last!
                }
                
                if presented.isKind(of: UITabBarController.self) {
                    let tabBarController = presented as! UITabBarController
                    return tabBarController.selectedViewController!
                }
                
                return getVisibleViewController(presented)
            }
            return nil
        }
        
        class func getTopMostViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
            if let nav = base as? UINavigationController {
                return getTopMostViewController(base: nav.visibleViewController)
            }
            if let tab = base as? UITabBarController {
                if let selected = tab.selectedViewController {
                    return getTopMostViewController(base: selected)
                }
            }
            if let presented = base?.presentedViewController {
                return getTopMostViewController(base: presented)
            }
            return base
        }
        
        class func addProgress(title: String, description : String) -> ProcessingBar {
            let parentVC = getTopMostViewController()
            let customPopup = ProcessingBar(frame: (parentVC?.view.frame)!)
            customPopup.showCustomPopup(title: title, description: description, parent_view: (parentVC?.view)!)
            parentVC?.view.addSubview(customPopup)
            return customPopup
        }
    }

