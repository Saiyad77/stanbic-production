//
//  RestAPIManager.swift
//  RestAPIJSONProject
//
//  Created by 5Exception-Mac2 on 22/07/16.
//  Copyright © 2016 5Exception-Mac2. All rights reserved.
//

// "https://beep2.cellulant.com:9001/MBServiceAPI/"

import UIKit
import Foundation


typealias ServiceResponse = (JSON, NSError?) -> Void

class RestAPIManager: NSObject {
     
     static let sharedInstance = RestAPIManager()
     static var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier(rawValue: 0)
     //https://beep2.cellulant.com:9001/MBServiceAPI/
     /////////**** BASE URL *****//////////
     static let baseURL = AppMode.appMode?.getBaseUrl() ?? ""
     static let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
     static let clientHash = "$P$B7lwP0IV3fdK4H3oAuttUZiKh3sc7c1"
     static let UDEVID = "0ed100cf-ad33-4953-b1ae-447ae4cc08d1"
     
     class func getuuidFromDb() -> String
     {
          var udidArray = CoreDataHelper().getDataForEntity(entity : Entity.DEVICEID)
          if udidArray.count > 0
          {
               let udid = udidArray[0]["aDEVICEID"] ?? ""
               return udid
          }
          else
          {
               CoreDataHelper().saveUUID(value: UIDevice.current.identifierForVendor!.uuidString, entityName: Entity.DEVICEID.rawValue, key: Entity.DEVICEID.rawValue, removeRecords: true)
               return UIDevice.current.identifierForVendor!.uuidString
          }
     }
     
     // PASS TOKEN :_ parseInstallationID
     
     // Get Validation Key
     class func get_validation_key<T>(title : String, subTitle : String, type: T.Type, mobileNo : String, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"              :   sCommand.GAK,
               "UPHONENUM"             :   mobileNo,
               "UDEVID"                :   getuuidFromDb(),
               "ORGID"                 :   sCommand.oRGID,
               "CUSTOMER_HASH"         :   getuuidFromDb(),
               "CLIENT_HASH"           :   clientHash,
               "osVersion"             :   UIDevice.current.systemVersion,
               "appVersion"            :   appVersion,
               "ORIGIN"                :   sCommand.oRIGIN,
               "parseInstallationID"   :   (UIApplication.shared.delegate as! AppDelegate).firebaseToken,
               "deviceName"            :   UIDevice.current.name,
               "HASH_KEY"              :   getuuidFromDb()] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     //  Validate Activation Key
     class func verify_validation_key<T>(title : String, subTitle : String, activationKey : String,type: T.Type, mobileNo : String, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"              :   sCommand.VAK,
               "UPHONENUM"             :   mobileNo,
               "UDEVID"                :   getuuidFromDb(),
               "ORGID"                 :   sCommand.oRGID,
               "CUSTOMER_HASH"         :   getuuidFromDb(),
               "CLIENT_HASH"           :   clientHash,
               "osVersion"             :   UIDevice.current.systemVersion,
               "appVersion"            :   appVersion,
               "ORIGIN"                :   sCommand.oRIGIN,
               "ACTIVATION_KEY"        :   activationKey,
               "parseInstallationID"   :   (UIApplication.shared.delegate as! AppDelegate).firebaseToken,
               "deviceName"            :   UIDevice.current.name,
               "devicePlatform"        :   "IOS"
               ] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     //Get details
     class func get_setQuestion(title : String, subTitle : String, action : String, answer : String,onCompletion: @escaping (JSON) -> Void) {
          
          let paramDict = [
               "SCOMMAND"      :   sCommand.SR,
               "UPHONENUM"     :   OnboardUser.mobileNo,
               "UDEVID"        :   getuuidFromDb(),
               "ORGID"         :   sCommand.oRGID,
               "CUSTOMER_HASH" :   getuuidFromDb(),
               "CLIENT_HASH"   :   clientHash,
               "osVersion"     :   UIDevice.current.systemVersion,
               "appVersion"    :   appVersion,
               "ORIGIN"        :   sCommand.oRIGIN,
               "ACTION"        :   action,
               "QUESTIONS"     :   answer
               ] as [String : Any]
          
          makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
               onCompletion(json as JSON)
          })
     }
     
     //Get details
     class func getAccountOpeningData(title : String, subTitle : String, action : String,token : String, answer : [[String : String]],onCompletion: @escaping (JSON) -> Void) {
          
          let paramDict = [
               "SCOMMAND"      :   sCommand.AO,
               "UPHONENUM"     :   OnboardUser.mobileNo,
               "UDEVID"        :   getuuidFromDb(),
               "ORGID"         :   sCommand.oRGID,
               "CUSTOMER_HASH" :   getuuidFromDb(),
               "CLIENT_HASH"   :   clientHash,
               "osVersion"     :   UIDevice.current.systemVersion,
               "appVersion"    :   appVersion,
               "ORIGIN"        :   sCommand.oRIGIN,
               "ACTION"        :   action,
               "TOKEN"         :   token,
               ] as [String : Any]
          
          makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
               onCompletion(json as JSON)
          })
     }
     
     //Get details
     class func validateData(title : String, subTitle : String, action : String, idNo : String, idType : String, images : [UIImage], onCompletion: @escaping (JSON) -> Void) {
          
          let paramDict = [
               "SCOMMAND"      :   sCommand.AO,
               "UPHONENUM"     :   OnboardUser.mobileNo,
               "UDEVID"        :   getuuidFromDb(),
               "ORGID"         :   "\(sCommand.oRGID)",
               "CUSTOMER_HASH" :   getuuidFromDb(),
               "CLIENT_HASH"   :   getuuidFromDb(),
               "osVersion"     :   UIDevice.current.systemVersion,
               "appVersion"    :   appVersion,
               "ORIGIN"        :   sCommand.oRIGIN,
               "ACTION"        :   action,
               "ID_NUMBER"     :   idNo,
               "ID_TYPE"       :   idType]
         var imageName: [String] = [String]()
          
          if global.imgCount == "1" {
               imageName = ["ID_FRONT"]
          } else {
               imageName = ["ID_FRONT","ID_BACK"]
          }
           var imgData = [Data]()
         //  var imgdataarr = [UIImage(named: "fingerprint")!, UIImage(named: "back.JPEG")!]
        //  imgdataarr.removeLast()
          for i in 0 ..< images.count {
               imgData.append((images[i].jpegData(compressionQuality: 0.50)!))
          }
         request(withImages: action, parameters: paramDict, imageKey: imageName, images: imgData, onCompletion: { json, err in
               onCompletion(json as JSON)
          })
//          let URL = "https://beep2.cellulant.com:9001/MBServiceAPI/multiPartUpload.php"
//          Alamofire.upload(multipartFormData: { multipartFormData in
//               // import image to request
//               for i in 0 ..< images.count {
//                    multipartFormData.append(imgData[i], withName: "\(imageName[i])", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
//               }
//               for (key, value) in paramDict {
//                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
//               }
//          }, to: URL,
//
//             encodingCompletion: { encodingResult in
//               switch encodingResult {
//               case .success(let upload, _, _):
//                    upload.responseJSON { response in
//
//                    }
//               case .failure(let error):
//                    print(error)
//               }
//
//          })
     }
          
     
     class func request(withImages action : String, parameters: [String:String]?,imageKey : [String], images:[Data], onCompletion: @escaping ServiceResponse) {
          // generate boundary string using a unique per-app string
          let boundary = (String.init(format: "Boundary-%@", NSUUID.init().uuidString))
          
          let str = baseURL.replacingOccurrences(of: "/beta.php", with: "", options: NSString.CompareOptions.literal, range: nil)
          let request = NSMutableURLRequest(url: URL(string: str+"multiPartUpload.php")!)
          request.httpMethod = "POST"
          request.timeoutInterval = 120 // In second
          request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
          let bodyData = try! createBodytemp(with: (parameters), imageKey: imageKey, images: images, boundary: boundary)
          
          callPostApi(body: bodyData, request: request, title: "title", subtitle: "subtitle", onCompletion: onCompletion)
          /*
           var encriptedInfo = [String : AnyObject]()
          // generate boundary string using a unique per-app string
          let boundary = (String.init(format: "Boundary-%@", NSUUID.init().uuidString))
//           let str = baseURL.replacingOccurrences(of: "/beta.php", with: "", options: NSString.CompareOptions.literal, range: nil)
//          let request = NSMutableURLRequest(url: URL(string: str+"multiPartUpload.php")!)
//          request.httpMethod = "POST"
//          request.timeoutInterval = 120 // In second
//          request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
          
           let bodyData = try! createBodytemp(with: (parameters), imageKey: imageKey, images: images, boundary: boundary)
          
          
          // let jsonData = try! JSONSerialization.data(withJSONObject: param, options: [])
          var decoded = String(data: bodyData, encoding: .utf8) ?? ""
          decoded = Secure().encryptPIN(text: decoded)
          let phnx = Variables().reveal(key: VariableConstants.phnx)
          let lpg = Variables().reveal(key: VariableConstants.lpg)
          encriptedInfo[phnx] = "1" as AnyObject
          encriptedInfo[lpg] = decoded as AnyObject
          let str = baseURL.replacingOccurrences(of: "/beta.php", with: "", options: NSString.CompareOptions.literal, range: nil)
          let request = NSMutableURLRequest(url: URL(string: str+"multiPartUpload.php")!)
          request.httpMethod = "POST"
          request.timeoutInterval = 120 // In second
          request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "content-type")
         
               // Set the POST body for the request
               let jsonBody = try JSONSerialization.data(withJSONObject: encriptedInfo, options: .prettyPrinted)
           callPostApi(body: jsonBody, request: request, title: "title", subtitle: "subtitle", onCompletion: onCompletion)
          
          */
           
     }
     
     class func createBodytemp(with parameters: [String: String]?, imageKey : [String] ,images: [Data], boundary: String) throws -> Data {
          
          let body = NSMutableData()
          let mimetype = "image/jpeg"
          if parameters != nil {
               for (key, value) in parameters! {
                    body.appendString("--\(boundary)\r\n")
                    body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                    body.appendString("\(value)\r\n")
                  
               }
          }
          
          for i in 0 ..< imageKey.count {
               
               //set file name as timestamp so file name will unique all the time
               let fileName = "\(Int64(Date().timeIntervalSince1970 * 1000)).jpg"
               
               body.appendString("--\(boundary)\r\n")
               body.appendString("Content-Disposition: form-data; name=\"\(imageKey[i])\"; filename=\"\(fileName)\"\r\n")
               body.appendString("Content-Type: \(mimetype)\r\n\r\n")
               body.append(images[i])
               body.appendString("\r\n")
          }
          body.appendString("--\(boundary)--\r\n")
          return body as Data
     }
     
     //Personal detail validation
     class func validatePersonalDetail(title : String, subTitle : String, action : String,idNumber : String,token : String,branch : String,country : String,postalcode : String,postaladdress : String, images : [UIImage], onCompletion: @escaping (JSON) -> Void) {

          let paramDict = [
               "SCOMMAND"      :   sCommand.AO,
               "UPHONENUM"     :   OnboardUser.mobileNo,
               "UDEVID"        :   getuuidFromDb(),
               "ORGID"         :   "\(sCommand.oRGID)",
               "CUSTOMER_HASH" :   getuuidFromDb(),
               "CLIENT_HASH"   :   clientHash,
               "osVersion"     :   UIDevice.current.systemVersion,
               "appVersion"    :   appVersion,
               "ORIGIN"        :   sCommand.oRIGIN,
               "ACTION"        :   action,
               "KRA_PIN"       :   OnboardUser.userKraPIN,
               "EMAIL"         :   OnboardUser.userEmail,
               "ID_NUMBER"     :   idNumber,
               "TOKEN"         :   token,
               "RESIDENCE"     :   action,
               "BRANCH"        :   branch,
               "COUNTRY"       :   country,
               "POSTAL_CODE"   :   postalcode,
               "POSTAL_ADDRESS":   postaladdress] as [String : Any]

          let imageName = ["SELFIE"]
          var imgData = [Data]()

          for i in 0 ..< images.count {

               imgData.append((images[i].jpegData(compressionQuality: 0.1)!))
          }
          
                         


          request(withImages: action, parameters: paramDict as? [String : String], imageKey: imageName, images: imgData, onCompletion: { json, err in
               onCompletion(json as JSON)
          })
     }
     
     //Personal detail validation
//          class func validatePersonalDetail(title : String, subTitle : String, action : String,idNumber : String,token : String,branch : String,country : String,postalcode : String,postaladdress : String, images : [UIImage], onCompletion: @escaping (JSON) -> Void) {
//
//               let compressData = images[0].jpegData(compressionQuality: 0.75)
//               let compressedImage = UIImage(data: compressData!)
//
//               let imageData = compressedImage!.pngData()!
//               let img =  imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
//
//               let paramDict = [
//                    "SCOMMAND"      :   sCommand.AO,
//                    "UPHONENUM"     :   OnboardUser.mobileNo,
//                    "UDEVID"        :   getuuidFromDb(),
//                    "ORGID"         :   "\(sCommand.oRGID)",
//                    "CUSTOMER_HASH" :   getuuidFromDb(),
//                    "CLIENT_HASH"   :   clientHash,
//                    "osVersion"     :   UIDevice.current.systemVersion,
//                    "appVersion"    :   appVersion,
//                    "ORIGIN"        :   sCommand.oRIGIN,
//                    "ACTION"        :   action,
//                    "KRA_PIN"       :   OnboardUser.userKraPIN,
//                    "EMAIL"         :   OnboardUser.userEmail,
//                    "ID_NUMBER"     :   idNumber,
//                    "TOKEN"         :   token,
//                    "RESIDENCE"     :   action,
//                    "BRANCH"        :   branch,
//                    "COUNTRY"       :   country,
//                    "POSTAL_CODE"   :   postalcode,
//                    "POSTAL_ADDRESS":   postaladdress,
//                    "SELFIE"        :   img] as [String : Any]
//
//
//
//               makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
//                    onCompletion(json as JSON)
//               })
//          }

     
     //ChangePin
     class func change_Pin<T>(title : String, subTitle : String,type: T.Type, pin1 : String, pin2 : String, accountAlias: String,uAuthPin: String, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          let paramDict = [
               "SCOMMAND"      :   sCommand.CP,
               "UPHONENUM"     :   OnboardUser.mobileNo,
               "UDEVID"        :   getuuidFromDb(),
               "ORGID"         :   sCommand.oRGID,
               "CUSTOMER_HASH" :   getuuidFromDb(),
               "CLIENT_HASH"   :   clientHash,
               "osVersion"     :   UIDevice.current.systemVersion,
               "appVersion"    :   appVersion,
               "ORIGIN"        :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS" : accountAlias,
               "UAUTHPIN" : uAuthPin,
               "PIN1" : pin1,
               "PIN2" : pin2
               ] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }


     
     //Occupational & next kra pin submit data
     class func submitOccupationl_KraDetail(title : String, subTitle : String, action : String, dataDict : [String : Any], onCompletion: @escaping (JSON) -> Void)  {
          
          var paramDict = dataDict
          paramDict["SCOMMAND"]       = sCommand.AO
          paramDict["UPHONENUM"]      = OnboardUser.mobileNo
          paramDict["UDEVID"]         = getuuidFromDb()
          paramDict["ORGID"]          = sCommand.oRGID
          paramDict["CUSTOMER_HASH"]  = getuuidFromDb()
          paramDict["CLIENT_HASH"]    = clientHash
          paramDict["osVersion"]      = UIDevice.current.systemVersion
          paramDict["appVersion"]     = appVersion
          paramDict["ORIGIN"]         = sCommand.oRIGIN
          paramDict["ACTION"]         = action
          paramDict["TOKEN"]         = global.token
         // paramDict["SIGNATURE"]     = global.imgsignature
          
          var imgData = [Data]()
          imgData.append((global.imgsignature.jpegData(compressionQuality: 0.50)!))
          
          request(withImages: action, parameters: paramDict as? [String : String], imageKey: ["SIGNATURE"], images: imgData, onCompletion: { json, err in
               onCompletion(json as JSON)
          })
          
       }

          
//          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
//               completion(response)
//          }) { (err) in
//               error(err)
//          }
//     }
     
     //Get Validation Key
     class func validatePIN<T>(title : String, subTitle : String, pin : String,type: T.Type, mobileNo : String, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"      :   sCommand.VPIN,
               "UPHONENUM"     :   mobileNo,
               "UDEVID"        :   getuuidFromDb(),
               "ORGID"         :   sCommand.oRGID,
               "CUSTOMER_HASH" :   getuuidFromDb(),
               "CLIENT_HASH"   :   clientHash,
               "osVersion"     :   UIDevice.current.systemVersion,
               "appVersion"    :   appVersion,
               "ORIGIN"        :   sCommand.oRIGIN,
               "UAUTHPIN"      :   Secure().encryptPIN(text: pin),
               ] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     //Check Updates/Fetch System Settings
     class func checkUpdatesFetchSystemSettings(title : String, subTitle : String, onCompletion: @escaping (JSON) -> Void) {
          
          let paramDict = [
               "SCOMMAND"      :   sCommand.CUP,
               "UPHONENUM"     :   OnboardUser.mobileNo,
               "UDEVID"        :   getuuidFromDb(),
               "ORGID"         :   sCommand.oRGID,
               "CUSTOMER_HASH" :   getuuidFromDb(),
               "CLIENT_HASH"   :   clientHash,
               "osVersion"     :   UIDevice.current.systemVersion,
               "appVersion"    :   appVersion,
               "ORIGIN"        :   sCommand.oRIGIN,
               "ALL_FLAG"      :   "1"
               ] as [String : Any]
          
         makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
               onCompletion(json as JSON)
          })
          
     }
     
     //CFetch Terms and conditions/privacy policy
     class func fetchtermsandcounditions(title : String, subTitle : String, onCompletion: @escaping (JSON) -> Void) {
          
          let paramDict = [
               "SCOMMAND"      :   sCommand.FTC,
               "UDEVID"        :   getuuidFromDb(),
               "ORGID"         :   sCommand.oRGID,
               "CUSTOMER_HASH" :   getuuidFromDb(),
               "CLIENT_HASH"   :   clientHash,
               "osVersion"     :   UIDevice.current.systemVersion,
               "appVersion"    :   appVersion] as [String : Any]
          
          makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
               onCompletion(json as JSON)
          })
     }
     
     // balanceEnquiryFor
     class func balanceEnquiryFor(title : String, subTitle : String, accountAlise : String,onCompletion: @escaping (JSON) -> Void) {
          
          let paramDict = [
               "SCOMMAND"      :   sCommand.BE,
               "UPHONENUM"     :   OnboardUser.mobileNo,
               "UDEVID"        :   getuuidFromDb(),
               "ORGID"         :   sCommand.oRGID,
               "CUSTOMER_HASH" :   getuuidFromDb(),
               "CLIENT_HASH"   :   clientHash,
               "osVersion"     :   UIDevice.current.systemVersion,
               "appVersion"    :   appVersion,
               "ORIGIN"        :   sCommand.oRIGIN,
               "UAUTHPIN"      :   OnboardUser.pin,
               "ALLFLAG"       :   "0",
               "ACCOUNT_ALIAS" :   accountAlise,
               ] as [String : Any]
          
          makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
               onCompletion(json as JSON)
          })
     }
     
     //Fetch bill
     class func fetchBill<T>(title : String, subTitle : String,type: T.Type, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"       :   sCommand.FB,
               "UPHONENUM"      :   OnboardUser.mobileNo,
               "UDEVID"         :   getuuidFromDb(),
               "ORGID"          :   sCommand.oRGID,
               "CUSTOMER_HASH"  :   getuuidFromDb(),
               "CLIENT_HASH"    :   clientHash,
               "osVersion"      :   UIDevice.current.systemVersion,
               "appVersion"     :   appVersion,
               "ORIGIN"         :   sCommand.oRIGIN] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     //Buy Airtime
     class func buyAirtime<T>(title : String, subTitle : String, pin : String,type: T.Type, mobileNo : String,accountAlias : String,amount : String,recipetno : String,mnoWalletId : String,nominate : String,reciptAlias : String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"       :   sCommand.BAO,
               "UPHONENUM"      :   mobileNo,
               "UDEVID"         :   getuuidFromDb(),
               "ORGID"          :   sCommand.oRGID,
               "CUSTOMER_HASH"  :   getuuidFromDb(),
               "CLIENT_HASH"    :   clientHash,
               "osVersion"      :   UIDevice.current.systemVersion,
               "appVersion"     :   appVersion,
               "ORIGIN"         :   sCommand.oRIGIN,
               "UAUTHPIN"       :   pin,
               "ACCOUNT_ALIAS"  :   accountAlias,
               "AMOUNT"         :   amount,
               "RECIPIENT_NO"   :   recipetno,
               "MNO_WALLET_ID"  :   mnoWalletId,
               "NOMINATE_FLAG"  :   nominate,
               "RECIPIENT_ALIAS":   reciptAlias,
               ] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     // Full Statement
     class func fullStatement<T>(title : String, subTitle : String, pin : String,type: T.Type, mobileNo : String,accountAlias : String,startDate : String,endDate : String,deliveryMode : String,email : String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"       :   sCommand.FSR,
               "UPHONENUM"      :   mobileNo,
               "UDEVID"         :   UDEVID,
               "ORGID"          :   sCommand.oRGID,
               "CUSTOMER_HASH"  :   getuuidFromDb(),
               "CLIENT_HASH"    :   clientHash,
               "osVersion"      :   UIDevice.current.systemVersion,
               "appVersion"     :   appVersion,
               "ORIGIN"         :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"  :   accountAlias,
               "UAUTHPIN"       :   pin,
               "START_DATE"     :   startDate,
               "END_DATE"       :   endDate,
               "DELIVERY_MODE"  :   deliveryMode,
               "EMAIL"          :   email,
               ] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     // Mini Statement
     
     class func miniStatement<T>(title : String, subTitle : String, pin : String,type: T.Type, mobileNo : String,accountAlias : String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"       :   sCommand.MSR,
               "UPHONENUM"      :   mobileNo,
               "UDEVID"         :   UDEVID,
               "ORGID"          :   sCommand.oRGID,
               "CUSTOMER_HASH"  :   getuuidFromDb(),
               "CLIENT_HASH"    :   clientHash,
               "osVersion"      :   UIDevice.current.systemVersion,
               "appVersion"     :   appVersion,
               "ORIGIN"         :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"  :   accountAlias,
               "UAUTHPIN"       :  pin] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     // Send to Mobile Money
     class func sendtomobilemoney<T>(title : String, subTitle : String, pin : String,type: T.Type, mobileNo : String,accountAlias : String,amount : String,mnowalletid : String,recipientno :String,recipientalias :String,nominateflag :String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"       :   sCommand.SM,
               "UPHONENUM"      :   mobileNo,
               "UDEVID"         :   UDEVID,
               "ORGID"          :   sCommand.oRGID,
               "CUSTOMER_HASH"  :   getuuidFromDb(),
               "CLIENT_HASH"    :   clientHash,
               "osVersion"      :   UIDevice.current.systemVersion,
               "appVersion"     :   appVersion,
               "ORIGIN"         :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"  :   accountAlias,
               "UAUTHPIN"       :   pin,
               "AMOUNT"         :   amount,
               "MNO_WALLET_ID"  :   mnowalletid,
               "RECIPIENT_ALIAS":   recipientalias,
               "RECIPIENT_NO"   :   recipientno,
               "NOMINATE_FLAG"   :   nominateflag] as [String : Any]
          
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     //  Mpesa STK Push
     class func mPesaToaccount<T>(title : String, subTitle : String, pin : String,type: T.Type, mobileNo : String,accountAlias : String,amount : String,accountnumber:String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"       :   sCommand.STKTOPUP,
               "UPHONENUM"      :   mobileNo,
               "UDEVID"         :   UDEVID,
               "ORGID"          :   sCommand.oRGID,
               "CUSTOMER_HASH"  :   getuuidFromDb(),
               "CLIENT_HASH"    :   clientHash,
               "osVersion"      :   UIDevice.current.systemVersion,
               "appVersion"     :   appVersion,
               "ORIGIN"         :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"  :   accountAlias,
               "UAUTHPIN"       :   pin,
               "AMOUNT"         :   amount,
               "ACCOUNT_NUMBER" :   accountnumber] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     // QueryBill
     class func querybill<T>(title : String, subTitle : String, billerrefrence : String,type: T.Type, mobileNo : String,biller : String,hubserviceid : String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"       :   sCommand.QBILL,
               "UPHONENUM"      :   mobileNo,
               "UDEVID"         :   UDEVID,
               "ORGID"          :   sCommand.oRGID,
               "CUSTOMER_HASH"  :   getuuidFromDb(),
               "CLIENT_HASH"    :   clientHash,
               "osVersion"      :   UIDevice.current.systemVersion,
               "appVersion"     :   appVersion,
               "ORIGIN"         :   sCommand.oRIGIN,
               "BILLER_REFERENCE":  billerrefrence,
               "BILLER"         :   biller,
               "HUB_SERVICEID"  :   hubserviceid,
               ] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     // paybill
     class func paybill<T>(title : String, subTitle : String, billerrefrence : String,type: T.Type, mobileNo : String,accountalias : String,amount : String,pin : String,biller:String,enrollbillrefrence:String,enrollmenalias:String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"           :   sCommand.PB,
               "UPHONENUM"          :   mobileNo,
               "UDEVID"             :   UDEVID,
               "ORGID"              :   sCommand.oRGID,
               "CUSTOMER_HASH"      :   getuuidFromDb(),
               "CLIENT_HASH"        :   clientHash,
               "osVersion"          :   UIDevice.current.systemVersion,
               "appVersion"         :   appVersion,
               "ORIGIN"             :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"      :   accountalias,
               "UAUTHPIN"           :   pin,
               "AMOUNT"             :   amount,
               "BILLER_REFERENCE"   :   billerrefrence,
               "BILLER"             :   biller,
               "ENROLL_BILL_REFERENCE": enrollbillrefrence,
               "ENROLLMENT_ALIAS"     : enrollmenalias] as [String : Any]
          
//          let paramDict = [
//               "SCOMMAND"           :   sCommand.PB,
//               "UPHONENUM"          :   mobileNo,
//               "UDEVID"             :   UDEVID,
//               "ORGID"              :   sCommand.oRGID,
//               "CUSTOMER_HASH"      :   getuuidFromDb(),
//               "CLIENT_HASH"        :   clientHash,
//               "osVersion"          :   UIDevice.current.systemVersion,
//               "appVersion"         :   appVersion,
//               "ORIGIN"             :   sCommand.oRIGIN,
//               "ACCOUNT_ALIAS"      :   accountalias,
//               "UAUTHPIN"           :   pin,
//               "AMOUNT"             :   amount,
//               "BILLER_REFERENCE"   :   billerrefrence,
//               "BILLER"             :   biller
//               ] as [String : Any]
          
     
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     // Fetch RTGS Time
     class func checkRTGSTransfer<T>(title : String, subTitle : String,type: T.Type, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict =  [
               "SCOMMAND"              :   sCommand.FST,
               "UPHONENUM"             :   OnboardUser.mobileNo,
               "UDEVID"                :   getuuidFromDb(),
               "ORGID"                 :   sCommand.oRGID,
               "CUSTOMER_HASH"         :   getuuidFromDb(),
               "CLIENT_HASH"           :   clientHash,
               "osVersion"             :   UIDevice.current.systemVersion,
               "appVersion"            :   appVersion,
               "ORIGIN"                :   sCommand.oRIGIN
               ] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     
     // Internal Fund Transfer
     class func Internalfuntranfer<T>(title : String, subTitle : String, scommand : String, acalias : String,type: T.Type, mobileNo : String,pin : String,amount : String,recieptacc : String,recieptBankbranch : String,nominate : String,recieptalias : String,reciptBank : String,recieptacname : String, narration : String, address : String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          var paramDict = [
               "SCOMMAND"       :   scommand,
               "UPHONENUM"      :   mobileNo,
               "UDEVID"         :   UDEVID,
               "ORGID"          :   sCommand.oRGID,
               "CUSTOMER_HASH"  :   getuuidFromDb(),
               "CLIENT_HASH"    :   clientHash,
               "osVersion"      :   UIDevice.current.systemVersion,
               "appVersion"     :   appVersion,
               "ORIGIN"         :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"  :   acalias,
               "UAUTHPIN"       :   pin,
               "AMOUNT"         :   amount,
               "RECIPIENT_ACC"  :   recieptacc,
               "RECIPIENT_ACC_NAME": recieptacname,
               "NOMINATE_FLAG"   : nominate,
               "RECIPIENT_ALIAS" : recieptalias,
               "RECIPIENT_BANK"  : reciptBank,
               "NARRATION"       : narration,
               ] as [String : Any]
          
          if scommand == sCommand.TF { // Internal address
               paramDict["RECIPIENT_BANK_BRANCH"] = recieptacname
          }else{
             paramDict["BENEFICIARY_ADDRESS"] = address
          }
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     // RTGS Fund Transfer
     class func rtgs<T>(title : String, subTitle : String, acalias : String,type: T.Type, mobileNo : String,pin : String,amount : String,recieptacc : String,recieptBankbranch : String,nominate : String,recieptalias : String,reciptBank : String,recieptacname : String,narration:String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"       :   sCommand.RTGS,
               "UPHONENUM"      :   mobileNo,
               "UDEVID"         :   UDEVID,
               "ORGID"          :   sCommand.oRGID,
               "CUSTOMER_HASH"  :   getuuidFromDb(),
               "CLIENT_HASH"    :   clientHash,
               "osVersion"      :   UIDevice.current.systemVersion,
               "appVersion"     :   appVersion,
               "ORIGIN"         :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"  :  acalias,
               "UAUTHPIN"       :   pin,
               "AMOUNT"         :   amount,
               "RECIPIENT_ACC"  :   recieptacc,
               "RECIPIENT_BANK_BRANCH":  recieptBankbranch,
               "NOMINATE_FLAG"     :   nominate,
               "RECIPIENT_ALIAS"   :   recieptalias,
               "RECIPIENT_BANK"    :  reciptBank,
               "RECIPIENT_ACC_NAME":recieptacname,
               "NARRATION":narration
               ] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     
     // Pesalink - Send to Account
     class func sendToAccount<T>(title : String, subTitle : String, acalias : String,type: T.Type, mobileNo : String,pin : String,amount : String,recieptacnumber : String,nominatealias : String,bank : String,narration:String,nominate:String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"             :   sCommand.PAYAC,
               "UPHONENUM"            :   mobileNo,
               "UDEVID"               :   UDEVID,
               "ORGID"                :   sCommand.oRGID,
               "CUSTOMER_HASH"        :   getuuidFromDb(),
               "CLIENT_HASH"          :   clientHash,
               "osVersion"            :   UIDevice.current.systemVersion,
               "appVersion"           :   appVersion,
               "ORIGIN"               :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"        :   acalias,
               "UAUTHPIN"             :   pin,
               "AMOUNT"               :   amount,
               "NOMINATION_ALIAS"     :   nominatealias,
               "BANK"                 :   bank,
               "NOMINATE"             :   nominate,
               "CURRENCY"             :   "KES",
               "RECIPIENT_ACCOUNT_NUMBER" :   recieptacnumber,
               "NARRATION"            :   narration,
               ] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     
     // Pesalink - Send to CARD
     class func sendToCard<T>(title : String, subTitle : String, acalias : String,type: T.Type, mobileNo : String,pin : String,amount : String,nominate : String,cardnumber : String,narration:String,nominatealias:String,bank:String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"             :   sCommand.PAYC,
               "UPHONENUM"            :   mobileNo,
               "UDEVID"               :   UDEVID,
               "ORGID"                :   sCommand.oRGID,
               "CUSTOMER_HASH"        :   getuuidFromDb(),
               "CLIENT_HASH"          :   clientHash,
               "osVersion"            :   UIDevice.current.systemVersion,
               "appVersion"           :   appVersion,
               "ORIGIN"               :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"        :   acalias,
               "UAUTHPIN"             :   pin,
               "AMOUNT"               :   amount,
               "NOMINATION_ALIAS"     :   nominatealias,
               "CARD_NUMBER"          :   cardnumber,
               "NOMINATE"             :   nominate,
               "CURRENCY"             :   "KES",
               "NARRATION"            :   narration,
               "BANK"                 :   bank] as [String : Any]
          
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     // Pesalink - Check Registration
     class func checkRegistration<T>(title : String, subTitle : String,type: T.Type, mobileNo : String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"             :   sCommand.CSR,
               "UPHONENUM"            :   mobileNo,
               "UDEVID"               :   UDEVID,
               "ORGID"                :   sCommand.oRGID,
               "CUSTOMER_HASH"        :   getuuidFromDb(),
               "CLIENT_HASH"          :   clientHash,
               "osVersion"            :   UIDevice.current.systemVersion,
               "appVersion"           :   appVersion,
               "ORIGIN"               :   sCommand.oRIGIN] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     //Pesalink - Unlink/Link/Primary Account
     class func linkAccount<T>(title : String, subTitle : String,type: T.Type, mobileNo : String, accountnumber : String,accountalias : String,pin : String,registerkits : String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"             :   sCommand.KREG,
               "UPHONENUM"            :   mobileNo,
               "UDEVID"               :   UDEVID,
               "ORGID"                :   sCommand.oRGID,
               "CUSTOMER_HASH"        :   getuuidFromDb(),
               "CLIENT_HASH"          :   clientHash,
               "osVersion"            :   UIDevice.current.systemVersion,
               "appVersion"           :   appVersion,
               "ORIGIN"               :   sCommand.oRIGIN,
               "ACCOUNT_NUMBER"       :   accountnumber,
               "ACCOUNT_ALIAS"        :   accountalias,
               "UAUTHPIN"             :   pin,
               "REGISTER_KITS"        :   registerkits] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     // Pesalink - Send to PHONE
     class func sendToPhone<T>(title : String, subTitle : String, acalias : String,type: T.Type, mobileNo : String,pin : String,amount : String,recieptbank : String,nominate : String,banksortcode : String,narration:String,nominatealias:String,receipentmobnumber:String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"             :   sCommand.PAYM,
               "UPHONENUM"            :   mobileNo,
               "UDEVID"               :   UDEVID,
               "ORGID"                :   sCommand.oRGID,
               "CUSTOMER_HASH"        :   getuuidFromDb(),
               "CLIENT_HASH"          :   clientHash,
               "osVersion"            :   UIDevice.current.systemVersion,
               "appVersion"           :   appVersion,
               "ORIGIN"               :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"        :   acalias,
               "UAUTHPIN"             :   pin,
               "AMOUNT"               :   amount,
               "NOMINATION_ALIAS"     :   nominatealias,
               "BANK_SORT_CODE"       :   banksortcode,
               "NOMINATE"             :   nominate,
               "CURRENCY"             :   "KES",
               "RECIPIENT_BANK"       :   recieptbank,
               "NARRATION"            :   narration,
               "RECIPIENT_MOBILE_NUMBER" : receipentmobnumber] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     // Pesalink - Phone/Pay Bill Lookup
     class func phoneLookup<T>(title : String, subTitle : String,type: T.Type, mobileNo : String,recipientmob : String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"             :   sCommand.FKITSB,
               "UPHONENUM"            :   mobileNo,
               "UDEVID"               :   UDEVID,
               "ORGID"                :   sCommand.oRGID,
               "CUSTOMER_HASH"        :   getuuidFromDb(),
               "CLIENT_HASH"          :   clientHash,
               "osVersion"            :   UIDevice.current.systemVersion,
               "appVersion"           :   appVersion,
               "ORIGIN"               :   sCommand.oRIGIN,
               "RECIPIENT_MOBILE_NUMBER": recipientmob
               ] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     // Cheque Book Request
     class func chequeBookRequest<T>(title : String, subTitle : String,type: T.Type, mobileNo : String,accountalias : String,pin : String,leaves : String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"             :   sCommand.RCB,
               "UPHONENUM"            :   mobileNo,
               "UDEVID"               :   UDEVID,
               "ORGID"                :   sCommand.oRGID,
               "CUSTOMER_HASH"        :   getuuidFromDb(),
               "CLIENT_HASH"          :   clientHash,
               "osVersion"            :   UIDevice.current.systemVersion,
               "appVersion"           :   appVersion,
               "ORIGIN"               :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS": accountalias,
               "UAUTHPIN": pin,
               "LEAVES": leaves,
               ] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     // Forex Rates
     class func forexRates(title : String, subTitle : String,mobileNo : String,accountalias : String,pin : String,onCompletion: @escaping (JSON) -> Void) {
          
          let paramDict = [
               "SCOMMAND"             :   sCommand.FR,
               "UPHONENUM"            :   mobileNo,
               "UDEVID"               :   UDEVID,
               "ORGID"                :   sCommand.oRGID,
               "CUSTOMER_HASH"        :   getuuidFromDb(),
               "CLIENT_HASH"          :   clientHash,
               "osVersion"            :   UIDevice.current.systemVersion,
               "appVersion"           :   appVersion,
               "ORIGIN"               :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"        :   accountalias,
               "UAUTHPIN"             :   pin] as [String : Any]
          
          makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
               onCompletion(json as JSON)
          })
     }
     
     // Salary Advance _ FETCH
     class func salaryAdvance<T>(title : String, subTitle : String,type: T.Type, mobileNo : String,accountalias : String,pin : String,action : String,amount : String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"             :   sCommand.SA,
               "UPHONENUM"            :   mobileNo,
               "UDEVID"               :   UDEVID,
               "ORGID"                :   sCommand.oRGID,
               "CUSTOMER_HASH"        :   getuuidFromDb(),
               "CLIENT_HASH"          :   clientHash,
               "osVersion"            :   UIDevice.current.systemVersion,
               "appVersion"           :   appVersion,
               "ORIGIN"               :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"        :   accountalias,
               "UAUTHPIN"             :   pin,
               "ACTION"               :   action,
               "AMOUNT"               :   amount] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     // Salary Advance _ SUBMIT
     class func salaryAdvanceSubmit<T>(title : String, subTitle : String,type: T.Type, mobileNo : String,accountalias : String,pin : String,action : String,amount : String,fname : String,lname : String,IDtype : String,idnumber : String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"             :   sCommand.SA,
               "UPHONENUM"            :   mobileNo,
               "UDEVID"               :   UDEVID,
               "ORGID"                :   sCommand.oRGID,
               "CUSTOMER_HASH"        :   getuuidFromDb(),
               "CLIENT_HASH"          :   clientHash,
               "osVersion"            :   UIDevice.current.systemVersion,
               "appVersion"           :   appVersion,
               "ORIGIN"               :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"        :   accountalias,
               "UAUTHPIN"             :   pin,
               "ACTION"               :   action,
               "AMOUNT"               :   amount,
               "FIRST_NAME"           :   fname,
               "LAST_NAME"            :   lname,
               "ID_TYPE"              :   IDtype,
               "ID_NUMBER"            :   idnumber] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     // Fetch ATMs & Branches
     class func fetchMapdata(title : String, subTitle : String,mobileNo : String,branchflag : String,longitude : String,latitude : String,onCompletion: @escaping (JSON) -> Void) {
          
          let paramDict = [
               "SCOMMAND"             :   sCommand.BAL,
               "UPHONENUM"            :   mobileNo,
               "UDEVID"               :   UDEVID,
               "ORGID"                :   sCommand.oRGID,
               "CUSTOMER_HASH"        :   getuuidFromDb(),
               "CLIENT_HASH"          :   clientHash,
               "osVersion"            :   UIDevice.current.systemVersion,
               "appVersion"           :   appVersion,
               "ORIGIN"               :   sCommand.oRIGIN,
               "ATM_BRANCH_FLAG"      :   branchflag,
               "LONGITUDE"            :   longitude,
               "LATITUDE"             :   latitude] as [String : Any]
          
          makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
               onCompletion(json as JSON)
          })
     }
     
     //   Beneficiary Management
     class func beneficiaryManagement<T>(title : String, subTitle : String,type: T.Type, mobileNo : String,pin : String,action : String,benificillyactype : String,reciepentalias : String,acnumber : String,profileid:String,newRecipetAalias:String,newaccountno:String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"                 :   sCommand.MBEF,
               "UPHONENUM"                :   mobileNo,
               "UDEVID"                   :   UDEVID,
               "ORGID"                    :   sCommand.oRGID,
               "CUSTOMER_HASH"            :   getuuidFromDb(),
               "CLIENT_HASH"              :   clientHash,
               "osVersion"                :   UIDevice.current.systemVersion,
               "appVersion"               :   appVersion,
               "ORIGIN"                   :   sCommand.oRIGIN,
               "UAUTHPIN"                 :   pin,
               "BENEFICIARY_ACCOUNT_TYPE" :   benificillyactype,
               "ACTION"                   :   action,
               "RECIPIENT_ALIAS"          :   reciepentalias,
               "ACCOUNT_NUMBER"           :   acnumber,
               "PROFILE_ID"               :   profileid,
               "NEW_RECIPIENT_ALIAS"      :   newRecipetAalias,
               "NEW_ACCOUNT_NUMBER"       :   newaccountno] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     //Loan Calculator
     class func loanCalculator(title : String, subTitle : String,mobileNo : String, amount : Int,rate: Int,duration: Int,LOAN_TYPE:String,onCompletion: @escaping (JSON) -> Void) {
          
          
          let paramDict = [
               "SCOMMAND" : sCommand.LCT,
               "UPHONENUM" : mobileNo,
               "UDEVID" : UDEVID,
               "ORGID" : sCommand.oRGID,
               "CUSTOMER_HASH" : getuuidFromDb(),
               "CLIENT_HASH" : clientHash,
               "osVersion" : UIDevice.current.systemVersion,
               "appVersion" : appVersion,
               "ORIGIN" : sCommand.oRIGIN,
               "AMOUNT" : amount,
               "RATE" : rate,
               "DURATION" : duration,
               "LOAN_TYPE" : LOAN_TYPE
               ] as [String : Any]
          
          makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
               onCompletion(json as JSON)
          })
     }
     
     //Loan Enquiries
     class func loanEnquiries(title : String, subTitle : String,mobileNo : String,LOAN_TYPE:String,onCompletion: @escaping (JSON) -> Void) {
          
          
          let paramDict = [
               "SCOMMAND" : sCommand.LEQ,
               "UPHONENUM" : mobileNo,
               "UDEVID" : UDEVID,
               "ORGID" : sCommand.oRGID,
               "CUSTOMER_HASH" : getuuidFromDb(),
               "CLIENT_HASH" : clientHash,
               "osVersion" : UIDevice.current.systemVersion,
               "appVersion" : appVersion,
               "ORIGIN" : sCommand.oRIGIN,
               "LOAN_TYPE" : LOAN_TYPE
               ] as [String : Any]
          
          makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
               onCompletion(json as JSON)
          })
     }
     
     //MARK: Buy Goods
     class func buyGoods<T>(title : String, subTitle : String,type: T.Type, mobileNo : String,accountalias : String,pin : String,amount : String,tillNumber: String,enrollAccount: String,enrollAlias: String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"             :   sCommand.ACCTILL,
               "UPHONENUM"            :   mobileNo,
               "UDEVID"               :   UDEVID,
               "ORGID"                :   sCommand.oRGID,
               "CUSTOMER_HASH"        :   getuuidFromDb(),
               "CLIENT_HASH"          :   clientHash,
               "osVersion"            :   UIDevice.current.systemVersion,
               "appVersion"           :   appVersion,
               "ORIGIN"               :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"        :   accountalias,
               "UAUTHPIN"             :   pin,
               "AMOUNT"               :   amount,
               "TILL_NUMBER"  : tillNumber,
               "ENROLL_ACCOUNT" : enrollAccount,
               "ENROLLMENT_ALIAS": enrollAlias] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     //MARK: Pay Bill Transaction
     class func payBillTransact<T>(title : String, subTitle : String,type: T.Type, mobileNo : String,accountalias : String,pin : String,amount : String,paybillNumber: String,paybillAccount: String,enrollAccount: String,enrollAlias: String,completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          
          let paramDict = [
               "SCOMMAND"             :   sCommand.ACCBILL,
               "UPHONENUM"            :   mobileNo,
               "UDEVID"               :   UDEVID,
               "ORGID"                :   sCommand.oRGID,
               "CUSTOMER_HASH"        :   getuuidFromDb(),
               "CLIENT_HASH"          :   clientHash,
               "osVersion"            :   UIDevice.current.systemVersion,
               "appVersion"           :   appVersion,
               "ORIGIN"               :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS"        :   accountalias,
               "UAUTHPIN"             :   pin,
               "AMOUNT"               :   amount,
               "PAYBILL_NUMBER"  : paybillNumber,
               "PAYBILL_ACCOUNT" : paybillAccount,
               "ENROLL_ACCOUNT" :  enrollAccount,
               "ENROLLMENT_ALIAS" : enrollAlias] as [String : Any]
          
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     //MARK: Self Registration
     class func checkRegistraionStatus(title : String, subTitle : String,mobileNo : String,onCompletion: @escaping (JSON) -> Void) {
          
          let paramDict = [
               "SCOMMAND"             :   sCommand.REG,
               "UPHONENUM"            :   mobileNo,
               "UDEVID"               :   UDEVID,
               "ORGID"                :   sCommand.oRGID,
               "CUSTOMER_HASH"        :   getuuidFromDb(),
               "CLIENT_HASH"          :   clientHash,
               "osVersion"            :   UIDevice.current.systemVersion,
               "appVersion"           :   appVersion,
               "ORIGIN"               :   sCommand.oRIGIN] as [String : Any]
          
          makeHTTPPOSTRequest(baseURL, body: paramDict as [String : AnyObject], title: title, subtitle: subTitle, onCompletion: { json, err in
               onCompletion(json as JSON)
          })
     }
     
     // Self Registration---> Change New Pin
     class func setNewPin<T>(title : String, subTitle : String,type: T.Type, pin1 : String, pin2 : String, accountAlias: String,otp: String, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          let paramDict = [
               "SCOMMAND"      :   sCommand.COTP,
               "UPHONENUM"     :   OnboardUser.mobileNo,
               "UDEVID"        :   getuuidFromDb(),
               "ORGID"         :   sCommand.oRGID,
               "CUSTOMER_HASH" :   getuuidFromDb(),
               "CLIENT_HASH"   :   clientHash,
               "osVersion"     :   UIDevice.current.systemVersion,
               "appVersion"    :   appVersion,
               "ORIGIN"        :   sCommand.oRIGIN,
               "ACCOUNT_ALIAS" :   accountAlias,
               "UAUTHPIN" : otp,
               "PIN1" : pin1,
               "PIN2" : pin2
               ] as [String : Any]
          
      
          RestAPIManager.callAPI(title: title, subTitle: subTitle, param: paramDict, type: type, completion: { (response) in
               completion(response)
          }) { (err) in
               error(err)
          }
     }
     
     
     //Google Map Directions:_
     class func get_Direction(url : String,onCompletion: @escaping (NSDictionary) -> Void) {
          
          let request = NSMutableURLRequest(url: URL(string: url)!)
          let config = URLSessionConfiguration.default
          let session = URLSession(configuration: config)
          
          let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
               
               // notice that I can omit the types of data, response and error
               do{
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                         
                         onCompletion(jsonResult)
                         
                    }
               }
               catch{
                    onCompletion(error as! NSDictionary)
                
               }
          });
          
          // do whatever you need with the task e.g. run
          task.resume()
     }
     
     
     
     
     //MARK: Perform POST Request
     class func makeHTTPPOSTRequest(_ path: String, body: [String: AnyObject], title : String, subtitle : String, onCompletion: @escaping ServiceResponse) {
          var encriptedInfo = [String : AnyObject]()
          
//          if AppMode.appMode == ApplicationMode.developmentEncpt {  //FOR NORMAL REQUEST
//               encriptedInfo = body
//          }
        //  else if AppMode.appMode == ApplicationMode.production { // //FOR ENCRIPTED REQUEST
               let jsonData = try! JSONSerialization.data(withJSONObject: body, options: [])
               var decoded = String(data: jsonData, encoding: .utf8) ?? ""
               decoded = Secure().encryptPIN(text: decoded)
               let phnx = Variables().reveal(key: VariableConstants.phnx)
               let lpg = Variables().reveal(key: VariableConstants.lpg)
               encriptedInfo[phnx] = "1" as AnyObject
               encriptedInfo[lpg] = decoded as AnyObject
         // }
          
           let headers = ["content-type": "application/json","Idempotency-Key":Global.randomString(length: 16)]
          let request = NSMutableURLRequest(url: URL(string: path)!)
          request.allHTTPHeaderFields = headers
          request.httpMethod = "POST"
          request.timeoutInterval = 120 // In second
          
          
          do {
               // Set the POST body for the request
               let jsonBody = try JSONSerialization.data(withJSONObject: encriptedInfo, options: .prettyPrinted)
               callPostApi(body: jsonBody, request: request, title: title, subtitle: subtitle, onCompletion: onCompletion)
               
          } catch {
               
               // Create your personal error
               var jsonTemp = JSON()
               jsonTemp["STATUS_CODE"] = 1003
               jsonTemp["STATUS_MESSAGE"] = "Can't perform the requested action due to a temporary server error.\nPlease try again later"
               onCompletion(jsonTemp, error as NSError?)
               
          }
     }
     
     class func callPostApi(body : Data, request : NSMutableURLRequest, title : String,  subtitle : String, onCompletion: @escaping ServiceResponse) {
          
          do {
               request.httpBody = body
               var session : URLSession!
           //    if AppMode.appMode == ApplicationMode.development || AppMode.appMode == ApplicationMode.developmentEncpt
            //   {
                    session = URLSession(
                         configuration: URLSessionConfiguration.default,
                         delegate: NSURLSessionPinningDelegate(),
                         delegateQueue: nil)
               
          //     }
//               else
//               {
//                   session = URLSession.shared //without pinning
//               }

              let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                    
                                   self.backgroundTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
                                        endBackgroundTask()
                                   })
                                   assert(self.backgroundTask.rawValue != 0)
                    
                    
                                   if let jsonData = data {
                    
                                        let json:JSON = JSON(data: jsonData)
                    
                                        let phnx = Variables().reveal(key: VariableConstants.phnx)
                                        let lpg = Variables().reveal(key: VariableConstants.lpg)
                    
                                        if json[phnx].intValue == 1 {
                    
                                             let encriptedStr = json[lpg].stringValue
                                             let decrptedStr = Secure().decryptPIN(text: encriptedStr)
                                             let decriptedData = Data(decrptedStr.utf8)
                                             let json:JSON = JSON(data: decriptedData)
                    
                                             if self.isDisableApp(json: json) {
                                                  return
                                             }
                                             else {
                                                  if json == JSON.null {
                                                       var jsonTemp = JSON()
                                                       jsonTemp["STATUS_CODE"] = 1003
                                                       jsonTemp["STATUS_MESSAGE"] = "Can't perform the requested action due to a temporary server error.\nPlease try again later"
                                                       onCompletion(jsonTemp, error as NSError?)
                                                  }
                                                  else {
                                                       onCompletion(json, nil)
                                                  }
                    
                                             }
                                        }
                                        else {
                    
                                             if self.isDisableApp(json: json) {
                                                  return
                                             }
                                             else {
                                                  onCompletion(json, nil)
                                             }
                                        }
                    
                                   } else {
                    
                                        var jsonTemp = JSON()
                                        jsonTemp["STATUS_CODE"] = 1003
                                        jsonTemp["STATUS_MESSAGE"] = "Can't perform the requested action due to a temporary server error.\nPlease try again later"
                                        onCompletion(jsonTemp, error as NSError?)
                                   }
                    
                                   DispatchQueue.main.async {
                    
                                        // loader.remove()
                                   }
              })
               task.resume()
             
          } catch {
               var jsonTemp = JSON()
               jsonTemp["STATUS_CODE"] = 1003
               jsonTemp["STATUS_MESSAGE"] = "Can't perform the requested action due to a temporary server error.\nPlease try again later"
               onCompletion(jsonTemp, nil)
          }
     }
     
     class func callAPI<T>(title: String, subTitle : String, param: [String : Any], type: T.Type, completion: @escaping (T) -> Void, error: @escaping (Error) -> Void) where T : Decodable {
          var encriptedInfo = [String : AnyObject]()
          
          
          var loader = ProcessingBar()
          
          //  let loader = RestAPIManager.addProgress(title: title, description: subtitle)
          if global.cupApi != "NO"{
               loader = RestAPIManager.addProgress(title: title, description: subTitle)
          }
          else {
               global.cupApi = ""
          }

//          if AppMode.appMode == ApplicationMode.development || AppMode.appMode == ApplicationMode.developmentEncpt {  //FOR NORMAL REQUEST
//               encriptedInfo = param as [String : AnyObject]
//          }
         // else if AppMode.appMode == ApplicationMode.production { // //FOR ENCRIPTED REQUEST
               let jsonData = try! JSONSerialization.data(withJSONObject: param, options: [])
               var decoded = String(data: jsonData, encoding: .utf8) ?? ""
               decoded = Secure().encryptPIN(text: decoded)
               let phnx = Variables().reveal(key: VariableConstants.phnx)
               let lpg = Variables().reveal(key: VariableConstants.lpg)
               encriptedInfo[phnx] = "1" as AnyObject
               encriptedInfo[lpg] = decoded as AnyObject
               //  encriptedInfo = param as [String : AnyObject]
       //   }

          let headers = ["content-type": "application/json","Idempotency-Key":Global.randomString(length: 16)]
          let request = NSMutableURLRequest(url: URL(string: baseURL)!)
          request.allHTTPHeaderFields = headers
          request.httpMethod = "POST"
          request.timeoutInterval = 120 // In second
          
                         //  let loader = RestAPIManager.addProgress(title: title, description: subTitle)
          
          
          do {
               // Set the POST body for the request
               let jsonBody = try JSONSerialization.data(withJSONObject: encriptedInfo, options: .prettyPrinted)

               request.httpBody = jsonBody
               var session : URLSession!
//               if AppMode.appMode == ApplicationMode.development || AppMode.appMode == ApplicationMode.developmentEncpt
//               {
//                   session = URLSession(
//                         configuration: URLSessionConfiguration.default,
//                         delegate: NSURLSessionPinningDelegate(),
//                         delegateQueue: nil)
//
//               }
//               else
//               {
//                    session = URLSession.shared //without pinning
//               }
               
               session = URLSession(
                                     configuration: URLSessionConfiguration.default,
                                      delegate: NSURLSessionPinningDelegate(),
                                      delegateQueue: nil)
                                 

               let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                    
                    DispatchQueue.main.async {
                     loader.remove()
                    }

                   // if AppMode.appMode == ApplicationMode.production
                   // {

                         if let jsonData = data {
                              let json:JSON = JSON(data: jsonData)
                              let phnx = Variables().reveal(key: VariableConstants.phnx)
                              let lpg = Variables().reveal(key: VariableConstants.lpg)

                              if json[phnx].intValue == 1 {
                                   let encriptedStr = json[lpg].stringValue
                                   let decrptedStr = Secure().decryptPIN(text: encriptedStr)
                                   let decriptedData = Data(decrptedStr.utf8)
                                  // let json:JSON = JSON(data: decriptedData)
                                     do {
                                             let decoder = JSONDecoder()
                                             let gitData = try decoder.decode(type, from: decriptedData)
                                             completion(gitData)
 
                                        } catch let err {
                                      //  print(err)
                                        }
                                   

                              } else {

                              }

                         }
                         else {

                              var jsonTemp = JSON()
                              jsonTemp["STATUS_CODE"] = 1003
                              jsonTemp["STATUS_MESSAGE"] = "Can't perform the requested action due to a temporary server error.\nPlease try again later"
                         }
                    
//                     } else {
//                         let json:JSON = JSON(data: data!)
//                         print(json)
//                         if let data = data{
//                              do {
//                                   let decoder = JSONDecoder()
//                                   let gitData = try decoder.decode(type, from: data)
//                                   completion(gitData)
//
//                              } catch let err {
//                                 print(err)
//                              }
//                         }else if let erro = error{
//                          print(error)
//                         }
//                    }
               })
               task.resume()
          } catch {
               var jsonTemp = JSON()
          }
     }
     
     class func endBackgroundTask() {
         // print("Background task ended.")
          UIApplication.shared.endBackgroundTask(backgroundTask)
          backgroundTask = UIBackgroundTaskIdentifier(rawValue: 0)
     }
     
     ///////////////////////////////Alamofire///////////////////////////////
     
     class func isDisableApp(json : JSON) -> Bool {
          
          if json["STATUS_CODE"].intValue == 211 {
               
               DispatchQueue.main.async {
                    let visibleViewController = self.getVisibleViewController(nil)
                    visibleViewController?.showMessage(title: "Alert!", message: json["STATUS_MESSAGE"].stringValue)
               }
               return true
          }
          return false
     }
     
     class func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {
          
          var rootVC = rootViewController
          if rootVC == nil {
               rootVC = UIApplication.shared.keyWindow?.rootViewController
          }
          
          if rootVC?.presentedViewController == nil {
               return rootVC
          }
          
          if let presented = rootVC?.presentedViewController {
               if presented.isKind(of: UINavigationController.self) {
                    let navigationController = presented as! UINavigationController
                    return navigationController.viewControllers.last!
               }
               
               if presented.isKind(of: UITabBarController.self) {
                    let tabBarController = presented as! UITabBarController
                    return tabBarController.selectedViewController!
               }
               
               return getVisibleViewController(presented)
          }
          return nil
     }
     
     class func getTopMostViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
          if let nav = base as? UINavigationController {
               return getTopMostViewController(base: nav.visibleViewController)
          }
          if let tab = base as? UITabBarController {
               if let selected = tab.selectedViewController {
                    return getTopMostViewController(base: selected)
               }
          }
          if let presented = base?.presentedViewController {
               return getTopMostViewController(base: presented)
          }
          return base
     }
     
     class func addProgress(title: String, description : String) -> ProcessingBar {
          let parentVC = getTopMostViewController()
          let customPopup = ProcessingBar(frame: (parentVC?.view.frame)!)
          customPopup.showCustomPopup(title: title, description: description, parent_view: (parentVC?.view)!)
          parentVC?.view.addSubview(customPopup)
          return customPopup
     }
}

extension NSMutableData {
     func appendString(_ string: String) {
          let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
          append(data!)
     }
}


