//
//  URLSessionPinningDelegate.swift
//  SSLPinningDemoALKI
//
//  Created by 5ExceptionsMac4 on 14/08/18.
//  Copyright © 2018 5Exceptions. All rights reserved.
//

import Foundation
import Security

class NSURLSessionPinningDelegate: NSObject, URLSessionDelegate {
    
    

    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
            if let serverTrust = challenge.protectionSpace.serverTrust {
                var secresult = SecTrustResultType.invalid
                let status = SecTrustEvaluate(serverTrust, &secresult)
                if (errSecSuccess == status) {
                    if let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0) {
                        let serverCertificateData = SecCertificateCopyData(serverCertificate)
                        let data = CFDataGetBytePtr(serverCertificateData);
                        let size = CFDataGetLength(serverCertificateData);
                        let cert1 = NSData(bytes: data, length: size)
                        var str = ""
                         if AppMode.appMode == ApplicationMode.production {
                          str = "cfccert"
                        }
                         else {
                            str = "beep_staging"
                        }
                         let file_der = Bundle.main.path(forResource: str, ofType: "der")
                        
                        if let file = file_der {
                            if let cert2 = NSData(contentsOfFile: file) {
                                if cert1.isEqual(to: cert2 as Data) {
                                    OnboardUser.sectrust = serverTrust
                                   completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust:serverTrust))

                                    return
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodClientCertificate
            {
                if let trust = OnboardUser.sectrust {
                    completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust:trust))
                    return
                }

            }
        }

        // Pinning failed
        completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
    }
}

//
//class NSURLSessionPinningDelegate: NSObject, URLSessionDelegate {
//
//    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
//        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
//            if let serverTrust = challenge.protectionSpace.serverTrust {
//                var secresult = SecTrustResultType.invalid
//                let status = SecTrustEvaluate(serverTrust, &secresult)
//
//                if (errSecSuccess == status) {
//                    if let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0) {
//                        let serverCertificateData = SecCertificateCopyData(serverCertificate)
//                        let data = CFDataGetBytePtr(serverCertificateData);
//                        let size = CFDataGetLength(serverCertificateData);
//                        let cert1 = NSData(bytes: data, length: size)
//                        let file_der = Bundle.main.path(forResource: "new_3july", ofType: "der")
//
//                        if let file = file_der {
//                            if let cert2 = NSData(contentsOfFile: file) {
//                                if cert1.isEqual(to: cert2 as Data) {
//                                    completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust:serverTrust))
//                                    return
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        // Pinning failed
//        completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
//    }
//
//}
