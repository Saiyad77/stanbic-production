//
//  SideLeftMenuViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 26/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class SideLeftMenuViewController: UIViewController,UIPopoverPresentationControllerDelegate {
    
    // MARK:- IBOUTLETS
    @IBOutlet weak var imgPick: UIButton!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblLastSeen: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var appVersionLabel: UILabel!
    
    // @IBOutlet weak var imageViewBg: UIImageView!
    
    // MARK: Variables
    let appLanguage = Session.sharedInstance.appLanguage
    var mainViewController: UIViewController!
    var myPickerController = UIImagePickerController()
    
    
    var arrMenus: [String] = [String]()
    var menuImageArray: [UIImage] = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // imageViewBg.layer.shadowColor = UIColor.black.cgColor
        // imageViewBg.layer.shadowOpacity = 0.5
        // imageViewBg.layer.shadowOffset = CGSize.zero
        // imageViewBg.layer.shadowRadius = 12
        
        
        lblUsername.text = ""
        lblLastSeen.text = ""
        imgUser.layer.masksToBounds = false
        imgUser.layer.cornerRadius = imgUser.frame.height/2
        imgUser.clipsToBounds = true
        imgPick.layer.masksToBounds = false
        imgPick.layer.cornerRadius = imgPick.frame.height/2
        imgPick.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
       // arrMenus = ["LOANCALCULATER", "FOREXRATES", "NOTIFICATION", "LOCATEUS", "SETTING","ABOUTUS","CONTACTUS"]
        arrMenus = ["LOANSERVICES", "FOREXRATES","LOCATEUS", "SETTING","ABOUTUS","CONTACTUS", "Logout"]
        menuImageArray = [#imageLiteral(resourceName: "015-payment"), #imageLiteral(resourceName: "010-exchange-1"), #imageLiteral(resourceName: "027-map"), #imageLiteral(resourceName: "026-automation"), #imageLiteral(resourceName: "041-team"), #imageLiteral(resourceName: "sidecontact"), #imageLiteral(resourceName: "logouts")]
        self.tableHeight.constant = CGFloat(self.arrMenus.count * 80) + 70
        self.tblView.reloadWithAnimation()
        setFonts()
        //lblUsername.text!.firstCharacterUpperCase()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        getImage()
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Home.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func setFonts() {
        let accountDetails = CoreDataHelper().getDataForEntity(entity: Entity.PROFILES_INFO)
        if accountDetails.count > 0 {
            let name = "\(accountDetails[0]["aFIRST_NAME"] ?? "") \(accountDetails[0]["aOTHER_NAMES"] ?? "")"
            Fonts().set(object: lblUsername, fontType: 0, fontSize: 17, color: Color.white, title: name, placeHolder: "")
            Fonts().set(object: lblLastSeen, fontType: 0, fontSize: 10, color: Color.white, title:"Last seen \(accountDetails[0]["aLAST_LOGIN"] ?? "")", placeHolder: "")
            lblLastSeen.alpha = 0.8
            
            if lblUsername.text?.count ?? 0 > 0 {
              lblUsername.text = "\(lblUsername.text?.firstCharacterUpperCase() ?? "")!"
             }
        }
    }
    
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func getImage(){
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent("profileImage.jpg")
        if fileManager.fileExists(atPath: imagePAth){
            let anyAvatarImage:UIImage = UIImage(contentsOfFile: imagePAth)!
            self.imgUser.maskCircle(anyImage: anyAvatarImage)
            
        }else{
            
        }
    }
    @IBAction func imgPickTapped(_ sender: UIButton) {
        showActionSheetForProfilePicture(sender)
    }
}

extension SideLeftMenuViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if DeviceType.IS_IPHONE_6 {
            self.tableHeight.constant = CGFloat(self.arrMenus.count * 80)
            return 80
        } else if DeviceType.IS_IPHONE_6P {
            self.tableHeight.constant = CGFloat(self.arrMenus.count * 80)
            return 80
        } else if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XR || DeviceType.IS_IPHONE_XSMAX {
            self.tableHeight.constant = CGFloat(self.arrMenus.count * 80) + 60
             return 90
        }  else  {
            self.tableHeight.constant = CGFloat(self.arrMenus.count * 60)
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenus.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! SidebarTableViewCell
        cell.lblMenu.text = arrMenus[indexPath.row].localized(appLanguage)
        cell.menuImage.image = menuImageArray[indexPath.row]
        
       if DeviceType.IS_IPHONE_6 {
            if indexPath.row == 0 {
                cell.imageviewLeadingConstant.constant = 15
                cell.imageviewCenterConstant.constant = 13
            } else if indexPath.row == 1 {
                cell.imageviewLeadingConstant.constant = 60
            } else if indexPath.row == 2 {
                cell.imageviewLeadingConstant.constant = 80
            } else if indexPath.row == 3 {
                cell.imageviewLeadingConstant.constant = 85
            } else if indexPath.row == 4 {
                cell.imageviewLeadingConstant.constant = 75
            } else if indexPath.row == 5 {
                cell.imageviewLeadingConstant.constant = 55
            } else if indexPath.row == 6 {
                cell.imageviewLeadingConstant.constant = 15
            }
       }else if DeviceType.IS_IPHONE_5 {
        if indexPath.row == 0 {
            cell.imageviewLeadingConstant.constant = 15
            cell.imageviewCenterConstant.constant = 13
        } else if indexPath.row == 1 {
            cell.imageviewLeadingConstant.constant = 60
        } else if indexPath.row == 2 {
            cell.imageviewLeadingConstant.constant = 80
        } else if indexPath.row == 3 {
            cell.imageviewLeadingConstant.constant = 85
        } else if indexPath.row == 4 {
            cell.imageviewLeadingConstant.constant = 77
        } else if indexPath.row == 5 {
            cell.imageviewLeadingConstant.constant = 65
        } else if indexPath.row == 6 {
            cell.imageviewLeadingConstant.constant = 15
        }
        
       }
       else if DeviceType.IS_IPHONE_6P {
            if indexPath.row == 0 {
                cell.imageviewLeadingConstant.constant = 15
                cell.imageviewCenterConstant.constant = 13
            } else if indexPath.row == 1 {
            cell.imageviewLeadingConstant.constant = 60
            } else if indexPath.row == 2 {
                cell.imageviewLeadingConstant.constant = 80
            } else if indexPath.row == 3 {
                cell.imageviewLeadingConstant.constant = 85
            } else if indexPath.row == 4 {
                cell.imageviewLeadingConstant.constant = 75
            } else if indexPath.row == 5 {
                cell.imageviewLeadingConstant.constant = 55
            } else if indexPath.row == 6 {
                cell.imageviewLeadingConstant.constant = 15
            }
      
       }  else if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XR || DeviceType.IS_IPHONE_XSMAX {
            if indexPath.row == 0 {
                cell.imageviewLeadingConstant.constant = 20
                cell.imageviewCenterConstant.constant = 13
            } else if indexPath.row == 1 {
                cell.imageviewLeadingConstant.constant = 60
            } else if indexPath.row == 2 {
                cell.imageviewLeadingConstant.constant = 80
            } else if indexPath.row == 3 {
                cell.imageviewLeadingConstant.constant = 85
            } else if indexPath.row == 4 {
                cell.imageviewLeadingConstant.constant = 75
            } else if indexPath.row == 5 {
                cell.imageviewLeadingConstant.constant = 55
            } else if indexPath.row == 6 {
                cell.imageviewLeadingConstant.constant = 15
            }
        } else if DeviceType.IS_IPHONE_5 {
                cell.imageviewWidht.constant = 35
                cell.imageviewHeight.constant = 35
                if indexPath.row == 0 {
                    cell.imageviewLeadingConstant.constant = 20
                    cell.imageviewCenterConstant.constant = 13
                } else if indexPath.row == 1 {
                    cell.imageviewLeadingConstant.constant = 60
                } else if indexPath.row == 2 {
                    cell.imageviewLeadingConstant.constant = 90
                } else if indexPath.row == 3 {
                    cell.imageviewLeadingConstant.constant = 90
                } else if indexPath.row == 4 {
                    cell.imageviewLeadingConstant.constant = 90
                } else if indexPath.row == 5 {
                    cell.imageviewLeadingConstant.constant = 15
                } else if indexPath.row == 6 {
                    cell.imageviewLeadingConstant.constant = 15
                }
        }
       else if DeviceType.IS_IPAD {
        cell.imageviewWidht.constant = 35
        cell.imageviewHeight.constant = 35
        if indexPath.row == 0 {
            cell.imageviewLeadingConstant.constant = 20
            cell.imageviewCenterConstant.constant = 13
        } else if indexPath.row == 1 {
            cell.imageviewLeadingConstant.constant = 60
        } else if indexPath.row == 2 {
            cell.imageviewLeadingConstant.constant = 85
        } else if indexPath.row == 3 {
            cell.imageviewLeadingConstant.constant = 90
        } else if indexPath.row == 4 {
            cell.imageviewLeadingConstant.constant = 90
        } else if indexPath.row == 5 {
            cell.imageviewLeadingConstant.constant = 64
        } else if indexPath.row == 6 {
            cell.imageviewLeadingConstant.constant = 30
        }
        }
       else if DeviceType.IS_IPAD_PRO {
        cell.imageviewWidht.constant = 35
        cell.imageviewHeight.constant = 35
        if indexPath.row == 0 {
            cell.imageviewLeadingConstant.constant = 20
            cell.imageviewCenterConstant.constant = 13
        } else if indexPath.row == 1 {
            cell.imageviewLeadingConstant.constant = 60
        } else if indexPath.row == 2 {
            cell.imageviewLeadingConstant.constant = 90
        } else if indexPath.row == 3 {
            cell.imageviewLeadingConstant.constant = 90
        } else if indexPath.row == 4 {
            cell.imageviewLeadingConstant.constant = 90
        } else if indexPath.row == 5 {
            cell.imageviewLeadingConstant.constant = 15
        } else if indexPath.row == 6 {
            cell.imageviewLeadingConstant.constant = 15
        }
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let vc = NavSelectLoan.getVCInstance() as! NavSelectLoan
            self.slideMenuController()?.changeMainViewController(vc, close: true)
       case 1:
            CurrentFlow.flowType = FlowType.ForexRate
            let vc = ForexNavController.getVCInstance() as! ForexNavController
            self.slideMenuController()?.changeMainViewController(vc, close: true)
       case 2:
            let vc = LocateUsNavController.getVCInstance() as! LocateUsNavController
            self.slideMenuController()?.changeMainViewController(vc, close: true)
        case 3:
            let vc = SettingNavController.getVCInstance() as! SettingNavController
            self.slideMenuController()?.changeMainViewController(vc, close: true)
        case 4:
            let vc = AboutUsNavController.getVCInstance() as! AboutUsNavController
            self.slideMenuController()?.changeMainViewController(vc, close: true)
        case 5:
            let vc = ContactUsNavController.getVCInstance() as! ContactUsNavController
            self.slideMenuController()?.changeMainViewController(vc, close: true)
        case 6:
            let alertCon = UIAlertController(title: "Confirm", message: "Are you sure you want to logout?", preferredStyle: UIAlertController.Style.alert)
            let actionCancel = UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: nil)
            let actionConfirm = UIAlertAction(title: "Yes", style: .default) { action in
                DispatchQueue.main.async {
                    self.nextVC()
                }
            }
            alertCon.addAction(actionConfirm)
            alertCon.addAction(actionCancel)
            self.present(alertCon,animated: true)
            
        default:
          return
        }
    }
    
    // MARK:_ SHOW NEXT VC
    func nextVC()  {
        UserDefaults.standard.set(false, forKey: "logout")
        UserDefaults.standard.synchronize()
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        self.show(nextvc, sender: nil )
       // self.navigationController?.pushViewController(nextvc, animated: true)
    }
}

extension SideLeftMenuViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func makeAttributed(title : String) -> NSMutableAttributedString {
        let attributedText = NSMutableAttributedString(string: title)
        
        let range = NSRange(location: 0, length: attributedText.length)
        attributedText.addAttribute(NSAttributedString.Key.kern, value: 1.5, range: range)
        attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont(name: Fonts.kFontMedium, size: 19.0)!, range: range)
        return attributedText
    }
    
    func showActionSheetForProfilePicture(_ sender: UIButton) {
        let appLanguage = Session.sharedInstance.appLanguage
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //makeAttributed(title: LabelField.actionAlwaysDisplayBalance)
        alert.view.tintColor = Color.c0_179_255
        
        let action1 = UIAlertAction(title: "TAKENEWPHOTO".localized(appLanguage as String), style: .default, handler: { (UIAlertAction)in
            self.tabBarController?.tabBar.isHidden = true
            self.takePhotoFromCamera()
        })
        
        let action2 = UIAlertAction(title: "CHOOSEEXISTING", style: .default, handler: { (UIAlertAction)in
            self.tabBarController?.tabBar.isHidden = true
            self.takePhotoFromLibrary()
        })
        
        let action3 = UIAlertAction(title: "CANCEL", style: .cancel, handler: { (UIAlertAction)in
            
        })
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
        guard let label1 = (action1.value(forKey: "__representer")as? NSObject)?.value(forKey: "label") as? UILabel else { return }
        label1.attributedText = makeAttributed(title: "TAKENEWPHOTO".localized(appLanguage as String))
        
        guard let label2 = (action2.value(forKey: "__representer")as? NSObject)?.value(forKey: "label") as? UILabel else { return }
        label2.attributedText = makeAttributed(title: "CHOOSEEXISTING".localized(appLanguage as String))
        
        guard let label3 = (action3.value(forKey: "__representer")as? NSObject)?.value(forKey: "label") as? UILabel else { return }
        label3.attributedText = makeAttributed(title: "CANCEL".localized(appLanguage as String))
    }
    
    func takePhotoFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            //self.myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            myPickerController.modalPresentationStyle = .overCurrentContext
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func takePhotoFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            //self.myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            myPickerController.modalPresentationStyle = .overCurrentContext
            self.present(myPickerController, animated: true, completion: nil)
        }else{
         
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.tabBarController?.tabBar.isHidden = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.tabBarController?.tabBar.isHidden = false
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            setAndSavePhoto(photo: image)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func setAndSavePhoto(photo: UIImage){
        //self.imgUser.image = photo
        self.imgUser.maskCircle(anyImage: photo)
        saveImageDocumentDirectory(imageToSave: photo)
    }
    
    func saveImageDocumentDirectory(imageToSave: UIImage) {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("profileImage.jpg")
        let image = imageToSave
        let imageData = image.jpegData(compressionQuality: 0.75)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
    }
}

extension UIImageView {
    public func maskCircle(anyImage: UIImage) {
        self.contentMode = UIView.ContentMode.scaleAspectFill
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = false
        self.clipsToBounds = true
        
        // make square(* must to make circle),
        // resize(reduce the kilobyte) and
        // fix rotation.
        self.image = anyImage
    }
}

extension UITableView {
    func reloadWithAnimation() {
        self.reloadData()
        let tableViewHeight = self.bounds.size.height
        let cells = self.visibleCells
        var delayCounter = 0
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        for cell in cells {
            UIView.animate(withDuration: 1.6, delay: 0.08 * Double(delayCounter),usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
}

