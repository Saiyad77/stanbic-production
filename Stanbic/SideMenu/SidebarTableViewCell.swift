//
//  SidebarTableViewCell.swift
//  Stanbic
//
//  Created by Vijay Patidar on 26/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class SidebarTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblMenu: UILabel!
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var imageviewLeadingConstant: NSLayoutConstraint!
    @IBOutlet weak var imageviewCenterConstant: NSLayoutConstraint!
    
    
    @IBOutlet weak var imageviewHeight: NSLayoutConstraint!
    @IBOutlet weak var imageviewWidht: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Fonts().set(object: lblMenu, fontType: 0, fontSize: 17, color: Color.white, title: "", placeHolder: "")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
