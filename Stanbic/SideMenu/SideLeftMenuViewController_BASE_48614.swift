//
//  SideLeftMenuViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 26/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class SideLeftMenuViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblLastSeen: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imageViewBg: UIImageView!
    
    //Mark:- Variables
    let appLanguage = Session.sharedInstance.appLanguage
    var mainViewController: UIViewController!
    
    let arrMenus = ["MYPROFILE", "LOANCALCULATER", "FOREXRATES", "NOTIFICATION", "LOCATEUS", "SETTING","ABOUTUS","CONTACTUS"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // background shadow
        //        imageViewBg.layer.shadowColor = UIColor.black.cgColor
        //        imageViewBg.layer.shadowOpacity = 0.5
        //        imageViewBg.layer.shadowOffset = CGSize.zero
        //        imageViewBg.layer.shadowRadius = 12
        
        lblUsername.text = ""
        lblLastSeen.text = ""
        setFonts()
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Home.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))") 
    }
    
    func setFonts() {
        let accountDetails = CoreDataHelper().getDataForEntity(entity: Entity.PROFILES_INFO)
        if accountDetails.count > 0 {
            Fonts().set(object: lblUsername, fontType: 0, fontSize: 17, color: Color.white, title: accountDetails[0]["aFIRST_NAME"] ?? "", placeHolder: "")
            Fonts().set(object: lblLastSeen, fontType: 0, fontSize: 10, color: Color.white, title:"Last seen \(accountDetails[0]["aLAST_LOGIN"] ?? "")", placeHolder: "")
            lblLastSeen.alpha = 0.8
            
            if lblUsername.text?.count ?? 0 > 0 {
                lblUsername.text = "\(lblUsername.text ?? "")!"
            }
        }
    }
    
}

extension SideLeftMenuViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! SidebarTableViewCell
        cell.lblMenu.text = arrMenus[indexPath.row].localized(appLanguage)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch indexPath.row {
        case 0:  //My profile
            let vc = ForexNavController.getVCInstance() as! ForexNavController
            //  self.slideMenuController()?.changeMainViewController(vc, close: true)
            
        case 1: //Loan Calculator
            let vc = SelectLoanCalculater.getVCInstance() as! SelectLoanCalculater
            self.slideMenuController()?.changeMainViewController(vc, close: true)
            
        case 2:  //Forex Rates
            let vc = ForexNavController.getVCInstance() as! ForexNavController
            self.slideMenuController()?.changeMainViewController(vc, close: true)
            
        case 3:  //Notifications
           let vc = ForexNavController.getVCInstance() as! ForexNavController
          //  self.slideMenuController()?.changeMainViewController(vc, close: true)
            
        case 4:  //Locate us
            let vc = LocateUsNavController.getVCInstance() as! LocateUsNavController
            self.slideMenuController()?.changeMainViewController(vc, close: true)
            
        case 5:  //Settings
            let vc = SettingNavController.getVCInstance() as! SettingNavController
            self.slideMenuController()?.changeMainViewController(vc, close: true)
            
        case 6: //About us
            let vc = AboutUsNavController.getVCInstance() as! AboutUsNavController
            self.slideMenuController()?.changeMainViewController(vc, close: true)
            
        case 7:  // Contact us
            let vc = ContactUsNavController.getVCInstance() as! ContactUsNavController
            self.slideMenuController()?.changeMainViewController(vc, close: true)
            
        default:
            print(indexPath)
        }
    }
}

