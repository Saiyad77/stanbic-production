//
//  CoreDataHelper.swift
//  Stanbic
//
//  Created by Vijay Patidar on 16/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import CoreData
import UIKit

typealias DBResponse = (Bool, String)

class CoreDataHelper : NSObject {
    
    var context : NSManagedObjectContext? = nil
    
    override init() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
    }
    
    func saveDictionaryFor(json : JSON, entityObject : Entity) {
        
        //FIRST REMOVE ALL RECORDS FROM ENTITY
        self.removeAllRecords(entityObject.rawValue)
        let entity = NSEntityDescription.entity(forEntityName: entityObject.rawValue, in: context!)
        let account_info = NSManagedObject(entity: entity!, insertInto: context)
        let account_data = json[entityObject.rawValue]
        for (key, value) in account_data {
            let hasValidKey =  account_info.entity.propertiesByName.keys.contains("a"+key)
            if hasValidKey == true {
                account_info.setValue(value.stringValue, forKey: "a"+key)
            }
        }
        saveContext()
    }
    
    func saveArrayFor(json : JSON, entityObject : Entity, jsonKey : String) {
        //FIRST REMOVE ALL RECORDS FROM ENTITY
        self.removeAllRecords(entityObject.rawValue)
        let entity = NSEntityDescription.entity(forEntityName: entityObject.rawValue, in: context!)
        for i in 0 ..< (json[jsonKey].count ) {
            let account_info = NSManagedObject(entity: entity!, insertInto: context)
            let account_data = json[jsonKey][i]
            
            for (key, value) in account_data {
                let hasValidKey =  account_info.entity.propertiesByName.keys.contains("a"+key)
                if hasValidKey == true {
                    account_info.setValue(value.description, forKey: "a"+key)
                }
            }
        }
        saveContext()
    }
    
    // save push notifications:_
    func saveArrayForNotifiation(json :  [[String:String]], entityObject : Entity, jsonKey : String) {
        
        //FIRST REMOVE ALL RECORDS FROM ENTITY
        self.removeAllRecords(entityObject.rawValue)
        let entity = NSEntityDescription.entity(forEntityName: entityObject.rawValue, in: context!)
        for i in 0 ..< json.count {
            let account_info = NSManagedObject(entity: entity!, insertInto: context)
            let dict = json[i]
            account_info.setValue(dict["abody"], forKey: "abody")
            account_info.setValue(dict["atitle"], forKey: "atitle")
            account_info.setValue(dict["aimageUrl"], forKey: "aimageUrl")
            account_info.setValue(dict["atime"], forKey: "atime")
            account_info.setValue(dict["alink"], forKey: "alink")
            account_info.setValue(dict["anotificationType"], forKey: "anotificationType")
            account_info.setValue(dict["aviewstatus"], forKey: "aviewstatus")
        }
        saveContext()
    }
    
    func saveUUID(value : String, entityName : String, key : String, removeRecords : Bool) {
        
        //FIRST REMOVE ALL RECORDS FROM ENTITY
        if removeRecords == true {
            self.removeAllRecords(entityName)
        }
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context!)
        let account_info = NSManagedObject(entity: entity!, insertInto: context)
        
        let hasValidKey =  account_info.entity.propertiesByName.keys.contains("a"+key)
        if hasValidKey == true {
            account_info.setValue(value, forKey: "a"+key)
        }
        saveContext()
    }
    
    func saveUUID1(value : Data, entityName : String, key : String, removeRecords : Bool) {
        
        //FIRST REMOVE ALL RECORDS FROM ENTITY
        if removeRecords == true {
            self.removeAllRecords(entityName)
        }
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context!)
        let account_info = NSManagedObject(entity: entity!, insertInto: context)
        
        let hasValidKey =  account_info.entity.propertiesByName.keys.contains("a"+key)
        if hasValidKey == true {
            account_info.setValue(value, forKey: "a"+key)
        }
        saveContext()
    }
    
    
    func getDataForEntity(entity : Entity) -> [[String : String]] {
        let arr_all_keys  = entity.Keys()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)
        var data_array = [[String : String]]()
        request.returnsObjectsAsFaults = false
        do {
            let result = try context?.fetch(request)
            for data in result as! [NSManagedObject] {
                
                var dict_data = [String : String]()
                for i in 0 ..< arr_all_keys.count {
                    //let value = self.convertValue1(text: data.value(forKey: arr_all_keys[i]) as! String)
                    dict_data[arr_all_keys[i]] = data.value(forKey: arr_all_keys[i]) as? String
                    
                }
                data_array.append(dict_data)
            }
            
        } catch {
            
        }
        return data_array
    }
    
    func getDataForEntity1(entity : Entity) -> [[String : Data]] {
        let arr_all_keys  = entity.Keys()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)
        var data_array = [[String : Data]]()
        request.returnsObjectsAsFaults = false
        do {
            let result = try context?.fetch(request)
            for data in result as! [NSManagedObject] {
                
                var dict_data = [String : Data]()
                for i in 0 ..< arr_all_keys.count {
                    //let value = self.convertValue1(text: data.value(forKey: arr_all_keys[i]) as! String)
                    dict_data[arr_all_keys[i]] = data.value(forKey: arr_all_keys[i]) as? Data
                    
                }
                data_array.append(dict_data)
            }
            
        } catch {
            
        }
        return data_array
    }
    
    
    // FetchNotifications
    func FetchNotifications(entity : Entity) -> [[String : String]]{
        let arr_all_keys  = entity.Keys()
        var data_array = [[String : String]]()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context?.fetch(request)
            for data in result as! [NSManagedObject] {
                var dict_data = [String : String]()
                for i in 0 ..< arr_all_keys.count {
                    dict_data[arr_all_keys[i]] = data.value(forKey: arr_all_keys[i]) as? String
                }
                if dict_data.count > 0 {
                    data_array.append(dict_data)
                }
            }
        } catch {
           
        }
        return data_array
    }
    
    func removeAllRecords(_ entityName: String) -> Void {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        var results: [AnyObject] = []
        do {
            results = (try context?.fetch(request))!
        } catch _ as NSError {
            
        }
        
        for record in results {
            context?.delete(record as! NSManagedObject)
        }
        saveContext()
    }
    func saveContext() -> Void {
        
        do {
            try context?.save()
            
        } catch _ as NSError {
            
        }
    }
    
    func convertValue(text : String) -> String {
        
        let input:String = text
        let cipher:String = CryptoHelper.encrypt(input:input)!;
        //cipher = "kb1TyFRUcMaY6Z1vCRravA==";
        //let output:String = CryptoHelper.decrypt(input:cipher)!;
        return cipher
    }
    
    func convertValue1(text : String) -> String {
        
        let input:String = text
        //let cipher:String = CryptoHelper.encrypt(input:input)!;
        //cipher = "kb1TyFRUcMaY6Z1vCRravA==";
        let output:String = CryptoHelper.decrypt(input:input)!;
        return output
    }
    
}

extension CoreDataHelper {
    func getCurrencyList() -> [[String : String]] {
        var arrfilterlist = [[String : String]]()
        
        if let csvPath = Bundle.main.path(forResource: "Currency", ofType: "csv"){
            do{
                let file = try String.init(contentsOfFile: csvPath)
                var arrList = file.components(separatedBy: "\n")
                var dictObject = [String : String]()
                var arrTotalObject = [[String : String]]()
                for i in (0..<arrList.count).reversed(){
                    let obj = arrList[i]
                    var arrObject = obj.components(separatedBy: ",")
                    
                    
                    if (arrObject.count > 2) {
                        
                        if arrObject[1] == "ISO-4217"{
                            continue
                        }
                    }
                    if (arrObject.count == 5 || arrObject.count > 5) {
                        let strCurrencyNumber = arrObject[0]
                        let strCurrencyCode = arrObject[1]
                        let strCurrencyCountry = arrObject[2]
                        let strCurrencyName = arrObject[3]
                        let strCountryCode = arrObject[4]
                        
                        dictObject["CurrencyNumber"] = strCurrencyNumber.replacingOccurrences(of: "\r", with: "")
                        dictObject["CurrencyCode"] = strCurrencyCode.replacingOccurrences(of: "\r", with: "")
                        dictObject["CountryName"] = strCurrencyCountry.replacingOccurrences(of: "\r", with: "")
                        dictObject["CurrencyName"] = strCurrencyName.replacingOccurrences(of: "\r", with: "")
                        dictObject["CountryISOCode"] = strCountryCode.replacingOccurrences(of: "\r", with: "")
                        
                        arrTotalObject.append(dictObject)
                        
                    }
                    
                }
                arrTotalObject.sort(by: {($0["CountryName"] as! String) < ($1["CountryName"] as! String)})
                var arrCurrencyCode = [String]()
                for object in arrTotalObject {
                    let currencyCode = object["CurrencyCode"]
                    if !arrCurrencyCode.contains(currencyCode ?? "") {
                        arrfilterlist.append(object)
                        arrCurrencyCode.append(currencyCode ?? "")
                    }
                }
                arrfilterlist.sort(by: {($0["CountryName"] as! String) < ($1["CountryName"] as! String)})
            }catch let error{
               
            }
        }
        
        return arrfilterlist
    }
}

