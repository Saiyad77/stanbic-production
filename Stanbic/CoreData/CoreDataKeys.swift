//
//  CoreDataKeys.swift
//  Stanbic
//
//  Created by Vijay Patidar on 16/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation

enum Entity : String {
    
    case DEVICEID, PIN, MSISDN,PROFILES_INFO, ACCOUNTS_INFO, ENQUIRY_ACCOUNTS_INFO, ENROLLMENTS_INFO, BENEFICIARIES_INFO, MONEY_TRANSFER_INFO,BANKS_INFO, BANK_BRANCHES_INFO, CURRENCIES_INFO, MNO_INFO, BILLERS_INFO, CARD_INFO,CONTACT_INFO,MOBILE_MONEY_INFO,SERVICES_INFO,MARKETING_INFO,ABOUT_US,NOTIFICATIONS_DATA,PROMOTIONS,WHAT_WE_OFFER,BIOMETRIC
    
    func Keys() -> [String] {
        switch self {
            
        case .DEVICEID:
            return ["aDEVICEID"]
            
        case .MSISDN:
            return ["aMSISDN"]
            
        case .PIN:
            return ["aPIN"]
            
        case .PROFILES_INFO:
            return ["aPROFILE_ID","aPIN_STATUS", "aOTHER_NAMES", "aFIRST_NAME", "aLAST_LOGIN"]
            
        case .ACCOUNTS_INFO:
            return ["aACCOUNT_ID", "aACCOUNT_NUMBER", "aACCOUNT_ALIAS", "aACCOUNT_TYPE", "aCURRENCY_CODE", "aCURRENCY_NUMBER", "aCURRENCY", "aPROFILE_ID", "aTARIFF_ID"]
            
        case .ENQUIRY_ACCOUNTS_INFO:
            return ["aACCOUNT_ID", "aACCOUNT_NUMBER", "aACCOUNT_ALIAS", "aACCOUNT_TYPE", "aCURRENCY_CODE", "aCURRENCY_NUMBER", "aCURRENCY", "aPROFILE_ID", "aTARIFF_ID"]
            
        case .ENROLLMENTS_INFO:
            return ["aBILLER_NAME", "aBILLER", "aBILLER_REFERENCE", "aENROLLMENT_ALIAS", "aDATE", "aBENEFICIARY_TYPE", "aMNO", "aMNO_WALLET_ID"]
            
        case .BENEFICIARIES_INFO:
            return ["aNOMINATION_ALIAS", "aNOMINATED_ACCOUNT", "aBANK_BRANCH_NAME", "aBANK_NAME", "aBANK_CODE", "aBRANCH_CODE", "aBENEFICIARY_TYPE", "aDATE"]
            
        case .MONEY_TRANSFER_INFO:
            return ["aMONEY_TRANSFER_PROVIDER", "aMONEY_TRANSFER_CODE", "aMTCN_LENGTH"]
            
        case .BANKS_INFO:
            return ["aBANK_ID", "aBANK", "aBANK_CODE", "aDATE","aPESALINK_STATUS"]
            
        case .BANK_BRANCHES_INFO:
            return ["aBANK_ID", "aBANK_BRANCH", "aBRANCH_CODE", "aDATE"]
            
        case .CURRENCIES_INFO:
            return ["aCURRENCY", "aCURRENCY_CODE", "aCURRENCY_NUMBER"]
            
        case .MNO_INFO:
            return ["aMNO_NAME", "aMNO_CODE", "aMNO_WALLET", "aMNO_WALLET_ID", "aORDER_ID", "aDATE","aREGEX"]
            
        case .BILLERS_INFO:
            return ["aBILLER_NAME", "aBILLER", "aACTIVE", "aDATE", "aBILLER_LOGO", "aPRESENTMENT_TYPE", "aREFERENCE_LABEL", "aHUB_SERVICEID", "aINACTIVE_MESSAGE"]
            
        case .CARD_INFO:
            return ["aCARD_ID", "aCARD_NAME", "aCARD_TYPE", "aCARD_ICON"]
        
        case .CONTACT_INFO:
            return ["aADDRESS","aCONTACT_TITLE","aEMAIL","aLOCATION","aPHONE_FAX","aDATE","aYOUTUBE","aTWITTER","aWEBSITE","aLINKEDIN","aFACEBOOK"]
            
        case .MOBILE_MONEY_INFO:
            return ["aACTIVE","aDATE","aINACTIVE_MESSAGE","aLOGO","aMNO_CODE","aMNO_NAME","aMNO_WALLET","aMNO_WALLET_ID","aORDER_ID","aREGEX"]
            
        case .SERVICES_INFO:
            return ["aACTIVE","aINACTIVE_MESSAGE","aSERVICE_CODE","aSERVICE_NAME","aCHARGE"]
            
        case .MARKETING_INFO:
            return ["aHEADER","aM_IMAGE","aM_IMAGE_URL","aM_TEXT","aMID","aM_CONTACT"]
            
        case .PROMOTIONS:
            return ["aCONTACT","aHEADER","aIMAGE_URL","aTEXT","aURL"]
            
        case .WHAT_WE_OFFER:
            return ["aCONTACT","aHEADER","aIMAGE_URL","aTEXT","aURL"]
            
        case .ABOUT_US:
            return ["aABOUT_US"]
            
        case .NOTIFICATIONS_DATA:
              return ["abody","atitle","aimageUrl","atime","alink","anotificationType","aviewstatus"]
        case .BIOMETRIC:
             return ["aNEW"]
        }
    }
}
