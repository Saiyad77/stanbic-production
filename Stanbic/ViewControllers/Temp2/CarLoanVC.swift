//
//  CarLoanVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 21/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

@objc protocol CarloanCellDelegate{
    func MoreButton(cell:CarLoanTableCellVC)
}

class CarLoanTableCellVC: UITableViewCell{
    
    
    @IBOutlet weak var lbldescr: UILabel!
    @IBOutlet weak var btnmore: UIButton!
    
    var cellDelegate: CarloanCellDelegate?
    
    override func awakeFromNib() {
        
        Fonts().set(object: lbldescr, fontType: 0, fontSize: 12, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: btnmore, fontType: 1, fontSize: 14, color: Color.white, title: "MORE", placeHolder: "")
        btnmore.backgroundColor = Color.c0_123_255
    }
    
    @IBAction func buttonMore(_ sender: UIButton) {
        cellDelegate?.MoreButton(cell: self)
    }
}


class CarLoanVC: UIViewController,CarloanCellDelegate {
    
    
    
    @IBOutlet weak var tblViewCarLoan: UITableView!
    
    var noDataLabel = UILabel()
    
    var loantype = ""
     var strtitle = ""
    
    var dict = [String:JSON]()
        var arrData = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetUpNavigationBar(headerTitle: "\(strtitle)")
       // arrCarLoan = [UserDefaults.standard.object(forKey: "AboutUs") as? String,UserDefaults.standard.object(forKey: "ImageUrl") as? String] as! [String]
        
        tblViewCarLoan.reloadData()
        
        noDataLabel = UILabel(frame: CGRect(x: 0, y: 10, width: 200, height: 18))
        noDataLabel.center.x = self.view.center.x
        noDataLabel.frame.origin.y = self.tblViewCarLoan.frame.minY + 270
        noDataLabel.numberOfLines = 0
        noDataLabel.textAlignment = .center
        noDataLabel.text = "No results found!"
        noDataLabel.font = UIFont.init(name: Fonts.kFontMedium, size: CGFloat(14))
        noDataLabel.textColor = .black
        self.noDataLabel.isHidden = true
        self.view.addSubview(noDataLabel)
        
        //        if arrAbout.count > 0 {
        //            noDataLabel.isHidden = true
        //        }
        //        else{
        //            noDataLabel.isHidden = false
        //        }
        
        tblViewCarLoan.rowHeight = UITableView.automaticDimension
        tblViewCarLoan.estimatedRowHeight = 160
        getloanDetails()
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    func initialSetUpNavigationBar(headerTitle:String)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headerTitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    @IBAction func barBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func MoreButton(cell: CarLoanTableCellVC) {
       // let indexpath = tblViewCarLoan.indexPath(for: cell)
        let url = dict["URL"]?.stringValue
        let viewController = WebViewController.getVCInstance() as! WebViewController
        viewController.url = url
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    static func getVCInstance() -> UIViewController {
        return UIStoryboard(name: Storyboards.Temp4.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
}

// MARK:_ TABLE VIEW DELEGATES:_
extension CarLoanVC  : UITableViewDelegate,UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dict.count > 0 {
        return 1
            
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as! CarLoanTableCellVC
        
        //cell.lbldesc.text = arrAbout[indexPath.section]
        let attributedString = NSMutableAttributedString(string:  dict["LOAN_INFO"]?.stringValue ?? "")
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = 7 // Whatever line spacing you want in points
        // *** Apply attribute to string ***
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        // *** Set Attributed String to your label ***
        cell.lbldescr.attributedText = attributedString
        cell.cellDelegate =  self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension CarLoanVC {
    
    func getloanDetails(){
        let loader = RestAPIManager.addProgress(title: "PLEASEWAIT", description: "WEAREPROCESSINGYOURREQUEST")
        RestAPIManager.loanEnquiries(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOURREQUEST", mobileNo: OnboardUser.mobileNo, LOAN_TYPE: loantype){(json) in
            DispatchQueue.main.async {
                loader.remove()
            }
              if json["STATUS_CODE"].intValue == 200 {
               
                self.dict = json["RESULTS_ARRAY"].dictionaryValue
                
               
                DispatchQueue.main.async {
                     self.tblViewCarLoan.reloadData()
                }
            }
              else {
                self.showMessage(title: "", message: json["STATUS_MESSAGE"].stringValue)
            }
        }
    }
    
}
