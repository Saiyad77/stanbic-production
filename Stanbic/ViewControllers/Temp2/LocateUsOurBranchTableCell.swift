//
//  LocateUsOurBranchTableCell.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 13/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class LocateUsOurBranchTableCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblBranchTitle: UILabel!
    @IBOutlet weak var lblBranchSubTitle: UILabel!
    @IBOutlet weak var lblKm: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    Fonts().set(object: lblBranchTitle, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: "Waiyaki way Branch", placeHolder: "")
    Fonts().set(object: lblBranchSubTitle, fontType: 0, fontSize: 11, color: Color.c134_142_150, title: "Lion Place off Waiyaki Way", placeHolder: "")
    Fonts().set(object: lblKm, fontType: 1, fontSize: 14, color: Color.c0_173_108, title: "0.7km", placeHolder: "")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
