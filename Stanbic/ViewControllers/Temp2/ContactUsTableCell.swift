//
//  ContactUsTableCell.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 27/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class ContactUsTableCell: UITableViewCell {

    @IBOutlet weak var imgObj: UIImageView!
    @IBOutlet weak var lblNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        Fonts().set(object: lblNumber, fontType: 1, fontSize: 16, color: Color.c74_74_74, title: "", placeHolder: "")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
