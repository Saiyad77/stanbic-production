//
//  ContactUsVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 13/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsNumberCollectionCell: UICollectionViewCell {

    @IBOutlet weak var lblNuber: UILabel!
    override func awakeFromNib() {
        Fonts().set(object: lblNuber, fontType: 1, fontSize: 12, color: Color.c0_123_255, title: "", placeHolder: "")
     }
}


class ContactUsVC: UIViewController {
    
    @IBOutlet weak var lblContactInfo: UILabel!
    @IBOutlet weak var collectionViewNumber: UICollectionView!
    @IBOutlet weak var btnEmailAddress: UIButton!
    @IBOutlet weak var btnWebsite: UIButton!
    
    
    var contactArr = [String]()
    var contactArr2 = [String]()

    //MARK: Variables
    var customertitle = ""
    var email = ""
    var website = ""
    var titleArr = ["Website","Email"]
    var headerTitle = ["WEBSITE","EMAIL", "TELEPHONE", "SOCIAL"]
 
    var youtube = ""
    var twiter = ""
    var facebook = ""
    var linkend = ""
    //MARK: View's life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        initialSetup()
        
        let coreData = CoreDataHelper()
        let getData  = coreData.getDataForEntity(entity: Entity.CONTACT_INFO)
        contactArr = getData[0]["aPHONE_FAX"]!.components(separatedBy: ",")
        email = getData[0]["aEMAIL"]!
        website = getData[0]["aWEBSITE"]!
        btnWebsite.setTitle(website, for: .normal)
        btnEmailAddress.setTitle(email, for: .normal)

        contactArr2 = getData[0]["aPHONE_FAX"]!.components(separatedBy: ",")

       
        if getData.count > 0 {
            
            youtube = getData[0]["aYOUTUBE"]!
            twiter = getData[0]["aTWITTER"]!
            facebook = getData[0]["aFACEBOOK"]!
            linkend = getData[0]["aLINKEDIN"]!
        }
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    
    
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func initialSetup() {
       // Fonts().set(object: lblTitleSocial, fontType: 0, fontSize: 16, color: Color.c10_34_64, title:"GETSOCIAL", placeHolder: "")
        initialSetUpNavigationBar(headerTitle:"CONTACTUS")

        let coreData = CoreDataHelper()
        let getData  = coreData.getDataForEntity(entity: Entity.CONTACT_INFO)
        customertitle = getData[0]["aCONTACT_TITLE"]!
         Fonts().set(object: lblContactInfo, fontType: 1, fontSize: 16, color: Color.c10_34_64, title: "Contact Information", placeHolder: "")
    }
    
    // MARK: IBACTION
    @IBAction func btnTappedYouTube(_ sender: UIButton) {
        let myUrl = youtube
        openSocialsitesinWebview(url: myUrl)
    }
    
    @IBAction func btnTappedTwiter(_ sender: UIButton) {
        let myUrl = twiter
        openSocialsitesinWebview(url: myUrl)
        
    }
    @IBAction func btnTappedFb(_ sender: UIButton) {
        let myUrl = facebook
        openSocialsitesinWebview(url: myUrl)
        
    }
    @IBAction func btnTappedLinkend(_ sender: UIButton) {
        let myUrl = linkend
        openSocialsitesinWebview(url: myUrl)
    }
    
    
    @IBAction func btnTappedWebsite(_ sender: UIButton) {
        let myUrl = website
        let viewController = WebViewController.getVCInstance() as! WebViewController
        viewController.url = myUrl
        navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func btnTappedEmail(_ sender: UIButton) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
   
    func openSocialsitesinWebview(url: String) {
        let viewController = WebViewController.getVCInstance() as! WebViewController
        viewController.url = url
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func initialSetUpNavigationBar(headerTitle:String)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headerTitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    

    @IBAction func barBtnBackTapped(_ sender: UIBarButtonItem) {
         global.cupApi = "NO"
        setUpSlideMenu()
    }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
}

extension ContactUsVC : MFMailComposeViewControllerDelegate {

    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property

        mailComposerVC.setToRecipients([email])
       // mailComposerVC.setSubject("Sending you an in-app e-mail...")
       // mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)

        return mailComposerVC
    }

    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }

    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}


extension ContactUsVC:  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contactArr2.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NumberCELL", for: indexPath) as! ContactUsNumberCollectionCell
        
        if indexPath.row == self.contactArr2.count - 1 {
            cell.lblNuber.text = self.contactArr2[indexPath.row]+""
        } else {
            cell.lblNuber.text = self.contactArr2[indexPath.row]+"/"
            
        }
         return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dialNumber(number: contactArr2[indexPath.row])
    }
    
    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }

}
