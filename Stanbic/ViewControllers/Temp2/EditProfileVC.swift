//
//  EditProfileVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 11/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnPickImg: UIButton!
    @IBOutlet weak var txtBeneficiaryBank: UITextField!
    @IBOutlet weak var txtBankBranch: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var btnEditDetail: AttributedButton!
    @IBOutlet weak var btnDeleteBeneficiary: AttributedButton!
    @IBOutlet weak var consBottombtn: NSLayoutConstraint!
    
    //MARK: Variables
     private var lastContentOffset: CGFloat = 0
    var name: String = ""
    var arrBankInfo = [String: String]()
    
    //MARK: View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
           initialFontSetup()
     
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
      // txtBankBranch.text = arrBankInfo["aENROLLMENT_ALIAS"]
        setData()
    
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    // MARK: Required methods
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func initialFontSetup()
    {
        Fonts().set(object: txtBeneficiaryBank, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
       txtBeneficiaryBank.addUI(placeholder: "BENEFICIARYBANK")
         Fonts().set(object: txtBankBranch, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        txtBankBranch.addUI(placeholder: "BANKBRANCH")
         Fonts().set(object: txtEmailAddress, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
         txtEmailAddress.addUI(placeholder: "EMAILADDRESS")
         Fonts().set(object: txtPhoneNumber, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
         txtPhoneNumber.addUI(placeholder: "PHONENUMBER")
          Fonts().set(object: btnEditDetail, fontType: 1, fontSize: 14, color: Color.white, title: "EDITDETAILS", placeHolder: "")
          Fonts().set(object: btnDeleteBeneficiary, fontType: 1, fontSize: 14, color: Color.c0_123_255, title: "DELETEBENEFICIARY", placeHolder: "")
        initialSetUpNavigationBar(headerTitle:name)
    }
    
    func initialSetUpNavigationBar(headerTitle:String)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headerTitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // set data to UI
    func setData() {
        if name == "Mobile"{
            txtBankBranch.text = arrBankInfo["aENROLLMENT_ALIAS"]!
            txtBeneficiaryBank.text = arrBankInfo["aBILLER_REFERENCE"]
            txtPhoneNumber.text = arrBankInfo["aBILLER"]
        }
        else if name == "Bank"{
            txtBankBranch.text = arrBankInfo["aNOMINATION_ALIAS"]!
            txtBeneficiaryBank.text = arrBankInfo["aNOMINATED_ACCOUNT"]
            txtPhoneNumber.text = arrBankInfo["aDATE"]
        }
        else {
            txtBankBranch.text = arrBankInfo["aENROLLMENT_ALIAS"]!
            txtBeneficiaryBank.text = arrBankInfo["aBILLER_REFERENCE"]
            txtPhoneNumber.text = arrBankInfo["aBILLER"]
        }
        
        txtPhoneNumber.didBegin()
        txtBeneficiaryBank.didBegin()
        txtBeneficiaryBank.didBegin()
    }
    
    //MARK:Actions
    @IBAction func btnTappedPickImage(_ sender: UIButton) {
    }
    
    @IBAction func btnTappedEditProfile(_ sender: AttributedButton) {
    }
    
    @IBAction func btnTappedDeleteBeneficiary(_ sender: AttributedButton) {
    }
    
    @IBAction func barBackBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


//Text field delegate
extension EditProfileVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
        
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
    

}

extension EditProfileVC
{
        // MARK: Keyboard hide/show Method
        @objc func keyboardWillShow(_ notification: Notification)
        {
            let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
            self.consBottombtn.constant = ((kb?.cgRectValue.height)! + 20)
            UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
        }
    
        @objc func keyboardWillHide(_ notification: Notification)
        {
            self.consBottombtn.constant = 100
            UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
        }
}

extension EditProfileVC : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        self.view.endEditing(true)
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}
