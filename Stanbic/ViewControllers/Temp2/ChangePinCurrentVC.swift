//
//  ChangePinCurrentVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 11/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class ChangePinCurrentVC: UIViewController {

    @IBOutlet weak var txtCurrentPin: UITextField!
    @IBOutlet weak var btnContinue: AttributedButton!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var showHideButton: UIButton!
    
    var checkOtp: Bool = true
    //MARK: View's Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
      initialSetup()
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        txtCurrentPin.keyboardType = .numberPad
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }

     // MARK: - Required Method's
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func initialSetup(){
      
        Fonts().set(object: lblSubTitle, fontType: 0, fontSize: 13, color: Color.c134_142_150, title: "FORYOURSECURITY", placeHolder: "")
        Fonts().set(object: txtCurrentPin, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        txtCurrentPin.addUI(placeholder: "ENTERCurrentPIN")
        Fonts().set(object: btnContinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        initialSetUpNavigationBar(headerTitle:"CHANGEPIN")
    }
    
    func initialSetUpNavigationBar(headerTitle:String)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headerTitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK: Actions
  //Check Current PIN
    @IBAction func btnTappedContinue(_ sender: AttributedButton)
    {
        var dataArray = CoreDataHelper().getDataForEntity(entity : Entity.PIN)
        if dataArray.count > 0
        {
            let data = Secure().decryptPIN(text: dataArray[0]["aPIN"]!)
            if data == txtCurrentPin.text
            {
                let nextvc = ChangePinNewVC.getVCInstance() as! ChangePinNewVC
                self.navigationController?.pushViewController(nextvc, animated: true)
            }
            else
            {
                //self.showMessage(title: "", message: "INVALIDPIN")
                txtCurrentPin.showError(errStr: "INVALIDPIN")
            }
        }
    }
    
    @IBAction func barBackbtnTapped(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func didShowHidePinButtonTapped(_ sender: Any) {
        if self.checkOtp {
            self.showHideButton.isSelected = true
            self.checkOtp = false
        } else {
            self.showHideButton.isSelected = false
            self.checkOtp = true
        }
        self.txtCurrentPin.isSecureTextEntry.toggle()
    }
}

//Text field delegate
extension ChangePinCurrentVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
        
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
}

extension ChangePinCurrentVC
{
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = 34
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}


extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
