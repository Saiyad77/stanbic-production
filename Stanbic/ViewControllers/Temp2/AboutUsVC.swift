//
//  AboutUsVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 13/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit


class AboutUsVCCell1: UITableViewCell {
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lbldesc: UILabel!
    
    override func awakeFromNib() {
        Fonts().set(object: lbltitle, fontType: 1, fontSize: 16, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: lbldesc, fontType: 0, fontSize: 12, color: Color.black, title: "", placeHolder: "")
    }
}

class AboutUsVCCell2: UITableViewCell {
  
    @IBOutlet weak var imgabout: UIImageView!
    
    override func awakeFromNib() {
       // imgabout.contentMode =
    }
}

class AboutUsVC: UIViewController {
    
    @IBOutlet weak var tblAboutUS: UITableView!
    
   var aboutUS = ""
    
    var arrAbout = [String]()
     var noDataLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetUpNavigationBar(headerTitle: "About us")
        arrAbout = [UserDefaults.standard.object(forKey: "AboutUs") as? String,UserDefaults.standard.object(forKey: "ImageUrl") as? String] as! [String]
        
        tblAboutUS.reloadData()
        
        noDataLabel = UILabel(frame: CGRect(x: 0, y: 10, width: 200, height: 18))
        noDataLabel.center.x = self.view.center.x
        noDataLabel.frame.origin.y = self.tblAboutUS.frame.minY + 270
        noDataLabel.numberOfLines = 0
        noDataLabel.textAlignment = .center
        noDataLabel.text = "No results found!"
        noDataLabel.font = UIFont.init(name: Fonts.kFontMedium, size: CGFloat(14))
        noDataLabel.textColor = .black
        self.noDataLabel.isHidden = true
        self.view.addSubview(noDataLabel)
        
        if arrAbout.count > 0 {
            noDataLabel.isHidden = true
        }
        else{
            noDataLabel.isHidden = false
        }
        
        tblAboutUS.rowHeight = UITableView.automaticDimension
        tblAboutUS.estimatedRowHeight = 160
        
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    //MARK: Required methods
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func initialSetUpNavigationBar(headerTitle:String)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headerTitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func barBtnBackTapped(_ sender: UIBarButtonItem) {
        global.cupApi = "NO"
        setUpSlideMenu()
    }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
}

// MARK:_ TABLE VIEW DELEGATES:_ 
extension AboutUsVC  : UITableViewDelegate,UITableViewDataSource {
    
  func numberOfSections(in tableView: UITableView) -> Int {
    return arrAbout.count
 
  }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
         let cell = tableView.dequeueReusableCell(withIdentifier: "AboutUsVCCell1") as! AboutUsVCCell1
          cell.lbltitle.text = "About"
          //cell.lbldesc.text = arrAbout[indexPath.section]
            
            let attributedString = NSMutableAttributedString(string:  arrAbout[indexPath.section])
            
            // *** Create instance of `NSMutableParagraphStyle`
            let paragraphStyle = NSMutableParagraphStyle()
            
            // *** set LineSpacing property in points ***
            paragraphStyle.lineSpacing = 7 // Whatever line spacing you want in points
            
            // *** Apply attribute to string ***
            attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
            
            // *** Set Attributed String to your label ***
           cell.lbldesc.attributedText = attributedString
         return cell
        }
        else{
           
        let cell = tableView.dequeueReusableCell(withIdentifier: "AboutUsVCCell2") as! AboutUsVCCell2
             cell.imgabout.sd_setImage(with: URL(string: arrAbout[indexPath.section]), placeholderImage: UIImage(named: "placeholderurl"))
            cell.imgabout.contentMode = .scaleAspectFit
        return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
        return UITableView.automaticDimension
        }else{
            return 200
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension
        }else{
            return 200
        }
    }
}
