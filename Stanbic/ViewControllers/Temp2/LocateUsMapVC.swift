//
//  LocateUsATMandBranchVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 14/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacePicker


class LocateUsMapVC: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate,UISearchBarDelegate,GMSAutocompleteViewControllerDelegate {
    
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var viewbtm: UIView!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet weak var locateUsImage: UIImageView!
    @IBOutlet var infoView: UIView!
    @IBOutlet weak var cardNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var cardTimeLabel: UILabel!
    @IBOutlet weak var cardDistanceLabel: UILabel!
    @IBOutlet weak var btnGetDirections: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var img_arrow: UIImageView!
    
    
    // View Location filter
    @IBOutlet var viewLeaves: UIView!
    @IBOutlet weak var lblSelectLeavesTitle: UILabel!
    @IBOutlet weak var tblViewLeaves: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
     @IBOutlet weak var viewBranches: UIView!
    
    var imgBlack : UIImageView?
    var arrNumberOfLeaves = [String]()
    var arrlocatorImage = [UIImage]()
    var flag = "0"
    var mapshow = ""
    var phoneno:Int?
    
    var userlatitude  = ""
    var userlongitude  = ""
    var destinationlatitude  = ""
    var destinationlongitude  = ""
    var locationManager = CLLocationManager()
    let CurrentLocMarker = GMSMarker()
    
    var resultArray = [String: JSON]()
    var atmArray = [JSON]()
    var cdmArray = [JSON]()
    var branchesArray = [JSON]()
    
     var allArray = [JSON]()
    
    var myloc = false
    let buttonBlue: UIColor = UIColor(red: 0/255.0, green: 118/255.0, blue: 255/255.0, alpha: 1.0)
    
    //MARK: View's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialSetup()
        Setmap() // SET GOOGLE MAP
        self.addShadowview(view : viewbtm)
        
       // infoView.isHidden = true
        
        self.infoView.frame = CGRect(x: 10, y: self.view.frame.size.height + 260, width: self.view.frame.width * 0.9, height: 230)
        
        setBorder()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    func setBorder()
    {
        btnGetDirections.layer.borderWidth = 1
        btnGetDirections.layer.borderColor = Color.c236_236_236.cgColor
        btnCall.layer.borderWidth = 1
        btnCall.layer.borderColor = Color.c236_236_236.cgColor
        btnShare.layer.borderWidth = 1
        btnShare.layer.borderColor = Color.c236_236_236.cgColor
        
        viewBranches.layer.shadowColor = UIColor.lightGray.cgColor
        viewBranches.layer.shadowOpacity = 0.5
        viewBranches.layer.shadowOffset = CGSize(width: 1, height: 1)
        viewBranches.layer.shadowRadius = 10
    }
    
    //MARK: Requiered Methods
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func initialSetup()
    {
        Fonts().set(object: lbltitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title:"", placeHolder: "")
        Fonts().set(object: lblSubTitle, fontType: 1, fontSize: 13, color: Color.c10_34_64, title:"ATMANDBRANCHES", placeHolder: "")
        Fonts().set(object: txtLocation, fontType: 0, fontSize: 14, color: Color.c134_142_150, title:"", placeHolder: "")
        lbltitle.text = "LOCATEUS".localized(Session.sharedInstance.appLanguage)
        txtLocation.text = "ENTERLOCATION".localized(Session.sharedInstance.appLanguage)
    }
    
    func setUpSlideMenu() {
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        leftVC.mainViewController = homeVC
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    func Setmap(){
        self.mapView.delegate = self
        self.locationManager.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        CurrentLocMarker.map = self.mapView
        self.locationManager.startMonitoringSignificantLocationChanges()
        self.locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 20)
    }
    
    //MARK:Actions
    @IBAction func barBtnBackTapped(_ sender: UIBarButtonItem)
    {
        global.cupApi = "NO"
        setUpSlideMenu()
    }
    
    // SHOW FILTER
    @IBAction func barBtnFilterTapped(_ sender: UIBarButtonItem) {
         hideView()
        UIView.animate(withDuration: 0.5) {
            self.img_arrow.transform = CGAffineTransform(rotationAngle: .pi)
        }
        arrNumberOfLeaves = ["ATM's","Branches","Cash Deposit Machines"]
        arrlocatorImage = [#imageLiteral(resourceName: "atm_icon"),#imageLiteral(resourceName: "branch_icon"),#imageLiteral(resourceName: "cdm_icon")]
        self.showPopup(popupView: viewLeaves, height: (arrNumberOfLeaves.count * 50) + 130)
    }
    
    @IBAction func btnNextVCTapped(_ sender: UIButton) {
        let nextvc = LocateUsOurBranchVC.getVCInstance() as! LocateUsOurBranchVC
        nextvc.userlatitude = userlatitude
        nextvc.userlongitude = userlongitude
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    
    @IBAction func btnlocationpicker(_ sender: UIButton) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    // MARK: - MARKER INFO WINDOW
    
    @IBAction func actionRemoveInfoView(_ sender: UIButton)
    {
        
        hideView()
    }
    
    @IBAction func btnActionshare(_ sender: UIButton)
    {
        handleButtonColor(sender)
        let textToShare = "Check It out \(cardNameLabel.text ?? "")"
        if let myWebsite = NSURL(string: "") {
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = sender
            self.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func btnActionGetDirections(_ sender: UIButton)
    {
        handleButtonColor(sender)

        let origin: CLLocation =  CLLocation(latitude: (userlatitude as NSString).doubleValue, longitude: (userlongitude as NSString).doubleValue)
        
         let destination: CLLocation =  CLLocation(latitude: (destinationlatitude as NSString).doubleValue, longitude: (destinationlongitude as NSString).doubleValue)
       
        drawPath(startLocation: origin, endLocation: destination)
    }
   
    
    
    @IBAction func btnActionCall(_ sender: UIButton)
    {
        handleButtonColor(sender)
        //MARK:- Replace with own Number
        let number = phoneno ?? 0
        
        guard let url = URL(string: "tel://\(number)") else {
            return //be safe
        }
        if #available(iOS 10.0, *)
        {
            UIApplication.shared.open(url)
        } else
        {
            UIApplication.shared.openURL(url)
        }
    }
}

extension LocateUsMapVC {
    
    // MARK:- locationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        self.CurrentLocMarker.position = (location?.coordinate)!
        self.CurrentLocMarker.title = "myLoc"
        var markerView = UIImageView()
        markerView = UIImageView(image: UIImage.init(named: "marker_image"))
        markerView.frame.size.width = 30
        markerView.frame.size.height = 30
        self.CurrentLocMarker.iconView = markerView
        self.CurrentLocMarker.map = self.mapView
        userlatitude = (location?.coordinate.latitude)!.description
        userlongitude = (location?.coordinate.longitude)!.description
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom:12)
        
        //self.mapView.animate(to: camera)
        // mapView.camera = camera
        
        let lat = (location?.coordinate.latitude)!.description
        let long = (location?.coordinate.longitude)!.description
        
        if flag == "0" {
        mapshow = "Userlocation"
        }
        // API ON MAP
        mapData(lat: lat, long: long, flag: flag)
        self.locationManager.stopUpdatingLocation()
        myloc = false
        
        CATransaction.begin()
        CATransaction.setValue(2.0, forKey: kCATransactionAnimationDuration)
        self.mapView.animate(to: camera)
        CATransaction.commit()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
       // print(error.localizedDescription)
    }
    
    // MARK:- googleMapsDelegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
   //     print("GMSMapView")
        
        
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
       // print("didTapMyLocationButton")
        mapView.clear()
        self.locationManager.startUpdatingLocation()
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
       // print("didTap marker")
        mapView.delegate = self
        if marker.title == "myLoc"
        {
            return true
        }
         let index:Int! = Int(marker.accessibilityLabel!)
        getmMarkerdetails(index: index)
        
        
        mapView.animate(toLocation: ( marker.position))
        mapView.selectedMarker = marker
        return true
    }
    
    
    // PLACEPICKER------------------------
    
    //MARK:_ GMSAutocompleteViewController
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        //userlatitude = place.coordinate.latitude.description
        //userlongitude = place.coordinate.longitude.description
        
        //userlatitude =
        //userlongitude = place.coordinate.longitude
        
        txtLocation.text = place.name
        dismiss(animated: true, completion: nil)
        
        mapView.clear()
        
        var markerView = UIImageView()
        markerView = UIImageView(image: UIImage.init(named: "current_location_icon"))
        markerView.frame.size.width = 30
        markerView.frame.size.height = 30
        self.CurrentLocMarker.iconView = markerView
        self.CurrentLocMarker.map = self.mapView
        
        mapshow = "Searchlocation"
        // mapView.camera = camera
        
        mapData(lat: userlongitude, long: userlongitude, flag: flag)
        
        Fonts().set(object: txtLocation, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
      //  print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension LocateUsMapVC: UITableViewDelegate, UITableViewDataSource {
    
    func showPopup(popupView : UIView, height : Int) {
        
        imgBlack = UIImageView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        imgBlack?.alpha = 0.5
        //imgBlack?.backgroundColor = colorConstant.kBlackColor
        imgBlack?.isUserInteractionEnabled = true
        self.view.addSubview(imgBlack!)
        
        let frm = CGRect(x: ((self.view.frame.size.width / 2) - ((self.view.frame.size.width * 0.9) / 2)), y: -(popupView.frame.size.height), width: (self.view.frame.size.width * 0.9), height: CGFloat(height))
        
        popupView.frame = frm
        
        self.view.addSubview(popupView)
        popupView.layer.borderWidth = 1
        popupView.layer.borderColor = Color.c236_236_236.cgColor
        
        popupView.layer.cornerRadius = 10
        popupView.clipsToBounds = true
        
        //  self.arrNumberOfLeaves.reverse()
        
        //  Fonts().set(object: lblSelectLeavesTitle, fontType: 1, fontSize: 20, color: Color.black, title: "NUMBEROFLEAVES", placeHolder: "")
        Fonts().set(object: btnCancel, fontType: 1, fontSize: 17, color: Color.black, title: "CANCEL", placeHolder: "")
        
        
        UIView.animate(withDuration: 0.5, animations: {
            
            let frm = CGRect(x: ((self.view.frame.size.width / 2) - ((self.view.frame.size.width * 0.9) / 2)), y: (self.view.frame.size.height / 2 - popupView.frame.size.height / 2), width: (self.view.frame.size.width * 0.9), height: CGFloat(height))
            
            popupView.frame = frm
            
        }){ (Bool) in
            
        }
    }
    
    
    // Dismiss popup
    func dismissPopup(popupView : UIView) {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            var frm =  popupView.frame
            
            frm.origin.y = -(popupView.frame.size.height)
            
            popupView.frame = frm
            
        }) { (Bool) in
            
            popupView.removeFromSuperview()
            
            self.imgBlack?.removeFromSuperview()
            
        }
    }
    @IBAction func btnCancelLeaves(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5) {
            self.img_arrow.transform = CGAffineTransform.identity
        }
        self.dismissPopup(popupView: viewLeaves)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrNumberOfLeaves.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LEAVES", for: indexPath) as! LeavesTableViewCell
        cell.lblNumberLeaves.text = arrNumberOfLeaves[indexPath.row]
        
        cell.imglocations.image = arrlocatorImage[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lblSubTitle.text = arrNumberOfLeaves[indexPath.row]
        self.locateUsImage.image = arrlocatorImage[indexPath.row]
        if arrNumberOfLeaves[indexPath.row] == "All ATMs,Branches & CDM"{
            flag = "0"
        }
        else if arrNumberOfLeaves[indexPath.row] == "ATM's"{
            flag = "1"
            mapshow = "Searchlocation"
        }
        else if arrNumberOfLeaves[indexPath.row] == "Branches"{
            flag = "2"
            mapshow = "Searchlocation"
        }
        else{
            //arrNumberOfLeaves[indexPath.row] == "CDM"{
            flag = "3"
            mapshow = "Searchlocation"
        }
        UIView.animate(withDuration: 0.5) {
              self.img_arrow.transform = CGAffineTransform.identity
        }
        self.dismissPopup(popupView: viewLeaves)
        mapView.clear()
        self.locationManager.startUpdatingLocation()
    }
}

extension LocateUsMapVC {
    
    func mapData(lat:String,long:String,flag:String)  {
        
          let loader = RestAPIManager.addProgress(title: "PLEASEWAIT", description: "WEAREPROCESSINGYOURREQUEST")
        
        RestAPIManager.fetchMapdata(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOURREQUEST",mobileNo: OnboardUser.mobileNo, branchflag: flag, longitude: long, latitude:lat){ (json) in
            
            DispatchQueue.main.async {
                loader.remove()
            }
            
            if json["STATUS_CODE"].intValue == 200 {
                DispatchQueue.main.async {
                    self.allArray.removeAll()
                    if flag == "0"{
                        self.resultArray = json["RESULTS_ARRAY"].dictionaryValue
                        let dictatm = self.resultArray["ATM"]?.dictionaryValue
                        self.atmArray = dictatm!["LOCATIONS"]!.arrayValue
                        
                        let dictcdm = self.resultArray["CDM"]?.dictionaryValue
                        self.cdmArray = dictcdm!["LOCATIONS"]!.arrayValue
                        
                        let dictbranch = self.resultArray["BRANCHES"]?.dictionaryValue
                        self.branchesArray = dictbranch!["LOCATIONS"]!.arrayValue
                        
                        
                    }
                    
                    if flag == "1"{
                        self.atmArray = json["RESULTS_ARRAY"].arrayValue
                    }
                    if flag == "2"{
                        self.branchesArray = json["RESULTS_ARRAY"].arrayValue
                    }
                    if flag == "3"{
                        self.cdmArray = json["RESULTS_ARRAY"].arrayValue
                    }
                    
                    for i in 0..<self.atmArray.count {
                      var dict = self.atmArray[i]
                        dict["LOCATION_FLAG"] = "1"
                     self.allArray.append(dict)
                    }
                    for j in 0..<self.branchesArray.count {
                            var dict = self.branchesArray[j]
                           dict["LOCATION_FLAG"] = "2"
                       self.allArray.append(dict)
                    }
                    for k in 0..<self.cdmArray.count {
                          var dict = self.cdmArray[k]
                         dict["LOCATION_FLAG"] = "3"
                        self.allArray.append(dict)
                    }
                    
                    self.addMarker(flag:flag)
                }
            }
            else {
                self.showMessage(title: "", message: json["STATUS_MESSAGE"].stringValue)
            }
        }
    }
    
    // MARK :_ get Marker details
    func getmMarkerdetails(index:Int){
        var distance = ""
        var name = ""
        var description = ""
        
        
//        didTap marker
//            {
//                "TITLE" : "Warwick",
//                "LATITUDE" : "-1.2360835",
//                "DESCRIPTION" : "Grnd Flr, Warwick Centre, UN Avenue",
//                "LONGITUDE" : "36.808975",
//                "PHONE_NUMBER" : "254203268888"
//        }
        
        if flag == "0"{
            let dict =   self.allArray[index].dictionaryValue
            name = dict["TITLE"]!.stringValue
            phoneno = Int(dict["PHONE_NUMBER"]!.stringValue)
            destinationlatitude = dict["LATITUDE"]!.stringValue
            destinationlongitude = dict["LONGITUDE"]!.stringValue
            description = dict["DESCRIPTION"]!.stringValue
        }
       else if flag == "1"{
            let dict = self.atmArray[index].dictionaryValue
            name = dict["TITLE"]!.stringValue
             phoneno = Int(dict["PHONE_NUMBER"]!.stringValue)
            destinationlatitude = dict["LATITUDE"]!.stringValue
            destinationlongitude = dict["LONGITUDE"]!.stringValue
            description = dict["DESCRIPTION"]!.stringValue
        }
        else if flag == "2"{
            let dict = self.branchesArray[index].dictionaryValue
            name = dict["TITLE"]!.stringValue
             phoneno = Int(dict["PHONE_NUMBER"]!.stringValue)
            destinationlatitude = dict["LATITUDE"]!.stringValue
            destinationlongitude = dict["LONGITUDE"]!.stringValue
            description = dict["DESCRIPTION"]!.stringValue
        }
        else if flag == "3"{
            let dict = self.cdmArray[index].dictionaryValue
            name = dict["TITLE"]!.stringValue
            phoneno = Int(dict["PHONE_NUMBER"]!.stringValue)
            destinationlatitude = dict["LATITUDE"]!.stringValue
            destinationlongitude = dict["LONGITUDE"]!.stringValue
            description = dict["DESCRIPTION"]!.stringValue
            
        }
        else {
            
        }
        
        let coordinate1 = CLLocation(latitude: (destinationlatitude as NSString).doubleValue, longitude: (destinationlongitude as NSString).doubleValue)
        let coordinate2 = CLLocation(latitude: (userlatitude as NSString).doubleValue, longitude: (userlongitude as NSString).doubleValue)
        
        let distanceInMeters = coordinate1.distance(from: coordinate2)
        let km = distanceInMeters / 1000
        
        self.cardDistanceLabel.text = String(format: "%.1f", km) + " km"
        //"\(km.description.prefix(2)) km"
        self.cardNameLabel.text = name
        self.descriptionLabel.text = description
        showInfoView()
    }
}
// LocateUsMapVC - LocateUsATMandBranchVC
extension LocateUsMapVC {
    
    func addMarker(flag:String){
        
        // 0 = all
        // 1 = atm
        // 2 braches
        // 3 = Cdm
        
        if flag == "0" {
         ALLshow()
        }
            
        else if flag == "1"{
            atmshow()
        }
            
        else if flag == "2"{
            brancheshow()
        }
            
        else if flag == "3"{
            cdmShow()
        }
        else {
            
        }
    }
    
    // ALL SHOWS  == 0
    func ALLshow(){
        
        for i in 0..<allArray.count {
            let dict = allArray[i].dictionaryValue

            let long = NSString(string: dict["LONGITUDE"]!.stringValue).doubleValue
            let  lat = NSString(string: dict["LATITUDE"]!.stringValue).doubleValue
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2DMake(lat, long)

            marker.map = self.mapView
            marker.map = self.mapView
            if dict["LOCATION_FLAG"]!.stringValue == "1"{
              marker.icon = #imageLiteral(resourceName: "atm_icon")
            }
            else if dict["LOCATION_FLAG"]!.stringValue == "2"{
            marker.icon = #imageLiteral(resourceName: "branch_icon")
            }
            else {
            marker.icon = #imageLiteral(resourceName: "cdm_icon")
            }
           
            marker.snippet = dict["TITLE"]?.stringValue
            marker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
            marker.accessibilityLabel = "\(i)"
            
            
//            if flag == "0"{
//                if mapshow == "Searchlocation"{
//                    if i == allArray.count-1{
//                        let camera = GMSCameraPosition.camera(withLatitude: (lat), longitude: (long), zoom:12)
//
//                        CATransaction.begin()
//                        CATransaction.setValue(2.0, forKey: kCATransactionAnimationDuration)
//                        self.mapView.animate(to: camera)
//                        CATransaction.commit()
//
////                        self.mapView.animate(to: camera)
//                    }
//                }
//            }


        }
    }
    
    // SHOW ATM  == 1
    func atmshow(){
        
        for i in 0..<atmArray.count {
            let dict = atmArray[i].dictionaryValue
            
            
            let long = NSString(string: dict["LONGITUDE"]!.stringValue).doubleValue
            let  lat = NSString(string: dict["LATITUDE"]!.stringValue).doubleValue
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2DMake(lat, long)
            
            marker.map = self.mapView
            marker.map = self.mapView
            marker.icon = #imageLiteral(resourceName: "atm_icon")
            marker.snippet = dict["TITLE"]?.stringValue
            marker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
            marker.accessibilityLabel = "\(i)"
        }
        
//        if flag == "1"{
//            if mapshow == "Searchlocation"{
//
//                    let camera = GMSCameraPosition.camera(withLatitude: (lat), longitude: (long), zoom:12)
//
//                    CATransaction.begin()
//                    CATransaction.setValue(2.0, forKey: kCATransactionAnimationDuration)
//                    self.mapView.animate(to: camera)
//                    CATransaction.commit()
//
//            }
//        }
    }
    
    // SHOW Branch  == 2
    func brancheshow(){
        
        for i in 0..<branchesArray.count {
            let dict = branchesArray[i].dictionaryValue
            
            let long = NSString(string: dict["LONGITUDE"]!.stringValue).doubleValue
            let  lat = NSString(string: dict["LATITUDE"]!.stringValue).doubleValue
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2DMake(lat, long)
            marker.map = self.mapView
            marker.map = self.mapView
            marker.icon = #imageLiteral(resourceName: "branch_icon")
            
            marker.snippet = dict["TITLE"]?.stringValue
            marker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
            marker.accessibilityLabel = "\(i)"
            
//            if flag == "2"{
//                if mapshow == "Searchlocation"{
//                    if i == branchesArray.count-1{
//                        let camera = GMSCameraPosition.camera(withLatitude: (lat), longitude: (long), zoom:12)
//
//                        CATransaction.begin()
//                        CATransaction.setValue(2.0, forKey: kCATransactionAnimationDuration)
//                        self.mapView.animate(to: camera)
//                        CATransaction.commit()
//                    }
//                }
//            }
            
        }
    }
    
    // SHOW CDM  == 3
    func cdmShow(){
        
        for i in 0..<cdmArray.count {
            let dict = cdmArray[i].dictionaryValue
            
            let long = NSString(string: dict["LONGITUDE"]!.stringValue).doubleValue
            let  lat = NSString(string: dict["LATITUDE"]!.stringValue).doubleValue
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2DMake(lat, long)
            marker.map = self.mapView
            marker.map = self.mapView
            marker.icon = #imageLiteral(resourceName: "cdm_icon")
            
            marker.snippet = dict["TITLE"]?.stringValue
            marker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
            marker.accessibilityLabel = "\(i)"
            
//            if flag == "3"{
//                if mapshow == "Searchlocation"{
//                    if i == cdmArray.count-1{
//                        let camera = GMSCameraPosition.camera(withLatitude: (lat), longitude: (long), zoom:12)
//
//                        CATransaction.begin()
//                        CATransaction.setValue(2.0, forKey: kCATransactionAnimationDuration)
//                        self.mapView.animate(to: camera)
//                        CATransaction.commit()
//                    }
//                }
//            }
        }
    }
    
}

extension LocateUsMapVC {
    
    func showInfoView()
    {
        UIView.animate(withDuration: 0.3)
        {
            self.view.addSubview(self.infoView)
            self.infoView.frame = CGRect(x: 20, y: self.view.frame.size.height - 260, width: self.view.frame.width * 0.9, height: 230)
            
        }
    }
    
    func hideView()
    {
        UIView.animate(withDuration: 0.3)
        {
            self.infoView.frame = CGRect(x: 10, y: self.view.frame.size.height + 260, width: self.view.frame.width * 0.9, height: 230)
            self.infoView.removeFromSuperview()
        }
    }
    
    func calculateDistance(lat:Double,long:Double)->Double
    {
        //let location1 = CLLocation(latitude: -1.291514, longitude: 36.874260)
        //    let location1 = CLLocation(latitude: currentLocation?.coordinate.latitude ?? 0.0, longitude: currentLocation?.coordinate.longitude ?? 0.0)
        //    let location2 = CLLocation(latitude: lat , longitude: long )
        //    let distance : CLLocationDistance = location1.distance(from: location2) / 1000
        //    let disSting = String(format: "%.1f", distance)
        //    let d = Double(disSting)
        return  0.0
    }
    
    func handleButtonColor(_ sender:UIButton)
    {
        if(sender.tag == 0)
        {
            sender.setTitleColor(UIColor.white, for: .normal)
            sender.backgroundColor = buttonBlue
           // sender.setImage(#imageLiteral(resourceName: "get_deirection"), for: .normal)
            self.btnCall.backgroundColor = UIColor.white
            self.btnCall.setTitleColor(UIColor.black, for: .normal)
            self.btnShare.backgroundColor = UIColor.white
            self.btnShare.setTitleColor(UIColor.black, for: .normal)
            btnShare.setImage(#imageLiteral(resourceName: "share"), for: .normal)
            btnCall.setImage(#imageLiteral(resourceName: "phone"), for: .normal)
        }
        else if(sender.tag == 1)
        {
            sender.setTitleColor(UIColor.white, for: .normal)
            sender.backgroundColor = buttonBlue
            sender.setImage(#imageLiteral(resourceName: "phone_white"), for: .normal)
            self.btnGetDirections.backgroundColor = UIColor.white
            btnGetDirections.setTitleColor(UIColor.black, for: .normal)
            btnGetDirections.setImage(#imageLiteral(resourceName: "get_directions"), for: .normal)
            self.btnShare.backgroundColor = UIColor.white
            self.btnShare.setTitleColor(UIColor.black, for: .normal)
            btnShare.setImage(#imageLiteral(resourceName: "share"), for: .normal)
        }
        else
        {
            sender.setTitleColor(UIColor.white, for: .normal)
            sender.backgroundColor = buttonBlue
            sender.setImage(#imageLiteral(resourceName: "share_white"), for: .normal)
            self.btnGetDirections.backgroundColor = UIColor.white
            btnGetDirections.setTitleColor(UIColor.black, for: .normal)
            btnGetDirections.setImage(#imageLiteral(resourceName: "get_directions"), for: .normal)
            self.btnCall.backgroundColor = UIColor.white
            self.btnCall.setTitleColor(UIColor.black, for: .normal)
            btnCall.setImage(#imageLiteral(resourceName: "phone"), for: .normal)
        }
        
    }
    
}

extension LocateUsMapVC {
    
    
    func drawPath(startLocation: CLLocation, endLocation: CLLocation)
    {
        //let origin = "\(startLocation.coordinate.latitude),\(startLocation.coordinate.longitude)"
       // let destination = "\(endLocation.coordinate.latitude),\(endLocation.coordinate.longitude)"
        
        
//        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving"
//
//        RestAPIManager.get_Direction(url: url,onCompletion: { (json) in
//         print(json)
//
//        })
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
        {
            if #available(iOS 10.0, *)
            {
                UIApplication.shared.open(NSURL(string:
                    "comgooglemaps://?saddr=&daddr=\(String(describing: Float(destinationlatitude)!)),\(String(describing: Float(destinationlongitude)!))&directionsmode=driving")! as URL)
            } else
            {
                UIApplication.shared.open(NSURL(string:
                    "comgooglemaps://?saddr=&daddr=\(String(describing: Float(destinationlatitude)!)),\(String(describing: Float(destinationlongitude)!))&directionsmode=driving")! as URL)
            }
        }
        else
        {
            let alertCon = UIAlertController(title: "Can't show directions", message: "To get direction, you may install Google Map application.", preferredStyle: .alert)
            let actionOkay = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
            alertCon.addAction(actionOkay)
            self.present(alertCon, animated: true)
            NSLog("Can't use com.google.maps://");
        }
    }
    
        
//        Alamofire.request(url).responseJSON { response in
//
//            print(response.request as Any)  // original URL request
//            print(response.response as Any) // HTTP URL response
//            print(response.data as Any)     // server data
//            print(response.result as Any)   // result of response serialization
//
//            let json = JSON(data: response.data!)
//            let routes = json["routes"].arrayValue
//
//            // print route using Polyline
//            for route in routes
//            {
//                let routeOverviewPolyline = route["overview_polyline"].dictionary
//                let points = routeOverviewPolyline?["points"]?.stringValue
//                let path = GMSPath.init(fromEncodedPath: points!)
//                let polyline = GMSPolyline.init(path: path)
//                polyline.strokeWidth = 4
//                polyline.strokeColor = UIColor.red
//                polyline.map = self.googleMaps
//            }
//
//        }
    
    
    func showDirectionsOnGoogleMap()
    {
        
        let lat = (destinationlatitude as NSString).doubleValue
        let long = (destinationlongitude as NSString).doubleValue
        
//        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
//        {
//            if #available(iOS 10.0, *)
//            {
//                UIApplication.shared.open(NSURL(string:
//                    "comgooglemaps://?saddr=&daddr=\(Float(lat!)),\(Float(long!))&directionsmode=driving")! as URL)
//            } else
//            {
//                UIApplication.shared.openURL(NSURL(string:
//                    "comgooglemaps://?saddr=&daddr=\(Float(lat!)),\(Float(long!))&directionsmode=driving")! as URL)
//            }
//        }
//        else
//        {
//            let alertCon = UIAlertController(title: "Can't show directions", message: "To get direction, you may install Google Map application.", preferredStyle: .alert)
//            let actionOkay = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
//            alertCon.addAction(actionOkay)
//            self.present(alertCon, animated: true)
//            NSLog("Can't use com.google.maps://");
//        }
    }
    
   func showRouteOnMap(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D) {
//
//        let sourcePlacemark = MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil)
//        let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil)
//
//        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
//        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
//
//        let sourceAnnotation = MKPointAnnotation()
//
//        if let location = sourcePlacemark.location {
//            sourceAnnotation.coordinate = location.coordinate
//        }
//
//        let destinationAnnotation = MKPointAnnotation()
//
//        if let location = destinationPlacemark.location {
//            destinationAnnotation.coordinate = location.coordinate
//        }
//
//        //self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
//
//        let directionRequest = MKDirectionsRequest()
//        directionRequest.source = sourceMapItem
//        directionRequest.destination = destinationMapItem
//        directionRequest.transportType = .automobile
//
//        // Calculate the direction
//        let directions = MKDirections(request: directionRequest)
//
//        directions.calculate {
//            (response, error) -> Void in
//
//            guard let response = response else {
//                if let error = error {
//                    if Session.sharedInstance.isPrint {
//                        print("Error: \(error)")
//                    }
//
//                    self.showDirectionsOnGoogleMap()
//                }
//
//                return
//            }
//            let route = response.routes[0]
//            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)
//            let rect = route.polyline.boundingMapRect
//            self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
//        }
    }
}
