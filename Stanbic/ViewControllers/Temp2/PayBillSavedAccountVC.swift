//
//  PayBillSavedAccountVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 13/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import SDWebImage

class PayBillSavedAccTableViewCell: UITableViewCell {
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var imgcards: UIImageView!

    func setfont(number:Int,title:String, url : String) {
        imgcards.layer.cornerRadius = imgcards.frame.height/2
        imgcards.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeholderurl"))
        Fonts().set(object: lbltitle, fontType: 1, fontSize: number, color: Color.black, title: title, placeHolder: "")
        Fonts().set(object: lblNumber, fontType: 0, fontSize: 12, color: Color.c142_142_147, title: title, placeHolder: "")
    }
    
    override func awakeFromNib() {
        imgcards.layer.cornerRadius = imgcards.frame.size.height / 2
        imgcards.layer.borderColor = Color.c233_233_235.cgColor
        imgcards.layer.borderWidth = 1.0
    }
}

class PayBillSavedAccountVC: UIViewController {

    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var tblpaybill: UITableView!
    @IBOutlet weak var txtserachfield: UITextField!
    @IBOutlet weak var viewSearch: AttributedView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblHeaderTitle: UIView!
    private var lastContentOffset: CGFloat = 0
    
    var billerShown = [[String : String]]()
    var paraBeniArray = [[String:String]]()
    var billerInfo = [[String : String]]()
    var resultInfo = [[String : String]]()
     var arrCards = [[String:String]]()
    var noDataLabel = UILabel()
    var model = paybillfetchdata()
    
    var contactno = ""
    var contactname = ""
    var nominateflag = ""
    
    var search = false
    var imgHeader =  UIImageView()
    var imgHederHeight : CGFloat = 0
    
    var cardno = ""
     var cardplace = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model.delegate = self
        setFonts()
        setupHeader()
        
        let bank_info =  EnrollmentInfo().getEnrollmentinforfor1(enrollmentinfo: CoreDataHelper().getDataForEntity(entity: Entity.ENROLLMENTS_INFO), type: "6")
        
        
        for i in 0..<bank_info.count
        {
            if Int(bank_info[i]["aBENEFICIARY_TYPE"]!)! == 6
            {
                paraBeniArray.append(bank_info[i])
            }
        }
        findBillerToShow()
        tblpaybill.reloadData()
        txtserachfield.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        resultInfo = billerShown
        noDataLabel = UILabel(frame: CGRect(x: 0, y: 10, width: 200, height: 18))
        noDataLabel.center.x = self.view.center.x
        noDataLabel.frame.origin.y = self.tblpaybill.frame.minY + 270
        noDataLabel.numberOfLines = 0
        noDataLabel.textAlignment = .center
        noDataLabel.text = "No results found!"
        noDataLabel.font = UIFont.init(name: Fonts.kFontMedium, size: CGFloat(14))
        noDataLabel.textColor = .black
        self.noDataLabel.isHidden = true
        self.view.addSubview(noDataLabel)
        
        
    NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
    
}

@objc func ReceivedNotification(notification: Notification) {
    
    let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
    appDelegate.window?.rootViewController = nextvc
    appDelegate.window?.makeKeyAndVisible()
    
}
    
    @objc func textFieldDidChange(textField: UITextField) {
//        resultInfo = billerShown.filter{
//            dictionary in
//
//            let match1 = dictionary["aBILLER_NAME"]!.lowercased().range(of: textField.text!.lowercased())
//            let match2 = dictionary["aBILLER_REFERENCE"]!.range(of: textField.text!)
//            return match1 != nil || match2 != nil  ? true : false
//            //return dictionary["showName"] == textField.text
//        }
//        if textField.text == ""{
//            resultInfo = billerShown
//        }
//        if resultInfo.count == 0{
//            self.noDataLabel.isHidden = false
//        }else{
//            self.noDataLabel.isHidden = true
//        }
        self.billerShown.removeAll()
        if textField.text?.count != 0 {
            for i in 0..<self.resultInfo.count {
                let dictdata = self.resultInfo[i]
                if (dictdata["aENROLLMENT_ALIAS"]!.lowercased()).contains((textField.text!).lowercased()){
                    billerShown.append(resultInfo[i])
                }
            }
        }
        search = true
        
        if billerShown.count > 0 {
            self.noDataLabel.isHidden = true
        }
        else{
            self.noDataLabel.isHidden = false
            if textField.text?.count == 0{
            search = false
                 self.noDataLabel.isHidden = true
            }
            
        }
        self.tblpaybill.reloadData()
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: "SAVEDACC", placeHolder: "")
        Fonts().set(object: self.lblHeaderTitle, fontType: 1, fontSize: 17, color: Color.white, title: "SAVEDACC", placeHolder: "")
        Fonts().set(object: self.txtserachfield, fontType: 0, fontSize: 13, color: Color.c10_34_64, title: "", placeHolder: "SEARCHFORSAVEDACC")
    }
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func BtnAdd(_ sender: AttributedButton) {
    }
}

//Text field delegate
extension PayBillSavedAccountVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " " {
            return false
        }
        return true
    }
}

//TableView
extension PayBillSavedAccountVC : UITableViewDelegate,UITableViewDataSource
{
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          //  return resultInfo.count
        if search == false{
            return resultInfo.count
        }
        else{
            return billerShown.count
        }
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! PayBillSavedAccTableViewCell
            cell.selectionStyle = .none
        cell.imgcards.contentMode = .scaleAspectFit
        
           if search == false{
            cell.lbltitle.text = self.resultInfo[indexPath.row]["aENROLLMENT_ALIAS"]
            cell.lblNumber.text = self.resultInfo[indexPath.row]["aBILLER_REFERENCE"]
            cell.imgcards.sd_setImage(with: URL(string: self.resultInfo[indexPath.row]["aBILLER_LOGO"] ?? ""), placeholderImage: nil)
        }
           else {
            cell.lbltitle.text = self.billerShown[indexPath.row]["aENROLLMENT_ALIAS"]
            cell.lblNumber.text = self.billerShown[indexPath.row]["aBILLER_REFERENCE"]
            cell.imgcards.sd_setImage(with: URL(string: self.billerShown[indexPath.row]["aBILLER_LOGO"] ?? ""), placeholderImage: nil)
        }
        
        
            return cell
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.resultInfo[indexPath.row]
        global.imgPaybill = ""
        if dict["aBILLER"] == "CARD"{
//         ["aBILLER_NAME": "Card Payment", "aBENEFICIARY_TYPE": "6", "aENROLLMENT_ALIAS": "Card Payment", "aBILLER_LOGO": "", "aBILLER": "CARD", "aMNO": "null", "aBILLER_REFERENCE": "5163420000137697", "aDATE": "2018-09-29 00:09:52", "aMNO_WALLET_ID": "null"]
            gocardVC(acno: dict["aBILLER_REFERENCE"] ?? "", dict: dict)
            
            
        }
        else{
        
        
        cardno = self.resultInfo[indexPath.row]["aBILLER_REFERENCE"]!
        cardplace = self.resultInfo[indexPath.row]["aREFERENCE_LABEL"]!
            
            global.imgPaybill = self.resultInfo[indexPath.row]["aBILLER_LOGO"] ?? ""
     // cardno =  "0732285"
        model.querybill(mobileNo: OnboardUser.mobileNo, biller: dict["aBILLER"]!, hubserviceid: dict["aHUB_SERVICEID"]!, billerrefrence: cardno)
        }
    }
    
    
        // SHow cradit card view contoller
    func gocardVC(acno:String,dict:[String:String]){
        let nextvc = PayBillSelectAccountViewController.getVCInstance() as! PayBillSelectAccountViewController
        
        if CoreDataHelper().getDataForEntity(entity: Entity.CARD_INFO).count > 0 {
        var cardInfo = CoreDataHelper().getDataForEntity(entity: Entity.CARD_INFO)[0]
        cardInfo["aCARD_TYPE"] =  dict["aBILLER"]
        nextvc.flow = "CREDITCARD"
        global.creditCardID = cardInfo["aCARD_ID"] ?? ""
        global.imgPaybill = cardInfo["aCARD_ICON"] ?? ""
        let dataModels = PayBillVM.init(dictPaybill:cardInfo, mobilenumber: contactno,mobilename: contactname, nominateflag: "No")
        nextvc.dataModels  = dataModels
            nextvc.savedcardno = acno
            nextvc.cardInfo = cardInfo
            self.navigationController?.pushViewController(nextvc, animated: true)
    }
   }
        
}

//MARK: ScrollViewDelegate
extension PayBillSavedAccountVC : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = imgHederHeight - (scrollView.contentOffset.y + 10)
        let height = min(max(y, 150), 400)
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
        
        if scrollView.contentOffset.y > 50 {
            self.lblHeaderTitle.isHidden = false
        }
        else {
            self.lblHeaderTitle.isHidden = true
        }
    }
    
    func setupHeader() {
        
        imgHederHeight = 210
        scrollView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        scrollView.delegate = self
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: imgHederHeight)
        imgHeader.contentMode = .scaleAspectFill
        imgHeader.image = UIImage.init(named: "GradientBar-3")
        imgHeader.clipsToBounds = true
        
        view.addSubview(imgHeader)
        self.view.bringSubviewToFront(scrollView)
        self.view.bringSubviewToFront(btnBack)
        self.view.bringSubviewToFront(lblHeaderTitle)
    }
    
    func findBillerToShow(){
        //this func fund the corresponding biller info in 'BILLERS_INFO' for "BENEFICIARY_TYPE": "6"
        
        for info in billerInfo{
            for beneInfo in paraBeniArray{
                if info["aBILLER"] == beneInfo["aBILLER"]{
                    var temp = info
                    temp["aBILLER_REFERENCE"] = beneInfo["aBILLER_REFERENCE"]
                    temp["aENROLLMENT_ALIAS"] = beneInfo["aENROLLMENT_ALIAS"]

                    billerShown.append(temp)
                }
                else if beneInfo["aBILLER"] == "CARD" {
                    var temp = beneInfo
                    temp["aBILLER_REFERENCE"] = beneInfo["aBILLER_REFERENCE"]
                    temp["aENROLLMENT_ALIAS"] = beneInfo["aENROLLMENT_ALIAS"]
                    temp["aBILLER_LOGO"] = ""
                    
                    billerShown.append(temp)
                }
                else{
                    
                }
            }
        }
        billerShown = noDuplicates(billerShown)
     }
    
    func noDuplicates(_ arrayOfDicts: [[String: String]]) -> [[String: String]] {
        var noDuplicates = [[String: String]]()
        var usedNames = [String]()
        for dict in arrayOfDicts {
            if let name = dict["aENROLLMENT_ALIAS"], !usedNames.contains(name) {
                noDuplicates.append(dict)
                usedNames.append(name)
            }
        }
        return noDuplicates
    }
    
}

// QueryBill API -
extension PayBillSavedAccountVC:paybillfetchdataVMDelegate {
    
    func fetchbilldata() {
        
        if model.responseData?.statusCode == 200 {
            let nextvc = PayBillSelectAccountViewController.getVCInstance() as! PayBillSelectAccountViewController
            nextvc.cardnumber = cardno
            nextvc.self.model = model
            nextvc.placecard = cardplace
            nextvc.flow = "Saved"
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
        else {
            // self.showMessage(title: "", message: model.responseData?.statusMessage ?? "")
            let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
            
            nextvc.message = model.responseData?.statusMessage ?? ""
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
}
