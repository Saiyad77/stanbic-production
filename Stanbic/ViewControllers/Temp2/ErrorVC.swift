//
//  ErrorVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 12/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class ErrorVC: UIViewController {

    
    @IBOutlet weak var btnCancel: AttributedButton!
   
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    
    var message = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        setFont ()
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func setFont ()  {
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 21, color: Color.red, title: "ERROR", placeHolder: "")
        Fonts().set(object: lblMsg, fontType: 0, fontSize: 16, color: Color.c52_58_64, title: message, placeHolder: "")
        Fonts().set(object: btnCancel, fontType: 1, fontSize: 14, color: Color.white, title: "CANCEL", placeHolder: "")
        //btnCancel.backgroundColor = Color.white
        
    }
    
    @IBAction func btnCancel(_ sender: AttributedButton) {
        
        
        self.navigationController?.popViewController(animated: true)
    }

}
