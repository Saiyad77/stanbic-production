//
//  PinChangeSuccessfullyVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 14/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class PinChangeSuccessfullyVC: UIViewController {
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDone: AttributedButton!
    
    var msg = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func initialSetup()
    {
        Fonts().set(object: lblTitle , fontType: 1, fontSize: 22, color: Color.c0_173_108, title: "CHANGEPINSUCCESSFULLY", placeHolder: "")
        Fonts().set(object: lblSubTitle , fontType: 0, fontSize: 17, color: Color.c52_58_64, title: msg, placeHolder: "")
        Fonts().set(object: btnDone, fontType: 1, fontSize: 14, color: Color.white, title: "DONE", placeHolder: "")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btnDoneTapped(_ sender: AttributedButton)
    {
         global.cupApi = "NO"
       setUpSlideMenu()
    }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
}
