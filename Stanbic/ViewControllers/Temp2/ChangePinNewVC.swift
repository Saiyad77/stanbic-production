//
//  ChangePinNewVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 11/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class ChangePinNewVC: UIViewController {

    @IBOutlet weak var btnChangePin: AttributedButton!
    @IBOutlet weak var txtRepeatPin: UITextField!
    @IBOutlet weak var txtNewPin: UITextField!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var btnShow: UIButton!
    @IBOutlet weak var btnShowtwo: UIButton!

    var showClick = true
    var showClick2 = true

    //MARK: View's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

       initialSetUp()
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
                txtRepeatPin.keyboardType = .numberPad
                txtNewPin.keyboardType = .numberPad
       
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }

    // MARK: - Required Method's
    
    static func getVCInstance() -> UIViewController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func initialSetUp() {
        Fonts().set(object: txtNewPin, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        txtNewPin.addUI(placeholder: "ENTERNEWPIN")
        Fonts().set(object: txtRepeatPin, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        txtRepeatPin.addUI(placeholder: "REPEATPIN")
        Fonts().set(object: btnChangePin, fontType: 1, fontSize: 14, color: Color.white, title: "CHANGE_PIN", placeHolder: "")
        initialSetUpNavigationBar(headerTitle:"CHANGEPIN")
        
        }
  
        func initialSetUpNavigationBar(headerTitle:String) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headerTitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK: Actions
    @IBAction func btnShowTapped(_ sender: UIButton)  {
 
        if self.showClick {
            self.btnShow.isSelected = true
            self.showClick = false
        } else {
            self.btnShow.isSelected = false
            self.showClick = true
        }
        self.txtNewPin.isSecureTextEntry.toggle()
     }

    
    @IBAction func btnShowTappedSecond(_ sender: UIButton)  {
        
        if self.showClick2 {
            self.btnShowtwo.isSelected = true
            self.showClick2 = false
        } else {
            self.btnShowtwo.isSelected = false
            self.showClick2 = true
        }
        self.txtRepeatPin.isSecureTextEntry.toggle()
    }
    
    
    @IBAction func barBtnBackTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
        }
    }
    
    @IBAction func btnChangePINTapped(_ sender: AttributedButton)
    {
        if !isInternetAvailable(){
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        //Coredata
        let coreData = CoreDataHelper()
        let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
        let  accAlias = acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!
        
        if txtNewPin.text == txtRepeatPin.text
        {
         
            let pin1 = Secure().encryptPIN(text : (self.txtNewPin?.text)!)
            let pin2 = Secure().encryptPIN(text : (self.txtRepeatPin?.text)!)
            
            RestAPIManager.change_Pin(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOURREQUEST", type: Response.self, pin1: "\(pin2)", pin2: "\(pin1)", accountAlias: accAlias, uAuthPin: OnboardUser.pin, completion: { (response) in
                DispatchQueue.main.async {
                    
                    if response.statusCode == 200 {
                        
                        CoreDataHelper().saveUUID(value: Secure().encryptPIN(text : (self.txtNewPin?.text)!), entityName: Entity.PIN.rawValue, key: Entity.PIN.rawValue, removeRecords: true)
                        
                        let nextVc = EnableTouchIDViewController.getVCInstance() as! EnableTouchIDViewController
                        self.navigationController?.pushViewController(nextVc, animated: true)
                    }
                    else{
                        //self.showMessage(title: "", message: response.statusMessage ?? "")
                        let nextVc = ErrorVC.getVCInstance() as! ErrorVC
                        nextVc.message =  response.statusMessage ?? ""
                        self.navigationController?.pushViewController(nextVc, animated: true)
                    }
                }
            })
            { (error) in
                
            }
        } else {
            self.showMessage(title: "", message: "INVALIDPIN")
        }
    }
}

//Text field delegate
extension ChangePinNewVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
 
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text == " " {
            return false
        }
        return true
    }
}

extension ChangePinNewVC {
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)  {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.consBottom.constant = 54
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}
