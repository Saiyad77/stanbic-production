//
//  SelectLoanVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 14/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit


class  SelectLoanCell: UICollectionViewCell {
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var viewbg: UIView!
    @IBOutlet weak var viewImgbg: UIView!
    
    override func awakeFromNib() {
        Fonts().set(object: lbltitle, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: "", placeHolder: "")
//        viewbg.layer.cornerRadius = 10
//        viewbg.layer.borderWidth = 0.5
//        viewbg.backgroundColor = UIColor.clear
//        var color1 = hexStringToUIColor(hex: "#868E96")
//        viewbg.layer.borderColor = color1.cgColor
        
        //   viewImgbg.roundCorners(corners: [.topLeft, .bottomLeft], radius: 10.0)
        
        Global.createCardView(view: viewbg)
        
     //           Fonts().set(object: self.lbltitle, fontType: 0, fontSize: 12, color: Color.black, title: "", placeHolder: "")
    }
    // Setdata
    func setdata(title:String, imageName : String) {
        lbltitle.text = title
        img.image = UIImage.init(named: imageName)
    }
}


class SelectLoanVC: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var collectionViewObj: UICollectionView!
    var arrtitle = ["Loans","Loan Calculator"]
    var arrImages = ["loan_service","loan_calculator"]
    
                
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetUpNavigationBar(headerTitle: "Loan Services")
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp4.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }

    func initialSetUpNavigationBar(headerTitle:String)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headerTitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func barBtnBackTapped(_ sender: UIBarButtonItem) {
         // self.navigationController?.popViewController(animated: true)
        global.cupApi = "NO"
        setUpSlideMenu()
    }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }

}
extension SelectLoanVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return arrtitle.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath as IndexPath) as! SelectLoanCell
        // cell.lbltitle.text = arrtitle[indexPath.row]
        // cell.img.image = UIImage(named: arrImages[indexPath.row])
         cell.setdata(title: arrtitle[indexPath.row], imageName: arrImages[indexPath.row])
        
        
      return cell
}
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath as IndexPath) as! SelectLoanCell
        
       
        if indexPath.row == 0{
            cell.viewbg.backgroundColor?.withAlphaComponent(0.5)
            cell.viewbg.layer.backgroundColor = (UIColor.blue).cgColor

            let vc = SelectGetLoanViewController.getVCInstance() as! SelectGetLoanViewController
             self.navigationController?.pushViewController(vc, animated: true)
        }else {
            cell.viewbg.backgroundColor?.withAlphaComponent(0.5)
            cell.viewbg.layer.backgroundColor = (UIColor.blue).cgColor

            let vc = LoanCalculaterSelectViewController.getVCInstance() as! LoanCalculaterSelectViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if DeviceType.IS_IPHONE_5 {
        
        let collectionViewSize = ((self.collectionViewObj.frame.size.width - 60)/2)
        return CGSize(width : collectionViewSize,height: collectionViewSize - 10)
            
        }
        else{
            let collectionViewSize = ((self.collectionViewObj.frame.size.width - 60)/2)
            return CGSize(width : collectionViewSize,height: collectionViewSize - 30)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
  
            return UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 20)
    
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
    
}
