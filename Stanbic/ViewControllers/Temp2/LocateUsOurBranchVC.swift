//
//  LocateUsOurBranchVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 13/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces

class LocateUsOurBranchVC: UIViewController {
    
    
    @IBOutlet weak var lblFilterTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewBranches: UIView!
    
    // View Location filter
    @IBOutlet var viewLeaves: UIView!
    @IBOutlet weak var lblSelectLeavesTitle: UILabel!
    @IBOutlet weak var tblViewLeaves: UITableView!
    @IBOutlet weak var tableviewlocateus: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var img_arrow: UIImageView!
    @IBOutlet weak var locateUsImage: UIImageView!
    
    var imgBlack : UIImageView?
    var arrNumberOfLeaves = [String]()
    var arrlocatorImage = [UIImage]()
    var flag = "0"
    var userlatitude  = ""
    var userlongitude  = ""
    
    var arrData = [JSON]()
    
    var arrAtm = [JSON]()
    var arrBranches = [JSON]()
    var arrCdm = [JSON]()
    
    //MARK:View's Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialSetUp()
        viewBranches.layer.shadowColor = UIColor.lightGray.cgColor
        viewBranches.layer.shadowOpacity = 0.5
        viewBranches.layer.shadowOffset = CGSize(width: 1, height: 1)
        viewBranches.layer.shadowRadius = 10
        mapData(lat: userlongitude, long: userlatitude, flag: flag)
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        tableviewlocateus.rowHeight = UITableView.automaticDimension
        tableviewlocateus.estimatedRowHeight = 75
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func initialSetUp(){
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "LOCATEUS", placeHolder: "")
        Fonts().set(object: lblFilterTitle, fontType: 1, fontSize: 13, color: Color.c10_34_64, title: "OURBRANCHES", placeHolder: "")
        
    }
    
    //MARK: Actions
    @IBAction func btnFilterTapped(_ sender: UIButton)
    {
        UIView.animate(withDuration: 0.5) {
            self.img_arrow.transform = CGAffineTransform(rotationAngle: .pi)
        }
        arrNumberOfLeaves = ["ATM's","Branches","Cash Deposit Machines"]
       arrlocatorImage = [#imageLiteral(resourceName: "atm_icon"),#imageLiteral(resourceName: "branch_icon"),#imageLiteral(resourceName: "cdm_icon")]
        self.showPopup(popupView: viewLeaves, height: (arrNumberOfLeaves.count * 50) + 130)
        
    }
    
    @IBAction func btnMapTapped(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBackTapped(_ sender: UIButton)
    {
        global.cupApi = "NO"
        setUpSlideMenu()
    }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    @IBAction func btnCancelLeaves(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5) {
            self.img_arrow.transform = CGAffineTransform.identity
        }
        self.dismissPopup(popupView: viewLeaves)
    }
}

extension LocateUsOurBranchVC: UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView == tblViewLeaves {
            return 1
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblViewLeaves {
            return arrNumberOfLeaves.count
        }
        else {
            if flag == "0"{
                return arrData.count
            }
            else if  flag == "1"{
               return arrAtm.count
            }
            else if  flag == "2"{
                 return arrBranches.count
            }
            else {
                 return arrCdm.count
            }
           
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblViewLeaves {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LEAVES", for: indexPath) as! LeavesTableViewCell
            cell.lblNumberLeaves.text = arrNumberOfLeaves[indexPath.row]
            cell.imglocations.image = arrlocatorImage[indexPath.row]
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! LocateUsOurBranchTableCell
            
            var dict = [String:JSON]()
            if flag == "0"{
                self.arrData.sort(by: {$0["TITLE"] <  $1["TITLE"]})
                dict = self.arrData[indexPath.row].dictionaryValue
            }
            else if  flag == "1"{
                self.arrAtm.sort(by: {$0["TITLE"] <  $1["TITLE"]})
                dict = self.arrAtm[indexPath.row].dictionaryValue
            }
            else if  flag == "2"{
                self.arrBranches.sort(by: {$0["TITLE"] <  $1["TITLE"]})
                dict = self.arrBranches[indexPath.row].dictionaryValue
            }
            else {
                self.arrCdm.sort(by: {$0["TITLE"] <  $1["TITLE"]})
                dict = self.arrCdm[indexPath.row].dictionaryValue
            }
            
            cell.lblBranchTitle.text = dict["TITLE"]?.stringValue
            cell.lblBranchSubTitle.text = dict["DESCRIPTION"]?.stringValue
            let lat = dict["LATITUDE"]!.stringValue
            let long = dict["LONGITUDE"]!.stringValue
            let coordinate1 = CLLocation(latitude: (lat as NSString).doubleValue, longitude: (long as NSString).doubleValue)
            let coordinate2 = CLLocation(latitude: (userlatitude as NSString).doubleValue, longitude: (userlongitude as NSString).doubleValue)
            let distanceInMeters = coordinate1.distance(from: coordinate2)
            let km = distanceInMeters / 1000
            cell.lblKm.text = "\(String(format: "%.2f", km)) km"
            cell.lblBranchTitle.numberOfLines = 0 
            
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblViewLeaves {
            lblFilterTitle.text = arrNumberOfLeaves[indexPath.row]
            self.locateUsImage.image = arrlocatorImage[indexPath.row]
            if arrNumberOfLeaves[indexPath.row] == "All ATMs,Branches & CDM"{
                flag = "0"
            }
            else if arrNumberOfLeaves[indexPath.row] == "ATM's"{
                flag = "1"
            }
            else if arrNumberOfLeaves[indexPath.row] == "Branches"{
                flag = "2"
            }
            else{
                //arrNumberOfLeaves[indexPath.row] == "CDM"{
                flag = "3"
            }
            UIView.animate(withDuration: 0.5) {
                self.img_arrow.transform = CGAffineTransform.identity
            }
            self.dismissPopup(popupView: viewLeaves)
           // tableviewlocateus.reloadData()
           mapData(lat: userlongitude, long: userlatitude, flag: flag)
        }
        else {
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let animation = AnimationFactory.makeFade(duration: 0.5, delayFactor: 0.0)
//        let animator = Animator(animation: animation)
//        animator.animate(cell: cell, at: indexPath, in: tableView)
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension LocateUsOurBranchVC {
    
    func showPopup(popupView : UIView, height : Int) {
        
        imgBlack = UIImageView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        imgBlack?.alpha = 0.5
        //imgBlack?.backgroundColor = colorConstant.kBlackColor
        imgBlack?.isUserInteractionEnabled = true
        self.view.addSubview(imgBlack!)
        
        let frm = CGRect(x: ((self.view.frame.size.width / 2) - ((self.view.frame.size.width * 0.9) / 2)), y: -(popupView.frame.size.height), width: (self.view.frame.size.width * 0.9), height: CGFloat(height))
        
        popupView.frame = frm
        //popupView.backgroundColor = Color.black
        
        popupView.layer.borderWidth = 1
        popupView.layer.borderColor = Color.c236_236_236.cgColor
        
        self.view.addSubview(popupView)
        popupView.layer.cornerRadius = 10
        popupView.clipsToBounds = true
        
        //  self.arrNumberOfLeaves.reverse()
        
      //   Fonts().set(object: lblSelectLeavesTitle, fontType: 1, fontSize: 20, color: Color.black, title: "ATMANDBRANCHES", placeHolder: "")
        Fonts().set(object: btnCancel, fontType: 1, fontSize: 17, color: Color.black, title: "CANCEL", placeHolder: "")
        
        
        UIView.animate(withDuration: 0.5, animations: {
            
            let frm = CGRect(x: ((self.view.frame.size.width / 2) - ((self.view.frame.size.width * 0.9) / 2)), y: (self.view.frame.size.height / 2 - popupView.frame.size.height / 2), width: (self.view.frame.size.width * 0.9), height: CGFloat(height))
            
            popupView.frame = frm
            
        }){ (Bool) in
            
        }
    }
    
    
    // Dismiss popup
    func dismissPopup(popupView : UIView) {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            var frm =  popupView.frame
            
            frm.origin.y = -(popupView.frame.size.height)
            
            popupView.frame = frm
            
        }) { (Bool) in
            
            popupView.removeFromSuperview()
            
            self.imgBlack?.removeFromSuperview()
            
        }
    }
  }

extension LocateUsOurBranchVC {
    
    func mapData(lat:String,long:String,flag:String)  {
        
         let loader = RestAPIManager.addProgress(title: "PLEASEWAIT", description: "WEAREPROCESSINGYOURREQUEST")
        
        RestAPIManager.fetchMapdata(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOURREQUEST",mobileNo: OnboardUser.mobileNo, branchflag: "0", longitude: long, latitude:lat){ (json) in
            DispatchQueue.main.async {
                loader.remove()
            }
            if json["STATUS_CODE"].intValue == 200 {
                DispatchQueue.main.async {
                    self.arrData.removeAll()
                    
                    let resultArray = json["RESULTS_ARRAY"].dictionaryValue
                    let dictatm = resultArray["ATM"]?.dictionaryValue
                    let dictcdm = resultArray["CDM"]?.dictionaryValue
                    let dictbranch = resultArray["BRANCHES"]?.dictionaryValue
                    
                    if flag == "0"{
                        
                       for i in 0..<(dictatm!["LOCATIONS"]!).arrayValue.count {
                            self.arrData.append((dictatm!["LOCATIONS"]!).arrayValue[i])
                        }
                        for i in 0..<(dictcdm!["LOCATIONS"]!).arrayValue.count {
                            self.arrData.append((dictcdm!["LOCATIONS"]!).arrayValue[i])
                        }
                        for i in 0..<(dictbranch!["LOCATIONS"]!).arrayValue.count {
                            self.arrData.append((dictbranch!["LOCATIONS"]!).arrayValue[i])
                        }
                     }
                     else if flag == "1"{
                         self.arrAtm.removeAll()
                        for i in 0..<(dictatm!["LOCATIONS"]!).arrayValue.count {
                            self.arrAtm.append((dictatm!["LOCATIONS"]!).arrayValue[i])
                        }
                    }
                     else if flag == "2" {
                            self.arrBranches.removeAll()
                        for i in 0..<(dictbranch!["LOCATIONS"]!).arrayValue.count {
                            self.arrBranches.append((dictbranch!["LOCATIONS"]!).arrayValue[i])
                        }
                    }
                     else{
                        self.arrCdm.removeAll()
                        for i in 0..<(dictcdm!["LOCATIONS"]!).arrayValue.count {
                            self.arrCdm.append((dictcdm!["LOCATIONS"]!).arrayValue[i])
                        }
                    }
//                    if flag == "1"{
//                        self.arrAtm = json["RESULTS_ARRAY"].arrayValue
//                    }
//                    if flag == "2"{
//                        self.arrBranches = json["RESULTS_ARRAY"].arrayValue
//                    }
//                    if flag == "3"{
//                        self.arrCdm = json["RESULTS_ARRAY"].arrayValue
//                    }
                    self.tableviewlocateus.reloadData()
                 }
             }
            else {
                self.showMessage(title: "", message: json["STATUS_MESSAGE"].stringValue)
            }
        }
    }
    

}
