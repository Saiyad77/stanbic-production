//
//  PayBillTransactionVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 23/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class PayBillTransactionVC: UIViewController {

    // MARK:- IBOUTLETS
    @IBOutlet weak var txtBussinessNo: UITextField!
    @IBOutlet weak var txtamount: UITextField!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var Clsprice: UICollectionView!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lbltranserfrom: UILabel!
    @IBOutlet weak var txtAccNumber: UITextField!
    
    // MARK:- Varibales
    
    // Variables for Bottom sheet
    var editableTextfield : UITextField?
    private var lastContentOffset: CGFloat = 0
    let Pricearr = ["100", "250","500","1000"]
    var isMobileEditing : Bool = true
    
    // Variables for Bottom sheet
    var bottomSheetVC: BottomSheetViewController?
    var imgView = UIImageView()
    var accountno = ""
    var indexcolletion = IndexPath()
    var dictdata = NSMutableDictionary()
    var selectnetworok = true
    var aMNO_WALLET_ID = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add Bottom sheet VC
        addBottomSheetView()
        bottomSheetVC?.delegate = self
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        Clsprice?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
        setFonts()
        Clsprice.dataSource = self
        Clsprice.delegate = self
        txtBussinessNo.delegate = self
        txtamount.delegate = self
        
        txtamount.text = "KES"
        
        txtamount.keyboardType = .numberPad
        
        txtAccNumber.keyboardType = .alphabet
        self.tabBarController?.tabBar.isHidden = true

 
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isMobileEditing == true {
            txtBussinessNo.isEnabled = true
        }
        else {
            txtBussinessNo.didBegin()
            txtBussinessNo.isEnabled = false

        }
        
        
        
        
        if txtamount.text!.count > 5 {
         //   print("amount greter than 5")
         } else {
            txtamount.didBegin()
            txtamount.becomeFirstResponder()
        }
        
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp4.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        if CurrentFlow.flowType == FlowType.PayBillTransaction{
            //Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: "ACCOUNTTOPHONE", placeHolder: "")
            Fonts().set(object: self.lblsubtitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "HOWWOULDYOULIKETOPAY", placeHolder: "")
            Fonts().set(object: self.lbltranserfrom, fontType: 0, fontSize: 12, color: Color.c0_123_255, title: "PAYFROM", placeHolder: "")
            Fonts().set(object: self.txtBussinessNo, fontType: 0, fontSize: 12, color: Color.c0_123_255, title: "PAYFROM", placeHolder: "")
            txtBussinessNo.addUI(placeholder: "BUSINESSNUMBER")
            txtAccNumber.addUI(placeholder: "ACCOUNTNUMBER")
            initialSetUpNavigationBar(headertitle:"PAYBILL")
        }
        
      
        let accountArray = CoreDataHelper().getDataForEntity(entity: Entity.ACCOUNTS_INFO)
        if accountArray.count > 0 {
            let accountInfo = accountArray[0]
            let acNo = (accountInfo["aACCOUNT_NUMBER"] ?? "").hashingOfTheAccount(noStr: (accountInfo["aACCOUNT_NUMBER"] ?? ""))
            Fonts().set(object: self.lblaccount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: accountInfo["aACCOUNT_ALIAS"] ?? "", placeHolder: "")
            Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: acNo, placeHolder: "")
            accountno = accountInfo["aACCOUNT_NUMBER"] ?? ""
        }
        
        Fonts().set(object: self.txtamount, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
         txtamount.addUI(placeholder: "ENTERAMOUNT")
        Fonts().set(object: self.txtBussinessNo, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
      txtBussinessNo.addUI(placeholder: "BUSINESSNUMBER")
        Fonts().set(object: self.txtAccNumber, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        txtAccNumber.addUI(placeholder: "ACCOUNTNUMBER")
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        
    }
    
    // MARK: - Required Method's
    func initialSetUpNavigationBar(headertitle:String){
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headertitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        self.view.endEditing(true)
        if lblaccountnumber.text == ""{
            showMessage(title: "", message: "SELECTACCOUNT")
            return
        }
        let amounts = getAmountValdiation1(service_code : "ACCBILL")
        
        let min = amounts.0
        let max = amounts.1
        let amount = removeKES(price : txtamount.text ?? "")
        let msg = "\("AMOUNTBEETWEEN".localized(Session.sharedInstance.appLanguage)) \(min) \("-".localized(Session.sharedInstance.appLanguage)) \(max) "
        
        
        if txtamount.text == "KES "{
            txtamount.showError(errStr: "ENTERAMOUNT")
        }
        else if checkAmount(min:Double(min),max:Double(max),amount:amount) == false {
            txtamount.showError(errStr: msg)
        }
        else if txtBussinessNo.text == ""{
            txtBussinessNo.showError(errStr: "ENTERBUSSINESSNOPLACEHOLDER")
        }
        else if lblaccountnumber.text == ""{
            showMessage(title: "", message: "SELECTACCOUNT")
        }else if txtAccNumber.text == ""{
             txtAccNumber.showError(errStr: "ENTERACCOUNTNOPLACEHOLDER")
        }
        else if getgenralRegax(value: txtAccNumber.text!, type: "genral") == false{
            txtAccNumber.showError(errStr: "GENRALREGAX")
        }
        else {
            let dataModels = BuyaTimeVM.init(mobilenumber: txtBussinessNo.text!, mobilename: txtAccNumber.text!, acalias: lblaccount.text!, aMNO_WALLET_ID: aMNO_WALLET_ID, amount:txtamount.text!, acnumber: lblaccountnumber.text!,nominateflag: "")
            let nextvc = CheckoutAirTimeViewController.getVCInstance() as! CheckoutAirTimeViewController
            nextvc.dataModels = dataModels
             nextvc.accountno = accountno
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
    
    @IBAction func accountselect(_ sender: AttributedButton) {
        view.endEditing(true)
        selectnetworok = false
        self.showBottomSheetVC(xibtype: "ACCOUNT", Selectname: lblaccount.text!)
    }
    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
    
    func showBottomSheetVC(xibtype: String,Selectname: String) {
        
        var height = 300  // 340
        let coreData = CoreDataHelper()
        if xibtype == "ACCOUNT" {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
                height = acdata.count * 100
                if acdata.count == 1 {
                    height = 200
                }
            }
        }
        else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acnetwork  = coreData.getDataForEntity(entity: Entity.MNO_INFO)
                height = acnetwork.count * 200
            }
        }
        
        // bottomSheetVC?.partialView = UIScreen.main.bounds.height - CGFloat(height)
        bottomSheetVC!.partialView = self.view.frame.size.height - CGFloat(height)
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imgView.backgroundColor = Color.black
        imgView.alpha = 0.2
        imgView.isUserInteractionEnabled = true
        self.view.addSubview(imgView)
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self as? UIGestureRecognizerDelegate
        imgView.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "", SelectName: "")
        imgView.removeFromSuperview()
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
}

//Text field delegate
extension PayBillTransactionVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtamount {
            txtamount.text = "KES "
            indexcolletion = IndexPath()
            Clsprice.reloadData()
        }
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtamount {
            
            textField.typingAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            let protectedRange = NSMakeRange(0, 4)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                
                return false
            }
            return true
        }
        else{
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

//Text field delegate
extension PayBillTransactionVC : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Pricearr.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PricebuttonCell1", for: indexPath as IndexPath) as! PricebuttonCell1
        if indexcolletion == indexPath {
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.white, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = Color.c0_123_255
        }else{
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = .clear
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        indexcolletion = indexPath
        txtamount.text = "KES \(self.Pricearr[indexPath.row])"
        txtamount.removeError()
        Clsprice.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.Clsprice.frame.width/4) - 8
        return CGSize(width: width, height: collectionView.frame.size.height - 5)
    }
    
}

extension PayBillTransactionVC : SelectNetworkDelegate{
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
        lblaccountnumber.text =  acnumber
        lblaccount.text = acname
    }
    
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
        hideBottomSheetVC()
        lblaccount.text = newtorkname
        lblaccountnumber.text = networkid.hashingOfTheAccount(noStr: networkid)
        accountno = networkid
    }
}

extension PayBillTransactionVC : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //  self.view.endEditing(true)
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}
