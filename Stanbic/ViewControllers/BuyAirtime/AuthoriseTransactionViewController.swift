//
//  AuthoriseTransactionViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 20/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class AuthoriseTransactionViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    
    //Mark:- Varibales
    var modelVerifyAuthKey : VerifyActivationkeyVM?
    var textField:UITextField!
    var txtField : UITextField? = nil
    var arrImgView = [UIImageView]()
    var arrLabels = [UILabel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setFonts()
        generatingOTPEnteringView()
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
        // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 17, color: Color.black, title: "AuthoriseTransaction", placeHolder: "")
        
        Fonts().set(object: self.lblsubtitle, fontType: 1, fontSize: 17, color: Color.black, title: "EnterAuthoriseTransaction", placeHolder: "")
        
    }
}

extension AuthoriseTransactionViewController {
    func generatingOTPEnteringView() {
        
        let lblWidth = 15
        let padding = 20
        let dots = 4
        let yOri = (lblsubtitle.frame.size.height + lblsubtitle.frame.origin.y) + 60
        //        let possition = ((btnReset.frame.origin.y) - (lblsubtitle.frame.size.height + lblsubtitle.frame.origin.y)) / 2
        
        
        
        
        var xOri = Int(self.view.frame.size.width / 2) - ((lblWidth * 6) + (padding * (dots - 1))) / 2
        
        
        //let frm1 = CGRect(x: xOri, y: Int(possition + yOri), width: (25 * dots), height: 15)
        let frm1 = CGRect(x: xOri, y: Int(yOri), width: (25 * dots), height: 15)
        txtField = UITextField.init(frame: frm1)
        txtField?.backgroundColor = UIColor.clear
        txtField?.keyboardType = .numberPad
        txtField?.autocorrectionType = .yes
        txtField?.delegate = self
        txtField?.becomeFirstResponder()
        txtField?.tintColor = UIColor.clear
        txtField?.textColor = UIColor.clear
        //        if #available(iOS 12.0, *) {
        //            txtField?.textContentType = .oneTimeCode
        //        }
        self.view.addSubview(txtField!)
        
        for _ in 0 ..< dots {
            
            // var frm = CGRect(x: xOri, y: Int(possition + yOri), width: lblWidth, height: 15)
            var frm = CGRect(x: xOri+10, y: Int(yOri), width: lblWidth, height: 15)
            let imgView = UIImageView.init(frame: frm)
            imgView.backgroundColor = Color.c10_34_64
            imgView.alpha = 0.8
            imgView.layer.cornerRadius = imgView.frame.size.height / 2
            self.view.addSubview(imgView)
            arrImgView.append(imgView)
            
            // frm = CGRect(x: xOri, y: Int(possition + yOri), width: lblWidth, height: 15)
            frm = CGRect(x: xOri, y: Int(yOri), width: lblWidth, height: 15)
            let lbl = UILabel.init(frame: frm)
            Fonts().set(object: lbl, fontType: 3, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
            lbl.textAlignment = .center
            self.view.addSubview(lbl)
            lbl.textColor = UIColor.clear // Change color for show text
            self.arrLabels.append(lbl)
            // Update x Origin
            xOri = Int(CGFloat(xOri) + frm.size.width + CGFloat(padding))
        }
        
        // txtField?.text = otp
        
    }
}


extension AuthoriseTransactionViewController : UITextFieldDelegate {
    
    // Mark : Text Field Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.textField = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            
            //IF 6 DIGITS COMPLETE THEN UTO CALL OTP VERIFICATION API
            if (textField.text?.count)! >= 4 && string != "" {
                
                return false
            }
            
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            for i in 0 ..< updatedText.count {
                
                if arrImgView.count >= updatedText.count {
                    //Change imgView's color
                    let imgView = arrImgView[i]
                    imgView.backgroundColor = Color.white
                    let chars = Array(updatedText)
                    arrLabels[i].text = "\(chars[i])"
                }
                else {
                    return false
                }
            }
            
            for i in updatedText.count ..< arrImgView.count {
                
                if arrImgView.count >= updatedText.count {
                    //Change imgView's color
                    let imgView = arrImgView[i]
                    imgView.backgroundColor = Color.c10_34_64
                    arrLabels[i].text = ""
                }
            }
            
            if updatedText.count >= arrImgView.count {
                txtField?.text = updatedText
                validatePIN()
                return true
            }
        }
        return true
    }
    
    func validatePIN() {
        //        self.view.endEditing(true)
        //        RestAPIManager.validatePIN(title: "PLEASEWAIT", subTitle: "VALACTIPIN", pin: textField.text!, type: Response.self, mobileNo: OnboardUser.mobileNo, completion: { (response) in
        //            DispatchQueue.main.async {
        //                if response.statusCode == 200 {
        //                    self.gotoNextVC()
        //                }
        //                else {
        //                    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
        //                }
        //            }
        //        }) { (error) in
        //            print(error)
        //        }
    }
}
