//
//  CheckoutAirTimeViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 20/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication

class CheckoutAirTimeViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblbuyairtimefor: UILabel!
    @IBOutlet weak var lblmyself: UILabel!
    @IBOutlet weak var lblmobile: UILabel!
    @IBOutlet weak var lblamounttitle: UILabel!
    @IBOutlet weak var lblamount: UILabel!
    @IBOutlet weak var lblbuyfrom: UILabel!
    @IBOutlet weak var lblactype: UILabel!
    @IBOutlet weak var lbltransactioncosttitle: UILabel!
    @IBOutlet weak var lbltransactioncost: UILabel!
    @IBOutlet weak var btnCountinue: UIButton!
    
    var dataModels : BuyaTimeVM?

     override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        
        
        lblactype.text = dataModels?.acalias!
        lblmobile.text = dataModels?.mobilenumber!
         lblmyself.text = dataModels?.mobilename!
         lblamount.text = dataModels?.amount!
        
      }
    
   
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "Confirmandbuy", placeHolder: "")
        Fonts().set(object: self.lblbuyairtimefor, fontType: 0, fontSize: 9, color: Color.c0_123_255, title: "BUYAIRTIMEFOR", placeHolder: "")
        Fonts().set(object: self.lblmyself, fontType: 1, fontSize: 13, color: Color.black, title: "", placeHolder: "")
        Fonts().set(object: self.lblamounttitle, fontType: 1, fontSize: 13, color: Color.black, title: "Amount", placeHolder: "")
        Fonts().set(object: self.lblbuyfrom, fontType: 1, fontSize: 13, color: Color.black, title: "BuyFrom", placeHolder: "")
        Fonts().set(object: self.lbltransactioncosttitle, fontType: 1, fontSize: 13, color: Color.black, title: "Transactionalcost", placeHolder: "")
        Fonts().set(object: self.lblmobile, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: self.lblamount, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: self.lblactype, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: self.lbltransactioncost, fontType: 1, fontSize: 11, color: Color.c160_166_173, title: "", placeHolder: "")
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "BUY AIRTIME", placeHolder: "")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        sendMoneytransactions()
       // authnticateUser()
        }
    

func authnticateUser() {
    //CHECK USER'S FINGERPRINT OR FACE ID
    let context = LAContext()
    Authntication(context).authnticateUser { (success, message) in
        if success == true {
            DispatchQueue.main.async {
                self.buyaTimeTransaction()
            }
        }
        else {
            DispatchQueue.main.async {
                if message != "" {
                    self.showMessage(title: "", message: message)
                }
            }
        }
    }
}
}

// BY A Time FLow API Data
extension CheckoutAirTimeViewController {
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        //let resultArray: Result?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            //  case resultArray = "RESULT_ARRAY"
            
        }
    }
    
    func buyaTimeTransaction(){
        
        
        let amount = lblamount.text!.replacingOccurrences(of: "KES", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        RestAPIManager.buyAirtime(title: "PLEASEWAIT", subTitle: "BUYAIRTIMETransaction", pin: "1234", type: Response.self, mobileNo:lblmobile.text!, accountAlias: lblactype.text!, amount: amount, recipetno: "700707453", mnoWalletId:(dataModels?.aMNO_WALLET_ID!)!, nominateFlag: "YES", reciptAlias: lblmyself.text!, completion: { (response) in
            DispatchQueue.main.async {
                print(response)
                if response.statusCode == 200 {
                
                let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionSuccessfullViewController") as! TransactionSuccessfullViewController
                    nextvc.dataModels = self.dataModels
                self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else{
                 self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                }
                
            }
        }) { (error) in
            print(error)
        }
    }
}

// SEND A MONEY API DATA
extension CheckoutAirTimeViewController {
    
    // Model : -
    struct SendMoneyResponse: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        //let resultArray: Result?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            //  case resultArray = "RESULT_ARRAY"
        }
    }
    
   func sendMoneytransactions(){
        RestAPIManager.sendtomobilemoney(title: "PLEASEWAIT", subTitle: "Send to Mobile Money", pin: "Cx+1e0vEBoZlPztxxZG5fQ==", type: SendMoneyResponse.self, mobileNo: "254700707453", accountAlias: "Current Acct-KES-0100006123443", amount: "250", mnowalletid: "", recipientno: "254700707359", recipientalias: "Account to M-Pesa",completion: { (response) in
            DispatchQueue.main.async {
                print(response)
               if response.statusCode == 200 {
                let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "TransactionSuccessfullViewController") as! TransactionSuccessfullViewController
                self.navigationController?.pushViewController(nextvc, animated: true)
            }
            else{
                self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
            }
            }
        }) { (error) in
            print(error)
        }
    }
}
