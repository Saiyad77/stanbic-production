//
//  BuyAirTimeViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 20/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit


class PricebuttonCell: UICollectionViewCell {
    
    @IBOutlet weak var lblprice: UILabel!
    @IBOutlet weak var viewbg: UIView!
}

class BuyAirTimeViewController: UIViewController,UIGestureRecognizerDelegate {
   
    //Mark:- IBOUTLETS
    @IBOutlet weak var txtmobile: UITextField!
    @IBOutlet weak var txtamount: UITextField!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblselectnetwork: UILabel!
    @IBOutlet weak var Clsprice: UICollectionView!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lblbuyfrom: UILabel!
    @IBOutlet weak var btnNetwork: UIButton!
    @IBOutlet weak var viewNetwork: UIView!
    @IBOutlet weak var constlblactitle: NSLayoutConstraint!
    //Mark:- Varibales
   
    // Variables for Bottom sheet
    //Mark:- Varibales
    var editableTextfield : UITextField?
    private var lastContentOffset: CGFloat = 0
    let Pricearr = ["100", "250","500","1000"]
    var aMNO_WALLET_ID = ""
    
    // Variables for Bottom sheet
    var bottomSheetVC: BottomSheetViewController?
    var imgView : UIImageView?
    
    var indexcolletion = IndexPath()
    var dictdata = NSMutableDictionary()
    var contactno = ""
    var contactname = ""
    
    var selectnetworok = true
 
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add Bottom sheet VC
       addBottomSheetView()
        bottomSheetVC?.delegate = self
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setFonts()
        
        txtmobile.delegate = self
        txtamount.delegate = self
        
        
        txtamount.addUI(placeholder: "ENTER AMOUNT")
        txtmobile.addUI(placeholder: "ENTERMOBPLACEHOLDER")
        txtmobile.text = contactno
        txtamount.text = "KES"
        
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
        let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
        lblaccount.text = acdata[0]["aACCOUNT_ALIAS"]!
        lblaccountnumber.text = acdata[0]["aACCOUNT_NUMBER"]!
        }
        
        if contactno == ""{
            txtmobile.becomeFirstResponder()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txtmobile.didBegin()
        txtamount.didBegin()
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
   
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.c10_34_64, title: "BuyAIRTIME", placeHolder: "")
        Fonts().set(object: self.txtmobile, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "ENTERMOBPLACEHOLDER")
        Fonts().set(object: self.txtamount, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
//        Fonts().set(object: self.lblkes, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "KES", placeHolder: "")
        Fonts().set(object: self.lblselectnetwork, fontType: 0, fontSize: 13, color: Color.c134_142_150, title: "SELECTNETWORKPROVIDER", placeHolder: "")
        
        Fonts().set(object: self.lblbuyfrom, fontType: 0, fontSize: 13, color: Color.c10_34_64, title: "BUY FROM", placeHolder: "")
        
        Fonts().set(object: self.lblaccount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "", placeHolder: "")
        
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func networkprovider(_ sender: AttributedButton) {
        view.endEditing(true)
        selectnetworok = true
        self.showBottomSheetVC(xibtype: "NETWORK")
     }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        
        if txtmobile.text == ""{
            showMessage(title: "", message: "ENTERMOBPLACEHOLDER")
        }
        else {
        if checkValidMobileNo() {
            if txtamount.text == "KES " || txtamount.text == "KES"{
                showMessage(title: "", message: "ENTERAMOUNT")
            }
            else if lblselectnetwork.text == "Select Network Provider"{
                showMessage(title: "", message: "SELECTNETWORKPROVIDER")
            }
            else if lblaccountnumber.text == ""{
                showMessage(title: "", message: "SELECTNETWORKPROVIDER")
            }
            else{
                
                let dataModels = BuyaTimeVM.init(mobilenumber: txtmobile.text!, mobilename:  contactname, acalias: lblaccount.text!, aMNO_WALLET_ID: aMNO_WALLET_ID, amount:txtamount.text!, acnumber: lblaccountnumber.text!)
                let nextvc = CheckoutAirTimeViewController.getVCInstance() as! CheckoutAirTimeViewController
                nextvc.dataModels = dataModels
                self.navigationController?.pushViewController(nextvc, animated: true)
             
            }
         }
    }
 }
    
    @IBAction func accountselect(_ sender: AttributedButton) {
        view.endEditing(true)
        selectnetworok = false
        self.showBottomSheetVC(xibtype: "ACCOUNT")
    }
    
    func checkValidMobileNo() -> Bool {
        
        if self.txtmobile.text?.count == 10 {
            
            let prefix = String((self.txtmobile.text?.prefix(2))!)
            if prefix != "07" {
                self.showMessage(title: "", message: "INVALIDMOB")
                return false
            }
            return true
            
        }
        else if self.txtmobile.text?.count == 9 {
            let prefix = String((self.txtmobile.text?.prefix(1))!)
            if prefix == "0" {
                return false
            }
            else if prefix != "7" {
                self.showMessage(title: "", message: "INVALIDMOB2")
                return false
            }
            
            return true
        }
        else {
            return false
        }
    }

    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
    func showBottomSheetVC(xibtype: String) {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height - 340
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView = UIImageView.init(frame: self.view.frame)
        imgView?.backgroundColor = Color.black
        imgView?.alpha = 0.2
        imgView?.isUserInteractionEnabled = true
        self.view.addSubview(imgView!)
        
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView?.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "")
        imgView?.removeFromSuperview()
        
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
}

//Text field delegate
extension BuyAirTimeViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtamount{
           txtamount.text = "KES "
         }
        editableTextfield = textField
        editableTextfield?.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

//Text field delegate
extension BuyAirTimeViewController : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Pricearr.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PricebuttonCell", for: indexPath as IndexPath) as! PricebuttonCell
        
        if indexcolletion == indexPath {
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.white, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = Color.c0_123_255
        }else{
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = .clear
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        indexcolletion = indexPath
        txtamount.didBegin()
        txtamount.text = "KES \(self.Pricearr[indexPath.row])"
        txtamount.didEnd()
        Clsprice.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.Clsprice.frame.width/4) - 7
        return CGSize(width: width, height: 50)
     }
}

extension BuyAirTimeViewController : SelectNetworkDelegate{
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
     lblaccountnumber.text =  acnumber
     lblaccount.text = acname
    }
    
    
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
        hideBottomSheetVC()

        if selectnetworok == true {
        Fonts().set(object: self.lblselectnetwork, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: newtorkname, placeHolder: "")
            self.aMNO_WALLET_ID = networkid
        }
        else{
          lblaccount.text = newtorkname
          lblaccountnumber.text = networkid.hashingOfTheAccount(noStr: networkid)
        }
    }
}


extension BuyAirTimeViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}
