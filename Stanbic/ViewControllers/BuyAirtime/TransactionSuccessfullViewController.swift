//
//  TransactionSuccessfullViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 20/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class TransactionSuccessfullViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var lblfrom: UILabel!
    @IBOutlet weak var lblto: UILabel!
    @IBOutlet weak var lbldatetitle: UILabel!
    @IBOutlet weak var lblrecieptnotitle: UILabel!
    @IBOutlet weak var lblacnumber: UILabel!
    @IBOutlet weak var lblactype: UILabel!
    @IBOutlet weak var lblmyself: UILabel!
    @IBOutlet weak var lblmyselfvalue: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var lbldatevalue: UILabel!
    @IBOutlet weak var lblrecieptno: UILabel!
    @IBOutlet weak var lblrecieptnovalue: UILabel!
    @IBOutlet weak var btndone: UIButton!
    
    var dataModels : BuyaTimeVM?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        
        lblactype.text = dataModels?.acalias!
        lblacnumber.text = dataModels?.acnumber!
        
        lblmyself.text = dataModels?.mobilename!
        lblmyselfvalue.text = dataModels?.mobilenumber!
        
         lblsubtitle.text = "You have successfully bought airtime\nworth \(dataModels?.amount!) for mobile number\n\(lblmyselfvalue.text!)"
       
     }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 22, color: Color.c0_173_108, title: "Transactionsuccessful", placeHolder: "")
        Fonts().set(object: self.lblsubtitle, fontType: 0, fontSize: 17, color: Color.c52_58_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblfrom, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "FROM", placeHolder: "")
        Fonts().set(object: self.lblto, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "TO", placeHolder: "")
        Fonts().set(object: self.lbldatetitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "DATE", placeHolder: "")
        Fonts().set(object: self.lblrecieptnotitle, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "RECIEPTNO", placeHolder: "")
        Fonts().set(object: self.lblactype, fontType: 1, fontSize: 15, color: Color.black, title: "Current account", placeHolder: "")
        Fonts().set(object: self.lblmyself, fontType: 1, fontSize: 15, color: Color.black, title: "Current account", placeHolder: "")
        Fonts().set(object: self.lbldate, fontType: 1, fontSize: 15, color: Color.black, title: "Current account", placeHolder: "")
        Fonts().set(object: self.lblrecieptno, fontType: 1, fontSize: 15, color: Color.black, title: "Current account", placeHolder: "")
        
        Fonts().set(object: self.lblacnumber, fontType: 1, fontSize: 13, color: Color.c134_142_150, title: "XX-XXX-XXX-X23", placeHolder: "")
        Fonts().set(object: self.lblmyselfvalue, fontType: 1, fontSize: 13, color: Color.c134_142_150, title: "XX-XXX-XXX-X23", placeHolder: "")
        Fonts().set(object: self.lbldatevalue, fontType: 1, fontSize: 13, color: Color.c134_142_150, title: "XX-XXX-XXX-X23", placeHolder: "")
        Fonts().set(object: self.lblrecieptnovalue, fontType: 1, fontSize: 13, color: Color.c134_142_150, title: "XX-XXX-XXX-X23", placeHolder: "")
        Fonts().set(object: btndone, fontType: 1, fontSize: 14, color: Color.white, title: "BUY DONE", placeHolder: "")
    
    }
    
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Done(_ sender: AttributedButton) {
        
    }
}
