//
//  BuyaTimeVM.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 24/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation


// ViewModel for Country List
class BuyaTimeVM {
    
    var mobilenumber: String?
    var mobilename: String?
    var acalias: String?
    var aMNO_WALLET_ID: String?
    var amount: String?
    var acnumber: String?
    
    init(mobilenumber: String?, mobilename: String?,acalias: String?, aMNO_WALLET_ID: String?,amount: String?,acnumber: String?) {
        self.mobilenumber = mobilenumber
        self.mobilename = mobilename
        self.acalias = acalias
        self.aMNO_WALLET_ID = aMNO_WALLET_ID
        self.amount = amount
        self.acnumber = acnumber
        
    }
}

// ac details
//["aACCOUNT_ID": "109780", "aCURRENCY_CODE": "KES ", "aCURRENCY": "Kenyan Shilling ", "aCURRENCY_NUMBER": "404", "aACCOUNT_TYPE": "null", "aACCOUNT_NUMBER": "0100006123443", "aPROFILE_ID": "108265", "aACCOUNT_ALIAS": "Current Acct-KES-0100006123443", "aTARIFF_ID": "null"]

// ["aDATE": "2019-04-24 14:20:41", "aMNO_WALLET_ID": "2", "aORDER_ID": "2", "aMNO_NAME": "Airtel", "aMNO_CODE": "AIRTEL", "aMNO_WALLET": "AIRTELMONEY"]
