//
//  PesaLinkVM.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 29/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation

// ViewModel for PAY BILL
class PesaLinkVM {

// Model : -
struct Response: Codable {
    
    let statusCode: Int?
    let statusMessage: String?
    let accountAlias: String?
    let refrenceId: String?
    
    private enum CodingKeys: String, CodingKey {
        case statusCode = "STATUS_CODE"
        case statusMessage = "STATUS_MESSAGE"
        case accountAlias = "ACCOUNT_ALIAS"
        case refrenceId = "REFERENCE_ID"
        
    }
}
}


// BUYGOOD for PAY BILL
class BuyGoodsVM {
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let refrenceid: String?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case refrenceid = "REFERENCE_ID"
        }
    }
}
