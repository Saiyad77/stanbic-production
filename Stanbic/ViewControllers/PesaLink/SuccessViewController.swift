//
//  SuccessViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 01/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class SuccessViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var btndone: UIButton!
   
    @IBOutlet weak var imgIcon: UIImageView!
    
    var msg = ""
    var link = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 21, color: Color.c0_173_108, title: "", placeHolder: "")
        Fonts().set(object: self.lblsubtitle, fontType: 0, fontSize: 16, color: Color.c52_58_64, title: "", placeHolder: "")
        Fonts().set(object: btndone, fontType: 1, fontSize: 14, color: Color.white, title: "DONE", placeHolder: "")
        imgIcon.image = UIImage(named: "mark")
        

        self.tabBarController?.tabBar.isHidden = true
        if CurrentFlow.flowType == FlowType.MpesaToAccount
        {
            Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 21, color: Color.red, title: "Error", placeHolder: "")
            Fonts().set(object: self.lblsubtitle, fontType: 0, fontSize: 16, color: Color.c52_58_64, title: "UNSUCCESSFULLMSG", placeHolder: "")
            Fonts().set(object: btndone, fontType: 1, fontSize: 14, color: Color.c0_137_255, title: "BACKTOPAYMENT", placeHolder: "")
            btndone.backgroundColor = Color.white
            imgIcon.image = UIImage(named: "Group25")
            
        }else if link == "Manage beneficiary"{
            lbltitle.text = "SUCCESSFULL".localized(Session.sharedInstance.appLanguage)
        }
        else{
        if link == "Deregister Account" {
        lbltitle.text = "UNLINKINGSUCCESSFULL".localized(Session.sharedInstance.appLanguage)
        }
        else{
        lbltitle.text = "LINKINGSUCCESSFULL".localized(Session.sharedInstance.appLanguage)
            }
           
        }
        
        lblsubtitle.text = msg
       
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Checkbook.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
      
        if link == "Manage beneficiary"{
            self.navigationController?.popViewController(animated: true)
        }else{
           global.cupApi = "NO"
            setUpSlideMenu()
        }
    

    }
    
    @IBAction func share(_ sender: AttributedButton) {
        
    }
    
    @IBAction func Done(_ sender: AttributedButton) {
        if CurrentFlow.flowType == FlowType.MpesaToAccount{
            self.navigationController?.popViewController(animated: true)
        }
        else {
        if link == "Manage beneficiary"{
            //self.navigationController?.popViewController(animated: true)
            global.cupApi = "NO"
            setUpSlideMenu()
        }else{
      global.cupApi = "NO"
     setUpSlideMenu()
        }
        }
    }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        leftVC.mainViewController = homeVC
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
 }
