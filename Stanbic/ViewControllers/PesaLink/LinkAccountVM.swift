//
//  LinkUnlinkAccontVM.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 08/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation


protocol LinkAccountVMDelegate: class {
    func getLinkData()
}

class LinkAccountVM{
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            
        }
    }
    
    var responseData : Response?
    weak var delegate : LinkAccountVMDelegate!
    
    // LINK UNLINK ACCOUNT:-
    func linkAccount(mob:String,accountnumber:String,accountalias:String,link:String) {
        RestAPIManager.linkAccount(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOURREQUEST", type: Response.self, mobileNo: mob, accountnumber: accountnumber, accountalias: accountalias, pin: OnboardUser.pin, registerkits: link,
                                   completion: { (response) in
                                    DispatchQueue.main.async {
                                        self.responseData = response
                                        self.delegate.getLinkData()
                                     }
        }) { (error) in
        
        }
    }
}
