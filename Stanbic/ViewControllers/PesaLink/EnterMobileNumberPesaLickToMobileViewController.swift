//
//  EnterMobileNumberPesaLickToMobileViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 30/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class EnterMobileNumberPesaLickToMobileViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var txtmobilenumber: UITextField!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    
    var contactno = ""
    var contactname = ""
    var nominateflag = ""
    var mySelf = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setFonts()
        txtmobilenumber.delegate = self
        txtmobilenumber.addUI(placeholder: "MOBILENUMBER1")
        txtmobilenumber.didBegin()
        
        if contactno != ""{
        txtmobilenumber.text = "+\(contactno)"
        }
        else {
           txtmobilenumber.text = contactno
        }
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txtmobilenumber.becomeFirstResponder()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Rtgs.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: "PESALINKTOPHONE", placeHolder: "")
        
        Fonts().set(object: self.lblsubtitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "ENTERACCOUNNUMBERTOCONTINUE", placeHolder: "")
        
        Fonts().set(object: self.txtmobilenumber, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
    }
    
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        self.view.endEditing(true)
        if txtmobilenumber.text == ""{
            txtmobilenumber.showError(errStr: "ENTERMOBPLACEHOLDER")
        }
        else{
            let nextvc = PesaLinkToMobileViewController.getVCInstance() as! PesaLinkToMobileViewController
            nextvc.nominateAccount = txtmobilenumber.text!
            nextvc.nominateflag = nominateflag
            //nextvc.mySelf = mySelf
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
}

//Text field delegate
extension EnterMobileNumberPesaLickToMobileViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if txtmobilenumber == textField{
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }else{
            return true
        }
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}
