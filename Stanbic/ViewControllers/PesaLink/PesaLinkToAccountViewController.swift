//
//  PesaLinkToAccountViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 29/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class PesaLinkToAccountViewController: UIViewController,UIGestureRecognizerDelegate{
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var txtrecipientnumber: UITextField!
    @IBOutlet weak var txtamount: UITextField!
    @IBOutlet weak var txtrecipientname: UITextField!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var lblselectbranch: UILabel!
    @IBOutlet weak var Clsprice: UICollectionView!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lblrequestfor: UILabel!
    @IBOutlet weak var lblselectbank: UILabel!
    @IBOutlet weak var lblreasonpayent: UITextField!
    
    @IBOutlet weak var txtSelectBank: UITextField!
    
    //Mark:- Varibales
    var editableTextfield : UITextField?
    private var lastContentOffset: CGFloat = 0
    let Pricearr = ["100", "250","500","1000"]
    
    // Variables for Bottom sheet
    var bottomSheetVC: BottomSheetViewController?
        var imgView = UIImageView()
    
    var indexcolletion = IndexPath()
    var nominateAccount = ""
    var nominateName = ""
    var nominateflag = ""
    var headerTitle = ""
    var arrBankbranch = [String:String]()
   var accountno = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add Bottom sheet VC
        addBottomSheetView()
        bottomSheetVC!.delegate = self
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setFonts()
        //txtmobile.delegate = self
        txtamount.delegate = self
        lblreasonpayent.delegate = self
        
        txtamount.addUI(placeholder: "ENTER AMOUNT")
        txtrecipientname.addUI(placeholder: "RECIPIENTNAME")
        lblreasonpayent.addUI(placeholder: "REASONFORPAYMENT")
        txtamount.text = "KES"
               txtamount.keyboardType = .numberPad
        if CurrentFlow.flowType == FlowType.PesaLinkToAc{
            txtrecipientnumber.addUI(placeholder: "RECIPIENTACCOUNTNUMBER")
        }
        if CurrentFlow.flowType == FlowType.PesaLinkToCard{
            txtrecipientnumber.addUI(placeholder: "RECIPIENTCard")
        }
        
        if nominateName  == "" {
            txtrecipientnumber.isUserInteractionEnabled = true
            txtrecipientname.isUserInteractionEnabled = true
        }
        else{
            txtrecipientnumber.isUserInteractionEnabled = false
            txtrecipientname.isUserInteractionEnabled = false
            txtrecipientnumber.text = nominateAccount
            txtrecipientname.text = nominateName
            
            Fonts().set(object: lblselectbranch, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
            lblselectbranch.text = arrBankbranch["aBANK_NAME"] ?? ""
         }
        
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            lblaccount.text = acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!
            lblaccountnumber.text = acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!.hashingOfTheAccount(noStr: acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!)
            accountno = acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!
            
        }
        self.tabBarController?.tabBar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
       }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
     }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //txtmobile.didBegin()
        if nominateName  != "" {
            txtrecipientname.didBegin()
            txtrecipientnumber.didBegin()
        }
        
        if txtamount.text!.count < 4  {
            txtamount.didBegin()
            txtamount.becomeFirstResponder()
        }
     }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Rtgs.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        let (header, reviewTitle, buttonTitle) = (CurrentFlow.flowType?.pesaLinkData())!
        // Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: header, placeHolder: "")
        Fonts().set(object: self.lblsubtitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: reviewTitle, placeHolder: "")
        Fonts().set(object: self.lblselectbank, fontType: 0, fontSize: 12, color: Color.c0_123_255, title: "SELECTBANKCAPS", placeHolder: "")
        Fonts().set(object: self.lblrequestfor, fontType: 0, fontSize: 12, color: Color.c0_123_255, title: "TRANSFERFROM", placeHolder: "")
        Fonts().set(object: lblselectbranch, fontType: 0, fontSize: 15, color: Color.c134_142_150, title: "SELECTBANK", placeHolder: "")
        Fonts().set(object: self.txtamount, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtrecipientnumber, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtrecipientname, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblaccount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "", placeHolder: "")
        Fonts().set(object: lblreasonpayent, fontType: 0, fontSize: 15, color: Color.c134_142_150, title: "", placeHolder: "")
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: buttonTitle, placeHolder: "")
        initialSetUpNavigationBar(title:header)
    }
    
    
    // MARK: - Required Method's
    func initialSetUpNavigationBar(title:String){
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = title.localized(Session.sharedInstance.appLanguage)
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectbankbranch(_ sender: AttributedButton) {
        view.endEditing(true)
        let nextvc = SelectBankPesaLInkViewController.getVCInstance() as! SelectBankPesaLInkViewController
        nextvc.delegate = self
        nextvc.name = lblselectbranch.text!
        nextvc.amount = txtamount.text ?? ""
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
                view.endEditing(true)
        var amounts = (0.0,0.0)
        if lblaccountnumber.text == ""{
            showMessage(title: "", message: "SELECTACCOUNT")
            return
        }
        if CurrentFlow.flowType == FlowType.PesaLinkToAc {
            amounts = getAmountValdiation1(service_code : "PAYAC") 
        } else if CurrentFlow.flowType == FlowType.PesaLinkToCard {
            amounts = getAmountValdiation1(service_code : "PAYC")
        } else {
           amounts = getAmountValdiation1(service_code : "PAYM")
        }
        let min = amounts.0
        let max = amounts.1
        let amount = removeKES(price : txtamount.text ?? "")
        let msg = "\("AMOUNTBEETWEEN".localized(Session.sharedInstance.appLanguage)) \(min) \("-".localized(Session.sharedInstance.appLanguage)) \(max) "
        
        if  txtamount.text == "KES " || txtamount.text == "KES"{
            txtamount.becomeFirstResponder()
            txtamount.showError(errStr: "ENTERAMOUNT")
            self.consBottom.constant = 100
            return
        } else if checkAmount(min:min,max:max,amount:amount) == false {
            txtamount.becomeFirstResponder()
            txtamount.showError(errStr: msg)
        } else if lblselectbranch.text == "SELECTBANK".localized(Session.sharedInstance.appLanguage) {
            //showMessage(title: "", message: "SELECTBANK")
            txtSelectBank.becomeFirstResponder()
            txtSelectBank.showError(errStr: "SELECTBANK")
            return
        } else if txtrecipientname.text == ""{
            txtrecipientname.becomeFirstResponder()
            txtrecipientname.showError(errStr: "GENRALREGAX")
            return
        } else if getgenralRegax(value: txtrecipientname.text!, type: "genral") == false {
            txtrecipientname.becomeFirstResponder()
            txtrecipientname.showError(errStr: "GENRALREGAX")
             // lblreasonpayent.becomeFirstResponder()
        }
        else if txtrecipientnumber.text == ""{
            txtrecipientnumber.becomeFirstResponder()
            txtrecipientnumber.showError(errStr: "RECIPIENTACCOUNTNAME")
             // lblreasonpayent.becomeFirstResponder()
            return
        }
        else if checkacbyflow(textvalue:txtrecipientnumber.text!) == false {
            if CurrentFlow.flowType == FlowType.PesaLinkToCard{
            txtrecipientnumber.becomeFirstResponder()
              txtrecipientnumber.showError(errStr: "RECIPIENTACCOUNTVALIDCARD")
                 // lblreasonpayent.becomeFirstResponder()
            } else {
                txtrecipientnumber.becomeFirstResponder()
                txtrecipientnumber.showError(errStr: "ACCOUNTREGAX")
               //   lblreasonpayent.becomeFirstResponder()
            }
        }
        else if lblreasonpayent.text == "" {
            lblreasonpayent.becomeFirstResponder()
            lblreasonpayent.showError(errStr: "Reason for payment")
            return
        } else  if getgenralRegax(value: lblreasonpayent.text!, type: "genral") == false {
             lblreasonpayent.becomeFirstResponder()
            lblreasonpayent.showError(errStr: "GENRALREGAX")
          //  lblreasonpayent.becomeFirstResponder()
            return
        } else {
            if nominateName == ""{
                nominateName = txtrecipientname.text!
                nominateAccount = txtrecipientnumber.text!
            }
            
            nominateAccount = txtrecipientnumber.text!
            nominateName =  txtrecipientname.text!
            
            let model = IntenalFundVM.init(dictbank: arrBankbranch,anominatedaccount: nominateAccount, amount: txtamount.text!,acalias: lblaccount.text!, anominatealias: txtrecipientname.text!,nominateflag:nominateflag)
            let nextvc = CheckoutAirTimeViewController.getVCInstance() as! CheckoutAirTimeViewController
            nextvc.self.internalfundModel = model
            nextvc.reasonForPayment = lblreasonpayent.text!
            nextvc.accountno = accountno
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
    
    // Mark:_ Accound Number Regax Check:-
    
    func checkacbyflow(textvalue:String) -> Bool {
         if CurrentFlow.flowType == FlowType.PesaLinkToAc{
             return getgenralRegax(value: textvalue, type: "other")
         }
         else if CurrentFlow.flowType == FlowType.PesaLinkToCard{
            return getgenralRegax(value: textvalue, type: "card")
         }
         else if CurrentFlow.flowType == FlowType.PesaLinkToPhone{
            return getgenralRegax(value: textvalue, type: "ac")
         }
         else {
          return true
        }
      }
    
    @IBAction func accountselect(_ sender: AttributedButton) {
        view.endEditing(true)
        self.showBottomSheetVC(xibtype: "ACCOUNT", Selectname: lblaccount.text!)
    }
    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
    func showBottomSheetVC(xibtype: String,Selectname: String) {
        
        var height = 300  // 340
        let coreData = CoreDataHelper()
        if xibtype == "ACCOUNT" {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
               height = acdata.count * 100
                if acdata.count == 1 {
                    height = 200
                }
            }
        }
        else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acnetwork  = coreData.getDataForEntity(entity: Entity.MNO_INFO)
                height = acnetwork.count * 200
            }
        }
        
        // bottomSheetVC?.partialView = UIScreen.main.bounds.height - CGFloat(height)
        bottomSheetVC!.partialView = self.view.frame.size.height - CGFloat(height)
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imgView.backgroundColor = Color.black
        imgView.alpha = 0.2
        imgView.isUserInteractionEnabled = true
        self.view.addSubview(imgView)
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "", SelectName: "")
        imgView.removeFromSuperview()
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
}

//Text field delegate
extension PesaLinkToAccountViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtamount {
            txtamount.text = "KES "
            indexcolletion = [1,6]
            Clsprice.reloadData()
        }
        textField.removeError()
        if textField == lblreasonpayent {
            Fonts().set(object: lblreasonpayent, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        }
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtamount {
            
            textField.typingAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            let protectedRange = NSMakeRange(0, 4)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                return false
            }
            return true
        }
        else{
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

//Text field delegate
extension PesaLinkToAccountViewController : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Pricearr.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PricebuttonCell", for: indexPath as IndexPath) as! PricebuttonCell
        
        if indexcolletion == indexPath {
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.white, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = Color.c0_123_255
        }else{
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = .clear
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        indexcolletion = indexPath
        txtamount.text = "KES \(self.Pricearr[indexPath.row])"
        txtamount.removeError()
        txtamount.didBegin()
        Clsprice.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.Clsprice.frame.width/4) - 7
        return CGSize(width: width, height: 50)
        
    }
}

extension PesaLinkToAccountViewController : SelectNetworkDelegate{
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
        
        lblaccountnumber.text =  acnumber
        lblaccount.text = acname
    }
    
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
        hideBottomSheetVC()
        lblaccount.text = newtorkname
        lblaccountnumber.text = networkid.hashingOfTheAccount(noStr: networkid)
        accountno = networkid
    }
}


// SELECT BRANCH DATA :_
extension PesaLinkToAccountViewController:SelectBankDelegate {
    
    func setAmount(amount: String) {
        txtamount.text = amount
    }
    
    func selectBank(dictBank: [String : String]) {
        txtSelectBank.removeError()
        Fonts().set(object: lblselectbranch, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        arrBankbranch = dictBank
        lblselectbranch.text = dictBank["aBANK"]
    }
}

extension PesaLinkToAccountViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
         
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}

