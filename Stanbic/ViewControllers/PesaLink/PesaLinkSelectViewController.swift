//
//  PesaLinkSelectViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 29/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication
import Contacts
import ContactsUI

class PesaLinkSelectViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var colletionview: UICollectionView!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    
    var arrtitle = [String]()
    var arrImages = ["mobile_money","mpesatoac",
                     "sendtocard","linkaccount"]//,"Bank"]  -
    
    let serviceTitle = ["PAYM", "PAYAC", "PAYC", "CSR"]
    
    
    var serviceTitleFinal = [String]()
    var serviceIconFinal = [String]()
    var activeServices = [[String : String]]()
    
    let model = LinkAccountVM()
    var accountalias = ""
    var accountnumber = ""
    
    var contacts = [CNContact]()
    var oneArray1 = [Character]()
    var newDick1 = [Character:[CNContact]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        model.delegate = self
        //  setFonts()
        checkRegistration()
        self.tabBarController?.tabBar.isHidden = true
        
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0 {
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            accountalias = acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!
            accountnumber = acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!
        }
        
        if #available(iOS 10.0, *) {
            self.colletionview?.isPrefetchingEnabled = false
        }
        
        if let layout = colletionview.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionHeadersPinToVisibleBounds = true
        }
        // colletionview?.contentInset = UIEdgeInsets(top: -20, left: 20, bottom: 20, right: 20)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Rtgs.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: self.lbltitle, fontType: 0, fontSize: 24, color: Color.white, title: "PESALINK", placeHolder: "")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension PesaLinkSelectViewController : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SelectTransactionHeaderCell", for: indexPath as IndexPath) as! SelectTransactionHeaderCell
            //   headerView.backgroundColor = Color.c10_34_64
            if indexPath.section == 0{
                headerView.lbltitle.text = ""
                headerView.imgBg.image = UIImage(named:"GradienTop2")
            }
            else {
                Fonts().set(object: headerView.lbltitle, fontType: 0, fontSize: 24, color: Color.white, title: "PESALINK", placeHolder: "")
                headerView.imgBg.image = UIImage(named:"GradienTop1")
            }
            return headerView
            
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0 {
            return 0
        }
        else {
            return serviceTitleFinal.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectTransactionCell", for: indexPath as IndexPath) as! SelectTransactionCell
        cell.setdata(title: serviceTitleFinal[indexPath.row], imageName: serviceIconFinal[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let activeService = activeServices[indexPath.row]
        if activeService["aACTIVE"] == "2" {
            self.showMessage(title: "INACTIVE", message: activeService["aINACTIVE_MESSAGE"] ?? "")
        }
        else {
            
            switch arrtitle[indexPath.row].localized(Session.sharedInstance.appLanguage){
            
            case "SENDTOPHONE".localized(Session.sharedInstance.appLanguage):
                CurrentFlow.flowType = .PesaLinkToPhone
                let nextvc = ContactListViewController.getVCInstance() as! ContactListViewController
                nextvc.contacts = contacts
                nextvc.oneArray1 = oneArray1
                nextvc.newDick1 = newDick1
                self.navigationController?.pushViewController(nextvc, animated: true)
               
            case "SENDTOACCOUNT".localized(Session.sharedInstance.appLanguage):
                CurrentFlow.flowType = .PesaLinkToAc
                let arrEnrolmentInfo = EnrollmentInfo().getEnrollmentinforfor(enrollmentinfo: CoreDataHelper().getDataForEntity(entity: Entity.BENEFICIARIES_INFO),  type: (CurrentFlow.flowType?.getType())!)
                
                if arrEnrolmentInfo.count > 0 {
                    let nextvc = ContactListFundTransferViewController.getVCInstance() as! ContactListFundTransferViewController
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else {
                    let nextvc = PesaLinkToAccountViewController.getVCInstance() as! PesaLinkToAccountViewController
                    nextvc.nominateflag = "No"
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                
            case "SENDTOCARD".localized(Session.sharedInstance.appLanguage):
                CurrentFlow.flowType = .PesaLinkToCard
                let arrEnrolmentInfo = EnrollmentInfo().getEnrollmentinforfor(enrollmentinfo: CoreDataHelper().getDataForEntity(entity: Entity.BENEFICIARIES_INFO),  type: (CurrentFlow.flowType?.getType())!)
                
                if arrEnrolmentInfo.count > 0 {
                    let nextvc = ContactListFundTransferViewController.getVCInstance() as! ContactListFundTransferViewController
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
                else {
                    let nextvc = PesaLinkToAccountViewController.getVCInstance() as! PesaLinkToAccountViewController
                    nextvc.nominateflag = "No"
                    self.navigationController?.pushViewController(nextvc, animated: true)
                }
            case "Register Account":
                getRegister(str: "Register Account")
                
            case "Deregister Account":
                getRegister(str: "Deregister Account")
                
            default:
                print(indexPath)
            }
        }
        
        //        switch indexPath.row {
        //        case 0 :
        //            CurrentFlow.flowType = .PesaLinkToPhone
        //            let nextvc = ContactListViewController.getVCInstance() as! ContactListViewController
        //            nextvc.contacts = contacts
        //            nextvc.oneArray1 = oneArray1
        //            nextvc.newDick1 = newDick1
        //            self.navigationController?.pushViewController(nextvc, animated: true)
        //            print(indexPath)
        //
        //        case 1:
        //            CurrentFlow.flowType = .PesaLinkToAc
        //            let arrEnrolmentInfo = EnrollmentInfo().getEnrollmentinforfor(enrollmentinfo: CoreDataHelper().getDataForEntity(entity: Entity.BENEFICIARIES_INFO),  type: (CurrentFlow.flowType?.getType())!)
        //
        //            if arrEnrolmentInfo.count > 0 {
        //                let nextvc = ContactListFundTransferViewController.getVCInstance() as! ContactListFundTransferViewController
        //                self.navigationController?.pushViewController(nextvc, animated: true)
        //            }
        //            else {
        //                let nextvc = PesaLinkToAccountViewController.getVCInstance() as! PesaLinkToAccountViewController
        //                nextvc.nominateflag = "No"
        //                self.navigationController?.pushViewController(nextvc, animated: true)
        //            }
        //        case 2 :
        //            CurrentFlow.flowType = .PesaLinkToCard
        //            let arrEnrolmentInfo = EnrollmentInfo().getEnrollmentinforfor(enrollmentinfo: CoreDataHelper().getDataForEntity(entity: Entity.BENEFICIARIES_INFO),  type: (CurrentFlow.flowType?.getType())!)
        //
        //            if arrEnrolmentInfo.count > 0 {
        //                let nextvc = ContactListFundTransferViewController.getVCInstance() as! ContactListFundTransferViewController
        //                self.navigationController?.pushViewController(nextvc, animated: true)
        //            }
        //            else {
        //                let nextvc = PesaLinkToAccountViewController.getVCInstance() as! PesaLinkToAccountViewController
        //                nextvc.nominateflag = "No"
        //                self.navigationController?.pushViewController(nextvc, animated: true)
        //            }
        //            case 3 :
        //            CurrentFlow.flowType = .LinkAccount
        //            if arrtitle[indexPath.row] == "Deregister Account"{
        //                // Create the alert controller
        //                let alertController = UIAlertController(title: "DeRegistedACCOUNT".localized(Session.sharedInstance.appLanguage), message: "WHOULDYOULIKETO".localized(Session.sharedInstance.appLanguage), preferredStyle: .alert)
        //
        //                let okAction = UIAlertAction(title: "OK".localized(Session.sharedInstance.appLanguage), style: UIAlertAction.Style.default) {
        //                    UIAlertAction in
        //                    self.validate()
        //                }
        //                let cancelAction = UIAlertAction(title: "CANCEL".localized(Session.sharedInstance.appLanguage), style: UIAlertAction.Style.cancel) {
        //                    UIAlertAction in
        //                    NSLog("Cancel Pressed")
        //                }
        //                alertController.addAction(okAction)
        //                alertController.addAction(cancelAction)
        //                self.present(alertController, animated: true, completion: nil)
        //
        //            }else {
        //                let nextvc = LinkAccountViewController.getVCInstance() as! LinkAccountViewController
        //                nextvc.link = "YES"
        //                self.navigationController?.pushViewController(nextvc, animated: true)
        //            }
        //        default:
        //            print(indexPath)
        //        }
    }
    
    func getRegister(str:String){
        CurrentFlow.flowType = .LinkAccount
        if str == "Deregister Account"{
            // Create the alert controller
            let alertController = UIAlertController(title: "DeRegistedACCOUNT".localized(Session.sharedInstance.appLanguage), message: "WHOULDYOULIKETO".localized(Session.sharedInstance.appLanguage), preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK".localized(Session.sharedInstance.appLanguage), style: UIAlertAction.Style.default) {
                UIAlertAction in
                self.validate()
            }
            let cancelAction = UIAlertAction(title: "CANCEL".localized(Session.sharedInstance.appLanguage), style: UIAlertAction.Style.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
            
        }else {
            let nextvc = LinkAccountViewController.getVCInstance() as! LinkAccountViewController
            nextvc.link = "YES"
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewSize = ((self.colletionview.frame.size.width - 60)/2)
        return CGSize(width : collectionViewSize,height: collectionViewSize - 10)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0{
            return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
        else {
            return UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 20)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0{
            let statusBarHieght = UIApplication.shared.statusBarFrame.size.height
            return CGSize(width: collectionView.frame.width, height: statusBarHieght+25)
        }
        else{
            return CGSize(width: collectionView.frame.width, height: 100)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
}

// Pesalink - Check Registration
extension PesaLinkSelectViewController{
    
    func checkRegistration(){
        if !isInternetAvailable(){
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        RestAPIManager.checkRegistration(title: "PLEASEWAIT", subTitle: "CHECKREGISTRATION", type: Response.self, mobileNo: OnboardUser.mobileNo, completion: { (response) in
            DispatchQueue.main.async {
              //  print(response)
                if response.statusCode == 200 {
                    if response.registrationstatus == 1 { // 1 register and 2 unregister
                        self.getArrayTitle(status:1)
                        
                    }
                    else{
                        self.getArrayTitle(status:2)
                        //self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                    }
                }
                else{
                    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                    
                    let arrtitletemp = ["SENDTOPHONE","SENDTOACCOUNT","SENDTOCARD"]
                    self.arrtitle = arrtitletemp
                    self.checkActiveService()
                }
            }
        }) { (error) in
            print(error)
        }
    }
    
    func getArrayTitle(status:Int){
        var accountlink = ""
        if status == 1 {
            accountlink = "Deregister Account"
        }
        else{
            accountlink = "Register Account"
        }
        
        let arrtitletemp = ["SENDTOPHONE","SENDTOACCOUNT","SENDTOCARD",accountlink]
        arrtitle = arrtitletemp
        checkActiveService() // check active deactive
    }
    
    func checkActiveService(){
        for i in 0 ..< serviceTitle.count{
            if let activeService = self.checkActiveService(service : serviceTitle[i]) {
              //  print(activeService)
                if activeService["aACTIVE"] != "3" {
                    // Only add data whice have service id 1 or 2. 3 is disable service
                    serviceTitleFinal.append(arrtitle[i].localized(Session.sharedInstance.appLanguage))
                    serviceIconFinal.append(arrImages[i])
                    activeServices.append(activeService)
                }
            }
        }
        colletionview.reloadData()
    }
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let registrationstatus: Int?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case registrationstatus = "REGISTRATION_STATUS"
        }
    }
}

// LINK - UNLINK
extension PesaLinkSelectViewController : LinkAccountVMDelegate,AuthDelegate{
    
    // VALIDATE FACE AND PIN :_
    func validate() {
        //Check if user enable biometric access
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            let context = LAContext()
            Authntication(context).authnticateUser { (success, message) in
                DispatchQueue.main.async {
                    if success == true {
                        global.enterpin = OnboardUser.pin
                        self.validAuth()
                    }
                    else {
                        if message != "" {
                              self.showMessage(title: "", message: message)
                        }
                    }
                    
                }
            }
        }
        else {
              global.enterpin = ""
            let vc = AuthoriseViewController.getVCInstance() as! AuthoriseViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    // UNLINK ACCOUNT :-
    func validAuth(){
        model.linkAccount(mob: OnboardUser.mobileNo, accountnumber: accountnumber, accountalias: accountalias, link: "DEL")
    }
    
    func getLinkData() {
       if model.responseData?.statusCode == 200 {
            let nextvc = SuccessViewController.getVCInstance() as! SuccessViewController
            nextvc.msg = (model.responseData?.statusMessage!)!
            nextvc.link = "Deregister Account"
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
        else {
            self.showMessage(title: "", message: model.responseData?.statusMessage ?? "")
        }
    }
}
