//
//  PayBillViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 23/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit


class PayBillHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imglogo: UIImageView!
    @IBOutlet weak var lbllogotitle: UILabel!
  
    
    override func awakeFromNib() {
         Fonts().set(object: lbltitle, fontType: 1, fontSize: 13, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: lbllogotitle, fontType: 2, fontSize: 8, color: Color.c134_142_150, title: "", placeHolder: "")
    }
}


class PayBillTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imgcards: UIImageView!
    
    func setfont(number:Int) {
        Fonts().set(object: lbltitle, fontType: 1, fontSize: number, color: Color.black, title: "", placeHolder: "")
    }
    override func awakeFromNib() {
        imgcards.layer.cornerRadius = imgcards.frame.height/2
    }
}




class PayBillViewController: UIViewController {

    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var tblpaybill: UITableView!
    @IBOutlet weak var txtserachfield: UITextField!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        txtserachfield.addUI(placeholder: "Search for a bill")
    }
    
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.c10_34_64, title: "PB", placeHolder: "")
         Fonts().set(object: self.txtserachfield, fontType: 0, fontSize: 13, color: Color.c134_142_150, title: "SEARCHFORBILL", placeHolder: "")
        
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
      
    }
}

//Text field delegate
extension PayBillViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
    
   
}

extension PayBillViewController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
             let cell = tableView.dequeueReusableCell(withIdentifier: "mycards") as! PayBillTableViewCell
            cell.setfont(number: 13)
            cell.lbltitle.text = "Visa Gold Credit Card"
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "merchants") as! PayBillTableViewCell
            cell.setfont(number: 16)
            cell.lbltitle.text = "DSTV"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         let cell = tableView.dequeueReusableCell(withIdentifier: "PayBillHeaderTableViewCell") as! PayBillHeaderTableViewCell
       
        
        if section == 0 {
            cell.lbltitle.text = "Pay to my Cards"
            cell.lbllogotitle.isHidden = true
            cell.imglogo.isHidden = true
        }
        else {
            cell.lbltitle.text = "All merchants"
            cell.lbllogotitle.isHidden = false
            cell.imglogo.isHidden = false
            cell.lbllogotitle.text = "POWERED BY"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let nextvc = PayBillNumberEnterViewController.getVCInstance() as! PayBillNumberEnterViewController
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
     return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 72
        }
        else {
            return 80
        }
    }
}

extension PayBillViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.view.endEditing(true)
        
    }
}
