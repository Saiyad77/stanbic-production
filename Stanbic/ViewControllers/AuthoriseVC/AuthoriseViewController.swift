//
//  AuthoriseViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 24/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

protocol AuthDelegate: class {
    func validAuth()
}

class AuthoriseViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    //Mark:- Varibales
    var delegate : AuthDelegate!
    var textField:UITextField!
    var txtField : UITextField? = nil
    var arrImgView = [UIImageView]()
    var arrLabels = [UILabel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        generatingOTPEnteringView()
        // Do any additional setup after loading the view.
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func setFonts() {
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "AUTHTRAN", placeHolder: "")
        Fonts().set(object: lblSubtitle, fontType: 1, fontSize: 17, color: Color.black, title: "ENTERPIN", placeHolder: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
       UIApplication.statusBarBackgroundColor = .black
    }
    
   
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
          UIApplication.statusBarBackgroundColor = .clear
        self.dismiss(animated: true, completion: nil)
    }
}

extension AuthoriseViewController {
    func generatingOTPEnteringView() {
        
        let lblWidth = 15
        let padding = 20
        let dots = 4
        let yOri = (lblSubtitle.frame.size.height + lblSubtitle.frame.origin.y)
        let possition = CGFloat(77.0)
        var xOri = Int(self.view.frame.size.width / 2) - ((lblWidth * dots) + (padding * (dots - 1))) / 2
        let frm1 = CGRect(x: xOri, y: Int(possition + yOri), width: (25 * dots), height: 15)
        txtField = UITextField.init(frame: frm1)
        txtField?.backgroundColor = UIColor.clear
        txtField?.keyboardType = .numberPad
        txtField?.autocorrectionType = .yes
        txtField?.delegate = self
        txtField?.becomeFirstResponder()
        txtField?.tintColor = UIColor.clear
        txtField?.textColor = UIColor.clear
        txtField?.keyboardAppearance = .dark
        //        if #available(iOS 12.0, *) {
        //            txtField?.textContentType = .oneTimeCode
        //        }
        self.view.addSubview(txtField!)
        
        for _ in 0 ..< dots {
            
            var frm = CGRect(x: xOri, y: Int(possition + yOri), width: lblWidth, height: 15)
            let imgView = UIImageView.init(frame: frm)
            imgView.backgroundColor = Color.c10_34_64
            imgView.alpha = 0.4
            imgView.layer.cornerRadius = imgView.frame.size.height / 2
            self.view.addSubview(imgView)
            arrImgView.append(imgView)
            
            frm = CGRect(x: xOri, y: Int(possition + yOri), width: lblWidth, height: 15)
            let lbl = UILabel.init(frame: frm)
            Fonts().set(object: lbl, fontType: 3, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
            lbl.textAlignment = .center
            self.view.addSubview(lbl)
            lbl.textColor = UIColor.clear // Change color for show text
            self.arrLabels.append(lbl)
            // Update x Origin
            xOri = Int(CGFloat(xOri) + frm.size.width + CGFloat(padding))
        }
        
        // txtField?.text = otp
        
    }
    
    func updateOTPEnteringView() {
        self.txtField = nil
        for i in 0 ..< arrLabels.count {
            arrLabels[i].removeFromSuperview()
        }
        for i in 0 ..< arrImgView.count {
            arrImgView[i].removeFromSuperview()
        }
        
        arrLabels.removeAll()
        arrImgView.removeAll()
        
        self.generatingOTPEnteringView()
        if textField != nil {
            textField.text = ""
        }
        txtField?.becomeFirstResponder()
    }
}

extension AuthoriseViewController : UITextFieldDelegate {
    
    // Mark : Text Field Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.textField = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            
            //IF 6 DIGITS COMPLETE THEN UTO CALL OTP VERIFICATION API
            if (textField.text?.count)! >= 6 && string != "" {
                
                return false
            }
            
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            for i in 0 ..< updatedText.count {
                
                if arrImgView.count >= updatedText.count {
                    //Change imgView's color
                    let imgView = arrImgView[i]
                    imgView.alpha = 1.0
                    imgView.backgroundColor = Color.black
                    let chars = Array(updatedText)
                    arrLabels[i].text = "\(chars[i])"
                }
                else {
                    return false
                }
            }
            
            for i in updatedText.count ..< arrImgView.count {
                
                if arrImgView.count >= updatedText.count {
                    //Change imgView's color
                    let imgView = arrImgView[i]
                    imgView.alpha = 0.4
                    imgView.backgroundColor = Color.c10_34_64
                    arrLabels[i].text = ""
                }
            }
            
            if updatedText.count >= arrImgView.count {
                txtField?.text = updatedText
                validate(text: updatedText)
                return true
            }
        }
        let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
    
    func validate(text : String) {
        
        global.enterpin = Secure().encryptPIN(text : (text))
        var dataArray = CoreDataHelper().getDataForEntity(entity : Entity.PIN)
        if dataArray.count > 0
        {
            let data = Secure().decryptPIN(text: dataArray[0]["aPIN"]!)
            if data == text {
                UIApplication.statusBarBackgroundColor = .clear
                self.dismiss(animated: true) {
                    self.delegate.validAuth()
                }
            } else {
                if CurrentFlow.flowType == FlowType.InternalFundTransfer || CurrentFlow.flowType  == FlowType.Rtgs ||
                    CurrentFlow.flowType == FlowType.MpesaToAccount || CurrentFlow.flowType == FlowType.PesaLinkToAc ||
                    CurrentFlow.flowType == FlowType.PesaLinkToCard || CurrentFlow.flowType == FlowType.PesaLinkToPhone ||
                    CurrentFlow.flowType == FlowType.BuyGoods || CurrentFlow.flowType == FlowType.PayBill ||
                    CurrentFlow.flowType == FlowType.BuyAirtime || CurrentFlow.flowType == FlowType.SendMoney || CurrentFlow.flowType == FlowType.ForexRate{
                    self.dismiss(animated: true)
                    self.delegate.validAuth()
                 } else {
                
                self.updateOTPEnteringView()
                self.showMessage(title: "", message: "INVALIDPIN")
                }
            }
            
        }
    }
}

extension UIApplication {
    
    class var statusBarBackgroundColor: UIColor? {
        get {
            return (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor
        } set {
            (shared.value(forKey: "statusBar") as? UIView)?.backgroundColor = newValue
        }
    }
}
