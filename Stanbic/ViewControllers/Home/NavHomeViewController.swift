//
//  NavHomeViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 23/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class NavHomeViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    static func getVCInstance() -> UINavigationController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Home.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))") as! UINavigationController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}


extension NavHomeViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
    
    }
    
    func leftDidOpen() {
     
    }
    
    func leftWillClose() {
 
    }
    
    func leftDidClose() {
    
                 NotificationCenter.default.post(name: Notification.Name(rawValue: "ImageChange"), object: nil)
    }
    
    func rightWillOpen() {
   
    }
    
    func rightDidOpen() {
      
    }
    
    func rightWillClose() {
     
    }
    
    func rightDidClose() {
     
    }
}
