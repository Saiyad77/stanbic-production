
//
//  Home2ViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 19/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import AVFoundation
import LocalAuthentication
import CoreImage
import Contacts
import ContactsUI
import Crashlytics


class Home2ViewController: UIViewController, UIGestureRecognizerDelegate, UITabBarControllerDelegate, UIPopoverPresentationControllerDelegate {
    
    //MARK:- IBoutlet
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pagerView: UIPageControl!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblHeader1: UILabel!
    @IBOutlet var lblHowHelp: UILabel!
    @IBOutlet var lblAccountsCard: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var collectionViewServices: UICollectionView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet var btnSideBar: UIButton!
    @IBOutlet var btnSideBar1: UIButton!
    private var lastContentOffset: CGFloat = 0
    
    // new image picker:-
    @IBOutlet weak var changePhotoBackgroundView: UIView!
    @IBOutlet weak var changePhotoViewBottom: NSLayoutConstraint!
    
    
    var serviceIcon = ["send_money", "pay_bill", "buy_air_time", "get_a_loan"]
    let serviceTitle = ["Mobile_money", "PB", "BA", "Salary_advance"]
    var serviceTitleFinal = [String]()
    var serviceIconFinal = [String]()
    var activeServices = [[String : String]]()
    var headerTitle = ["UPCOMINGBILL", "PROMO", "OFFER"]
    //MARK: Collection View Variables
    var isfirstTimeTransform = true
    let transformCellValue = CGAffineTransform(scaleX: 1.0, y: 1.0)
    let animationSpeed = 0.2
    let headerTileHeight = CGFloat(100)
    var myPickerController = UIImagePickerController()
    let modelcontacts = ContactVm()
    var contacts = [CNContact]()
    var oneArray = [Character]()
    var newDick = [Character:[CNContact]]()
    //MARK: Variables
    var headerHeight : CGFloat?
    let model = CUP()
    let modelBE = BalanceEnquiry()
    let modelFetchBill = FetchBill()
    var selectedAccountBE : String = ""
    var selectedAccountBE2 : String = ""
    
    var imgHederHeight : CGFloat = 0
    var arrMarketingInfo = [[String : String]]()
    var ministatement = false
    var isBalanceViewVisible: Bool = false
    var dictministatement = [String:String]()
    var ischeckingBalance: Bool = Bool()
    var timer = Timer()
    var counter: Int = Int()
    var isStatus: Bool = Bool()
    var modelsalarayad = SalaryAdvanceVM()
    var isSessionOut: Bool = false
    var checkingValue: NSArray = NSArray()
    
    var selectedIndex: IndexPath = IndexPath()
    let iPad = UIUserInterfaceIdiom.pad
    let iPhone = UIUserInterfaceIdiom.phone
    var TotelAmount = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        modelcontacts.delegate = self
        modelcontacts.delegate = self
        //modelcontacts.getContacts()
        
        collectionView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        setFonts()
        setupHeader()
        fetchCUP()
        fetchBill()
        checkActiveService()
        //
        if UserDefaults.standard.object(forKey: "badgeCount") != nil{
            tabBarController?.tabBar.items?[3].badgeValue = "\(UserDefaults.standard.object(forKey: "badgeCount") as! Int)"
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(getImageNotification), name: Notification.Name(rawValue: "ImageChange"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getBedgeNotification), name: Notification.Name(rawValue: "badgecount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(methodOfReceivedNotification), name:NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil)
        self.changePhotoBackgroundView.isHidden = true
        let tapOnBackView = UITapGestureRecognizer(target: self, action: #selector(removeChangePhotoView(_:)))
        self.changePhotoBackgroundView.addGestureRecognizer(tapOnBackView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        model.enquiryAccountsInfo = CoreDataHelper().getDataForEntity(entity: Entity.ENQUIRY_ACCOUNTS_INFO)
        
        //        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(Home2ViewController.doStuff), userInfo: nil, repeats: true)
        //        let resetTimer = UITapGestureRecognizer(target: self, action: #selector(Home2ViewController.resetTimer));
        //        self.view.isUserInteractionEnabled = true
        //        self.view.addGestureRecognizer(resetTimer)
    }
    
    //    @objc func doStuff() {
    //        // perform any action you wish to
    //        print("User inactive for more than 5 seconds .")
    //        timer.invalidate()
    //
    //
    //    }
    //
    //    @objc func resetTimer() {
    //        timer.invalidate()
    //        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(Home2ViewController.doStuff), userInfo: nil, repeats: true)
    //        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
    //        let appDelegate = UIApplication.shared.delegate as! AppDelegate
    //        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
    //        appDelegate.window?.rootViewController = nextvc
    //        appDelegate.window?.makeKeyAndVisible()
    //    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getImage()
        if tabBarController?.tabBar.items?[3].badgeValue == "0"{
            tabBarController?.tabBar.items?[3].badgeValue = nil
        }
        modelsalarayad.delegate = self
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
    }
    
    @objc func removeChangePhotoView(_ tap: UITapGestureRecognizer){
        //let heigh = self.changePhotoView.frame.height
        
        UIView.animate(withDuration: 0.4, animations: {
            self.changePhotoViewBottom.constant = -350
            self.view.layoutIfNeeded()
        }) { (true) in
            self.changePhotoBackgroundView.isHidden = true
        }
    }
    
    @IBAction func takePhotoBtnAction(_ sender: Any) {
        
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            self.openCamera()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    self.openCamera()
                } else {
                    let alert = UIAlertController(
                        title: "IMPORTANT",
                        message: "Camera access required for capturing photos!",
                        preferredStyle: UIAlertController.Style.alert
                    )
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Allow Camera", style: .cancel, handler: { (alert) -> Void in
                        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            })
        }
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            myPickerController.modalPresentationStyle = .overCurrentContext
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    @IBAction func chnagePhotoBtnAction(_ sender: Any) {
        
        self.hidesBottomBarWhenPushed = true
        
        self.changePhotoBackgroundView.isHidden = false
        if UIScreen.main.bounds.height >= 800 {
            self.changePhotoViewBottom.constant = 0
        }else{
            self.changePhotoViewBottom.constant = 8
        }
        UIView.animate(withDuration: 0.4) {
            
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func cancelChangePhoto(_ sender: Any) {
        self.removeChangePhotoView(UITapGestureRecognizer())
    }
    
    @objc func methodOfReceivedNotification(notification: NSNotification) {
        let data:NSDictionary = notification.userInfo! as NSDictionary
        if let status = data.object(forKey: "value") as? String  {
            
            
            if status == "1" {
                perform(#selector(callbackToReload), with: nil, afterDelay: 10)
            }
        }
    }
    
    @objc func callbackToReload() {
        tblView.reloadData()
    }
    
    
    
    func checkActiveService(){
        serviceIconFinal.removeAll()
        activeServices.removeAll()
        serviceTitleFinal.removeAll()
        for i in 0 ..< serviceTitle.count{
            if let activeService = self.checkActiveService(service : serviceTitle[i]) {
                if activeService["aACTIVE"] != "3" {
                    // Only add data whice have service id 1 or 2. 3 is disable service
                    serviceTitleFinal.append(serviceTitle[i])
                    serviceIconFinal.append(serviceIcon[i])
                    activeServices.append(activeService)
                }
            }
        }
        tblView.reloadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tblHeight.constant = tblView.contentSize.height + 50
        self.tabBarController?.tabBar.isHidden = false
        getImage()
        imgUser.layer.masksToBounds = false
        imgUser.layer.cornerRadius = imgUser.frame.height/2
        imgUser.clipsToBounds = true
        
        profileImg.layer.masksToBounds = false
        profileImg.layer.cornerRadius = profileImg.frame.height/2
        profileImg.clipsToBounds = true
        
        if tabBarController?.tabBar.items?[3].badgeValue == "0"{
            tabBarController?.tabBar.items?[3].badgeValue = nil
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func showUpdateCUPData() {
        
        if arrMarketingInfo.count == 0 {
            arrMarketingInfo = CoreDataHelper().getDataForEntity(entity: Entity.MARKETING_INFO)
        }
    }
    
    func setupHeader() {
        
        imgHederHeight = self.view.frame.size.height * 0.55
        scrollView.contentInset = UIEdgeInsets(top: UIApplication.shared.statusBarFrame.size.height, left: 0, bottom: 0, right: 0)
        scrollView.delegate = self
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: imgHederHeight)
        imgHeader.contentMode = .scaleAspectFill
        imgHeader.image = UIImage.init(named: "home_bg")
        imgHeader.clipsToBounds = true
        //view.addSubview(imgHeader)
    }
    
    func setFonts() {
        Fonts().set(object: lblHeader, fontType: 2, fontSize: 24, color: Color.white, title: "CAPTION", placeHolder: "")
        Fonts().set(object: lblHowHelp, fontType: 1, fontSize: 14, color: Color.white, title: "HOWHELP", placeHolder: "")
        Fonts().set(object: lblAccountsCard, fontType: 1, fontSize: 14, color: Color.white, title: "ACCOUNTSANDCARD", placeHolder: "")
        
        Fonts().set(object: lblHeader1, fontType: 2, fontSize: 17, color: Color.white, title: "STANBICMOBILE", placeHolder: "")
    }
    
    
    
    
    //Mark:- Custom Methods
    @IBAction func showProfilePictureOptions(_ sender: UIButton) {
        
        
        showActionSheetForProfilePicture(sender)
        
    }
    
    @IBAction func showProfilePictureOptions1(_ sender: UIButton) {
        
        
        showActionSheetForProfilePicture(sender)
        
        
    }
    
    @IBAction func openLeftMenu(_ sender: UIButton) {
        slideMenuController()?.openLeft()
    }
    
    @IBAction func openLeftMenu1(_ sender: UIButton) {
        slideMenuController()?.openLeft()
    }
    
    @IBAction func btnPayUpcomingBill(_ sender: UIButton) {
        payUpcomingBillFlow(row: sender.tag)
    }
    
    @IBAction func chosePhotoBtnAction(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            //self.myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            myPickerController.modalPresentationStyle = .overCurrentContext
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    @IBAction func didMyCheckMyBalanceButtonTapped(_ sender: Any) {
        self.ischeckingBalance = true
        self.selectedAccountBE = self.model.enquiryAccountsInfo?[(sender as AnyObject).tag]["aACCOUNT_ALIAS"] ?? ""
        self.checkingValue.adding(self.selectedAccountBE)
        //selectedAccountBE = (sender as AnyObject).restorationIdentifier ?? ""
        self.perform(#selector(self.callback), with: nil, afterDelay: 0)
        }
    
    @IBAction func didMyViewMinistatementButtonTapped(_ sender: Any) {
        
       self.ischeckingBalance = false
        self.selectedAccountBE = self.model.enquiryAccountsInfo?[(sender as AnyObject).tag]["aACCOUNT_ALIAS"] ?? ""
        self.modelBE.delegate = self
        self.self.modelBE.homeVC = self
        // modelBE.getBE(accountAlise: selectedAccountBE)
        self.perform(#selector(self.callback), with: nil, afterDelay: 1.5)
       }
    
    
    func validAuth() {
        perform(#selector(check), with: nil, afterDelay: 1)
    }
    
    @objc func check() {
        checkBEData()
    }
    
    func checkBEData() {
        if ischeckingBalance == false {
            if let data = OnboardUser.beArray.first(where: {$0["ACCOUNT_ALIAS"].stringValue == selectedAccountBE}) {
                let vc = AccountDetailViewController.getVCInstance() as! AccountDetailViewController
                //vc.dictAccount = data
                vc.ACCOUNT_ALIAS = data["ACCOUNT_ALIAS"].stringValue
                vc.curruntbalance = data["BALANCE"].stringValue
                vc.availablebalance = data["BOOK_BALANCE"].stringValue
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                self.modelBE.getBEMiniStatement(accountAlise: selectedAccountBE, totelcharge: TotelAmount)
            }
            
        } else {
            //First check if we have balance for this alias
            if let _ = OnboardUser.beArray.first(where: {$0["ACCOUNT_ALIAS"].stringValue == selectedAccountBE}) {
                //Check api time
                if let index = Session.sharedInstance.cardInfo?.firstIndex(where: {($0["ACCOUNT_ALIAS"] as! String) == selectedAccountBE}) {
                    var unlockCardData = Session.sharedInstance.cardInfo?[index]
                    
                    let startTime = unlockCardData?["APITIME"] as! Date
                    let elapsed = Date().timeIntervalSince(startTime)
                    
                    if Int(elapsed) <= global.balance_expire_time {
                        unlockCardData?["CARDSHOW"] = true
                        Session.sharedInstance.cardInfo?[index] = unlockCardData!
                        Session.sharedInstance.timerManager.startTimer(target: self, selector: #selector(self.lockCardNew(_:)), alise: selectedAccountBE)
                        self.collectionView.reloadData()
                    }
                    else {
                        self.fetchBalanceEnquiryFor(accountAlise: selectedAccountBE) // High prority
                    }
                }
                else {
                    self.fetchBalanceEnquiryFor(accountAlise: selectedAccountBE) // Low prority
                }
            }
            else {
                self.fetchBalanceEnquiryFor(accountAlise: selectedAccountBE) // High prority
            }
        }
    }
    
    @objc func callback() {
        
        var acno = ""
        if selectedAccountBE.contains("KES-"){
            let arrac = selectedAccountBE.components(separatedBy: "KES-")[1]
            acno = arrac
        }
        else {
            acno = selectedAccountBE
        }
        
                self.validate()
        
      
    }
}

extension Home2ViewController : UITableViewDelegate, UITableViewDataSource {
    
    //MARK: UITableView DataSource implementation
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if modelFetchBill.responseData?.fetchedBill.count == nil || modelFetchBill.responseData?.fetchedBill.count == 0   {
            if headerTitle.count > 0{
                headerTitle = headerTitle.filter(){$0 != "UPCOMINGBILL"}
                return headerTitle.count
            }
            else {
                return headerTitle.count
                
            }
        }
        else {
            return headerTitle.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "HEADER") as! HeaderTableViewCell
        header.lblHeaderTitle.text = headerTitle[section].localized(Session.sharedInstance.appLanguage)
        header.contentView.backgroundColor = Color.white
        return header.contentView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let coreData = CoreDataHelper()
        if headerTitle[section] == "UPCOMINGBILL" {
            return modelFetchBill.responseData?.fetchedBill.count ?? 1
            
            
        } else if headerTitle[section] == "PROMO" {
            if (coreData.getDataForEntity(entity: Entity.PROMOTIONS)).count > 0{
                return 1
            } else {
                return 1
            }
            
        } else {
            if (coreData.getDataForEntity(entity: Entity.WHAT_WE_OFFER)).count > 0{
                return 1
            }
            else {
                return 0
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if headerTitle[indexPath.section] == "UPCOMINGBILL" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UPCOMINGBILL",   for: indexPath) as! UpcomingBillTableViewCell
            cell.btnPay.tag = indexPath.row
            if let data = modelFetchBill.responseData?.fetchedBill[indexPath.row] {
                cell.initiateData(data: data)
                cell.cardView.isHidden = true
            } else {
                cell.cardView.isHidden = false
            }
            return cell
        } else if headerTitle[indexPath.section] == "PROMO" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PROMOTIONS",   for: indexPath) as! PromotionTableViewCell
            cell.selectedViewController = self
            cell.collectionViewPromotions.reloadData()
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OFFERCELL",   for: indexPath) as! OfferTableViewCell
            cell.selectedViewController = self
            cell.collectionViewOffer.reloadData()
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tblHeight.constant = tblView.contentSize.height + 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if headerTitle[indexPath.section] == "UPCOMINGBILL" {
            return 107
        } else if headerTitle[indexPath.section] == "PROMO" {
            return 153
        } else {
            return 200
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 { // Upcoming Bill
            payUpcomingBillFlow(row: indexPath.row)
        }
    }
}

extension Home2ViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewServices {
            return serviceTitleFinal.count
        }  else {
            return model.enquiryAccountsInfo?.count ?? 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionViewServices {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SERVICES", for: indexPath) as! ServicesCollectionViewCell
            let activeService = activeServices[indexPath.row]
            if activeService["aACTIVE"] == "2" {
                cell.imglayerview.isHidden = false
            }
            else {
                cell.imglayerview.isHidden = true
            }
            cell.imgIcon.image = UIImage.init(named: serviceIconFinal[indexPath.row])
            
            Fonts().set(object: cell.lblTitle, fontType: 1, fontSize: 9, color: Color.white, title: serviceTitleFinal[indexPath.row], placeHolder: "")
            cell.lblTitle.alpha = 0.8
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ACCOUNTS", for: indexPath) as! AccountsCollectionViewCell
            
            // MARK: "Cell Is here"
            let dict = model.enquiryAccountsInfo?[indexPath.row] ?? [String : String]()
            cell.lblAcType.text = dict["aACCOUNT_ALIAS"] ?? global.cardacalis
            let str = dict["aACCOUNT_NUMBER"] ?? global.cardacc
            cell.lblAcNo.text = str.hashingOfTheAccount(noStr: str)
            
            if dict.count > 2 {
                global.cardacalis = dict["aACCOUNT_ALIAS"] ?? "NA"
                global.cardacc = dict["aACCOUNT_ALIAS"] ?? "XX-XXX-XXX-XXX"
            }
            if ischeckingBalance {
                self.isStatus = true
                cell.initiateData(data: model.enquiryAccountsInfo?[indexPath.row] ?? [String : String]())
            } else {
                self.isStatus = false
                cell.shouldHideInfo(flag: true, data: nil)
            }
            
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [UIColor.purple.cgColor, UIColor.yellow.cgColor]
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 1, y: 1)
            gradientLayer.frame = cell.viewContainer.bounds
            
            //  card_newblue -  cardblue || cardgreen  - card_newgreen
            if (indexPath.row % 2 != 0) {
                cell.backGroundCard.image = UIImage(named: "card_newblue")
            } else {
                cell.backGroundCard.image = UIImage(named: "card_newgreen")
            }
            cell.backGroundCard.contentMode = .center
            
            self.selectedIndex = indexPath
            
            cell.btnViewBalance.tag = indexPath.row
            cell.btnministatementBalance.tag = indexPath.row
            if indexPath.row == 0 && isfirstTimeTransform {
                isfirstTimeTransform = false
            } else {
                cell.transform = transformCellValue
            }
            
            return cell
        }
    }
    
    
    //Mark:- Flowlayout Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewServices {
            return CGSize(width: collectionView.frame.size.width / 4, height: collectionView.frame.size.height)
        }
        else {
            
            if model.enquiryAccountsInfo?.count ?? 0 > 1 {
                return CGSize(width: collectionView.frame.size.width * 0.85, height: collectionView.frame.size.height - 20)
            }
            else {
                return CGSize(width: collectionView.frame.size.width * 0.9, height: collectionView.frame.size.height - 20)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if collectionView != collectionViewServices {
            
            if model.enquiryAccountsInfo?.count ?? 0 > 1 {
                let margin = ((collectionView.frame.size.width) - (collectionView.frame.size.width * 0.85)) / 2
                let edge = UIEdgeInsets(top: 0, left: margin, bottom: 0, right: margin)
                return edge
            }
            else {
                let margin = ((collectionView.frame.size.width) - (collectionView.frame.size.width * 0.9)) / 2
                let edge = UIEdgeInsets(top: 0, left: margin, bottom: 0, right: margin)
                return edge
            }
        }
        else {
            let edge = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return edge
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionViewServices {
            let activeService = activeServices[indexPath.row]
            if activeService["aACTIVE"] == "2" {
                self.showMessage(title: "INACTIVE", message: activeService["aINACTIVE_MESSAGE"] ?? "")
            }
            else {
                serviceSelected(indexPath: indexPath)
            }
            
        } else if collectionView == self.collectionView {
            
            
                    if self.isStatus {
                        self.ischeckingBalance = false
                    } else {
                        self.ischeckingBalance = true
                    }
            
            
            
            
        }
    }
}

//Action from table view and collection view
extension Home2ViewController {
    
    func serviceSelected(indexPath : IndexPath) {
        
        DispatchQueue.main.async {
            
            switch indexPath.row {
                
            case 0: // Send Money
                CurrentFlow.flowType = FlowType.SendMoney
                let vc = ContactListViewController.getVCInstance() as! ContactListViewController
                vc.contacts = self.contacts
                vc.oneArray1 = self.oneArray
                vc.newDick1 = self.newDick
                self.navigationController?.pushViewController(vc, animated: true)
                
            case 1:  // Pay Bill
                CurrentFlow.flowType = FlowType.PayBill
                let vc = PayBillViewController.getVCInstance() as! PayBillViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            case 2:  // BuyAirtime
                CurrentFlow.flowType = FlowType.BuyAirtime
                let vc = ContactListViewController.getVCInstance() as! ContactListViewController
                //vc.contacts = self.contacts
                //vc.oneArray1 = self.oneArray
                //vc.newDick1 = self.newDick
                self.navigationController?.pushViewController(vc, animated: true)
                
                //            CurrentFlow.flowType = FlowType.BuyAirtime
                //            let vc = ContactListVC.getVCInstance() as! ContactListVC
                //            self.navigationController?.pushViewController(vc, animated: true)
                
                
            case 3: // LOAN SHOW
                self.FetchSalary()
                //            let vc = SalaryAdvanceLoanViewController.getVCInstance() as! SalaryAdvanceLoanViewController
                //            self.navigationController?.pushViewController(vc, animated: true)
                
            default:
                return
            }
        }
    }
    
    func accountSelected(indexPath : IndexPath) {
        selectedAccountBE = model.enquiryAccountsInfo?[indexPath.row]["aACCOUNT_ALIAS"] ?? ""
         self.validate()
       }
    
    
    func payUpcomingBillFlow(row : Int) {
        
        CurrentFlow.flowType = FlowType.PayBill
        let nextVC = PayUpcomingBillViewController.getVCInstance() as! PayUpcomingBillViewController
        if let data = modelFetchBill.responseData?.fetchedBill[row] {
            nextVC.billInfo = data
        }
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    // called every time interval from the timer
}

//Call API CUP
extension Home2ViewController : CUPDelegate {
    
    func fetchCUP() {
        let mobileArr = CoreDataHelper().getDataForEntity(entity: Entity.MSISDN)
        if mobileArr.count > 0 {
            OnboardUser.mobileNo = mobileArr[0]["aMSISDN"] ?? ""
        }
        let pin = CoreDataHelper().getDataForEntity(entity: Entity.PIN)
        if pin.count > 0 {
            OnboardUser.pin = pin[0]["aPIN"] ?? ""
        }
        model.delegate = self
        model.getCUP()
    }
    
    func reloadDataForCUP(service: String) {
        checkActiveService()
        
        if let response = model.jsonData { // If status code will be 200 then json data will be nil
            self.showMessage(title: "", message: response["STATUS_MESSAGE"].stringValue)
        }
        else {
            showCaption()
            pagerView.numberOfPages = model.enquiryAccountsInfo?.count ?? 0
            tblView.reloadData()
            collectionView.reloadData()
        }
    }
}

//Call API BE
extension Home2ViewController : BEDelegate {
    
    // when show ministament without view balance :_
    func dataMiniStatements(data: JSON) {
        let vc = AccountDetailViewController.getVCInstance() as! AccountDetailViewController
        //vc.dictAccount = data
        vc.totelcharge = TotelAmount
        vc.ACCOUNT_ALIAS = data["ACCOUNT_ALIAS"].stringValue
        vc.curruntbalance = data["BALANCE"].stringValue
        vc.availablebalance = data["BOOK_BALANCE"].stringValue
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK:_ fetchBalanceEnquiry
    func fetchBalanceEnquiryFor(accountAlise : String) {
        modelBE.delegate = self
        modelBE.homeVC = self
        modelBE.getBE(accountAlise: accountAlise, totelcharge: TotelAmount)
    }
    
    func showBalanceFor(accountAlise: String, message: String, code : Int) {
        if code == 200 {
            collectionView.reloadData()
        }
        else {
            let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
            if message == "" {
                let str = "SERVERERROR"
                nextvc.message = str.localized(Session.sharedInstance.appLanguage)
            }
            else {
                nextvc.message = message
            }
            
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
}

//Call API BE
extension Home2ViewController : FetchBillDelegate {
    
    func fetchBill() {
        
        
        modelFetchBill.delegate = self
        modelFetchBill.getBills()
    }
    
    func fetchedBill(message: String) {
        
        if modelFetchBill.responseData?.statusCode == 200 {
            if (modelFetchBill.responseData?.fetchedBill.count ?? 0) == 0 {
                headerTitle = ["PROMO", "OFFER"]
            }
            else if (modelFetchBill.responseData?.fetchedBill.count ?? 0) > 0 {
                if !headerTitle.contains("UPCOMINGBILL") {
                    headerTitle = ["UPCOMINGBILL", "PROMO", "OFFER"]
                }
            }
            
            tblView.reloadData()
        }
        else {
            //   self.showMessage(title: "", message: modelFetchBill.responseData?.statusMessage ?? "")
        }
        
    }
}

extension Home2ViewController: AuthDelegate {
    func validate() {
        //Check if user enable biometric access
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            let context = LAContext()
            Authntication(context).authnticateUser { (success, message) in
                DispatchQueue.main.async {
                    if success == true {
                        global.enterpin = OnboardUser.pin
                        self.validAuth()
                    }
                    else {
                        if message != "" {
                            self.showMessage(title: "", message: message)
                        }
                    }
                    
                }
            }
        } else {
            global.enterpin = ""
            let vc = AuthoriseViewController.getVCInstance() as! AuthoriseViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension Home2ViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.collectionView == scrollView as? UICollectionView {
            let cardCellWidth = collectionView.frame.size.width * 0.85
            let indx = Int(scrollView.contentOffset.x + 150) / Int(cardCellWidth)
            pagerView.currentPage = indx
            
        }  else if scrollView == self.scrollView {
            let y = imgHederHeight - (scrollView.contentOffset.y)
            let height = min(max(y, 100), 400)
            imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
            
            // This is the offset at the bottom of the scroll view.
            let totalScroll: CGFloat = scrollView.contentSize.height - scrollView.bounds.size.height
            
            // This is the current offset.
            let offset: CGFloat = scrollView.contentOffset.y
            
            // This is the percentage of the current offset / bottom offset.
            let percentage: CGFloat = offset / totalScroll
            
            // When percentage = 0, the alpha should be 1 so we should flip the percentage.
            viewHeader.alpha = (percentage * 3)
            
        }
        Global.invalidTimer()
    }
}

extension Home2ViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func showActionSheetForProfilePicture(_ sender: UIButton) {
        let appLanguage = Session.sharedInstance.appLanguage
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //makeAttributed(title: LabelField.actionAlwaysDisplayBalance)
        alert.view.tintColor = Color.c0_179_255
        
        let action1 = UIAlertAction(title: "TAKENEWPHOTO".localized(appLanguage as String), style: .default, handler: { (UIAlertAction)in
            self.tabBarController?.tabBar.isHidden = true
            self.takePhotoFromCamera()
        })
        
        let action2 = UIAlertAction(title: "CHOOSEEXISTING", style: .default, handler: { (UIAlertAction)in
            self.tabBarController?.tabBar.isHidden = true
            self.takePhotoFromLibrary()
        })
        
        let action3 = UIAlertAction(title: "CANCEL", style: .cancel, handler: { (UIAlertAction)in
            
        })
        //  action1.setValue(UIImage.init(named: "next arrow"), forKey: "image")
        
        //        let image = UIImage(named: "cameras")
        //        action1.setValue(image?.withRenderingMode(.automatic), forKey: "image")
        //
        //        let image1 = UIImage(named: " gallrays")
        //        action2.setValue(image1?.withRenderingMode(.automatic), forKey: "image")
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
        
        
        guard let label1 = (action1.value(forKey: "__representer")as? NSObject)?.value(forKey: "label") as? UILabel else { return }
        label1.attributedText = makeAttributed(title: "TAKENEWPHOTO".localized(appLanguage as String))
        
        guard let label2 = (action2.value(forKey: "__representer")as? NSObject)?.value(forKey: "label") as? UILabel else { return }
        label2.attributedText = makeAttributed(title: "CHOOSEEXISTING".localized(appLanguage as String))
        
        guard let label3 = (action3.value(forKey: "__representer")as? NSObject)?.value(forKey: "label") as? UILabel else { return }
        label3.attributedText = makeAttributed(title: "CANCEL".localized(appLanguage as String))
    }
    
    func takePhotoFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            //self.myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            myPickerController.modalPresentationStyle = .overCurrentContext
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func takePhotoFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            //self.myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            myPickerController.modalPresentationStyle = .overCurrentContext
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func makeAttributed(title : String) -> NSMutableAttributedString {
        let attributedText = NSMutableAttributedString(string: title)
        
        let range = NSRange(location: 0, length: attributedText.length)
        attributedText.addAttribute(NSAttributedString.Key.kern, value: 1.5, range: range)
        attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont(name: Fonts.kFontMedium, size: 19.0)!, range: range)
        return attributedText
    }
    
    func showCaption() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        let time = dateFormatter.string(from: Date())
        let timeStr = time.replacingOccurrences(of: ":", with: ".")
        let hourMinute = Float(timeStr)
        lblHeader.text = getMsg(hourMinute!)
        
    }
    
    func getMsg(_ hour: Float) -> String{
        
        if (model.profileInfo?.count ?? 0) > 0 {
            let dict = model.profileInfo?[0]
            let userProfile = dict?["aFIRST_NAME"]?.capitalized
            
            
            if hour >= 0 && hour <= 11.30 {
                return "Good Morning,\n\(userProfile ?? "110")!"
            } else if hour > 11.30 && hour <= 16.30 {
                return "Good Afternoon,\n\(userProfile  ?? "111")!"
            } else {
                return "Good Evening,\n\(userProfile ?? "112")!"
            }
        }
        else {
            if hour >= 0 && hour <= 11.30 {
                return "Good Morning,"
            } else if hour > 11.30 && hour <= 16.30 {
                return "Good Afternoon,"
            } else {
                return "Good Evening,"
            }
        }
    }
}

// Open Camera and Gallery _.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._
extension Home2ViewController {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.tabBarController?.tabBar.isHidden = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.tabBarController?.tabBar.isHidden = false
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            setAndSavePhoto(photo: image)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func setAndSavePhoto(photo: UIImage){
        self.imgUser.image = photo
        self.profileImg.image = photo
        saveImageDocumentDirectory(imageToSave: photo)
    }
    
    func saveImageDocumentDirectory(imageToSave: UIImage) {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("profileImage.jpg")
        let image = imageToSave
        let imageData = image.jpegData(compressionQuality: 0.75)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
    }
    
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func getImage(){
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent("profileImage.jpg")
        if fileManager.fileExists(atPath: imagePAth){
            self.imgUser.image = UIImage(contentsOfFile: imagePAth)
            profileImg.image = self.imgUser.image
        }else{
            
        }
    }
    
    @objc func getImageNotification(){
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent("profileImage.jpg")
        if fileManager.fileExists(atPath: imagePAth){
            self.imgUser.image = UIImage(contentsOfFile: imagePAth)
            profileImg.image = self.imgUser.image
        }else{
            
        }
    }
    
    @objc func getBedgeNotification(){
        
        if (UIApplication.shared.delegate as! AppDelegate).appBackground == true{
            let no = UIApplication.shared.applicationIconBadgeNumber
            if no < 0{
                tabBarController?.tabBar.items?[3].badgeValue = nil
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
            else if no == 0 {
                tabBarController?.tabBar.items?[3].badgeValue = "\(no)"
                UIApplication.shared.applicationIconBadgeNumber = no
            }
            else {
                tabBarController?.tabBar.items?[3].badgeValue = "\(no)"
                UIApplication.shared.applicationIconBadgeNumber = no
            }
        }
        else {
            if UserDefaults.standard.object(forKey: "badgeCount") != nil{
                let badgeCount = UserDefaults.standard.object(forKey: "badgeCount") as! Int
                if badgeCount < 0 {
                    tabBarController?.tabBar.items?[3].badgeValue = nil
                    UIApplication.shared.applicationIconBadgeNumber = 0
                }
                else if badgeCount == 0 {
                    tabBarController?.tabBar.items?[3].badgeValue = nil
                    UIApplication.shared.applicationIconBadgeNumber = 0
                }
                else {
                    tabBarController?.tabBar.items?[3].badgeValue = "\(badgeCount)"
                    UIApplication.shared.applicationIconBadgeNumber = badgeCount
                }
                
            }
        }
        
        if tabBarController?.tabBar.items?[3].badgeValue == "0"{
            tabBarController?.tabBar.items?[3].badgeValue = nil
        }
    }
}

// Fetch COntact Fastly
extension Home2ViewController : ContactVMDelegate{
    
    func reloadData(status: Int) {
        if status == 200 {
            contacts = modelcontacts.responseData
            sortContact()
        }
    }
    
    func sortContact() {
        contacts = contacts.sorted { $0.givenName.localizedCaseInsensitiveCompare($1.givenName) == ComparisonResult.orderedAscending }
        
        for i in 0..<contacts.count
        {
            if let firstLetter = contacts[i].givenName.first
            {
                if(!oneArray.contains((firstLetter)))
                {
                    oneArray.append(firstLetter)
                }
            }
        }
        
        for i in 0..<oneArray.count
        {
            var contactarray = [CNContact]()
            for j in 0..<contacts.count
            {
                if oneArray[i] == contacts[j].givenName.first
                {
                    contactarray.append(contacts[j])
                }
                
            }
            newDick[oneArray[i]] = contactarray
        }
        
    }
}

extension Home2ViewController:SalaryVMDelegate {
    
    
    func FetchSalary() {
        if !isInternetAvailable() {
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            modelsalarayad.SalaryAdvance(mob: OnboardUser.mobileNo, accountalias:  acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!, action: "FETCH", amount: "")
        }
    }
    
    func salayData() {
        DispatchQueue.main.async {
            if self.modelsalarayad.responseData?.statusCode == 200 {
                
                let nextvc = SalaryAdvanceLoanViewController.getVCInstance() as! SalaryAdvanceLoanViewController
                nextvc.model = self.modelsalarayad
                self.navigationController?.pushViewController(nextvc, animated: true)
            } else {
                let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
                nextvc.message = self.modelsalarayad.responseData?.statusMessage ?? ""
                nextvc.Flow = "salary"
                self.navigationController?.pushViewController(nextvc, animated: true)
            }
        }
    }
    
    // Service is changed
    @objc func lockCardNew(_ timer : Timer) {
        if let userInfo = (timer.userInfo ?? "") as? String {
            if let index = Session.sharedInstance.cardInfo?.firstIndex(where: {($0["ACCOUNT_ALIAS"] as! String) == userInfo}) {
                var unlockCardData = Session.sharedInstance.cardInfo?[index]
                unlockCardData?["CARDSHOW"] = false
                Session.sharedInstance.cardInfo?[index] = unlockCardData!
                Session.sharedInstance.timerManager.stopTimer(alise: userInfo)
                self.collectionView.reloadData()
            }
        }
        
    }
}


extension UIDevice {
    static var type: UIUserInterfaceIdiom
    { return UIDevice.current.userInterfaceIdiom }
}
