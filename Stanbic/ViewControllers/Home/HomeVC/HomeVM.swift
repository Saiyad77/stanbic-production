//
//  HomeVM.swift
//  Stanbic
//
//  Created by Vijay Patidar on 23/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import CoreData
import UIKit

protocol CUPDelegate: class {
    func reloadDataForCUP(service : String)
}

protocol BEDelegate: class {
    func showBalanceFor(accountAlise : String, message : String, code : Int)
    func dataMiniStatements(data : JSON)
    
}

protocol FetchBillDelegate: class {
    func fetchedBill(message : String)
}


// View Model
class CUP {
    
    weak var delegate : CUPDelegate!
    var enquiryAccountsInfo : [[String : String]]?
    var profileInfo : [[String : String]]?
    var jsonData : JSON?
    
    // Call API for CUP
    func getCUP() {
        
      //  if global.cupApi != "NO"{
       
            RestAPIManager.checkUpdatesFetchSystemSettings(title: "PLEASEWAIT", subTitle: "FETCHCUP") { (json) in
                if json["STATUS_CODE"].intValue == 200 {
                    DispatchQueue.main.async {
                        
                        self.jsonData = nil  // no need to pass jsondata to view controller
                        let coreData = CoreDataHelper()
                        //SAVE
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.ACCOUNTS_INFO, jsonKey: Entity.ACCOUNTS_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.ENQUIRY_ACCOUNTS_INFO, jsonKey: Entity.ENQUIRY_ACCOUNTS_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.PROFILES_INFO, jsonKey: Entity.PROFILES_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.ENROLLMENTS_INFO, jsonKey: Entity.ENROLLMENTS_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.BENEFICIARIES_INFO, jsonKey: Entity.BENEFICIARIES_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.MONEY_TRANSFER_INFO, jsonKey: Entity.MONEY_TRANSFER_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.BANKS_INFO, jsonKey: Entity.BANKS_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.BANK_BRANCHES_INFO, jsonKey: Entity.BANK_BRANCHES_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.CURRENCIES_INFO, jsonKey: Entity.CURRENCIES_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.MNO_INFO, jsonKey: Entity.MNO_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.BILLERS_INFO, jsonKey: Entity.BILLERS_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.CARD_INFO, jsonKey: Entity.CARD_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.CONTACT_INFO, jsonKey: Entity.CONTACT_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.MOBILE_MONEY_INFO, jsonKey: Entity.MOBILE_MONEY_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.SERVICES_INFO, jsonKey: Entity.SERVICES_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.MARKETING_INFO, jsonKey: Entity.MARKETING_INFO.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.PROMOTIONS, jsonKey: Entity.PROMOTIONS.rawValue)
                        coreData.saveArrayFor(json: json["RESULT_ARRAY"], entityObject: Entity.WHAT_WE_OFFER, jsonKey: Entity.WHAT_WE_OFFER.rawValue)
                        
                        let arr = json["RESULT_ARRAY"].dictionaryValue
                        global.percent = arr["EXCISE_DUTY"]?.stringValue ?? "0"
                        global.dictRegax = arr["REGEXES"]!.dictionaryValue
                        global.termsandcounditions = arr["TERMS_CONDITIONS"]?.stringValue ?? ""
                        global.privacyandpolicies = arr["PRIVACY_POLICY"]?.stringValue ?? ""
                        let about = arr["ABOUT_US"]?.dictionaryValue
                        let imageurl = about!["IMAGE_URL"]?.stringValue
                        let test = about!["TEXT"]?.stringValue
                        global.ftmessage = arr["FT_MESSAGE"]?.stringValue ?? ""
                        
                        global.timeout = arr["SESSION_TIMEOUT"]?.stringValue ?? "120"
                        global.loandesc = arr["LOAN_DISCLAIMER"]?.stringValue ?? ""
                        UserDefaults.standard.setValue(imageurl, forKey: "ImageUrl")
                        UserDefaults.standard.setValue(test, forKey: "AboutUs")
                        UserDefaults.standard.synchronize()
                        
                        global.balance_expire_time = arr["BALANCE_EXPIRE_TIME"]?.int ?? 40
                        //
                     //  coreData.saveArrayFor(json: arrAboutus, entityObject: Entity.ABOUT_US, jsonKey: Entity.ABOUT_US.rawValue)
                        
                        //GET VALUE
                        self.enquiryAccountsInfo = coreData.getDataForEntity(entity: Entity.ENQUIRY_ACCOUNTS_INFO)
                        self.enquiryAccountsInfo?.sort(by: {($0["aACCOUNT_ALIAS"] as! String) < $1["aACCOUNT_ALIAS"] as! String})
                        
                        self.profileInfo = coreData.getDataForEntity(entity: Entity.PROFILES_INFO)
                        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo: ["value":"1"])
                        
                        // creditcard array
                        
                        let creditcard = arr["CARD_INFO"]?.arrayValue
                        global.cardInfo.removeAll()
                        for i in 0 ..< creditcard!.count {
                            let dict = creditcard![i].dictionaryValue
                            if dict["CHARGE"] != nil{
                            let dictnew = ["ID":dict["CARD_ID"]!.stringValue,"Charge":dict["CHARGE"]!.dictionaryValue] as [String : Any]
                            global.cardInfo.append(dictnew)
                            }
                        }
                        self.delegate.reloadDataForCUP(service: "CUP")
                       }
                } else {
                    self.jsonData = json
                    self.delegate.reloadDataForCUP(service: "CUP")
            }
        }
    }
}

// View Model
class BalanceEnquiry {
    
    weak var delegate : BEDelegate!
    var enquiryAccountsInfo : [[String : String]]?
    var profileInfo : [[String : String]]?
    var homeVC : Home2ViewController?
    
    // Call API for CUP
    func getBE(accountAlise : String,totelcharge:String) {
        let loader = RestAPIManager.addProgress(title: "PLEASEWAIT", description: "WEAREPROCESSINGYOURREQUEST")
        var lockCardInfo = [[String : Any]]()
         var acalis = ""
        
        RestAPIManager.balanceEnquiryFor(title: "PLEASEWAIT", subTitle: "FETCHCUP", accountAlise: accountAlise ) { (json) in
            DispatchQueue.main.async {
                loader.remove()
            }
            DispatchQueue.main.async {
                if json["STATUS_CODE"].intValue == 200 {
                    for (key, value) in json["RESULT_ARRAY"].dictionaryValue {
                        if let index = OnboardUser.beArray.firstIndex(where: {($0["ACCOUNT_ALIAS"].stringValue) == accountAlise}) {
                            OnboardUser.beArray[index] = value
                        }
                        else {
                            OnboardUser.beArray.append(value)
                        }
                        let dict = json["RESULT_ARRAY"].dictionaryValue[key]
                        
                        let cardInfo = [
                            "ACCOUNT_ALIAS" :
                            "\(dict?["ACCOUNT_ALIAS"] ?? "")",
                            "UNLOCKTIME" : Date(),
                            "APITIME" : Date(),
                            "CARDSHOW":true
                            ] as [String : Any]
                        acalis = "\(dict?["ACCOUNT_ALIAS"] ?? "")"
                        
                        lockCardInfo.append(cardInfo)
                     }
                    Session.sharedInstance.cardInfo = lockCardInfo
                    Session.sharedInstance.timerManager.startTimer(target: self.homeVC!, selector: #selector(self.homeVC!.lockCardNew(_:)), alise: acalis)
                }
                self.delegate.showBalanceFor(accountAlise: accountAlise, message: json["STATUS_MESSAGE"].stringValue, code : json["STATUS_CODE"].intValue)
            }
        }
    }
    
    // Call API for CUP
    func getBEMiniStatement(accountAlise : String,totelcharge : String) {
        let loader = RestAPIManager.addProgress(title: "PLEASEWAIT", description: "WEAREPROCESSINGYOURREQUEST")
        RestAPIManager.balanceEnquiryFor(title: "PLEASEWAIT", subTitle: "FETCHCUP", accountAlise: accountAlise) { (json) in
            DispatchQueue.main.async {
                loader.remove()
            }
            DispatchQueue.main.async {
                if json["STATUS_CODE"].intValue == 200 {
                  for (key, value) in json["RESULT_ARRAY"].dictionaryValue {
                     if value["ACCOUNT_ALIAS"].stringValue == accountAlise {
                        self.delegate.dataMiniStatements(data : value)
                    }
                  }
                    
                }
                self.delegate.showBalanceFor(accountAlise: accountAlise, message: json["STATUS_MESSAGE"].stringValue, code : json["STATUS_CODE"].intValue)
                
            }
        }
    }
    
   /* // Call API for CUP
    func getBEMIniStatement(accountAlise : String) {
        
       // let loader = RestAPIManager.addProgress(title: "PLEASEWAIT", description: "WEAREPROCESSINGYOURREQUEST")
        
        RestAPIManager.balanceEnquiryFor(title: "PLEASEWAIT", subTitle: "FETCHCUP", accountAlise: accountAlise) { (json) in
            DispatchQueue.main.async {
                //loader.remove()
            }
            DispatchQueue.main.async {
                if json["STATUS_CODE"].intValue == 200 {
                    for (_, value) in json["RESULT_ARRAY"].dictionaryValue {
                        OnboardUser.beArray.append(value)
                    }
                }
              //  self.delegate.showBalanceFor(accountAlise: accountAlise, message: json["STATUS_MESSAGE"].stringValue)
            }
        }
    }
 */
}

// View Model
class FetchBill {
//
//    {
//    "STATUS_CODE" : 200,
//    "STATUS_MESSAGE" : "Successfully retrieved bills.",
//    "FETCHED_BILLS" : [
//    {
//    "AMOUNT" : 344,
//    "BILL_REFERENCE" : "2377927",
//    "BILLER" : "NWC",
//    "BILLER_LOGO" : "https:\/\/mula.co.ke\/mula_ke\/api\/v1\/images\/services\/nairobi_water.jpg",
//    "BILLER_NAME" : "Nairobi Water",
//    "DUE_DATE" : "2019-07-18 17:03:51",
//    "BILL_DESCRIPTION" : "Bill information is available",
//    "CUSTOMER_NAME" : "ROSEMARY WANGUI KIMAKU",
//    "LAST_CHECKED" : "2019-07-18 17:03:51"
//    }
//    ]
//    }
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let fetchedBill: [Result]
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case fetchedBill = "FETCHED_BILLS"
            
        }
    }
    
    
    struct Result: Codable{
        let billDes        : String?
        let billRef        : String?
        let biller         : String?
        let amount         : String?
        let customerName   : String?
        let dueDate        : String?
        let billerName     : String?
        let billerLogo     : String?
        let lastCheck      :String?
        
        private enum CodingKeys: String, CodingKey {
            case billDes = "BILL_DESCRIPTION"
            case billRef = "BILL_REFERENCE"
            case biller = "BILLER"
            case amount = "AMOUNT"
            case customerName = "CUSTOMER_NAME"
            case dueDate = "DUE_DATE"
            case billerName = "BILLER_NAME"
            case billerLogo = "BILLER_LOGO"
            case lastCheck = "LAST_CHECKED"
            
        }
        
    }
    
    weak var delegate : FetchBillDelegate!
    var responseData : Response?
    
    // Call API for CUP
    func getBills() {
        global.cupApi = "NO"
        RestAPIManager.fetchBill(title: "PLEASEWAIT", subTitle: "FETCHBILL", type: Response.self, completion: { (response) in
            self.responseData = response
            DispatchQueue.main.async {
                self.delegate.fetchedBill(message: response.statusMessage ?? "")
            }
            
        }) { (error) in
          //  print(error)
        }
    }
}

//
struct global {
    static var cupApi = ""
    static var timeout = "40"
    static var imgselfi = UIImage()
    static var imgsignature = UIImage()
    static var acnumber = ""
    static var imgCount = ""
    static var idnumber = ""
    static var token = ""
    static var viewbalance = false
    static var initialview = false
    static var dictRegax = [String:JSON]()
    static var privacyandpolicies = ""
    static var termsandcounditions = ""
    static var ftmessage = ""
    static var cardInfo = [[String : Any]]()
    static var creditCardID = ""
    static var balance_expire_time: Int = Int()
    static var data : [[String : String]]?
    static var counter: Int = 0
    static var loandesc = String()
    static var imgPaybill = ""
    static var enterpin = ""
    static var cardacalis = "NA"
    static var cardacc = "XX-XXX-XXX-XXX"
    static var oldDomainState = Data()
    static var showbiometric = false
    static var percent = "0"
    static var pickerValueindustry = [[String : String]]()
    static var type = ""
    
}

