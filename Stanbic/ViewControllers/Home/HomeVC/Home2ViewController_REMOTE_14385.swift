//
//  Home2ViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 19/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import AVFoundation
import LocalAuthentication
import CoreImage
import Contacts
import ContactsUI

class Home2ViewController: UIViewController {
    //MARK:- IBoutlet
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pagerView: UIPageControl!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblHeader1: UILabel!
    @IBOutlet var lblHowHelp: UILabel!
    @IBOutlet var lblAccountsCard: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var collectionViewServices: UICollectionView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet var btnSideBar: UIButton!
    @IBOutlet var btnSideBar1: UIButton!
    private var lastContentOffset: CGFloat = 0
    
    var serviceIcon = ["send_money", "pay_bill", "buy_air_time", "get_a_loan"]
    let serviceTitle = ["Mobile_money", "PB", "BA", "Salary_advance"]
    var serviceTitleFinal = [String]()
    var serviceIconFinal = [String]()
    var activeServices = [[String : String]]()
    var headerTitle = ["UPCOMINGBILL", "PROMO", "OFFER"]
    //MARK: Collection View Variables
    var isfirstTimeTransform = true
    let transformCellValue = CGAffineTransform(scaleX: 1.0, y: 1.0)
    let animationSpeed = 0.2
    let headerTileHeight = CGFloat(100)
    var myPickerController = UIImagePickerController()
    let modelcontacts = ContactVm()
    var contacts = [CNContact]()
    var oneArray = [Character]()
    var newDick = [Character:[CNContact]]()
    //MARK: Variables
    var headerHeight : CGFloat?
    let model = CUP()
    let modelBE = BalanceEnquiry()
    let modelFetchBill = FetchBill()
    var selectedAccountBE : String = ""
    var imgHederHeight : CGFloat = 0
    var arrMarketingInfo = [[String : String]]()
    
      var ministatement = false
    var index = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        modelcontacts.delegate = self
        modelcontacts.delegate = self
        modelcontacts.getContacts()
        
        collectionView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        setFonts()
        setupHeader()
        fetchCUP()
        fetchBill()
        checkActiveService()
        
        if UserDefaults.standard.object(forKey: "badgeCount") != nil{
            tabBarController?.tabBar.items?[3].badgeValue = "\(UserDefaults.standard.object(forKey: "badgeCount") as! Int)"
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(getImageNotification), name: Notification.Name(rawValue: "ImageChange"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(getBedgeNotification), name: Notification.Name(rawValue: "badgecount"), object: nil)
    }
    
    func checkActiveService(){
        for i in 0 ..< serviceTitle.count{
            if let activeService = self.checkActiveService(service : serviceTitle[i].localized(Session.sharedInstance.appLanguage)) {
                if activeService["aACTIVE"] != "3" {
                    // Only add data whice have service id 1 or 2. 3 is disable service
                    serviceTitleFinal.append(serviceTitle[i])
                    serviceIconFinal.append(serviceIcon[i])
                    activeServices.append(activeService)
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tblHeight.constant = tblView.contentSize.height
        self.tabBarController?.tabBar.isHidden = false
        getImage()
        imgUser.layer.masksToBounds = false
        imgUser.layer.cornerRadius = imgUser.frame.height/2
        imgUser.clipsToBounds = true
        
        profileImg.layer.masksToBounds = false
        profileImg.layer.cornerRadius = profileImg.frame.height/2
        profileImg.clipsToBounds = true
        
        if tabBarController?.tabBar.items?[3].badgeValue == "0"{
            tabBarController?.tabBar.items?[3].badgeValue = nil
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getImage()
        if tabBarController?.tabBar.items?[3].badgeValue == "0"{
            tabBarController?.tabBar.items?[3].badgeValue = nil
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func showUpdateCUPData() {
        
        if arrMarketingInfo.count == 0 {
            arrMarketingInfo = CoreDataHelper().getDataForEntity(entity: Entity.MARKETING_INFO)
        }
    }
    
    func setupHeader() {
        
        imgHederHeight = self.view.frame.size.height * 0.55
        scrollView.contentInset = UIEdgeInsets(top: UIApplication.shared.statusBarFrame.size.height, left: 0, bottom: 0, right: 0)
        scrollView.delegate = self
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: imgHederHeight)
        imgHeader.contentMode = .scaleAspectFill
        imgHeader.image = UIImage.init(named: "home_bg")
        imgHeader.clipsToBounds = true
        //view.addSubview(imgHeader)
    }
    
    func setFonts() {
        Fonts().set(object: lblHeader, fontType: 2, fontSize: 24, color: Color.white, title: "CAPTION", placeHolder: "")
        Fonts().set(object: lblHowHelp, fontType: 1, fontSize: 14, color: Color.white, title: "HOWHELP", placeHolder: "")
        Fonts().set(object: lblAccountsCard, fontType: 1, fontSize: 14, color: Color.white, title: "ACCOUNTSANDCARD", placeHolder: "")
        
        Fonts().set(object: lblHeader1, fontType: 2, fontSize: 17, color: Color.white, title: "STANBICMOBILE", placeHolder: "")
    }
    
    //Mark:- Custom Methods
    @IBAction func showProfilePictureOptions(_ sender: UIButton) {
        showActionSheetForProfilePicture()
    }
    
    @IBAction func openLeftMenu(_ sender: UIButton) {
        slideMenuController()?.openLeft()
    }
    
    @IBAction func openLeftMenu1(_ sender: UIButton) {
        slideMenuController()?.openLeft()
    }
    
    @IBAction func btnPayUpcomingBill(_ sender: UIButton) {
        payUpcomingBillFlow(row: sender.tag)
    }
    
    
    @IBAction func chosePhotoBtnAction(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            //self.myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            myPickerController.modalPresentationStyle = .overCurrentContext
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
}

extension Home2ViewController : UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - UITableView implementation
    //MARK: UITableViewDataSource implementation
    func numberOfSections(in tableView: UITableView) -> Int {
        return headerTitle.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "HEADER") as! HeaderTableViewCell
        header.lblHeaderTitle.text = headerTitle[section].localized(Session.sharedInstance.appLanguage)
        header.contentView.backgroundColor = Color.white
        return header.contentView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          let coreData = CoreDataHelper()
        if headerTitle[section] == "UPCOMINGBILL" {
            return modelFetchBill.responseData?.fetchedBill.count ?? 1
        } else if headerTitle[section] == "PROMO" {
            if (coreData.getDataForEntity(entity: Entity.PROMOTIONS)).count > 0{
            return 1
            } else {
            return 1
        }
           
        } else {
            if (coreData.getDataForEntity(entity: Entity.WHAT_WE_OFFER)).count > 0{
                return 1
            }
            else {
                return 0
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if headerTitle[indexPath.section] == "UPCOMINGBILL" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UPCOMINGBILL",   for: indexPath) as! UpcomingBillTableViewCell
            cell.btnPay.tag = indexPath.row
            if let data = modelFetchBill.responseData?.fetchedBill[indexPath.row] {
                cell.initiateData(data: data)
                cell.cardView.isHidden = true
            } else {
                cell.cardView.isHidden = false
            }
            return cell
        } else if headerTitle[indexPath.section] == "PROMO" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PROMOTIONS",   for: indexPath) as! PromotionTableViewCell
             cell.selectedViewController = self
            cell.collectionViewPromotions.reloadData()
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OFFERCELL",   for: indexPath) as! OfferTableViewCell
            cell.selectedViewController = self
             cell.collectionViewOffer.reloadData()

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tblHeight.constant = tblView.contentSize.height
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if headerTitle[indexPath.section] == "UPCOMINGBILL" {
            return 107
    } else if headerTitle[indexPath.section] == "PROMO" {
            return 153
    } else {
            return 200
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 { // Upcoming Bill
            payUpcomingBillFlow(row: indexPath.row)
        }
    }
}

extension Home2ViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewServices {
            return serviceTitleFinal.count
        }
        else {
            return model.enquiryAccountsInfo?.count ?? 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewServices {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SERVICES", for: indexPath) as! ServicesCollectionViewCell
            cell.imgIcon.image = UIImage.init(named: serviceIconFinal[indexPath.row])
            Fonts().set(object: cell.lblTitle, fontType: 1, fontSize: 9, color: Color.white, title: serviceTitleFinal[indexPath.row], placeHolder: "")
            cell.lblTitle.alpha = 0.8
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ACCOUNTS", for: indexPath) as! AccountsCollectionViewCell
            cell.initiateData(data: model.enquiryAccountsInfo?[indexPath.row] ?? [String : String]())
            cell.btnViewBalance.addTarget(self, action: #selector(self.viewBalance(_:)), for: .touchUpInside)
            cell.btnministatementBalance.tag = indexPath.row
            cell.btnministatementBalance.addTarget(self, action: #selector(self.viewMiniStatement(_:)), for: .touchUpInside)
            
            
            if indexPath.row == 0 && isfirstTimeTransform {
                isfirstTimeTransform = false
                
            }
            else {
                cell.transform = transformCellValue
               
            }
            return cell
        }
    }
    
    //Mark:- Flowlayout Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewServices {
            return CGSize(width: collectionView.frame.size.width / 4, height: collectionView.frame.size.height)
        }
        else {
            
            if model.enquiryAccountsInfo?.count ?? 0 > 1 {
                return CGSize(width: collectionView.frame.size.width * 0.85, height: collectionView.frame.size.height - 20)
            }
            else {
                return CGSize(width: collectionView.frame.size.width * 0.9, height: collectionView.frame.size.height - 20)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if collectionView != collectionViewServices {
            
            if model.enquiryAccountsInfo?.count ?? 0 > 1 {
                let margin = ((collectionView.frame.size.width) - (collectionView.frame.size.width * 0.85)) / 2
                let edge = UIEdgeInsets(top: 0, left: margin, bottom: 0, right: margin)
                return edge
            }
            else {
                let margin = ((collectionView.frame.size.width) - (collectionView.frame.size.width * 0.9)) / 2
                let edge = UIEdgeInsets(top: 0, left: margin, bottom: 0, right: margin)
                return edge
            }
        }
        else {
            let edge = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return edge
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionViewServices {
            let activeService = activeServices[indexPath.row]
            if activeService["aACTIVE"] == "2" {
                self.showMessage(title: "INACTIVE", message: activeService["aINACTIVE_MESSAGE"] ?? "")
            }
            else {
               serviceSelected(indexPath: indexPath)
            }
            
        }
        else if collectionView == self.collectionView {
            accountSelected(indexPath: indexPath)
        }
    }
}

//Action from table view and collection view
extension Home2ViewController {
    
    // show ministatement
      @objc func viewMiniStatement(_ sender: UIButton) {
         index = sender.tag
        selectedAccountBE = model.enquiryAccountsInfo?[index]["aACCOUNT_ALIAS"] ?? ""
        validate()
      }
    
    @objc func viewBalance(_ sender: UIButton) {
        selectedAccountBE = sender.restorationIdentifier ?? ""
        validate()
    }
    
    func serviceSelected(indexPath : IndexPath) {
        
        switch indexPath.row {
            
        case 0: // Send Money
            CurrentFlow.flowType = FlowType.SendMoney
            let vc = ContactListViewController.getVCInstance() as! ContactListViewController
            vc.contacts = contacts
            vc.oneArray1 = oneArray
            vc.newDick1 = newDick
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 1:  // Pay Bill
            CurrentFlow.flowType = FlowType.PayBill
            let vc = PayBillViewController.getVCInstance() as! PayBillViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 2:  // BuyAirtime
            CurrentFlow.flowType = FlowType.BuyAirtime
            let vc = ContactListViewController.getVCInstance() as! ContactListViewController
            vc.contacts = contacts
            vc.oneArray1 = oneArray
            vc.newDick1 = newDick
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 3: // LOAN SHOW
            let vc = SalaryAdvanceLoanViewController.getVCInstance() as! SalaryAdvanceLoanViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        default:
            return
        }
    }
    
    func accountSelected(indexPath : IndexPath) {
        selectedAccountBE = model.enquiryAccountsInfo?[indexPath.row]["aACCOUNT_ALIAS"] ?? ""
        validate()
    }
    
    func payUpcomingBillFlow(row : Int) {
        
        CurrentFlow.flowType = FlowType.PayBill
        let nextVC = PayUpcomingBillViewController.getVCInstance() as! PayUpcomingBillViewController
        if let data = modelFetchBill.responseData?.fetchedBill[row] {
            nextVC.billInfo = data
        }
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
}

//Call API CUP
extension Home2ViewController : CUPDelegate {
    
    func fetchCUP() {
        let mobileArr = CoreDataHelper().getDataForEntity(entity: Entity.MSISDN)
        if mobileArr.count > 0 {
            OnboardUser.mobileNo = mobileArr[0]["aMSISDN"] ?? ""
        }
        let pin = CoreDataHelper().getDataForEntity(entity: Entity.PIN)
        if pin.count > 0 {
            OnboardUser.pin = pin[0]["aPIN"] ?? ""
        }
        model.delegate = self
        model.getCUP()
    }
    
    func reloadDataForCUP(service: String) {
        
        if let response = model.jsonData { // If status code will be 200 then json data will be nil
            self.showMessage(title: "", message: response["STATUS_MESSAGE"].stringValue)
        }
        else {
            showCaption()
            pagerView.numberOfPages = model.enquiryAccountsInfo?.count ?? 0
            collectionView.reloadData()
        }
    }
}

//Call API BE
extension Home2ViewController : BEDelegate {
    
    func fetchBalanceEnquiryFor(accountAlise : String) {
        modelBE.delegate = self
        modelBE.getBE(accountAlise: accountAlise)
    }
    
    func showBalanceFor(accountAlise: String, message: String) {
        if OnboardUser.beArray.count > 0 {
            collectionView.reloadData()
        }
        else {
            // self.showMessage(title: "", message: message)
            
            let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
            if message == ""{
                let str = "SERVERERROR"
                nextvc.message = str.localized(Session.sharedInstance.appLanguage)
            }
            else {
                nextvc.message = message
            }
            
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
}

//Call API BE
extension Home2ViewController : FetchBillDelegate {
    
    func fetchBill() {
        modelFetchBill.delegate = self
        modelFetchBill.getBills()
    }
    
    func fetchedBill(message: String) {
        
        if modelFetchBill.responseData?.statusCode == 200 {
            if (modelFetchBill.responseData?.fetchedBill.count ?? 0) == 0 {
                headerTitle = ["PROMO", "OFFER"]
            }
            else if (modelFetchBill.responseData?.fetchedBill.count ?? 0) > 0 {
                if !headerTitle.contains("UPCOMINGBILL") {
                    headerTitle = ["UPCOMINGBILL", "PROMO", "OFFER"]
                }
            }
            
            tblView.reloadData()
        }
        else {
            self.showMessage(title: "", message: modelFetchBill.responseData?.statusMessage ?? "")
        }
        
    }
}


extension Home2ViewController: AuthDelegate {
    func validate() {
        //Check if user enable biometric access
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            let context = LAContext()
            Authntication(context).authnticateUser { (success, message) in
                DispatchQueue.main.async {
                    if success == true {
                        self.validAuth()
                    }
                    else {
                        if message != "" {
                            self.showMessage(title: "", message: message)
                        }
                    }
                }
            }
        }
        else {
            let vc = AuthoriseViewController.getVCInstance() as! AuthoriseViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func validAuth() {
        checkBEData()
    }
    
    func checkBEData() {
//        if let data = OnboardUser.beArray.first(where: {$0["ACCOUNT_ALIAS"].stringValue == selectedAccountBE}) {
            //Ministatement
        
         let data = model.enquiryAccountsInfo?[index] ?? [String : String]()
        
        
            let vc = AccountDetailViewController.getVCInstance() as! AccountDetailViewController
            //vc.dictAccount = data
        
            self.navigationController?.pushViewController(vc, animated: true)
//
//        } else {
//            self.fetchBalanceEnquiryFor(accountAlise: selectedAccountBE)
//        }
        
    }
}


extension Home2ViewController : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.collectionView == scrollView as? UICollectionView {
            let cardCellWidth = collectionView.frame.size.width * 0.85
            let indx = Int(scrollView.contentOffset.x + 115.0) / Int(cardCellWidth)
            pagerView.currentPage = indx
        }
        else if scrollView == self.scrollView {
            let y = imgHederHeight - (scrollView.contentOffset.y)
            let height = min(max(y, 100), 400)
            imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
            
            // This is the offset at the bottom of the scroll view.
            let totalScroll: CGFloat = scrollView.contentSize.height - scrollView.bounds.size.height
            
            // This is the current offset.
            let offset: CGFloat = scrollView.contentOffset.y
            
            // This is the percentage of the current offset / bottom offset.
            let percentage: CGFloat = offset / totalScroll
            
            // When percentage = 0, the alpha should be 1 so we should flip the percentage.
            viewHeader.alpha = (percentage * 3)
            
        }
    }
}

extension Home2ViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func showActionSheetForProfilePicture() {
        let appLanguage = Session.sharedInstance.appLanguage
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //makeAttributed(title: LabelField.actionAlwaysDisplayBalance)
        alert.view.tintColor = Color.c0_179_255
        
        let action1 = UIAlertAction(title: "TAKENEWPHOTO".localized(appLanguage as String), style: .default, handler: { (UIAlertAction)in
            self.tabBarController?.tabBar.isHidden = true
            self.takePhotoFromCamera()
        })
        
        let action2 = UIAlertAction(title: "CHOOSEEXISTING", style: .default, handler: { (UIAlertAction)in
            self.tabBarController?.tabBar.isHidden = true
            self.takePhotoFromLibrary()
        })
        
        let action3 = UIAlertAction(title: "CANCEL", style: .cancel, handler: { (UIAlertAction)in
            
        })
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
        guard let label1 = (action1.value(forKey: "__representer")as? NSObject)?.value(forKey: "label") as? UILabel else { return }
        label1.attributedText = makeAttributed(title: "TAKENEWPHOTO".localized(appLanguage as String))
        
        guard let label2 = (action2.value(forKey: "__representer")as? NSObject)?.value(forKey: "label") as? UILabel else { return }
        label2.attributedText = makeAttributed(title: "CHOOSEEXISTING".localized(appLanguage as String))
        
        guard let label3 = (action3.value(forKey: "__representer")as? NSObject)?.value(forKey: "label") as? UILabel else { return }
        label3.attributedText = makeAttributed(title: "CANCEL".localized(appLanguage as String))
    }
    
    func takePhotoFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            //self.myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            myPickerController.modalPresentationStyle = .overCurrentContext
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func takePhotoFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            //self.myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            myPickerController.modalPresentationStyle = .overCurrentContext
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func makeAttributed(title : String) -> NSMutableAttributedString {
        let attributedText = NSMutableAttributedString(string: title)
        
        let range = NSRange(location: 0, length: attributedText.length)
        attributedText.addAttribute(NSAttributedString.Key.kern, value: 1.5, range: range)
        attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont(name: Fonts.kFontMedium, size: 19.0)!, range: range)
        return attributedText
    }
    
    func showCaption() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        let time = dateFormatter.string(from: Date())
        let timeStr = time.replacingOccurrences(of: ":", with: ".")
        let hourMinute = Float(timeStr)
        lblHeader.text = getMsg(hourMinute!)
        
    }
    
    func getMsg(_ hour: Float) -> String{
        
        if (model.profileInfo?.count ?? 0) > 0 {
            let dict = model.profileInfo?[0]
            let userProfile = dict?["aFIRST_NAME"]
            print(userProfile!)

            if hour >= 0 && hour <= 11.30 {
                return "Good morning.\n\(userProfile ?? "110")!"
            } else if hour > 11.30 && hour <= 16.30 {
                return "Good afternoon.\n\(userProfile  ?? "111")!"
            } else {
                return "Good evening.\n\(userProfile ?? "112")!"
            }
        }
        else {
            if hour >= 0 && hour <= 11.30 {
                return "Good morning."
            } else if hour > 11.30 && hour <= 16.30 {
                return "Good afternoon."
            } else {
                return "Good evening."
            }
        }
    }
}

// Open Camera and Gallery _.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._.-^-._
extension Home2ViewController {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.tabBarController?.tabBar.isHidden = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.tabBarController?.tabBar.isHidden = false
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            setAndSavePhoto(photo: image)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func setAndSavePhoto(photo: UIImage){
        self.imgUser.image = photo
        saveImageDocumentDirectory(imageToSave: photo)
    }
    
    func saveImageDocumentDirectory(imageToSave: UIImage) {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("profileImage.jpg")
        let image = imageToSave
        let imageData = image.jpegData(compressionQuality: 0.75)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
    }
    
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func getImage(){
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent("profileImage.jpg")
        if fileManager.fileExists(atPath: imagePAth){
            self.imgUser.image = UIImage(contentsOfFile: imagePAth)
            profileImg.image = self.imgUser.image
        }else{
            
        }
    }
    @objc func getImageNotification(){
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent("profileImage.jpg")
        if fileManager.fileExists(atPath: imagePAth){
            self.imgUser.image = UIImage(contentsOfFile: imagePAth)
            profileImg.image = self.imgUser.image
        }else{
            
        }
    }
    
    @objc func getBedgeNotification(){
        
        if (UIApplication.shared.delegate as! AppDelegate).appBackground == true{
            let no = UIApplication.shared.applicationIconBadgeNumber
            if no < 0{
                tabBarController?.tabBar.items?[3].badgeValue = nil
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
            else if no == 0 {
                tabBarController?.tabBar.items?[3].badgeValue = "\(no)"
                UIApplication.shared.applicationIconBadgeNumber = no
            }
            else {
                tabBarController?.tabBar.items?[3].badgeValue = "\(no)"
                 UIApplication.shared.applicationIconBadgeNumber = no
            }
        }
        else {
            if UserDefaults.standard.object(forKey: "badgeCount") != nil{
             let badgeCount = UserDefaults.standard.object(forKey: "badgeCount") as! Int
                if badgeCount < 0 {
                    tabBarController?.tabBar.items?[3].badgeValue = nil
                     UIApplication.shared.applicationIconBadgeNumber = 0
                }
                else if badgeCount == 0 {
                    tabBarController?.tabBar.items?[3].badgeValue = nil
                     UIApplication.shared.applicationIconBadgeNumber = 0
                }
                else {
                    tabBarController?.tabBar.items?[3].badgeValue = "\(badgeCount)"
                      UIApplication.shared.applicationIconBadgeNumber = badgeCount
                }
                
            }
        }
        
        if tabBarController?.tabBar.items?[3].badgeValue == "0"{
            tabBarController?.tabBar.items?[3].badgeValue = nil
        }
    }
    
}

// Fetch COntact Fastly
extension Home2ViewController : ContactVMDelegate{
    
    func reloadData(status: Int) {
        if status == 200 {
            contacts = modelcontacts.responseData
            sortContact()
        }
    }
    
    func sortContact() {
        contacts = contacts.sorted { $0.givenName.localizedCaseInsensitiveCompare($1.givenName) == ComparisonResult.orderedAscending }
        
        for i in 0..<contacts.count
        {
            if let firstLetter = contacts[i].givenName.first
            {
                if(!oneArray.contains((firstLetter)))
                {
                    oneArray.append(firstLetter)
                }
            }
        }
        
        for i in 0..<oneArray.count
        {
            var contactarray = [CNContact]()
            for j in 0..<contacts.count
            {
                if oneArray[i] == contacts[j].givenName.first
                {
                    contactarray.append(contacts[j])
                }
                
            }
            newDick[oneArray[i]] = contactarray
        }
    }
}
