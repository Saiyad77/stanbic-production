//
//  WebViewController.swift
//  Stanbic
//
//  Created by 5exceptions on 17/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKUIDelegate, WKNavigationDelegate,UIGestureRecognizerDelegate, UINavigationControllerDelegate   {
    // MARK: All IBOutlet
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var noDatafoundLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    // MARK: All Properties
    var url: String?
    var phoneNumber: String?
    var titleweb = "Stanbic"
    // MARK: View Controller Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        
        initialSetUpNavigationBar(title: titleweb)
         self.tabBarController?.tabBar.isHidden = true
     }
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Required Method's
    func initialSetUpNavigationBar(title:String){
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = title.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        
        let urlx = Global.getStringValue(url as AnyObject)
        if !urlx.isEmpty {
            
            if  urlx != "null" {
                activityIndicator.isHidden = false
                activityIndicator.startAnimating()
                let url = URL(string: urlx)!
                webView.load(URLRequest(url: url))
                noDatafoundLabel.isHidden = true
            } else {
                if !phoneNumber!.isEmpty {
                    Global.callNumber(phoneNumber: phoneNumber!)
                    noDatafoundLabel.isHidden = true
                }
             }
        } else if !phoneNumber!.isEmpty {
            Global.callNumber(phoneNumber: phoneNumber!)
            noDatafoundLabel.isHidden = true
        } else {
            noDatafoundLabel.isHidden = false
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = true
    
       
    }
    
    // MARK: webViewDidFinishLoad
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
         activityIndicator.stopAnimating()
        activityIndicator.hidesWhenStopped = true
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
       
        isActivitiIndicatorStopAnimating()
     }
    
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        isActivitiIndicatorStopAnimating()
     }
    
    //MARK:- WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
       isActivitiIndicatorStopAnimating()
     }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if webView != self.webView {
            decisionHandler(.allow)
            return
        }
        
        let app = UIApplication.shared
        if let url = navigationAction.request.url {
           
            // Handle target="_blank"
            if navigationAction.targetFrame == nil {
                if app.canOpenURL(url) {
                    app.open(url)
                    decisionHandler(.cancel)
                    return
                }
            }
          
            decisionHandler(.allow)
        }
        
    }
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        isActivitiIndicatorStopAnimating()
    }
    // MARK: Instance of controller
    static func getVCInstance() -> UIViewController{
        return UIStoryboard(name: Storyboards.Home.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func isActivitiIndicatorStopAnimating() {
        activityIndicator.stopAnimating()
        activityIndicator.hidesWhenStopped = true
    }
 }
