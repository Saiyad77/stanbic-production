//
//  CollectionViewCell.swift
//  Stanbic
//
//  Created by Vijay Patidar on 22/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class ServicesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imglayerview: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var shadowview: UILabel!
    
    
    override func awakeFromNib() {
        imglayerview.layer.cornerRadius = 25
     }
}

class AccountsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnViewBalance: UIButton!
    @IBOutlet weak var viewSeperator: AttributedView!
    @IBOutlet weak var lblAcType: UILabel!
    @IBOutlet weak var lblAcNo: UILabel!
    @IBOutlet weak var lblCurrentBalTitle: UILabel!
    @IBOutlet weak var lblCurrentBalValue: UILabel!
    @IBOutlet weak var lblAvlbBalTitle: UILabel!
    @IBOutlet weak var lblAvlbBalValue: UILabel!
    @IBOutlet weak var btnministatementBalance: UIButton!
  //@IBOutlet weak var viewBlue: UIView!
    @IBOutlet weak var backGroundCard: UIImageView!
    
    var timer = Timer()
    var counter: Int = 0
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = 4.0
        layer.shadowRadius = 7
        layer.shadowOpacity = 0.16
        layer.shadowOffset = CGSize(width: 5, height: 7)
        self.clipsToBounds = false
    }
    
    override func awakeFromNib() {
        
        lblAvlbBalTitle.isHidden = true
        lblAvlbBalValue.isHidden = true
        lblCurrentBalTitle.isHidden = true
        lblCurrentBalValue.isHidden = true
        btnViewBalance.isHidden = true
        
        
        Fonts().set(object: lblAcType, fontType: 1, fontSize: 15, color: Color.white, title: "", placeHolder: "")
        Fonts().set(object: lblCurrentBalValue, fontType: 1, fontSize: 14, color: Color.white, title: "KES 00.00", placeHolder: "")
        Fonts().set(object: lblAvlbBalValue, fontType: 1, fontSize: 14, color: Color.white, title: "KES 00.00", placeHolder: "")
        
        Fonts().set(object: lblAcNo, fontType: 1, fontSize: 11, color: Color.white, title: "XX-XXX-XXX-XXX", placeHolder: "")
        Fonts().set(object: lblCurrentBalTitle, fontType: 1, fontSize: 11, color: Color.white, title: "CURRUNTBALANCE", placeHolder: "")
        Fonts().set(object: lblAvlbBalTitle, fontType: 1, fontSize: 11, color: Color.white, title: "AVAILABLEBALANCE", placeHolder: "")
       if DeviceType.IS_IPHONE_5 {
         Fonts().set(object: btnViewBalance, fontType: 1, fontSize: 9, color: Color.white, title: "VIEWMYBALANCE", placeHolder: "")
         Fonts().set(object: btnministatementBalance, fontType: 1, fontSize: 9, color: Color.white, title: "VIEWMINISTATEMENT", placeHolder: "")
          Fonts().set(object: lblAcType, fontType: 1, fontSize: 13, color: Color.white, title: "", placeHolder: "")
        }
       else {
        Fonts().set(object: btnViewBalance, fontType: 1, fontSize: 11, color: Color.white, title: "VIEWMYBALANCE", placeHolder: "")
        Fonts().set(object: btnministatementBalance, fontType: 1, fontSize: 11, color: Color.white, title: "VIEWMINISTATEMENT", placeHolder: "")
        }
        
        lblAvlbBalTitle.alpha = 0.5
        lblCurrentBalTitle.alpha = 0.5
        lblAcNo.alpha = 0.5
        shouldHideInfo(flag : true, data: nil)
    }
    
    func initiateData(data : [String : String]) {
        if data.count != 0 {
            lblAcType.text = data["aACCOUNT_ALIAS"]
            lblAcNo.text = data["aACCOUNT_NUMBER"]!.hashingOfTheAccount(noStr: data["aACCOUNT_NUMBER"]!)
            btnViewBalance.restorationIdentifier = data["aACCOUNT_ALIAS"]
            checkBEData(accountAlise: data["aACCOUNT_ALIAS"] ?? "")
        }
    }
    
    func checkBEData(accountAlise : String) {
        if let data = OnboardUser.beArray.first(where: {$0["ACCOUNT_ALIAS"].stringValue == accountAlise}) {
            if let unlockCardData = Session.sharedInstance.cardInfo?.first(where: {($0["ACCOUNT_ALIAS"] as! String) == accountAlise}) {
                if (unlockCardData["CARDSHOW"] as! Bool) == true {
                    shouldHideInfo(flag: false, data: data)
                }
                else {
                    shouldHideInfo(flag: true, data: data)
                }
                
            }
            else {
                shouldHideInfo(flag: true, data: nil)
            }
            
        } else {
            shouldHideInfo(flag: true, data: nil)
        }
    }

    
    func shouldHideInfo(flag : Bool, data : JSON?) {
        
        lblAvlbBalTitle.isHidden = flag
        lblAvlbBalValue.isHidden = flag
        lblCurrentBalTitle.isHidden = flag
        lblCurrentBalValue.isHidden = flag
        viewSeperator.isHidden = flag
        btnViewBalance.isHidden = !flag
        btnministatementBalance.isHidden = !flag
         
        if flag == false {
            let currency = data?["CURRENCY"].stringValue ?? ""
            lblCurrentBalValue.text = currency + " " + (data?["BOOK_BALANCE"].stringValue ?? "")
            lblAvlbBalValue.text = currency + " " + (data?["BALANCE"].stringValue ?? "")
        }
    }
}


class PromotionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var cardView: UIView!
    
    override func awakeFromNib() {
        imgView.layer.cornerRadius = 4.0
        imgView.clipsToBounds = true
    }
    
    
}

class OfferCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        imgView.layer.cornerRadius = 4.0
        imgView.clipsToBounds = true
    }
    
}
