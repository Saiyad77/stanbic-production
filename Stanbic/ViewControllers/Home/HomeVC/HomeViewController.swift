//
//  HomeViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 21/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication
import Contacts
import ContactsUI

class HomeViewController: UIViewController {
    
    //MARK:- IBoutlet
    @IBOutlet weak var viewHeader: CustomHeaderView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pagerView: UIPageControl!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var viewAlpha: UIView!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet var lblHowHelp: UILabel!
    @IBOutlet var lblAccountsCard: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var collectionViewServices: UICollectionView!
    
    @IBOutlet var viewTopBar: UIView!
    @IBOutlet var lblStanbicMobile: UILabel!
    @IBOutlet var btnSideBar: UIButton!
    @IBOutlet var imgUserimage: UIImageView!
    
    let serviceIcon = ["send_money", "pay_bill", "buy_air_time", "get_a_loan"]
    let serviceTitle = ["SM", "PB", "BA", "GETLOAN"]
    var headerTitle = ["UPCOMINGBILL", "PROMO", "OFFER"]
    
    //MARK: Collection View Variables
    var isfirstTimeTransform = true
    let transformCellValue = CGAffineTransform(scaleX: 1.0, y: 1.0)
    let animationSpeed = 0.2
    let headerTileHeight = CGFloat(100)
    var myPickerController = UIImagePickerController()
    
  
    
    //MARK: Variables
    var headerHeight : CGFloat?
    let model = CUP()
    let modelBE = BalanceEnquiry()
    let modelFetchBill = FetchBill()
    var selectedAccountBE : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        setFonts()
        setUpHeader()
        //fetchCUP()x
        //fetchBill()
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
       
    }
    

    func setFonts() {
        Fonts().set(object: lblHeader, fontType: 2, fontSize: 24, color: Color.white, title: "CAPTION", placeHolder: "")
        Fonts().set(object: lblHowHelp, fontType: 1, fontSize: 14, color: Color.white, title: "HOWHELP", placeHolder: "")
        Fonts().set(object: lblAccountsCard, fontType: 1, fontSize: 14, color: Color.white, title: "ACCOUNTSANDCARD", placeHolder: "")
        Fonts().set(object: lblStanbicMobile, fontType: 2, fontSize: 17, color: Color.white, title: "STANBICMOBILE", placeHolder: "")
    }
    
    //Mark:- Custom Methods
    @IBAction func showProfilePictureOptions(_ sender: UIButton) {
        showActionSheetForProfilePicture()
    }
    
    @IBAction func openLeftMenu(_ sender: UIButton) {
        slideMenuController()?.openLeft()
    }
    
    @IBAction func btnPayUpcomingBill(_ sender: UIButton) {
        payUpcomingBillFlow(row: sender.tag)
    }
}

extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - UITableView implementation
    //MARK: UITableViewDataSource implementation
    func numberOfSections(in tableView: UITableView) -> Int {
        return headerTitle.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "HEADER") as! HeaderTableViewCell
        header.lblHeaderTitle.text = headerTitle[section].localized(Session.sharedInstance.appLanguage)
        header.contentView.backgroundColor = Color.white
        return header.contentView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if headerTitle[section] == "UPCOMINGBILL" {
            return modelFetchBill.responseData?.fetchedBill.count ?? 0
        } else if headerTitle[section] == "PROMO" {
            return 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if headerTitle[indexPath.section] == "UPCOMINGBILL" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UPCOMINGBILL",   for: indexPath) as! UpcomingBillTableViewCell
            cell.btnPay.tag = indexPath.row
            if let data = modelFetchBill.responseData?.fetchedBill[indexPath.row] {
                cell.initiateData(data: data)
            }
            return cell
        } else if headerTitle[indexPath.section] == "PROMO" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PROMOTIONS",   for: indexPath) as! PromotionTableViewCell
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OFFERCELL",   for: indexPath) as! OfferTableViewCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if headerTitle[indexPath.section] == "UPCOMINGBILL" {
           return 107
        }
        else if headerTitle[indexPath.section] == "PROMO" {
            return 153
        } else {
            return 200
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 { // Upcoming Bill
            payUpcomingBillFlow(row: indexPath.row)
        }
    }

}

extension HomeViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewServices {
            return 4
        }
        else {
            return model.enquiryAccountsInfo?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewServices {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SERVICES", for: indexPath) as! ServicesCollectionViewCell
            cell.imgIcon.image = UIImage.init(named: serviceIcon[indexPath.row])
            Fonts().set(object: cell.lblTitle, fontType: 1, fontSize: 9, color: Color.white, title: serviceTitle[indexPath.row], placeHolder: "")
            cell.lblTitle.alpha = 0.8
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ACCOUNTS", for: indexPath) as! AccountsCollectionViewCell
            cell.initiateData(data: model.enquiryAccountsInfo?[indexPath.row] ?? [String : String]())
            cell.btnViewBalance.addTarget(self, action: #selector(self.viewBalance(_:)), for: .touchUpInside)
            if indexPath.row == 0 && isfirstTimeTransform {
                isfirstTimeTransform = false
            }
            else {
                cell.transform = transformCellValue
            }
            //cell.viewContainer = GradientView()
            
            return cell
        }
    }
    
    //Mark:- Flowlayout Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewServices {
            return CGSize(width: collectionView.frame.size.width / 4, height: collectionView.frame.size.height)
        }
        else {
            
            if model.enquiryAccountsInfo?.count ?? 0 > 1 {
                return CGSize(width: collectionView.frame.size.width * 0.85, height: collectionView.frame.size.height - 20)
            }
            else {
               return CGSize(width: collectionView.frame.size.width * 0.9, height: collectionView.frame.size.height - 20)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if collectionView != collectionViewServices {
            
            if model.enquiryAccountsInfo?.count ?? 0 > 1 {
                let margin = ((collectionView.frame.size.width) - (collectionView.frame.size.width * 0.85)) / 2
                let edge = UIEdgeInsets(top: 0, left: margin, bottom: 0, right: margin)
                return edge
            }
            else {
                let margin = ((collectionView.frame.size.width) - (collectionView.frame.size.width * 0.9)) / 2
                let edge = UIEdgeInsets(top: 0, left: margin, bottom: 0, right: margin)
                return edge
            }
        }
        else {
            let edge = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return edge
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionViewServices {
            serviceSelected(indexPath: indexPath)
        }
        else if collectionView == self.collectionView {
            accountSelected(indexPath: indexPath)
        }
    }
}

//Action from table view and collection view
extension HomeViewController {
    
    @objc func viewBalance(_ sender: UIButton) {
        selectedAccountBE = sender.restorationIdentifier ?? ""
        validate()
    }
    
    func serviceSelected(indexPath : IndexPath) {
        
        switch indexPath.row {
            
        case 0: // Send Money
            CurrentFlow.flowType = FlowType.SendMoney
            let vc = ContactListViewController.getVCInstance() as! ContactListViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 1:  // Pay Bill
            CurrentFlow.flowType = FlowType.PayBill
            let vc = PayBillViewController.getVCInstance() as! PayBillViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
       case 2:  // BuyAirtime
            CurrentFlow.flowType = FlowType.BuyAirtime
            let vc = ContactListViewController.getVCInstance() as! ContactListViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
       case 3: // LOAN SHOW
            let vc = SelectGetLoanViewController.getVCInstance() as! SelectGetLoanViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        default:
            return
        }
    }
    
    func accountSelected(indexPath : IndexPath) {
        selectedAccountBE = model.enquiryAccountsInfo?[indexPath.row]["aACCOUNT_ALIAS"] ?? ""
        validate()
    }
    
    func payUpcomingBillFlow(row : Int) {
        
        CurrentFlow.flowType = FlowType.PayBill
        let nextVC = PayUpcomingBillViewController.getVCInstance() as! PayUpcomingBillViewController
        if let data = modelFetchBill.responseData?.fetchedBill[row] {
            nextVC.billInfo = data
        }
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
}

//Call API CUP
extension HomeViewController : CUPDelegate {
    
    func fetchCUP() {
        let mobileArr = CoreDataHelper().getDataForEntity(entity: Entity.MSISDN)
        if mobileArr.count > 0 {
            OnboardUser.mobileNo = mobileArr[0]["aMSISDN"] ?? ""
        }
        let pin = CoreDataHelper().getDataForEntity(entity: Entity.PIN)
        if pin.count > 0 {
            OnboardUser.pin = pin[0]["aPIN"] ?? ""
        }
        model.delegate = self
        model.getCUP()
    }
    
    func reloadDataForCUP(service: String) {
        
        if let response = model.jsonData { // If status code will be 200 then json data will be nil
            self.showMessage(title: "", message: response["STATUS_MESSAGE"].stringValue)
        }
        else {
            showCaption()
            pagerView.numberOfPages = model.enquiryAccountsInfo?.count ?? 0
            collectionView.reloadData()
        }
    }
}

//Call API BE
extension HomeViewController : BEDelegate {
    func dataMiniStatements(data: JSON) {
    }
    
    
    
    func fetchBalanceEnquiryFor(accountAlise : String) {
        modelBE.delegate = self
        //modelBE.homeVC = self
        modelBE.getBE(accountAlise: accountAlise, totelcharge: "")
    }
    
    func showBalanceFor(accountAlise: String, message: String, code : Int) {
        if OnboardUser.beArray.count > 0 {
            collectionView.reloadData()
        }
        else {
            self.showMessage(title: "", message: message)
        }
    }
}

//Call API BE
extension HomeViewController : FetchBillDelegate {
    
    func fetchBill() {
        modelFetchBill.delegate = self
        modelFetchBill.getBills()
    }
    
    func fetchedBill(message: String) {
        if modelFetchBill.responseData?.statusCode == 200 {
            if (modelFetchBill.responseData?.fetchedBill.count ?? 0) == 0 {
                headerTitle = ["PROMO", "OFFER"]
            }
            else if (modelFetchBill.responseData?.fetchedBill.count ?? 0) > 0 {
                if !headerTitle.contains("UPCOMINGBILL") {
                    headerTitle = ["UPCOMINGBILL", "PROMO", "OFFER"]
                }
            }
            tblView.reloadData()
        }
        else {
            self.showMessage(title: "", message: modelFetchBill.responseData?.statusMessage ?? "")
        }
    }
}


extension HomeViewController:AuthDelegate {
    func validate() {
        //Check if user enable biometric access
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            let context = LAContext()
            Authntication(context).authnticateUser { (success, message) in
                DispatchQueue.main.async {
                    if success == true {
                        global.enterpin = OnboardUser.pin
                        self.validAuth()
                    }
                    else {
                        if message != "" {
                            self.showMessage(title: "", message: message)
                        }
                    }
                    
                }
            }
        }
        else {
             global.enterpin = ""
            let vc = AuthoriseViewController.getVCInstance() as! AuthoriseViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func validAuth() {
        checkBEData()
    }
    
    func checkBEData() {
        if let data = OnboardUser.beArray.first(where: {$0["ACCOUNT_ALIAS"].stringValue == selectedAccountBE}) {
            //Ministatement
            let vc = AccountDetailViewController.getVCInstance() as! AccountDetailViewController
          //  vc.dictAccount = data
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            self.fetchBalanceEnquiryFor(accountAlise: selectedAccountBE)
        }
        
    }
}


extension NSLayoutConstraint {
    /**
     Change multiplier constraint
     
     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
     */
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem!,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
    
   
}
