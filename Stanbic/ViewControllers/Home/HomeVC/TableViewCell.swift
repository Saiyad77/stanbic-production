//
//  TableViewCell.swift
//  Stanbic
//
//  Created by Vijay Patidar on 22/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import UIKit

class HeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Fonts().set(object: lblHeaderTitle, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: "", placeHolder: "")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func initiateValue() {
        
    }

}

class UpcomingBillTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewLeft: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblBillerService: UILabel!
    @IBOutlet weak var lblLastChecked: UILabel!
    @IBOutlet weak var lblBillerName: UILabel!
    @IBOutlet weak var lblAcNo: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var btnPay: UIButton!
    @IBOutlet weak var cardView: UIViewPropertys!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Fonts().set(object: lblBillerService, fontType: 1, fontSize: 12, color: Color.black, title: "KPLC  Postpaid", placeHolder: "")
        Fonts().set(object: lblAmount, fontType: 1, fontSize: 12, color: Color.black, title: "KSH 1, 742.86", placeHolder: "")
        Fonts().set(object: lblBillerName, fontType: 1, fontSize: 12, color: Color.black, title: "James Macharia", placeHolder: "")
        Fonts().set(object: lblLastChecked, fontType: 1, fontSize: 8, color: Color.c169_169_169, title: "Last checked 03:19 PM", placeHolder: "")
        Fonts().set(object: lblAcNo, fontType: 1, fontSize: 8, color: Color.c169_169_169, title: "Acc No. 22166241", placeHolder: "")
        Fonts().set(object: lblDueDate, fontType: 1, fontSize: 8, color: Color.c232_0_51, title: "Due today", placeHolder: "")
        Fonts().set(object: btnPay, fontType: 1, fontSize: 12, color: Color.white, title: "Pay", placeHolder: "")
        
        viewContainer.layer.cornerRadius = 4.0
        viewContainer.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.viewContainer.layer.cornerRadius = 4.0
        self.viewContainer.layer.shadowRadius = 0.5
        self.viewContainer.layer.shadowColor = Color.c10_34_64.cgColor
        self.viewContainer.layer.shadowOpacity = 0.16
        self.viewContainer.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.viewContainer.clipsToBounds = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func initiateData(data : FetchBill.Result) {
         
            let dateConverter = DateConverter()
            let lastChecked = dateConverter.convertDate(date_str: data.lastCheck ?? "", formatTo: "dd MMM, yyyy", formatFrom: "yyyy-MM-dd HH:mm:ss")
            let due_date = dateConverter.getDateInterval(toDate: Date(), date_str: data.dueDate ?? "", formatFrom: "yyyy-MM-dd hh:mm:ss")
            let (salutation_str, textColor) = self.getSalutationDescription(salutation: due_date)
            
            lblBillerService.text = data.billerName
            lblLastChecked.text = data.lastCheck
            lblBillerName.text = data.customerName
            lblAcNo.text = "Acc No.\(data.billRef!)"
            let amount = "\(data.amount ?? "0")"
            lblAmount.text = "\(OnboardUser.currency) \(amount.currencyFormatting())"
            lblDueDate.text = data.dueDate
            lblLastChecked.text = "Last checked \(lastChecked)"
            lblDueDate.text = salutation_str
            lblDueDate.textColor = textColor
            viewLeft.backgroundColor = textColor
            imgIcon.sd_setImage(with: URL(string: data.billerLogo ?? ""), placeholderImage: UIImage(named: "placeholderurl"))
        
    }
    
    func getSalutationDescription(salutation : Int) -> (String, UIColor) {
        
        switch salutation {
            
        case 0:
            return ("Bill due today", Color.c232_0_51) //red
            
        case 1:
            return ("Bill due tomorrow", Color.c250_164_49) // yellow
        default:
            return ("Bill due in \(salutation) days", Color.c250_164_49) // yellow
        }
        
    }
    
}

class PromotionTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionViewPromotions: UICollectionView!
    
    var promotionalArray = [[String : String]]()
    var selectedViewController: UIViewController!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionViewPromotions.delegate = self
        collectionViewPromotions.dataSource = self
        
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.PROMOTIONS)).count > 0{
            promotionalArray  = coreData.getDataForEntity(entity: Entity.PROMOTIONS)
        }
        
        collectionViewPromotions.reloadData()
         perform(#selector(callbackToReload), with: nil, afterDelay: 5)
     }
    
    
    @objc func callbackToReload() {
        
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.PROMOTIONS)).count > 0 {
            promotionalArray  = coreData.getDataForEntity(entity: Entity.PROMOTIONS)
        }
         collectionViewPromotions.reloadData()
     }
    
   override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //Mark: Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        if promotionalArray.count != 0 {
            return promotionalArray.count
        } else {
            return 1
        }
     }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let promoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PROMO", for: indexPath) as! PromotionCollectionViewCell
      if promotionalArray.count != 0 {
            DispatchQueue.main.async {
                let promo = self.promotionalArray[indexPath.row]
            promoCell.imgView.sd_setImage(with: URL(string: promo["aIMAGE_URL"]!), placeholderImage: UIImage(named: "placeholderurl"))
              promoCell.cardView.isHidden = true
              promoCell.imgView.isHidden = false
            }
        } else {
            promoCell.cardView.isHidden = false
            promoCell.imgView.isHidden = true
        }
        return promoCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let promo = promotionalArray[indexPath.row]
        let url = promo["aURL"] ?? ""
        let phoneNumber = promo["aCONTACT"] ?? ""
        let urlx = Global.getStringValue(url as AnyObject)
        if !urlx.isEmpty {
            
            if  urlx != "null" {
                let viewController = WebViewController.getVCInstance() as! WebViewController
                let navigator = selectedViewController.navigationController
                viewController.url = url
                navigator?.pushViewController(viewController, animated: true)
            } else {
                if !phoneNumber.isEmpty {
                    if phoneNumber != "null" {
                        Global.callNumber(phoneNumber: phoneNumber)
                    }
                    
                }
            }
        } else if !phoneNumber.isEmpty {
                 if phoneNumber != "null" {
            Global.callNumber(phoneNumber: phoneNumber)
            }
            
        } else {
            
        }
    }
    
    //Mark:- Flowlayout Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width - 30, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let edge = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        return edge
    }
}





class OfferTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionViewOffer: UICollectionView!
    
    var offerArray = [[String : String]]()
    var selectedViewController: UIViewController!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionViewOffer.delegate = self
        collectionViewOffer.dataSource = self
        
        let coreData = CoreDataHelper()
        
        if (coreData.getDataForEntity(entity: Entity.WHAT_WE_OFFER)).count > 0{
            offerArray  = coreData.getDataForEntity(entity: Entity.WHAT_WE_OFFER)
        }
        
        collectionViewOffer.reloadData()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    //Mark: -Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        
        if offerArray.count != 0 {
            return offerArray.count
        } else {
            return 1
        }
      }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let offerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "OFFER", for: indexPath) as! OfferCollectionViewCell
  
        let promo = offerArray[indexPath.row]
        DispatchQueue.main.async {
    
            
            offerCell.imgView.sd_setImage(with: URL(string: promo["aIMAGE_URL"]!), placeholderImage: UIImage(named: "placeholderurl"))
        }
        return offerCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let promo = offerArray[indexPath.row]
        let url = promo["aURL"] ?? ""
        let phoneNumber = Global.getStringValue(promo["aCONTACT"] as AnyObject)
        
        let urlx = Global.getStringValue(url as AnyObject)
        if !urlx.isEmpty {
            
            if  urlx != "null" {
                let viewController = WebViewController.getVCInstance() as! WebViewController
                let navigator = selectedViewController.navigationController
                viewController.url = url
                navigator?.pushViewController(viewController, animated: true)
            } else {
                if !phoneNumber.isEmpty {
                    if phoneNumber != "null" {
                    Global.callNumber(phoneNumber: phoneNumber)
                    }
                }
                  
                }
            
        } else if !phoneNumber.isEmpty {
                 if phoneNumber != "null" {
            Global.callNumber(phoneNumber: phoneNumber)
            }
        } else {
            
        }
     }
    
    //Mark:- Flowlayout Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width / 1.8) - 60, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let edge = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        return edge
    }
}

//"https://www.stanbicbank.co.ke/standimg//Banners%20V8/Kenya%20Banners/pamoja%20trader%20business%20account.jpg"
extension String {
    // formatting text for currency textField
    func currencyFormatting() -> String {
        if let value = Double(self) {
            let formatter = NumberFormatter()
         formatter.numberStyle = .currency
            formatter.currencySymbol = ""
            formatter.maximumFractionDigits = 2
            if let str = formatter.string(for: value) {
                return str
            }
//            let formatter = NumberFormatter()
//            formatter.numberStyle = NumberFormatter.Style.currency
//            formatter.currencySymbol = ""
//            formatter.locale = Locale(identifier: "en_US") // set only if necessary
//
//            return formatter.string(for: value)!
        }
        return ""
    }
}
