//
//  CustomHeaderView.swift
//  Stanbic
//
//  Created by Vijay Patidar on 21/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit

class CustomHeaderView: UIView {
    
    //MARK:- Variables
    //MARK: Constants

    //MARK: Variables
    var imageView:UIImageView!
    var colorView:UIView!
    var bgColor = UIColor.init(red: 31/255.0, green: 89/255.0, blue: 216/255.0, alpha: 1.0)
    var titleLabel = UILabel()
    var articleIcon:UIImageView!
    var alphaV = CGFloat(0.1)
    
    
//    //MARK:- Constructor
//    init(frame:CGRect, title: String) {
//        
//        //self.titleLabel.text = title.uppercased()
//        super.init(frame: frame)
//        //setUpView()
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        
//        fatalError("init(coder:) has not been implemented")
//        
//    }
    
    
    
    //MARK:- Private methods
    private func setUpView() {
        backgroundColor = UIColor.white
        
        imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = UIColor.red
        addSubview(imageView)
        
        colorView = UIView()
        colorView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(colorView)
        
        let constraints:[NSLayoutConstraint] = [
            imageView.topAnchor.constraint(equalTo: self.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            colorView.topAnchor.constraint(equalTo: self.topAnchor),
            colorView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            colorView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            colorView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
        
        
        imageView.image = UIImage(named: "home_bg")
        imageView.contentMode = .scaleAspectFill
        
        colorView.backgroundColor = bgColor
        colorView.alpha = alphaV
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(titleLabel)
        let titlesConstraints:[NSLayoutConstraint] = [
            titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 28),
            ]
        NSLayoutConstraint.activate(titlesConstraints)
        
        titleLabel.font = UIFont.systemFont(ofSize: 15)
        titleLabel.textAlignment = .center
        
        articleIcon = UIImageView()
        articleIcon.translatesAutoresizingMaskIntoConstraints = false
        addSubview(articleIcon)
        let imageConstraints:[NSLayoutConstraint] = [
            articleIcon.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            articleIcon.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 6),
            articleIcon.widthAnchor.constraint(equalToConstant: 40),
            articleIcon.heightAnchor.constraint(equalToConstant: 40)
        ]
        
        NSLayoutConstraint.activate(imageConstraints)
        articleIcon.image = UIImage(named: "1.0")
    }
    
    
    //MARK:- Public methods
    func decrementColorAlpha(offset: CGFloat) {
        
        if self.colorView.alpha <= 1 {
            
            let alphaOffset = (offset/500)/85
            self.colorView.alpha += alphaOffset
            
        }
    }
    
//    func decrementArticleAlpha(offset: CGFloat) {
//
//        if self.articleIcon.alpha >= 0 {
//
//            let alphaOffset = max((offset - 65)/85.0, 0)
//            self.articleIcon.alpha = alphaOffset
//
//        }
//
//    }
//
    func incrementColorAlpha(offset: CGFloat) {
        
        if self.colorView.alpha >= alphaV {
            
            let alphaOffset = (offset/200)/85
            self.colorView.alpha -= alphaOffset
            
        }
        
    }
    
//    func incrementArticleAlpha(offset: CGFloat) {
//        
//        if self.articleIcon.alpha <= 1 {
//            
//            let alphaOffset = max((offset - 65)/85, 0)
//            self.articleIcon.alpha = alphaOffset
//            
//        }
//        
//    }
    
}
