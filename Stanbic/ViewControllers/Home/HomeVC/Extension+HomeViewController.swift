//
//  Extension+HomeViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 23/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit

extension HomeViewController {
    
    func showActionSheetForProfilePicture() {
        let appLanguage = Session.sharedInstance.appLanguage
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //makeAttributed(title: LabelField.actionAlwaysDisplayBalance)
        alert.view.tintColor = Color.c0_179_255
        
        let action1 = UIAlertAction(title: "TAKENEWPHOTO".localized(appLanguage as String), style: .default, handler: { (UIAlertAction)in
            self.takePhotoFromCamera()
        })
        
        let action2 = UIAlertAction(title: "CHOOSEEXISTING", style: .default, handler: { (UIAlertAction)in
            self.takePhotoFromLibrary()
        })
        
        let action3 = UIAlertAction(title: "CANCEL", style: .cancel, handler: { (UIAlertAction)in
            
        })
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        
        self.present(alert, animated: true, completion: {
         
        })
        
        guard let label1 = (action1.value(forKey: "__representer")as? NSObject)?.value(forKey: "label") as? UILabel else { return }
        label1.attributedText = makeAttributed(title: "TAKENEWPHOTO".localized(appLanguage as String))
        
        guard let label2 = (action2.value(forKey: "__representer")as? NSObject)?.value(forKey: "label") as? UILabel else { return }
        label2.attributedText = makeAttributed(title: "CHOOSEEXISTING".localized(appLanguage as String))
        
        guard let label3 = (action3.value(forKey: "__representer")as? NSObject)?.value(forKey: "label") as? UILabel else { return }
        label3.attributedText = makeAttributed(title: "CANCEL".localized(appLanguage as String))
    }
    
    func takePhotoFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            //self.myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            myPickerController.modalPresentationStyle = .overCurrentContext
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func takePhotoFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            //self.myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            myPickerController.modalPresentationStyle = .overCurrentContext
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func makeAttributed(title : String) -> NSMutableAttributedString {
        let attributedText = NSMutableAttributedString(string: title)
        
        let range = NSRange(location: 0, length: attributedText.length)
        attributedText.addAttribute(NSAttributedString.Key.kern, value: 1.5, range: range)
        attributedText.addAttribute(NSAttributedString.Key.font, value: UIFont(name: Fonts.kFontMedium, size: 19.0)!, range: range)
        return attributedText
    }
    
    //MARK: - Private methods
    func setUpHeader() {
        
        if self.view.frame.size.height * 0.75 > 500 {
            headerHeight = 500
        }
        else {
            headerHeight = self.view.frame.size.height * 0.75
        }
        tblView.contentInset = UIEdgeInsets(top: headerHeight!, left: 0, bottom: 0, right: 0)
        self.headerHeightConstraint.constant = headerHeight!
        viewHeader.colorView = viewAlpha
        viewHeader.imageView = imgHeader
    }
    
    func animateHeader() {
        
        self.headerHeightConstraint.constant = headerHeight!
        self.collViewBottomConstraint.constant = 22
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: [.curveEaseInOut], animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        
     }
    
       //MARK: UITableViewDelegate implementation
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //self.view.endEditing(true)
        if self.collectionView == scrollView as? UICollectionView {
            let cardCellWidth = collectionView.frame.size.width * 0.85
            let indx = Int(scrollView.contentOffset.x + 115.0) / Int(cardCellWidth)
            pagerView.currentPage = indx
        }
        else {
            let offset = scrollView.contentOffset.y
            let scale = min(max(1.0 - offset / 2000.0, 0.0), 2.0)
            
            lblHeader.transform = CGAffineTransform(scaleX: scale, y: scale)
            imgUser.transform = CGAffineTransform(scaleX: scale, y: scale)
        
            if scrollView.contentOffset.y < 0 {
                
                self.viewTopBar.isHidden = true
                self.viewHeader.backgroundColor = Color.white
                self.headerHeightConstraint.constant += abs(scrollView.contentOffset.y)
                self.collViewBottomConstraint.constant -= abs(scrollView.contentOffset.y * 0.2)
                
                viewHeader.incrementColorAlpha(offset: self.headerHeightConstraint.constant)
                //headerView.incrementArticleAlpha(offset: self.headerHeightConstraint.constant)
            }
            else if scrollView.contentOffset.y > 0 && self.headerHeightConstraint.constant >= headerTileHeight {
                
                self.headerHeightConstraint.constant -= scrollView.contentOffset.y / 10
                self.collViewBottomConstraint.constant += (scrollView.contentOffset.y / 10) * 0.2
                
                viewHeader.decrementColorAlpha(offset: scrollView.contentOffset.y)
                // headerView.decrementArticleAlpha(offset: self.headerHeightConstraint.constant)
                
                if self.headerHeightConstraint.constant < headerTileHeight + 50 {
                    self.headerHeightConstraint.constant = headerTileHeight
                    self.collViewBottomConstraint.constant = 110
                    self.viewTopBar.isHidden = false
                    self.viewHeader.backgroundColor = Color.c28_80_194
                }
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if self.headerHeightConstraint.constant > headerHeight! {
            animateHeader()
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.headerHeightConstraint.constant > headerHeight! {
            animateHeader()
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if scrollView == tblView {
            return
        }
        
        let pageWidth = (collectionView.frame.size.width * 0.85) + 17// width + space
        let currentOffset = scrollView.contentOffset.x
        let targetOffset = targetContentOffset.pointee.x
        var newTargetOffset : CGFloat = 0
        if targetOffset > currentOffset {
            newTargetOffset = CGFloat(ceilf(Float(currentOffset / pageWidth))) * pageWidth
        } else {
            newTargetOffset = CGFloat(floorf(Float(currentOffset / pageWidth))) * pageWidth
        }
        
        if newTargetOffset < 0 {
            newTargetOffset = 0
        } else if CGFloat(newTargetOffset) > scrollView.contentSize.width {
            newTargetOffset = scrollView.contentSize.width
        }
        
        targetContentOffset.pointee.x = currentOffset
        scrollView.setContentOffset(CGPoint(x: CGFloat(newTargetOffset), y: 0), animated: true)
        
        var index: Int = Int(newTargetOffset / pageWidth)
        
        if index == 0 {
            // If first index
            var cell: UICollectionViewCell? = collectionView.cellForItem(at: IndexPath(item: index, section: 0))
            
            UIView.animate(withDuration: animationSpeed, animations: {
                cell?.transform = .identity
            })
            cell = collectionView.cellForItem(at: IndexPath(item: index + 1, section: 0))
            UIView.animate(withDuration: animationSpeed, animations: {
                cell?.transform = self.transformCellValue
            })
        } else {
            var cell: UICollectionViewCell? = collectionView.cellForItem(at: IndexPath(item: index, section: 0))
            UIView.animate(withDuration: animationSpeed, animations: {
                cell?.transform = .identity
            })
            
            index -= 1 // left
            cell = collectionView.cellForItem(at: IndexPath(item: index, section: 0))
            UIView.animate(withDuration: animationSpeed, animations: {
                cell?.transform = self.transformCellValue
            })
            
            index += 1
            index += 1 // right
            cell = collectionView.cellForItem(at: IndexPath(item: index, section: 0))
            UIView.animate(withDuration: animationSpeed, animations: {
                cell?.transform = self.transformCellValue
            })
        }
    }
}

extension HomeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imgUser.image = image
            imgUserimage.image = image
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension HomeViewController {
    
    func showCaption() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        let time = dateFormatter.string(from: Date())
        let timeStr = time.replacingOccurrences(of: ":", with: ".")
        let hourMinute = Float(timeStr)
        lblHeader.text = getMsg(hourMinute!)
    }
    
    func getMsg(_ hour: Float) -> String{
        
        if (model.profileInfo?.count ?? 0) > 0 {
            let userProfile = model.profileInfo?[0]
            
            if hour >= 0 && hour <= 11.30 {
                return "Good Morning,\n\(userProfile?["aFIRST_NAME"] ?? "")!"
            } else if hour > 11.30 && hour <= 16.30 {
                return "Good Afternoon,\n\(userProfile?["aFIRST_NAME"] ?? "")!"
            } else {
                return "Good Evening,\n\(userProfile?["aFIRST_NAME"] ?? "")!"
            }
        }
        else {
            if hour >= 0 && hour <= 11.30 {
                return "Good Morning,"
            } else if hour > 11.30 && hour <= 16.30 {
                return "Good Afternoon,"
            } else {
                return "Good Evening,"
            }
        }
    }
}
