//
//  TabbarViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 30/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class TabbarViewController: UITabBarController {

   // var tabBarIteam = UITabBarItem()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // selected tab background color
//        let numberOfItems = CGFloat(tabBar.items!.count)
//        let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: tabBar.frame.height)
//
//
//        tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1) , size: tabBarItemSize)
//
//        // initaial tab bar index
//        self.selectedIndex = 0
              delegate = self
        
    }
    
    static func getVCInstance() -> UITabBarController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Home.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))") as! UITabBarController
    }

    
    func getImageWithColorPosition(color: UIColor, size: CGSize, lineSize: CGSize) -> UIImage {
        
        let rect = CGRect(x:0, y: -10, width: size.width, height: size.height)
        // let rectLine = CGRect(x:0, y:size.height-lineSize.height,width: lineSize.width,height: lineSize.height)
        let rectLine = CGRect(x:25, y:-1,width: lineSize.width - 55,height: lineSize.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.clear.setFill()
        UIRectFill(rect)
        color.setFill()
        UIRectFill(rectLine)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension TabbarViewController: UITabBarControllerDelegate  {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        guard let fromView = viewController.view, let toView = viewController.view else {
            return false // Make sure you want this as false
        }
        
        if fromView != toView {
            UIView.transition(from: fromView, to: toView, duration: 0.3, options: [.transitionCrossDissolve], completion: nil)
        }
        
        return true
    }
}
//extension UIImage {
//    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
//        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
//        UIGraphicsBeginImageContextWithOptions(size, false, 0)
//        color.setFill()
//        UIRectFill(rect)
//        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
//        UIGraphicsEndImageContext()
//        return image
//}
//}
