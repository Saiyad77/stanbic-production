//
//  ContactTableViewCell.swift
//  dtb
//
//  Created by Sumit5Exceptions on 07/09/18.
//  Copyright © 2018 5Exceptions. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameLetters:UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    @IBOutlet weak var imgContact: UIImageView!
    
    func roundLabel()
    {
        nameLetters.layer.cornerRadius = nameLetters.frame.width/2
        nameLetters.layer.masksToBounds = true
    }
    
    func roundImage()
    {
        imgContact.layer.cornerRadius = imgContact.frame.width/2
        imgContact.layer.masksToBounds = true
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
