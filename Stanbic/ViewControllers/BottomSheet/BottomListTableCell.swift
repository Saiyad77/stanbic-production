//
//  BottomListTableCell.swift
//  Stanbic
//
//  Created by Admin on 25/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class BottomListTableCell: UITableViewCell {

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    
    @IBOutlet weak var imgSelection: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        baseView.layer.borderWidth = 1
//        baseView.layer.borderColor = Color.c237_237_237.cgColor
        
//        imgSelection.layer.borderWidth = 1
//        imgSelection.layer.borderColor = Color.c237_237_237.cgColor
        
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 15, color: Color.c52_58_64, title: "", placeHolder: "")
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
