//
//  BottomListVC.swift
//  Stanbic
//
//  Created by Admin on 25/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

protocol BottomListDelegate: class {
    func getIndx(indx: Int)
}

protocol BottomListForMultiSelectionDelegate: class {
    func getIndxList(indxs: [Int])
}

class BottomListVC: UIViewController {

    enum CellString: String{
        case imgCell = "ImgCell"        // image with title only
        case imgSubtitleCell = "ImgSubtitleCell"    // Title, subtile with image
        case titleCell = "TitleCell"    // Title and subtitle only
        case titleOnlyCell = "TitleOnlyCell" // Title only
    }
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgViewHandle: UIImageView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    
    var dictData : [String : Any]?
    
    var selectedIndxList = [Int]()
    var cellIdentifier = CellString.imgCell
    
    weak var delegate: BottomListDelegate!
    weak var multiDelegate: BottomListForMultiSelectionDelegate!
    
    var isAllowMultipleSelection = false
    var titleString: String!
    var titleList: [String]!
    var subTitleList: [String]!
    var imgList: [UIImage]!
    var imgUrl: [String]!
      var Selectdefault = ""
    var selectedIndx = -1
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = .clear
        imgViewHandle.layer.cornerRadius = 3.5
        
        
        if let headerTitle = (dictData?["headerTitle"] as? String) {
            Fonts().set(object: lblTitle, fontType: 1, fontSize: 13, color: Color.c134_142_150, title: headerTitle, placeHolder: "")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isAllowMultipleSelection{
            tableView.allowsMultipleSelection = true
            btnClose.setTitle("Done", for: .normal)
            btnClose.setTitleColor(UIColor.black, for: .normal)
            btnClose.setImage(nil, for: .normal)
        }
        
        let cellType = (dictData?["cellType"] as? Int) ?? 0
        guard let titleArray = dictData?["titleArray"] as? [String] else { return }
        
        switch cellType {
            
        case 0: // Title only cell
            cellIdentifier = CellString.titleOnlyCell
            
        case 1:// Title and subtitle cell
            cellIdentifier = CellString.titleCell
            
        case 2: // image with title cell
            cellIdentifier = CellString.imgCell

        case 3: // image title subtitle cell
            cellIdentifier = CellString.imgSubtitleCell
        default:
            cellIdentifier = CellString.titleOnlyCell
        }
        tableHeightConstraint.constant = CGFloat(max(titleArray.count,2) * 70)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewHeader.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        super.viewDidAppear(animated)
        view.isOpaque = false
        UIView.animate(withDuration: 0.1) {
            self.view.backgroundColor = Color.c11_15_42.withAlphaComponent(0.5)
        }
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Home.identifier(), bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }

    @IBAction func backGoroundTapped(_ sender: UIButton) {
        if isAllowMultipleSelection{
            if selectedIndxList.count > 0{
                multiDelegate.getIndxList(indxs: self.selectedIndxList)
            }
            self.view.backgroundColor = .clear
            self.dismiss(animated: true, completion: nil)
            
        }else{
            
            self.view.backgroundColor = .clear
            self.dismiss(animated: true, completion: nil)
        }
        
    }
}

extension BottomListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let titleArray = dictData?["titleArray"] as? [String] {
             return titleArray.count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier.rawValue, for: indexPath) as! BottomListTableCell
        
        //Fill title
        if let titleArray = dictData?["titleArray"] as? [String] {
            cell.lblTitle.text = titleArray[indexPath.row]
        }
        //Fill Subtitle title
        if let subTitleArray = dictData?["subTitleArray"] as? [String] {
            cell.lblSubtitle.text = subTitleArray[indexPath.row]
        }
        // Fill Image
        let cellType = (dictData?["cellType"] as? Int) ?? 0
        if cellType == 2 || cellType == 3 {
            if let image = getImage(title: cell.lblTitle.text ?? "") {
                cell.imgIcon.image = image
            }
        }
        //Checkmark
        if selectedIndxList.contains(indexPath.row) || Selectdefault == cell.lblTitle.text ?? "0"   {
            cell.imgSelection.image = #imageLiteral(resourceName: "check")
            Selectdefault = ""
        }
        else {
            cell.imgSelection.image = #imageLiteral(resourceName: "uncheck")
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! BottomListTableCell
        
        if isAllowMultipleSelection {
            if selectedIndxList.contains(indexPath.row){
                cell.imgSelection.image = #imageLiteral(resourceName: "uncheck")
                selectedIndxList.remove(at: selectedIndxList.indexes(of: indexPath.row)[0])
                
            }else{
                selectedIndxList.append(indexPath.row)
                cell.imgSelection.image = #imageLiteral(resourceName: "check")
            }
        }
        else {
            cell.imgSelection.image = #imageLiteral(resourceName: "check")
            selectedIndxList[0] = indexPath.row
            delegate.getIndx(indx: indexPath.row)
            view.backgroundColor = .clear
            view = nil
            dismiss(animated: true, completion: nil)
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func getImage(title : String) -> UIImage? {
        switch title {
        case "AIRTELMONEY", "Airtel":
            return UIImage.init(named: "airtel")!
            
        case "MPESA", "Safaricom":
            return UIImage.init(named: "safaricom")!
            
        case "telkom":
            return UIImage.init(named: "telkom")!
            
        default:
            return nil
        }
    }

}

extension Array where Element: Equatable {
    func indexes(of element: Element) -> [Int] {
        return self.enumerated().filter({ element == $0.element }).map({ $0.offset })
    }
}
