//
//  BottomSheetAccountTableViewCell.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 23/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class BottomSheetAccountTableViewCell: UITableViewCell {

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imgCheckmark: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 13, color: Color.black, title: "Currentbalance", placeHolder: "")
        Fonts().set(object: lblSubtitle, fontType: 1, fontSize: 13, color: Color.c160_166_173, title: "**** **** **** 1234", placeHolder: "")
        
        
        imgCheckmark.layer.cornerRadius = imgCheckmark.frame.size.width / 2.0
        imgIcon.layer.cornerRadius = imgIcon.frame.size.width / 3.0
    }
    
    func setdata(acname:String,acnumber:String) {
        lblTitle.text = acname
        lblSubtitle.text = acnumber.hashingOfTheAccount(noStr: acnumber)
     }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
