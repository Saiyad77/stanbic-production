//
//  EnterPinChangeBiometricVC.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 24/07/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class EnterPinChangeBiometricVC: UIViewController {

    //Mark:- IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //Mark:- Varibales
    var modelVerifyAuthKey : VerifyActivationkeyVM?
    var textField:UITextField!
    var txtField : UITextField? = nil
    var arrImgView = [UIImageView]()
    var arrLabels = [UILabel]()
    var flow = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        //  generatingOTPEnteringView()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //    override var preferredStatusBarStyle: UIStatusBarStyle {
    //        return .default
    //    }
    
    override func viewDidAppear(_ animated: Bool) {
        generatingOTPEnteringView()
        textField.becomeFirstResponder()
        
    }
    // MARK:_SET FONT
    func setFonts() {
        if !isInternetAvailable(){
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        Fonts().set(object: lblTitle, fontType: 0, fontSize: 20, color: Color.white, title: "WELCOMEBACK", placeHolder: "")
        lblTitle.text = lblTitle.text?.replacingOccurrences(of: "NAME", with: modelVerifyAuthKey?.responseData?.resultArray?.name ?? "")
        Fonts().set(object: btnReset, fontType: 1, fontSize: 15, color: Color.white, title: "", placeHolder: "")
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    //Mark:- Button Actions
    @IBAction func reset(_ sender: UIButton) {
        
        
    }
    
}



extension EnterPinChangeBiometricVC {
    func generatingOTPEnteringView() {
        
        let lblWidth = 15
        let padding = 20
        let dots = 4
        let yOri = (lblTitle.frame.size.height + lblTitle.frame.origin.y)
        let possition = ((btnReset.frame.origin.y) - (lblTitle.frame.size.height + lblTitle.frame.origin.y)) / 2
        var xOri = Int(self.view.frame.size.width / 2) - ((lblWidth * dots) + (padding * (dots - 1))) / 2
        let frm1 = CGRect(x: xOri, y: Int(possition + yOri), width: (25 * dots), height: 15)
        txtField = UITextField.init(frame: frm1)
        txtField?.backgroundColor = UIColor.clear
        txtField?.keyboardType = .numberPad
        txtField?.autocorrectionType = .yes
        txtField?.delegate = self
        txtField?.becomeFirstResponder()
        txtField?.tintColor = UIColor.clear
        txtField?.textColor = UIColor.clear
        txtField?.keyboardAppearance = .dark
        //        if #available(iOS 12.0, *) {
        //            txtField?.textContentType = .oneTimeCode
        //        }
        self.view.addSubview(txtField!)
        
        for _ in 0 ..< dots {
            
            var frm = CGRect(x: xOri, y: Int(possition + yOri), width: lblWidth, height: 15)
            let imgView = UIImageView.init(frame: frm)
            imgView.backgroundColor = Color.c10_34_64
            imgView.alpha = 0.8
            imgView.layer.cornerRadius = imgView.frame.size.height / 2
            self.view.addSubview(imgView)
            arrImgView.append(imgView)
            
            frm = CGRect(x: xOri, y: Int(possition + yOri), width: lblWidth, height: 15)
            let lbl = UILabel.init(frame: frm)
            Fonts().set(object: lbl, fontType: 3, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
            lbl.textAlignment = .center
            self.view.addSubview(lbl)
            lbl.textColor = UIColor.clear // Change color for show text
            self.arrLabels.append(lbl)
            // Update x Origin
            xOri = Int(CGFloat(xOri) + frm.size.width + CGFloat(padding))
        }
        
        // txtField?.text = otp
        
    }
}


extension EnterPinChangeBiometricVC : UITextFieldDelegate {
    
    // Mark : Text Field Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.textField = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            
            //IF 6 DIGITS COMPLETE THEN UTO CALL OTP VERIFICATION API
            if (textField.text?.count)! >= 6 && string != "" {
                
                return false
            }
            
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            for i in 0 ..< updatedText.count {
                
                if arrImgView.count >= updatedText.count {
                    //Change imgView's color
                    let imgView = arrImgView[i]
                    imgView.backgroundColor = Color.white
                    let chars = Array(updatedText)
                    arrLabels[i].text = "\(chars[i])"
                }
                else {
                    return false
                }
            }
            
            for i in updatedText.count ..< arrImgView.count {
                
                if arrImgView.count >= updatedText.count {
                    //Change imgView's color
                    let imgView = arrImgView[i]
                    imgView.backgroundColor = Color.c10_34_64
                    arrLabels[i].text = ""
                }
            }
            
            if updatedText.count >= arrImgView.count {
                txtField?.text = updatedText
                validatePIN()
                return true
            }
        }
        let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
    
    func gotoNextVC() {
        let vc = EnableTouchIDViewController.getVCInstance()
        
        if flow == "BIO"{
         self.show(vc, sender: nil)
    } else {
              self.navigationController?.pushViewController(vc, animated: true)
        }
}
}



extension EnterPinChangeBiometricVC {
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let activationKey: Int?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case activationKey = "ACTIVATION_KEY"
        }
    }
    
    func validatePIN() {
        if !isInternetAvailable(){
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        self.view.endEditing(true)
        
        let mobileArr = CoreDataHelper().getDataForEntity(entity: Entity.MSISDN)
        if mobileArr.count > 0 && OnboardUser.mobileNo == "" {
            OnboardUser.mobileNo = mobileArr[0]["aMSISDN"] ?? ""
        }
        
        
        RestAPIManager.validatePIN(title: "PLEASEWAIT", subTitle: "VALACTIPIN", pin: textField.text!, type: Response.self, mobileNo: OnboardUser.mobileNo, completion: { (response) in
            DispatchQueue.main.async {
                if response.statusCode == 200 {
                    self.saveData()
                }
                else {
                    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                }
            }
        }) { (error) in
            
        }
    }
    
    func saveData() {
        
        CoreDataHelper().saveUUID(value: Secure().encryptPIN(text : (txtField?.text)!), entityName: Entity.PIN.rawValue, key: Entity.PIN.rawValue, removeRecords: true)
        self.gotoNextVC()
    }
}
