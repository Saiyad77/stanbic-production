//
//  EnterPinCollectionViewCell.swift
//  Stanbic
//
//  Created by Vijay Patidar on 17/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class EnterPinCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblText: UILabel!
    
    override func awakeFromNib() {
        Fonts().set(object: lblText, fontType: 1, fontSize: 28, color: Color.white, title: "", placeHolder: "")
        viewContainer.layer.cornerRadius = viewContainer.frame.size.height / 2
        viewContainer.clipsToBounds = true
    }
    
    func intiateValue(text : String) {
        lblText.text = text
    }
}
