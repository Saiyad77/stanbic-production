//
//  VerifyActivationKeyVC.swift
//  Stanbic
//
//  Created by Vijay Patidar on 17/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

@available(iOS 12.0, *)
class VerifyActivationKeyVC: UIViewController {
    
    //Model
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let activationKey: Int?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case activationKey = "ACTIVATION_KEY"
        }
//
//        {
//        "STATUS_CODE": 200,
//        "STATUS_MESSAGE": "Successfully requested for an activation key.",
//        "ACTIVATION_KEY": 111111
//        }
    }
    
    //Mark:- IBOUTLET
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblResendOtp: UILabel!
    @IBOutlet weak var txtVerificationCode: UITextField!
    @IBOutlet weak var btnResendOTP: AttributedButton!
    @IBOutlet weak var btnContinue: AttributedButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    
    //Mark:- Variables
    var count = 10
    var timer : Timer?
    var activationKey : Int = 0
    let model = VerifyActivationkeyVM()
    var textField:UITextField!
    var txtField : UITextField? = nil
    var arrImgView = [UIImageView]()
    var arrLabels = [UILabel]()
    let appLanguage = Session.sharedInstance.appLanguage

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setFonts()
        //generatingOTPEnteringView()
        getActivationKey()
        
       // txtVerificationCode.textContentType = .oneTimeCode
         txtField?.textContentType = .oneTimeCode
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if txtField != nil {
            self.updateOTPEnteringView()
        }
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
        // MARK:_SET FONT
    func setFonts() {
        let appLanguage = Session.sharedInstance.appLanguage
        let lblTitleStr = "VERIFYMOB".localized(appLanguage) + OnboardUser.mobileNo
      //  lblTitleStr.setLineSpacing(lineSpacing: 4.0)
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 17, color: Color.white, title: lblTitleStr, placeHolder: "")
        Fonts().set(object: lblSubTitle, fontType: 3, fontSize: 15, color: Color.white, title: "DECTECTAUTO", placeHolder: "")
        Fonts().set(object: lblResendOtp, fontType: 0, fontSize: 15, color: Color.white, title: "RESENDCODEIN", placeHolder: "")
        Fonts().set(object: btnResendOTP, fontType: 1, fontSize: 14, color: Color.white, title: "RESENDCODE", placeHolder: "")
        Fonts().set(object: btnContinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
       // Fonts().set(object: txtVerificationCode, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "ENTERVERIFICATION")
        
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resend(_ sender: AttributedButton) {
        getActivationKey()
        self.view.endEditing(true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        self.view.endEditing(true)
        validateActivationKey()
    }
}

//Call API
@available(iOS 12.0, *)
extension VerifyActivationKeyVC : VerifyActivationkeyVMDelegate {
    
    func getActivationKey() {
        
        if !isInternetAvailable() {
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        
        RestAPIManager.get_validation_key(title: "PLEASEWAIT", subTitle: "REQUESTCODE", type: Response.self, mobileNo: OnboardUser.mobileNo, completion: { (response) in
            DispatchQueue.main.async {
                if response.statusCode == 200 {
                // print( "😺 😺 😺 😺 \n😺 😺 😺 😺 😺  \(response)  🎉 🎉 🎉 🎉 🎉 🎉 🎉\n 🎉 🎉 🎉 🎉 🎉 🎉" )
                    self.activationKey = response.activationKey ?? 0
                    self.count = 2
                    self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
                    self.updateOTPEnteringView()
                }
                else {
                    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
                }
            }
        }) { (error) in
         
        }
    }
    
    func validateActivationKey() {
        self.view.endEditing(true)
        model.delegate = self
        model.validateActivationKeyAPI(activationKey : "\(activationKey)")
    }
    
    func reloadData() {
        let response = model.responseData
        if response!.statusCode == 200 {
            self.gotoNextVC(model: model)
        }
        else {
            self.showMessage(title: "", message: response!.statusMessage ?? "")
        }
    }
    
    func gotoNextVC(model : VerifyActivationkeyVM) {
        
        if !isInternetAvailable(){
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        
        
        if model.responseData?.resultArray?.mbStatus == 0 { // new user
            let vc = WelcomeViewController.getVCInstance()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if model.responseData?.resultArray?.mbStatus == 1 { // existing user
            
            if model.responseData?.resultArray?.pinStatus == "1" {
                let vc = EnterPinViewController.getVCInstance() as! EnterPinViewController
                vc.modelVerifyAuthKey = model
                self.navigationController?.pushViewController(vc, animated: true)

            }
            else if model.responseData?.resultArray?.pinStatus == "2" {
                let vc = EnterYourOTPVC.getVCInstance() as! EnterYourOTPVC
                 //vc.modelVerifyAuthKey = model
                 self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                let nextVc = ErrorVC.getVCInstance() as! ErrorVC
                nextVc.message =  model.responseData?.resultArray?.pinStatusMsg ?? ""
                self.navigationController?.pushViewController(nextVc, animated: true)
             }
          }
    }
}

//Timer
@available(iOS 12.0, *)
extension VerifyActivationKeyVC {
    //MARK:- CUSTOM METHODS
    
    @objc func updateTime() {
        if(count > 0) {
            let title = "RESENDCODEIN".localized(appLanguage as String)
            lblResendOtp.text = "\(title)\(timeString(time: TimeInterval(count)))"
            count = count - 1
            btnResendOTP.alpha = 0.4
            btnResendOTP.isEnabled = false
            lblResendOtp.isHidden = false
        }
        else {
            timer?.invalidate()
            lblResendOtp.isHidden = true
            btnResendOTP.alpha = 1.0
            btnResendOTP.isEnabled = true
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
}


//Text field delegate
@available(iOS 12.0, *)
extension VerifyActivationKeyVC {
    
    func generatingOTPEnteringView() {
        
        let lblWidth = 15
        let padding = 20
        let dots = 6
        let yOri = (lblTitle.frame.size.height + lblTitle.frame.origin.y)
        let possition = CGFloat(100)
        var xOri = Int(self.view.frame.size.width / 2) - ((lblWidth * dots) + (padding * (dots - 1))) / 2
        let frm1 = CGRect(x: xOri, y: Int(possition + yOri), width: (25 * dots), height: 15)
        txtField = UITextField.init(frame: frm1)
        txtField?.backgroundColor = UIColor.clear
        txtField?.keyboardType = .numberPad
        txtField?.autocorrectionType = .yes
        txtField?.delegate = self
        txtField?.tintColor = UIColor.clear
        txtField?.textColor = UIColor.clear
        txtField?.keyboardAppearance = .dark
        //        if #available(iOS 12.0, *) {
        //            txtField?.textContentType = .oneTimeCode
        //        }
      
        self.view.addSubview(txtField!)
        
        for _ in 0 ..< dots {
            
            var frm = CGRect(x: xOri, y: Int(possition + yOri), width: lblWidth, height: 15)
            let imgView = UIImageView.init(frame: frm)
            imgView.backgroundColor = Color.c10_34_64
            imgView.alpha = 0.8
            imgView.layer.cornerRadius = imgView.frame.size.height / 2
            self.view.addSubview(imgView)
            arrImgView.append(imgView)
            
            frm = CGRect(x: xOri, y: Int(possition + yOri), width: lblWidth, height: 15)
            let lbl = UILabel.init(frame: frm)
            Fonts().set(object: lbl, fontType: 3, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
            lbl.textAlignment = .center
            self.view.addSubview(lbl)
            lbl.textColor = UIColor.clear // Change color for show text
            self.arrLabels.append(lbl)
            // Update x Origin
            xOri = Int(CGFloat(xOri) + frm.size.width + CGFloat(padding))
        }
        
        // txtField?.text = otp
        
    }
    
    func updateOTPEnteringView() {
        self.txtField = nil
        for i in 0 ..< arrLabels.count {
            arrLabels[i].removeFromSuperview()
        }
        for i in 0 ..< arrImgView.count {
            arrImgView[i].removeFromSuperview()
        }
        
        arrLabels.removeAll()
        arrImgView.removeAll()
        
        self.generatingOTPEnteringView()
        if textField != nil {
            textField.text = ""
        }
        txtField?.becomeFirstResponder()
    }
}


@available(iOS 12.0, *)
extension VerifyActivationKeyVC : UITextFieldDelegate {
    
    // Mark : Text Field Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.textField = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            
            //IF 6 DIGITS COMPLETE THEN UTO CALL OTP VERIFICATION API
            if (textField.text?.count)! >= 6 && string != "" {
                return false
            }
            
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            for i in 0 ..< updatedText.count {
                
                if arrImgView.count >= updatedText.count {
                    //Change imgView's color
                    let imgView = arrImgView[i]
                    imgView.backgroundColor = Color.white
                    let chars = Array(updatedText)
                    arrLabels[i].text = "\(chars[i])"
                }
                else {
                    return false
                }
            }
            
            for i in updatedText.count ..< arrImgView.count {
                
                if arrImgView.count >= updatedText.count {
                    //Change imgView's color
                    let imgView = arrImgView[i]
                    imgView.backgroundColor = Color.c10_34_64
                    arrLabels[i].text = ""
                }
            }
            
            if updatedText.count >= arrImgView.count {
                txtField?.text = updatedText
                self.activationKey =  Int(txtField?.text ?? "0") ?? 0
                validateActivationKey()
                return true
            }
        }
        let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
}

@available(iOS 12.0, *)
extension VerifyActivationKeyVC {
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}
