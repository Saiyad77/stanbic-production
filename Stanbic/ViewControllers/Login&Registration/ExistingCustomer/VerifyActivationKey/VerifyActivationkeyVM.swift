//
//  VerifyActivationkeyVM.swift
//  Stanbic
//
//  Created by Vijay Patidar on 17/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation

protocol VerifyActivationkeyVMDelegate: class {
    func reloadData()
}

// ViewModel for Country List
class VerifyActivationkeyVM{
    //MARK:- Model for API calling
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let resultArray: Result?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case resultArray = "RESULT_ARRAY"
            
        }
    }
    
    
    struct Result: Codable{
        let mbStatus    : Int?
        let name        : String?
        let profileID   : String?
        let pinStatus   : String?
        let lastLogin   : String?
        let pinStatusMsg: String?
        
        private enum CodingKeys: String, CodingKey {
            case mbStatus = "MB_STATUS"
            case name = "NAME"
            case profileID = "PROFILE_ID"
            case pinStatus = "PIN_STATUS"
            case lastLogin = "LAST_LOGIN"
             case pinStatusMsg = "PIN_STATUS_MESSAGE"
        }
        
    }
    
    
    var responseData : Response?
    weak var delegate : VerifyActivationkeyVMDelegate!
    // Call API for Country List
    func validateActivationKeyAPI(activationKey : String) {
        
        RestAPIManager.verify_validation_key(title: "PLEASEWAIT", subTitle: "VALACTICODE", activationKey: activationKey, type: Response.self, mobileNo: OnboardUser.mobileNo, completion: { (response) in
            DispatchQueue.main.async {
                self.responseData = response
                self.delegate.reloadData()
            }
        }) { (error) in
           
        }
    }
}
