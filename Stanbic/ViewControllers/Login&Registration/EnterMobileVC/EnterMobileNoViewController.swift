//
//  EnterMobileNoViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 17/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

@available(iOS 12.0, *)
@available(iOS 12.0, *)
@available(iOS 12.0, *)
class EnterMobileNoViewController: UIViewController,UITextViewDelegate {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var lblTerms: UILabel!
    @IBOutlet weak var btnCountinue: AttributedButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var superView: GradientView!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var scrollViewObj: UIScrollView!
    
     @IBOutlet weak var txtterms: UITextView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        showTerms_Condition()
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        txtMobileNo.becomeFirstResponder()
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //Mark:- Button Actions

    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @available(iOS 12.0, *)
    @IBAction func countinue(_ sender: AttributedButton) {
                view.endEditing(true)
       if checkValidMobileNo() {
            self.view.endEditing(true)
            gotoNextVC()
        }
    }
    
}

//Mark:- Custom Methods
@available(iOS 12.0, *)
extension EnterMobileNoViewController {
    
    // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 24, color: Color.white, title: "ENTERMOB", placeHolder: "")
        Fonts().set(object: lblSubtitle, fontType: 3, fontSize: 15, color: Color.white, title: "WESENDCODE", placeHolder: "")
        Fonts().set(object: lblCountryCode, fontType: 1, fontSize: 17, color: Color.white, title: "+254", placeHolder: "")
        Fonts().set(object: txtMobileNo, fontType: 1, fontSize: 17, color: Color.white, title: "", placeHolder: "ENTERMOBPLACEHOLDER")
        //Change placeholder color
        let applanguage = Session.sharedInstance.appLanguage
        txtMobileNo.attributedPlaceholder = NSAttributedString(string: "ENTERMOBPLACEHOLDER".localized(applanguage as String), attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(white: 1, alpha: 0.4)])
        
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        
        viewContainer.layer.borderColor = Color.white.cgColor
        viewContainer.layer.borderWidth = 0.3
        viewContainer.layer.cornerRadius = 2.0
    }
    
    func showTerms_Condition() {
        //txtterms.delegate = self
        let applanguage = Session.sharedInstance.appLanguage
        let attributedString = NSMutableAttributedString(string: "TERMSCONDITION".localized(applanguage as String), attributes: [
            .font: UIFont(name: Fonts.kFontRegular, size: 13.0)!,
            .foregroundColor: UIColor(white: 1.0, alpha: 1.0),
            .kern: 0.0
            ])
        attributedString.addAttributes([
            .font: UIFont(name: Fonts.kFontMedium, size: 13.0)!,
            .foregroundColor: Color.c3_150_253
            ], range: NSRange(location: 41, length: 20))
        attributedString.addAttributes([
            .font: UIFont(name: Fonts.kFontMedium, size: 13.0)!,
            .foregroundColor: Color.c3_150_253
            ], range: NSRange(location: 70, length: 15))
        
       
        
        lblTerms.attributedText = attributedString
      
        lblTerms.setLineSpacing(lineSpacing: 2.0)
        lblTerms.addGestureRecognizer(UITapGestureRecognizer(target:self, action: #selector(tapLabel(gesture:))))
        self.lblTerms.isUserInteractionEnabled = true
        
        //self.txtterms.isUserInteractionEnabled = true
        //self.txtterms.isEditable = false
      
    }
    
    //Mark:- Button Actions
    @IBAction func privacy(_ sender: UIButton) {
        getprivacy()
    }
    
    //Mark:- Button Actions
    @IBAction func terms(_ sender: UIButton) {
          getterms()
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        if (URL.absoluteString == global.termsandcounditions) {
              getterms()
        }
        else if(URL.absoluteString == global.privacyandpolicies) {
         getprivacy()
        }
        else {
            
        }
        return false
    }
    
   @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        let text = "By continuing, you agree to Stanbic Bank Terms and Conditions and our Privacy policy."
        
        let termsRange = (text as NSString).range(of: "Terms and Conditions")
        // comment for now
        let privacyRange = (text as NSString).range(of: "Privacy policy.")
    
        if gesture.didTapAttributedTextInLabel(label: lblTerms, inRange: termsRange) {

            getterms()

        } else if gesture.didTapAttributedTextInLabel(label: lblTerms, inRange: privacyRange) {
         
               getprivacy()

        } else {
          
        }
    
    }
    
    func getprivacy() {
        let viewController = WebViewController.getVCInstance() as! WebViewController
        viewController.url = global.privacyandpolicies
        viewController.titleweb = "Privacy policy"
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func getterms() {
        let viewController = WebViewController.getVCInstance() as! WebViewController
        viewController.url = global.termsandcounditions
        viewController.titleweb = "Terms and Conditions"
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func checkValidMobileNo() -> Bool {
        
        if self.txtMobileNo.text?.count == 10 {
            
            let prefix = String((self.txtMobileNo.text?.prefix(2))!)
            if prefix != "07" {
                self.showMessage(title: "", message: "INVALIDMOB")
                return false
            }
            return true
        }
        else if self.txtMobileNo.text?.count == 9 {
            let prefix = String((self.txtMobileNo.text?.prefix(1))!)
            if prefix == "0" {
                self.showMessage(title: "", message: "INVALIDMOB")
                return false
            }
            else if prefix != "7" {
                self.showMessage(title: "", message: "INVALIDMOB2")
                return false
            }
            
            return true
        }
        else {
            self.showMessage(title: "", message: "INVALIDMOB")
            return false
        }
    }
}

@available(iOS 12.0, *)
extension EnterMobileNoViewController : UITextFieldDelegate {
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
        
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

@available(iOS 12.0, *)
extension EnterMobileNoViewController {
    
    func gotoNextVC() {
      
        if (txtMobileNo.text!).first == "0"{
            var mob = txtMobileNo.text!.description
            let mob1 = mob.dropFirst()
            txtMobileNo.text = "\(mob1)"
        }
        
        CoreDataHelper().saveUUID(value: "254" + txtMobileNo.text!, entityName: Entity.MSISDN.rawValue, key: Entity.MSISDN.rawValue, removeRecords: true)
        OnboardUser.mobileNo = "254" + txtMobileNo.text!
        let verifyVC = VerifyActivationKeyVC.getVCInstance()
        self.navigationController?.pushViewController(verifyVC, animated: true)
    }
}


extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        //let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
        //(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        //let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
        // locationOfTouchInLabel.y - textContainerOffset.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
    
    
}
