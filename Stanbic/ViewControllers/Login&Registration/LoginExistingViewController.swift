//
//  LoginExistingViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 21/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication

class LoginExistingViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lblusername: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtpin: UITextField!
    @IBOutlet weak var img_ID: UIImageView!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    // @IBOutlet weak var scrollBottom: NSLayoutConstraint!
    @IBOutlet weak var lblfaceId: UILabel!
    private var lastContentOffset: CGFloat = 0
    @IBOutlet weak var ViewtouchID: UIView!
    @IBOutlet weak var cartoonImageHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnShowHideToggle: UIButton!
    @IBOutlet weak var cartoonImage: UIImageView!
    
    var supportid = ""
    var isFaceIDSupport : Bool?
    var checkOtp: Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        txtpin.keyboardType = .numberPad
        setFonts()
        showCaption()
        checkUserDeviceSupport()
        txtpin.isSecureTextEntry = true
        txtpin.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        txtpin.textColor = Color.black
        
        let pin = CoreDataHelper().getDataForEntity(entity: Entity.PIN)
        if pin.count > 0 {
            OnboardUser.pin = pin[0]["aPIN"] ?? ""
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        txtpin.backgroundColor = UIColor.clear
        
        if UserDefaults.standard.object(forKey: "logout") as? Bool == true {
            
            if UserDefaults.standard.object(forKey: UserDefault.biometric) as? Bool == false {
                showMessage(title: "", message: "\(supportid) not enable")
            } else {
                if  global.showbiometric == false  {
                    validAuth()
                }
            }
            
        }
        global.showbiometric = false
    }
    
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        txtpin.resignFirstResponder()
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 14, color: Color.white, title: "LOGINTOPROCESS", placeHolder: "")
        Fonts().set(object: self.txtpin, fontType: 0, fontSize: 17, color: Color.white, title: "", placeHolder: "PIN")
        Fonts().set(object: self.btnLogin, fontType: 1, fontSize: 14, color: Color.c0_123_255, title: "LOGINEXIST", placeHolder: "")
        
        btnLogin.layer.cornerRadius = 8
        ///txtpin.addUItwo(placeholder: "pin")
    }
    
    static func getVCInstance() -> UIViewController{
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    @IBAction func Login(_ sender: AttributedButton) {
        
        if txtpin.text == ""{
            txtpin.showError(errStr: "ENTERPIN")
        }
        else {
            validatePIN(pin: (self.txtpin?.text)!)
            
        }
    }
    
    // Login With Face ID / Touch ID
    @IBAction func LoginwithFaceId(_ sender: AttributedButton) {
        
        if UserDefaults.standard.object(forKey: UserDefault.biometric) as? Bool == false{
            showMessage(title: "", message: "\(supportid) not enable")
        }
        else {
            validAuth()
        }
    }
    
    @IBAction func didTogleButtonTapped(_ sender: Any) {
        if self.checkOtp {
            self.btnShowHideToggle.isSelected = true
            self.checkOtp = false
        } else {
            self.btnShowHideToggle.isSelected = false
            self.checkOtp = true
        }
        self.txtpin.isSecureTextEntry.toggle()
        
    }
 }

extension LoginExistingViewController {
    
    func checkUserDeviceSupport()
    {
        //IF USER DEVICE SUPPORT FACE ID, CHANGE LABEL ON SCREEN
        let context = LAContext()
        if Authntication(context).checkUserDeviceFaceIDSupport() == true
        {
            isFaceIDSupport = true
            supportid = "Login with Face ID"
            img_ID.image = UIImage.init(named: "faceid")
        }
            
        else {
            isFaceIDSupport = true
            supportid = "Login with Touch ID"
            img_ID.image = UIImage.init(named: "fingerprint")
        }
        Fonts().set(object: self.lblfaceId, fontType: 0, fontSize: 14, color: Color.white, title: supportid, placeHolder: "")
        
    }
}

extension LoginExistingViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtpin.removeError()
        //textField.didBegin()
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //   textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
    
    func textField(
        textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            // self.view.frame.origin.y -= keyboardSize.height - 150
            if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_XR || DeviceType.IS_IPHONE_XSMAX {
                self.consBottom.constant = 260
                self.cartoonImageHeight.constant = 150
            } else if DeviceType.IS_IPHONE_5 {
                
                self.consBottom.constant = 350
                self.cartoonImageHeight.constant = 100
            } else if DeviceType.IS_IPHONE_6P {
                self.consBottom.constant = 260
                self.cartoonImageHeight.constant = 150
            } else if  DeviceType.IS_IPHONE_6 {
                self.consBottom.constant = 280
                self.cartoonImageHeight.constant = 130
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
        self.consBottom.constant = 0
        self.cartoonImageHeight.constant = 220
    }
    
    func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func showCaption() {
        Fonts().set(object: lblusername, fontType: 0, fontSize: 22, color: Color.white, title: "", placeHolder: "")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        let time = dateFormatter.string(from: Date())
        let timeStr = time.replacingOccurrences(of: ":", with: ".")
        let hourMinute = Float(timeStr)
        lblusername.text = getMsg(hourMinute!)
    }
    
    func getMsg(_ hour: Float) -> String{
        
        if (CoreDataHelper().getDataForEntity(entity: Entity.PROFILES_INFO).count) > 0 {
            let dict = CoreDataHelper().getDataForEntity(entity: Entity.PROFILES_INFO)
            let userProfile = dict[0]["aFIRST_NAME"]?.capitalized
            
            
            if hour >= 0 && hour <= 11.30 {
                return "Good Morning, \(userProfile ?? "110")!"
            } else if hour > 11.30 && hour <= 16.30 {
                return "Good Afternoon, \(userProfile  ?? "111")!"
            } else {
                return "Good Evening, \(userProfile ?? "112")!"
            }
        }
        else {
            if hour >= 0 && hour <= 11.30 {
                return "Good Morning,"
            } else if hour > 11.30 && hour <= 16.30 {
                return "Good Afternoon,"
            } else {
                return "Good Evening,"
            }
        }
    }
}

extension LoginExistingViewController :AuthDelegate   {
    
    func validate() {
        //Check if user enable biometric access
        if UserDefaults.standard.bool(forKey: UserDefault.biometric) {
            let context = LAContext()
            Authntication(context).authnticateUser { (success, message) in
                DispatchQueue.main.async {
                    if success == true {
                        self.validAuth()
                    }
                    else {
                        if message != "" {
                            self.showMessage(title: "", message: message)
                        }
                    }
                }
            }
        }
        else {
            let vc = AuthoriseViewController.getVCInstance() as! AuthoriseViewController
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func validAuth() {
        authnticateUser()
    }
    
    func authnticateUser() {
        //CHECK USER'S FINGERPRINT OR FACE ID
        let context = LAContext()
        Authntication(context).authnticateUser { (success, message) in
            if success == true {
                DispatchQueue.main.async {
                    //Set flag in user defaults for biometric access
                    UserDefaults.standard.set(true, forKey: UserDefault.biometric)
                    self.setUpSlideMenu()
                }
            }
            else {
                DispatchQueue.main.async {
                    if message != "" {
                        //Set flag in user defaults for biometric access
                        UserDefaults.standard.set(false, forKey: UserDefault.biometric)
                        self.showMessage(title: "", message: message)
                    }
                }
            }
        }
    }
    
    func setUpSlideMenu() {
        
         self.view.endEditing(true)
        UserDefaults.standard.set(true, forKey: "logout")
        UserDefaults.standard.synchronize()
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
}

extension LoginExistingViewController {
    
    func validatePIN(pin :String) {
        
        if !isInternetAvailable(){
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        var mob = OnboardUser.mobileNo
        if mob == ""{
            let mobileArr = CoreDataHelper().getDataForEntity(entity: Entity.MSISDN)
            if mobileArr.count > 0 {
                mob = mobileArr[0]["aMSISDN"] ?? ""
            }
        }
        
        
        self.view.endEditing(true)
        RestAPIManager.validatePIN(title: "PLEASEWAIT", subTitle: "VALACTIPIN", pin: pin, type: Response.self, mobileNo: mob, completion: { (response) in
            DispatchQueue.main.async {
                if response.statusCode == 200 {
                    self.setUpSlideMenu()
                }
                else {
                    self.txtpin?.text = ""
                    self.showMessage(title: "", message: response.statusMessage ?? "")
                }
            }
        }) { (error) in
            
        }
    }
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let activationKey: Int?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case activationKey = "ACTIVATION_KEY"
        }
    }
}


