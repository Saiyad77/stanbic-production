//
//  OnboardingCollectionViewCell.swift
//  Stanbic
//
//  Created by Vijay Patidar on 17/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class OnboardingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblLeft: UILabel!
    @IBOutlet weak var lblRight: UILabel!
    
    override func awakeFromNib() {
        
    }
    
    func initiateValue(image : UIImage, leftText : String, rightText : String) {
        
        Fonts().set(object: lblLeft, fontType: 3, fontSize: 18, color: Color.white, title: leftText, placeHolder: "")
        Fonts().set(object: lblRight, fontType: 2, fontSize: 19, color: Color.white, title: rightText, placeHolder: "")
        imgView.image = image
    }
    
}
