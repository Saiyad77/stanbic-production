//
//  OnboardingViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 17/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {

    //Mark :- IBOUTLET
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var btnPager: [AttributedButton]!
    @IBOutlet weak var btnGetStarted: AttributedButton!
    
    //Mark :- Variable
    let arrLeftTitle = ["ONBLEFTTITLE1", "ONBLEFTTITLE2", "ONBLEFTTITLE3"]
    let arrRightTitle = ["ONBRIGHTTITLE1", "ONBRIGHTTITLE2", "ONBRIGHTTITLE3"]
    let arrImage = [UIImage.init(named: "1.1"), UIImage.init(named: "1.2"), UIImage.init(named: "1.3")]
    var  lastContent = CGFloat()
    var selectedIndx = 0
    var timer : Timer?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if checkIsiPhoneIsJailbroken() {
            self.btnGetStarted.isHidden = true
            Global.showAlertMessageWithOkButtonAndTitle("Stanbic", andMessage: "You can't access app features in Jailbreak/Rooted device.")
        }
        
        
        // Do any additional setup after loading the view.
        Fonts().set(object: btnGetStarted, fontType: 1, fontSize: 14, color: Color.white, title: "GETSTARTED", placeHolder: "")
        btnGetStarted.roundedButton()
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
        
        fetchTermsCounditions()
        
        
    }
    
    
    func checkIsiPhoneIsJailbroken() -> Bool {
        
        if TARGET_IPHONE_SIMULATOR != 1 {
            // Check 1 : existence of files that are common for jailbroken devices
            if FileManager.default.fileExists(atPath: "/Applications/Cydia.app")
                || FileManager.default.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib")
                || FileManager.default.fileExists(atPath: "/bin/bash")
                || FileManager.default.fileExists(atPath: "/usr/sbin/sshd")
                || FileManager.default.fileExists(atPath: "/etc/apt")
                || FileManager.default.fileExists(atPath: "/private/var/lib/apt/")
                || UIApplication.shared.canOpenURL(URL(string: "cydia://package/com.example.package")!)
            {
                return true
            }
            // Check 2 : Reading and writing in system directories (sandbox violation)
            let stringToWrite = "Jailbreak Test"
            do
            {
                try stringToWrite.write(toFile: "/private/JailbreakTest.txt", atomically:true, encoding:String.Encoding.utf8)
                //Device is jailbroken
                return true
            }catch
            {
                return false
            }
        } else {
            return false
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    //Mark:- Custom Methods
    @available(iOS 12.0, *)
    @IBAction func btnGetStarted(_ sender: AttributedButton) {
        let vc = EnterMobileNoViewController.getVCInstance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//Mark :- Extension
extension OnboardingViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath) as! OnboardingCollectionViewCell
        cell.initiateValue(image: arrImage[indexPath.row]!, leftText: arrLeftTitle[indexPath.row], rightText: arrRightTitle[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.frame.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
        self.selectedIndx = (visibleIndexPath?.row)!
        self.setPageControl()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.frame.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)
        if let cell = self.collectionView.cellForItem(at: visibleIndexPath!) as? OnboardingCollectionViewCell {
            
            if  visibleIndexPath?.row == 0 {
                
                if lastContent > self.collectionView.contentOffset.x {
                    
                    let percent = self.collectionView.contentOffset.x / (self.collectionView.frame.size.width)
                    cell.lblLeft.transform = CGAffineTransform(scaleX: (1 - percent), y: (1 - percent))
                    cell.lblRight.transform = CGAffineTransform(scaleX: (1 - percent), y: (1 - percent))
                }
                else if (lastContent < self.collectionView.contentOffset.x) {
                    
                    let percent = self.collectionView.contentOffset.x / (self.collectionView.frame.size.width)
                    cell.lblLeft.transform = CGAffineTransform(scaleX: (1 - percent), y: (1 - percent))
                    cell.lblRight.transform = CGAffineTransform(scaleX: (1 - percent), y: (1 - percent))
                }
            }
            else if  visibleIndexPath?.row == 1 {
                
                if lastContent > self.collectionView.contentOffset.x {
                    
                    var percent = self.collectionView.contentOffset.x / (self.collectionView.frame.size.width )
                    
                    if percent > 1.0 {
                        percent = (2.0 - percent)
                    }
                    
                    cell.lblRight.transform = CGAffineTransform(scaleX: (percent), y: (percent))
                    cell.lblLeft.transform = CGAffineTransform(scaleX: (percent), y: (percent))
                }
                else if (lastContent < self.collectionView.contentOffset.x) {
                    
                    var percent = self.collectionView.contentOffset.x / (self.collectionView.frame.size.width )
                    
                    if percent > 1.0 {
                        percent = (2.0 - percent)
                    }
                    
                    cell.lblRight.transform = CGAffineTransform(scaleX: (percent), y: (percent))
                    cell.lblLeft.transform = CGAffineTransform(scaleX: (percent), y: (percent))
                }
            }
            else if  visibleIndexPath?.row == 2 {
                
                if lastContent > self.collectionView.contentOffset.x {
                    
                    let percent = self.collectionView.contentOffset.x / (self.collectionView.frame.size.width * 2)
                    
                    cell.lblLeft.transform = CGAffineTransform(scaleX: (percent), y: (percent))
                    cell.lblRight.transform = CGAffineTransform(scaleX: (percent), y: (percent))
                }
                else if (lastContent < self.collectionView.contentOffset.x) {
                    
                    let percent = self.collectionView.contentOffset.x / (self.collectionView.frame.size.width * 2)
                    cell.lblRight.transform = CGAffineTransform(scaleX: (percent), y: (percent))
                    cell.lblLeft.transform = CGAffineTransform(scaleX: (percent), y: (percent))
                }
            }
        }
        lastContent = self.collectionView.contentOffset.x
    }
    
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        if let coll  = collectionView {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)!  < arrImage.count - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                    self.selectedIndx = (indexPath1?.row)!
                    self.setPageControl()
                }
                else {
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                    self.selectedIndx = (indexPath1?.row)!
                    self.setPageControl()
                }
            }
        }
    }
    
    func setPageControl(){
        
        for i in 0 ..< btnPager.count {
            if i == selectedIndx {
               btnPager[i].alpha = 1.0
            }
            else {
               btnPager[i].alpha = 0.5
            }
        }
    }
}

// Fetch Terms Counditions:_
extension OnboardingViewController {
    
    func fetchTermsCounditions(){
        RestAPIManager.fetchtermsandcounditions(title: "", subTitle: ""){ (json) in
            
            if json["STATUS_CODE"].intValue == 200 {
                DispatchQueue.main.async {
               let dict = json["RESULT_ARRAY"].dictionaryValue
                    global.termsandcounditions = dict["TERMS_CONDITIONS"]?.stringValue ?? ""
                    global.privacyandpolicies = dict["PRIVACY_POLICY"]?.stringValue ?? ""
                }
            }
            else {
                self.showMessage(title: "", message: json["STATUS_MESSAGE"].stringValue)
            }
        }
    }
}
