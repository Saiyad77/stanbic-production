//
//  SuccessfullRegisterMobBankingVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 28/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class SuccessfullRegisterMobBankingVC: UIViewController {
    

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTitle: UILabel!

    var dict = [String: JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(true)
        
        if !isInternetAvailable(){
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        
        RestAPIManager.checkRegistraionStatus(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOURREQUEST", mobileNo: OnboardUser.mobileNo) { (json) in
            DispatchQueue.main.async {
               // print(json)
                if json["STATUS_CODE"].intValue == 200 {
                    
                    self.dict = json["RESULT_ARRAY"].dictionaryValue
                    
                    if self.dict["PIN_STATUS"]?.stringValue == "1"
                    {
                        let vc = EnterPinViewController.getVCInstance() as! EnterPinViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else if self.dict["PIN_STATUS"]?.stringValue == "2"
                    {
                        let vc = EnterYourOTPVC.getVCInstance() as! EnterYourOTPVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else{
                      self.showMessage(title: "", message: json["STATUS_MESSAGE"].stringValue)
                    }
                }
                else{
                    self.showMessage(title: "", message: json["STATUS_MESSAGE"].stringValue)
                }
            }
        }
     }
    
    func setFonts() {
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 28, color: Color.black, title: "WELCOME1", placeHolder: "")
        Fonts().set(object: lblMessage, fontType: 0, fontSize: 13, color: Color.c134_142_150, title: "", placeHolder: "")
        lblMessage.text = "PLEASEWAITASWE".localized(Session.sharedInstance.appLanguage)

    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
}
