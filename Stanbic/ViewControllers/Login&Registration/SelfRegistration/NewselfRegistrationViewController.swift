//  NewselfRegistrationViewController.swift
//  Stanbic
//  Created by 5exceptions-mac3 on 27/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.

import UIKit

class NewselfRegistrationViewController: UIViewController {
 
    
    //MARK:  IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var btnProofOption: UIButton!
    @IBOutlet weak var txtNationalityProof: UITextField!
    @IBOutlet weak var txtBirthdayDate: UITextField!
    @IBOutlet weak var txtAccountNumber: UITextField!
    @IBOutlet weak var tableviewProof: UITableView!
    @IBOutlet weak var ProofView: UIView!
   
    
    // MARK: ALL PROPERTIES
    var model = FetchQuestionVM()
    var editableTextfield : UITextField?
    var ansArr = [[String : String]]()
    private var lastContentOffset: CGFloat = 0
    var presenter: SelfRegisterPresenter!
    var proofItemArray: NSMutableArray = NSMutableArray()
    var isViewOn: Bool = true
    var selfRegisterData:NewOption!
    
    //Regex Var
    
    var alienIDRegex: String = String()
    var paasportRegex: String = String()
    var idNumberRegex: String = String()
    var accountNumberRegex: String = String()
    var errorName: String = String()
    var titleMatch: String = String()
    var datediffrent: String = String()
    
    var charlimitForAccountNumber: String = ""
 
    
    //let datePicker = UIDatePicker()
     // MARK: VIEW CONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = SelfRegisterPresenter(delegate: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setFonts()
        dataLoad()
        
        self.btnProofOption.setTitle("ID Number", for: .normal)
        self.btnProofOption.setTitleColor(.lightGray, for: .normal)
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.ProofView.isHidden = true
    }
    
    func dataLoad() {
        
        DispatchQueue.main.async {
            self.errorName = Global.getStringValue(self.selfRegisterData.questionone.value(forKey: "question") as AnyObject)
            self.txtNationalityProof.addUI(placeholder: Global.getStringValue(self.selfRegisterData.questionone.value(forKey: "question") as AnyObject))
            self.txtBirthdayDate.addUI(placeholder: Global.getStringValue(self.selfRegisterData.questiontwo.value(forKey: "question") as AnyObject))
            self.txtAccountNumber.addUI(placeholder: Global.getStringValue(self.selfRegisterData.questionthree.value(forKey: "question") as AnyObject))
            
            self.alienIDRegex = Global.getStringValue(self.selfRegisterData.questionone.value(forKey: "ALIEN_ID_REGEX") as AnyObject)
            self.paasportRegex = Global.getStringValue(self.selfRegisterData.questionone.value(forKey: "PASSPORT_REGEX") as AnyObject)
            self.idNumberRegex = Global.getStringValue(self.selfRegisterData.questionone.value(forKey: "ID_NUMBER_REGEX") as AnyObject)
            self.accountNumberRegex = Global.getStringValue(self.selfRegisterData.questionthree.value(forKey: "STANBIC_ACCOUNT_NUMBER_REGEX") as AnyObject)
            self.charlimitForAccountNumber = Global.getStringValue(self.selfRegisterData.questionthree.value(forKey: "charlimit") as AnyObject)
            
            if let dict = self.selfRegisterData.questionone.value(forKey: "options") as? NSDictionary {
                
                let one = Global.getStringValue(dict.value(forKey: "1") as AnyObject)
                let two = Global.getStringValue(dict.value(forKey: "2") as AnyObject)
                let three = Global.getStringValue(dict.value(forKey: "3") as AnyObject)
                
                var dictionary: NSDictionary = NSDictionary()
                dictionary = ["one":one]
                self.proofItemArray.add(dictionary)
                dictionary = ["one":two]
                self.proofItemArray.add(dictionary)
                dictionary = ["one":three]
                self.proofItemArray.add(dictionary)
            }
            self.tableviewProof.reloadData()
            
            if self.txtMobileNo.text != "" {
                self.txtMobileNo.didBegin()
            }
            if  self.txtBirthdayDate.text != "" {
                self.txtBirthdayDate.didBegin()
            }
            if  self.txtNationalityProof.text != "" {
                self.txtNationalityProof.didBegin()
            }
            if  self.txtAccountNumber.text != "" {
                self.txtAccountNumber.didBegin()
            }
        }
        
    }
    
    static func getVCInstance() -> UIViewController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lblTitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "REGISTERFORMOBILEBAK", placeHolder: "")
        Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 15, color: Color.c52_58_64, title: "LETSGETSTARTED", placeHolder: "")
        lblSubtitle.setLineSpacing(lineSpacing: 4.0)
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        
        Fonts().set(object: txtNationalityProof, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: txtBirthdayDate, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: txtAccountNumber, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        
        let mobileNo = OnboardUser.mobileNo.replacingOccurrences(of: "254", with: "")
        Fonts().set(object: self.lblCountryCode, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "+254", placeHolder: "")
        Fonts().set(object: txtMobileNo, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: mobileNo, placeHolder: "ENTERMOBPLACEHOLDER")
        txtMobileNo.isEnabled = false
        txtMobileNo.text = mobileNo
        let applanguage = Session.sharedInstance.appLanguage
        txtMobileNo.attributedPlaceholder = NSAttributedString(string: "ENTERMOBPLACEHOLDER".localized(applanguage as String), attributes: [NSAttributedString.Key.foregroundColor: Color.c134_142_150])

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if txtMobileNo.text == "" {
            txtMobileNo.didBegin()
        }
        if txtBirthdayDate.text == "" {
            txtBirthdayDate.didBegin()
        }
        if txtNationalityProof.text == "" {
            txtNationalityProof.didBegin()
        }
        if txtAccountNumber.text == "" {
            txtAccountNumber.didBegin()
        }
        
    }
    
    
    //MARK:- Button Actions
    @IBAction func didBackButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didCountinueButtonPressed(_ sender: AttributedButton) {
        let accNumber = NSPredicate(format:"SELF MATCHES %@", accountNumberRegex)

        if txtNationalityProof.text!.isEmpty {
            txtNationalityProof.showError(errStr: "Please enter \(errorName)")
        } else if checkacbyType(textvalue: txtNationalityProof.text!).0 == false {
            txtNationalityProof.showError(errStr: checkacbyType(textvalue: txtNationalityProof.text!).1)
        } else if txtBirthdayDate.text!.isEmpty {
            txtBirthdayDate.showError(errStr: "Enter your date of birth (dd/mm/yyyy)")
        } else if Int(self.datediffrent)! < 17 {
            txtBirthdayDate.showError(errStr: "Please enter a date equal to or above 18 years.")
            return
        }  else if txtAccountNumber.text!.isEmpty {
            txtAccountNumber.showError(errStr: "Please enter Account Number")
        } else if accNumber.evaluate(with: txtAccountNumber.text) == false {
                    txtAccountNumber.showError(errStr: "Please enter valid Account Number")
        } else {
             submitQuestion()
        }
        
     self.view.endEditing(true)
        
    }

    @IBAction func isButtonTappedForOpenProofTable(_ sender: Any) {
       
        self.view.endEditing(true)
        if isViewOn {
            self.ProofView.isHidden = false
            isViewOn = false
        } else {
            self.ProofView.isHidden = true
            isViewOn = true
        }
    }
    
    func submitQuestion() {
        
        var dict: NSDictionary = NSDictionary()
        var ary: NSMutableArray = NSMutableArray()
        
        dict = ["1": txtNationalityProof.text!, "2": txtBirthdayDate.text!, "3": txtAccountNumber.text!]
            ary.add(dict)
         let str = Global.convertArrayToJson(from: ary)
        
        presenter.get_setQuestion(title: "PLEASEWAIT", subTitle : "SUBMITANS", action: "SUBMIT", answer: str!)
      //  model.submitQuestionAPI(action: "SUBMIT", answerArr:str!)
    }
    
    func checkacbyType(textvalue:String) -> (Bool, String){
        if self.titleMatch ==  "Alien ID" {
            return (createRegext(value: textvalue, type: "Alien ID"), "Please enter valid Alied ID")
        } else if self.titleMatch ==  "Passport"  {
            return (createRegext(value: textvalue, type: "Passport"), "Please enter valid Passport Number")
        } else if self.titleMatch ==  "ID Number"  {
            return (createRegext(value: textvalue, type: "ID Number"), "Please enter valid ID Number")
        } else {
            return (true, "")
        }
    }
    
    
    func createRegext(value:String,type:String) ->Bool{
        
        switch type {
            
        case "Alien ID":
            let acTest = NSPredicate(format:"SELF MATCHES %@", alienIDRegex)
            return acTest.evaluate(with: value)
            
        case "Passport":
            let cardTest = NSPredicate(format:"SELF MATCHES %@", paasportRegex)
            return cardTest.evaluate(with: value)
            
        case "ID Number":
            let genralTest = NSPredicate(format:"SELF MATCHES %@", idNumberRegex)
            return genralTest.evaluate(with: value)
 
        default:
            return false
            
        }
    }
    
    
    
    @IBAction func didTapTextField(_ sender: UITextField) {
        let datePickerView: UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        txtBirthdayDate.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerFromValueChanged), for: UIControl.Event.valueChanged)
    }
    
    
   @objc func datePickerFromValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        txtBirthdayDate.text = dateFormatter.string(from: sender.date)
        
    }
    
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        txtBirthdayDate.text = dateFormatter.string(from: sender.date)
        self.datediffrent = Global.dateDiff(txtBirthdayDate.text!, DateFormat: "dd/MM/yyyy")
      }
}


//MARK: UITextFieldDelegate
extension NewselfRegistrationViewController : UITextFieldDelegate {
     
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
         if textField == txtBirthdayDate {
            let datePickerView = UIDatePicker()
            datePickerView.maximumDate = Date()
            datePickerView.datePickerMode = .date
            txtBirthdayDate.inputView = datePickerView
            datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        }
        editableTextfield = textField
        editableTextfield?.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    
    
    // MARK: UITextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == " " {
            return false
        }
        
        let mobileNumber: String = txtAccountNumber.text!
        if textField.text?.count == 0 && string == " " {
            return false
        }
        if mobileNumber.isInt {
            
            if textField == self.txtAccountNumber {
                
                if (textField.text?.count)! > 12 &&  range.length == 0 {
                    return false
                } else {
                    if string.count > 12 {
                        let index = string.index((string.startIndex), offsetBy: Int(self.charlimitForAccountNumber)!)
                        textField.text = String(string[index...])
                        return false
                    } else {
                        return true
                    }
                }
            } else {
                return true
            }
        } else {
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
    }
    
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification) {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
        
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
 }

extension NewselfRegistrationViewController: SelfRegisterModelDelegate {
    
    func apiCallsuccessResultGet(selfRegisterData: NewOption) {

        DispatchQueue.main.async {
            
            self.errorName = Global.getStringValue(selfRegisterData.questionone.value(forKey: "question") as AnyObject)
            self.txtNationalityProof.addUI(placeholder: Global.getStringValue(selfRegisterData.questionone.value(forKey: "question") as AnyObject))
            self.txtBirthdayDate.addUI(placeholder: Global.getStringValue(selfRegisterData.questiontwo.value(forKey: "question") as AnyObject))
            self.txtAccountNumber.addUI(placeholder: Global.getStringValue(selfRegisterData.questionthree.value(forKey: "question") as AnyObject))
            
            self.alienIDRegex = Global.getStringValue(selfRegisterData.questionone.value(forKey: "ALIEN_ID_REGEX") as AnyObject)
            self.paasportRegex = Global.getStringValue(selfRegisterData.questionone.value(forKey: "PASSPORT_REGEX") as AnyObject)
            self.idNumberRegex = Global.getStringValue(selfRegisterData.questionone.value(forKey: "ID_NUMBER_REGEX") as AnyObject)
            self.accountNumberRegex = Global.getStringValue(selfRegisterData.questionthree.value(forKey: "STANBIC_ACCOUNT_NUMBER_REGEX") as AnyObject)
            self.charlimitForAccountNumber = Global.getStringValue(selfRegisterData.questionthree.value(forKey: "charlimit") as AnyObject)
            
        if let dict = selfRegisterData.questionone.value(forKey: "options") as? NSDictionary {
            
            let one = Global.getStringValue(dict.value(forKey: "1") as AnyObject)
            let two = Global.getStringValue(dict.value(forKey: "2") as AnyObject)
            let three = Global.getStringValue(dict.value(forKey: "3") as AnyObject)
            
            var dictionary: NSDictionary = NSDictionary()
            dictionary = ["one":one]
            self.proofItemArray.add(dictionary)
            dictionary = ["one":two]
            self.proofItemArray.add(dictionary)
            dictionary = ["one":three]
            self.proofItemArray.add(dictionary)
        }
        self.tableviewProof.reloadData()
            
            if self.txtMobileNo.text != "" {
                self.txtMobileNo.didBegin()
            }
            if  self.txtBirthdayDate.text != "" {
                self.txtBirthdayDate.didBegin()
            }
            if  self.txtNationalityProof.text != "" {
                 self.txtNationalityProof.didBegin()
            }
            if  self.txtAccountNumber.text != "" {
                 self.txtAccountNumber.didBegin()
            }
    }
}
    
    func Errormsg(message: String, code: Int , action: String) {
        if action == "SUBMIT" {
            
            if code == 200 {
                 DispatchQueue.main.async {
                let vc = ValidationSuccessViewController.getVCInstance() as! ValidationSuccessViewController
                self.navigationController?.pushViewController(vc, animated: true)
                }
                
            } else {
                 DispatchQueue.main.async {
                 let nextVc = ErrorVC.getVCInstance() as! ErrorVC
                nextVc.message =  message
                self.navigationController?.pushViewController(nextVc, animated: true)
                }
                
            }
        } else {
            DispatchQueue.main.async {
                let nextVc = ErrorVC.getVCInstance() as! ErrorVC
                nextVc.message =  message
                self.navigationController?.pushViewController(nextVc, animated: true)
            }
        }
    }
}

extension NewselfRegistrationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return proofItemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SelfRegisterProofItemTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SelfRegisterProofItemTableViewCell", for: indexPath) as! SelfRegisterProofItemTableViewCell
        
        let dict = proofItemArray[indexPath.row] as! NSDictionary
        cell.configureCell(with: dict)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = proofItemArray[indexPath.row] as! NSDictionary
        self.btnProofOption.setTitle(Global.getStringValue(dict.value(forKey: "one") as AnyObject), for: .normal)
        self.btnProofOption.setTitleColor(.black, for: .normal)
        self.txtNationalityProof.addUI(placeholder: Global.getStringValue(dict.value(forKey: "one") as AnyObject))
        self.titleMatch = Global.getStringValue(dict.value(forKey: "one") as AnyObject)
        self.ProofView.isHidden = true
        isViewOn = true
        self.txtNationalityProof.text = ""
        
    }
}



extension NewselfRegistrationViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //self.view.endEditing(true)
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}

// MARK: String int check
extension String {
    var isInt:Bool {
        return Int(self) != nil
    }
}
