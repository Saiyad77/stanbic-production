//
//  ValidationSuccessViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 19/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class ValidationSuccessViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnDone: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setFonts()
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
        // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: self.lblTitle, fontType: 1, fontSize: 22, color: Color.c0_173_108, title: "VALIDATIONSUCCESS", placeHolder: "")
        Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 17, color: Color.c52_58_64, title: "VALIDATIONSUCCESSDESCR", placeHolder: "")
        Fonts().set(object: self.btnDone, fontType: 1, fontSize: 14, color: Color.white, title: "DONE", placeHolder: "")
        btnDone.roundedButton()
    }
    
    //Mark:- Button Actions
    @IBAction func done(_ sender: UIButton) {
//        let v = self.navigationController?.viewControllers
//        for i in (v)!
//        {
//            if i is EnterMobileNoViewController
//            {
//                self.navigationController?.popToViewController(i, animated: true)
//                break
//            }
//        }
        let vc = SuccessfullRegisterMobBankingVC.getVCInstance()  as! SuccessfullRegisterMobBankingVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
