//
//  SelfRegisterProofItemTableViewCell.swift
//  Stanbic
//
//  Created by 5exceptions-mac3 on 28/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class SelfRegisterProofItemTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(with cellData:NSDictionary ) {
        self.lblName.text = Global.getStringValue(cellData.value(forKey: "one") as AnyObject)
    }

}
