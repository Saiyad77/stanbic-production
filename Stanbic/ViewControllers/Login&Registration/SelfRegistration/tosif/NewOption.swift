//
//  NewOption.swift
//  Stanbic
//
//  Created by 5exceptions-mac3 on 27/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation


class NewOption: NSObject {
 
    var questionone: NSDictionary = NSDictionary()
    var questiontwo: NSDictionary = NSDictionary()
    var questionthree: NSDictionary = NSDictionary()
 
    init?(dict : [String:AnyObject]) {
        
        self.questionone = dict["1"] as! NSDictionary
        self.questiontwo = dict["2"] as! NSDictionary
        self.questionthree = dict["3"]! as! NSDictionary
     }
}

