//
//  SelfRegisterPresenter.swift
//  Stanbic
//
//  Created by 5exceptions-mac3 on 28/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit

protocol SelfRegisterModelDelegate: class {
    
    func apiCallsuccessResultGet(selfRegisterData: NewOption)
    func Errormsg(message : String, code:Int, action: String)
    
}

class SelfRegisterPresenter {
    
    weak var delegate: SelfRegisterModelDelegate?
    init(delegate: SelfRegisterModelDelegate) {
        self.delegate = delegate
    }
    
    /*
     func fetchQuestionAPI(action : String) {
     
     RestAPIManager.get_setQuestion(title: "PLEASEWAIT", subTitle: "FETCHINGQUE", action: action, answer: "") { (json) in
     if json["STATUS_CODE"].intValue == 200 {
     do {
     
     
     
     } catch let error as NSError {
     print(error)
     }
     }
     }
     }
     */
    
    func get_setQuestion(title: String, subTitle : String, action : String, answer : String) {
        let loader =  RestAPIManager.addProgress(title: title, description: subTitle)
        
        let Url = String(format: RestAPIManager.baseURL)
        guard let serviceUrl = URL(string: Url) else { return }
        let paramDict = [
            "SCOMMAND"      :   sCommand.SR,
            "UPHONENUM"     :   OnboardUser.mobileNo,
            "UDEVID"        :   RestAPIManager.getuuidFromDb(),
            "ORGID"         :   sCommand.oRGID,
            "CUSTOMER_HASH" :   RestAPIManager.getuuidFromDb(),
            "CLIENT_HASH"   :   RestAPIManager.clientHash,
            "osVersion"     :   UIDevice.current.systemVersion,
            "appVersion"    :   RestAPIManager.appVersion,
            "ORIGIN"        :   sCommand.oRIGIN,
            "ACTION"        :   action,
            "QUESTIONS"     :   answer
            ] as [String : Any]
        
        var encriptedInfo = [String : AnyObject]()
        let jsonData = try! JSONSerialization.data(withJSONObject: paramDict, options: [])
        var decoded = String(data: jsonData, encoding: .utf8) ?? ""
        decoded = Secure().encryptPIN(text: decoded)
        let phnx = Variables().reveal(key: VariableConstants.phnx)
        let lpg = Variables().reveal(key: VariableConstants.lpg)
        encriptedInfo[phnx] = "1" as AnyObject
        encriptedInfo[lpg] = decoded as AnyObject
          let headers = ["content-type": "application/json","Idempotency-Key":Global.randomString(length: 16)]
        let request = NSMutableURLRequest(url: serviceUrl)
        request.allHTTPHeaderFields = headers
        request.httpMethod = "POST"
        request.timeoutInterval = 120 // In second
        
        do {
            let jsonBody = try JSONSerialization.data(withJSONObject: encriptedInfo, options: .prettyPrinted)
            request.httpBody = jsonBody
            var session : URLSession!
         //   if AppMode.appMode == ApplicationMode.development || AppMode.appMode == ApplicationMode.developmentEncpt
         //   {
                session = URLSession(
                    configuration: URLSessionConfiguration.default,
                    delegate: NSURLSessionPinningDelegate(),
                    delegateQueue: nil)
             
      //      }
//            else
//            {
//                session = URLSession.shared //without pinning
//            }
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                DispatchQueue.main.async {
                    loader.remove()
                }
                
                if let jsonData = data {
                    let json:JSON = JSON(data: jsonData)
                    let phnx = Variables().reveal(key: VariableConstants.phnx)
                    let lpg = Variables().reveal(key: VariableConstants.lpg)
                    
                    if json[phnx].intValue == 1 {
                        let encriptedStr = json[lpg].stringValue
                        let decrptedStr = Secure().decryptPIN(text: encriptedStr)
                        let decriptedData = Data(decrptedStr.utf8)
                     // let json:JSON = JSON(data: decriptedData)
                        do {
                            let json = try JSONSerialization.jsonObject(with: decriptedData, options: .mutableContainers) as? [String:Any]
                            
                            
                            if action != "SUBMIT" {
                                if json!["STATUS_CODE"] as! Int == 200 {
                                let result = (json as AnyObject).value(forKey: "RESULTS_ARRAY")
                                let resultData  = NewOption(dict: (result as? [String: AnyObject])!)
                                self.delegate?.apiCallsuccessResultGet(selfRegisterData: resultData!)
                                } else {
                                    self.delegate?.Errormsg(message: json!["STATUS_MESSAGE"] as! String, code: json!["STATUS_CODE"] as? Int ?? 0, action: action)
                                }
                                
                            }else {
                            self.delegate?.Errormsg(message: json!["STATUS_MESSAGE"] as! String, code: json!["STATUS_CODE"] as! Int, action: action)
                                
                            }
                            
                        } catch let err {
                            
                        }
                        
                        
                    } else {
                        
                    }
                    
                } else {
                    
                    var jsonTemp = JSON()
                    jsonTemp["STATUS_CODE"] = 1003
                    jsonTemp["STATUS_MESSAGE"] = "Can't perform the requested action due to a temporary server error.\nPlease try again later"
                }
            })
            task.resume()
        } catch {
            var jsonTemp = JSON()
        }
        
        //        var request = URLRequest(url: serviceUrl)
        //        request.httpMethod = "POST"
        //        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        //        guard let httpBody = try? JSONSerialization.data(withJSONObject: paramDict, options: []) else {
        //            return
        //        }
        //        request.httpBody = httpBody
        //
        //        let session = URLSession.shared
        //        session.dataTask(with: request) { (data, response, error) in
        //            if let response = response {
        //                  DispatchQueue.main.async {
        //                loader.remove()
        //                }
        //            }
        //               if AppMode.appMode == ApplicationMode.production {
        //
        //                if let jsonData = data {
        //                   let json:JSON = JSON(data: jsonData)
        //                   let phnx = Variables().reveal(key: VariableConstants.phnx)
        //                    let lpg = Variables().reveal(key: VariableConstants.lpg)
        //
        //                    if json[phnx].intValue == 1 {
        //
        //                        let encriptedStr = json[lpg].stringValue
        //                        let decrptedStr = Secure().decryptPIN(text: encriptedStr)
        //                       // let decriptedData = Data(decrptedStr.utf8)
        //
        //
        //                        if let data = decrptedStr.data(using: String.Encoding.utf8) {
        //                            do {
        //                                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]
        //
        //
        //                                if action != "SUBMIT" {
        //
        //
        //                                    let result = (json as AnyObject).value(forKey: "RESULTS_ARRAY")
        //
        //
        //                                    let resultData  = NewOption(dict: (result as? [String: AnyObject])!)
        //                                    self.delegate?.apiCallsuccessResultGet(selfRegisterData: resultData!)
        //
        //                                }
        //                               self.delegate?.Errormsg(message: "", code: 0, action: action)
        //
        //                            } catch {
        //
        //                            }
        //                        }
        //                        } else {
        //                    }
        //                } else {
        //            }
        //        }  else {
        //               do {
        //                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String:Any]
        //
        //
        //                        if action != "SUBMIT" {
        //
        //
        //                            let result = (json as AnyObject).value(forKey: "RESULTS_ARRAY")
        //
        //
        //                            let resultData  = NewOption(dict: (result as? [String: AnyObject])!)
        //                            self.delegate?.apiCallsuccessResultGet(selfRegisterData: resultData!)
        //
        //                        }
        //
        //                self.delegate?.Errormsg(message: json!["STATUS_MESSAGE"] as! String, code: json!["STATUS_CODE"] as! Int, action: action)
        //
        //                    } catch {
        //
        //                    }
        //
        //
        //
        //            }
        //        }.resume()
    }
}
