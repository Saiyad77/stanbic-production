//
//  WelcomeViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 18/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

@available(iOS 12.0, *)
class WelcomeViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblName1: UILabel!
    @IBOutlet weak var lblName22: UILabel!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnCreateAnAccount: UIButton!
    var presenter: SelfRegisterPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         presenter = SelfRegisterPresenter(delegate: self)
        setFonts()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
        // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: self.lblTitle, fontType: 1, fontSize: 24, color: Color.white, title: "WELCOMESTANBIC", placeHolder: "")
        Fonts().set(object: self.lblSubtitle, fontType: 3, fontSize: 15, color: Color.white, title: "FOUNDACCOUNT", placeHolder: "")
        
        lblSubtitle.setLineSpacing(lineSpacing: 3.0)
        lblSubtitle.textAlignment = .center
        Fonts().set(object: self.btnRegister, fontType: 1, fontSize: 14, color: Color.white, title: "REGISTERFORMOBBANKING", placeHolder: "")
        btnRegister.roundedButton()
     //   Fonts().set(object: self.btnCreateAnAccount, fontType: 1, fontSize: 14, color: Color.white, title: "OpenACOUNT", placeHolder: "")
         Fonts().set(object: self.lblName1, fontType: 1, fontSize: 14, color: Color.white, title: "Open Account", placeHolder: "")
             Fonts().set(object: self.lblName22, fontType: 1, fontSize: 14, color: Color.white, title: "Become Part of the Stanbic Family", placeHolder: "")
        btnCreateAnAccount.roundedButtonBorder()
        btnCreateAnAccount.layer.borderColor = UIColor.white.cgColor
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        let v = self.navigationController?.viewControllers
        for i in (v)!
        {
            if i is EnterMobileNoViewController
            {
                self.navigationController?.popToViewController(i, animated: true)
                break
            }
        }
    }
    
    // MARK:_ SELF REG. BUTTON
    @IBAction func register(_ sender: UIButton) {
        
        presenter.get_setQuestion(title: "PLEASEWAIT", subTitle : "FETCHINGQUE", action: "FETCH", answer: "")

    }
    
    // MARK:_ CREATE AN ACCOUN BUTTON
    @IBAction func createAnAccount(_ sender: UIButton) {
       self.showMessage(title: "", message: "Coming Soon")
     //   let vc = BeforeWeCountinueViewController.getVCInstance() as! BeforeWeCountinueViewController
      //  self.navigationController?.pushViewController(vc, animated: true)
    }
}

@available(iOS 12.0, *)
extension WelcomeViewController: SelfRegisterModelDelegate {
    
    func apiCallsuccessResultGet(selfRegisterData: NewOption) {
        
            DispatchQueue.main.async {
                let vc = NewselfRegistrationViewController.getVCInstance() as! NewselfRegistrationViewController
                vc.selfRegisterData = selfRegisterData
                self.navigationController?.pushViewController(vc, animated: true)
            }
       }
    
    func Errormsg(message: String, code: Int , action: String) {
       
            if code == 200 {
                DispatchQueue.main.async {
                 let vc = NewselfRegistrationViewController.getVCInstance() as! NewselfRegistrationViewController
                    
                          self.navigationController?.pushViewController(vc, animated: true)
                }
                
            } else {
                DispatchQueue.main.async {
                    let nextVc = ErrorVC.getVCInstance() as! ErrorVC
                    nextVc.message =  message
                    self.navigationController?.pushViewController(nextVc, animated: true)
        
            }
        }
    }
}
