//
//  QuestionsTableViewCell.swift
//  Stanbic
//
//  Created by Vijay Patidar on 18/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

protocol DatePickerValueDelegate: class {
    func didSetValue(value : String)
}

import UIKit

class QuestionsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txtQuestions: UITextField!
    
    let datePicker = UIDatePicker()
    weak var delegate : DatePickerValueDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        Fonts().set(object: txtQuestions, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initiateValue(model : FetchQuestionVM, indexPath : IndexPath, error : String) {
       /* if model.responseData.count != 0 {
            
        let question = model.responseData[indexPath.row - 1]["value"] as! JSON
        txtQuestions.restorationIdentifier = model.responseData[indexPath.row - 1]["que_id"] as? String
        if error != "" {
           txtQuestions.showError(errStr: error)
        }
        else if txtQuestions.text != "" {
            txtQuestions.didBegin()
        }
        else{
            txtQuestions.addUI(placeholder: question["question"].stringValue)
        }
        
        //Set Keyboard type
        if question["dataType"].intValue == 1 { //text
           // txtQuestions.keyboardType = .default
             initiateDatePicker(textfield : txtQuestions)
        }
        else if question["dataType"].intValue == 2 { //date in the format(ddmmyyyy)
            initiateDatePicker(textfield : txtQuestions)
        }
        else{ //number
            txtQuestions.keyboardType = .numberPad
        }
        }*/
    }
    
    // Date Picker Logic =======
    func initiateDatePicker(textfield : UITextField){
        //Registrater date picker value changed event
        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        datePicker.maximumDate = Date()
        //Formate Date
        datePicker.datePickerMode = .date
        textfield.inputView = datePicker
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        delegate.didSetValue(value: formatter.string(from: sender.date))
    }

}
