//
//  RegisterMobileBankingViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 18/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class RegisterMobileBankingViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    

    //Mark:- Varibales
    var model = FetchQuestionVM()
    var editableTextfield : UITextField?
    var ansArr = [[String : String]]()
    private var lastContentOffset: CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        tblView.tableFooterView = UIView()
        setFonts()
        fetchQuestion()
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lblTitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "REGISTERFORMOBILEBAK", placeHolder: "")
        Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 15, color: Color.c52_58_64, title: "LETSGETSTARTED", placeHolder: "")
        lblSubtitle.setLineSpacing(lineSpacing: 4.0)
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.setNeedsStatusBarAppearanceUpdate()
//        UIApplication.statusBarBackgroundColor =  Color.c0_123_255
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
                view.endEditing(true)
        if isValid() {
            self.view.endEditing(true)
            submitQuestion()
        }
    }
    
    func isValid() -> Bool {
        var isError : Bool = false
        for i in 0 ..< ansArr.count {
            var ansDict = ansArr[i]
            let answer = ansDict["answer"]
            if answer == "" {
                ansDict["error"] = ansArr[i]["question"]
                ansArr[i] = ansDict
                isError = true
            }
        }
        
        if isError == true {
            tblView.reloadData()
        }
        
        return !isError
    }
}

//Text field delegate
extension RegisterMobileBankingViewController : UITextFieldDelegate, DatePickerValueDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        editableTextfield = textField
        editableTextfield?.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            let que_id = textField.restorationIdentifier
            let limit = getCharLimit(que_id: que_id ?? "0")
            let question = getQuestion(que_id: que_id ?? "0")
            
            let dict = [
                "que_id" : que_id,
                "charlimit" : "\(limit)",
                "answer" : updatedText,
                "error" : "",
                "question" : question
                ] as! [String : String]
            
            if limit != 0 {
                if updatedText.count <= limit {
                    self.updateAnswer(que_id: que_id ?? "0", value: dict)
                    return true
                }
                else {
                    return false
                }
            }
            else {
                self.updateAnswer(que_id: que_id ?? "0", value: dict)
                return true
            }
        }
        
        let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
        
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    func didSetValue(value: String) {
        editableTextfield?.text = value
        let que_id = editableTextfield?.restorationIdentifier
        let limit = getCharLimit(que_id: que_id ?? "0")
        let question = getQuestion(que_id: que_id ?? "0")
        let dict = [
            "que_id" : que_id,
            "charlimit" : "\(limit)",
            "answer" : value,
            "error" : "",
            "question" : question
            ] as! [String : String]
        self.updateAnswer(que_id: que_id ?? "0", value: dict)
    }
    
    func getCharLimit(que_id : String) -> Int {
        for i in 0 ..< model.responseData.count {
            let queID = model.responseData[i]["que_id"] as? String
            if queID == que_id {
                let question = model.responseData[i]["value"] as! JSON
                let limit = question["charlimit"].intValue
                return limit
            }
        }
        return 0
    }
    
    func getQuestion(que_id : String) -> String {
        for i in 0 ..< model.responseData.count {
            let queID = model.responseData[i]["que_id"] as? String
            if queID == que_id {
                let question = model.responseData[i]["value"] as! JSON
                let questionVlue = question["question"].stringValue
                return questionVlue
            }
        }
        return ""
    }
    
    func updateAnswer(que_id : String, value : [String : String]) {
        if let index = ansArr.index(where: {$0["que_id"] == que_id}) {
            ansArr[index] = value
        }
        else {
            ansArr.append(value)
        }
    }
}

extension RegisterMobileBankingViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.responseData.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MOBILENO", for: indexPath) as! MobileNoTableViewCell
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QUESTIONS", for: indexPath) as! QuestionsTableViewCell
            var error = ""
//            if ansArr.count > (indexPath.row - 2) {
//                error = ansArr[indexPath.row - 2]["error"] ?? ""
//            }
            
            cell.initiateValue(model: model, indexPath: indexPath, error: error)
            cell.txtQuestions.delegate = self
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 87
        }
        else {
            return 72
        }
    }
}

//Call API
extension RegisterMobileBankingViewController : FetchQuestionVMDelegate {
    
    func Errromsg(msg: String, code: Int) {
        if code != 200{
           self.showMessage(title: "", message: msg)
        }
    }
    
    func fetchQuestion() {
        if !isInternetAvailable(){
            self.showMessage(title: "Error", message: "No internet connection!")
            return
        }
        model.delegate = self
        model.fetchQuestionAPI(action: "FETCH")
    }
    
    func reloadData(isFetch : Bool) {
        
        if isFetch == true {
 
            for i in 0 ..< model.responseData.count {
                model.responseData.sort(by: {($0["que_id"] as! String) < $1["que_id"] as! String})
                
                let question = model.responseData[i]["value"] as! JSON
                let que_id = model.responseData[i]["que_id"] as? String
                let limit = question["charlimit"].stringValue
                
                let dict = [
                    "que_id" : que_id,
                    "charlimit" : limit,
                    "answer" : "",
                    "error" : "",
                    "question" : question["question"].stringValue
                    ] as! [String : String]
                

                self.updateAnswer(que_id: que_id ?? "0", value: dict)
            }
            tblView.reloadData()
        }
        else {
            self.gotoNextVC()
        }
    }
    
    
    func submitQuestion() {
       /// model.submitQuestionAPI(action: "SUBMIT", answerArr: ansArr)
    }
    
    func gotoNextVC() {
        let vc = ValidationSuccessViewController.getVCInstance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func dicttojson(dictionary:NSDictionary) ->JSON{
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
            // here "decoded" is of type `AnyObject`, decoded from JSON data
            return decoded as! JSON
        } catch let error as NSError {
          
            return JSON()
        }
    }
}

extension RegisterMobileBankingViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
             //self.view.endEditing(true)
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}
