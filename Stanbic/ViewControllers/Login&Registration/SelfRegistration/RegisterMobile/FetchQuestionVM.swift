//
//  FetchQuestionVM.swift
//  Stanbic
//
//  Created by Vijay Patidar on 18/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
protocol FetchQuestionVMDelegate: class {
    func reloadData(isFetch : Bool)
    func Errromsg(msg : String,code:Int)
}

// ViewModel for Country List
class FetchQuestionVM{
    //MARK:- Model for API calling
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let resultArray: Result?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAE"
            case resultArray = "RESULTS_ARRAY"
            
        }
    }
    
    
    struct Result: Codable{
        let mbStatus    : Int?
        let name        : String?
        let profileID   : String?
        let pinStatus   : String?
        let lastLogin   : String?
        
        private enum CodingKeys: String, CodingKey {
            case mbStatus = "MB_STATUS"
            case name = "NAME"
            case profileID = "PROFILE_ID"
            case pinStatus = "PIN_STATUS"
            case lastLogin = "LAST_LOGIN"
        }
        
    }
    
    
    var responseData = [[String : Any]]()
    weak var delegate : FetchQuestionVMDelegate!
    func fetchQuestionAPI(action : String) {
        
        RestAPIManager.get_setQuestion(title: "PLEASEWAIT", subTitle: "FETCHINGQUE", action: action, answer: "") { (json) in
            if json["STATUS_CODE"].intValue == 200 {
                self.parseQuestion(json: json)
                
            }
        }
    }
    
    func parseQuestion(json : JSON) {
        let dictquestions = json["RESULTS_ARRAY"]
        for (key, value) in dictquestions {
            let dict  = [
                "que_id" : key,
                "value" : value
                ] as [String : Any]
            responseData.append(dict)
        }
        DispatchQueue.main.async {
            self.delegate.reloadData(isFetch: true)
        }
    }
    
    
   
    
    func submitQuestionAPI(action : String, answerArr : String) {
 
        RestAPIManager.get_setQuestion(title: "PLEASEWAIT", subTitle: "SUBMITANS", action: action, answer: answerArr ) { (json) in
            if json["STATUS_CODE"].intValue == 200 {
                
                DispatchQueue.main.async {
                    self.delegate.reloadData(isFetch: false)
                }
            } else {
                
                
                 self.delegate.Errromsg(msg: json["STATUS_MESSAGE"].stringValue, code: json["STATUS_CODE"].intValue )
                //self.showMessage(title: "", message: json["STATUS_MESSAGE"].stringValue)
 
            }
         }
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
}
