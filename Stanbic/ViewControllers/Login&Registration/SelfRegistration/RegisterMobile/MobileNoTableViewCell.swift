//
//  MobileNoTableViewCell.swift
//  Stanbic
//
//  Created by Vijay Patidar on 18/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class MobileNoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var txtMobileNo: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        let mobileNo = OnboardUser.mobileNo.replacingOccurrences(of: "254", with: "")
        Fonts().set(object: self.lblCountryCode, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "+254", placeHolder: "")
        Fonts().set(object: txtMobileNo, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: mobileNo, placeHolder: "ENTERMOBPLACEHOLDER")
        txtMobileNo.isEnabled = false
        txtMobileNo.text = mobileNo
        let applanguage = Session.sharedInstance.appLanguage
        txtMobileNo.attributedPlaceholder = NSAttributedString(string: "ENTERMOBPLACEHOLDER".localized(applanguage as String), attributes: [NSAttributedString.Key.foregroundColor: Color.c134_142_150])
        viewContainer.layer.borderColor = Color.c134_142_150.cgColor
        viewContainer.layer.borderWidth = 1.0
        viewContainer.layer.cornerRadius = 2.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
