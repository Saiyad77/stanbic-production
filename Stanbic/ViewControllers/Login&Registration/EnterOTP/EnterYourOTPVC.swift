//
//  EnterYourOTPVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 03/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class EnterYourOTPVC: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var txtOtpPin: UITextField!
    @IBOutlet weak var txtNewPin: UITextField!
    @IBOutlet weak var txtConfirmPin: UITextField!
    @IBOutlet weak var btnContinue: AttributedButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var btnShowHide: UIButton!
    var showClick = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setInitialFont()
        // Do any additional setup after loading the view.
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        txtOtpPin.keyboardType = .numberPad
        txtNewPin.keyboardType = .numberPad
        txtConfirmPin.keyboardType = .numberPad
        
        txtOtpPin.delegate = self
        txtNewPin.delegate = self
        txtConfirmPin.delegate = self
        
        txtConfirmPin.isUserInteractionEnabled = true
        
        btnShowHide.setTitle("Show", for: .normal)
        txtOtpPin.isSecureTextEntry = true
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func setInitialFont(){
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "ENTERYOUR_OTP", placeHolder: "")
        Fonts().set(object: lblSubTitle, fontType: 1, fontSize: 15, color: Color.c52_58_64, title: "Please_enter_your_One_Time_Pin_OTP", placeHolder: "")
        Fonts().set(object: txtOtpPin, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        txtOtpPin.addUI(placeholder: "ENTEROTPPIN")
        Fonts().set(object: txtNewPin, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        txtNewPin.addUI(placeholder: "ENTERNEWPIN")
        Fonts().set(object: txtConfirmPin, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        txtConfirmPin.addUI(placeholder: "CONFIRMNEWPIN")
        Fonts().set(object: btnContinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        Fonts().set(object: btnShowHide, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "Hide", placeHolder: "")
    }
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
        }
    }
    
    func CheckOTP()
    {

        
        if txtOtpPin.text == "" {
          txtOtpPin.showError(errStr: "ENTEROTP")
        }
        else if txtNewPin.text == ""{
               txtNewPin.showError(errStr: "ENTERPIN")
        }
        else if txtConfirmPin.text == ""{
            txtConfirmPin.showError(errStr: "ENTERCONFIRMPIN")
        }
        else if txtOtpPin.text  == txtNewPin.text{
            txtConfirmPin.showError(errStr: "OTP and PIN should not match.")
        }
        else if txtNewPin.text != txtConfirmPin.text{
            txtConfirmPin.showError(errStr: "New PIN and confirm PIN should be same")
        }
        else {
            if !isInternetAvailable(){
                self.showMessage(title: "Error", message: "No internet connection!")
                return
            }
            
            let pin1 = Secure().encryptPIN(text : (self.txtNewPin?.text)!)
            let pin2 = Secure().encryptPIN(text : (self.txtConfirmPin?.text)!)
            let otp = Secure().encryptPIN(text : (self.txtOtpPin.text!))
            let accAlias = ""
            
            RestAPIManager.setNewPin(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOURREQUEST", type: Response.self, pin1: "\(pin1)", pin2: "\(pin2)", accountAlias: accAlias, otp: "\(otp)", completion: { (response) in
                DispatchQueue.main.async {
                    
                    if response.statusCode == 200 {
                        
                        CoreDataHelper().saveUUID(value: Secure().encryptPIN(text : (self.txtNewPin?.text)!), entityName: Entity.PIN.rawValue, key: Entity.PIN.rawValue, removeRecords: true)
                        
                        let nextVc = EnableTouchIDViewController.getVCInstance() as! EnableTouchIDViewController
                 //       nextvc.msg = response.statusMessage!
                        self.navigationController?.pushViewController(nextVc, animated: true)
                    }
                    else{
                       // self.showMessage(title: "", message: response.statusMessage ?? "")
                        let nextVc = ErrorVC.getVCInstance() as! ErrorVC
                        nextVc.message = response.statusMessage ?? ""
                        self.navigationController?.pushViewController(nextVc, animated: true)
                        
                    }
                }
            }) { (error) in
              
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        txtOtpPin.didBegin()
//        txtNewPin.didBegin()
//        txtConfirmPin.didBegin()
    }
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnContinueTapped(_ sender: AttributedButton) {
        CheckOTP()
    }
    @IBAction func btnShowTapped(_ sender: UIButton)
    {
        if(showClick == true) {
            txtOtpPin.isSecureTextEntry = false
            btnShowHide.setTitle("Hide", for: .normal)
        } else {
            txtOtpPin.isSecureTextEntry = true
            btnShowHide.setTitle("Show", for: .normal)
        }
        
        showClick = !showClick
    }
}

//Text field delegate
extension EnterYourOTPVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        if textField == txtNewPin || textField == txtConfirmPin {
//            let maxLength = 4
//            let currentString: NSString = textField.text as! NSString
//            let newString: NSString =
//                currentString.replacingCharacters(in: range, with: string) as NSString
//            return newString.length <= maxLength
//        }
//        else {
        let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
        //}
     }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        
        return true
    }
    
    
}

extension EnterYourOTPVC
{
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = 34
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}


private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
}

extension String
{
    func safelyLimitedTo(length n: Int)->String {
        if (self.count <= n) {
            return self
        }
        return String( Array(self).prefix(upTo: n) )
    }
}
