//
//  TakeSelfiViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 19/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class TakeSelfiViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imgselfi: UIImageView!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnCountinueThisPhoto: UIButton!
    @IBOutlet weak var btnTakePhoto: UIButton!
    
    var isselfi = false
    var imgObj = UIImage()
    var imgObj1 = UIImage()
    var imgSelfie : UIImage?
    var imgSignature : UIImage?
    let model = SetPersonalDetailVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        imgselfi.image = imgObj
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if isselfi == true {
            imgSelfie = imgObj
            imgselfi.image = global.imgselfi
            
        }
        else{
            imgSignature = imgObj1
            imgselfi.image = global.imgsignature
        }
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        if isselfi == true {
            
            Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "TakeSELFI", placeHolder: "")
            Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 15, color: Color.c52_58_64, title: "TAKESELFIDEC", placeHolder: "")
        }
        else{
            Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "TAKEYOURSIGNATURE", placeHolder: "")
            Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 15, color: Color.c52_58_64, title: "SIGNATUREBGCLEAR", placeHolder: "")
            
        }
        
        lblSubtitle.setLineSpacing(lineSpacing: 4.0)
        lblSubtitle.textAlignment = .center
        
        
        Fonts().set(object: btnCountinueThisPhoto, fontType: 1, fontSize: 14, color: Color.white, title: "COUNTINUETHISPHOTO", placeHolder: "")
        btnCountinueThisPhoto.roundedButton()
        Fonts().set(object: btnTakePhoto, fontType: 1, fontSize: 14, color: Color.c0_123_255, title: "RETAKEPHOTO", placeHolder: "")
        btnTakePhoto.roundedButtonBorder()
        btnTakePhoto.layer.borderColor = Color.c207_211_214.cgColor
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func CountinueThisPhoto(_ sender: UIButton) {
        if isselfi == true {
            let vc = FillPersonalDetailViewController.getVCInstance()
            self.navigationController?.pushViewController(vc, animated: true)
         }
        else {
          //  Signature
            gotoNextVC()
        }
    }
    
    @IBAction func Retake(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension TakeSelfiViewController : SetPersonalDetailVMDelegate {
    
    func reloadData(msg: String) {
        if msg == ""{
            if model.responseData?["STATUS_CODE"].intValue == 200 {
                gotoNextVC()
            }
            else {
                self.showMessage(title: "", message: model.responseData?["STATUS_MESSAGE"].stringValue ?? "Error")
            }
        }
        else {
            self.showMessage(title: "", message: msg)
        }
    }
    
//    func submitData() {
//        model.delegate = self
//        if imgSignature == nil {
//           imgSignature = global.imgsignature
//        }
//        else if imgSelfie == nil {
//         imgSelfie = global.imgselfi
//        }
//        else {
//            model.validatePersonalDetail(action: "PERSONAL_DETAILS", imgArr: [imgSelfie! , imgSignature!],idNumber : "1",token : "212212",branch : "121212",country : "dsdds",postalcode : "211221",postaladdress : "assassa")
//        }
//    }
    
    func reloadData() {
        if model.responseData?["STATUS_CODE"].intValue == 200 {
            self.gotoNextVC()
        }
        else {
            self.showMessage(title: "", message: model.responseData?["STATUS_MESSAGE"].stringValue ?? "Error")
        }
    }
    
    func gotoNextVC() {
        let vc = OccupationalDetailsViewController.getVCInstance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

