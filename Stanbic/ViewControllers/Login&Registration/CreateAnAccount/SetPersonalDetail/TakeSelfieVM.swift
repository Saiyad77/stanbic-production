//
//  TakeSelfieVM.swift
//  Stanbic
//
//  Created by Vijay Patidar on 19/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit

protocol SetPersonalDetailVMDelegate: class {
   // func reloadData()
    func reloadData(msg:String)
}

class SetPersonalDetailVM {
    var responseData : JSON?
    weak var delegate : SetPersonalDetailVMDelegate!
    
    func validatePersonalDetail(action : String,imgArr : [UIImage],idNumber : String,token : String,branch : String,country : String,postalcode : String,postaladdress : String) {
        
        let loader = RestAPIManager.addProgress(title: "PLEASEWAIT", description: "WEAREPROCESSINGYOURREQUEST")
   
        RestAPIManager.validatePersonalDetail(title: "PLEASEWAIT", subTitle: "VALIDDATEDETAIL", action: action,idNumber : idNumber,token : token,branch : branch,country : country,postalcode : postalcode,postaladdress : postaladdress, images: imgArr) { (json) in
            DispatchQueue.main.async {
                loader.remove()
            }
            if json["STATUS_CODE"].intValue == 200 {
            DispatchQueue.main.async {
                self.responseData = json
                self.delegate.reloadData(msg: "")
                }
            }else{
                 self.delegate.reloadData(msg: json["STATUS_MESSAGE"].stringValue)
            }
        }
    }
}
