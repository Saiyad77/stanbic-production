//
//  ReadySelfiViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 19/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class ReadySelfiViewController: UIViewController {

    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imgselfi: UIImageView!
    @IBOutlet weak var btnReady: UIButton!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    //Mark:- Varibales
    var isselfi = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        setFonts()
    }
        // MARK:_SET FONT
    func setFonts() {
        if isselfi == true {
            imgselfi.image = UIImage(named: "take selfi")
            Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "READYSELFI", placeHolder: "")
            Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 15, color: Color.c52_58_64, title: "TAKESELFIDEC", placeHolder: "")
            lblSubtitle.setLineSpacing(lineSpacing: 2.5)
        }
        else{
            imgselfi.image = UIImage(named: "Signature")
            Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "TAKESIGNATURE1", placeHolder: "")
            Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 15, color: Color.c52_58_64, title: "TAKESIGNATUREDes", placeHolder: "")
        }
        lblSubtitle.setLineSpacing(lineSpacing: 4.0)
        lblSubtitle.textAlignment = .center
        Fonts().set(object: btnReady, fontType: 1, fontSize: 14, color: Color.white, title: "IAMREADY", placeHolder: "")
        
    }

    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Ready(_ sender: UIButton) {
        let vc = CameraViewController.getVCInstance() as! CameraViewController
        vc.isDocumentPhoto = false
        if isselfi == true {
            vc.isShowBackCamera = false
            vc.imageType = "SELFIE"
        }
        else {
            vc.isShowBackCamera = true
            vc.imageType = "SIGNATURE"
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
