//
//  BeforeWeCountinueViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 19/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class BeforeWeCountinueViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnGetStarted: UIButton!
    
    @IBOutlet weak var lblPhotoID: UILabel!
    @IBOutlet weak var lblKraPin: UILabel!
    @IBOutlet weak var lblSelfie: UILabel!
    @IBOutlet weak var lblSignature: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
        // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lblTitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "BEFOREWECOUNTINUE", placeHolder: "")
        Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 15, color: Color.c52_58_64, title: "MAKESURE", placeHolder: "")
        lblSubtitle.setLineSpacing(lineSpacing: 4.0)
        Fonts().set(object: self.btnGetStarted, fontType: 1, fontSize: 14, color: Color.white, title: "GETSTARTED", placeHolder: "")
        
        Fonts().set(object: self.lblPhotoID, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: "PHOTOID", placeHolder: "")
        lblPhotoID.setLineSpacing(lineSpacing: 4.0)
        Fonts().set(object: self.lblKraPin, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: "KRAPIN1", placeHolder: "")
        lblKraPin.setLineSpacing(lineSpacing: 4.0)
        Fonts().set(object: self.lblSelfie, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: "SELFIE", placeHolder: "")
        lblSelfie.setLineSpacing(lineSpacing: 4.0)
        Fonts().set(object: self.lblSignature, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: "SIGNATURE", placeHolder: "")
        lblSignature.setLineSpacing(lineSpacing: 4.0)
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func getStarted(_ sender: UIButton) {
        let vc = GetStartedViewController.getVCInstance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
