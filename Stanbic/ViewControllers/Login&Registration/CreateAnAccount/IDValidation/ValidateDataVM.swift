//
//  ValidateDataVM.swift
//  Stanbic
//
//  Created by Vijay Patidar on 19/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation
import UIKit

protocol ValidateDataVMDelegate: class {
    func reloadData(msg:String)
}

class ValidateDataVM {
    
    var responseData : JSON?
    weak var delegate : ValidateDataVMDelegate!
    
    func validateData(action : String, idNo : String, idType : String, imgArray : [UIImage]) {
        
       let loader = RestAPIManager.addProgress(title: "PLEASEWAIT", description: "WEAREPROCESSINGYOURREQUEST")
        
        RestAPIManager.validateData(title: "PLEASEWAIT", subTitle: "VALIDDATEDETAIL", action: action, idNo: idNo, idType: idType, images: imgArray) { (json) in
            DispatchQueue.main.async {
               loader.remove()
            }
            if json["STATUS_CODE"].intValue == 200 {
                DispatchQueue.main.async {
                    self.responseData = json
                    self.delegate.reloadData(msg: "")
                }
            }
            else {
              self.delegate.reloadData(msg: json["STATUS_MESSAGE"].stringValue)
            }
        }
    }
}
