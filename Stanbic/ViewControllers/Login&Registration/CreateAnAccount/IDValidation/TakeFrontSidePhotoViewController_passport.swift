//
//  TakeFrontSidePhotoViewController_passport.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 13/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class TakeFrontSidePhotoViewController_passport: UIViewController {

    //Mark:- IBOUTLETS
    @IBOutlet weak var lblStep1: UILabel!
    @IBOutlet weak var lblStep2: UILabel!
    @IBOutlet weak var imgStep1: UIImageView!
    @IBOutlet weak var imgStep2: UIImageView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblphotoheading: UILabel!
    @IBOutlet weak var imgFrontPic: UIImageView!
    @IBOutlet weak var imgFrontPic1: UIImageView!
    
    @IBOutlet weak var Viewimages: UIView!
    @IBOutlet weak var btnCountinueThisPhoto: UIButton!
    @IBOutlet weak var btnTakePhoto: UIButton!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    //Mark:- Varibles
    var imgFront : UIImage?
    var imgBack : UIImage?
    let model = ValidateDataVM()
    var idNo = ""
    var idtype = ""
    let modelfetch = GetStartedVM()
    var imgFrontscreenshot = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        
        //REGISTER  LISTNER FOR OBSERVE DOCUMENT DATA
        NotificationCenter.default.addObserver(self, selector:#selector(self.receiveDocumentData(notification:)), name:Notification.Name(NotificationID().document), object: nil)
        btnCountinueThisPhoto.isHidden = true
        
        //   model.delegate = self
         //model.validateData(action: "VALIDATE_ID", idNo: idNo, idType: idtype, imgArray: [UIImage]())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if imgFront != nil {
            Fonts().set(object: btnTakePhoto, fontType: 1, fontSize: 14, color: Color.c0_123_255, title: "RETAKEPHOTO", placeHolder: "")
            
            btnCountinueThisPhoto.isHidden = false
            btnCountinueThisPhoto.roundedButton()
            btnTakePhoto.roundedButtonBorder()
            btnTakePhoto.layer.borderWidth = 1.0
            btnTakePhoto.layer.borderColor = Color.c207_211_214.cgColor
            //   btnTakePhoto.backgroundColor = Color.white
        }
        else {
            btnCountinueThisPhoto.isHidden = true
            
        }
    }
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lblStep1, fontType: 1, fontSize: 11, color: Color.white, title: "1", placeHolder: "")
        Fonts().set(object: self.lblStep2, fontType: 1, fontSize: 11, color: Color.c52_58_64, title: "2", placeHolder: "")
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 20, color: Color.c10_34_64, title: "FRONTSIDE", placeHolder: "")
        Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 13, color: Color.c52_58_64, title: "FRONTSIDEBG", placeHolder: "")
        Fonts().set(object: self.lblphotoheading, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "TAKEAPHOTO", placeHolder: "")
        lblSubtitle.setLineSpacing(lineSpacing: 4.0)
        lblSubtitle.textAlignment = .center
        Fonts().set(object: btnTakePhoto, fontType: 1, fontSize: 14, color: Color.white, title: "TAKEPHOTO", placeHolder: "")
        //        btnTakePhoto.roundedButtonBorder()
        //        btnTakePhoto.layer.borderColor = Color.c207_211_214.cgColor
        Fonts().set(object: btnCountinueThisPhoto, fontType: 1, fontSize: 14, color: Color.white, title: "COUNTINUETHISPHOTO", placeHolder: "")
        
        lblStep1.backgroundColor = Color.c10_34_64
        lblStep1.layer.masksToBounds = true
        lblStep1.layer.cornerRadius = lblStep2.frame.height/2
        
        lblStep2.layer.masksToBounds = true
        lblStep2.layer.cornerRadius = lblStep2.frame.height/2
        lblStep2.layer.borderWidth = 2
        lblStep2.layer.borderColor = Color.c134_142_150.cgColor
        
        imgStep1.backgroundColor = Color.c134_142_150
        imgStep2.backgroundColor = Color.c134_142_150
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    
    
    //MARK :- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func CountinueThisPic(_sender: UIButton) {
        
           // gotoNextVC()
 //        global.imgCount = "1"
//            //Remove notification
//            model.delegate = self
//            NotificationCenter.default.removeObserver(self)
          validateIDData()
      
    }
    
    @IBAction func takephoto(_sender: UIButton) {
        
        let vc = CameraViewController.getVCInstance() as! CameraViewController
        vc.isDocumentPhoto = true
        vc.isShowBackCamera = true
        let appLanguage = Session.sharedInstance.appLanguage
        if _sender.titleLabel?.text == "RETAKEPHOTO".localized(appLanguage as String) && imgBack == nil {
            vc.imageType = "FRONT"
        }
        else {
            vc.imageType = imgFront == nil ? "FRONT" : "BACK"
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension TakeFrontSidePhotoViewController_passport {
    
    //Lister observer  method
    @objc func receiveDocumentData(notification : NSNotification) {
        if let docData = notification.userInfo as? [String : Any] {
            let imgType = docData["IMAGETYPE"] as? String
            let img = docData["IMAGE"] as? UIImage
            if imgType == "FRONT" {
                imgFront = img
            }
            else {
                imgBack = img
            }
            lblphotoheading.isHidden = true
            imgFrontPic1.image = img
          imgFrontscreenshot = imgFrontPic1.takscreenshot()
           updateUI(imageType : imgType ?? "")
        }
    }
    
    func updateUI(imageType : String) {
        
        if imageType == "FRONT" {
            lblStep1.backgroundColor = Color.c0_173_108
            imgStep1.backgroundColor = lblStep1.backgroundColor
        }
        else {
            lblStep1.backgroundColor = Color.c0_173_108
            imgStep1.backgroundColor = lblStep1.backgroundColor
            lblStep2.backgroundColor = Color.c0_173_108
            imgStep2.backgroundColor = lblStep1.backgroundColor
            lblStep2.layer.borderWidth = 0.0
            lblStep2.textColor = Color.white
            Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 20, color: Color.c10_34_64, title: "BACKSIDE", placeHolder: "")
        }
    }
}

//Call API
extension TakeFrontSidePhotoViewController_passport : ValidateDataVMDelegate {
    
    
    func validateIDData() {
        model.delegate = self
        //gotoNextVC()
       
        model.validateData(action: "VALIDATE_ID", idNo: idNo, idType: idtype, imgArray: [imgFrontscreenshot])
        
//        let vc = demoViewController.getVCInstance() as! demoViewController
//        vc.img = imgFrontscreenshot
//        self.present(vc, animated: true, completion: nil)
        
    }
    
    func reloadData(msg: String) {
        
        if msg == ""{
            if model.responseData?["STATUS_CODE"].intValue == 200 {
                let dict = model.responseData?["RESULT_ARRAY"].dictionaryValue
                global.token = dict!["TOKEN"]!.stringValue
                fetchAccountOpeningForm(token: dict!["TOKEN"]!.stringValue)
                gotoNextVC()
            }
            else {
                self.showMessage(title: "", message: model.responseData?["STATUS_MESSAGE"].stringValue ?? "Error")
            }
        }
        else {
            self.showMessage(title: "", message: msg)
        }
    }
    
    
    func gotoNextVC() {
        //Goto next vc
        let vc = ReadySelfiViewController.getVCInstance() as! ReadySelfiViewController
        vc.isselfi = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//Fetch account opening form
extension TakeFrontSidePhotoViewController_passport : GetStartedVMDelegate {
    
    func fetchAccountOpeningForm(token : String) {
        modelfetch.delegate = self
        modelfetch.getData(action: "FETCH", token: token)
    }
    
    func reloadData() {
        
        if modelfetch.message != nil {
            self.showMessage(title: "", message: modelfetch.message!)
        }
        else {
            OnboardUser.occupationalData = modelfetch.OccupationalData
            OnboardUser.kinData = modelfetch.kinData
            gotoNextVC()
        }
    }
}


extension UIImageView {
    
    func takscreenshot() ->UIImage{
        UIGraphicsBeginImageContext(self.frame.size)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(img!, nil, nil, nil)
        return img!
    }
}
extension UIView {
    
    func takeScreenshot() -> UIImage {
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
}
