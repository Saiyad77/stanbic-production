//
//  OccupationalDetailsViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 19/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class OccupationalDetailsViewController: UIViewController {

    //Mark:- IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    
    //Variables:
    private var lastContentOffset: CGFloat = 0
    var occupData = [String : Any]()
    var dataOccu = OnboardUser.occupationalData
    var willShowError : Bool = false
    
    override func viewDidLoad() {
        //Set Email in dictionary
        occupData["EMAIL"] = OnboardUser.userEmail
        super.viewDidLoad()
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        setFonts()
    }
    // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "OCCUPATIONALDETAILS", placeHolder: "")
        lblTitle.setLineSpacing(lineSpacing: 4.0)
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        print(occupData)
        self.view.endEditing(true)
        willShowError = true
        if checkError() {
            tblView.reloadData()
        } else {
           gotoNextVC()
        }
    }
    
    func gotoNextVC() {
        let vc = KinInformationViewController.getVCInstance() as! KinInformationViewController
        vc.occupData = occupData
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

//TABLE VIEW
extension OccupationalDetailsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OnboardUser.occupationalData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! TextfieldTableViewCell
        cell.textFieldData = OnboardUser.occupationalData
        let data = OnboardUser.occupationalData[indexPath.row]
        cell.willShowError = willShowError
        cell.initiateValue(data: data, indexPath: indexPath)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
}

//Text field delegate

extension OccupationalDetailsViewController {
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
        
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

extension OccupationalDetailsViewController : PickerViewData {
    
    func selectedData(data : [String : String]?, valueType : String?, next_of_kin: String?) {
                
        if valueType == "EMPLOYMENT_STATUS" || valueType == "INDUSTRY" || valueType == "INCOME"  || valueType == "KINMOBILENO"  {
            
            removeError(valueType: valueType ?? "")
            if let occupaionalData = occupData["OCCUPATION"] as? [String : String] {
                if let _ = occupaionalData["\(valueType ?? "")"] {
                    var dtaValue = occupData["OCCUPATION"] as! [String : String]
                    dtaValue["\(valueType ?? "")"] = data?[""]
                    occupData["OCCUPATION"] = dtaValue
                } else {
                    var dict = occupData["OCCUPATION"] as! [String : String]
                    dict["\(valueType ?? "")"] = data?["VALUE"]
                    occupData["OCCUPATION"] = dict
                }
            } else {
                let dict = [
                    valueType : data?["VALUE"]
                ]
                occupData["OCCUPATION"] = dict
            }
            
        } else if valueType == "BRANCHES" {
            removeError(valueType: valueType ?? "")
            occupData["BRANCH"] = data?["VALUE"]
        } else if valueType == "WHERELIVE" {
            if data?["VALUE"] != "" {
                removeError(valueType: valueType ?? "")
            } else {
                addError(valueType: valueType ?? "")
            }
            occupData["RESIDENCE"] = data?["VALUE"]
        }  else  if valueType == "KINMOBILENO" {
            if data?["VALUE"] != "" {
                removeError(valueType: valueType ?? "")
            } else {
                addError(valueType: valueType ?? "")
            }
            occupData["KINMOBILENO"] = data?["VALUE"]
        }
        
    }
    
    func removeError(valueType : String) {
        
        for i in 0 ..< dataOccu.count {
            let dataOccuType = dataOccu[i]["TITLE"] as? String
            if dataOccuType == valueType {
                dataOccu[i]["ERROR"] = ""
                OnboardUser.occupationalData = dataOccu
                break
            }
        }
    }
    
    func addError(valueType : String) {
        
        for i in 0 ..< dataOccu.count {
            let dataOccuType = dataOccu[i]["TITLE"] as? String
            if dataOccuType == valueType {
                dataOccu[i]["ERROR"] = valueType
                OnboardUser.occupationalData = dataOccu
                break
            }
        }
    }
    
    func checkError() -> Bool {
        
        for i in 0 ..< dataOccu.count {
            
            
            if (dataOccu[i]["ERROR"] as! String) != "" {
                return true
            }
        }
        return false
    }
}

extension OccupationalDetailsViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
             //self.view.endEditing(true)
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}

