//
//  TextfieldTableViewCell.swift
//  Stanbic
//
//  Created by Vijay Patidar on 20/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

protocol PickerViewData: class {
    func selectedData(data : [String : String]?, valueType : String?, next_of_kin: String?)
}

class TextfieldTableViewCell: UITableViewCell {
    
    //Mark:- IBOutet
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var imgDropDown: UIImageView!
    
    //Mark:- Variables
    var textFieldData = [[String : Any]]()
    var delegate : PickerViewData!
    var pickerView: UIPickerView!
    var pickerValue = [[String : String]]()
    
     var pickerValue1 = [[String : AnyObject]]()
    var willShowError = false
    var EMPLOYMENT_STATUS = false

    override func awakeFromNib() {
        super.awakeFromNib()
        txtField.delegate = self
        Fonts().set(object: txtField, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        initialisationPicker()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initiateValue(data : [String : Any], indexPath : IndexPath) {
        
        let placeholder = data["PLACEHOLDER"] as? String
        let dataType = data["TITLE"] as? String
        if dataType == "WHERELIVE" || dataType == "FULLNAME" || dataType == "KINIDNUMBER" {
            imgDropDown.isHidden = true
            txtField.keyboardType = .default
            txtField.inputView = nil
        }
        else if dataType == "KINMOBILENO" {
            imgDropDown.isHidden = true
            txtField.keyboardType = .numberPad
            txtField.inputView = nil
        }
        
        else { // SAIYAD
            if dataType  != "EMPLOYMENT_STATUS" {
            txtField.inputView = pickerView
            imgDropDown.isHidden = false
            }
        }
        txtField.restorationIdentifier = dataType
        if (data["ERROR"] as! String) != "" && willShowError == true  {
 
            txtField.showError(errStr: data["ERROR"] as! String)
        }
        else if txtField.text != "" {
            txtField.didBegin()
        }
        else{
            txtField.addUI(placeholder: placeholder ?? "")
        }
    }

}

extension TextfieldTableViewCell : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let dataType = textField.restorationIdentifier
        
        let data = textFieldData
        for i in 0 ..< data.count {
            var type = data[i]["TITLE"] as? String
            if dataType == type {
                if dataType == "EMPLOYMENT_STATUS" {
                    pickerValue1 = OnboardUser.empStatusData
                    EMPLOYMENT_STATUS = true
                    global.pickerValueindustry.removeAll()
                }
                if dataType == "INDUSTRY" {
                   // print(dataType)
                    global.type = "INDUSTRY"
                }
                else {
                  global.type = "SALARY"
                let result = data[i]["RESULT"] as! [[String : String]]
                pickerValue = result
                }
            }
        }
        textField.removeError()
        textField.didBegin()
    }
    // SAIYAD
    func textFieldDidEndEditing(_ textField: UITextField) {
        let txtID = textField.restorationIdentifier
        if txtID == "WHERELIVE" || txtID == "FULLNAME" || txtID == "KINIDNUMBER" || txtID == "KINMOBILENO"  {

            let dict1 = [
                "ID" : "0",
                "VALUE" : txtField.text!
                ] as [String : Any]

            delegate.selectedData(data: dict1 as? [String : String] , valueType: txtField.restorationIdentifier ?? "", next_of_kin: "")


        }
       textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
}

extension TextfieldTableViewCell : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func initialisationPicker() {
        pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        txtField.inputView = pickerView
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if  EMPLOYMENT_STATUS == true  {
         return pickerValue1.count
        }else {
            if global.type == "SALARY" {
        return pickerValue.count
            }
            else {
                return global.pickerValueindustry.count
            }
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         if  EMPLOYMENT_STATUS == true  {
            let dict = pickerValue1[row]
            let name = dict["NAME"]
            return "\(name!)"
        }else {
             if global.type == "SALARY" {
             return pickerValue[row]["VALUE"]
            }
             else{
              return global.pickerValueindustry[row]["VALUE"]
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if  EMPLOYMENT_STATUS == true  {
            let dict = pickerValue1[row]
            let dictindustry = dict["INDUSTRIES"] as AnyObject
            print("xxxxxxxxx---\(dictindustry as AnyObject)")
            
          let arr = String(describing: dict["INDUSTRIES"]!)
            print(arr)
            
//            let datar = arr.toJSON()
//             print(datar)
            
            
            
            let satr = convertToDictionary1(text: arr)
            print(satr)
            
            
            for (key, value) in satr!{
                let dict = ["ID":key,"VALUE":value] as [String : Any]
                print(dict)
                global.pickerValueindustry.append(dict as! [String : String])
             }
            
//            if let data = try? JSONSerialization.data(withJSONObject: arr, options: .prettyPrinted),
//                let str = String(data: data, encoding: .utf8) {
//                print(str)
//            }
            
//            for i in 0..<arr.count {
//                let arrstr = String(describing: arr[i]).components(separatedBy: ":")
//                let dict = ["ID":arrstr[0],"VALUE":arrstr[1]]
//                print(dict)
//                pickerValue.append(dict)
//            }
//            print(pickerValue)
          
            
            let ftOfTime = dict["NAME"]
             let ft_of_time = String(describing: ftOfTime!)
                txtField.text! = ft_of_time
           let dict1 = [
                "ID" : "0",
                "VALUE" : ft_of_time
                ] as [String : Any]
            delegate.selectedData(data: dict1 as? [String : String] , valueType: txtField.restorationIdentifier ?? "", next_of_kin: "")
            
           
            
        } else {
            if global.type == "SALARY" {
        txtField.text = pickerValue[row]["VALUE"]
            delegate.selectedData(data: pickerValue[row], valueType: txtField.restorationIdentifier ?? "", next_of_kin: "")
                
            }
            else{
                txtField.text = global.pickerValueindustry[row]["VALUE"]
                delegate.selectedData(data: global.pickerValueindustry[row], valueType: txtField.restorationIdentifier ?? "", next_of_kin: "")
            }
        }
    }
    
    func convertToDictionary1(text: String) -> [String: AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            } catch {
            }
        }
        return nil
    }
}
extension String {
    
    func toJSON() -> Any? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
}
