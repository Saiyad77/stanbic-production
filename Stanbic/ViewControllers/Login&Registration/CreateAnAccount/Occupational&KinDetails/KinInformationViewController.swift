//
//  KinInformationViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 20/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class KinInformationViewController: UIViewController {

    //Mark:- IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    
    //Variables:
    private var lastContentOffset: CGFloat = 0
    var occupData = [String : Any]()
    var dataOccu = OnboardUser.kinData
    var willShowError : Bool = false
    var kinData = [String : String]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        setFonts()
    }
        // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "KININFO", placeHolder: "")
        lblTitle.setLineSpacing(lineSpacing: 4.0)
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        self.view.endEditing(true)
        willShowError = true
        if checkError() {
            tblView.reloadData()
        }
        else {
            
            occupData["NEXT_OF_KIN"] = kinData
            gotoNextVC()
        }
    }
    
    func gotoNextVC() {
        let vc = ReviewDetailsViewController.getVCInstance() as! ReviewDetailsViewController
        vc.occupData = occupData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//TABLE VIEW
extension KinInformationViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OnboardUser.kinData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! TextfieldTableViewCell
        cell.textFieldData = OnboardUser.kinData
        let data = OnboardUser.kinData[indexPath.row]
        cell.willShowError = willShowError
        cell.initiateValue(data: data, indexPath: indexPath)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
}

//Text field delegate
extension KinInformationViewController {
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
        
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

extension KinInformationViewController : PickerViewData {
    
    
    func selectedData(data : [String : String]?, valueType : String?, next_of_kin: String?) {
        if valueType == "NEXT_OF_KIN"  {
            removeError(valueType: valueType ?? "")
            kinData["RELATIONSHIP"] = data?["ID"]
        }
        else if valueType == "FULLNAME" {
            if data?["VALUE"] != "" {
                removeError(valueType: valueType ?? "")
            }
            else {
                addError(valueType: valueType ?? "")
            }
            kinData["NAME"] = data?["VALUE"]
        }
        else if valueType == "KINIDNUMBER" {
            if data?["VALUE"] != "" {
                removeError(valueType: valueType ?? "")
            }
            else {
                addError(valueType: valueType ?? "")
            }
            kinData["ID_NUMBER"] = data?["VALUE"]
        }
        else if valueType == "KINMOBILENO" {
            if data?["VALUE"] != "" {
                removeError(valueType: valueType ?? "")
            }
            else {
                addError(valueType: valueType ?? "")
            }
            kinData["MSISDN"] = data?["VALUE"]
        }
    }
    
    func removeError(valueType : String) {
        
        for i in 0 ..< dataOccu.count {
            let dataOccuType = dataOccu[i]["TITLE"] as? String
            if dataOccuType == valueType {
                dataOccu[i]["ERROR"] = ""
                OnboardUser.kinData = dataOccu
                break
            }
        }
    }
    
    func addError(valueType : String) {
        
        for i in 0 ..< dataOccu.count {
            let dataOccuType = dataOccu[i]["TITLE"] as? String
            if dataOccuType == valueType {
                dataOccu[i]["ERROR"] = valueType
                OnboardUser.kinData = dataOccu
                break
            }
        }
    }
    
    func checkError() -> Bool {
        
        for i in 0 ..< dataOccu.count {
            if (dataOccu[i]["ERROR"] as! String) != "" {
                return true
            }
        }
        return false
    }
}

extension KinInformationViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            // self.view.endEditing(true)
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}


