//
//  FillPersonalDetailViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 19/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class FillPersonalDetailViewController: UIViewController {
    
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var txtkrapin: UITextField!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    
    // new
    @IBOutlet weak var txtplaceresidence: UITextField!
    @IBOutlet weak var txtpostalcode: UITextField!
    @IBOutlet weak var txtposteladdress: UITextField!
    @IBOutlet weak var txtbankbranch: UITextField!
    
    // Mark:- Variables
    private var lastContentOffset: CGFloat = 0
    var pickerView: UIPickerView!
    var pickerValue = [String]()
    var occupData = [String : Any]()
    
    let model = SetPersonalDetailVM()

    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        initialisationPicker()
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
        // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: self.lblTitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "FILLPERSONALDETAIL", placeHolder: "")
        Fonts().set(object: self.btnContinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        Fonts().set(object: txtemail, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: txtkrapin, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        lblTitle.setLineSpacing(lineSpacing: 3.0)
        txtemail.addUI(placeholder: "EMAILADDRESS1")
        txtkrapin.addUI(placeholder: "KRAPIN")
        
          Fonts().set(object: txtplaceresidence, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
          Fonts().set(object: txtpostalcode, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
          Fonts().set(object: txtposteladdress, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
          Fonts().set(object: txtbankbranch, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "SELECTBANKBRANCH", placeHolder: "")
        
        txtplaceresidence.addUI(placeholder: "RESIDENCE")
        txtpostalcode.addUI(placeholder: "POSTELCODE")
        txtposteladdress.addUI(placeholder: "POSTELADDRESS")
        let appLanguage = Session.sharedInstance.appLanguage
        txtbankbranch.text = "SELECTBANKBRANCH".localized(appLanguage as String)
        
      }
    
    //MARK :- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Countinue(_sender: UIButton) {
        let appLanguage = Session.sharedInstance.appLanguage
        
        if isValidEnput() {
            if txtplaceresidence.text == ""{
               txtplaceresidence.showError(errStr: "RESIDENCE")
            }
           else if txtpostalcode.text == ""{
                txtpostalcode.showError(errStr: "POSTELCODE")
            }
            else if txtposteladdress.text == ""{
                txtposteladdress.showError(errStr: "POSTELADDRESS")
            }
            else if txtbankbranch.text == ""{
                txtbankbranch.showError(errStr: "SELECTBANKBRANCH")
            }
            else if txtbankbranch.text == "SELECTBANKBRANCH".localized(appLanguage as String){
                txtbankbranch.showError(errStr: "SELECTBANKBRANCH")
            }else {
                
                OnboardUser.userEmail = txtemail.text ?? ""
                OnboardUser.userKraPIN = txtkrapin.text ?? ""
                OnboardUser.userplaceresidence = txtplaceresidence.text ?? ""
                OnboardUser.userbank = txtbankbranch.text ?? ""
                OnboardUser.userpostalcode = txtpostalcode.text ?? ""
                OnboardUser.userPostaladdress = txtposteladdress.text ?? ""
                // SAIYAD
                self.view.endEditing(true)
                submitData()
               // gotoNextVC()
                
                
            }
         }
    }
    
    func isValidEnput() -> Bool {
        if isValidEmail(emailStr: txtemail.text ?? "") {
            if txtkrapin.text != "" {
                return true
            }
            else {
                txtkrapin.showError(errStr: "KRAPIN")
                return false
            }
        }
        else {
            txtemail.showError(errStr: "EMAILERROR")
            return false
        }
    }
    
    func gotoNextVC() {
        let v = self.navigationController?.viewControllers
        for i in (v)!
        {
            if i is ReadySelfiViewController
            {
                if let vc = i as? ReadySelfiViewController {
                    vc.isselfi = false
                }
                self.navigationController?.popToViewController(i, animated: true)
                break
            }
        }
    }
}

// MARK:- Textfield
//Text field delegate
extension FillPersonalDetailViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
        
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

extension FillPersonalDetailViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func initialisationPicker() {
        pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        txtbankbranch.inputView = pickerView
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return OnboardUser.branchData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dict = OnboardUser.branchData[row]
       return dict["VALUE"]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let dict = OnboardUser.branchData[row]
        OnboardUser.userbankID = dict["ID"] ?? ""
        OnboardUser.userbank = dict["VALUE"] ?? ""
        txtbankbranch.text = dict["VALUE"] ?? ""
     }
}

extension FillPersonalDetailViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            // self.view.endEditing(true)
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
    func getValuefrom(id : String, dataType : String, data : [[String : Any]]) -> String {
        for i in 0 ..< data.count {
            let dataValue = data[i]["TITLE"] as! String
            if dataType == dataValue {
                if let result = data[i]["RESULT"] as? [[String : String]] {
                    for j in 0 ..< result.count {
                        let idValue = result[j]["ID"]
                        if idValue == id {
                            return result[j]["VALUE"] ?? ""
                        }
                    }
                }
            }
        }
        
        return ""
    }
}

extension FillPersonalDetailViewController : SetPersonalDetailVMDelegate  {
    
    func reloadData(msg: String) {
        if msg == ""{
            if model.responseData?["STATUS_CODE"].intValue == 200 {
                gotoNextVC()
            }
            else {
                self.showMessage(title: "", message: model.responseData?["STATUS_MESSAGE"].stringValue ?? "Error")
            }
        }
        else {
            self.showMessage(title: "", message: msg)
        }
    }
    
    
    func submitData() {
    model.delegate = self
    
    model.validatePersonalDetail(action: "PERSONAL_DETAILS", imgArr: [global.imgselfi],idNumber : global.idnumber,token : global.token,branch : txtbankbranch.text!,country :txtplaceresidence.text!,postalcode : txtpostalcode.text!,postaladdress : txtposteladdress.text!)
    }
    


func reloadData() {
    if model.responseData?["STATUS_CODE"].intValue == 200 {
        self.gotoNextVC()
    }
    else {
        self.showMessage(title: "", message: model.responseData?["STATUS_MESSAGE"].stringValue ?? "Error")
    }
   }
}

