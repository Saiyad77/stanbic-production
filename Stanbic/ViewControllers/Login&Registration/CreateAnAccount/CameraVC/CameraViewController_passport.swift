//
//  CameraViewController_passport.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 13/06/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class CameraViewController_passport: UIViewController {

    //Mark:- IBOutlets
    @IBOutlet weak var viewPreview: UIView!
    @IBOutlet weak var viewPreviewSelfie: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblPhotoTitle: UILabel!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var btnRotateCamera: UIButton!
    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    
    //Mark:- Variables
    let cameraController = CameraController()
    var isShowBackCamera = true
    var isDocumentPhoto = true
    var imageType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        setFont()
        configureCameraController()
        
        if isDocumentPhoto {
            viewPreview.isHidden = false
            viewPreviewSelfie.isHidden = true
        }
        else {
            viewPreview.isHidden = true
            viewPreviewSelfie.isHidden = false
        }
    }
    
    func setFont() {
        if imageType == "FRONT" {
            Fonts().set(object: lblPhotoTitle, fontType: 1, fontSize: 15, color: Color.white, title: "CAMERAFROUNTSIDE", placeHolder: "")
            Fonts().set(object: lblWarning, fontType: 0, fontSize: 15, color: Color.white, title: "CAMERAPOSITION", placeHolder: "")
            Fonts().set(object: lblHeader, fontType: 1, fontSize: 17, color: Color.white, title: "CAMERAPHOTOID", placeHolder: "")
        }
        else if imageType == "BACK" {
            Fonts().set(object: lblPhotoTitle, fontType: 1, fontSize: 15, color: Color.white, title: "CAMERABACKSIDE", placeHolder: "")
            Fonts().set(object: lblWarning, fontType: 0, fontSize: 15, color: Color.white, title: "CAMERAPOSITIONBACK", placeHolder: "")
            Fonts().set(object: lblHeader, fontType: 1, fontSize: 17, color: Color.white, title: "CAMERAPHOTOID", placeHolder: "")
        }
        else if imageType == "SELFIE" {
            
            //this label will be hide
            Fonts().set(object: lblPhotoTitle, fontType: 1, fontSize: 15, color: Color.white, title: "", placeHolder: "")
            Fonts().set(object: lblWarning, fontType: 0, fontSize: 15, color: Color.white, title: "", placeHolder: "")
            //this label will be hide
            
            Fonts().set(object: lblHeader, fontType: 1, fontSize: 17, color: Color.white, title: "TakeSELFI", placeHolder: "")
        }
            
        else if imageType == "SIGNATURE" {
            
            //this label will be hide
            Fonts().set(object: lblPhotoTitle, fontType: 1, fontSize: 15, color: Color.white, title: "", placeHolder: "")
            Fonts().set(object: lblWarning, fontType: 0, fontSize: 15, color: Color.white, title: "", placeHolder: "")
            //this label will be hide
            
            Fonts().set(object: lblHeader, fontType: 1, fontSize: 17, color: Color.white, title: "TAKESIGNATURE", placeHolder: "")
            //  backBtn.setImage(UIImage(named: "back_arrow"), for: .normal)
            
        }
        
        lblWarning.setLineSpacing(lineSpacing: 4)
        lblWarning.textAlignment = .center
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewPreview.layer.cornerRadius = 4
        viewPreview.clipsToBounds = true
        viewPreviewSelfie.layer.cornerRadius = 4
        viewPreviewSelfie.clipsToBounds = true
    }
    
    static func getVCInstance() -> UIViewController {
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    //Mark:- Custom Action method
    @IBAction func takeSelfie(_ sender: Any) {
        cameraController.captureImage {(image, error) in
            guard let image = image?.fixedOrientation() else {
                self.navigationController?.popViewController(animated: true)
                return
            }
            if self.imageType == "FRONT" || self.imageType == "BACK" {
                // Post a notification
                let documentData: [String : Any] = ["IMAGE" : image, "IMAGETYPE" : self.imageType]
                NotificationCenter.default.post(name: Notification.Name(NotificationID().document), object: nil, userInfo: documentData)
                
                self.navigationController?.popViewController(animated: true)
            }
            else if self.imageType == "SELFIE" {
                let vc = TakeSelfiViewController.getVCInstance() as! TakeSelfiViewController
                vc.imgObj = image
                
            global.imgselfi = image
                
                vc.isselfi = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if self.imageType == "SIGNATURE" {
                let vc = TakeSelfiViewController.getVCInstance() as! TakeSelfiViewController_passport
                vc.imgObj1 = image
                global.imgsignature = image
                
                vc.isselfi = false
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func rotateCamera(_ sender: Any) {
        do {
            try cameraController.switchCameras()
        }
            
        catch {
           
        }
        
        switch cameraController.currentCameraPosition {
        case .some(.front):
            btnFlash.isHidden = false
            
        case .some(.rear):
            btnFlash.isHidden = true
            
        case .none:
            return
        }
    }
    @IBAction func flashLight(_ sender: Any) {
        if cameraController.flashMode == .on {
            cameraController.flashMode = .off
        }
            
        else {
            cameraController.flashMode = .on
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension CameraViewController_passport {
    
    func configureCameraController() {
        cameraController.prepare {(error) in
            if let error = error {
             }
            self.cameraController.currentCameraPosition = self.isShowBackCamera ? .front : .rear
            try? self.cameraController.displayPreview(on: self.isDocumentPhoto ? self.viewPreview : self.viewPreviewSelfie)
            try? self.cameraController.switchCameras()
            if self.cameraController.currentCameraPosition != .rear {
                self.btnFlash.isHidden = true
            }
            else {
                self.btnFlash.isHidden = false
            }
        }
    }
}

