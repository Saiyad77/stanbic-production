//
//  ReviewDetailsTableViewCell.swift
//  Stanbic
//
//  Created by Vijay Patidar on 20/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class ReviewDetailsTableViewCell: UITableViewCell {

    //Mark:- IBOutet
    //CELL
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    //HEADER
    @IBOutlet weak var lblHeader: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if self.reuseIdentifier == "CELL" {
            Fonts().set(object: lblPlaceholder, fontType: 1, fontSize: 9, color: Color.c134_142_150, title: "", placeHolder: "")
            Fonts().set(object: lblValue, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initiateData(data : [String : String]) {
        lblPlaceholder.text = data["PLACEHOLDER"] ?? ""
        lblValue.text = data["VALUE"] ?? ""
    }

}
