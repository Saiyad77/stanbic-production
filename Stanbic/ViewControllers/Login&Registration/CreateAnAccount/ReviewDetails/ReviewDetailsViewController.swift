//
//  ReviewDetailsViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 20/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class ReviewDetailsViewController: UIViewController {
    
    //MARK:- IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    
    //Variables
    var occupData = [String : Any]()
    var reviewData = [[String : Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setFonts()
        sortData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
        // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "REVIEWDETAILS", placeHolder: "")
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONFIRM", placeHolder: "")
        
    }
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func countinue(_ sender: AttributedButton) {
                view.endEditing(true)
        submitData()
    }
}

//TABLE VIEW
extension ReviewDetailsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return reviewData.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "HEADER") as! ReviewDetailsTableViewCell
        let headerData = reviewData[section]["HEADER"] as! String
        Fonts().set(object: header.lblHeader, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: headerData, placeHolder: "")
        header.backgroundColor = Color.white
        return header
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let valueData = reviewData[section]["VALUE"] as? [[String : String]] {
            return valueData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! ReviewDetailsTableViewCell
        let data = reviewData[indexPath.section]["VALUE"] as! [[String : String]]
       cell.initiateData(data: data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
}

extension ReviewDetailsViewController {
    
    func sortData() {
        var reviewDict = [String : Any]()
        var reviewArr = [[String : String]]()
        
        
        
        //Presonal detail
        reviewDict["HEADER"] = "PERSONAL DETAILS"
        if let _ = occupData["EMAIL"] as? String {
            let dict = [
                "PLACEHOLDER" : "EMAILADDRESS".localized(Session.sharedInstance.appLanguage),
                "VALUE" : occupData["EMAIL"] as! String
            ]
            reviewArr.append(dict)
            
        }
        reviewDict["VALUE"] = reviewArr
        reviewData.append(reviewDict)
        reviewArr.removeAll()
        
        //Occupation data
        reviewDict["HEADER"] = "OCCUPATION DETAILS"
        if let occupData = occupData["OCCUPATION"] as? [String : String] {
            for (key, value) in occupData {
                if key == "EMPLOYMENT_STATUS" {
                    let dict = [
                        "PLACEHOLDER" : "OCCUPATIONSTATUS1".localized(Session.sharedInstance.appLanguage),
                        "VALUE" : value
                    ]
                    reviewArr.append(dict)
                }
                else if key == "INDUSTRY" {
                    let dict = [
                        "PLACEHOLDER" : "JOBINDUSTRY".localized(Session.sharedInstance.appLanguage),
                        "VALUE" : value
                    ]
                    reviewArr.append(dict)
                }
                else if key == "INCOME" {
                    let dict = [
                        "PLACEHOLDER" : "GROSSSALARY1".localized(Session.sharedInstance.appLanguage),
                        "VALUE" : value
                    ]
                    reviewArr.append(dict)
                }
            }
            
        }
        if let _ = occupData["RESIDENCE"] as? String {
            let dict = [
                "PLACEHOLDER" : "MYADDRESS".localized(Session.sharedInstance.appLanguage),
                "VALUE" : occupData["RESIDENCE"] as! String
            ]
            reviewArr.append(dict)
            
        }
        if let branchID = occupData["BRANCH"] as? String {
            let dict = [
                "PLACEHOLDER" : "PREFFEREDBRANCH".localized(Session.sharedInstance.appLanguage),
                "VALUE" : getValuefrom(id: branchID, dataType: "BRANCHES", data: OnboardUser.occupationalData)
            ]
            reviewArr.append(dict)
            
        }
        
        reviewDict["VALUE"] = reviewArr
        reviewData.append(reviewDict)
        reviewArr.removeAll()
        
        //next of kin data
        reviewDict["HEADER"] = "NEXT OF KIN"
        if let occupData = occupData["NEXT_OF_KIN"] as? [String : String] {
            for (key, value) in occupData {
                if key == "RELATIONSHIP" {
                    let dict = [
                        "PLACEHOLDER" : "RELATIONSHIP".localized(Session.sharedInstance.appLanguage),
                        "VALUE" : getValuefrom(id: value, dataType: "NEXT_OF_KIN", data: OnboardUser.kinData)
                    ]
                    reviewArr.append(dict)
                }
                else if key == "NAME" {
                    let dict = [
                        "PLACEHOLDER" : "NAME".localized(Session.sharedInstance.appLanguage),
                        "VALUE" : value
                    ]
                    reviewArr.append(dict)
                }
                else if key == "MSISDN" {
                    let dict = [
                        "PLACEHOLDER" : "MSISDN".localized(Session.sharedInstance.appLanguage),
                        "VALUE" : value
                    ]
                    reviewArr.append(dict)
                }
                else if key == "ID_NUMBER" {
                    let dict = [
                        "PLACEHOLDER" : "ID_NUMBER".localized(Session.sharedInstance.appLanguage),
                        "VALUE" : value
                    ]
                    reviewArr.append(dict)
                }
            }
            
        }
        reviewDict["VALUE"] = reviewArr
        reviewData.append(reviewDict)
        tblView.reloadData()
    }
    
    func getValuefrom(id : String, dataType : String, data : [[String : Any]]) -> String {
        for i in 0 ..< data.count {
            let dataValue = data[i]["TITLE"] as! String
            if dataType == dataValue {
                if let result = data[i]["RESULT"] as? [[String : String]] {
                    for j in 0 ..< result.count {
                        let idValue = result[j]["ID"]
                        if idValue == id {
                            return result[j]["VALUE"] ?? ""
                        }
                    }
                }
            }
        }
        
        return ""
    }
}

//Call API
extension ReviewDetailsViewController {
    
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
        }
    }
    
    func submitData() {
        
        let loader = RestAPIManager.addProgress(title: "PLEASEWAIT", description: "SUBMITTINGDETAILS")
        RestAPIManager.submitOccupationl_KraDetail(title: "PLEASEWAIT", subTitle: "SUBMITTINGDETAILS", action: "SUBMIT", dataDict: occupData) { (json) in
            print(json)
            DispatchQueue.main.async {
                loader.remove()
                if json["STATUS_CODE"].intValue == 200 {
                    self.gotoNextVC(message: json["STATUS_MESSAGE"].stringValue )
                }
                else {
                    self.showMessageAndBack(title: "", message: json["STATUS_MESSAGE"].stringValue )
                }
               
            }
        }
       
        
//        RestAPIManager.submitOccupationl_KraDetail(title: "PLEASEWAIT", subTitle: "SUBMITTINGDETAILS", action: "SUBMIT", dataDict: occupData, onCompletion: { (response) in
//            DispatchQueue.main.async {
//                if response.statusCode == 200 {
//                    self.gotoNextVC(message: response.statusMessage ?? "")
//                }
//                else {
//                    self.showMessageAndBack(title: "", message: response.statusMessage ?? "")
//                }
//            }
//        }) { (error) in
//
//        }
    }
    
    func gotoNextVC(message : String) {
        let vc = DetailsSubmitedViewController.getVCInstance() as! DetailsSubmitedViewController
        vc.message = message
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
