//
//  DetailsSubmitedViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 21/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class DetailsSubmitedViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblFooter: UILabel!
    
    //Mark:- Variables
    var message : String?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setFonts()
        setValue()
    }
    

    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
        // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lblTitle, fontType: 1, fontSize: 22, color: Color.c0_173_108, title: "DETAILSSUBMIT", placeHolder: "")
        Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 17, color: Color.c52_58_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblFooter, fontType: 0, fontSize: 13, color: Color.c134_142_150, title: "FEELFREE", placeHolder: "")
        lblFooter.setLineSpacing(lineSpacing: 4.0)
        lblFooter.textAlignment = .center
    }
    
    func setValue() {
        lblSubtitle.text = message
        lblSubtitle.setLineSpacing(lineSpacing: 4.0)
        lblSubtitle.textAlignment = .center
    }

}
