//
//  GetStartedViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 19/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class GetStartedViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var btnCountinue: AttributedButton!
    @IBOutlet weak var txtIdType: UITextField!
    @IBOutlet weak var txtIdNo: UITextField!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var viewTextField: AttributedView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //Variables:-
    private var lastContentOffset: CGFloat = 0
    var pickerView: UIPickerView!
    var pickerValue = [String]()
    let placeholder = ["PASSPORTNO", "ALIENID", "NATIONALIDNO"]
    let idType = ["PASSPORT","ALIEN ID",  "ID"]
    let model = GetStartedVM()

    var idtype = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // fetchAccountOpeningForm()
        setFonts()
        initialisationPicker()
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
   
    // MARK:_SET FONT
    func setFonts() {

        Fonts().set(object: self.lblTitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "LETGETSTARTEDSMALL", placeHolder: "")
        Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 15, color: Color.c52_58_64, title: "ENTERID", placeHolder: "")
        lblSubtitle.setLineSpacing(lineSpacing: 4.0)
        Fonts().set(object: self.btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        
        Fonts().set(object: lblCountryCode, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "+254", placeHolder: "")
        Fonts().set(object: txtMobileNo, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "", placeHolder: "ENTERMOBPLACEHOLDER")
        Fonts().set(object: txtIdType, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: txtIdNo, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        txtIdType.addUI(placeholder: "SELECTIDTYPE")
        
        let mobileNo = OnboardUser.mobileNo.replacingOccurrences(of: "254", with: "")
        txtMobileNo.text = mobileNo
        
        txtIdNo.isHidden = true
        viewTextField.isHidden = true
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinue(_ sender: UIButton) {
          let appLanguage = Session.sharedInstance.appLanguage
        if isValid() {
            global.idnumber = txtIdNo.text ?? "0"
            self.view.endEditing(true)
            if txtIdType.text == "PASSPORT".localized(appLanguage as String) {
                let vc = TakeFrontSidePhotoViewController_passport.getVCInstance() as! TakeFrontSidePhotoViewController_passport
                vc.idNo = txtIdNo.text ?? "0"
                vc.idtype = idtype
                global.imgCount = "1"
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                let vc = TakeFrontSidePhotoViewController.getVCInstance() as! TakeFrontSidePhotoViewController
                vc.idNo = txtIdNo.text ?? "0"
                vc.idtype = idtype
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else {
            txtIdNo.showError(errStr: "EMPTYIDNO")
        }
    }
    
    func isValid() -> Bool {
        if txtIdNo.text == "" {
            return false
        }
        return true
    }
}



//Text field delegate
extension GetStartedViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
        
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

extension GetStartedViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func initialisationPicker() {
        let appLangauge = Session.sharedInstance.appLanguage
        pickerValue = ["PASSPORT".localized(appLangauge as String),"ALIENID".localized(appLangauge as String), "NATIONALID".localized(appLangauge as String)]
        pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        txtIdType.inputView = pickerView
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerValue.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerValue[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtIdType.text = pickerValue[row]
        idtype = idType[row]
        txtIdNo.isHidden = false
        viewTextField.isHidden = false
        txtIdNo.text = ""
        txtIdNo.addUI(placeholder: placeholder[row])
        
        if scrollView.contentSize.height > (btnCountinue.frame.origin.y + btnCountinue.frame.size.height) {
            //scroll for visible text field
            let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.size.height)
            scrollView.setContentOffset(bottomOffset, animated: true)
        }
        
    }
}


//Fetch account opening form
extension GetStartedViewController : GetStartedVMDelegate {

    func fetchAccountOpeningForm() {
        model.delegate = self
        model.getData(action: "FETCH", token: "")
    }

    func reloadData() {
        if model.message != nil {
            self.showMessage(title: "", message: model.message!)
        }
        else {
            OnboardUser.occupationalData = model.OccupationalData
            OnboardUser.kinData = model.kinData
        }
    }
}

extension GetStartedViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}
