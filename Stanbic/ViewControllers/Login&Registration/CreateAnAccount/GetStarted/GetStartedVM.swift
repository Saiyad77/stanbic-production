//
//  GetStartedVM.swift
//  Stanbic
//
//  Created by Vijay Patidar on 19/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation

protocol GetStartedVMDelegate: class {
    func reloadData()
}

// ViewModel for Country List
class GetStartedVM{
    
    var OccupationalData = [[String : Any]]()
    var kinData = [[String : Any]]()
    var message : String?
    weak var delegate : GetStartedVMDelegate!
    
    func getData(action : String,token : String) {
        RestAPIManager.getAccountOpeningData(title: "PLEASEWAIT", subTitle: "FETCHINGQUE", action: action, token: token, answer: [[String : String]]()) { (json) in
            
            
        DispatchQueue.main.async {
                if json["STATUS_CODE"].intValue != 200 {
                    self.message = json["STATUS_MESSAGE"].stringValue
                }
                else {
                    
                     self.OccupationalData = self.splitJSONForOccupationalDetail(json: json)
                    self.kinData = self.splitJSONForKinInfo(json: json)
                }
                self.delegate.reloadData()
            }
        }
    }
    
    func splitJSONForOccupationalDetail(json : JSON) -> [[String : Any]] {
        
        var bundleData = [[String : Any]]()
        
        let employmentStatus = json["RESULT_ARRAY"]["EMPLOYMENT_STATUS"].dictionaryValue
        if let employemntData = createBundle(dataType: "EMPLOYMENT_STATUS", placeholder: "OCCUPATIONSTATUS1", data: employmentStatus) {
            bundleData.append(employemntData)
        }
        // SAIYAD
        let industry = json["RESULT_ARRAY"]["EMPLOYMENT_STATUS"].dictionaryValue
        if let industryData = createBundle(dataType: "INDUSTRY", placeholder: "SELECTINDUSTRY", data: industry) {
            bundleData.append(industryData)
        }
        
//        let nextkin = json["RESULT_ARRAY"]["NEXT_OF_KIN"].dictionaryValue
//        if let incomeData = createBundle(dataType: "KINMOBILENO", placeholder: "NEXT_OF_KIN", data: nextkin) {
//            bundleData.append(incomeData)
//        }
        
        let income = json["RESULT_ARRAY"]["INCOME"].dictionaryValue
        if let incomeData = createBundle(dataType: "INCOME", placeholder: "GROSSSALARY", data: income) {
            bundleData.append(incomeData)
        }
        
        let brancehs = json["RESULT_ARRAY"]["BRANCHES"].dictionaryValue
        
//        if let branchesData = createBundle(dataType: "BRANCHES", placeholder: "PREFEREDBRANCH", data: brancehs) {
//            bundleData.append(branchesData)
//        }
        
        var dataArray = [[String : String]]()
        for (key, value) in brancehs {
            let dict = [
                "ID"    : key,
                "VALUE" : value.stringValue,
            ]
            dataArray.append(dict)
        }
         OnboardUser.branchData = dataArray
        
        var dataArray1 = [[String : AnyObject]]()
        for (key, value) in employmentStatus {
            
            let dict = employmentStatus[key]?.dictionaryValue
            dataArray1.append(dict! as [String : AnyObject])
        }
        OnboardUser.empStatusData = dataArray1
        
        //Resedence field
        let residenceData = [
            "ERROR" : "WHERELIVE",
            "TITLE"  : "WHERELIVE",
            "PLACEHOLDER"  : "WHERELIVE",
            "RESULT" : [[String : String]]()
            ] as [String : Any]
        bundleData.append(residenceData)
        
        
        return bundleData
    }
    
    func splitJSONForKinInfo(json : JSON) -> [[String : Any]] {
        
        var bundleData = [[String : Any]]()
        
        //Full name
        var residenceData = [
            "ERROR" : "Enter full name",
            "TITLE"  : "FULLNAME",
            "PLACEHOLDER"  : "FULLNAME",
            "RESULT" : [[String : String]]()
            ] as [String : Any]
        bundleData.append(residenceData)
        
        
        let nextKIN = json["RESULT_ARRAY"]["NEXT_OF_KIN"].dictionaryValue
        if let nextKINData = createBundle(dataType: "NEXT_OF_KIN", placeholder: "HOWRELATED", data: nextKIN) {
            bundleData.append(nextKINData)
        }
        
        //Kin id no
        residenceData = [
            "ERROR" : "KINIDNUMBER",
            "TITLE"  : "KINIDNUMBER",
            "PLACEHOLDER"  : "KINIDNUMBER",
            "RESULT" : [[String : String]]()
            ] as [String : Any]
        bundleData.append(residenceData)
        
       // Kin id mobile no
        residenceData = [
            "ERROR" : "KINMOBILENO",
            "TITLE"  : "KINMOBILENO",
            "PLACEHOLDER"  : "KINMOBILENO",
            "RESULT" : [[String : String]]()
            ] as [String : Any]
        bundleData.append(residenceData)
        
        return bundleData
    }
    
    func createBundle(dataType : String, placeholder : String, data : [String : JSON]) -> [String : Any]? {
        
        var dataArray = [[String : String]]()
        for (key, value) in data {
            let dict = [
                "ID"    : key,
                "VALUE" : value.stringValue,
                ]
            dataArray.append(dict)
        }
        
        if dataArray.count > 0 {
            let dict = [
                "ERROR" : placeholder,
                "TITLE"  : dataType,
                "PLACEHOLDER"  : placeholder,
                "RESULT" : dataArray
                ] as [String : Any]
            return dict
        }
        return nil
    }
}
