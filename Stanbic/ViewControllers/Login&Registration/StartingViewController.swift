//
//  StartingViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 16/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication

class StartingViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var rotateView: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    
    var timerStart : Timer?
     var str_biometric = Data()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        
         // Check Biometric :-
        checkChangesInBioMetric()
        
       // startAnimate()
     }
    
    // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 28, color: Color.white, title: "WELCOME", placeHolder: "")
        Fonts().set(object: lblMessage, fontType: 1, fontSize: 10, color: Color.c124_134_141, title: "SECURECONNECTION", placeHolder: "")
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
 
    func checkChangesInBioMetric() {
        
        let isRegistered = UserDefaults.standard.bool(forKey:UserDefault.registered)
        if isRegistered {
            
            // Add biometic mode
            addBiometricMode()
            
            if !ChangeBiometic() {
                
              startAnimate()
                
            } else {
                
                
                let alert = UIAlertController(title: "Stanbic", message: "We have detected changes in your biometrics. Please log in using your pin and enroll your biometrics again", preferredStyle: .alert)
                
                let actionYes = UIAlertAction(title: "Ok", style: .default, handler: { action in
                    self.setUpEnterpin()
                })
                
                alert.addAction(actionYes)
                DispatchQueue.main.async {
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
        } else {
            //SAVE DATA IN KEYCHAIN
//            let encpt = KeychainPasswordItem(service: KeychainConfiguration.encptKey, account: KeychainConfiguration.encptValue, accessGroup: KeychainConfiguration.accessGroup)
//            try? encpt.saveValue(Variables().reveal(key: VariableConstants.VarialbeString))
            startAnimate()
        }
    }
    
    
    func setUpEnterpin() {
        
        // global.initialview = true
        let nextvc = EnterPinChangeBiometricVC.getVCInstance() as! EnterPinChangeBiometricVC
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        nextvc.flow = "BIO"
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    // Detect Change biometric ThumbID / Face ID -
    
    
    func ChangeBiometic() -> Bool {
        
        //print(str_biometric)
        let context = LAContext()
        context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        
        let arr = CoreDataHelper().getDataForEntity1(entity : Entity.BIOMETRIC)
        if arr.count > 0
        {
            if str_biometric == arr[0]["aNEW"] ?? Data() || str_biometric == Data()
            {
                return false
             }
            else {
                UserDefaults.standard.set(false, forKey: UserDefault.biometric)
                UserDefaults.standard.synchronize()
                return true
            }
        }
        else{
            return false
        }
    }
    
    // Add Biometic mode
func addBiometricMode(){
    
    let arr = CoreDataHelper().getDataForEntity1(entity : Entity.BIOMETRIC)
    if arr.count > 0
    {
        str_biometric = arr[0]["aNEW"] ?? Data()
    }
    
    var datamode = Data()
    let context = LAContext()
    context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
    
    if context.evaluatedPolicyDomainState != nil {
        datamode = (context.evaluatedPolicyDomainState ?? nil)!
//        let str = String(decoding: datamode, as: UTF8.self)
//        print(str)
        CoreDataHelper().removeAllRecords(Entity.BIOMETRIC.rawValue)
        CoreDataHelper().saveUUID1(value : datamode, entityName : Entity.BIOMETRIC.rawValue, key : "NEW", removeRecords : true)
       // print("evaluatedPolicyDomainState no nil")
        
    }
    else{
        // let str = String(decoding: datamode, as: UTF8.self)
        CoreDataHelper().removeAllRecords(Entity.BIOMETRIC.rawValue)
//        CoreDataHelper().saveUUID(value : "datanil", entityName : Entity.BIOMETRIC.rawValue, key : "NEW", removeRecords : true)
      //  print("evaluatedPolicyDomainState nil")
        
       let data = Data("1212".utf8)
        
        CoreDataHelper().removeAllRecords(Entity.BIOMETRIC.rawValue)
        CoreDataHelper().saveUUID1(value : data, entityName : Entity.BIOMETRIC.rawValue, key : "NEW", removeRecords : true)
        
    }
}
    
    func startAnimate() {
        
        animate()
        timerStart = Timer.scheduledTimer(withTimeInterval: 0.95, repeats: true) { (timer) in
            self.animate()
        }
        Timer.scheduledTimer(withTimeInterval: 4, repeats: true) { (timer) in
            self.timerStart?.invalidate()
            timer.invalidate()
            if global.initialview == true {
              global.initialview = false
//                    let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
//                    self.navigationController?.pushViewController(nextvc, animated: true)
                let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
                appDelegate.window?.rootViewController = nextvc
                appDelegate.window?.makeKeyAndVisible()
                
            }
            else {
                let vc = OnboardingViewController.getVCInstance()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        //CHECK DEVICE HEIGHT
        let deviceHeight = self.view.frame.size.height
        
        if deviceHeight > 800 {
            Session.sharedInstance.bottamSpace = CGFloat(34)
        }
        else {
            Session.sharedInstance.bottamSpace = CGFloat(20)
        }
    }
    
    func animate(){
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: { () -> Void in
            self.rotateView.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.45, options: .curveLinear, animations: { () -> Void in
            self.rotateView.transform = .identity
        }, completion: nil)
    }
    
    
}
