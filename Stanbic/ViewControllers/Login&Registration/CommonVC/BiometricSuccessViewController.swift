//
//  BiometricSuccessViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 18/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class BiometricSuccessViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    
    var isFaceSupport : Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        if isFaceSupport == true {
            Fonts().set(object: self.lblTitle, fontType: 1, fontSize: 22, color: Color.c0_173_108, title: "FACEENABLE", placeHolder: "")
            Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 17, color: Color.c52_58_64, title: "NOWLOGINSMILE", placeHolder: "")
            Fonts().set(object: self.btnDone, fontType: 1, fontSize: 14, color: Color.white, title: "DONE", placeHolder: "")
            btnDone.roundedButton()
        }
        else {
            Fonts().set(object: self.lblTitle, fontType: 1, fontSize: 22, color: Color.c0_173_108, title: "TOUCHENABLE", placeHolder: "")
            Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 17, color: Color.c52_58_64, title: "NOWLOGINTOUCH", placeHolder: "")
            Fonts().set(object: self.btnDone, fontType: 1, fontSize: 14, color: Color.white, title: "DONE", placeHolder: "")
             btnDone.roundedButton()
        }
    }
    
    //Mark:- Button Actions
    @IBAction func done(_ sender: UIButton) {
        setUpSlideMenu()
//        let homeVC = NavHomeViewController.getVCInstance()
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.window?.rootViewController = homeVC
    }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
 }
