//
//  EnableTouchIDViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 18/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit
import LocalAuthentication

class EnableTouchIDViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnEnable: UIButton!
    @IBOutlet weak var btnNotNow: UIButton!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var faceIDAlertPopUpView: UIViewPropertys!
    @IBOutlet weak var StaticBioMetricLabel: UILabel!
    
    var isFaceIDSupport : Bool?
    var biocheck: Bool = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()
        checkUserDeviceSupport()
        
        //Set Flag for Registered user
        UserDefaults.standard.set(true, forKey: UserDefault.registered)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        backgroundView.isHidden = true
        faceIDAlertPopUpView.isHidden = true
        
        
        // Add biometric State
        if UserDefaults.standard.object(forKey: "ChangedBiometric") == nil {
            let context = LAContext()
            context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
            
            if context.evaluatedPolicyDomainState != nil {
                global.oldDomainState = (context.evaluatedPolicyDomainState ?? nil)!
                UserDefaults.standard.set(global.oldDomainState, forKey: "ChangedBiometric")
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Login_Registrtation.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK: - Button Actions
    
    @IBAction func enable(_ sender: UIButton) {
        var supportid = ""
        let context = LAContext()
        context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
        
        if Authntication(context).checkUserDeviceFaceIDSupport() == true
        {
             supportid = "Login with Face ID"
        }
        else
        {
            supportid = "Login with Touch ID"
        }
        if context.evaluatedPolicyDomainState != nil {
        authnticateUser()
        }
        else {
           showMessage(title: "", message: "\(supportid) not enable")
        }
     }
    
    @IBAction func notNow(_ sender: UIButton) {
        animateButtonUp()
    }
    
    @IBAction func didFaceIDButtonTapped(_ sender: Any) {
        
       animateButtonDown()
    //Set flag in user defaults for biometric access
    UserDefaults.standard.set(false, forKey: UserDefault.biometric)
    setUpSlideMenu()
    }
    
    
    
    func animateButtonDown() {
        
        UIView.animate(withDuration: 0.1, delay: 0.0, options: [.allowUserInteraction, .curveEaseIn], animations: {
            self.backgroundView.isHidden = true
            self.faceIDAlertPopUpView.isHidden = true
        }, completion: nil)
    }
    
    func animateButtonUp() {
        
        UIView.animate(withDuration: 0.1, delay: 0.0, options: [.allowUserInteraction, .curveEaseOut], animations: {
              self.backgroundView.isHidden = false
                self.faceIDAlertPopUpView.isHidden = false
        }, completion: nil)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touch: UITouch? = touches.first
        if touch?.view != faceIDAlertPopUpView {
            self.backgroundView.isHidden = true
            self.faceIDAlertPopUpView.isHidden = true
        }
    }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
}



//Mark: -Extension for BiometricAuthnicate
extension EnableTouchIDViewController {
    func checkUserDeviceSupport()
    {
        //IF USER DEVICE SUPPORT FACE ID, CHANGE LABEL ON SCREEN
        let context = LAContext()
        if Authntication(context).checkUserDeviceFaceIDSupport() == true
        {
            isFaceIDSupport = true
            DispatchQueue.main.async {
                //Set label title, font, color
                Fonts().set(object: self.lblTitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "LOGINWITHSMILE", placeHolder: "")
                Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 17, color: Color.c52_58_64, title: "USEFACE", placeHolder: "")
                Fonts().set(object: self.btnEnable, fontType: 1, fontSize: 14, color: Color.white, title: "ENABLEFACE", placeHolder: "")
                 self.btnEnable.roundedButton()
                Fonts().set(object: self.btnNotNow, fontType: 1, fontSize: 14, color: Color.c0_123_255, title: "SKIPFORNOW", placeHolder: "")
                self.btnNotNow.roundedButtonBorder()
                self.btnNotNow.layer.borderColor = Color.c207_211_214.cgColor
                self.StaticBioMetricLabel.text = "Use Face ID Later?"
     
            }
        }
        else {
            isFaceIDSupport = false
            DispatchQueue.main.async {
                //Set label title, font, color
                Fonts().set(object: self.lblTitle, fontType: 1, fontSize: 22, color: Color.c10_34_64, title: "LOGINWITHSMILE", placeHolder: "")
                Fonts().set(object: self.lblSubtitle, fontType: 0, fontSize: 17, color: Color.c52_58_64, title: "USETOUCH", placeHolder: "")
                Fonts().set(object: self.btnEnable, fontType: 1, fontSize: 14, color: Color.white, title: "ENABLETOUCH", placeHolder: "")
                self.btnEnable.roundedButton()
               
                Fonts().set(object: self.btnNotNow, fontType: 1, fontSize: 14, color: Color.c0_123_255, title: "SKIPFORNOW", placeHolder: "")
                self.btnNotNow.roundedButtonBorder()
                self.btnNotNow.layer.borderColor = Color.c207_211_214.cgColor
                self.StaticBioMetricLabel.text = "Use Touch ID Later?"
          
            }
        }
    }
    
    func authnticateUser() {
        //CHECK USER'S FINGERPRINT OR FACE ID
        let context = LAContext()
        Authntication(context).authnticateUser { (success, message) in
            if success == true {
                DispatchQueue.main.async {
                    //Set flag in user defaults for biometric access
                    UserDefaults.standard.set(true, forKey: UserDefault.biometric)
                    let vc = BiometricSuccessViewController.getVCInstance() as! BiometricSuccessViewController
                    vc.isFaceSupport = self.isFaceIDSupport
                    //self.navigationController?.pushViewController(vc, animated: true)
                    self.show(vc, sender: nil)
                }
            }
            else {
                DispatchQueue.main.async {
                    if message != "" {
                        //Set flag in user defaults for biometric access
                        UserDefaults.standard.set(false, forKey: UserDefault.biometric)
                        self.showMessage(title: "", message: message)
                    }
                }
            }
        }
    }
}


extension UIView {
    
   
}
