//
//  AccountDetailVC.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 22/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation


protocol AccountDetailVMDelegate: class {
    func reloadData()
}


// ViewModel for Country List
class AccountDetailVM{
    //MARK:- Model for API calling
    // Model : -
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let resultArray: [Result]
        
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case resultArray = "RESULT_ARRAY"
            
        }
    }
   
    
    struct Result: Codable{
        let transaction    : String?
        let transactionamount        : String?
        let accountbalance   : String?
        let narration   : String?
        let transactiondate   : String?
        
        private enum CodingKeys: String, CodingKey {
            case transaction = "TRANSACTION"
            case transactionamount = "TRANSACTION_AMOUNT"
            case accountbalance = "ACCOUNT_BALANCE"
            case narration = "NARRATION"
            case transactiondate = "TRANSACTION_DATE"
            
             }
    }
    
    var responseData : Response?
    weak var delegate : AccountDetailVMDelegate!
    
    
    // GET Mini Statement
    func miniStatement(pin: String, mobileNo: String, accountAlias:String) {
        
        RestAPIManager.miniStatement(title : "PLEASEWAIT", subTitle : "FETCHACDETAILS", pin: "Cx+1e0vEBoZlPztxxZG5fQ==", type: Response.self, mobileNo: "254700707453", accountAlias: "Current Acct-KES-0100006123443", completion: { (response) in
            DispatchQueue.main.async {
                self.responseData = response
                self.delegate.reloadData()
                
            }
        }) { (error) in
            print(error)
        }
    }
        
  }
