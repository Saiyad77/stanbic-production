//
//  AccountDetailViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 21/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class AccountDetailHeaderCell: UITableViewCell {
    @IBOutlet weak var lbltitle: UILabel!
    
     override func awakeFromNib() {
    Fonts().set(object: lbltitle, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: "RecentTransactions", placeHolder: "")
    }
}

class AccountDetailCell: UITableViewCell {
    
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var lbltransactionby: UILabel!
    @IBOutlet weak var lblamount: UILabel!
    @IBOutlet weak var imgcrdr: UIImageView!
    
    override func awakeFromNib() {
        Fonts().set(object: lbltransactionby, fontType: 1, fontSize: 14, color: Color.black, title: "Mobile Baking", placeHolder: "")
        Fonts().set(object: lbltime, fontType: 1, fontSize: 14, color: Color.c134_142_150, title: "2:30:56 pm", placeHolder: "")
        Fonts().set(object: lblamount, fontType: 1, fontSize: 14, color: Color.black, title: "-KSH 50,987.93", placeHolder: "")
    }
    
    func initiatedata(dictdetails:AccountDetailVM.Result,curruncycode:String){
       
        let amount = dictdetails.transactionamount
        if dictdetails.transaction == "DR" {
            imgcrdr.image = UIImage(named: "DR image")
            lblamount.text = "-\(curruncycode) \(amount!)"
         }
        else {
            imgcrdr.image = UIImage(named: "CR image")
            lblamount.text = "\(curruncycode) \(amount!)"
            
        }
        
        lbltime.text = dictdetails.transactiondate
        lbltransactionby.text = dictdetails.narration
        
    }
}

class AccountDetailViewController: UIViewController,AccountDetailVMDelegate {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lblactypetop: UILabel!
    @IBOutlet weak var lblacnumbertop: UILabel!
    
    @IBOutlet weak var lblactype: UILabel!
    @IBOutlet weak var lblacnumber: UILabel!
    @IBOutlet weak var lblcurruntbalancetitle: UILabel!
    @IBOutlet weak var lblavailbalebalancetitle: UILabel!
    @IBOutlet weak var lblcurruntbalance: UILabel!
    @IBOutlet weak var lblavailbalebalance: UILabel!
    
    @IBOutlet weak var tblAccountDetails: UITableView!
    
    let model = AccountDetailVM()
    var curruncycode = "KSH"

    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        model.delegate = self
        model.miniStatement(pin: "Cx+1e0vEBoZlPztxxZG5fQ==", mobileNo: "254700707453", accountAlias: "Current Acct-KES-0100006123443")
    }
    
    func reloadData() {
        let response = model.responseData
        if response!.statusCode == 200 {
          tblAccountDetails.reloadData()
        }
        else {
            self.showMessage(title: "", message: response!.statusMessage ?? "")
        }
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
         Fonts().set(object: self.lblactypetop, fontType: 1, fontSize: 20, color: Color.white, title: "Current account", placeHolder: "")
         Fonts().set(object: self.lblacnumbertop, fontType: 1, fontSize: 11, color: Color.c0_123_255, title: "001 333 5690 098", placeHolder: "")
         Fonts().set(object: self.lblactype, fontType: 1, fontSize: 15, color: Color.white, title: "Current account", placeHolder: "")
         Fonts().set(object: self.lblacnumber, fontType: 1, fontSize: 11, color: Color.white, title: "001 333 5690 098", placeHolder: "")
        
         Fonts().set(object: self.lblcurruntbalancetitle, fontType: 1, fontSize: 11, color: Color.white, title: "Currentbalance", placeHolder: "")
         Fonts().set(object: self.lblavailbalebalancetitle, fontType: 1, fontSize: 11, color: Color.white, title: "Availablebalance", placeHolder: "")
        
        Fonts().set(object: self.lblcurruntbalance, fontType: 1, fontSize: 14, color: Color.white, title: "KES 256,890.67", placeHolder: "")
        Fonts().set(object: self.lblavailbalebalance, fontType: 1, fontSize: 14, color: Color.white, title: "KES 255,890.98", placeHolder: "")
        }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
      self.navigationController?.popViewController(animated: true)
    }

}

extension AccountDetailViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
     return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else {
            return (model.responseData?.resultArray.count ?? 0)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AccountDetailHeaderCell", for: indexPath as IndexPath) as! AccountDetailHeaderCell
        return cell
        }
        else {
         let cell = tableView.dequeueReusableCell(withIdentifier: "AccountDetailCell", for: indexPath as IndexPath) as! AccountDetailCell
            cell.initiatedata(dictdetails: (model.responseData?.resultArray[indexPath.row])!, curruncycode: curruncycode)
           return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          if indexPath.section == 0 {
            return 46
        }
        else {
            return 80
        }
    }
}



