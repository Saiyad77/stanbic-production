//
//  RequestBankStatementViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 22/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class RequestBankStatementViewController: UIViewController,UIGestureRecognizerDelegate,UITextFieldDelegate{
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var lblselectdatetitle: UILabel!
    @IBOutlet weak var lblrequestfor: UILabel!
    @IBOutlet weak var lblacnumber: UILabel!
    @IBOutlet weak var lblactype: UILabel!
    @IBOutlet weak var txtstartdate: UITextField!
    @IBOutlet weak var txtenddate: UITextField!
    @IBOutlet weak var btnsubmit: UIButton!
    
    
    // Variables for Bottom sheet
    var bottomSheetVC: BottomSheetViewController?
    var imgView : UIImageView?
        let datePicker = UIDatePicker()
    
    var txtfield = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.c10_34_64, title: "Fullstatement", placeHolder: "")
        Fonts().set(object: self.lblsubtitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "Getafullaccountstatement", placeHolder: "")
        
        Fonts().set(object: self.lblselectdatetitle, fontType: 1, fontSize: 9, color: Color.c0_123_255, title: "SELECTDATEPERIOD", placeHolder: "")
        
        Fonts().set(object: self.txtenddate, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "Start Date")
          Fonts().set(object: self.txtstartdate, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "End Date")
        
        Fonts().set(object: self.lblrequestfor, fontType: 1, fontSize: 9, color: Color.c0_123_255, title: "REQUESTFOR", placeHolder: "")
        Fonts().set(object: self.lblactype, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "Current account", placeHolder: "")
        
         Fonts().set(object: self.lblacnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "XX-XXX-XXX-X23", placeHolder: "")
            Fonts().set(object: self.btnsubmit, fontType: 1, fontSize: 14, color: Color.white, title: "SUBMIT", placeHolder: "")
        
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func networkprovider(_ sender: AttributedButton) {
        view.endEditing(true)
        self.showBottomSheetVC()
    }
    
    @IBAction func submit(_ sender: UIButton) {
        let nextvc = RequestSuccessfullViewControllerViewController.getVCInstance() as! RequestSuccessfullViewControllerViewController
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
    func showBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height - 340
        bottomSheetVC?.moveBottomSheet(xibtype: "ACCOUNT")
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView = UIImageView.init(frame: self.view.frame)
        imgView?.backgroundColor = Color.black
        imgView?.alpha = 0.2
        imgView?.isUserInteractionEnabled = true
        self.view.addSubview(imgView!)
        
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView?.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "")
        imgView?.removeFromSuperview()
        
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtstartdate{
              txtfield = "1"
         initiateDatePicker(textfield : txtstartdate)
          
        }
        if textField == txtenddate{
              txtfield = "2"
            initiateDatePicker(textfield : txtenddate)
            
        }
        

       // textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
      // textField.didEnd()
    }
    
    // Date Picker Logic =======
    func initiateDatePicker(textfield : UITextField){
        //Registrater date picker value changed event
        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        datePicker.maximumDate = Date()
        //Formate Date
        datePicker.datePickerMode = .date
        textfield.inputView = datePicker
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        if txtfield == "1" {
          txtstartdate.text = (formatter.string(from: sender.date))
        }
        if txtfield == "2" {
            txtenddate.text = (formatter.string(from: sender.date))
        }
        
    }
 }


