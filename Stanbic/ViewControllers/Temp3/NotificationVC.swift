//
//  NotificationVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 16/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {
    
    @IBOutlet weak var tblViewNotification: UITableView!
    
    //Mark:- IBOUTLETS _ header
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblHeaderTitle: UIView!
    private var lastContentOffset: CGFloat = 0
    
    var imgHeader =  UIImageView()
    var imgHederHeight : CGFloat = 0
    
    var arrNotification = [[String:String]]()
    var arrnew = [[String:String]]()
    var allData = [[String:String]]()
    var arrnewupdate = [[String:String]]()
    let coreData = CoreDataHelper()
    var dataNotification = [[String:String]]()
    var indexarray = [Int]()
    var badgeCount = 0
    var hight = 0
    var count = 50
    
  override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.delegate = self
        setFonts()
        // Do any additional setup after loading the view.
        //initialSetup()
        setupHeader()
        
        NotificationCenter.default.addObserver(self, selector: #selector(catchNotification), name: Notification.Name(rawValue: "notificationshow"), object: nil)
        if UserDefaults.standard.object(forKey: "badgeCount") != nil{
            badgeCount = UserDefaults.standard.object(forKey: "badgeCount") as! Int
        }
      
        tblViewNotification.rowHeight = UITableView.automaticDimension
        tblViewNotification.estimatedRowHeight = 125
   
    NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
    
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        Fonts().set(object: self.lbltitle, fontType: 0, fontSize: 24, color: Color.white, title: "NOTIFICATION", placeHolder: "")
        Fonts().set(object: self.lblHeaderTitle, fontType: 1, fontSize: 14, color: Color.white, title: "NOTIFICATION", placeHolder: "")
        Fonts().set(object: lblMsg, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "NOTIFICATIONNOTFOUND", placeHolder: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        datashow()
    }
    
    func datashow() {
        
        arrnew.removeAll()
        arrNotification.removeAll()
        
        allData = CoreDataHelper().FetchNotifications(entity: Entity.NOTIFICATIONS_DATA)
        
        for i in 0..<allData.count {
            let dict = allData[i]
            if dict["aviewstatus"] == "false" {
                arrNotification.append(dict)
            }
            else {
                arrnew.append(dict)
            }
        }
        if allData.count == 0 {
            lblMsg.isHidden = false
        }
        else{
            lblMsg.isHidden = true
        }
        tblViewNotification.reloadData()
    }
    
    // Fetch NOTIFICATIONS_DATA
    @objc func catchNotification(){
   
        arrnew.removeAll()
        arrNotification.removeAll()
        allData = CoreDataHelper().FetchNotifications(entity: Entity.NOTIFICATIONS_DATA)
        
        for i in 0..<allData.count {
            let dict = allData[i]
            if dict["aviewstatus"] == "false" {
                arrNotification.append(dict)
            }
            else {
                arrnew.append(dict)
            }
        }
        if allData.count == 0 {
            lblMsg.isHidden = false
        }
        else{
            lblMsg.isHidden = true
        }
        tblViewNotification.reloadData()
        
        if tabBarController?.tabBar.items?[3].badgeValue == "0"{
            tabBarController?.tabBar.items?[3].badgeValue = nil
        }
     }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    @IBAction func barBtnBackTapped(_ sender: UIBarButtonItem) {
        if tabBarController?.tabBar.items?[3].badgeValue == "0"{
            tabBarController?.tabBar.items?[3].badgeValue = nil
        }
        savedata()
        global.cupApi = "NO"
        setUpSlideMenu()
    }
}

extension NotificationVC: UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
//        arrnewupdate.removeAll()
//        checkstatus()
        indexarray.removeAll()
        if arrnew.count > 0 {
            if arrNotification.count > 0 {
                return 2
            }else{
                return 1
            }
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationHeaderCell") as! NotificationHeaderCell
        
        if arrnew.count > 0 {
            if section == 0 {
                Fonts().set(object: cell.lblTitle, fontType: 1, fontSize: 11, color: Color.c160_166_173
                    , title: "NEWNOTIFICATIONS", placeHolder: "")
            }
            else{
                Fonts().set(object: cell.lblTitle, fontType: 1, fontSize: 11, color: Color.c160_166_173
                    , title: "OLDNOTIFICATIONS", placeHolder: "")
            }
        }
        else{
            if arrNotification.count > 0 {
                Fonts().set(object: cell.lblTitle, fontType: 1, fontSize: 11, color: Color.c160_166_173
                    , title: "OLDNOTIFICATIONS", placeHolder: "")
            }
            else {
                Fonts().set(object: cell.lblTitle, fontType: 1, fontSize: 11, color: Color.c160_166_173
                    , title: "NEWNOTIFICATIONS", placeHolder: "")
            }
        }
        cell.backgroundView?.backgroundColor = UIColor.white
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrnew.count > 0 {
            if section == 0 {
                return arrnew.count
            }
            else {
                return arrNotification.count
            }
        }
        else{
            if arrNotification.count > 0 {
                return arrNotification.count
            }
            else{
                return arrnew.count
            }
        }
      }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! NotificationTableCell
        
        var dict = [String:String]()

        if arrnew.count > 0 {
            if indexPath.section == 0 {
                dict = arrnew[indexPath.row]
                cell.setdata(dict: dict)
                indexarray.append(indexPath.row)
            }else{
                dict = arrNotification[indexPath.row]
                cell.setdata(dict: dict)
            }
        }
        else{
            dict = arrNotification[indexPath.row]
            cell.setdata(dict: dict)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if allData.count > 0 {
            return 51
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tblHeight.constant = tblViewNotification.contentSize.height
        
        if arrnew.count > 0 {
        
            if indexarray.contains(indexPath.row)
            {
                var dictnew = [String: String]()
                let dict = arrnew[indexPath.row]
                
                dictnew = ["abody":dict["abody"] ?? "","atitle":dict["atitle"] ?? "","aimageUrl":dict["aimageUrl"] ?? "","atime":dict["atime"] ?? "","alink":dict["alink"] ?? "","anotificationType":dict["anotificationType"]  ?? "","aviewstatus":"false"]
                    
                 arrnewupdate.append(dictnew)
                 if UserDefaults.standard.object(forKey: "badgeCount") != nil{
                    badgeCount = UserDefaults.standard.object(forKey: "badgeCount") as! Int
                    
                    if badgeCount < 0 {
                        UserDefaults.standard.set(0, forKey: "badgeCount")
                        UserDefaults.standard.synchronize()
                        tabBarController?.tabBar.items?[3].badgeValue = nil
                          UIApplication.shared.applicationIconBadgeNumber = 0
                    }
                    else {
                        if badgeCount - 1 < 1 {
                            UserDefaults.standard.set(0, forKey: "badgeCount")
                            UserDefaults.standard.synchronize()
                            tabBarController?.tabBar.items?[3].badgeValue = nil
                              UIApplication.shared.applicationIconBadgeNumber = 0
                        }
                        else {
                            UserDefaults.standard.set(badgeCount - 1, forKey: "badgeCount")
                            UserDefaults.standard.synchronize()
                            tabBarController?.tabBar.items?[3].badgeValue = "\(badgeCount - 1)"
                              UIApplication.shared.applicationIconBadgeNumber = badgeCount - 1
                        }
                    }
                }
            }
        }
        else {
             tabBarController?.tabBar.items?[3].badgeValue = nil
             UIApplication.shared.applicationIconBadgeNumber = 0
             UserDefaults.standard.set(0, forKey: "badgeCount")
             UserDefaults.standard.synchronize()
        }
    }
    
    func tableView(_ tableView: UITableView,
                   didEndDisplaying cell: UITableViewCell,
                   forRowAt indexPath: IndexPath){
        
    }
}

extension NotificationVC : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let y = imgHederHeight - (scrollView.contentOffset.y + 10)
        let height = min(max(y, 150), 200)
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
        
        if scrollView.contentOffset.y > 50 {
            self.lblHeaderTitle.isHidden = false
        }
        else {
            self.lblHeaderTitle.isHidden = true
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in tblViewNotification.visibleCells {
            let indexPath: IndexPath? = tblViewNotification.indexPath(for: cell)
            if let indexPath = indexPath {
               // print("\(indexPath)")
                
            }
        }
    }
    
    // Add Header
    func setupHeader() {
        imgHederHeight = 168
        scrollView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        scrollView.delegate = self
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: imgHederHeight)
        imgHeader.contentMode = .scaleAspectFill
        imgHeader.image = UIImage.init(named: "GradienTop2") // - GradientBar-3
        imgHeader.clipsToBounds = true
        
        view.addSubview(imgHeader)
        self.view.bringSubviewToFront(scrollView)
        self.view.bringSubviewToFront(btnBack)
        self.view.bringSubviewToFront(lblHeaderTitle)
    }
}


extension NotificationVC : UITabBarControllerDelegate
{
    // UITabBarDelegate
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        //  print("Selected item")
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if tabBarController.selectedIndex != 3{
            savedata()
        }
        
    }
    
    func checkstatus() {
        
        if arrnew.count > 0 {
            let countnoti = tblViewNotification.visibleCells.capacity
            
            if arrnew.count < countnoti {
                for i in 0..<arrnew.count {
                    let dict = arrnew[i]
                    
                    for (key, value) in dict {
                        var dictnew = [String: String]()
                        if key == "aviewstatus"{
                            dictnew[key] = "false"
                        }
                        else {
                            dictnew[key] = value
                        }
                        arrnewupdate.append(dictnew)
                    }
                }
             }
            
            for i in 0..<arrnew.count {
                if i < countnoti {
                    let dict = arrnew[i]
                    
                    for (key, value) in dict {
                        var dictnew = [String: String]()
                        if key == "aviewstatus"{
                            dictnew[key] = "false"
                        }
                        else {
                            dictnew[key] = value
                        }
                        arrnewupdate.append(dictnew)
                    }
                }
            }
        }
    }
    
    // Save Notification in DB:_
    func savedata() {
        
        for i in 0..<arrnewupdate.count {
            dataNotification.append(arrnewupdate[i])
        }
        
        for i in 0..<arrNotification.count {
            dataNotification.append(arrNotification[i])
        }
        
        if dataNotification.count > 0 {
         // print("SAVE _ SAVE")
            
            let array = noDuplicates(dataNotification)
       coreData.saveArrayForNotifiation(json: array, entityObject: Entity.NOTIFICATIONS_DATA, jsonKey: Entity.NOTIFICATIONS_DATA.rawValue)
        }
        else {
           // print("0000000000000000")
        }
        
    }
    
    func noDuplicates(_ arrayOfDicts: [[String: String]]) -> [[String: String]] {
        var noDuplicates = [[String: String]]()
        var usedNames = [String]()
        for dict in arrayOfDicts {
            if let name = dict["atime"], !usedNames.contains(name) {
                noDuplicates.append(dict)
                usedNames.append(name)
            }
        }
        return noDuplicates
    }
    
}
