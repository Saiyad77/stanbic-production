//
//  CalculateVAFRepaymentSecondVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 16/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class CalculateVAFRepaymentSecondVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var txtInterestRate: UITextField!
    @IBOutlet weak var txtLoanAmmount: UITextField!
    @IBOutlet weak var lblShowYourTotalRepayment: UILabel!
    @IBOutlet weak var lblSelect: UILabel!
    @IBOutlet weak var btnCalculate: AttributedButton!
    @IBOutlet weak var btnconsBottom: NSLayoutConstraint!
    @IBOutlet weak var txtmonths: UITextField!
    @IBOutlet weak var lblincome: UILabel!
    
     var model = VAFLoanVM()
     var dictdata = [String:String]()
    
    // View Leaves
    @IBOutlet var viewLeaves: UIView!
    @IBOutlet weak var lblSelectLeavesTitle: UILabel!
    @IBOutlet weak var tblViewLeaves: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    var imgBlack : UIImageView?
    var arrNumberOfLeaves = [String]()
   
    var loan = ""
     let coreData = CoreDataHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
        model.delegate = self
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
//          txtLoanAmmount.isUserInteractionEnabled = false
//          txtInterestRate.isUserInteractionEnabled = false
//          lblSelect.isUserInteractionEnabled = false
        
        let dict = model.responseData!["RESULTS_ARRAY"].dictionaryValue
        let monthly =  dict["MONTHLY_PAYMENT"]?.stringValue
        let totel =  dict["TOTAL_PAYMENT"]?.stringValue
        
        lblShowYourTotalRepayment.text = "Your monthly repayment is KES \(monthly ?? "")\nTotal repayment is KES \(totel ?? "")"
        txtLoanAmmount.text = dictdata["amount"]
        txtInterestRate.text = dictdata["rate"]
         txtmonths.text = dictdata["duration"]
        
        if loan == "3"{
         lblincome.text = "Income Ratio is \(dict["INCOME_RATIO"]?.stringValue ?? "0")\n Affordable Loan is KES \(dict["AFFORDABLE_LOAN"]?.stringValue ?? "0")"
      }
   
        txtInterestRate.keyboardType = .numberPad
        txtLoanAmmount.keyboardType = .numberPad
        txtmonths.keyboardType = .numberPad
        
       
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    // MARK: - Required Method's
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp3.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func initialSetup(){
        
        Fonts().set(object: txtInterestRate, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        
        txtInterestRate.addUI(placeholder: "INTERESTRATE1")
       
        Fonts().set(object: txtLoanAmmount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        if loan == "1"{
        txtLoanAmmount.addUI(placeholder: "LOANAMOUNTTITLE")
            lblincome.isHidden = true
        }else  if loan == "2"{
            txtLoanAmmount.addUI(placeholder: "INCOMEAMOUNT")
              lblincome.isHidden = true
        }else {
            txtLoanAmmount.addUI(placeholder: "INCOMEAMOUNT")
              lblincome.isHidden = false
        }
        
//        Fonts().set(object: lblSelect, fontType: 1, fontSize: 15, color: Color.c134_142_150, title: "SELECTREPAYMENT", placeHolder: "")
        Fonts().set(object: lblShowYourTotalRepayment, fontType: 0, fontSize: 16, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: btnCalculate, fontType: 1, fontSize: 14, color: Color.white, title: "CALCULATE", placeHolder: "")
        Fonts().set(object: lblincome, fontType: 0, fontSize: 16, color: Color.c10_34_64, title: "", placeHolder: "")
        
        if loan == "1"{
            initialSetUpNavigationBar(headerTitle:"VAFREPAYMENT")
        }
        else if loan == "2"{
            initialSetUpNavigationBar(headerTitle:"HOMELOANREPAYMENT")
        }
        else {
            initialSetUpNavigationBar(headerTitle:"HOMELOANAFFORDABILITY")
        }
        
        Fonts().set(object: txtmonths, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        txtmonths.addUI(placeholder: "SELECTREPAYMENT")
    }
    
    func initialSetUpNavigationBar(headerTitle:String)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headerTitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txtLoanAmmount.didBegin()
        txtInterestRate.didBegin()
           txtmonths.didBegin()
    }
    
    @IBAction func barBtnBackTapped(_ sender: UIBarButtonItem) {
       // self.navigationController?.popViewController(animated: true)
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }
    
    @IBAction func btnCalculateTapped(_ sender: AttributedButton) {
        self.view.endEditing(true)
        
        var acno = ""
        
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            let acnos = acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!
            
            if acnos.contains("KES-"){
                let arrac = acnos.components(separatedBy: "KES-")[1]
                acno = arrac
            }
            else {
                acno = acnos
            }
        }
        
        let amounts = getAmountValdiation1(service_code : "LCT")
        
        let min = amounts.0
        let max = amounts.1
        let amount = removeKES(price : txtLoanAmmount.text ?? "")
        let msg = "\("AMOUNTBEETWEEN".localized(Session.sharedInstance.appLanguage)) \(min) \("-".localized(Session.sharedInstance.appLanguage)) \(max) "
        
        view.endEditing(true
        )
        if txtLoanAmmount.text == "KES "{
            txtLoanAmmount.showError(errStr: "LOANAMOUNT")
        }
        else if txtLoanAmmount.text == "KES 0"{
            txtLoanAmmount.showError(errStr: "LOANAMOUNT")
        }
        else if checkAmount(min:min,max:max,amount:amount) == false {
            txtLoanAmmount.showError(errStr: msg)
        }
        else if txtInterestRate.text == "" || txtInterestRate.text == "%" || txtInterestRate.text == "0%" || txtInterestRate.text == "0"    {
            txtInterestRate.showError(errStr: "VALIDINTRESTRATE")
        }
        else if txtmonths.text == "" || txtmonths.text == "0"{
            txtmonths.showError(errStr: "VALIDREPAYMENT")
        }
        
        else if txtmonths.text == "SELECTREPAYMENT".localized(Session.sharedInstance.appLanguage) {
            txtmonths.showError(errStr: "SELECTREPAYMENT")
        }
        else {
            var type = ""
            if loan == "1"{
                type = "VAF"
            }
            else if loan == "2"{
                   type = "HOMELOAN"
            }
            else {
                   type = "HOMEAFFORD"
            }
            model.loancalulate(amount: txtLoanAmmount.text!, rate: txtInterestRate.text!, duration: txtmonths.text!, loantype: type)
        }
    }
    
    @IBAction func btnSelectRepaymentPeriodTapped(_ sender: UIButton) {
        view.endEditing(true)
          arrNumberOfLeaves = ["24 Month","36 Month"]
        self.showPopup(popupView: viewLeaves, height: (arrNumberOfLeaves.count * 50) + 130)
    }
    
    @IBAction func btnCancelLeaves(_ sender: UIButton) {
        self.dismissPopup(popupView: viewLeaves)
    }
}

extension CalculateVAFRepaymentSecondVC {
    
    func showPopup(popupView : UIView, height : Int) {
        
        imgBlack = UIImageView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        imgBlack?.alpha = 0.5
        //imgBlack?.backgroundColor = colorConstant.kBlackColor
        imgBlack?.isUserInteractionEnabled = true
        self.view.addSubview(imgBlack!)
        
        let frm = CGRect(x: ((self.view.frame.size.width / 2) - ((self.view.frame.size.width * 0.9) / 2)), y: -(popupView.frame.size.height), width: (self.view.frame.size.width * 0.9), height: CGFloat(height))
        
        popupView.frame = frm
        
        self.view.addSubview(popupView)
        
        popupView.layer.cornerRadius = 10
        popupView.clipsToBounds = true
        
        self.arrNumberOfLeaves.reverse()
        
        //  Fonts().set(object: lblSelectLeavesTitle, fontType: 1, fontSize: 20, color: Color.black, title: "NUMBEROFLEAVES", placeHolder: "")
        Fonts().set(object: btnCancel, fontType: 1, fontSize: 17, color: Color.black, title: "CANCEL", placeHolder: "")
        
        
        UIView.animate(withDuration: 0.5, animations: {
            
            let frm = CGRect(x: ((self.view.frame.size.width / 2) - ((self.view.frame.size.width * 0.9) / 2)), y: (self.view.frame.size.height / 2 - popupView.frame.size.height / 2), width: (self.view.frame.size.width * 0.9), height: CGFloat(height))
            
            popupView.frame = frm
            
        }){ (Bool) in
            
        }
    }
    
    
    // Dismiss popup
    func dismissPopup(popupView : UIView) {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            var frm =  popupView.frame
            
            frm.origin.y = -(popupView.frame.size.height)
            
            popupView.frame = frm
            
        }) { (Bool) in
            
            popupView.removeFromSuperview()
            
            self.imgBlack?.removeFromSuperview()
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrNumberOfLeaves.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LEAVES", for: indexPath) as! LeavesTableViewCell
        cell.lblNumberLeaves.text = arrNumberOfLeaves[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //txt.text = arrNumberOfLeaves[indexPath.row]
        self.dismissPopup(popupView: viewLeaves)
    }
}

//Text field delegate
extension CalculateVAFRepaymentSecondVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtLoanAmmount {
            
            textField.typingAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            let protectedRange = NSMakeRange(0, 4)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                
                return false
            }
            return true
        }
        else if textField == txtmonths {
                
                let maxLength = 3
                let currentString: NSString = textField.text! as NSString
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
                
            }
        else{
            return true
        }
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
    
    
}

extension CalculateVAFRepaymentSecondVC
{
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.btnconsBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.btnconsBottom.constant = 54
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

extension CalculateVAFRepaymentSecondVC  : VAFLOANVMDelegate{
    
    // GET RESPONCE
    func loanData() {
        
        if model.responseData?["STATUS_CODE"] == 200 {
       
            let dict = model.responseData!["RESULTS_ARRAY"].dictionaryValue
            let monthly =  dict["MONTHLY_PAYMENT"]?.stringValue
            let totel =  dict["TOTAL_PAYMENT"]?.stringValue
            let income = dict["INCOME_RATIO"]?.stringValue
            let Affordable = dict["AFFORDABLE_LOAN"]?.stringValue
            lblShowYourTotalRepayment.text = "Your monthly repayment is KES \(monthly ?? "")\nTotal repayment is KES \(totel ?? "")"
            lblincome.text = "Income Ratio is \(income ?? "0")\n Affordable Loan is KES \(Affordable ?? "0")"
        }
        else
        {
           // self.showMessage(title: "", message: model.responseData!["STATUS_MESSAGE"].stringValue)
            
            let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
            nextvc.message = model.responseData!["STATUS_MESSAGE"].stringValue
            nextvc.Flow = "1"
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
}


class PostFixTextField: UITextField {
    
    @IBInspectable var postfix : String = "%"
    @IBInspectable var removePostfixOnEditing : Bool = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addTarget(self, action: #selector(textHasChanged), for: .editingDidEnd)
        self.addTarget(self, action: #selector(removePostFix), for: .editingDidBegin)
        self.addTarget(self, action: #selector(textHasChanged), for: .editingChanged)
    }
    
    @objc func textHasChanged()
    {
        self.removePostFix()
        self.addPostFix()
        self.setCursorPosition(input: self, position: (self.originalText()?.characters.count)!)
    }
    
    private func setCursorPosition(input: UITextField, position: Int) {
        let position = input.position(from: input.beginningOfDocument, offset: position)!
        input.selectedTextRange = input.textRange(from: position, to: position)
    }
    
    func addPostFix()
    {
        if(self.text != nil)
        {
            self.text = self.text! + postfix
        }
    }
    
    func originalText() ->String?{
        let prefixRange = NSString(string: (self.attributedText?.string)!).range(of: postfix)
        if(prefixRange.location != NSNotFound)
        {
            return self.text!.replacingOccurrences(of: postfix, with: "")
        }
        return self.text
    }
    
    @objc func removePostFix(){
        
        if(self.removePostfixOnEditing && self.text != nil)
        {
            self.text = self.originalText()
        }
    }
}
