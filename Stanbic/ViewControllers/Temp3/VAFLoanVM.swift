//
//  VAFLoanVM.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 17/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation



protocol VAFLOANVMDelegate: class {
    func loanData()
}

class VAFLoanVM {
    

    
    
//    // Model : - for FETCH
//    struct Response: Codable {
//
//        let statusCode: Int?
//        let statusMessage: String?
//        let resultArray: Result?
//
//
//        private enum CodingKeys: String, CodingKey {
//            case statusCode = "STATUS_CODE"
//            case statusMessage = "STATUS_DESCRIPTION"
//            case resultArray = "RESULTS_ARRAY"
//        }
//
//        struct Result : Decodable {
//            private enum CodingKeys : String, CodingKey {
//                case MONTHLY_PAYMENT = "MONTHLY_PAYMENT"
//
//            }
//            let raw : MONTHLY_PAYMENT
//        }
//
//    }
//
    
        var responseData : JSON?
        weak var delegate : VAFLOANVMDelegate!
    
    func loancalulate(amount:String,rate:String,duration:String,loantype:String)
    {
        
        let amount = amount.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range: nil)
        let timeduration = duration.replacingOccurrences(of: "Month", with: "", options: NSString.CompareOptions.literal, range: nil)
      
       let amount_comma = amount.replacingOccurrences(of: ",", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        let amount1 =  (amount_comma as NSString).integerValue
        let timeduration1 =   (timeduration as NSString).integerValue
        
        let rate1 =    (rate as NSString).integerValue
     //   print(rate1)
        let loader = RestAPIManager.addProgress(title: "PLEASEWAIT", description: "WEAREPROCESSINGYOURREQUEST")
        
        RestAPIManager.loanCalculator(title: "PLEASEWAIT", subTitle: "WEAREPROCESSINGYOURREQUEST", mobileNo: OnboardUser.mobileNo, amount: amount1, rate: rate1, duration: timeduration1, LOAN_TYPE: loantype){ (json) in
            DispatchQueue.main.async {
                loader.remove()
            }
            DispatchQueue.main.async {
                self.responseData = json
                self.delegate.loanData()
            }
            }
        }
    }
