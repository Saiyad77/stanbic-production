//
//  NotificationTableCell.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 16/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class NotificationTableCell: UITableViewCell {

    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var lblNotificationTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTimeStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       Fonts().set(object: lblNotificationTitle, fontType: 1, fontSize: 14, color: Color.c10_34_64
        , title: "Transaction Successful!", placeHolder: "")
       Fonts().set(object: lblDescription, fontType: 0, fontSize: 12, color: Color.c52_58_64, title: "Your transfer of KES 4,689.00 to Kelly Abena was successful.", placeHolder: "")
       Fonts().set(object: lblTimeStatus, fontType: 1, fontSize: 10, color: Color.c10_34_64
            , title: "Today at 17:45", placeHolder: "")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setdata(dict:[String:String])
    {
        lblNotificationTitle.text = dict["atitle"]
        lblDescription.text = dict["abody"]
        lblTimeStatus.text = dict["atime"]
        imgStatus.sd_setImage(with: URL(string: dict["imageUrl"] ?? ""), placeholderImage: UIImage(named: "recieve"))
        
    }

}

class NotificationHeaderCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
      
    }

}
