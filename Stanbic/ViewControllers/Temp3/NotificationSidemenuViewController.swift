//
//  NotificationSidemenuViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 27/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class NotificationSidemenuViewController: UIViewController {
    
    @IBOutlet weak var tblViewNotification: UITableView!
    
    var arrNotification = [[String:String]]()
    
    var badgeCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialSetup()
        NotificationCenter.default.addObserver(self, selector: #selector(catchNotification), name: Notification.Name(rawValue: "notificationshow"), object: nil)
        
        if UserDefaults.standard.object(forKey: "badgeCount") != nil{
            badgeCount = UserDefaults.standard.object(forKey: "badgeCount") as! Int
        }
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp2.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func initialSetup()
    {
        initialSetUpNavigationBar(headerTitle:"NOTIFICATION")
    }
    
    func initialSetUpNavigationBar(headerTitle:String)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headerTitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        catchNotification()
        
        if badgeCount < 5 {
            UserDefaults.standard.setValue(0, forKey: "badgeCount")
            UserDefaults.standard.synchronize()
            tabBarController?.tabBar.items?[3].badgeValue = nil
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
        let count = badgeCount - 5
        if count < 1 {
            UserDefaults.standard.setValue(0, forKey: "badgeCount")
            UserDefaults.standard.synchronize()
            tabBarController?.tabBar.items?[3].badgeValue = nil
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
        else {
            UserDefaults.standard.setValue(count, forKey: "badgeCount")
            UserDefaults.standard.synchronize()
            tabBarController?.tabBar.items?[3].badgeValue = "\(count)"
            UIApplication.shared.applicationIconBadgeNumber = count
        }
        
    }
    
    // Fetch NOTIFICATIONS_DATA
    @objc func catchNotification(){
        
        arrNotification = CoreDataHelper().FetchNotifications(entity: Entity.NOTIFICATIONS_DATA)
        tblViewNotification.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    @IBAction func barBtnBackTapped(_ sender: UIBarButtonItem) {
        global.cupApi = "NO"
        setUpSlideMenu()
    }
}

extension NotificationSidemenuViewController: UITableViewDataSource,UITableViewDelegate
{
    //    func numberOfSections(in tableView: UITableView) -> Int
    //    {
    //    return 3
    //    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationHeaderCell") as! NotificationHeaderCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //        if section == 0 {
        //          return 2
        //        }
        //        else if section == 1 {
        //            return 1
        //        }
        // else {
        return arrNotification.count
        //  }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! NotificationTableCell
        
        if indexPath.row/2 == 0 {
            cell.imgStatus.image = UIImage.init(named: "recieve")
        }
        else {
            cell.imgStatus.image = UIImage.init(named: "send")
        }
        // if indexPath.section == 2 {
        let dict = arrNotification[indexPath.row]
        cell.lblNotificationTitle.text = dict["atitle"]
        cell.lblDescription.text = dict["abody"]
        cell.lblTimeStatus.text = dict["atime"]
        // }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 114
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 1 {
            return 51
        }else {
            return 0
        }
    }
}


extension NotificationSidemenuViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in tblViewNotification.visibleCells {
            let indexPath: IndexPath? = tblViewNotification.indexPath(for: cell)
            if let indexPath = indexPath {
               
                if indexPath.row < badgeCount {
                    let count = badgeCount - 2
                    if count < 1 {
                        UserDefaults.standard.setValue(0, forKey: "badgeCount")
                        UserDefaults.standard.synchronize()
                    }
                    else {
                        UserDefaults.standard.setValue(count, forKey: "badgeCount")
                        UserDefaults.standard.synchronize()
                    }
                }
                else {
                    let count = badgeCount - 2
                    if count < 1 {
                        UserDefaults.standard.setValue(0, forKey: "badgeCount")
                        UserDefaults.standard.synchronize()
                    }
                    else {
                        UserDefaults.standard.setValue(count, forKey: "badgeCount")
                        UserDefaults.standard.synchronize()
                    }
                }
                
                if UserDefaults.standard.object(forKey: "badgeCount") != nil{
                    badgeCount = UserDefaults.standard.object(forKey: "badgeCount") as! Int
                }
                
                
                UIApplication.shared.applicationIconBadgeNumber = badgeCount
                if badgeCount < 1{
                    tabBarController?.tabBar.items?[3].badgeValue = nil
                    UIApplication.shared.applicationIconBadgeNumber = 0
                }
                else {
                    tabBarController?.tabBar.items?[3].badgeValue = "\(badgeCount)"
                    UIApplication.shared.applicationIconBadgeNumber = badgeCount
                }
            }
        }
    }
}
