//
//  CalculateVAFRepaymentVC.swift
//  Stanbic
//
//  Created by 5Exceptions6 on 16/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class CalculateVAFRepaymentFirstVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
 //   @IBOutlet weak var txtSelectBranch: UITextField!
    @IBOutlet weak var txtInterestRate: UITextField!
    @IBOutlet weak var txtLoanAmmount: UITextField!
    @IBOutlet weak var txtmonths: UITextField!
    @IBOutlet weak var lblSelectRepayment: UILabel!
    @IBOutlet weak var btnCalculate: AttributedButton!
    @IBOutlet weak var btnRead: UIButton!
    @IBOutlet weak var btnconsBottom: NSLayoutConstraint!
    // READ DISCLAIMER
      @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var viewReadDesc: UIView!
     @IBOutlet weak var lblReadTitle: UILabel!
     @IBOutlet weak var lblReadmsg: UITextView!
     @IBOutlet weak var btncloseRead: UIButton!
    
    // View Leaves
    @IBOutlet var viewLeaves: UIView!
    @IBOutlet weak var lblSelectLeavesTitle: UILabel!
    @IBOutlet weak var tblViewLeaves: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    var imgBlack : UIImageView?
    var arrNumberOfLeaves = [String]()
    var linkUrl = ""
    var loan = ""
    var model = VAFLoanVM()
     let coreData = CoreDataHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblReadmsg.delegate = self
        model.delegate = self
        
        txtLoanAmmount.delegate = self
        txtInterestRate.delegate = self
 
         initialSetup()

          txtLoanAmmount.text = "KES "
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        txtInterestRate.keyboardType = .numberPad
        txtLoanAmmount.keyboardType = .numberPad
        txtmonths.keyboardType = .numberPad
        viewReadDesc.isHidden = true
        imgBlur.isHidden = true
        btncloseRead.isHidden = true
        
        let coreData = CoreDataHelper()
        let getData  = coreData.getDataForEntity(entity: Entity.CONTACT_INFO)
        //aWEBSITE
        let str =  global.loandesc// "DISCLAIMERMSG"
        let website = getData[0]["aWEBSITE"] ?? ""
         linkUrl = website
         setmsg(url: website,msg:str.localized(Session.sharedInstance.appLanguage))
        
       
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
     
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        imgBlur.addGestureRecognizer(tap)
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        viewReadDesc.isHidden = true
        imgBlur.isHidden = true
        btncloseRead.isHidden = true
        btnCalculate.isUserInteractionEnabled = true
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    // MARK: - Required Method's
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp3.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func initialSetup(){
        
        Fonts().set(object: txtInterestRate, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        txtInterestRate.addUI(placeholder: "INTERESTRATE1")
        Fonts().set(object: txtLoanAmmount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        if loan == "1"{
        txtLoanAmmount.addUI(placeholder: "LOANAMOUNTTITLE")
        }else if loan == "2"{
            txtLoanAmmount.addUI(placeholder: "LOANAMOUNTTITLE")
        }else {
            self.txtLoanAmmount.addUI(placeholder: "INCOMEAMOUNT")
        }
       // Fonts().set(object: lblSelectRepayment, fontType: 1, fontSize: 15, color: Color.c134_142_150, title: "SELECTREPAYMENT", placeHolder: "")
         Fonts().set(object: btnCalculate, fontType: 1, fontSize: 14, color: Color.white, title: "CALCULATE", placeHolder: "")
        Fonts().set(object: btnRead, fontType: 1, fontSize: 14, color: Color.c0_123_255, title: "READDISCLAMER", placeHolder: "")
        
        if loan == "1"{
        initialSetUpNavigationBar(headerTitle:"VAFREPAYMENT")
        }
        else if loan == "2"{
             initialSetUpNavigationBar(headerTitle:"HOMELOANREPAYMENT")
        }
        else {
           initialSetUpNavigationBar(headerTitle:"HOMELOANAFFORDABILITY")
        }
        
        Fonts().set(object: lblReadTitle, fontType: 1, fontSize: 15, color: Color.c52_58_64, title: "DISCLAIMER", placeHolder: "")
        Fonts().set(object: lblReadmsg, fontType: 0, fontSize: 13, color: Color.c52_58_64, title: "", placeHolder: "")
        
        Fonts().set(object: txtmonths, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        txtmonths.addUI(placeholder: "SELECTREPAYMENT")
    }
    
    func initialSetUpNavigationBar(headerTitle:String)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = headerTitle.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        txtLoanAmmount.didBegin()
     }
    
    @IBAction func btnCancelLeaves(_ sender: UIButton) {
        self.dismissPopup(popupView: viewLeaves)
    }
    
    @IBAction func btnSelectRepaymentPeriodTapped(_ sender: UIButton) {
        view.endEditing(true)
    //    txtSelectBranch.removeError()
     
        self.showPopup(popupView: viewLeaves, height: (arrNumberOfLeaves.count * 50) + 130)
    }
    
    @IBAction func btnRead(_ sender: UIButton) {
       // viewReadDesc.isHidden = false
        setView(view: viewReadDesc, hidden: false)
        setView(view: btncloseRead, hidden: false)
        setView(view: imgBlur, hidden: false)
      //  imgBlur.isHidden = false
       // btncloseRead.isHidden = false
        btnCalculate.isUserInteractionEnabled = false
    }
    
    @IBAction func btnCloseRead(_ sender: UIButton) {
         //viewReadDesc.isHidden = true
        setView(view: viewReadDesc, hidden: true)
         setView(view: btncloseRead, hidden: true)
         setView(view: imgBlur, hidden: true)
        
         //imgBlur.isHidden = true
          // btncloseRead.isHidden = true
         btnCalculate.isUserInteractionEnabled = true
    }
    
    @IBAction func btnCalculateTapped(_ sender: AttributedButton) {
        
        self.view.endEditing(true)
        var acno = ""
        
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            let acnos = acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!

            if acnos.contains("KES-"){
                let arrac = acnos.components(separatedBy: "KES-")[1]
                acno = arrac
            }
            else {
                acno = acnos
            }
        }
            
        let amounts = getAmountValdiation1(service_code : "LCT")
        let min = amounts.0
        let max = amounts.1
        let amount = removeKES(price : txtLoanAmmount.text ?? "")
        let msg = "\("AMOUNTBEETWEEN".localized(Session.sharedInstance.appLanguage)) \(min) \("-".localized(Session.sharedInstance.appLanguage)) \(max) "
        
        //view.endEditing(true)
        if txtLoanAmmount.text == "KES "{
            txtLoanAmmount.showError(errStr: "LOANAMOUNT")
        }
        else if txtLoanAmmount.text == "KES 0"{
            txtLoanAmmount.showError(errStr: "LOANAMOUNT")
        }
        else if checkAmount(min:min,max:max,amount:amount) == false {
            txtLoanAmmount.showError(errStr: msg)
        }
        else if txtInterestRate.text == "" || txtInterestRate.text == "%" || txtInterestRate.text == "0%" || txtInterestRate.text == "0"    {
         txtInterestRate.showError(errStr: "VALIDINTRESTRATE")
        }
        else if txtmonths.text == "" || txtmonths.text == "0"{
           txtmonths.showError(errStr: "VALIDREPAYMENT")
        }
        else if txtmonths.text == "SELECTREPAYMENT".localized(Session.sharedInstance.appLanguage) {
          txtmonths.showError(errStr: "SELECTREPAYMENT")
        }
        else {
            var type = ""
            if loan == "1"{
                type = "VAF"
            }
            else if loan == "2"{
                type = "HOMELOAN"
            }
            else {
                type = "HOMEAFFORD"
            }
            
            model.loancalulate(amount: txtLoanAmmount.text!, rate: txtInterestRate.text!, duration: txtmonths.text!, loantype: type)
        }
     }
    
    @IBAction func barBtnbackTapped(_ sender: UIBarButtonItem) {
       self.navigationController?.popViewController(animated: true)
    }
}

//Text field delegate
extension CalculateVAFRepaymentFirstVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtLoanAmmount {
            txtLoanAmmount.text = "KES "
            
        }
     
        textField.removeError()
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
     
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtLoanAmmount {
            
            textField.typingAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            let protectedRange = NSMakeRange(0, 4)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                
                return false
            }
            return true
        }
        else if textField == txtmonths {
            
            let maxLength = 3
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        else{
            return true
        }
    }
}

extension CalculateVAFRepaymentFirstVC
{
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.btnconsBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.btnconsBottom.constant = 54
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

extension CalculateVAFRepaymentFirstVC{
    
    func showPopup(popupView : UIView, height : Int) {
        
        imgBlack = UIImageView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        imgBlack?.alpha = 0.5
        //imgBlack?.backgroundColor = colorConstant.kBlackColor
        imgBlack?.isUserInteractionEnabled = true
        self.view.addSubview(imgBlack!)
        
        let frm = CGRect(x: ((self.view.frame.size.width / 2) - ((self.view.frame.size.width * 0.9) / 2)), y: -(popupView.frame.size.height), width: (self.view.frame.size.width * 0.9), height: CGFloat(height))
        
        popupView.frame = frm
        
        self.view.addSubview(popupView)
        
        popupView.layer.cornerRadius = 10
        popupView.clipsToBounds = true
        
        self.arrNumberOfLeaves.reverse()
        
        //  Fonts().set(object: lblSelectLeavesTitle, fontType: 1, fontSize: 20, color: Color.black, title: "NUMBEROFLEAVES", placeHolder: "")
        Fonts().set(object: btnCancel, fontType: 1, fontSize: 17, color: Color.black, title: "CANCEL", placeHolder: "")
        
        
        UIView.animate(withDuration: 0.5, animations: {
            
            let frm = CGRect(x: ((self.view.frame.size.width / 2) - ((self.view.frame.size.width * 0.9) / 2)), y: (self.view.frame.size.height / 2 - popupView.frame.size.height / 2), width: (self.view.frame.size.width * 0.9), height: CGFloat(height))
            
            popupView.frame = frm
            
        }){ (Bool) in
            
        }
    }
    
    
    // Dismiss popup
    func dismissPopup(popupView : UIView) {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            var frm =  popupView.frame
            
            frm.origin.y = -(popupView.frame.size.height)
            
            popupView.frame = frm
            
        }) { (Bool) in
            
            popupView.removeFromSuperview()
            
            self.imgBlack?.removeFromSuperview()
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrNumberOfLeaves.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LEAVES", for: indexPath) as! LeavesTableViewCell
        cell.lblNumberLeaves.text = arrNumberOfLeaves[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lblSelectRepayment.text = arrNumberOfLeaves[indexPath.row]
         self.dismissPopup(popupView: viewLeaves)
    }
}


extension CalculateVAFRepaymentFirstVC  : VAFLOANVMDelegate{
    
    // GET RESPONCE
     func loanData() {
        
     if model.responseData?["STATUS_CODE"] == 200 {
        
        let nextvc = CalculateVAFRepaymentSecondVC.getVCInstance() as! CalculateVAFRepaymentSecondVC
        let dict = ["rate":txtInterestRate.text!,"amount":txtLoanAmmount.text!,"duration":txtmonths.text!]
        nextvc.model = model
        nextvc.dictdata = dict
        nextvc.loan = loan
        self.navigationController?.pushViewController(nextvc, animated: true)
     }
     else
     {
        
        let nextvc = ErrorShowVC.getVCInstance() as! ErrorShowVC
        nextvc.message = model.responseData!["STATUS_MESSAGE"].stringValue
        nextvc.Flow = "1"
        self.navigationController?.pushViewController(nextvc, animated: true)
        // self.showMessage(title: "", message: model.responseData!["STATUS_MESSAGE"].stringValue)
    }
  }
}

extension CalculateVAFRepaymentFirstVC :UITextViewDelegate {
    
    func setmsg(url:String,msg:String){
        
   
        
        linkUrl = url
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 10
        let linespace = [NSAttributedString.Key.paragraphStyle : style,NSAttributedString.Key.foregroundColor:Color.c52_58_64]
     
        let attributedString = NSMutableAttributedString(string: "\(msg) \(url)",attributes: linespace)
        let url = URL(string: url)!
        
        // Set the 'click here' substring to be the link
        attributedString.setAttributes([.link: url], range: NSMakeRange(msg.count, linkUrl.count))
        self.lblReadmsg.attributedText = attributedString
        self.lblReadmsg.isUserInteractionEnabled = true
        self.lblReadmsg.isEditable = false
        
        // Set how links should appear: blue and underlined
        self.lblReadmsg.linkTextAttributes = [
            .foregroundColor: UIColor.blue,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        if (URL.absoluteString == linkUrl) {
            let viewController = WebViewController.getVCInstance() as! WebViewController
            viewController.url = linkUrl
            navigationController?.pushViewController(viewController, animated: true)
        }
        return false
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
}
extension UIView {
    func hideWithAnimation(hidden: Bool) {
        UIView.transition(with: self, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.isHidden = hidden
        })
    }
}
