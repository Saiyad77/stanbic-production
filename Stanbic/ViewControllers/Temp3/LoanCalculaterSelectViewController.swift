//
//  LoanCalculaterSelectViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 18/05/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class loanCell : UITableViewCell{
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imglogo: UIImageView!
    
    override func awakeFromNib() {
        Fonts().set(object: lblTitle, fontType: 1, fontSize: 14, color: Color.c10_34_64, title: "", placeHolder: "")
    }
}

class LoanCalculaterSelectViewController: UIViewController {
    
    @IBOutlet weak var tableViewLoan: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblHeaderTitle: UIView!
    @IBOutlet weak var viewContainer: UIView!
    private var lastContentOffset: CGFloat = 0
    @IBOutlet weak var lbltitle: UILabel!
    
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtFieldSearch: UITextField!
    @IBOutlet weak var viewtxtsearch: UIView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var imgViewGradient: UIImageView!
    
    //Variables
    var imgHeader =  UIImageView()
    var imgHederHeight : CGFloat = 0
    
    let arrtitle = ["Calculate your Vehicle Finance\nrepayment","Calculate your home loan\nrepayment","Calculate your home loan\naffordability"]
    let arrimg = ["car loan","Mortgage loans","home loanaffordability"]
    
    //MARK: View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        setupHeader()
        self.tabBarController?.tabBar.isHidden = true
        
       NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    
    func initialSetup(){
        
        Fonts().set(object: lbltitle, fontType: 1, fontSize: 22, color: Color.white, title: "LOANCALCULATERS", placeHolder: "")
        Fonts().set(object: lblHeaderTitle, fontType: 1, fontSize: 22, color: Color.white, title: "LOANCALCULATERS", placeHolder: "")
        Fonts().set(object: lblSubTitle, fontType: 0, fontSize: 15, color: Color.white, title: "LOANCALCULATERDES", placeHolder: "")
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Temp3.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func crossBtnAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
//        global.cupApi = "NO"
//        setUpSlideMenu()
    }
    
    func setUpSlideMenu() {
        
        let homeVC = NavHomeViewController.getVCInstance() as! NavHomeViewController
        let leftVC = SideLeftMenuViewController.getVCInstance() as! SideLeftMenuViewController
        
        leftVC.mainViewController = homeVC
        
        let slideMenuController = ExSlideMenuController(mainViewController: homeVC, leftMenuViewController: leftVC)
        slideMenuController.delegate = homeVC
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window!.rootViewController = slideMenuController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    @IBAction func barBtnBackTapped(_ sender: UIBarButtonItem) {
        global.cupApi = "NO"
        setUpSlideMenu()
    }
}

extension LoanCalculaterSelectViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrtitle.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! loanCell
        cell.lblTitle.text = arrtitle[indexPath.row]
        cell.imglogo.image = UIImage.init(named: arrimg[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let nextVC = CalculateVAFRepaymentFirstVC.getVCInstance() as! CalculateVAFRepaymentFirstVC
            nextVC.loan = "1"
            self.navigationController?.pushViewController(nextVC, animated: true)
        case 1:
            let nextVC = CalculateVAFRepaymentFirstVC.getVCInstance() as! CalculateVAFRepaymentFirstVC
            nextVC.loan = "2"
            self.navigationController?.pushViewController(nextVC, animated: true)
        case 2:
            let nextVC = CalculateVAFRepaymentFirstVC.getVCInstance() as! CalculateVAFRepaymentFirstVC
            nextVC.loan = "3"
            self.navigationController?.pushViewController(nextVC, animated: true)
        default: break
           return
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tblHeight.constant = self.tableViewLoan.contentSize.height
        let animation = AnimationFactory.makeFade(duration: 0.5, delayFactor: 0.0)
        let animator = Animator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: tableView)
    }
}

extension LoanCalculaterSelectViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = imgHederHeight - (scrollView.contentOffset.y + 10)
        let height = min(max(y, 150), 300)
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
        
        if scrollView.contentOffset.y > 50 {
            self.lblHeaderTitle.isHidden = false
        }
        else {
            self.lblHeaderTitle.isHidden = true
        }
    }
    
    func setupHeader() {
        
        imgHederHeight = 300
        scrollView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        scrollView.delegate = self
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: imgHederHeight)
        imgHeader.contentMode = .scaleAspectFill
        imgHeader.image = UIImage.init(named: "GradientBar-3")
        imgHeader.clipsToBounds = true
        
        view.addSubview(imgHeader)
        self.view.bringSubviewToFront(scrollView)
        self.view.bringSubviewToFront(btnBack)
        self.view.bringSubviewToFront(lblHeaderTitle)
    }
}
