//
//  SelectBranchViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 27/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class SelectBranchCell: UITableViewCell {
    
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var imgbranch: UIImageView!
    @IBOutlet weak var imgcheckmark: UIImageView!
    
    override func awakeFromNib() {
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 13, color: Color.c10_34_64, title: "", placeHolder: "")
       // Fonts().set(object: self.lblsubtitle, fontType: 0, fontSize: 11, color: Color.c134_142_150, title: "", placeHolder: "")
        imgbranch.layer.cornerRadius = imgbranch.frame.size.height / 2
        imgbranch.layer.borderWidth = 0.5
        imgbranch.layer.borderColor = UIColor.lightGray.cgColor
        imgbranch.layer.masksToBounds = true
        
    }
    
    func setData(title:String,subtitle:String){
        lbltitle.text = title
        lblsubtitle.text = title
        
    }
}

class SelectBranchViewController: UIViewController {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var txtsearch: UITextField!
    @IBOutlet weak var tableviewbranch: UITableView!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var tblHieght: NSLayoutConstraint!
    @IBOutlet weak var txtserachfield: UITextField!
    @IBOutlet weak var viewSearch: AttributedView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblHeaderTitle: UIView!
    private var lastContentOffset: CGFloat = 0
    
    //Mark:- Varibales
    var name = ""
    var delegate : SelectBranchDelegate?
    var arrBankBranch = [[String:String]]()
    var arrSearchInfo = [[String:String]]()
    var index = 999
    var search = false
    var isscroll = false
    var amount = ""
    var imgHeader =  UIImageView()
    var imgHederHeight : CGFloat = 0
      var noDataLabel = UILabel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFonts()
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        arrBankBranch =  CoreDataHelper().getDataForEntity(entity: Entity.BANK_BRANCHES_INFO);
        arrBankBranch.sort(by: {($0["aBANK_BRANCH"] as! String) < $1["aBANK_BRANCH"] as! String})

        tableviewbranch.reloadData()
        txtsearch.addTarget(self, action: #selector(SearchBank(_:)), for: UIControl.Event.editingChanged)
        txtsearch.autocorrectionType = .no
        setupHeader()
        noDataLabel = UILabel(frame: CGRect(x: 0, y: 10, width: 200, height: 18))
        noDataLabel.center.x = self.view.center.x
        noDataLabel.frame.origin.y = self.tableview.frame.minY + 270
        noDataLabel.numberOfLines = 0
        noDataLabel.textAlignment = .center
        noDataLabel.text = "No results found!"
        noDataLabel.font = UIFont.init(name: Fonts.kFontMedium, size: CGFloat(14))
        noDataLabel.textColor = .black
        self.noDataLabel.isHidden = true
        self.view.addSubview(noDataLabel)
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Rtgs.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 17, color: Color.white, title: "SELECTBRANCH", placeHolder: "")
        Fonts().set(object: self.lblHeaderTitle, fontType: 1, fontSize: 17, color: Color.white, title: "SELECTBRANCH", placeHolder: "")
        Fonts().set(object: self.txtsearch, fontType: 1, fontSize: 13, color: Color.c134_142_150, title: "", placeHolder: "SELECTFORBRANCH")
        Fonts().set(object: self.btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
        
    }
    
    // Searaching
    @objc func SearchBank(_ textField:UITextField)
    {
//        if textField.text?.count != 0 {
//            let resultObjectsArray = arrBankBranch.filter{
//                ($0["aBANK_BRANCH"] as! String).range(of: textField.text!,
//                                               options: .caseInsensitive,
//                                               range: nil,
//                                               locale: nil) != nil
//            }
//            if resultObjectsArray != nil
//            {
//                search = true
//                arrSearchInfo = resultObjectsArray
//            }
//            else {
//                search = false
//            }
//            self.tableview.reloadData()
//        }
//        else {
//            search = false
//            self.tableview.reloadData()
//        }
        arrSearchInfo.removeAll()
            if textField.text?.count != 0 {
                let resultObjectsArray = arrBankBranch.filter{
                                   ($0["aBANK_BRANCH"] as! String).range(of: textField.text!,
                                                                options: .caseInsensitive,
                                                              range: nil,
                                                                  locale: nil) != nil
                }
                arrSearchInfo = resultObjectsArray
               
            }
            search = true
            
            if arrSearchInfo.count > 0 {
                self.noDataLabel.isHidden = true
            }
            else{
                self.noDataLabel.isHidden = false
                if textField.text?.count == 0{
                    search = false
                    self.noDataLabel.isHidden = true
                }
                
            
            
            self.tableview.reloadData()
        }
   }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
                view.endEditing(true)
        if index == 999 {
            showMessage(title: "", message: "SELECTRECIENTBRANT")
        }
        else {
            delegate?.selectBranch(dictBanch: arrBankBranch[index])
            delegate?.setAmount(amount: amount)
            self.navigationController?.popViewController(animated: true)
            
        }
    }
}

//Text field delegate
extension SelectBranchViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
         let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
        
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        return (string == filtered)
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        // self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}


//UITableViewDataSource ,delegates
extension SelectBranchViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if search == false{
            return arrBankBranch.count
        }
        else{
            return arrSearchInfo.count
        }
     }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectBranchCell", for: indexPath) as! SelectBranchCell
        var dict = [String:String]()
        if search == false{
            dict = arrBankBranch[indexPath.row]
        }
        else{
            dict = arrBankBranch[indexPath.row]
        }
        cell.setData(title: dict["aBANK_BRANCH"]!, subtitle: dict["aBRANCH_CODE"]!)
        if name == dict["aBANK_BRANCH"]! {
            cell.imgcheckmark.image = UIImage.init(named: "check")
             cell.imgcheckmark.isHidden = false
        }
        else{
            cell.imgcheckmark.isHidden = true
           //cell.imgcheckmark.image = UIImage.init(named: "uncheck")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        index = indexPath.row
        
        if search == false{
            let dict = arrBankBranch[indexPath.row]
            name = dict["aBANK_BRANCH"]!
        }
        else{
            let dict = arrSearchInfo[indexPath.row]
            name = dict["aBANK_BRANCH"]!
        }
        tableview.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tblHieght.constant = tableviewbranch.contentSize.height
        let animation = AnimationFactory.makeFade(duration: 0.5, delayFactor: 0.0)
        let animator = Animator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: tableView)
    }
}


protocol SelectBranchDelegate{
    func selectBranch(dictBanch:[String:String])
   func setAmount(amount:String)
    
}

extension SelectBranchViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = imgHederHeight - (scrollView.contentOffset.y + 10)
        let height = min(max(y, 150), 400)
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
        
        if scrollView.contentOffset.y > 50 {
            self.lblHeaderTitle.isHidden = false
        }
        else {
            self.lblHeaderTitle.isHidden = true
        }
    }
    
    func setupHeader() {
        
        imgHederHeight = 210
        scrollView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        scrollView.delegate = self
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: imgHederHeight)
        imgHeader.contentMode = .scaleAspectFill
        imgHeader.image = UIImage.init(named: "GradientBar-3")
        imgHeader.clipsToBounds = true
        
        view.addSubview(imgHeader)
        self.view.bringSubviewToFront(scrollView)
        self.view.bringSubviewToFront(btnBack)
        self.view.bringSubviewToFront(lblHeaderTitle)
    }
}

