
//
//  InternalFundVM.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 27/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import Foundation


// ViewModel for PAY BILL
class IntenalFundVM {
    
    var amount: String?
    var acalias: String?
    var dictbank: [String:String]
    var anominatedaccount :String?
    var anominatealias :String?
    var nominateflag : String?
    var narration : String?
    
    init(dictbank:[String:String],anominatedaccount:String,amount:String,acalias:String,anominatealias:String,nominateflag:String, narration : String = "") {
        self.dictbank = dictbank
        self.anominatedaccount = anominatedaccount
        self.anominatealias = anominatealias
        self.amount = amount
        self.acalias = acalias
        self.nominateflag = nominateflag
        self.narration = narration
    }
    
    struct Response: Codable {
        
        let statusCode: Int?
        let statusMessage: String?
        let acalias: String?
        let amount: String?
        let reciptno: String?
        let reciptacc: String?
        let reciptbank: String?
        let reciptbankbranch: String?
        let reffrenceid: String?
       
        private enum CodingKeys: String, CodingKey {
            case statusCode = "STATUS_CODE"
            case statusMessage = "STATUS_MESSAGE"
            case acalias = "ACCOUNT_ALIAS"
            case amount = "AMOUNT"
            case reciptno = "RECIPIENT_ACC"
            case reciptacc = "RECIPIENT_ACC_NAME"
            case reciptbank = "RECIPIENT_BANK"
            case reciptbankbranch = "RECIPIENT_BANK_BRANCH"
            case reffrenceid = "REFERENCE_ID"
        
        }
    }
 }
