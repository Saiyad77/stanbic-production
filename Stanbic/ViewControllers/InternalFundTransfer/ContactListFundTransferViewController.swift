//
//  ContactListFundTransferViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 27/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit


class ContactListFundTransferViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate
{
    
 // @IBOutlet weak var searchBarHeight: NSLayoutConstraint!
    @IBOutlet weak var contactsTableView: UITableView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imgViewGradient: UIImageView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var txtFieldSearch: UITextField!
    @IBOutlet weak var viewtxtsearch: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblHeaderTitle: UIView!
    @IBOutlet weak var viewContainer: UIView!
    private var lastContentOffset: CGFloat = 0
    
    var noDataLabel = UILabel()
    var arrEnrolmentInfo = [[String:String]]()
    var arrSearchInfo = [[String:String]]()
    var search = false
    var imgHeader =  UIImageView()
    var imgHederHeight : CGFloat = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //searchBarHeight.constant = 0
        contactsTableView.delegate = self
        contactsTableView.dataSource = self
        
        viewtxtsearch.layer.shadowColor = Color.c10_34_64.cgColor
        viewtxtsearch.layer.shadowOpacity = 0.5
        viewtxtsearch.layer.shadowOffset = CGSize.zero
        viewtxtsearch.layer.shadowRadius = 0.5
        viewtxtsearch.layer.cornerRadius = 4
        
        // Sort data by type
        
        if CurrentFlow.flowType == FlowType.InternalFundTransfer{
            arrEnrolmentInfo = EnrollmentInfo().getEnrollmentinforfor(enrollmentinfo: CoreDataHelper().getDataForEntity(entity: Entity.BENEFICIARIES_INFO),  type: (CurrentFlow.flowType?.getType())!)
        }
            
        else if CurrentFlow.flowType == FlowType.Rtgs{
            
            arrEnrolmentInfo = EnrollmentInfo().getEnrollmentinforfor(enrollmentinfo: CoreDataHelper().getDataForEntity(entity: Entity.BENEFICIARIES_INFO),  type: (CurrentFlow.flowType?.getType())!)
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToAc{
            arrEnrolmentInfo = EnrollmentInfo().getEnrollmentinforfor(enrollmentinfo: CoreDataHelper().getDataForEntity(entity: Entity.BENEFICIARIES_INFO),  type: (CurrentFlow.flowType?.getType())!)
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToCard{
            arrEnrolmentInfo = EnrollmentInfo().getEnrollmentinforfor(enrollmentinfo: CoreDataHelper().getDataForEntity(entity: Entity.BENEFICIARIES_INFO),  type: (CurrentFlow.flowType?.getType())!) // TODO
        }
        else {
            
        }
            setFonts()
            setupHeader()
        
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        //no data label
        noDataLabel = UILabel(frame: CGRect(x: 0, y: 10, width: 200, height: 18))
        noDataLabel.center.x = self.view.center.x
        noDataLabel.frame.origin.y = self.contactsTableView.frame.minY + 250
        noDataLabel.numberOfLines = 0
        noDataLabel.text = "No results found!"
        noDataLabel.textAlignment = .center
        noDataLabel.font = UIFont.init(name: Fonts.kFontMedium, size: CGFloat(14))
        noDataLabel.textColor = .black
        self.noDataLabel.isHidden = true
        self.view.addSubview(noDataLabel)
        
        
        txtFieldSearch.addTarget(self, action: #selector(sortContacts(_:)), for: UIControl.Event.editingChanged)
        self.txtFieldSearch.delegate = self
        self.txtFieldSearch.autocorrectionType = .no
        txtFieldSearch.tintColor = UIColor.blue
        
        // CurrentFlow.flowType = FlowType.BuyAirtime
        
        if arrEnrolmentInfo.count == 0 {
          addContact()
        }
        
       // fetchContact()
        contactsTableView.reloadData()
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.addShadow(button: btnAdd)
        self.navigationController?.navigationBar.isHidden = true
        view.bringSubviewToFront(btnAdd)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Rtgs.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
       
        Fonts().set(object: txtFieldSearch, fontType: 0, fontSize: 13, color: Color.c10_34_64, title: "", placeHolder: "SEACHCONTACTS")
        

         if  CurrentFlow.flowType == FlowType.PesaLinkToPhone{
            Fonts().set(object: lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: CurrentFlow.flowType?.getContactListHeader() ?? "", placeHolder: "")
            Fonts().set(object: lblHeaderTitle, fontType: 1, fontSize: 15, color: Color.white, title: "SELECTCONTACT", placeHolder: "")
        }     else if  CurrentFlow.flowType == FlowType.PesaLinkToAc{
            Fonts().set(object: lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: CurrentFlow.flowType?.getContactListHeader() ?? "", placeHolder: "")
            Fonts().set(object: lblHeaderTitle, fontType: 1, fontSize: 15, color: Color.white, title: "SELECTCONTACT", placeHolder: "")
        } else if  CurrentFlow.flowType == FlowType.PesaLinkToCard {
            Fonts().set(object: lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: CurrentFlow.flowType?.getContactListHeader() ?? "", placeHolder: "")
            Fonts().set(object: lblHeaderTitle, fontType: 1, fontSize: 15, color: Color.white, title: "SELECTCONTACT", placeHolder: "")
         } else if  CurrentFlow.flowType == FlowType.Rtgs{
            Fonts().set(object: lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: CurrentFlow.flowType?.getContactListHeader() ?? "", placeHolder: "")
            Fonts().set(object: lblHeaderTitle, fontType: 1, fontSize: 15, color: Color.white, title: "SELECTCONTACT", placeHolder: "")
         } else {
            Fonts().set(object: lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: CurrentFlow.flowType?.getContactListHeader() ?? "", placeHolder: "")
            Fonts().set(object: lblHeaderTitle, fontType: 1, fontSize: 15, color: Color.white, title: CurrentFlow.flowType?.getContactListHeader() ?? "", placeHolder: "")
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func newContact(_ sender: Any) {
      addContact()
    }
    
    func addContact() {
        
        if CurrentFlow.flowType == FlowType.InternalFundTransfer {
            let nextvc = InternalFundTransferViewController.getVCInstance() as! InternalFundTransferViewController
            nextvc.nominateflag = "No"
            self.navigationController?.pushViewController(nextvc, animated: true)
        } else if CurrentFlow.flowType == FlowType.Rtgs {
            let nextvc = RTGSViewController.getVCInstance() as! RTGSViewController
            nextvc.nominateflag = "No"
            self.navigationController?.pushViewController(nextvc, animated: true)
        } else if CurrentFlow.flowType == FlowType.PesaLinkToAc {
            let nextvc = PesaLinkToAccountViewController.getVCInstance() as! PesaLinkToAccountViewController
            nextvc.nominateflag = "No"
            self.navigationController?.pushViewController(nextvc, animated: true)
        } else if CurrentFlow.flowType == FlowType.PesaLinkToCard {
            let nextvc = PesaLinkToAccountViewController.getVCInstance() as! PesaLinkToAccountViewController
            nextvc.nominateflag = "No"
            self.navigationController?.pushViewController(nextvc, animated: true)
        } else {
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HEADERCELL") as! ContactHeaderTableViewCell
        if search == false{
            if arrEnrolmentInfo.count > 0 {
                Fonts().set(object: cell.lblLetter, fontType: 1, fontSize: 13, color: Color.c10_34_64, title: "SAVEDBENEFICIARIES", placeHolder: "")
             }
            else {
                Fonts().set(object: cell.lblLetter, fontType: 1, fontSize: 13, color: Color.c10_34_64, title: "", placeHolder: "")
            }
        }
        else{
            if arrSearchInfo.count > 0 {
                Fonts().set(object: cell.lblLetter, fontType: 1, fontSize: 13, color: Color.c10_34_64, title: "SAVEDBENEFICIARIES", placeHolder: "")
             }
            else {
                Fonts().set(object: cell.lblLetter, fontType: 1, fontSize: 13, color: Color.c10_34_64, title: "", placeHolder: "")
            }
        }
         return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if search == false{
            return arrEnrolmentInfo.count
        }
        else{
            return arrSearchInfo.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as! ContactTableViewCell
        
        
        var contacts = [String:String]()
        
        if search == true {
            contacts = arrSearchInfo[indexPath.row]
        } else {
            contacts = arrEnrolmentInfo[indexPath.row]
        }
            var contactname = ""
            var accountnumber = contacts["aNOMINATED_ACCOUNT"]!
            if let dotRange = accountnumber.range(of: "*") {
                accountnumber.removeSubrange(dotRange.lowerBound..<accountnumber.endIndex)
            }
            if CurrentFlow.flowType == FlowType.InternalFundTransfer || CurrentFlow.flowType == FlowType.Rtgs  ||  CurrentFlow.flowType == FlowType.PesaLinkToAc {
            contactname = contacts["aNOMINATION_ALIAS"]!
            }
            
            else {
                let name = String(contacts["aNOMINATED_ACCOUNT"]!)
                contactname = name.replacingOccurrences(of: "\(accountnumber)*", with: "", options: NSString.CompareOptions.literal, range: nil)
            }
        
        cell.nameLabel.text = contactname
        cell.phoneNumberLabel.text = accountnumber.hashingOfTheAccount(noStr: accountnumber)
        cell.imgContact.isHidden = true
        cell.nameLetters.text = (contactname.characters.first?.description ?? "");
        
        cell.roundLabel()
        
     return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.view.endEditing(true)
        
        var contacts = [String:String]()
        
        if search == false {
            contacts = arrEnrolmentInfo[indexPath.row]
        } else{
            contacts = arrSearchInfo[indexPath.row]
        }
        
        
        var contactname = ""
        var accountnumber = contacts["aNOMINATED_ACCOUNT"]!
 //       var branch = arrEnrolmentInfo[]
         if let dotRange = accountnumber.range(of: "*") {
            accountnumber.removeSubrange(dotRange.lowerBound..<accountnumber.endIndex)
        }
        if CurrentFlow.flowType == FlowType.InternalFundTransfer{
            contactname = contacts["aNOMINATION_ALIAS"]!
        }
        else{
                let name = String(contacts["aNOMINATED_ACCOUNT"]!)
            contactname = name.replacingOccurrences(of: "\(accountnumber)*", with: "", options: NSString.CompareOptions.literal, range: nil)
        }
        
          accountnumber = removeSpecialCharsFromString(text: accountnumber)
        
        if CurrentFlow.flowType == FlowType.PesaLinkToAc {
            contactname = contacts["aNOMINATION_ALIAS"]!
        }
        
        if CurrentFlow.flowType == FlowType.InternalFundTransfer{
            let nextvc = InternalFundTransferViewController.getVCInstance() as! InternalFundTransferViewController
            nextvc.nominateAccount = accountnumber
            nextvc.nominateName = contactname
            nextvc.nominateflag = "Yes"
            nextvc.arrBankbranch = contacts

            self.navigationController?.pushViewController(nextvc, animated: true)
        }
        else if CurrentFlow.flowType == FlowType.Rtgs{
            let nextvc = RTGSViewController.getVCInstance() as! RTGSViewController
//            nextvc.nominateAccount = accountnumber
//            nextvc.nominateName = contactname
            nextvc.nominateflag = "Yes"
            nextvc.arrBankbranch = contacts
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToAc{
            
            let nextvc = PesaLinkToAccountViewController.getVCInstance() as! PesaLinkToAccountViewController
            nextvc.nominateAccount = accountnumber
            nextvc.nominateName = contactname
            nextvc.nominateflag = "Yes"
            nextvc.arrBankbranch = contacts
        
            self.navigationController?.pushViewController(nextvc, animated: true)
            
        }
        else if CurrentFlow.flowType == FlowType.PesaLinkToCard {
            
            let nextvc = PesaLinkToAccountViewController.getVCInstance() as! PesaLinkToAccountViewController
            nextvc.nominateAccount = accountnumber
            nextvc.nominateName = contactname
            nextvc.nominateflag = "Yes"
            nextvc.arrBankbranch = contacts
            
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
        else{
            
        }
    }
    
    @objc func sortContacts(_ textField:UITextField)
    {
        
        self.arrSearchInfo.removeAll()
        if textField.text?.count != 0 {
            let resultObjectsArray = arrEnrolmentInfo.filter{
                ($0["aNOMINATION_ALIAS"] as! String).range(of: textField.text!,
                                               options: .caseInsensitive,
                                               range: nil,
                                               locale: nil) != nil
            }
            arrSearchInfo = resultObjectsArray
        }
        search = true
        
        if arrSearchInfo.count > 0 {
            self.noDataLabel.isHidden = true
        } else{
            self.noDataLabel.isHidden = false
            if textField.text?.count == 0{
                search = false
                self.noDataLabel.isHidden = true
            }
            
        }
        
        self.contactsTableView.reloadData()
        
        
//        self.arrSearchInfo.removeAll()
//        if textField.text?.count != 0 {
//
//            for i in 0..<self.arrEnrolmentInfo.count {
//
//                let dictdata = self.arrEnrolmentInfo[i]
//                if (dictdata["aNOMINATION_ALIAS"]!.lowercased()).contains((textField.text!).lowercased()) {
//                    arrSearchInfo.append(dictdata)
//                }
//            }
//        }
//        if arrSearchInfo.count > 0 {
//            search = true
//            self.noDataLabel.isHidden = true
//         } else {
//            //search = false
//            self.noDataLabel.isHidden = false
//            if textField.text?.count == 0{
//                search = false
//                self.noDataLabel.isHidden = true
//            }
//         }
//        self.contactsTableView.reloadData()
        
    }
    
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(txtFieldSearch.isFirstResponder)
        {
            txtFieldSearch.resignFirstResponder()
        }
        return true
    }
    
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        var contentInset:UIEdgeInsets = self.contactsTableView.contentInset
        contentInset.bottom = (kb?.cgRectValue.height ?? 270)
        contactsTableView.contentInset = contentInset
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        contactsTableView.contentInset = contentInset
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
}

extension ContactListFundTransferViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        imgHederHeight = 250
        let y = imgHederHeight - (scrollView.contentOffset.y + 10)
        let height = min(max(y, 150), 400)
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height)
        if scrollView.contentOffset.y > 70 {
            self.lblHeaderTitle.isHidden = false
        } else {
            self.lblHeaderTitle.isHidden = true
        }
     }
    
    func setupHeaderCell() {
        //
        imgHederHeight = 200
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: imgHederHeight + 30)
        
    }
    func setupHeader() {
        
        imgHederHeight = 250
        scrollView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        scrollView.delegate = self
        imgHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: imgHederHeight + 30)
        imgHeader.contentMode = .scaleAspectFill
        imgHeader.image = UIImage.init(named: "GradientBar-3")
        imgHeader.clipsToBounds = true
        viewContainer.frame.size.height = viewContainer.frame.size.height + lblHeaderTitle.frame.size.height
        view.addSubview(imgHeader)
        self.view.bringSubviewToFront(scrollView)
        self.view.bringSubviewToFront(btnBack)
        self.view.bringSubviewToFront(lblHeaderTitle)
    }
    
    
    func removeSpecialCharsFromString(text: String) -> String {
        let withoutSpaces = text.replacingOccurrences(of: " ", with: "", options: .regularExpression)
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_")
        return String(withoutSpaces.filter {okayChars.contains($0) })
    }
}



