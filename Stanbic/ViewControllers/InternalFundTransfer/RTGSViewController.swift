//
//  RTGSViewController.swift
//  Stanbic
//
//  Created by Vijay Patidar on 30/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class RTGSViewController: UIViewController, UIGestureRecognizerDelegate {
    
    //Mark:- IBOUTLETS
    @IBOutlet weak var txtSelectBranch: UITextField!
    @IBOutlet weak var txtrecipientnumber: UITextField!
    @IBOutlet weak var txtamount: UITextField!
    @IBOutlet weak var txtrecipientname: UITextField!
    @IBOutlet weak var txtRecipientAddress: UITextField!
    @IBOutlet weak var txtNarration: UITextField!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var lblselectbranch: UILabel!
    @IBOutlet weak var Clsprice: UICollectionView!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lblrequestfor: UILabel!
    @IBOutlet weak var lblselectbank: UILabel!
    
    //Mark:- Varibales
    var editableTextfield : UITextField?
    private var lastContentOffset: CGFloat = 0
    let Pricearr = ["100", "250","500","1000"]
    
    // Variables for Bottom sheet
    var bottomSheetVC: BottomSheetViewController?
    var imgView = UIImageView()
    
    var indexcolletion = IndexPath()
    var nominateAccount = ""
    var nominateName = ""
    var nominateAddress = ""
    var nominateflag = ""
    var accountno = ""
    var arrBankbranch = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add Bottom sheet VC
        addBottomSheetView()
        bottomSheetVC!.delegate = self
        self.tabBarController?.tabBar.isHidden = true
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setFonts()
        
        //txtmobile.delegate = self
        txtamount.delegate = self
        
        txtamount.addUI(placeholder: "ENTER AMOUNT")
        txtrecipientname.addUI(placeholder: "RECIPIENTNAME")
        txtrecipientnumber.addUI(placeholder: "RECIPIENT ACCOUNT NUMBER")
          txtRecipientAddress.addUI(placeholder: "RECIPIENT ADDRESS")
        txtNarration.addUI(placeholder: "REASONFORPAYMENT")
        txtamount.text = "KES"
        txtamount.keyboardType = .numberPad
        if nominateName  == "" {
            txtrecipientnumber.isUserInteractionEnabled = true
            txtrecipientname.isUserInteractionEnabled = true
            txtRecipientAddress.isUserInteractionEnabled = true
            
            txtRecipientAddress.text = nominateAddress
            lblselectbranch.text = arrBankbranch["aBANK_NAME"] ?? "SELECTBANKRTGS".localized(Session.sharedInstance.appLanguage)
            txtrecipientname.text = arrBankbranch["aNOMINATION_ALIAS"] ?? ""
            
            let accountnumber = arrBankbranch["aNOMINATED_ACCOUNT"] ?? ""
            txtrecipientnumber.text = accountnumber
            
            
        }
        else{
            txtrecipientnumber.isUserInteractionEnabled = false
            txtrecipientname.isUserInteractionEnabled = false
            //txtrecipientnumber.text = nominateAccount
           // txtrecipientname.text = nominateName
          
        }
        
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            lblaccount.text = acdata[acdata.count - 1]["aACCOUNT_ALIAS"]!
            lblaccountnumber.text = acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!.hashingOfTheAccount(noStr: acdata[acdata.count - 1]["aACCOUNT_NUMBER"]!)
            accountno = acdata[acdata.count - 1]["aACCOUNT_NUMBER"] ?? ""
        }
        
       
        NotificationCenter.default.addObserver(self, selector: #selector(ReceivedNotification(notification:)), name: Notification.Name("Idle"), object: nil)
        
    }
    
    @objc func ReceivedNotification(notification: Notification) {
        
        let nextvc = LoginExistingViewController.getVCInstance() as! LoginExistingViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.rootViewController = nextvc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = true
     
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //txtmobile.didBegin()
       
        if txtamount.text == "KES" {
            txtamount.didBegin()
            txtamount.becomeFirstResponder()
        }
        
         if txtrecipientnumber.text != "" && txtrecipientname.text != "" {
            txtrecipientnumber.didBegin()
            txtrecipientname.didBegin()
         }
     }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: Storyboards.Rtgs.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        let (header, reviewTitle, buttonTitle) = (CurrentFlow.flowType?.pesaLinkData())!
        
     //   Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: header, placeHolder: "")
        Fonts().set(object: self.lblsubtitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: reviewTitle, placeHolder: "")
        
        Fonts().set(object: self.lblselectbank, fontType: 0, fontSize: 12, color: Color.c0_123_255, title: "SELECTBANKCAPS", placeHolder: "")
        Fonts().set(object: self.lblrequestfor, fontType: 0, fontSize: 12, color: Color.c0_123_255, title: "TRANSFERFROM", placeHolder: "")
        Fonts().set(object: lblselectbranch, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtamount, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtrecipientnumber, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtrecipientname, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
                Fonts().set(object: self.txtRecipientAddress, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblaccount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "", placeHolder: "")
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: buttonTitle, placeHolder: "")
        initialSetUpNavigationBar(title:header)
        lblselectbranch.text = "SELECTBANKRTGS".localized(Session.sharedInstance.appLanguage)
    }
    
    // MARK: - Required Method's
    func initialSetUpNavigationBar(title:String){
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .always
        } else {
            // Fallback on earlier versions
        }
        
        let navigation = self.navigationController?.navigationBar
        let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
        let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
        navigationItem.title = title.localized(Session.sharedInstance.appLanguage)
        
        navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
        
        if #available(iOS 11, *){
            navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
        }
        
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectbankbranch(_ sender: AttributedButton) {
        txtSelectBranch.removeError()
        view.endEditing(true)
        let nextvc = SelectBankPesaLInkViewController.getVCInstance() as! SelectBankPesaLInkViewController
        nextvc.delegate = self
        nextvc.name = lblselectbranch.text!
        nextvc.amount = txtamount.text ?? ""
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        view.endEditing(true)
        if lblaccountnumber.text == ""{
            showMessage(title: "", message: "SELECTACCOUNT")
            return
        }
        let amounts = getAmountValdiation1(service_code : "RTGS")
        let min = amounts.0
        let max = amounts.1
        let amount = removeKES(price : txtamount.text ?? "")
        let msg = "\("AMOUNTBEETWEEN".localized(Session.sharedInstance.appLanguage)) \(min) \("-".localized(Session.sharedInstance.appLanguage)) \(max) "
        
        if txtamount.text == "KES " || txtamount.text == "KES"{
            txtamount.becomeFirstResponder()
            txtamount.showError(errStr: "ENTERAMOUNT")
            return
        }
        else if checkAmount(min:min,max:max,amount:amount) == false {
             txtamount.becomeFirstResponder()
            txtamount.showError(errStr: msg)
        }
        else if lblselectbranch.text == "SELECTBANKRTGS".localized(Session.sharedInstance.appLanguage) {
          txtSelectBranch.becomeFirstResponder()
            txtSelectBranch.showError(errStr: "SELECTBANKRTGS")
            return
        }
        else if txtrecipientname.text == ""{
         txtrecipientname.becomeFirstResponder()
            txtrecipientname.showError(errStr: "GENRALREGAX")
            return
        }
        else if getgenralRegax(value: txtrecipientname.text!, type: "genral") == false {
          //  txtrecipientname.becomeFirstResponder()
            txtrecipientname.showError(errStr: "GENRALREGAX")
            
        }
        else if txtrecipientnumber.text == ""{
         txtrecipientnumber.becomeFirstResponder()
            txtrecipientnumber.showError(errStr: "RECIPIENTACCOUNTNAME")
            return
        }
        else if getgenralRegax(value: txtrecipientnumber.text!, type: "other") == false {
            txtrecipientnumber.showError(errStr: "ACCOUNTREGAX")
            
        }
        else if txtRecipientAddress.text == ""{
          txtRecipientAddress.becomeFirstResponder()
            txtRecipientAddress.showError(errStr: "RECIPIENTADDRESS")
            return
        }
        else if getgenralRegax(value: txtRecipientAddress.text!, type: "genral") == false {
            txtRecipientAddress.becomeFirstResponder()
            txtRecipientAddress.showError(errStr: "GENRALREGAX")
            
        }
        else if txtNarration.text == ""{
           txtNarration.becomeFirstResponder()
            txtNarration.showError(errStr: "REASONFORPAYMENT")
            return
        }
        else if getgenralRegax(value: txtNarration.text!, type: "genral") == false {
            txtNarration.becomeFirstResponder()
            txtNarration.showError(errStr: "GENRALREGAX")
            return
        }
        else{
            if nominateName == ""{
                nominateName = txtrecipientname.text!
                nominateAccount = txtrecipientnumber.text!
                nominateAddress = txtRecipientAddress.text!
            }
            
            let model = IntenalFundVM.init(dictbank: arrBankbranch,anominatedaccount: nominateAccount, amount: txtamount.text!,acalias: lblaccount.text!, anominatealias: nominateName,nominateflag:nominateflag, narration:  txtNarration.text ?? "")
            let nextvc = CheckoutAirTimeViewController.getVCInstance() as! CheckoutAirTimeViewController
            nextvc.self.internalfundModel = model
            nextvc.recipientAddress = nominateAddress
            nextvc.accountno = accountno
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
    
    @IBAction func accountselect(_ sender: AttributedButton) {
        view.endEditing(true)
        self.showBottomSheetVC(xibtype: "ACCOUNT", Selectname: lblaccount.text!)
    }
    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
   
    func showBottomSheetVC(xibtype: String,Selectname: String) {
        
        var height = 300  // 340
        let coreData = CoreDataHelper()
        if xibtype == "ACCOUNT" {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
                height = acdata.count * 100
                if acdata.count == 1 {
                    height = 200
                }
            }
        }
        else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acnetwork  = coreData.getDataForEntity(entity: Entity.MNO_INFO)
                height = acnetwork.count * 200
            }
        }
        
        // bottomSheetVC?.partialView = UIScreen.main.bounds.height - CGFloat(height)
        bottomSheetVC!.partialView = self.view.frame.size.height - CGFloat(height)
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imgView.backgroundColor = Color.black
        imgView.alpha = 0.2
        imgView.isUserInteractionEnabled = true
        self.view.addSubview(imgView)
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "", SelectName: "")
        imgView.removeFromSuperview()
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
}

//Text field delegate
extension RTGSViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
      if textField == txtamount {
            txtamount.text = "KES "
            indexcolletion = [1,6]
            Clsprice.reloadData()
        }
        textField.removeError()
        textField.didBegin()
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtamount {
            
            textField.typingAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            let protectedRange = NSMakeRange(0, 4)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                
                return false
            }
            return true
        } else {
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_ "
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        }
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

//Text field delegate
extension RTGSViewController : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Pricearr.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PricebuttonCell", for: indexPath as IndexPath) as! PricebuttonCell
        
        if indexcolletion == indexPath {
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.white, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = Color.c0_123_255
        }else{
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = .clear
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        indexcolletion = indexPath
        txtamount.text = "KES \(self.Pricearr[indexPath.row])"
        txtamount.removeError()
        txtamount.didBegin()
        Clsprice.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.Clsprice.frame.width/4) - 7
        return CGSize(width: width, height: 50)
        
    }
}

extension RTGSViewController : SelectNetworkDelegate{
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
        lblaccountnumber.text =  acnumber
        lblaccount.text = acname
    }
    
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
        hideBottomSheetVC()
        lblaccount.text = newtorkname
        lblaccountnumber.text = networkid.hashingOfTheAccount(noStr: networkid)
        accountno = networkid
    }
}


// SELECT BRANCH DATA :_
extension RTGSViewController:SelectBankDelegate {
    func setAmount(amount: String) {
        txtamount.text = amount
    }
    
    func selectBank(dictBank: [String : String]) {
        arrBankbranch = dictBank
        
        lblselectbranch.text = dictBank["aBANK"]
    }
}

extension RTGSViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
           // self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}



