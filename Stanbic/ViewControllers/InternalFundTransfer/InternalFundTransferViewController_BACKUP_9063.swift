//
//  InternalFundTransferViewController.swift
//  Stanbic
//
//  Created by 5exceptions mac 4 on 24/04/19.
//  Copyright © 2019 Eco Bank. All rights reserved.
//

import UIKit

class InternalFundTransferViewController: UIViewController,UIGestureRecognizerDelegate{
    
    //Mark:- IBOUTLETS
    
    @IBOutlet weak var txtrecipientnumber: UITextField!
    @IBOutlet weak var txtamount: UITextField!
    @IBOutlet weak var txtrecipientname: UITextField!
    @IBOutlet weak var txtNarration: UITextField!
    @IBOutlet weak var btnCountinue: UIButton!
    @IBOutlet weak var consBottom: NSLayoutConstraint!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblsubtitle: UILabel!
    @IBOutlet weak var lblselectbranch: UILabel!
    @IBOutlet weak var Clsprice: UICollectionView!
    @IBOutlet weak var lblaccount: UILabel!
    @IBOutlet weak var lblaccountnumber: UILabel!
    @IBOutlet weak var lblrequestfor: UILabel!
    @IBOutlet weak var lblrecipientbankbranch: UILabel!
    
    //Mark:- Varibales
    var editableTextfield : UITextField?
    private var lastContentOffset: CGFloat = 0
    let Pricearr = ["100", "250","500","1000"]
    
    // Variables for Bottom sheet
    var bottomSheetVC: BottomSheetViewController?
    var imgView = UIImageView()
    
    var indexcolletion = IndexPath()
    var nominateAccount = ""
    var nominateName = ""
    var nominateflag = ""
    var arrBankbranch = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Add Bottom sheet VC
        addBottomSheetView()
        bottomSheetVC!.delegate = self
        
        Clsprice?.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
        //Resistration for Keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setFonts()
        
        //txtmobile.delegate = self
        txtamount.delegate = self
        
        txtamount.addUI(placeholder: "ENTER AMOUNT")
        txtrecipientname.addUI(placeholder: "RECIPIENTNAME")
        txtrecipientnumber.addUI(placeholder: "RECIPIENT ACCOUNT NUMBER")
        txtNarration.addUI(placeholder: "REASONFORPAYMENT")
        txtamount.text = "KES"
               txtamount.keyboardType = .numberPad
        if nominateName  == "" {
            txtrecipientnumber.isUserInteractionEnabled = true
            txtrecipientname.isUserInteractionEnabled = true
        }
        else{
        txtrecipientnumber.isUserInteractionEnabled = false
        txtrecipientname.isUserInteractionEnabled = false
        txtrecipientnumber.text = nominateAccount
        txtrecipientname.text = nominateName
        //lblselectbranch.text = arrBenificialyData["aBANK_NAME"]!
           
        }
        
        let coreData = CoreDataHelper()
        if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
            let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
            lblaccount.text = acdata[0]["aACCOUNT_ALIAS"]!
            lblaccountnumber.text = acdata[0]["aACCOUNT_NUMBER"]!.hashingOfTheAccount(noStr: acdata[0]["aACCOUNT_NUMBER"]!)
        }
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        if nominateName != "" {
            txtrecipientname.didBegin()
            txtrecipientnumber.didBegin()
        }
        else {
            txtrecipientname.didBegin()
            txtrecipientname.becomeFirstResponder()
        }
        
        if txtamount.text!.count < 4  {
            txtamount.didBegin()
           // txtamount.becomeFirstResponder()
        }
    }
    
    static func getVCInstance() -> UIViewController{
        return UIStoryboard(name: Storyboards.Rtgs.rawValue, bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    // MARK:_SET FONT
    func setFonts() {
        
        if CurrentFlow.flowType == FlowType.Rtgs {
           // Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: "RTGS", placeHolder: "")
            initialSetUpNavigationBar(headerTitle: "RTGS")
        }
        else {
           // Fonts().set(object: self.lbltitle, fontType: 1, fontSize: 24, color: Color.white, title: "INTERNALFUNTRANSFER", placeHolder: "")
            initialSetUpNavigationBar(headerTitle: "INTERNALFUNTRANSFER")
        }
        
        Fonts().set(object: self.lblsubtitle, fontType: 1, fontSize: 17, color: Color.c10_34_64, title: "HOWMUCHWOULDYOULIKETOTRANSFER", placeHolder: "")
        Fonts().set(object: self.lblrecipientbankbranch, fontType: 1, fontSize: 9, color: Color.c0_123_255, title: "RECIPIENTBANKBRANCH", placeHolder: "")
        Fonts().set(object: self.lblrequestfor, fontType: 1, fontSize: 9, color: Color.c0_123_255, title: "TRANSFERFROM", placeHolder: "")
        Fonts().set(object: lblselectbranch, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "SELECTRECIENTBRANT", placeHolder: "")
         Fonts().set(object: self.txtamount, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtrecipientnumber, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.txtrecipientname, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblaccount, fontType: 0, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        Fonts().set(object: self.lblaccountnumber, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "", placeHolder: "")
        Fonts().set(object: btnCountinue, fontType: 1, fontSize: 14, color: Color.white, title: "CONTINUE", placeHolder: "")
    }

    // MARK: - Required Method's
    func initialSetUpNavigationBar(headerTitle:String){
    
    self.navigationController?.setNavigationBarHidden(false, animated: true)
    if #available(iOS 11.0, *) {
        navigationController?.navigationBar.prefersLargeTitles = true
    } else {
        // Fallback on earlier versions
    }
    if #available(iOS 11.0, *) {
        self.navigationItem.largeTitleDisplayMode = .always
    } else {
        // Fallback on earlier versions
    }
    
    let navigation = self.navigationController?.navigationBar
    let navigationFont = UIFont(name: Fonts.kFontMedium, size: 17)
    let navigationLargeFont = UIFont(name: Fonts.kFontMedium, size: 24) //34 is Large Title size by default
    navigationItem.title = headerTitle.localized(Session.sharedInstance.appLanguage)
    
    navigation?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationFont!]
    
    if #available(iOS 11, *){
        navigation?.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: Color.white, NSAttributedString.Key.font: navigationLargeFont!]
    }
<<<<<<< HEAD
        let bgimage = self.imageWithGradient(startColor: Color.c0_51_161, endColor: Color.c31_89_216, size: CGSize(width: UIScreen.main.bounds.size.width, height: 1))
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: bgimage!)
        self.navigationController?.navigationBar.isTranslucent = false
        
}
=======
    
    self.navigationController?.navigationBar.barTintColor = UIColor(patternImage:(UIImage(named: "GradientBar-2")?.resizableImage(withCapInsets: UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0), resizingMode: .stretch))!)
     self.navigationController?.navigationBar.isTranslucent = false
     }
>>>>>>> 631dd7df2c706561610350afdc46003a41c424bb

    //Mark:- Button Actions
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectbankbranch(_ sender: AttributedButton) {
        view.endEditing(true)
        let nextvc = SelectBranchViewController.getVCInstance() as! SelectBranchViewController
        nextvc.delegate = self
        nextvc.name = lblselectbranch.text!
        nextvc.amount = txtamount.text ?? ""
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    
    @IBAction func countinue(_ sender: AttributedButton) {
        
        if lblselectbranch.text == "SELECTRECIENTBRANT".localized(Session.sharedInstance.appLanguage) {
            showMessage(title: "", message: "SELECTRECIENTBRANT")
            return
        }
        else if txtrecipientname.text == ""{
          txtrecipientname.showError(errStr: "RECIPIENTNAME")
            return
        }
        else if txtrecipientnumber.text == ""{
           txtrecipientnumber.showError(errStr: "RECIPIENTACCOUNTNAME")
            return
        }
        else if txtamount.text == "KES " || txtamount.text == "KES"{
            txtamount.showError(errStr: "ENTERAMOUNT")
            return
        }
        else if txtNarration.text == ""{
            txtNarration.showError(errStr: "REASONFORPAYMENT")
            return
        }
        else {
            let amount = txtamount.text!.replacingOccurrences(of: "KES ", with: "", options: NSString.CompareOptions.literal, range:nil)
            
            if nominateName == ""{
             nominateName = txtrecipientname.text!
             nominateAccount = txtrecipientnumber.text!
            }
            
            let model = IntenalFundVM.init(dictbank: arrBankbranch,anominatedaccount: nominateAccount, amount: txtamount.text!,acalias: lblaccount.text!, anominatealias: nominateName,nominateflag:nominateflag, narration: txtNarration.text ?? "")
            let nextvc = CheckoutAirTimeViewController.getVCInstance() as! CheckoutAirTimeViewController
            nextvc.self.internalfundModel = model
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
    
    @IBAction func accountselect(_ sender: AttributedButton) {
        view.endEditing(true)
        self.showBottomSheetVC(xibtype: "ACCOUNT", Selectname: lblaccount.text!)
    }
    
    func addBottomSheetView() {
        
        bottomSheetVC = BottomSheetViewController()
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        
        self.addChild(bottomSheetVC!)
        self.view.addSubview(bottomSheetVC!.view)
        bottomSheetVC!.didMove(toParent: self)
        
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
  
    func showBottomSheetVC(xibtype: String,Selectname: String) {
        
        var height = 300  // 340
        let coreData = CoreDataHelper()
        if xibtype == "ACCOUNT" {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acdata  = coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)
                height = acdata.count * 200
                if acdata.count == 1 {
                    height = 200
                }
            }
        }
        else {
            if (coreData.getDataForEntity(entity: Entity.ACCOUNTS_INFO)).count > 0{
                let acnetwork  = coreData.getDataForEntity(entity: Entity.MNO_INFO)
                height = acnetwork.count * 200
            }
        }
        
        // bottomSheetVC?.partialView = UIScreen.main.bounds.height - CGFloat(height)
        bottomSheetVC!.partialView = self.view.frame.size.height - CGFloat(height)
        bottomSheetVC?.moveBottomSheet(xibtype:xibtype,SelectName: Selectname)
        addBGImage()
        self.view.bringSubviewToFront(bottomSheetVC!.view)
    }
    
    func addBGImage() {
        
        imgView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        imgView.backgroundColor = Color.black
        imgView.alpha = 0.2
        imgView.isUserInteractionEnabled = true
        self.view.addSubview(imgView)
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(self.tapGesture))
        gesture.delegate = self
        imgView.addGestureRecognizer(gesture)
    }
    
    func hideBottomSheetVC() {
        bottomSheetVC?.partialView = UIScreen.main.bounds.height
        bottomSheetVC?.moveBottomSheet(xibtype: "", SelectName: "")
        imgView.removeFromSuperview()
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        hideBottomSheetVC()
    }
}

//Text field delegate
extension InternalFundTransferViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtamount {
        txtamount.text = "KES "
            indexcolletion = [1,6]
            Clsprice.reloadData()
        }
        textField.removeError()
        if textField == txtNarration {
            Fonts().set(object: txtNarration, fontType: 1, fontSize: 15, color: Color.c10_34_64, title: "", placeHolder: "")
        }
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtamount {
            
            textField.typingAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            let protectedRange = NSMakeRange(0, 4)
            let intersection = NSIntersectionRange(protectedRange, range)
            if intersection.length > 0 {
                
                return false
            }
            return true
        }
        else{
            return true
        }
    }
    
    func textField(
        textField: UITextField,
        shouldChangeCharactersInRange range: NSRange,
        replacementString string: String)
        -> Bool
    {
        if textField.text == " "{
            return false
        }
        return true
    }
    
    // MARK: Keyboard hide/show Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let kb =   notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        self.consBottom.constant = ((kb?.cgRectValue.height)! + 20)
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        self.consBottom.constant = Session.sharedInstance.bottamSpace
        UIView.animate(withDuration: 0.5) { self.view.layoutIfNeeded() }
    }
}

//Text field delegate
extension InternalFundTransferViewController : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Pricearr.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PricebuttonCell", for: indexPath as IndexPath) as! PricebuttonCell
        
        if indexcolletion == indexPath {
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.white, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = Color.c0_123_255
        }else{
            Fonts().set(object: cell.lblprice, fontType: 1, fontSize: 11, color: Color.c134_142_150, title: "KES \(self.Pricearr[indexPath.row])", placeHolder: "")
            cell.viewbg.backgroundColor = .clear
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        indexcolletion = indexPath
        txtamount.text = "KES \(self.Pricearr[indexPath.row])"
        txtamount.removeError()
        txtamount.didBegin()
        Clsprice.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.Clsprice.frame.width/4) - 8
        return CGSize(width: width, height: collectionView.frame.size.height - 5)
        
    }
}

extension InternalFundTransferViewController : SelectNetworkDelegate{
    
    // Set account data 0 indexbydeafault
    func acDetails(acname: String, acnumber: String) {
        lblaccountnumber.text =  acnumber
        lblaccount.text = acname
    }
    
    // Select network/ account in bottomshit
    func selectnewtwork(newtorkname: String, networkid: String) {
        hideBottomSheetVC()
    }
}

// SELECT BRANCH DATA :_
extension InternalFundTransferViewController:SelectBranchDelegate {
    func setAmount(amount: String) {
        txtamount.text = amount
     }
    
    func selectBranch(dictBanch: [String : String]) {
        arrBankbranch = dictBanch
        lblselectbranch.text = dictBanch["aBANK_BRANCH"]
    }
}

extension InternalFundTransferViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (self.lastContentOffset > scrollView.contentOffset.y + 20) {
            self.view.endEditing(true)
        }
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}

